//
//  VenuesTVC.h
//  Phenom
//
//  Created by James Chung on 4/3/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddVenueTVC.h"
#import "Venue.h"
#import "VenueDetailTVC.h"
#import "AddVenueIntroVC.h"
#import "CustomBaseTVC.h"


@class VenuesTVC;

@protocol VenueTVC_Delegate <NSObject>
-(void)setVenueViewController:(VenuesTVC *)controller withVenue:(Venue *)venue;
@end

@interface VenuesTVC : CustomBaseTVC <AddVenueTVC_Delegate, CLLocationManagerDelegate, VenueDetailTVC_Delegate, AddVenueIntroVC_Delegate>
{
    CLLocationManager *locationManager;
}


@property (nonatomic, weak) id <VenueTVC_Delegate> delegate;
@property (nonatomic) BOOL showChooseButton;
@property (nonatomic) BOOL showMakeTempVenueButtonOnSubsequentVC;

@end
