//
//  ProfileViewController.h
//  Phenom
//
//  Created by James Chung on 3/20/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "CustomBaseTVC.h"

@interface ProfileSettingsVC : CustomBaseTVC <MBProgressHUDDelegate> {
    MBProgressHUD *HUD;
    NSOperationQueue         *operationQueue;
}


@end
