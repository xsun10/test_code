//
//  TipsCell.h
//  Vaiden
//
//  Created by Turbo on 8/4/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TipsCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *image;

@end
