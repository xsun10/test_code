//
//  FinalDisputeMessageVC.m
//  Vaiden
//
//  Created by James Chung on 9/3/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "FinalDisputeMessageVC.h"
#import "UIView+Manipulate.h"
#import "UIImage+ImageEffects.h"
#import "UIColor+VaidenColors.h"
#import "UserPreferences.h"

@interface FinalDisputeMessageVC ()
@property (weak, nonatomic) IBOutlet UIImageView *disputeIcon;
@property (weak, nonatomic) IBOutlet UIView *subBackground1;
@property (weak, nonatomic) IBOutlet UIButton *exitButtonHandle;
@property (weak, nonatomic) IBOutlet UIButton *rateButtonHandle;
@property (weak, nonatomic) IBOutlet UIScrollView *pageScrollView;
@property (weak, nonatomic) IBOutlet UIImageView *blurredImageBackground;
@property (nonatomic, strong) UIBarButtonItem *doneButton;
@property (nonatomic, strong) UserPreferences *preferences;

@end

@implementation FinalDisputeMessageVC

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //[self.pageScrollView setContentInset:UIEdgeInsetsMake(64, 0, 0, 0)];
 //   [self setPageBlurredBackgroundImage:1];
    

	// Do any additional setup after loading the view.
    //self.disputeIcon.image = [[UIImage imageNamed:@"BTN Dispute Score Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    //[self.disputeIcon setTintColor:[UIColor whiteColor]];
    
    // [self.subBackground1 makeRoundedBorderWithColor:[UIColor midGray2]];
    //[self.exitButtonHandle makeCircleWithColorAndBackground:[UIColor salmonColor] andRadius:5.0];
//    [self.rateButtonHandle makeRoundedBorderWithColor:[UIColor colorWithRed:51.0/255.0 green:153.0/255.0 blue:255.0/255.0 alpha:1.0 ]];
    //[self setDoneButton];
}


- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    self.pageScrollView.contentSize = CGSizeMake(320.0, 570.0);
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationItem setHidesBackButton:YES animated:YES];

}
/*- (void)setDoneButton
{
    UIButton *btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 50, 21);
    btn.layer.borderColor = [UIColor whiteColor].CGColor;
    btn.layer.borderWidth = 1.0;
    
    [btn addTarget:self action:@selector(doneAction:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundColor:[UIColor clearColor]];
    [btn setTitle:@"DONE" forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-MediumItalic" size:12.0]];
    
    // Initialize UIBarbuttonitem...
    self.doneButton = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = self.doneButton;
}*/

- (IBAction)doneAction:(id)sender
{
    [self.preferences setNewsfeedRefreshState:YES];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)setPageBlurredBackgroundImage:(NSUInteger)imageIndex
{
    NSString *effectText = @"";
    UIImage *effectImage = nil;
    UIImage *backImage = [UIImage imageNamed:@"BBall Player Background"];
    
    effectImage = backImage;
    
    switch (imageIndex)
    {
        case 0:
            effectImage = backImage;
            break;
        case 1:
            effectImage = [backImage applyLightEffect];
            effectText = NSLocalizedString(@"Light", @"");
            break;
        case 2:
            effectImage = [backImage applyExtraLightEffect];
            effectText = NSLocalizedString(@"Extra light", @"");
            break;
        case 3:
            effectImage = [backImage applyDarkEffect];
            effectText = NSLocalizedString(@"Dark", @"");
            break;
        case 4:
            effectImage = [backImage applyTintEffectWithColor:[UIColor blueColor]];
            effectText = NSLocalizedString(@"Color tint", @"");
            break;
    }
    
    self.blurredImageBackground.image = effectImage;
    
    
}
@end
