//
//  AddVenueTVC.h
//  Phenom
//
//  Created by James Chung on 4/3/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChooseStatesCDTVC.h"
#import <CoreLocation/CoreLocation.h>
#import "AddVenueSportsTVC.h"
#import "MBProgressHUD.h"
#import "CustomBaseTVC.h"

@class AddVenueTVC;

@protocol AddVenueTVC_Delegate <NSObject>
-(void)setMyVenueViewController:(AddVenueTVC *)controller andVenueID:(NSInteger)venueID andVenueName:(NSString *)venueName;
@end

@interface AddVenueTVC : CustomBaseTVC <ChooseStatesCDTVC_Delegate, AddVenueSportsTVC_Delegate, MBProgressHUDDelegate>
{
    MBProgressHUD *HUD;
}

@property (nonatomic, weak) id <AddVenueTVC_Delegate> delegate;

@property (nonatomic) BOOL isCreatingMatch;
@end
