//
//  SportRating.m
//  Vaiden
//
//  Created by James Chung on 10/7/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "SportRating.h"

@implementation SportRating

-(SportRating *)initWithSportRating:(float)rating
              byRatingPlayerID:(NSUInteger)ratingPlayerID
        byRatingPlayerUsername:(NSString *)ratingPlayerUsername
       byRatingPlayerPicString:(NSString *)ratingPlayerPicString
              forRatedPlayerID:(NSUInteger)ratedPlayerID
                   withComment:(NSString *)comment
                        onDate:(NSDate *)ratingDate
             forSportSkillAttribute:(SportSkillAttribute *)sportSkillAttribute

           
{
    self = [super init];
    
    if (self) {
        _ratingPlayer = [[Player alloc] initWithPlayerDetails:ratingPlayerID
                                                  andUserName:ratingPlayerUsername
                                          andProfilePicString:ratingPlayerPicString];
        _ratedPlayerID = ratedPlayerID;
        _comment = comment;
        _ratingDate = ratingDate;
        _rating = rating;
        _sportSkillAttribute = sportSkillAttribute;
    }
    
    return self;
}

+ (UIImage *)getRatingImageForRating:(float)rating
{
    UIImage *ratingImage = nil;
    NSInteger rate = (int)floor(rating);
    
    switch (rate) {
        case 0:
            ratingImage = [UIImage imageNamed:@"star zero"];
            break;
            
        case 1:
            ratingImage = [UIImage imageNamed:@"star one"];
            break;
            
        case 2:
            ratingImage = [UIImage imageNamed:@"star two"];
            break;
            
        case 3:
            ratingImage = [UIImage imageNamed:@"star three"];
            break;
            
        case 4:
            ratingImage = [UIImage imageNamed:@"star four"];
            break;
            
        case 5:
            ratingImage = [UIImage imageNamed:@"star five"];
            break;
            
        default:
            ratingImage = [UIImage imageNamed:@"star zero"];
            break;
    }
    return  ratingImage;
}

@end
