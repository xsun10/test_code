//
//  VenueCheckIn.m
//  Vaiden
//
//  Created by James Chung on 4/24/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "VenueCheckIn.h"
#import "NSDate+Utilities.h"

@implementation VenueCheckIn

-(VenueCheckIn *)initWithCheckInID:(NSInteger)checkInID
                 checkedInDateTime:(NSDate *)checkedInDateTime
                  checkedInMessage:(NSString *)message
                          isPublic:(BOOL)isPublic
                             isNow:(BOOL)isNow
                     checkedInUser:(Player *)checkedInUser
                    checkedInSport:(Sport *)checkedInSport
{
    self = [super init];
    
    if (self) {
        _checkedInUser = checkedInUser;
        _checkedInDateTime = checkedInDateTime;
        _message = message;
        _isPublic = isPublic;
        _isNow = isNow;
        _checkInID = checkInID;
        _checkedInSport = checkedInSport;
        
    }
    
    return self;
}


- (NSString *)getCheckedInStatusMessage
{
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDateComponents *componentsForFirstDate = [calendar components:NSDayCalendarUnit|NSMonthCalendarUnit|NSYearCalendarUnit fromDate:_checkedInDateTime];
    
    NSDateComponents *componentsForSecondDate = [calendar components:NSDayCalendarUnit|NSMonthCalendarUnit|NSYearCalendarUnit fromDate:[NSDate date]];
    
    NSString *todayOrDateString = @"";
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMMM d, YYYY"];
    NSString *dateText = [dateFormatter stringFromDate:_checkedInDateTime];
    
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    [timeFormatter setDateFormat:@"h:mm a"];
    NSString *timeText = [timeFormatter stringFromDate:_checkedInDateTime];
    
    if ([componentsForFirstDate day] == [componentsForSecondDate day]) {
        todayOrDateString = [NSString stringWithFormat:@"today at %@", timeText];
    } else if (![_checkedInDateTime isDateOverWeekAgo] && _isNow) {
        todayOrDateString = [NSDate timeDiffCalc:_checkedInDateTime];
    } else
        todayOrDateString = [NSString stringWithFormat:@"on %@ at %@", dateText, timeText];
    

    
    if (_isNow) {
        return [NSString stringWithFormat:@"checked into this location %@ to play %@", todayOrDateString, [_checkedInSport.sportName capitalizedString]];
    } else {
        return [NSString stringWithFormat:@"will be at this venue %@ to play %@", todayOrDateString, [_checkedInSport.sportName capitalizedString]];
    }
    
    return @"";
}

- (NSString *)getTimeStatusMessage:(BOOL)showAbbrev
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDateComponents *componentsForFirstDate = [calendar components:NSDayCalendarUnit|NSMonthCalendarUnit|NSYearCalendarUnit fromDate:_checkedInDateTime];
    
    NSDateComponents *componentsForSecondDate = [calendar components:NSDayCalendarUnit|NSMonthCalendarUnit|NSYearCalendarUnit fromDate:[NSDate date]];
    
    NSString *todayOrDateString = @"";
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMMM d, YYYY"];
    NSString *dateText = [dateFormatter stringFromDate:_checkedInDateTime];
    
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    [timeFormatter setDateFormat:@"h:mm a"];
    NSString *timeText = [timeFormatter stringFromDate:_checkedInDateTime];
    
    if ([componentsForFirstDate day] == [componentsForSecondDate day]) {
        todayOrDateString = [NSString stringWithFormat:@"today at %@", timeText];
    } else if (![_checkedInDateTime isDateOverWeekAgo] && _isNow && showAbbrev) {
        todayOrDateString = [NSDate timeDiffCalc:_checkedInDateTime];
    } else
        todayOrDateString = [NSString stringWithFormat:@"%@ at %@", dateText, timeText];
    
    
    
    if (_isNow) {
        return [NSString stringWithFormat:@"%@", todayOrDateString];
    } else {
        return [NSString stringWithFormat:@"will be here %@", todayOrDateString];
    }
    
    return @"";

}

- (NSString *)getAbbrevTimeStatusMessage:(BOOL)showAbbrev
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDateComponents *componentsForFirstDate = [calendar components:NSDayCalendarUnit|NSMonthCalendarUnit|NSYearCalendarUnit fromDate:_checkedInDateTime];
    
    NSDateComponents *componentsForSecondDate = [calendar components:NSDayCalendarUnit|NSMonthCalendarUnit|NSYearCalendarUnit fromDate:[NSDate date]];
    
    NSString *todayOrDateString = @"";
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMMM d, YYYY"];
    NSString *dateText = [dateFormatter stringFromDate:_checkedInDateTime];
    
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    [timeFormatter setDateFormat:@"h:mm a"];
    NSString *timeText = [timeFormatter stringFromDate:_checkedInDateTime];
    
    if ([componentsForFirstDate day] == [componentsForSecondDate day]) {
        todayOrDateString = [NSString stringWithFormat:@"today at %@", timeText];
    } else if (![_checkedInDateTime isDateOverWeekAgo] && _isNow && showAbbrev) {
        todayOrDateString = [NSDate timeDiffCalc:_checkedInDateTime];
    } else
        todayOrDateString = [NSString stringWithFormat:@"%@ at %@", dateText, timeText];
    
    /*
    
    if (_isNow) {
        return [NSString stringWithFormat:@"%@", todayOrDateString];
    } else {
        return [NSString stringWithFormat:@"will be here %@", todayOrDateString];
    }*/
    
    return todayOrDateString;
    
}



@end
