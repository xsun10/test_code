//
//  UpcomingIndividualMatchesCell.h
//  Phenom
//
//  Created by James Chung on 5/11/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IndividualMatchListCell : UITableViewCell
/*
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *profilePic;
@property (weak, nonatomic) IBOutlet UILabel *headlineLabel;
@property (weak, nonatomic) IBOutlet UILabel *probabilityLabel;
@property (weak, nonatomic) IBOutlet UILabel *probabilityMessage;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *venueLabel;
@property (weak, nonatomic) IBOutlet UILabel *streetLabel;
@property (weak, nonatomic) IBOutlet UILabel *cityStateLabel;
 */

@property (weak, nonatomic) IBOutlet UILabel *matchNameLabel;
@property (weak, nonatomic) IBOutlet UIView *sportIconBackground;
@property (weak, nonatomic) IBOutlet UIImageView *sportIcon;
@property (weak, nonatomic) IBOutlet UIImageView *opponentPic;
@property (weak, nonatomic) IBOutlet UILabel *matchHeading;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UILabel *sportLabel;
@property (weak, nonatomic) IBOutlet UIImageView *sessionUserProfilePic;
@property (weak, nonatomic) IBOutlet UIImageView *triangleIcon;
@property (weak, nonatomic) IBOutlet UIImageView *bulbIcon;
@property (weak, nonatomic) IBOutlet UIView *bulbIconBackground;
@property (weak, nonatomic) IBOutlet UIImageView *forwardIcon;
@property (weak, nonatomic) IBOutlet UIView *alertIconBackground;
@property (weak, nonatomic) IBOutlet UIImageView *alertIcon;
@property (weak, nonatomic) IBOutlet UIImageView *starIcon;
@property (weak, nonatomic) IBOutlet UILabel *matchTypeLabel;
//@property (weak, nonatomic) IBOutlet UIImageView *sportIcon;
@property (weak, nonatomic) IBOutlet UIButton *detailsButton;
@property (weak, nonatomic) IBOutlet UIImageView *sportPictureBackground;
@property (weak, nonatomic) IBOutlet UIImageView *newsfeedIcon;
@property (weak, nonatomic) IBOutlet UIImageView *player1ProfilePic;
@property (weak, nonatomic) IBOutlet UIImageView *player2ProfilePic;
@property (weak, nonatomic) IBOutlet UILabel *player1Username;
@property (weak, nonatomic) IBOutlet UILabel *player2Username;
@property (weak, nonatomic) IBOutlet UIView *vsCircle;

@property (weak, nonatomic) IBOutlet UIView *mainBackgroundViewArea;
@end
