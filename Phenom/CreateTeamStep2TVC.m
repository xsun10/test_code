//
//  CreateTeamStep2TVC.m
//  Vaiden
//
//  Created by James Chung on 4/6/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "CreateTeamStep2TVC.h"
#import "UserPreferences.h"
#import "IOSRequest.h"
#import "CreateTeamStep2PlayerCell.h"
#import "UIView+Manipulate.h"
#import "S3Tools.h"
#import "AFNetworking.h"
#import "UIImage+ProportionalFill.h"
#import "CreateTeamStep3VC.h"

@interface CreateTeamStep2TVC ()
@property (nonatomic, strong) UIBarButtonItem *doneButton;
@property (nonatomic, strong) UIView *tempView;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
@property (nonatomic, strong) UserPreferences *preferences;
@property (nonatomic, strong) NSMutableArray *followingList;
@property (nonatomic, strong) NSMutableArray *invitedList;

@end

@implementation CreateTeamStep2TVC

- (NSMutableArray *)invitedList
{
    if (!_invitedList) _invitedList = [[NSMutableArray alloc] init];
    return _invitedList;
}

- (NSMutableArray *)followingList
{
    if (!_followingList) _followingList = [[NSMutableArray alloc] init];
    return _followingList;
}

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
    
    [self setDoneButton];
    
    [self showTempSpinner];
    
    [IOSRequest fetchUserFollowingList:[self.preferences getUserID]
                         sessionUserID:[self.preferences getUserID]
                          onCompletion:^(NSMutableArray *results) {
                              
                              for (id object in results) {
                                  Player *p = [[Player alloc] initWithPlayerDetails:[object[@"user_id"] integerValue]
                                                                        andUserName:object[@"username"]
                                                                andProfilePicString:object[@"profile_pic_string"]];
                                  [self.followingList addObject:p];
                              }
                              
                              dispatch_async(dispatch_get_main_queue(), ^{
                                  [self.activityIndicator stopAnimating];
                                  self.tempView.hidden = YES;
                                  
                                  [self.tableView reloadData];
                              });
                              
                          }];
}

- (void)showTempSpinner
{
    self.tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    self.tempView.backgroundColor = [UIColor lightGrayColor];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] init];
    [self.activityIndicator setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width/2.0, [UIScreen mainScreen].bounds.size.height/2.0 )];
    [self.activityIndicator startAnimating];
    [self.tempView addSubview:self.activityIndicator];
    
    [self.view addSubview:self.tempView];
    
}

- (void)setDoneButton
{
    UIButton *btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 50, 21);
    btn.layer.borderColor = [UIColor whiteColor].CGColor;
    btn.layer.borderWidth = 1.0;
    
    [btn addTarget:self action:@selector(done:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundColor:[UIColor clearColor]];
    [btn setTitle:@"NEXT" forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-MediumItalic" size:12.0]];
    
    // Initialize UIBarbuttonitem...
    self.doneButton = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = self.doneButton;
}

- (void)done:(UIButton *)sender
{
    [self.nTeam setCaptain:[[Player alloc] initWithPlayerDetails:[self.preferences getUserID]
                                                     andUserName:[self.preferences getUserName]
                                             andProfilePicString:[self.preferences getProfilePicString]]];
    [self performSegueWithIdentifier:@"Step 3 Create Team Segue" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Step 3 Create Team Segue"]) {
        self.nTeam.players = [[NSMutableArray alloc] initWithArray:self.invitedList];
        CreateTeamStep3VC *controller = segue.destinationViewController;
        controller.nTeam = self.nTeam;
    }
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([self.followingList count] == 0)
        return 1;
    
    return [self.followingList count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Player Cell";
    CreateTeamStep2PlayerCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if ([self.followingList count] == 0) {
        cell.textLabel.hidden = NO;
        cell.textLabel.text = @"You are not following any players";
        cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Italic" size:13];
        cell.textLabel.textColor = [UIColor lightGrayColor];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.usernameLabel.hidden = YES;
        cell.inviteButton.hidden = YES;
        cell.profilePic.hidden = YES;
        
    } else {
        cell.usernameLabel.hidden = NO;
        cell.inviteButton.hidden = NO;
        cell.profilePic.hidden = NO;
        cell.textLabel.hidden = YES;
        Player *p = [self.followingList objectAtIndex:indexPath.row];
        cell.inviteButton.tag = indexPath.row;
        cell.usernameLabel.text = p.userName;
        [cell.inviteButton makeRoundedBorderWithRadius:3];
        
        NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                baseString:p.profileBaseString
                         ];
        
        [cell.profilePic setImageWithURL:[NSURL URLWithString:url]
                          placeholderImage:[UIImage imageNamed:@"avatar round"]];
        
        [UIImage makeRoundedImage:cell.profilePic withRadius:20];

        
    }
    
    return cell;
}
- (IBAction)inviteAction:(UIButton *)sender
{
    if ([sender.titleLabel.text isEqualToString:@"Invite"]) {
        [sender setTitle:@"Invited" forState:UIControlStateNormal];
        Player *p = [self.followingList objectAtIndex:sender.tag];
        [self.invitedList addObject:p];
    } else {
        [sender setTitle:@"Invite" forState:UIControlStateNormal];
        Player *p = [self.followingList objectAtIndex:sender.tag];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userID != %d", p.userID];
        self.invitedList = [NSMutableArray arrayWithArray:[self.invitedList filteredArrayUsingPredicate:predicate]];
    }
}
@end
