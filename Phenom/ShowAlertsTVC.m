//
//  ShowAlertsTVC.m
//  Vaiden
//
//  Created by James Chung on 5/6/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "ShowAlertsTVC.h"
#import "ShowAlertsCell.h"
#import "UserPreferences.h"
#import "IOSRequest.h"
#import "PlayerAlert.h"
#import "NSDate+Utilities.h"
#import "UIColor+VaidenColors.h"
#import "S3Tools.h"
#import "AFNetworking.h"
#import "UIImage+ProportionalFill.h"
#import "UIView+Manipulate.h"
#import "PlayerDetailTVC.h"
#import "Contact.h"
#import "PlayerAlertFollow.h"
#import "PlayerAlertTag.h"
#import "LockerPhotoDetailTVC.h"
#import "ShowAlertsMatchCell.h"
#import "IndividualMatchDetailTVC.h"
#import "PickupMatchDetailTVC.h"
#import "RecordIndMatchScoreIntroMessage.h"
#import "ConfirmIndMatchScoreIntroMessage.h"
#import "ConfirmIndMatchScoreSuccessVC.h"
#import "VenueDetailTVC.h"
#import "ShowAlertsUpcomingMatchesCell.h"
#import "MatchListTVC.h"
#import "NewsfeedAlert.h"
#import "ShowAlertsNewsfeedType1Cell.h"
#import "ShowAlertsNewsfeedType2Cell.h"
#import "S3Tools2.h"
#import "NewsFeedTVC.h"
#import "VoteAlert.h"
#import "GetVoteTVC.h"
#import "sendThanksDialog.h"
#import "ThanksTVCell.h"

@interface ShowAlertsTVC ()

@property (nonatomic, strong) UserPreferences *preferences;
@property (nonatomic, strong) NSMutableArray *socialAlertsArray;
@property (nonatomic, strong) NSMutableArray *matchAlertsArray;
@property (nonatomic, strong) UIView *tempView;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
@property (nonatomic, strong) UIView *overlay;
@property (nonatomic) NSInteger playerIDToSegue;
@property (nonatomic, strong) LockerPic *selectedLockerPic;
@property (nonatomic) NSInteger selectedLockerPicID;
@property (nonatomic) NSInteger selectedMatchID;
@property (nonatomic) NSInteger selectedNewsfeedID;
@property (nonatomic) NSInteger numUpcomingMatches;
@property (nonatomic) NSInteger numAlerts;
@property (nonatomic) NSInteger targetID;
@property (nonatomic) NSInteger alertID;
@end

@implementation ShowAlertsTVC

#define FOLLOWING_ALERT 1
#define REQUEST_TO_FOLLOW_ALERT 2
#define PHOTO_TAG_ALERT 3
#define NEWSFEED_PROP_ALERT 4
#define NEWSFEED_COMMENT_ALERT 5
#define GET_VOTE_ALERT 6
#define VOTE_THANKS_ALERT 7

#define SECTION_UPCOMING_MATCHES 0
#define SECTION_MATCH_ALERTS 1
#define SECTION_SOCIAL_ALERTS 2


- (NSMutableArray *)socialAlertsArray
{
    if (!_socialAlertsArray) _socialAlertsArray = [[NSMutableArray alloc] init];
    return _socialAlertsArray;
}

- (NSMutableArray *)matchAlertsArray
{
    if (!_matchAlertsArray) _matchAlertsArray = [[NSMutableArray alloc] init];
    return _matchAlertsArray;
}

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
    self.modalPresentationStyle = UIModalPresentationCurrentContext;
    self.tabBarController.modalPresentationStyle = UIModalPresentationCurrentContext;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshMatchNotificationsPageFromNSNotification:)
                                                 name:@"refreshMatchNotificationsPageNotification"
                                               object:nil];
    
    if (![self.preferences getHasMatchNotificationsPageBeenLoadedAtLeastOnce]) {
        [self.preferences setHasMatchNotificationsPageBeenLoadedAtLeastOnce:YES];
        [self.tabBarController setSelectedIndex:0];
      
    }
    
    [self.preferences setMatchNotificationRefreshState:NO];
    
    self.tableView.contentInset = UIEdgeInsetsMake(-36, 0, 0, 0);
    
    /* test */
    
//    PhenomAppDelegate *ad = (PhenomAppDelegate *)[[UIApplication sharedApplication] delegate];
//    ad.myTabBarController = self.tabBarController;

    
    
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];

    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(refreshView) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshControl];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];

    //[self refreshView];
    
    self.playerIDToSegue = 0;
    
    [self showTempSpinner];
 //   [self loadAlerts];
}

- (void)viewDidAppear:(BOOL)animated
{
    // clears the num bubble on the Vaiden tab page
    [IOSRequest setAlertsAsReadForUser:[self.preferences getUserID]
                          onCompletion:^(NSDictionary *results) {
                              
                              
                          }];
}

- (void)refreshView
{
    [self.socialAlertsArray removeAllObjects];
    [self.matchAlertsArray removeAllObjects];

    [self.refreshControl endRefreshing];
    
    [self loadAlerts];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.overlay setHidden:YES];
    //if ([self.preferences getAlertsPageRefreshState] == YES) {
        [self refreshView];
        [self.preferences setAlertsPageRefreshState:NO];
    //}
}

- (void)showTempSpinner
{
    self.tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    self.tempView.backgroundColor = [UIColor lightGrayColor];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] init];
    [self.activityIndicator setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width/2.0, [UIScreen mainScreen].bounds.size.height/2.0 )];
    [self.activityIndicator startAnimating];
    [self.tempView addSubview:self.activityIndicator];
    
    [self.view addSubview:self.tempView];
    
}

- (void)displayErrorOverlay
{
    self.overlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, [UIScreen mainScreen].bounds.size.height)];
    self.overlay.backgroundColor = [UIColor colorWithRed:239/255.0 green:239/255.0 blue:244/255.0 alpha:1.0];
    
    UIImage *img = [UIImage imageNamed:@"no_alert"];
    UIImageView *icon = [[UIImageView alloc] initWithImage:img];
    [icon setFrame:CGRectMake(130,110,50,50)];
    [icon setContentMode:UIViewContentModeCenter];
    
    UILabel *firstLine = [[UILabel alloc] initWithFrame:CGRectMake(35, 190, 240, 21)];
    firstLine.textColor = [UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:1.0];
    firstLine.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:15];
    firstLine.text = @"No Alerts";
    [firstLine setTextAlignment:NSTextAlignmentCenter];
    
    UILabel *secondLine = [[UILabel alloc] initWithFrame:CGRectMake(20, 210, 260, 63)];
    secondLine.textColor = [UIColor colorWithRed:170/255.0 green:170/255.0 blue:170/255.0 alpha:1.0];
    secondLine.font = [UIFont fontWithName:@"HelveticaNeue" size:13];
    secondLine.text = @"You do not have any social or match alerts yet. The more you interact, the more alerts you will receive.";
    secondLine.numberOfLines = 0;
    [secondLine setTextAlignment:NSTextAlignmentCenter];
    
    [self.overlay addSubview:icon];
    [self.overlay addSubview:firstLine];
    [self.overlay addSubview:secondLine];
    
    [self.tableView addSubview:self.overlay];
}

- (void)loadAlerts
{
    [IOSRequest fetchAlertsForUser:[self.preferences getUserID]
                      onCompletion:^(NSDictionary *results) {
                          NSLog(@"%@",results[@"social_alerts"]);
                          for (id object in results[@"social_alerts"]) {
                              NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
                              [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                              NSDate *dateTime = [[formatter dateFromString:object[@"ts"]] toLocalTime];
                              if ([object[@"alert_type"] integerValue] == FOLLOWING_ALERT ||
                                  [object[@"alert_type"] integerValue] == REQUEST_TO_FOLLOW_ALERT) {
                                  PlayerAlertFollow *pa = [[PlayerAlertFollow alloc] initWithPlayerAlertDetails:[object[@"user_id"] integerValue]
                                                                                                    andUserName:object[@"username"]
                                                                                            andProfilePicString:object[@"profile_pic_string"]
                                                                                                     andMessage:object[@"message"]
                                                                                                   andAlertType:[object[@"alert_type"] integerValue]
                                                                                                   andAlertDate:dateTime
                                                                                                   andWasViewed:[object[@"was_viewed"] boolValue]
                                                                                                andFollowStatus:[object[@"follow_status"] integerValue]];
                                  [pa.refPlayer setApprovalRequiredToFollow:[object[@"need_approval_flag"] boolValue]];
                                  
                                  [self.socialAlertsArray addObject:pa];
                              } else if ([object[@"alert_type"] integerValue] == PHOTO_TAG_ALERT) {
                                  NSUInteger barIndex = [self.socialAlertsArray indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
                                      PlayerAlert *pa = (PlayerAlert*)obj;
                                      
                                      if (pa.refPlayer.userID == [object[@"creator_id"] integerValue] && [pa isKindOfClass:[PlayerAlertTag class]] &&
                                          (([pa.alertDateTime compare:[dateTime dateByAddingTimeInterval:-1800]] == NSOrderedDescending &&
                                            [pa.alertDateTime compare:[dateTime dateByAddingTimeInterval:1800]] == NSOrderedAscending))
                                          ) {
                                          *stop = YES;
                                          return YES;
                                      }
                                      return NO;
                                  }];
                                  
                                  if (barIndex != NSNotFound) {
                                      // then add to existing pa object
                                      PlayerAlertTag *pa = [self.socialAlertsArray objectAtIndex:barIndex];
                                      LockerPic *lp = [[LockerPic alloc] init];
                                      [lp setLockerID:[object[@"locker_id"] integerValue]];
                                      [lp setImageFileName:object[@"image_name"]];
                                      [pa.tagDataArray addObject:lp];
                                      [self.socialAlertsArray replaceObjectAtIndex:barIndex withObject:pa];
                                  } else {
                                      // then create a new pa object
                                      PlayerAlertTag *pa = [[PlayerAlertTag alloc] initWithPlayerAlertTagDetails:[object[@"creator_id"] integerValue]
                                                                                                     andUserName:object[@"creator_username"]
                                                                                             andProfilePicString:object[@"creator_profile_pic_string"]
                                                                                                      andMessage:object[@"message"]
                                                                                                    andAlertType:[object[@"alert_type"] integerValue]
                                                                                                    andAlertDate:dateTime
                                                                                                    andWasViewed:[object[@"was_viewed"] boolValue]
                                                                                                    withLockerID:[object[@"locker_id"] integerValue]
                                                                                                    andImageName:object[@"image_name"]];
                                      [self.socialAlertsArray addObject:pa];
                                      
                                  }
                              } else if ([object[@"alert_type"] integerValue] == NEWSFEED_PROP_ALERT ||
                                         [object[@"alert_type"] integerValue] == NEWSFEED_COMMENT_ALERT) {
                                  
                                  NewsfeedAlert *na = [[NewsfeedAlert alloc] initWithPlayerAlertDetails:[object[@"user_id"] integerValue]
                                                                                            andUserName:object[@"username"]
                                                                                    andProfilePicString:object[@"profile_pic_string"]
                                                                                             andMessage:object[@"message"]
                                                                                           andAlertType:[object[@"alert_type"] integerValue]
                                                                                           andAlertDate:dateTime
                                                                                           andWasViewed:[object[@"was_viewed"] boolValue]
                                                                                          andNewsfeedID:[object[@"newsfeed_id"] integerValue]
                                                                                    andNewsfeedPostType:[object[@"newsfeed_post_type"] integerValue]];
                                  NSString *strType = [na getStringRepresentationOfNewsfeedType];
                                  if ([strType isEqualToString:@"individual"] || [strType isEqualToString:@"pickup"]) {
                                      [na setSupplementaryName:object[@"match_name"]];
                                  } else if ([strType isEqualToString:@"venue_checkin"] || [strType isEqualToString:@"venue_king"]) {
                                      [na setSupplementaryName:object[@"venue_name"]];
                                  } else if ([strType isEqualToString:@"locker"]) {
                                      [na setSupplementaryName:object[@"image_name"]];
                                  }
                                  
                                  
                                  if ([object[@"alert_type"] integerValue] == NEWSFEED_COMMENT_ALERT) {
                                      [na setCommentID:[object[@"comment_id"] integerValue]];
                                  }
                                  
                                  [self.socialAlertsArray addObject:na];
                              } else if ([object[@"alert_type"] integerValue] == GET_VOTE_ALERT) {
                                  VoteAlert *alert = [[VoteAlert alloc] initWithVoteAlertDetails:[object[@"alert_id"] integerValue]
                                                                                        fromUser:[object[@"user_id"] integerValue]
                                                                                     andUserName:object[@"username"]
                                                                             andProfilePicString:object[@"profile_pic_string"]
                                                                                      andMessage:object[@"message"]
                                                                                    andAlertType:[object[@"alert_type"] integerValue]
                                                                                    andAlertDate:dateTime
                                                                                    andWasViewed:[object[@"was_viewed"] boolValue]
                                                                                   andVoteNumber:[object[@"follow_status"] integerValue]
                                                                                         andSent:[object[@"sent"] integerValue]];
                                  
                                  [self.socialAlertsArray addObject:alert];
                              } else if ([object[@"alert_type"] integerValue] == VOTE_THANKS_ALERT) {
                                  VoteAlert *alert = [[VoteAlert alloc] initWithVoteAlertThanksDetails:[object[@"user_id"] integerValue]
                                                                                           andUserName:object[@"username"]
                                                                                   andProfilePicString:object[@"profile_pic_string"]
                                                                                            andMessage:object[@"message"]
                                                                                          andAlertType:[object[@"alert_type"] integerValue]
                                                                                          andAlertDate:dateTime
                                                                                          andWasViewed:[object[@"was_viewed"] boolValue]];
                                  
                                  [self.socialAlertsArray addObject:alert];
                              }
                          }
                          
                          // Match Alerts
                          id matchAlertsObject = (id)results[@"match_alerts"];
                          //                      self.totalAlerts = [matchAlertsObject[@"total_count"] integerValue];
                          // Need to sort the data array on the server side
                          for (id alertSubObject in matchAlertsObject[@"data"]) {
                              
                              NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
                              [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                              NSDate *alertDateTime = [[formatter dateFromString:alertSubObject[@"ts"]] toLocalTime];
                              
                              if ([alertSubObject[@"match_category"] isEqualToString:@"individual"]) {
                                  IndividualMatch *scoreIndividualMatch = [[IndividualMatch alloc] init];
                                  [scoreIndividualMatch setMatchID:[alertSubObject[@"match_id"] integerValue]];
                                  [scoreIndividualMatch setMatchName:alertSubObject[@"match_name"]];
                                  [scoreIndividualMatch setDateTime:alertDateTime];
                                  [scoreIndividualMatch setMatchNotification:alertSubObject[@"match_notification"]];
                                  
                                  MatchPlayer *player2 = [MatchPlayer makeMatchCompetitorObject:alertSubObject[@"competitors"]
                                                                                      forPlayer:2
                                                                                   forSportName:alertSubObject[@"sport_name"]
                                                                                        andType:alertSubObject[@"sport_type"]];
                                  
                                  MatchPlayer *player1 = [MatchPlayer makeMatchCompetitorObject:alertSubObject[@"competitors"]
                                                                                      forPlayer:1
                                                                                   forSportName:alertSubObject[@"sport_name"]
                                                                                        andType:alertSubObject[@"sport_type"]];
                                  [scoreIndividualMatch setPlayer1:player1];
                                  [scoreIndividualMatch setPlayer2:player2];
                                  Sport *sport = [[Sport alloc] initWithSportDetails:[alertSubObject[@"sport_id"] integerValue]
                                                                             andName:alertSubObject[@"sport_name"]
                                                                             andType:alertSubObject[@"sport_type"]];
                                  
                                  [scoreIndividualMatch setSport:sport];
                                  [scoreIndividualMatch setMatchStatus:alertSubObject[@"match_status"]];
                                  [self.matchAlertsArray addObject:scoreIndividualMatch];
                              } else {
                                  PickupMatch *alertPickupMatch = [[PickupMatch alloc] init];
                                  [alertPickupMatch setMatchID:[alertSubObject[@"match_id"] integerValue]];
                                  [alertPickupMatch setMatchName:alertSubObject[@"match_name"]];
                                  [alertPickupMatch setDateTime:alertDateTime];
                                  [alertPickupMatch setMatchNotification:alertSubObject[@"match_notification"]];
                                  [alertPickupMatch setMatchCreatorID:[alertSubObject[@"creator_id"] integerValue]];
                                  [alertPickupMatch setMatchCreatorUsername:alertSubObject[@"creator_username"]];
                                  
                                  NSMutableArray *competitorArray = [PickupMatch makeCompetitorArray:alertSubObject[@"competitors"]
                                                                                        forSportName:alertSubObject[@"sport_name"]
                                                                                             andType:alertSubObject[@"sport_type"]];
                                  [alertPickupMatch setCompetitorArray:competitorArray];
                                  
                                  Sport *sport = [[Sport alloc] initWithSportDetails:[alertSubObject[@"sport_id"] integerValue]
                                                                             andName:alertSubObject[@"sport_name"]
                                                                             andType:alertSubObject[@"sport_type"]];
                                  
                                  [alertPickupMatch setSport:sport];
                                  [alertPickupMatch setMatchStatus:alertSubObject[@"match_status"]];
                                  
                                  [self.matchAlertsArray addObject:alertPickupMatch];
                              }
                          }
                          
                          
                          [self setAlertNumInTagWithValue:[results[@"num_alerts"] integerValue]];
                          
                          self.numUpcomingMatches = [results[@"num_upcoming_matches"] integerValue];
                          
                          dispatch_async(dispatch_get_main_queue(), ^{
                              [self.activityIndicator stopAnimating];
                              self.tempView.hidden = YES;
                              
                              if ([self.matchAlertsArray count] == 0 && [self.socialAlertsArray count] == 0) {
                                  [self displayErrorOverlay];
                              } else {
                                  [self.tableView reloadData];
                              }
                          });
                      }];
    
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == SECTION_MATCH_ALERTS && [self.matchAlertsArray count] > 0) {
        return @"MATCHES";
    }  else if (section == SECTION_SOCIAL_ALERTS && [self.socialAlertsArray count] > 0) {
        return @"SOCIAL";
    }
    
    return @"";
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == SECTION_UPCOMING_MATCHES && self.numUpcomingMatches > 0)
        return 1;
    else if (section == SECTION_MATCH_ALERTS && [self.matchAlertsArray count] > 0)
        return [self.matchAlertsArray count];
    else if (section == SECTION_SOCIAL_ALERTS && [self.socialAlertsArray count] > 0)
        return [self.socialAlertsArray count];
    /*
    if (section == SECTION_SOCIAL_ALERTS
        || (section == SECTION_MATCH_ALERTS && [self.matchAlertsArray count] == 0)
        || (section == SECTION_UPCOMING_MATCHES && self.numUpcomingMatches == 0)) {
        return [self.socialAlertsArray count];
    } else if (section == SECTION_MATCH_ALERTS) {
        return [self.matchAlertsArray count];
    } else if (section == SECTION_UPCOMING_MATCHES && self.numUpcomingMatches > 0)
        return 1;
    */
    
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{/*
    NSInteger factorUpcoming = 0;
    NSInteger factorMatchAlerts = 0;
    NSInteger factorSocialAlerts = 0;

    if (self.numUpcomingMatches > 0)
        factorUpcoming = 1;
    
    if ([self.socialAlertsArray count] > 0)
        factorSocialAlerts = 1;
    
    if ([self.matchAlertsArray count] > 0)
        factorMatchAlerts = 1;
    
    return factorUpcoming + factorSocialAlerts + factorMatchAlerts;
    */
    
    return 3;
    
    /*
    if (self.numUpcomingMatches > 0 && [self.socialAlertsArray count] > 0 & [self.matchAlertsArray count] > 0) {
        return 3;
    }
    if ([self.socialAlertsArray count] > 0 && [self.matchAlertsArray count] > 0)
        return 2;
    else if ([self.socialAlertsArray count] > 0 || [self.matchAlertsArray count] > 0)
        return 1;

    return 0;*/
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == SECTION_UPCOMING_MATCHES)
        return 0;
    else if ([self.matchAlertsArray count] > 0 && section == SECTION_MATCH_ALERTS)
        return 30;
    else if ([self.socialAlertsArray count] > 0 && section == SECTION_SOCIAL_ALERTS)
        return  30;
    
        
    return 0;
}
#define LABEL_LENGTH_TYPE1 0  // long
#define LABEL_LENGTH_TYPE2 1  // short

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

    
    if ((indexPath.section == SECTION_SOCIAL_ALERTS && [self.socialAlertsArray count] > 0) ||
        (indexPath.section == SECTION_MATCH_ALERTS && [self.matchAlertsArray count] == 0)) {
        if ([[self.socialAlertsArray objectAtIndex:indexPath.row] isKindOfClass:[PlayerAlertFollow class]]
            || [[self.socialAlertsArray objectAtIndex:indexPath.row] isKindOfClass:[VoteAlert class]]) {
            CGFloat testNum = [self calculateHeightForTextPost:indexPath.row];
            if (testNum < 20)
                return 60;
            else
                return 40 + testNum;
        } else if ([[self.socialAlertsArray objectAtIndex:indexPath.row] isKindOfClass:[PlayerAlertTag class]]) {
            return 92;
        } else if ([[self.socialAlertsArray objectAtIndex:indexPath.row] isKindOfClass:[NewsfeedAlert class]]) {
         /*   NewsfeedAlert *na = [self.socialAlertsArray objectAtIndex:indexPath.row];
            NSString *newsfeedType = [na getStringRepresentationOfNewsfeedType];
            
            if ([newsfeedType isEqualToString:@"locker"]) {
                return 92;
            } else {
                return 60;
            }*/
            CGFloat testNum = [self calculateHeightForNewsfeedAlert:indexPath.row];
            if (testNum < 60)
                return 60;
            else if (testNum > 107) // Height for two lines of message, more than two lines will be showed as "..."
                return 107;
            else
                return testNum;
        }
    } else if (indexPath.section == SECTION_MATCH_ALERTS && [self.matchAlertsArray count] > 0) {
        return 60;
    } else if (indexPath.section == SECTION_UPCOMING_MATCHES && self.numUpcomingMatches > 0) {
        return 30;
    }
    return 0;
}

#define CELL_NEWSFEED_ALERT_CELL_CONTENT_WIDTH_LONG 272
#define CELL_NEWSFEED_ALERT_CELL_CONTENT_WIDTH_SHORT 227
#define CELL_NEWSFEED_ALERT_CELL_CONTENT_MARGIN_SHORT 93
#define CELL_NEWSFEED_ALERT_FONT_SIZE 13

- (CGFloat)calculateHeightForNewsfeedAlert:(NSInteger)row
{
    if ([[self.socialAlertsArray objectAtIndex:row] isKindOfClass:[NewsfeedAlert class]]) {
        NewsfeedAlert *na = [self.socialAlertsArray objectAtIndex:row];
        NSString *tempString = @"";
        NSString *type = [na getStringRepresentationOfNewsfeedType];
        NSInteger cellWidth = 0;
        
        if (![type isEqualToString:@"locker"]) { // shorter uilabel
            if (na.alertType == NEWSFEED_PROP_ALERT) {
                if ([type isEqualToString:@"individual"] || [type isEqualToString:@"pickup"])
                    tempString = [NSString stringWithFormat:@"%@ gave you props for your status for match %@", na.refPlayer.userName, na.supplementaryName];
                else if ([type isEqualToString:@"venue_checkin"])
                    tempString = [NSString stringWithFormat:@"%@ gave you props for checking into %@", na.refPlayer.userName, na.supplementaryName];
                else if ([type isEqualToString:@"venue_king"]) {
                    tempString = [NSString stringWithFormat:@"%@ gave you props for becoming king of %@", na.refPlayer.userName, na.supplementaryName];
                }
            } else if (na.alertType == NEWSFEED_COMMENT_ALERT) {
                tempString = [NSString stringWithFormat:@"%@ made a comment: \"%@\"", na.refPlayer.userName, na.message];
            }
            cellWidth = CELL_NEWSFEED_ALERT_CELL_CONTENT_WIDTH_SHORT;
            
        } else { // longer uilabel
            if (na.alertType == NEWSFEED_PROP_ALERT)
                tempString = [NSString stringWithFormat:@"%@ gave you props for your photo", na.refPlayer.userName];
            else
                tempString = [NSString stringWithFormat:@"%@ made a comment: \"%@\"", na.refPlayer.userName, na.message];
            
            cellWidth = CELL_NEWSFEED_ALERT_CELL_CONTENT_WIDTH_LONG;
        }
        
        CGSize constraint = CGSizeMake(cellWidth, 1000.0f);
        
        
        NSMutableDictionary *attributes = [NSMutableDictionary new];
        [attributes setObject:[UIFont systemFontOfSize:CELL_NEWSFEED_ALERT_FONT_SIZE] forKey:NSFontAttributeName];
        
        CGRect sizeRect = [tempString boundingRectWithSize:constraint options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
        
        CGSize size = sizeRect.size;
        
        if(cellWidth == CELL_NEWSFEED_ALERT_CELL_CONTENT_WIDTH_SHORT)
            return size.height + 25;
        else if (cellWidth == CELL_NEWSFEED_ALERT_CELL_CONTENT_WIDTH_LONG)
            return size.height + 80;
        
        
    }
    
    return 0;
}

#define CELL_CONTENT_WIDTH 179
#define CELL_CONTENT_MARGIN 22.5
#define FONT_SIZE 13 // is actually 13

- (CGFloat)calculateHeightForTextPost:(NSUInteger)row
{
    PlayerAlert *vci = [self.socialAlertsArray objectAtIndex:row];
    NSString *text = vci.message;
    
    CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH, 1000.0f);
    
    /********** because sizeWithFont deprecated *************/
    
    NSMutableDictionary *attributes = [NSMutableDictionary new];
    [attributes setObject:[UIFont systemFontOfSize:FONT_SIZE] forKey:NSFontAttributeName];
    
    CGRect sizeRect = [text boundingRectWithSize:constraint options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
    
    CGSize size = sizeRect.size;
    
    /********* end because sizeWithFont deprecated *********/
    
    return size.height;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == SECTION_UPCOMING_MATCHES && self.numUpcomingMatches > 0) {
        ShowAlertsUpcomingMatchesCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Upcoming Cell" forIndexPath:indexPath];
        
        if (self.numUpcomingMatches == 1)
            cell.upcomingMatchesLabel.text = [NSString stringWithFormat:@"You have %ld upcoming match", self.numUpcomingMatches];
        else
            cell.upcomingMatchesLabel.text = [NSString stringWithFormat:@"You have %ld upcoming matches", self.numUpcomingMatches];
        
        return cell;
        
    } else if ((indexPath.section == SECTION_SOCIAL_ALERTS && indexPath.row < [self.socialAlertsArray count]) ||
        (indexPath.section == SECTION_MATCH_ALERTS && indexPath.row < [self.socialAlertsArray count] && [self.matchAlertsArray count] == 0)) {
        
        if ([[self.socialAlertsArray objectAtIndex:indexPath.row] isKindOfClass:[PlayerAlertFollow class]]) {
            ShowAlertsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Alert Cell" forIndexPath:indexPath];
    
            PlayerAlert *pa = [self.socialAlertsArray objectAtIndex:indexPath.row];
            pa.refPlayer.userName = [pa.refPlayer.userName isKindOfClass:[NSNull class]]?@"unknown":pa.refPlayer.userName;
            NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ %@", pa.refPlayer.userName, pa.message]];
            [string addAttribute:NSForegroundColorAttributeName value:[UIColor goldColor] range:NSMakeRange(0, [pa.refPlayer.userName length])];
            cell.updateLabel.attributedText = string;
    
            NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                    baseString:pa.refPlayer.profileBaseString];
    
            [cell.profilePic setImageWithURL:[NSURL URLWithString:url]
                            placeholderImage:[UIImage imageNamed:@"avatar square"]];
            [UIImage makeRoundedImageNoBorder:cell.profilePic withRadius:5];
    
            // profile pic button
            UIButton *picButton1 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
            [picButton1 addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
            picButton1.tag = pa.refPlayer.userID;
            [cell.profilePic addSubview:picButton1];
            cell.profilePic.userInteractionEnabled = YES;
            // end profile pic button
    
            [cell.followButton makeRoundedBorderWithRadius:3];
            cell.followButton.tag = indexPath.row;

            // alertType 2 is request to follow
            if (pa.alertType == 2 && pa.refPlayer.approvalRequiredToFollow) {
                cell.followButton.hidden = NO;
                cell.followButton.enabled = YES;
                cell.followButton.backgroundColor = [UIColor blackColor];
                [cell.followButton setTitle:@"Approve" forState:UIControlStateNormal];
        
            } else if (pa.alertType == 2) {//  && pa.refPlayer.followingStatus == APPROVED_FOLLOWER_STATUS
                cell.followButton.hidden = NO;
                cell.followButton.enabled = NO;
                cell.followButton.backgroundColor = [UIColor lightGrayColor];
                [cell.followButton setTitle:@"Approved" forState:UIControlStateNormal];
            } else if (pa.refPlayer.followingStatus == IS_FOLLOWING_STATUS) {
                cell.followButton.hidden = NO;
                cell.followButton.enabled = YES;
                cell.followButton.backgroundColor = [UIColor blackColor];
                [cell.followButton setTitle:@"UnFollow" forState:UIControlStateNormal];
            } else if (pa.refPlayer.followingStatus == NOT_FOLLOWING_STATUS) {
                cell.followButton.hidden = NO;
                cell.followButton.enabled = YES;
                cell.followButton.backgroundColor = [UIColor blackColor];
                [cell.followButton setTitle:@"Follow" forState:UIControlStateNormal];
            } else if (pa.refPlayer.followingStatus == PENDING_FOLLOWING_STATUS) {
                cell.followButton.hidden = NO;
                cell.followButton.enabled = NO;
                cell.followButton.backgroundColor = [UIColor lightGrayColor];
                [cell.followButton setTitle:@"Pending" forState:UIControlStateNormal];
            } else {
                cell.followButton.hidden = YES;
            }
    
            return cell;
        
        
        } else if ([[self.socialAlertsArray objectAtIndex:indexPath.row] isKindOfClass:[PlayerAlertTag class]]) {
            ShowAlertsTagCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Tag Cell" forIndexPath:indexPath];
            PlayerAlertTag *pa = [self.socialAlertsArray objectAtIndex:indexPath.row];
        
            [cell myInit];
            cell.pa = pa;
            [cell.imageCollectionView reloadData];
        
            NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@", pa.refPlayer.userName, pa.message ]];
            
            [string addAttribute:NSForegroundColorAttributeName value:[UIColor goldColor] range:NSMakeRange(0, [pa.refPlayer.userName length])];
            cell.alertLabel.attributedText = string;
            
            NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                    baseString:pa.refPlayer.profileBaseString];
            [cell.profilePic setImageWithURL:[NSURL URLWithString:url]
                            placeholderImage:[UIImage imageNamed:@"avatar square"]];
            [UIImage makeRoundedImageNoBorder:cell.profilePic withRadius:5];
            
            // profile pic button
            UIButton *picButton1 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
            [picButton1 addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
            picButton1.tag = pa.refPlayer.userID;
            [cell.profilePic addSubview:picButton1];
            cell.profilePic.userInteractionEnabled = YES;
            
            return cell;
            
        }  else if ([[self.socialAlertsArray objectAtIndex:indexPath.row] isKindOfClass:[NewsfeedAlert class]]) {
         
            NewsfeedAlert *na = [self.socialAlertsArray objectAtIndex:indexPath.row];
            NSString *type = [na getStringRepresentationOfNewsfeedType];
            
            if ([type isEqualToString:@"individual"] || [type isEqualToString:@"pickup"] || [type isEqualToString:@"venue_checkin"] || [type isEqualToString:@"venue_king"]) {
                ShowAlertsNewsfeedType1Cell *cell = [tableView dequeueReusableCellWithIdentifier:@"Newsfeed Type 1 Cell" forIndexPath:indexPath];
                
                NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                        baseString:na.refPlayer.profileBaseString];
                
                [cell.profilePic setImageWithURL:[NSURL URLWithString:url]
                                placeholderImage:[UIImage imageNamed:@"avatar square"]];
                [UIImage makeRoundedImageNoBorder:cell.profilePic withRadius:5];
                
                // profile pic button
                UIButton *picButton1 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
                [picButton1 addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
                picButton1.tag = na.refPlayer.userID;
                [cell.profilePic addSubview:picButton1];
                cell.profilePic.userInteractionEnabled = YES;
                // end profile pic button
                
                NSString *text;
                if (na.alertType == NEWSFEED_PROP_ALERT) {
                    if ([type isEqualToString:@"individual"] || [type isEqualToString:@"pickup"])
                        text = [NSString stringWithFormat:@"%@ gave you props for your status for match %@", na.refPlayer.userName, na.supplementaryName];
                    else if ([type isEqualToString:@"venue_checkin"])
                        text = [NSString stringWithFormat:@"%@ gave you props for checking into %@", na.refPlayer.userName, na.supplementaryName];
                    else if ([type isEqualToString:@"venue_king"]) {
                        text = [NSString stringWithFormat:@"%@ gave you props for becoming king of %@", na.refPlayer.userName, na.supplementaryName];
                    }
                } else if (na.alertType == NEWSFEED_COMMENT_ALERT) {
                    text = [NSString stringWithFormat:@"%@ made a comment: \"%@\"", na.refPlayer.userName, na.message];
                }
                NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:text];
                [string addAttribute:NSForegroundColorAttributeName value:[UIColor goldColor] range:NSMakeRange(0, [na.refPlayer.userName length])];
                cell.alertLabel.attributedText = string;
                return cell;
                
            } else if ([type isEqualToString:@"locker"]) {
                ShowAlertsNewsfeedType2Cell *cell = [tableView dequeueReusableCellWithIdentifier:@"Newsfeed Type 2 Cell" forIndexPath:indexPath];
                
                NewsfeedAlert *na = [self.socialAlertsArray objectAtIndex:indexPath.row];
                
                NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                        baseString:na.refPlayer.profileBaseString];
                [cell.profilePic setImageWithURL:[NSURL URLWithString:url]
                                placeholderImage:[UIImage imageNamed:@"avatar square"]];
                [UIImage makeRoundedImageNoBorder:cell.profilePic withRadius:5];
                
                // profile pic button
                UIButton *picButton1 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
                [picButton1 addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
                picButton1.tag = na.refPlayer.userID;
                [cell.profilePic addSubview:picButton1];
                cell.profilePic.userInteractionEnabled = YES;
                
                NSString *text;
                if (na.alertType == NEWSFEED_PROP_ALERT)
                    text = [NSString stringWithFormat:@"%@ gave you props for your photo", na.refPlayer.userName];
                else
                    text = [NSString stringWithFormat:@"%@ made a comment: \"%@\"", na.refPlayer.userName, na.message];
                NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:text];
                [string addAttribute:NSForegroundColorAttributeName value:[UIColor goldColor] range:NSMakeRange(0, [na.refPlayer.userName length])];
                cell.alertLabel.attributedText = string;
                
                NSString *urlPostPic = [S3Tools2 getFileNameStringWithType:@"photo_regsize" baseString:[NSString stringWithFormat:@"%ld/photo/%@", (long)[self.preferences getUserID], na.supplementaryName]];
                
                cell.imagePic.contentMode = UIViewContentModeScaleAspectFill;
                cell.imagePic.clipsToBounds = YES;
                
                [cell.imagePic setImageWithURL:[NSURL URLWithString:urlPostPic] placeholderImage:[UIImage imageNamed:@"Landscape Placeholder"]];
                
                return cell;
            } else {
                ShowAlertsNewsfeedType1Cell *cell = [tableView dequeueReusableCellWithIdentifier:@"Newsfeed Type 1 Cell" forIndexPath:indexPath];
                
                NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                        baseString:na.refPlayer.profileBaseString];
                
                [cell.profilePic setImageWithURL:[NSURL URLWithString:url]
                                placeholderImage:[UIImage imageNamed:@"avatar square"]];
                [UIImage makeRoundedImageNoBorder:cell.profilePic withRadius:5];
                
                // profile pic button
                UIButton *picButton1 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
                [picButton1 addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
                picButton1.tag = na.refPlayer.userID;
                [cell.profilePic addSubview:picButton1];
                cell.profilePic.userInteractionEnabled = YES;
                // end profile pic button
                
                NSString *text;
                if (na.alertType == NEWSFEED_PROP_ALERT) {
                    text = [NSString stringWithFormat:@"%@ gave you props for you", na.refPlayer.userName];
                } else if (na.alertType == NEWSFEED_COMMENT_ALERT) {
                    text = [NSString stringWithFormat:@"%@ made a comment: \"%@\"", na.refPlayer.userName, na.message];
                }
                NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:text];
                [string addAttribute:NSForegroundColorAttributeName value:[UIColor goldColor] range:NSMakeRange(0, [na.refPlayer.userName length])];
                cell.alertLabel.attributedText = string;
                return cell;
            }
        } else if ([[self.socialAlertsArray objectAtIndex:indexPath.row] isKindOfClass:[VoteAlert class]]) { // it is vote class
            VoteAlert *alert = (VoteAlert *)[self.socialAlertsArray objectAtIndex:indexPath.row];
            if (alert.alertType == VOTE_THANKS_ALERT) {
                ThanksTVCell * cell = [tableView dequeueReusableCellWithIdentifier:@"thanks_cell"];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
                NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:alert.message];
                [string addAttribute:NSForegroundColorAttributeName value:[UIColor goldColor] range:NSMakeRange(0, [alert.refPlayer.userName length])];
                cell.message.attributedText = string;
                
                NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                        baseString:alert.refPlayer.profileBaseString];
                [cell.profilePic setImageWithURL:[NSURL URLWithString:url]
                                placeholderImage:[UIImage imageNamed:@"avatar square"]];
                [UIImage makeRoundedImageNoBorder:cell.profilePic withRadius:5];
                
                // profile pic button
                UIButton *picButton1 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
                [picButton1 addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
                picButton1.tag = alert.refPlayer.userID;
                [cell.profilePic addSubview:picButton1];
                cell.profilePic.userInteractionEnabled = YES;
                
                return cell;
            } else {
                GetVoteTVC * cell = [tableView dequeueReusableCellWithIdentifier:@"vote_alert"];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
                NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ %@",alert.refPlayer.userName,alert.message]];
                [string addAttribute:NSForegroundColorAttributeName value:[UIColor goldColor] range:NSMakeRange(0, [alert.refPlayer.userName length])];
                cell.message.attributedText = string;
                
                NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                        baseString:alert.refPlayer.profileBaseString];
                [cell.profilePic setImageWithURL:[NSURL URLWithString:url]
                                placeholderImage:[UIImage imageNamed:@"avatar square"]];
                [UIImage makeRoundedImageNoBorder:cell.profilePic withRadius:5];
                
                // profile pic button
                UIButton *picButton1 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
                [picButton1 addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
                picButton1.tag = alert.refPlayer.userID;
                [cell.profilePic addSubview:picButton1];
                cell.profilePic.userInteractionEnabled = YES;
                
                if (alert.sent == 1) {
                    [cell.Button setTitle:@"Thanks Sent" forState:UIControlStateNormal];
                    [cell.Button setBackgroundColor:[UIColor lightGrayColor]];
                    [cell.Button setEnabled:NO];
                } else {
                    [cell.Button setTitle:@"Send Thanks" forState:UIControlStateNormal];
                    [cell.Button setBackgroundColor:[UIColor goldColor]];
                    [cell.Button setEnabled:YES];
                }
                [cell.Button makeRoundedBorderWithRadius:5];
                cell.Button.tag = alert.alertID;
                cell.Button.restorationIdentifier = [NSString stringWithFormat:@"%d",alert.refPlayer.userID];
                [cell.Button addTarget:self action:@selector(sendThanks:) forControlEvents:UIControlEventTouchUpInside];
                if (!alert.wasViewed) { // If this are any unread alert for get vote, when user click profile page, it will refresh automatically.
                    [self.preferences setProfilePageRefreshState:YES];
                }
                
                return cell;
            }
        }
    } else if (indexPath.section == SECTION_MATCH_ALERTS && indexPath.row < [self.matchAlertsArray count]) {
        ShowAlertsMatchCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Match Cell" forIndexPath:indexPath];
        
        Match *alertMatchObject = [self.matchAlertsArray objectAtIndex:indexPath.row];
        
        cell.headlineMessage.hidden = NO;
        cell.description.hidden = NO;
        cell.notePic.hidden = NO;
        cell.sportLabel.hidden = NO;
        
        
        if ([alertMatchObject isKindOfClass:[IndividualMatch class]]) {
            
            
            IndividualMatch *iMatch = (IndividualMatch *)alertMatchObject;
            NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                    baseString:[iMatch getChallengerPlayerObject:[self.preferences getUserID]].profileBaseString];
            
            [cell.notePic setImageWithURL:[NSURL URLWithString:url]
                         placeholderImage:[UIImage imageNamed:@"avatar round"]];
            [UIImage makeRoundedImage:cell.notePic withRadius:20];
            cell.headlineMessage.text = [NSString stringWithFormat:@"%@ %@ Challenge", [iMatch getOtherPlayer:[self.preferences getUserID]].userName, [iMatch.sport.sportName capitalizedString]];

          //  cell.sportLabel.text = [NSString stringWithFormat:@"%@ Challenge Match", [iMatch.sport.sportName capitalizedString]];
            
        } else {
            // must be a pickup match
            cell.description.textColor = [UIColor lightGreenColor];
            PickupMatch *pMatch = (PickupMatch *)alertMatchObject;
            
            cell.notePic.tintColor = [UIColor goldColor];
            cell.notePic.image = [[pMatch.sport getSportIcon] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            cell.headlineMessage.text = [NSString stringWithFormat:@"%@ %@ Pickup Match", pMatch.matchCreatorUsername, [pMatch.sport.sportName capitalizedString]];
            
          //  cell.sportLabel.text = [NSString stringWithFormat:@"%@ Pickup Match", [pMatch.sport.sportName capitalizedString]];
            
            
        }
        
        [cell.goButton makeRoundedBorderWithRadius:3];
        cell.goButton.tag = indexPath.row;
        
        cell.description.text = alertMatchObject.matchNotification;
        
        return cell;
    }
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Temp Cell" forIndexPath:indexPath];
    return cell;
}

- (void)goToProfile:(UIButton *)sender
{
    self.playerIDToSegue = sender.tag;
    [self performSegueWithIdentifier:@"Player Segue" sender:self];
}

- (void)sendThanks:(UIButton *)sender
{
    self.alertID = sender.tag;
    self.targetID = [sender.restorationIdentifier integerValue];
    [self performSegueWithIdentifier:@"send_thanks_segue" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Player Segue"]) {
        PlayerDetailTVC *controller = segue.destinationViewController;
        controller.playerUserID = self.playerIDToSegue;
    } else if ([segue.identifier isEqualToString:@"Present Pic Segue"]) {
        LockerPhotoDetailTVC *controller = segue.destinationViewController;
//        controller.lockerID = self.selectedLockerPic.lockerID;
        controller.lockerID = self.selectedLockerPicID;
        [self.preferences setNewsfeedRefreshState:NO];
        /*controller.imageName = self.selectedLockerPic.imageFileName;
        controller.picCreatorID = self.selectedLockerPic.creator.userID; // need
        controller.picSeason = self.selectedLockerPic.picSeason; // need
        controller.picYear = self.selectedLockerPic.picYear; // need
        controller.picSport = self.selectedLockerPic.sp; // need */
    } else if ([segue.identifier isEqualToString:@"Individual Match Segue"]) {
        IndividualMatchDetailTVC *controller = segue.destinationViewController;
        controller.matchID = self.selectedMatchID;
        
    } else if ([segue.identifier isEqualToString:@"Pickup Match Segue"]) {
        PickupMatchDetailTVC *controller = segue.destinationViewController;
        controller.matchID = self.selectedMatchID;
    } else if ([segue.identifier isEqualToString:@"Record Score For Match Segue"]) {
        RecordIndMatchScoreIntroMessage *controller = segue.destinationViewController;
        controller.matchID = self.selectedMatchID;
        
    } else if ([segue.identifier isEqualToString:@"Confirm Score For Match Segue"]) {
        ConfirmIndMatchScoreIntroMessage *controller = segue.destinationViewController;
        controller.matchID = self.selectedMatchID;
    } else if ([segue.identifier isEqualToString:@"Force Push Record Score For Match Segue"]) {
        RecordIndMatchScoreIntroMessage *controller = segue.destinationViewController;
        controller.matchID = [self.preferences getMatchNotificationsForeSegueMatchID];
        [self.preferences setMatchNotificationsForceSegueWithAction:@"" andMatchID:0]; // reset
    } else if ([segue.identifier isEqualToString:@"Force Push Confirm Score For Match Segue"]) {
        ConfirmIndMatchScoreIntroMessage *controller = segue.destinationViewController;
        controller.matchID = [self.preferences getMatchNotificationsForeSegueMatchID];
        [self.preferences setMatchNotificationsForceSegueWithAction:@"" andMatchID:0]; // reset
    } else if ([segue.identifier isEqualToString:@"Force Push Individual Match Detail Segue"]) {
        IndividualMatchDetailTVC *controller = segue.destinationViewController;
        controller.matchID = [self.preferences getMatchNotificationsForeSegueMatchID];
    } else if ([segue.identifier isEqualToString:@"Force Push Pickup Match Detail Segue"]) {
        PickupMatchDetailTVC *controller = segue.destinationViewController;
        controller.matchID = [self.preferences getMatchNotificationsForeSegueMatchID];
      
    } else if ([segue.identifier isEqualToString:@"Force Push Match Result Level Segue"]) {
        ConfirmIndMatchScoreSuccessVC *controller = segue.destinationViewController;
        controller.matchID = [self.preferences getMatchNotificationsForeSegueMatchID];
    } else if ([segue.identifier isEqualToString:@"Force Push Venue Page Details Segue"]) {
        VenueDetailTVC *controller = segue.destinationViewController;
        controller.venueID = [self.preferences getMatchNotificationsForeSegueVenueID];
    } else if ([segue.identifier isEqualToString:@"Match List Segue"]) {
        MatchListTVC *controller = segue.destinationViewController;
        controller.loadMatchType = LOAD_UPCOMING_MATCHES;
    } else if ([segue.identifier isEqualToString:@"Newsfeed Detail Segue"]) {
        NewsFeedTVC *controller = segue.destinationViewController;
        controller.newsfeedDetailID = self.selectedNewsfeedID;
    } else if ([segue.identifier isEqualToString:@"send_thanks_segue"]) {
        sendThanksDialog *controller = segue.destinationViewController;
        controller.targetID = self.targetID;
        controller.alertID = self.alertID;
    }
}

- (IBAction)goToMatchAlert:(UIButton *)sender
{ 
    Match *iMatch = [self.matchAlertsArray objectAtIndex:sender.tag];
    self.selectedMatchID = iMatch.matchID;
    /*
    if ([m isKindOfClass:[IndividualMatch class]]) {
        [self performSegueWithIdentifier:@"Individual Match Segue" sender:self];
    } else if ([m isKindOfClass:[PickupMatch class]]) {
        [self performSegueWithIdentifier:@"Pickup Match Segue" sender:self];
    }*/
    
    if ([iMatch.matchNotification isEqualToString:@"Record your match score"] &&
        [iMatch isKindOfClass:[IndividualMatch class]]) {
        [self performSegueWithIdentifier:@"Record Score For Match Segue" sender:self];
    } else if ([iMatch.matchNotification isEqualToString:@"Confirm your match score"] &&
               [iMatch isKindOfClass:[IndividualMatch class]]){
        [self performSegueWithIdentifier:@"Confirm Score For Match Segue" sender:self];
    } else if ([iMatch.matchNotification isEqualToString:@"Accept or Decline Challenge"] &&
               [iMatch isKindOfClass:[IndividualMatch class]]) {
        [self performSegueWithIdentifier:@"Individual Match Segue" sender:self];
    } else if ([iMatch isKindOfClass:[PickupMatch class]]) {
        [self performSegueWithIdentifier:@"Pickup Match Segue" sender:self];
    }
}



- (IBAction)followAction:(UIButton *)sender
{
    PlayerAlert *pa = [self.socialAlertsArray objectAtIndex:sender.tag];
    
    sender.enabled = NO;
    
    if (pa.alertType == 2 && pa.refPlayer.approvalRequiredToFollow) {
        [IOSRequest approveFollowConnection:[self.preferences getUserID]
                                 withLeader:[self.preferences getUserID]
                                andFollower:pa.refPlayer.userID
                               onCompletion:^(NSDictionary *results) {
                                   
                                   dispatch_async(dispatch_get_main_queue(), ^{
                                       sender.enabled = YES;
                                       if (![results[@"outcome"] isEqualToString:@"failure"]) {
                                           pa.refPlayer.followingStatus = APPROVED_FOLLOWER_STATUS;
                                           pa.refPlayer.approvalRequiredToFollow = NO;
                                           [self.socialAlertsArray replaceObjectAtIndex:sender.tag withObject:pa];
                                           [self.tableView reloadData];
                                       }
                                   });
                               }];
    } else if (pa.alertType == 2) {

    
    } else if (pa.refPlayer.followingStatus == IS_FOLLOWING_STATUS) {
        [IOSRequest removeFollowConnectionWithLeader:pa.refPlayer.userID
                                         andFollower:[self.preferences getUserID]
                                        onCompletion:^(NSDictionary *results) {
                                            
                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                sender.enabled = YES;
                                                
                                                if (![results[@"outcome"] isEqualToString:@"failure"]) {
                                                    [self.preferences setNewsfeedRefreshState:YES];
                                                    pa.refPlayer.followingStatus = NOT_FOLLOWING_STATUS;
                                                    [self.socialAlertsArray replaceObjectAtIndex:sender.tag withObject:pa];
                                                    [self.tableView reloadData];

                                                } 

                                            });
                                        }];
    } else if (pa.refPlayer.followingStatus == NOT_FOLLOWING_STATUS) {
        [IOSRequest makeFollowConnectionWithLeader:pa.refPlayer.userID
                                       andFollower:[self.preferences getUserID]
                                      onCompletion:^(NSDictionary *results) {
                                          
                                          dispatch_async(dispatch_get_main_queue(), ^ {
                                              sender.enabled = YES;
                                              
                                              if (![results[@"outcome"] isEqualToString:@"failure"]) {
                                                  
                                                  [self.preferences setNewsfeedRefreshState: YES];
                                                  pa.refPlayer.followingStatus = [results[@"follow_status"] integerValue];
                                                  [self.socialAlertsArray replaceObjectAtIndex:sender.tag withObject:pa];
                                                  [self.tableView reloadData];

                                              }
                                              

                                          });
                                      }];
        
        
    }
    
}


- (IBAction)chooseTaggedPicAction:(UIButton *)sender
{
    self.selectedLockerPicID = sender.tag;
    [self performSegueWithIdentifier:@"Present Pic Segue" sender:self];

}

- (void)setAlertNumInTagWithValue:(NSInteger)val
{
    val = val + [self.matchAlertsArray count];  // always want to display match alerts if there are any. the social alerts disappear once viewed.
    if (val) {
        /*for (UIView *tabBarButton in self.tabBarController.tabBar.subviews) {
            for (UIView *badgeView in tabBarButton.subviews) {
                NSString *className = NSStringFromClass([badgeView class]);
                
                if ([className isEqualToString:@"UITabBarButtonBadge"] || [className isEqualToString:@"_UIBadgeView"]) {
                    CGRect rec = CGRectMake(badgeView.frame.origin.x-19, badgeView.frame.origin.y+5, badgeView.frame.size.width, badgeView.frame.size.height);
                    [badgeView setFrame:rec];
                    NSLog(@"a badge in %f, %f",badgeView.frame.origin.x, badgeView.frame.origin.y);
                }
            }
        }*/
        [[self.navigationController tabBarItem] setBadgeValue:[NSString stringWithFormat:@"%lu", val]];
        [UIApplication sharedApplication].applicationIconBadgeNumber = val;
        
        
    } else {
        [[self.navigationController tabBarItem] setBadgeValue:nil];
        [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    }

}

- (void) refreshMatchNotificationsPageFromNSNotification:(NSNotification*)notification
{
    //    [[NSNotificationCenter defaultCenter]
    //     removeObserver:self
    //     name:@"refreshMatchNotificationsPageNotification"
    //     object:nil];
    self.tabBarController.selectedIndex = 1;
    
    [IOSRequest fetchNumUnreadAlertsForUser:[self.preferences getUserID]
                               onCompletion:^(NSDictionary *results) {
                                   
                                   dispatch_async(dispatch_get_main_queue(), ^{
                                       [self setAlertNumInTagWithValue:[results[@"outcome"] integerValue]];
                                   });
                               }];


    
    if ([[self.preferences getMatchNotificationsForceSegueAction] isEqualToString:@"IR"]) {
        [self.navigationController popToRootViewControllerAnimated:NO];
        [self performSegueWithIdentifier:@"Force Push Record Score For Match Segue" sender:self];
    } else if ([[self.preferences getMatchNotificationsForceSegueAction] isEqualToString:@"IC"]) {
        [self.navigationController popToRootViewControllerAnimated:NO];
        [self performSegueWithIdentifier:@"Force Push Confirm Score For Match Segue" sender:self];
    } else if ([[self.preferences getMatchNotificationsForceSegueAction] isEqualToString:@"ID"]) {
        [self.navigationController popToRootViewControllerAnimated:NO];
        [self performSegueWithIdentifier:@"Force Push Individual Match Detail Segue" sender:self];
    } else if ([[self.preferences getMatchNotificationsForceSegueAction] isEqualToString:@"PD"]) {
        [self.navigationController popToRootViewControllerAnimated:NO];
        [self performSegueWithIdentifier:@"Force Push Pickup Match Detail Segue" sender:self];
    } else if ([[self.preferences getMatchNotificationsForceSegueAction] isEqualToString:@"IH"]) {
        [self.navigationController popToRootViewControllerAnimated:NO];
        [self performSegueWithIdentifier:@"Force Push Individual Match Detail Segue" sender:self];
    } else if ([[self.preferences getMatchNotificationsForceSegueAction] isEqualToString:@"PH"]) {
        [self.navigationController popToRootViewControllerAnimated:NO];
        [self performSegueWithIdentifier:@"Force Push Pickup Match Detail Segue" sender:self];
        
    } else if ([[self.preferences getMatchNotificationsForceSegueAction] isEqualToString:@"IF"]) {
        [self.navigationController popToRootViewControllerAnimated:NO];
        [self performSegueWithIdentifier:@"Force Push Match Result Level Segue" sender:self];
    } else if ([[self.preferences getMatchNotificationsForceSegueAction] isEqualToString:@"IN"]) {
        [self.navigationController popToRootViewControllerAnimated:NO];
        [self performSegueWithIdentifier:@"Match Dispute Message Segue" sender:self];
    } else if ([[self.preferences getMatchNotificationsForceSegueAction] isEqualToString:@"TD"]) {
    //    [self.navigationController popToRootViewControllerAnimated:NO];
    //    [self performSegueWithIdentifier:@"Force Push Team Page Details Segue" sender:self];
    } //else if ([[self.preferences getMatchNotificationsForceSegueAction] isEqualToString:@"CD"]) {
     //   [self.navigationController popToRootViewControllerAnimated:NO];
     //   [self performSegueWithIdentifier:@"Venue Page Details Segue" sender:self];
        
        
//    }
    else if ([[self.preferences getMatchNotificationsForceSegueAction] isEqualToString:@"CD"]) {
        [self.navigationController popToRootViewControllerAnimated:NO];
        [self performSegueWithIdentifier:@"Force Push Venue Page Details Segue" sender:self];
        
        
    } /*else {
        [self refreshView];
    }*/
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == SECTION_UPCOMING_MATCHES) {
        [self performSegueWithIdentifier:@"Match List Segue" sender:self];
    } else if (indexPath.section == SECTION_SOCIAL_ALERTS && [[self.socialAlertsArray objectAtIndex:indexPath.row] isKindOfClass:[NewsfeedAlert class]] ) {
        NewsfeedAlert *na = [self.socialAlertsArray objectAtIndex:indexPath.row];
        self.selectedNewsfeedID = na.newsfeedID;
        [self performSegueWithIdentifier:@"Newsfeed Detail Segue" sender:self];
    } else if (indexPath.section == SECTION_SOCIAL_ALERTS && [[self.socialAlertsArray objectAtIndex:indexPath.row] isKindOfClass:[VoteAlert class]] ) {
        VoteAlert *alert = (VoteAlert *)[self.socialAlertsArray objectAtIndex:indexPath.row];
        if (alert.alertType == VOTE_THANKS_ALERT) {
            self.playerIDToSegue = alert.refPlayer.userID;
            [self performSegueWithIdentifier:@"Player Segue" sender:self];
        }
    }
}

@end
