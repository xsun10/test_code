//
//  AddPickupMatchTVC.m
//  Phenom
//
//  Created by James Chung on 4/3/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "AddPickupMatchTVC.h"
#import "PickupMatch.h"
#import "UserPreferences.h"
#import "ChooseMatchCompetitorTVC.h"
#import "ChooseMatchTypeTVC.h"
#import "UITextField+FormValidations.h"
#import "UITextView+FormValidations.h"

@interface AddPickupMatchTVC ()

@property (strong, nonatomic) PickupMatch *pickupMatch;
@property (strong, nonatomic) UserPreferences *preferences;

@property (weak, nonatomic) IBOutlet UITextField *matchNameTextField;
@property (weak, nonatomic) IBOutlet UITableViewCell *minCompetitorsCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *maxCompetitorsCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *dateTimeCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *venueCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *experienceCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *sportCell;
@property (weak, nonatomic) IBOutlet UITextView *messageTextBlock;
@property (weak, nonatomic) IBOutlet UITableViewCell *matchTypeCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *genderCell;

@property (nonatomic, strong) UIButton *closeDatePickerButton;
@property (nonatomic, strong) UIView *closeDatePickerView;

@property (nonatomic) UIDatePicker *datePicker;
@property (nonatomic) NSInteger datePickerHeight;
@property (nonatomic) BOOL datePickerIsShowing;
@property (nonatomic, strong) UIImageView *imageView;


@end

@implementation AddPickupMatchTVC

#define DATE_ROW 0 // also section 2?

- (UIDatePicker *)datePicker
{
    if (!_datePicker) _datePicker = [[UIDatePicker alloc] init];
    return _datePicker;
}


- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (PickupMatch *) pickupMatch
{
    if (!_pickupMatch) _pickupMatch = [[PickupMatch alloc] init];
    
    return _pickupMatch;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];

    
    self.datePicker.datePickerMode = UIDatePickerModeDateAndTime;
    self.datePicker.maximumDate = [NSDate dateWithTimeIntervalSinceNow:7776000];
    self.datePicker.minimumDate = [NSDate date];
    self.datePickerHeight = 44;
    self.datePickerIsShowing = NO;
    
    self.matchTypeCell.userInteractionEnabled = NO;
    self.matchTypeCell.textLabel.textColor = [UIColor lightGrayColor];
    self.matchTypeCell.detailTextLabel.textColor = [UIColor lightGrayColor];
    
    
    
/*
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
 
*/
    
    // single tap gesture recognizer
//    UITapGestureRecognizer *tapGestureRecognize = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissDate)];
 //   tapGestureRecognize.numberOfTapsRequired = 1;
 //   [self.datePicker addGestureRecognizer:tapGestureRecognize];
 //   [self showOverlay];

 //   [self setNextButton];
}

/*
- (void)setNextButton
{
    UIButton *btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 50, 21);
    btn.layer.borderColor = [UIColor whiteColor].CGColor;
    btn.layer.borderWidth = 1.0;
    
    [btn addTarget:self action:@selector(nextStep:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundColor:[UIColor clearColor]];
    [btn setTitle:@"NEXT" forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-MediumItalic" size:12.0]];
    
    // Initialize UIBarbuttonitem...
    UIBarButtonItem *proe = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = proe;
}

*/
- (void)showOverlay
{
    self.imageView = [[UIImageView alloc]
                      initWithImage:[UIImage imageNamed:@"Pickup Intro"]];
    
    
    UITapGestureRecognizer * recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    
    [self.imageView addGestureRecognizer:recognizer];
    self.imageView.userInteractionEnabled =  YES;
    
    [[[UIApplication sharedApplication] keyWindow] addSubview:self.imageView];
    
}

- (void) handleTap:(UITapGestureRecognizer *)recognize
{
    //  [self.imageView removeFromSuperView];
    self.imageView.hidden = YES;
}

/*
- (void)dismissDate
{
    [self.matchNameTextField resignFirstResponder];
    [self.messageTextBlock resignFirstResponder];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:2];
    UITableViewCell* cell = (UITableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    
    
    if (self.datePickerIsShowing) {
        [self performCloseCell:indexPath];
        self.datePickerIsShowing = NO;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
}*/


- (IBAction)clearMatchName:(id)sender
{
    self.matchNameTextField.text = @"";
    [self.matchNameTextField resignFirstResponder];
}

- (IBAction)privateToggle:(UISwitch *)sender
{
    self.pickupMatch.private = sender.on;
    // might want to check for some errors
}

- (IBAction)nextStep:(id)sender
{
    if ([self validateAllFields]) {
        self.pickupMatch.matchCreatorID = [self.preferences getUserID];
        self.pickupMatch.matchName = self.matchNameTextField.text;
        self.pickupMatch.message = self.messageTextBlock.text;
        [self performSegueWithIdentifier:@"Invite Players Segue" sender:self];
    }
}

- (BOOL)validateAllFields
{
    UIAlertView *alert = [[UIAlertView alloc] init];
    alert.title = @"Match Creation Error";
    
    BOOL validationTest = NO;
    
    if (![self.matchNameTextField checkMatchName]) {
        alert.message = @"Match Name must be less than 50 letters and contain only alphanumeric characters.";
        validationTest = NO;;
    } else if ([self.sportCell.detailTextLabel.text isEqualToString:@"Detail"]) {
        alert.message = @"Please choose a Sport";
        validationTest = NO;
    } else if ([self.matchTypeCell.detailTextLabel.text isEqualToString:@"Detail"]) {
        alert.message = @"Please choose a Match Type";
        validationTest = NO;
    } else if ([self.experienceCell.detailTextLabel.text isEqualToString:@"Detail"]) {
        alert.message = @"Please choose the Experience Level";
        validationTest = NO;
    } else if ([self.genderCell.detailTextLabel.text isEqualToString:@"Detail"]) {
        alert.message = @"Please choose a Gender";
        validationTest = NO;
    } else if ([self.dateTimeCell.detailTextLabel.text isEqualToString:@"Detail"]) {
        alert.message = @"Please choose a Match Date / Time";
        validationTest = NO;
    } else if ([self.venueCell.detailTextLabel.text isEqualToString:@"Detail"]) {
        alert.message = @"Please choose a Venue";
        validationTest = NO;
    } else if ([self.messageTextBlock checkMatchDescription]){
        alert.message = @"Please enter a valid match description";
        validationTest = NO;
    } else if ([self.messageTextBlock.text length] > 1000) {
       alert.message = @"Please enter a short match description";
        validationTest = NO;
    } else {
        validationTest = YES;
    }
    
    if (validationTest == NO) {
        [alert addButtonWithTitle:@"OK"];
        [alert show];
        return  NO;
    }
    
    return  YES;
}


// delegate method




- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"Min Players Segue"]){
        
       /* PlayerCount_AddPickupMatchTVC *controller = (PlayerCount_AddPickupMatchTVC *)segue.destinationViewController;
        
        controller.delegate = self;
        controller.chooseMaxOrMinPlayers = 0;
    } else if ([segue.identifier isEqualToString:@"Max Players Segue"]) {
        PlayerCount_AddPickupMatchTVC *controller = (PlayerCount_AddPickupMatchTVC *)segue.destinationViewController;
        controller.delegate = self;
        controller.chooseMaxOrMinPlayers = 1;
      */
    } else if ([segue.identifier isEqualToString:@"Add Date"]) {
      
//        DateTimePickerViewController *controller = (DateTimePickerViewController *)segue.destinationViewController;
//        controller.delegate = self;
        
    } else if ([segue.identifier isEqualToString:@"My Venue List"]) {
        VenuesTVC *controller = (VenuesTVC *)segue.destinationViewController;
        controller.delegate = self;
        controller.showChooseButton = YES;
        
    } else if ([segue.identifier isEqualToString:@"Match Experience"]) {
        RequiredMatchExperienceTVC *controller = (RequiredMatchExperienceTVC *)segue.destinationViewController;
        controller.delegate = self;
    } else if ([segue.identifier isEqualToString:@"Match Sport"]) {
        ChoosePickupSportTVC *controller = (ChoosePickupSportTVC *)segue.destinationViewController;
        controller.delegate = self;
    } else if ([segue.identifier isEqualToString:@"Invite Players Segue"]) {
        self.pickupMatch.matchCreatorID = [self.preferences getUserID];
        self.pickupMatch.matchName = self.matchNameTextField.text;
        self.pickupMatch.message = self.messageTextBlock.text;
        self.pickupMatch.gender = self.genderCell.detailTextLabel.text;
        // Right now doing this for individual competitors...
        ChooseMatchCompetitorTVC *controller = (ChooseMatchCompetitorTVC *)segue.destinationViewController;
        controller.pickupMatch = self.pickupMatch;
        controller.userID = [self.preferences getUserID];
    } else if ([segue.identifier isEqualToString:@"Match Type Segue"]) {
        ChooseMatchTypeTVC *controller = (ChooseMatchTypeTVC *)segue.destinationViewController;
        controller.sportID = self.pickupMatch.sport.sportID;
        controller.delegate = self;
    } else if ([segue.identifier isEqualToString:@"Choose Gender Segue"]) {
        ChooseGenderTVC *controller = (ChooseGenderTVC *)segue.destinationViewController;
        controller.delegate = self;
    }
}

// delegate method
/*
- (void)setWithViewController:(DateTimePickerViewController *)controller withDate:(NSDate *)date
{
    self.pickupMatch.dateTime = date;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM/dd/yyyy hh:mm a"];
 
    self.dateTimeCell.detailTextLabel.text = [formatter stringFromDate: date];
    
        
}*/

// venue delegate method
-(void)setVenueViewController:(VenuesTVC *)controller withVenue:(Venue *)venue
{
    self.pickupMatch.venue = venue;
    self.venueCell.detailTextLabel.text = venue.venueName;
}

// match experience delegate method
-(void)setMatchExperienceViewController:(RequiredMatchExperienceTVC *)controller withExperience:(NSMutableArray *)experience andCode:(NSString *)experienceCode
{
    self.pickupMatch.experienceArray = experience;
    self.pickupMatch.experienceCode = experienceCode;
    self.experienceCell.detailTextLabel.text = [self getFullExperienceString:experience];
    
}

- (void)setWithSportFromVC:(ChoosePickupSportTVC *)controller withSport:(Sport *)sport
{
    self.pickupMatch.sport = sport;
    self.sportCell.detailTextLabel.text = [sport.sportName capitalizedString];
    
    self.matchTypeCell.userInteractionEnabled = YES;
    self.matchTypeCell.textLabel.textColor = [UIColor blackColor];
    self.matchTypeCell.detailTextLabel.textColor = [UIColor colorWithRed:0.22 green:0.33 blue:0.53 alpha:1.0];
    
    // if chose a new sport, need to reset match type
    
    self.pickupMatch.matchType.description = @"";
    self.pickupMatch.matchType.matchTypeID = 0;
    self.matchTypeCell.detailTextLabel.text = @"Detail";
    
}

- (void)setWithMatchType:(ChooseMatchTypeTVC *)controller withMatchType:(MatchType *)mType
{
    self.matchTypeCell.detailTextLabel.text = mType.description;
    self.pickupMatch.matchType = mType;
}


- (NSString *)getFullExperienceString:(NSMutableArray *)experience
{
    NSString *resultString = @"";
    NSInteger counter = 0;
    
    NSLog (@"experience array count: %lu", [experience count]);
    
    for (NSString *object in experience) {
        
        if (counter == 0)
            resultString = object;
        else
            resultString = [resultString stringByAppendingFormat:@", %@", object];
        counter ++;
    }
    
    return resultString;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = (UITableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    
    [self.matchNameTextField resignFirstResponder];
    [self.messageTextBlock resignFirstResponder];
    
    if (indexPath.row == DATE_ROW && indexPath.section == 2 && !self.datePickerIsShowing) {
        
        
        cell.textLabel.text = @"";
        cell.detailTextLabel.text = @"";
        cell.accessoryType = UITableViewCellAccessoryNone;
        
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:2] atScrollPosition:UITableViewScrollPositionTop animated:YES];
        
        self.datePicker.alpha = 0;
        [self.tableView beginUpdates];
        self.datePickerHeight = 44 + 190;
        [self.tableView endUpdates];
        
        
        [cell addSubview:self.datePicker];
        [UIView animateWithDuration:0.2 animations:^{
            self.datePicker.alpha = 1;
            self.closeDatePickerView = [[UIView alloc] initWithFrame:CGRectMake(0, cell.frame.origin.y, 320, 40)];
            self.closeDatePickerView.backgroundColor = [UIColor darkGrayColor];
            self.closeDatePickerView.alpha = 0.5;
            
            self.closeDatePickerButton = [[UIButton alloc] initWithFrame:CGRectMake(260,  7, 50, 25)];
            [self.closeDatePickerButton setTitle:@"CLOSE" forState:UIControlStateNormal];
            [self.closeDatePickerButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-MediumItalic" size:11]];
            self.closeDatePickerButton.layer.borderColor = [UIColor lightGrayColor].CGColor;
            self.closeDatePickerButton.layer.borderWidth = 1.0;
            
            [self.closeDatePickerButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
            [self.closeDatePickerButton addTarget:self action:@selector(tapDate:) forControlEvents:UIControlEventTouchUpInside];
            
            [self.closeDatePickerView addSubview:self.closeDatePickerButton];
            [self.tableView addSubview:self.closeDatePickerView];

        } completion:nil];
        self.datePickerIsShowing = YES;
        
        
    } else if (indexPath.row == DATE_ROW && indexPath.section == 2 && self.datePickerIsShowing) {
  //      [self performCloseCell:indexPath];
  //      self.datePickerIsShowing = NO;
  //      cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
}


- (void)tapDate:(UIButton *)sender
{
    [self.matchNameTextField resignFirstResponder];
    [self.messageTextBlock resignFirstResponder];
 
    [UIView animateWithDuration:0.5 animations:^(void) {
        self.closeDatePickerView.alpha = 0.0;
     
        if (self.datePickerIsShowing) {
            [self performCloseCell:[NSIndexPath indexPathForRow:0 inSection:2]];
            self.datePickerIsShowing = NO;
        }
    }];
    
    
    
}



- (NSIndexPath *)tableView:(UITableView *)tableView willDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.closeDatePickerButton.hidden = YES;
    self.closeDatePickerView.hidden = YES;
    self.closeDatePickerView = nil;
    self.closeDatePickerButton = nil;
    

    [self performCloseCell:indexPath];
    self.datePickerIsShowing = NO;
    
    return indexPath;
}

- (void)performCloseCell:(NSIndexPath *)indexPath
{
    
    if (indexPath.row == DATE_ROW && indexPath.section == 2 && self.datePickerIsShowing) {
        
        [UIView animateWithDuration:0.2 animations:^{self.datePicker.alpha = 0;} completion:^(BOOL finished){[self.datePicker removeFromSuperview];}];
        UITableViewCell *cell = (UITableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
        NSDate *myDate = self.datePicker.date;
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MM/dd/yyyy hh:mm a"];
        cell.detailTextLabel.text = [dateFormat stringFromDate:myDate];
        cell.textLabel.text = @"Date";
        self.pickupMatch.dateTime = myDate;
        
        
        [self.tableView beginUpdates];
        
        if (indexPath.row == DATE_ROW)
            self.datePickerHeight = 44;
        
        [self.tableView endUpdates];
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row == DATE_ROW && indexPath.section == 2) {
        return self.datePickerHeight;
    }
    else if (indexPath.row == 0 && indexPath.section == 5) { // message text area
        return 170;
    }
    
    return 44;
}

- (void)setGenderWithController:(ChooseGenderTVC *)controller withGenderString:(NSString *)genderString
{
    self.genderCell.detailTextLabel.text = [genderString capitalizedString];
}

@end
