//
//  MySportLevelsCell.h
//  Vaiden
//
//  Created by James Chung on 8/13/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MySportLevelsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *sportPic;
@property (weak, nonatomic) IBOutlet UILabel *sportName;
@property (weak, nonatomic) IBOutlet UILabel *level;

@end
