//
//  LockerTagSingle.m
//  Vaiden
//
//  Created by James Chung on 5/7/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "LockerTagSingle.h"

@implementation LockerTagSingle

- (LockerTagSingle *) initMatchWithDetails:(NSInteger)lockerTagID
                                andTagType:(NSInteger)lockerTagType
                                andTagName:(NSString *)lockerTagName
                           andTagSelection:(NSString *)lockerTagSelection

{
    
    self = [super initWithTagDetails:lockerTagID
                        andTagType:lockerTagType
                        andTagName:lockerTagName];
    
    if (self) {
        _tagSelection = lockerTagSelection;
    }
    
    return self;
    
}


@end
