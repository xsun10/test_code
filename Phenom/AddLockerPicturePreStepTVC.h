//
//  AddLockerPicturePreStepTVC.h
//  Vaiden
//
//  Created by James Chung on 5/27/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "CustomBaseVC.h"
#import "VaidenImageObject.h"
#import "ChooseSeasonTVC.h"
#import "ChooseYearTVC.h"
#import "ChooseSportUtilityTVC.h"
#import "ChooseContactsUtilityTVC.h"
#import "ChooseLocationTVC.h"
#import "S3Tools2.h"


@interface AddLockerPicturePreStepVC : CustomBaseVC

@property (nonatomic, strong) VaidenImageObject *viObject;
@property (nonatomic, strong) S3Tools2 *regImageSizeUploader;
@property (nonatomic, strong) S3Tools2 *regImageRetinaSizeUploader;
@property (nonatomic, strong) S3Tools2 *thumbnailImageSizeUploader;
@property (nonatomic, strong) S3Tools2 *thumbnailImageRetinaSizeUploader;
@property (nonatomic, strong) NSString *imageFileName;
@property (nonatomic, strong) NSString *imageFileNameBaseString;

@end
