//
//  PlayerAlertTag.h
//  Vaiden
//
//  Created by James Chung on 5/16/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "PlayerAlert.h"
#import "LockerPic.h"

@interface PlayerAlertTag : PlayerAlert

//@property (nonatomic) NSInteger lockerID;
//@property (nonatomic, strong ) NSMutableArray *imageArray;
- (PlayerAlertTag *)initWithPlayerAlertTagDetails:(NSInteger)userID
                                      andUserName:(NSString *)userName
                              andProfilePicString:(NSString *)profilePicString
                                       andMessage:(NSString *)message
                                     andAlertType:(NSInteger)alertType
                                     andAlertDate:(NSDate *)alertDate
                                     andWasViewed:(BOOL)wasViewed
                                     withLockerID:(NSInteger)lockerID
                                     andImageName:(NSString *)imageName;

@property (nonatomic, strong) NSMutableArray *tagDataArray;

@end
