//
//  SportRating.h
//  ;

//
//  Created by James Chung on 10/7/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Player.h"
#import "SportSkillAttribute.h"

@interface SportRating : NSObject

@property (nonatomic) float rating;
@property (nonatomic, strong) Player *ratingPlayer;
@property (nonatomic) NSUInteger ratedPlayerID;
@property (nonatomic, strong) NSString *comment;
@property (nonatomic, strong) NSDate *ratingDate;
@property (nonatomic, strong) SportSkillAttribute *sportSkillAttribute;

-(SportRating *)initWithSportRating:(float)rating
                   byRatingPlayerID:(NSUInteger)ratingPlayerID
             byRatingPlayerUsername:(NSString *)ratingPlayerUsername
            byRatingPlayerPicString:(NSString *)ratingPlayerPicString
                   forRatedPlayerID:(NSUInteger)ratedPlayerID
                        withComment:(NSString *)comment
                             onDate:(NSDate *)ratingDate
             forSportSkillAttribute:(SportSkillAttribute *)sportSkillAttribute;

+ (UIImage *)getRatingImageForRating:(float)rating;

@end
