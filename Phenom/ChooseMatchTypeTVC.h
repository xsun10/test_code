//
//  ChooseMatchTypeTVC.h
//  Vaiden
//
//  Created by James Chung on 8/6/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MatchType.h"
#import "CustomBaseTVC.h"

@class ChooseMatchTypeTVC;

@protocol ChooseMatchTypeTVC_Delegate <NSObject>
- (void)setWithMatchType:(ChooseMatchTypeTVC *)controller withMatchType:(MatchType *)mType;

@end

@interface ChooseMatchTypeTVC : CustomBaseTVC

@property (nonatomic) NSInteger sportID;
@property (nonatomic, weak) id <ChooseMatchTypeTVC_Delegate> delegate;

@end


