//
//  VoteReceiving.m
//  Vaiden
//
//  Created by Turbo on 7/30/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "VoteReceiving.h"

@implementation VoteReceiving

- (VoteReceiving *)initWithNewsDetails:(NSInteger)newsMakerUserID
                           andUserName:(NSString *)newsMakerUserName
                         andProfileStr:(NSString *)profileBaseString
                              withDate:(NSDate *)newsDate
                           andPostType:(NSInteger)postType
                             andNewsID:(NSInteger)newsID
                           andTextPost:(NSString *)textPost
                           andTargetID:(NSInteger)targetID
                     andTargetUsername:(NSString *)targetName
                   andtargetProfileStr:(NSString *)targetProfileBaseString
                        andNumComments:(NSUInteger)numComments
                         andNumRemixes:(NSUInteger)numRemixes
                           andLockerID:(NSInteger)lockerID
                               andType:(NSString *)type

{
    self = [super initWithNewsDetails:newsMakerUserID
                          andUserName:newsMakerUserName
                        andProfileStr:profileBaseString
                             withDate:newsDate
                          andPostType:postType
                       andNumComments:numComments
                        andNumRemixes:numRemixes
                            andNewsID:newsID
                          andTextPost:textPost];
    
    if (self) {
        _targetID = targetID;
        _targetName = targetName;
        _targetProfileBaseString = targetProfileBaseString;
        _type = type;
    }
    return self;
}

- (VoteReceiving *)initWithNewsDetails:(NSInteger)newsMakerUserID
                           andUserName:(NSString *)newsMakerUserName
                         andProfileStr:(NSString *)profileBaseString
                              withDate:(NSDate *)newsDate
                           andPostType:(NSInteger)postType
                             andNewsID:(NSInteger)newsID
                           andTextPost:(NSString *)textPost
                        andNumComments:(NSUInteger)numComments
                         andNumRemixes:(NSUInteger)numRemixes
                             andwinner:(NSMutableArray *)winners
                               andType:(NSString *)type
                         andUserRegion:(NSString *)region
{
    self = [super initWithNewsDetails:newsMakerUserID
                          andUserName:newsMakerUserName
                        andProfileStr:profileBaseString
                             withDate:newsDate
                          andPostType:postType
                       andNumComments:numComments
                        andNumRemixes:numRemixes
                            andNewsID:newsID
                          andTextPost:textPost];
    
    if (self) {
        _type = type;
        _winners = winners;
        _region = region;
    }
    return self;
}


@end
