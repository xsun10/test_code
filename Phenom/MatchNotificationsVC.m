//
//  MatchNotificationsVC.m
//  Vaiden
//
//  Created by James Chung on 11/25/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "MatchNotificationsVC.h"
#import "IOSRequest.h"
#import "UserPreferences.h"
#import "PickupMatch.h"
#import "S3Tools.h"
#import "AFNetworking.h"
#import "UIImage+ProportionalFill.h"
#import "UrgentMatchNotificationCell.h"
#import "UpcomingPickupMatchNotificationCell.h"
#import "UpcomingIndividualMatchNotificationCell.h"
#import "UpdatesMatchNotificationCell.h"
#import "MatchNotificationsNewMessageCell.h"
#import <QuartzCore/QuartzCore.h>
#import "UIView+Manipulate.h"
#import "Sport.h"
#import "IndividualMatchDetailTVC.h"
#import "PickupMatchDetailTVC.h"
#import "RecordIndMatchScoreIntroMessage.h"
#import "ConfirmIndMatchScoreIntroMessage.h"
#import "UIColor+VaidenColors.h"
#import "MatchListTVC.h"
#import "MatchUpdateInfo.h"
#import "NSDate+Utilities.h"
#import "UIImage+ImageEffects.h"
#import "PhenomAppDelegate.h"
#import "NSDate+Utilities.h"
#import "NumPastMatchesCell.h"
#import "MyHelpers.h"
#import "MKNumberBadgeView.h"
#import "MatchPlayerNews.h"
#import "MatchHistoryTVC.h"
#import "ConfirmIndMatchScoreSuccessVC.h"
#import "FinalDisputeMessageVC.h"
#import "ComposeNewMessageIntroTVC.h"
#import "VenueDetailTVC.h"

@interface MatchNotificationsVC ()

@property (nonatomic, strong) UIImage *backImage;

@property (strong, nonatomic) UserPreferences *preferences;

@property (weak, nonatomic) IBOutlet UIImageView *profilePic;
@property (nonatomic, strong) NSMutableArray *urgentNotifications;

@property (nonatomic, strong) NSMutableArray *otherNotifications;


// needed
@property (weak, nonatomic) IBOutlet UIButton *todaysFeaturedMatchDetailButton;
@property (nonatomic, strong) NSMutableArray *alertNotifications;
@property (nonatomic, strong) NSMutableArray *upcomingNotifications;
@property (nonatomic, strong) NSMutableArray *todaysNotifications;
@property (nonatomic, strong) NSMutableArray *updatesFeed;
@property (nonatomic, strong) NSMutableArray *newMessages;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic) NSUInteger totalAlerts;
@property (nonatomic) NSUInteger totalUpcoming;
@property (nonatomic) NSUInteger totalToday;
@property (nonatomic) NSUInteger totalMessages;
@property (weak, nonatomic) IBOutlet UILabel *todayMatchName;
@property (weak, nonatomic) IBOutlet UILabel *todayMatchDetails;
@property (weak, nonatomic) IBOutlet UIImageView *todayMatchSportIcon;
@property (weak, nonatomic) IBOutlet UIButton *todayMoreButton;
@property (weak, nonatomic) IBOutlet UILabel *todaysDateLabel;
@property (weak, nonatomic) IBOutlet UIView *todayHeaderView;

@property (nonatomic, strong) NSIndexPath * selectedIndexPath;
@property (nonatomic, strong) UIRefreshControl *refreshControl;
@property (nonatomic, strong) UIView *tempView;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;

@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedController;

// end needed

@property (nonatomic) NSInteger urgentCount;
@property (nonatomic) BOOL showNoResultsUrgentFlag;
@property (nonatomic) BOOL showNoResultsUpcomingFlag;
@property (nonatomic) BOOL showNoResultsMessagesFlag;

@property (nonatomic) NSUInteger matchListTypeToShowOnNextSegue;

@property (nonatomic) NSInteger numPastMatches;
@property (nonatomic) NSInteger numWins;
@property (nonatomic) NSInteger numLosses;
@property (nonatomic) NSInteger numTies;
@property (nonatomic) NSDate *dateOfLastMatch;
@property (nonatomic) Player *lastMatchCompetitor;
@property (weak, nonatomic) IBOutlet UIView *fixedMenu;
@property (nonatomic, strong) UIView *redUnderline;
@property (nonatomic) NSUInteger pageToDisplay;

@property (nonatomic, strong) UIButton *urgentButton;
@property (nonatomic, strong) UIButton *upcomingButton;
@property (nonatomic, strong) UIButton *historyButton;
//@property (nonatomic, strong) MKNumberBadgeView *messagesNumberBubble;

@property (nonatomic, strong) MKNumberBadgeView *urgentNumberBubble;
@property (nonatomic, strong) MKNumberBadgeView *upcomingNumberBubble;
@property (nonatomic) NSInteger numNewMessages;

@property (nonatomic, strong) UIView *errorOverlay;
@end

@implementation MatchNotificationsVC


#define LOAD_UPCOMING_MATCHES 0
#define LOAD_TODAYS_MATCHES 1
#define LOAD_PAST_MATCHES 2

#define SEGMENT_ALERTS 0
#define SEGMENT_HISTORY 1

#define SHOW_URGENT_PAGE 0
#define SHOW_UPCOMING_PAGE 1
#define SHOW_HISTORY_PAGE 2

- (NSMutableArray *)newMessages
{
    if (!_newMessages) _newMessages = [[NSMutableArray alloc] init];
    return _newMessages;
}

- (NSMutableArray *)updatesFeed
{
    if (!_updatesFeed) _updatesFeed = [[NSMutableArray alloc] init];
    return _updatesFeed;
}

- (NSMutableArray *)todaysNotifications
{
    if (!_todaysNotifications) _todaysNotifications = [[NSMutableArray alloc] init];
    return _todaysNotifications;
}

- (NSMutableArray *)alertNotifications
{
    if (!_alertNotifications) _alertNotifications = [[NSMutableArray alloc] init];
    return _alertNotifications;
}

- (NSMutableArray *)otherNotifications
{
    if (!_otherNotifications) _otherNotifications = [[NSMutableArray alloc] init];
    return _otherNotifications;
}

- (NSMutableArray *)urgentNotifications
{
    if (!_urgentNotifications) _urgentNotifications = [[NSMutableArray alloc] init];
    return _urgentNotifications;
}

- (NSMutableArray *)upcomingNotifications
{
    if (!_upcomingNotifications) _upcomingNotifications = [[NSMutableArray alloc] init];
    return _upcomingNotifications;
}

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshMatchNotificationsPageFromNSNotification:)
                                                 name:@"refreshMatchNotificationsPageNotification"
                                               object:nil];
    
    if (![self.preferences getHasMatchNotificationsPageBeenLoadedAtLeastOnce]) {
        [self.preferences setHasMatchNotificationsPageBeenLoadedAtLeastOnce:YES];
        [self.tabBarController setSelectedIndex:0];
    }
    
    [self.preferences setMatchNotificationRefreshState:NO];
    self.todayHeaderView.hidden = YES;
    self.numPastMatches = 0;
    
    
    /* test */
    
    PhenomAppDelegate *ad = (PhenomAppDelegate *)[[UIApplication sharedApplication] delegate];
    ad.myTabBarController = self.tabBarController;
    
  /*  [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshMatchNotificationsPageFromNSNotification:)
                                                 name:@"refreshMatchNotificationsPageNotification"
                                               object:nil];
    */
    /* end test */
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    self.pageToDisplay = SHOW_URGENT_PAGE;
    
    
   
    self.matchListTypeToShowOnNextSegue = 0;
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(refreshView) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshControl];
    
//    [self refreshView]; // temporarily removing and placing in view will appear in case push notifications don't come through
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
    
    [self makeFixedMenu];
    [self setUpMenuButtonUnderline];
    
}

- (void)setUpMenuButtonUnderline
{
    self.redUnderline = [[UIView alloc] initWithFrame:CGRectMake(0, 99, [UIScreen mainScreen].bounds.size.width / 3, 2)];
    [self.redUnderline setBackgroundColor:[UIColor peacock]];
    
    [self.view addSubview:self.redUnderline];
    
}
- (void)makeFixedMenu
{
    UIView *fixedMenu = [[UIView alloc] initWithFrame:CGRectMake(0, 60, 320, 40)];
    fixedMenu.backgroundColor = [UIColor midGray2];
    fixedMenu.layer.borderColor = [UIColor darkGrayColor].CGColor;
    fixedMenu.layer.borderWidth = .5f;
    
    CGFloat myOffset = [UIScreen mainScreen].bounds.size.width / 3;
    
    self.urgentButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 3, myOffset, 38)];
    [self.urgentButton setTitle:@"To Do" forState:UIControlStateNormal];
    [self.urgentButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    self.urgentButton.titleLabel.font = [UIFont systemFontOfSize:14];
    [self.urgentButton.layer setBorderWidth:1.0];
    [self.urgentButton.layer setBorderColor:[[UIColor midGray1] CGColor]];
    [self.urgentButton addTarget:self action:@selector(urgentButtonAction) forControlEvents:UIControlEventTouchUpInside];
    
    [fixedMenu addSubview:self.urgentButton];
    
    self.upcomingButton = [[UIButton alloc] initWithFrame:CGRectMake(myOffset, 3, [UIScreen mainScreen].bounds.size.width / 3, 38)];
    [self.upcomingButton setTitle:@"Upcoming" forState:UIControlStateNormal];
    [self.upcomingButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    self.upcomingButton.titleLabel.font = [UIFont systemFontOfSize:14];
    [self.upcomingButton.layer setBorderWidth:1.0];
    [self.upcomingButton.layer setBorderColor:[[UIColor midGray1] CGColor]];
    [self.upcomingButton addTarget:self action:@selector(upcomingButtonAction) forControlEvents:UIControlEventTouchUpInside];

    
    [fixedMenu addSubview:self.upcomingButton];
    
    self.historyButton = [[UIButton alloc] initWithFrame:CGRectMake(myOffset * 2, 3, [UIScreen mainScreen].bounds.size.width / 3, 38)];
    [self.historyButton setTitle:@"History" forState:UIControlStateNormal];
    [self.historyButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    self.historyButton.titleLabel.font = [UIFont systemFontOfSize:14];
    [self.historyButton.layer setBorderWidth:1.0];
    [self.historyButton.layer setBorderColor:[[UIColor midGray1] CGColor]];
    [self.historyButton addTarget:self action:@selector(historyButtonAction) forControlEvents:UIControlEventTouchUpInside];
//    self.messagesButton.titleLabel.textAlignment = NSTextAlignmentRight;
 //   self.messagesButton.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
 //   [self.messagesButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
    [fixedMenu addSubview:self.historyButton];
    
    
 //   fixedMenu.center = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height-10/2 + 50);
    [self.view addSubview:fixedMenu];
    


}


- (void)adjustMenuButtons
{
    [self.urgentNumberBubble removeFromSuperview];
    [self.upcomingNumberBubble removeFromSuperview];
    
    if ([self.alertNotifications count] > 0) {
        [self.urgentButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
        self.urgentButton.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 25);
        
        self.urgentNumberBubble = [[MKNumberBadgeView alloc] initWithFrame:CGRectMake(2, 8, 30,20)];
        self.urgentNumberBubble.value = [self.alertNotifications count];
        self.urgentNumberBubble.fillColor = [UIColor peacock];
        self.urgentNumberBubble.textColor = [UIColor whiteColor];
        
        
        [self.urgentButton addSubview:self.urgentNumberBubble];

    } else {
         [self.urgentButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
        self.urgentButton.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    }
    
    if ([self.upcomingNotifications count] > 0) {
        [self.upcomingButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
        self.upcomingButton.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 8);
        
        self.upcomingNumberBubble = [[MKNumberBadgeView alloc] initWithFrame:CGRectMake(2, 8, 30, 20)];
        self.upcomingNumberBubble.value = [self.upcomingNotifications count];
        self.upcomingNumberBubble.fillColor = [UIColor darkGrayColor]; // midGray1
        self.upcomingNumberBubble.textColor = [UIColor whiteColor]; // darkGrayColor
        
        [self.upcomingButton addSubview:self.upcomingNumberBubble];
        
    } else {
        [self.upcomingButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
    }
}

- (void)urgentButtonAction
{
    // clear subviews
    if ([self.tableView subviews]){
        for (UIView *subview in [self.tableView subviews]) {
            if (subview.tag == 1)
                [subview removeFromSuperview];
            
        }
    }
    
    
    [self.errorOverlay removeFromSuperview];
    self.errorOverlay = nil;
    
    CGRect newUnderlineFrame = self.redUnderline.frame;
    newUnderlineFrame.origin.x = 0;
    
    [UIView animateWithDuration:0.2
                          delay:0
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         self.redUnderline.frame = newUnderlineFrame;
                     }
                     completion:^(BOOL finished){
                         self.pageToDisplay = SHOW_URGENT_PAGE;
                         
                   //      if ([self.alertNotifications count] == 0)
                   //          [self displayErrorOverlay:SHOW_URGENT_PAGE];
                     
                         self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
                         [self refreshView];
                       //  [self.tableView reloadData];
                         
                         
                         
                         NSLog(@"Animation Complete!");
                         
                     }];
}

- (void)upcomingButtonAction
{
    // clear subviews
    if ([self.tableView subviews]){
        for (UIView *subview in [self.tableView subviews]) {
            if (subview.tag == 1)
                [subview removeFromSuperview];
            
        }
    }
    
    
    [self.errorOverlay removeFromSuperview];
    self.errorOverlay = nil;
    CGRect newUnderlineFrame = self.redUnderline.frame;
    newUnderlineFrame.origin.x = [UIScreen mainScreen].bounds.size.width / 3;
    
    [UIView animateWithDuration:0.2
                          delay:0
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         self.redUnderline.frame = newUnderlineFrame;
                     }
                     completion:^(BOOL finished){
                         self.pageToDisplay = SHOW_UPCOMING_PAGE;
                         
                       //  if ([self.upcomingNotifications count] == 0)
                       //      [self displayErrorOverlay:SHOW_UPCOMING_PAGE];
                         
                         [self refreshView];
                         self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

                        // [self.tableView reloadData];
                         NSLog(@"Animation Complete!");
                         
                     }];
}

- (void)historyButtonAction
{
    // clear subviews
    if ([self.tableView subviews]){
        for (UIView *subview in [self.tableView subviews]) {
            if (subview.tag == 1)
                [subview removeFromSuperview];
            
        }
    }
    
    
    [self.errorOverlay removeFromSuperview];
    self.errorOverlay = nil;
    
    CGRect newUnderlineFrame = self.redUnderline.frame;
    newUnderlineFrame.origin.x = ([UIScreen mainScreen].bounds.size.width / 3 ) * 2 ;
    
    [UIView animateWithDuration:0.2
                          delay:0
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         self.redUnderline.frame = newUnderlineFrame;
                     }
                     completion:^(BOOL finished){
                         self.pageToDisplay = SHOW_HISTORY_PAGE;
                         
                     //    if (self.numPastMatches == 0)
                    //         [self displayErrorOverlay:SHOW_HISTORY_PAGE];
                         self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

                      [self refreshView];
                    //     [self.tableView reloadData];
                         NSLog(@"Animation Complete!");
                         
                     }];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
      self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 195, 0);

    

}
// using this to refresh the match notifications page from the app delegate when receive push notification

- (void) refreshMatchNotificationsPageFromNSNotification:(NSNotification*)notification
{
//    [[NSNotificationCenter defaultCenter]
//     removeObserver:self
//     name:@"refreshMatchNotificationsPageNotification"
//     object:nil];
    
    if ([[self.preferences getMatchNotificationsForceSegueAction] isEqualToString:@"IR"]) {
        [self.navigationController popToRootViewControllerAnimated:NO];
        [self performSegueWithIdentifier:@"Force Push Record Score For Match Segue" sender:self];
    } else if ([[self.preferences getMatchNotificationsForceSegueAction] isEqualToString:@"IC"]) {
        [self.navigationController popToRootViewControllerAnimated:NO];
        [self performSegueWithIdentifier:@"Force Push Confirm Score For Match Segue" sender:self];
    } else if ([[self.preferences getMatchNotificationsForceSegueAction] isEqualToString:@"ID"]) {
        [self.navigationController popToRootViewControllerAnimated:NO];
        [self performSegueWithIdentifier:@"Force Push Individual Match Detail Segue" sender:self];
    } else if ([[self.preferences getMatchNotificationsForceSegueAction] isEqualToString:@"PD"]) {
        [self.navigationController popToRootViewControllerAnimated:NO];
        [self performSegueWithIdentifier:@"Force Push Pickup Match Detail Segue" sender:self];
    } else if ([[self.preferences getMatchNotificationsForceSegueAction] isEqualToString:@"IH"]) {
        [self.navigationController popToRootViewControllerAnimated:NO];
        [self performSegueWithIdentifier:@"Force Push Individual Match Detail Segue" sender:self];
    } else if ([[self.preferences getMatchNotificationsForceSegueAction] isEqualToString:@"PH"]) {
        [self.navigationController popToRootViewControllerAnimated:NO];
        [self performSegueWithIdentifier:@"Force Push Pickup Match Detail Segue" sender:self];
    
    } else if ([[self.preferences getMatchNotificationsForceSegueAction] isEqualToString:@"IF"]) {
        [self.navigationController popToRootViewControllerAnimated:NO];
        [self performSegueWithIdentifier:@"Force Push Match Result Level Segue" sender:self];
    } else if ([[self.preferences getMatchNotificationsForceSegueAction] isEqualToString:@"IN"]) {
        [self.navigationController popToRootViewControllerAnimated:NO];
        [self performSegueWithIdentifier:@"Force Push Match Dispute Message Segue" sender:self];
    } else if ([[self.preferences getMatchNotificationsForceSegueAction] isEqualToString:@"TD"]) {
        [self.navigationController popToRootViewControllerAnimated:NO];
        [self performSegueWithIdentifier:@"Force Push Team Page Details Segue" sender:self];
    } else if ([[self.preferences getMatchNotificationsForceSegueAction] isEqualToString:@"CD"]) {
        [self.navigationController popToRootViewControllerAnimated:NO];
        [self performSegueWithIdentifier:@"Force Push Venue Page Details Segue" sender:self];

    
    } else {
        [self refreshView];
    }
}



- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    // clear subviews
    if ([self.tableView subviews]){
        for (UIView *subview in [self.tableView subviews]) {
            if (subview.tag == 1)
                [subview removeFromSuperview];
            
        }
    }
    
    
    [self.errorOverlay removeFromSuperview];
    self.errorOverlay = nil;
    

    
    if ([self.preferences getMatchNotificationRefreshState] == YES) {
        [self refreshView];
        [self.preferences setMatchNotificationRefreshState:NO];
    } else {
        [self refreshView]; // temporarily doing this in case push notifications don't come through
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    if (self.pageToDisplay == SHOW_URGENT_PAGE) {
        
        return [self.alertNotifications count];
    } else if (self.pageToDisplay == SHOW_UPCOMING_PAGE) {
    
        return 1;
    } else if (self.pageToDisplay == SHOW_HISTORY_PAGE) {
        if (self.numPastMatches > 0)
            return 1;
        else {
        //    [self displayErrorOverlay:SHOW_HISTORY_PAGE];
        }
    }
    
    return 0;
  
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (self.pageToDisplay == SHOW_UPCOMING_PAGE) {
        //   if ([self.upcomingNotifications count] == 0) {
        //        [self displayErrorOverlay:SHOW_UPCOMING_PAGE];
        //    }
            return [self.upcomingNotifications count];
    }
    
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Probably don't know MatchNumNote object any more.
    
    if (self.pageToDisplay == SHOW_URGENT_PAGE) {
        if (self.showNoResultsUrgentFlag || [self.alertNotifications count] == 0) {
            static NSString *CellIdentifier = @"Urgent Match Cell";
            UrgentMatchNotificationCell *cell = (UrgentMatchNotificationCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
       //     cell.headlineMessage.text = @"No Actions Needed";
            //        cell.headlineMessage.textColor = [UIColor grayColor];
            cell.headlineMessage.font = [UIFont preferredFontForTextStyle:UIFontTextStyleSubheadline];
            cell.matchCount.hidden = YES;
            cell.description.hidden = YES;
            cell.notePic.hidden = YES;
            cell.sportLabel.hidden = YES;
     //       cell.backgroundColor = [UIColor clearColor];
            cell.accessoryType = UITableViewCellAccessoryNone;
            
            return cell;
        }
        
        static NSString *CellIdentifier = @"Urgent Match Cell";
        UrgentMatchNotificationCell *cell = (UrgentMatchNotificationCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        
    //    cell.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.2];
        
        Match *alertMatchObject = [self.alertNotifications objectAtIndex:indexPath.row];
        
        cell.headlineMessage.hidden = NO;
        cell.description.hidden = NO;
        cell.notePic.hidden = NO;
        cell.sportLabel.hidden = NO;
    
        
        if ([alertMatchObject isKindOfClass:[IndividualMatch class]]) {
            
            
            IndividualMatch *iMatch = (IndividualMatch *)alertMatchObject;
            NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                    baseString:[iMatch getChallengerPlayerObject:[self.preferences getUserID]].profileBaseString];
            
            [cell.notePic setImageWithURL:[NSURL URLWithString:url]
                         placeholderImage:[UIImage imageNamed:@"avatar round"]];
            [UIImage makeRoundedImage:cell.notePic withRadius:18];
            cell.sportLabel.text = [NSString stringWithFormat:@"%@ Challenge Match", [iMatch.sport.sportName capitalizedString]];
            
        } else {
            // must be a pickup match
            cell.description.textColor = [UIColor lightGreenColor];
            PickupMatch *pMatch = (PickupMatch *)alertMatchObject;
            
          //  cell.notePic.image = [pMatch.sport getSportIcon];
            cell.notePic.tintColor = [UIColor peacock];
            cell.notePic.image = [[pMatch.sport getSportIcon] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];

            
            cell.sportLabel.text = [NSString stringWithFormat:@"%@ Pickup Match", [pMatch.sport.sportName capitalizedString]];
            cell.bulbIcon.tintColor = [UIColor lightGreenColor];
            cell.bulbIcon.image = [[UIImage imageNamed:@"Bulb Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            
        }
        
        cell.headlineMessage.text = alertMatchObject.matchName;
        cell.description.text = alertMatchObject.matchNotification;
        
        return cell;
    } else if (self.pageToDisplay == SHOW_UPCOMING_PAGE) {
        
        if (self.showNoResultsUpcomingFlag || [self.upcomingNotifications count] == 0) {
            static NSString *CellIdentifier = @"Upcoming Match Cell";
            UpcomingPickupMatchNotificationCell *cell = (UpcomingPickupMatchNotificationCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
         //   cell.headlineMessage.text = @"No Upcoming Matches";
            cell.headlineMessage.font = [UIFont preferredFontForTextStyle:UIFontTextStyleSubheadline];
            cell.matchCount.hidden = YES;
            cell.description.hidden = YES;
            cell.noteBackground.hidden = YES;
            cell.notePic.hidden = YES;
            cell.pendingConfirmedTag.hidden = YES;
            cell.montLabel.hidden = YES;
            cell.dayLabel.hidden = YES;
      //      [cell.backgroundView makeRoundedBorderWithColor:[UIColor redColor]];
            return cell;
            
        }
        
        Match *upcomingMatchObject = [self.upcomingNotifications objectAtIndex:indexPath.section];

        if ([upcomingMatchObject isKindOfClass:[PickupMatch class]]) {
        
            static NSString *CellIdentifier = @"Upcoming Pickup Match Cell";
            UpcomingPickupMatchNotificationCell *cell = (UpcomingPickupMatchNotificationCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        
            // Note: when we pulled data from server, we distinguished between Individual Matches and Pickup Matches.  For time's sake, I won't distinguish
            // here because it's not necessary righ tnow.  In future we might want to offer functionality based on differences of match types.
            // (Match class is parent class of Individual and Pickup Match classes)
        
           
            cell.matchNameLabel.text = upcomingMatchObject.matchName;
        
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"MMMM d, YYYY"];
            
            NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
            [timeFormatter setDateFormat:@"h:mm a"];
        
            
            cell.dateLabel.text = [NSString stringWithFormat:@"%@", [dateFormatter stringFromDate:upcomingMatchObject.dateTime]];
            cell.timeLabel.text = [timeFormatter stringFromDate:upcomingMatchObject.dateTime];
        
            cell.headlineMessage.text = [NSString stringWithFormat:@"%@ Pickup Match", [upcomingMatchObject.sport.sportName capitalizedString]];
            cell.matchCategory = @"pickup";
        
            cell.detailButton.tag = indexPath.section;
            
            PickupMatch *pMatch = (PickupMatch *)upcomingMatchObject;
            cell.competitors = pMatch.competitorArray;
            
            cell.withLabel.text = [NSString stringWithFormat:@"%ld players", [pMatch getNumConfirmed]];
            
        
            [cell.mainBackgroundViewCell makeRoundedBorderWithRadius:3];
            cell.mainBackgroundViewCell.layer.borderColor = [UIColor midGray2].CGColor;
            cell.mainBackgroundViewCell.layer.borderWidth = 1.0;
            
            
            cell.sportPicBackground.contentMode = UIViewContentModeScaleAspectFill;
            cell.sportPicBackground.clipsToBounds = YES;
            cell.sportPicBackground.image = [pMatch getMatchDetailPageBackground];
            
            
            
            NSString *todayTomorrowStatus = [upcomingMatchObject.dateTime getTodayTomorrowDateStatus];
        
            if ([todayTomorrowStatus length] > 0) {
                cell.todayTomorrowLabel.text = todayTomorrowStatus;
                cell.todayTomorrowLabel.hidden = NO;
                cell.todayTomorrowLabel.textColor = [UIColor salmonColor];
            } else
                cell.todayTomorrowLabel.hidden = YES;
        
        
            if ([upcomingMatchObject.matchStatus isEqualToString:@"UNCONFIRMED"]) {
            
                if ([upcomingMatchObject isKindOfClass:[IndividualMatch class]]) {
                    cell.pendingConfirmedTag.text = @"Awaiting opponent response";
                } else {
                    cell.pendingConfirmedTag.text = @"Needs More Players";
                }
            
            } else if ([upcomingMatchObject.matchStatus isEqualToString:@"MANUAL_CONFIRMED"] ||
                   [upcomingMatchObject.matchStatus isEqualToString:@"RSVP_CONFIRMED"]) {
           //     cell.pendingConfirmedTag.textColor = [UIColor whiteColor];
                cell.pendingConfirmedTag.text = @"Ready To Play";
            } else if ([upcomingMatchObject.matchStatus isEqualToString:@"CANCELLED"]) {
          //      cell.pendingConfirmedTag.textColor = [UIColor whiteColor];
                cell.pendingConfirmedTag.text = @"Match Is Cancelled";
            }
        
            cell.sportIcon.image= [[upcomingMatchObject.sport getSportIcon] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            cell.sportIcon.tintColor = [UIColor whiteColor];
        
            cell.sportLabel.text = [upcomingMatchObject.sport.sportName capitalizedString];
        
    
            cell.starIcon.image = [[UIImage imageNamed:@"Star Filled Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            cell.starIcon.tintColor = [UIColor whiteColor];
        
        
            cell.venueLabel.text = [upcomingMatchObject.venue.venueName capitalizedString];
        
            [cell setPlayerValues];
            [cell.playerCollectionView reloadData];
            
            NSInteger numDiff = [pMatch getNumConfirmed] - 2;
            
            if (numDiff > 0)
                cell.additionalPlayersLabel.text = [NSString stringWithFormat:@"and %ld others", numDiff];
            else
                cell.additionalPlayersLabel.hidden = YES;

            
        
            return cell;
        } else if ([upcomingMatchObject isKindOfClass:[IndividualMatch class]]) {
            
            static NSString *CellIdentifier = @"Upcoming Individual Match Cell";
            UpcomingIndividualMatchNotificationCell *cell = (UpcomingIndividualMatchNotificationCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
            
      //      cell.description.text = [NSString stringWithFormat:@"%@ Challenge Match", [upcomingMatchObject.sport.sportName capitalizedString]];
            
            IndividualMatch *iMatch = (IndividualMatch*)upcomingMatchObject;
            
         
            cell.headlineMessage.text = upcomingMatchObject.matchName;

            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            
              // player2 pic
            
            Player *p = [iMatch getOtherPlayer:[self.preferences getUserID]];
            
            NSString *urlMain2 = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                         baseString:p.profileBaseString];
            
            [cell.userImage2 setImageWithURL:[NSURL URLWithString:urlMain2]
                            placeholderImage:[UIImage imageNamed:@"avatar square"]];
            
//            [UIImage makeRoundedImage:cell.userImage2 withRadius:20];
  
            [cell.mainBackgroundViewCell makeRoundedBorderWithRadius:3];
            cell.mainBackgroundViewCell.layer.borderColor = [UIColor midGray2].CGColor;
            cell.mainBackgroundViewCell.layer.borderWidth = 1.0;
            
            
            cell.sportPicBackground.contentMode = UIViewContentModeScaleAspectFill;
            cell.sportPicBackground.clipsToBounds = YES;
            cell.sportPicBackground.image = [iMatch getMatchDetailPageBackground];
            
            
             //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            
            cell.detailButton.tag = indexPath.section;
            
            NSString *todayTomorrowStatus = [upcomingMatchObject.dateTime getTodayTomorrowDateStatus];
            
            if ([todayTomorrowStatus length] > 0) {
                cell.todayTomorrowLabel.text = todayTomorrowStatus;
                cell.todayTomorrowLabel.hidden = NO;
                cell.todayTomorrowLabel.textColor = [UIColor salmonColor];
            } else
                cell.todayTomorrowLabel.hidden = YES;
            
            
            if ([upcomingMatchObject.matchStatus isEqualToString:@"UNCONFIRMED"]) {
                
                if ([upcomingMatchObject isKindOfClass:[IndividualMatch class]]) {
                    cell.pendingConfirmedTag.text = @"Awaiting opponent response";
                } else {
                    cell.pendingConfirmedTag.text = @"Needs More Players";
                }
                
            } else if ([upcomingMatchObject.matchStatus isEqualToString:@"MANUAL_CONFIRMED"] ||
                       [upcomingMatchObject.matchStatus isEqualToString:@"RSVP_CONFIRMED"]) {
         //       cell.pendingConfirmedTag.textColor = [UIColor whiteColor];
                cell.pendingConfirmedTag.text = @"Ready To Play";
            } else if ([upcomingMatchObject.matchStatus isEqualToString:@"CANCELLED"]) {
            //    cell.pendingConfirmedTag.textColor = [UIColor whiteColor];
                cell.pendingConfirmedTag.text = @"Match Is Cancelled";
            } else if ([upcomingMatchObject.matchStatus isEqualToString:@"NEVER_OCCURRED"]) {
            //    cell.pendingConfirmedTag.textColor = [UIColor whiteColor];
                cell.pendingConfirmedTag.text = @"Match Was Voided";
            }
            
            cell.sportIcon.image= [[upcomingMatchObject.sport getSportIcon] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            cell.sportIcon.tintColor = [UIColor whiteColor];
            
            cell.headlineMessage.text = [NSString stringWithFormat:@"%@ CHALLENGE MATCH", [upcomingMatchObject.sport.sportName uppercaseString]];
            cell.matchNameLabel.text = upcomingMatchObject.matchName;
            
            cell.venueLabel.text = [upcomingMatchObject.venue.venueName capitalizedString];
            
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"MMMM d, YYYY"];
            
            NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
            [timeFormatter setDateFormat:@"h:mm a"];
            
            
            cell.dateLabel.text = [NSString stringWithFormat:@"%@", [dateFormatter stringFromDate:upcomingMatchObject.dateTime]];
            cell.timeLabel.text = [timeFormatter stringFromDate:upcomingMatchObject.dateTime];
        
            
            return  cell;
            
        }
    } else if (self.pageToDisplay == -2 ) {
        if (self.showNoResultsMessagesFlag || [self.newMessages count] == 0) {
            static NSString *CellIdentifier = @"New Message Cell";
            MatchNotificationsNewMessageCell *cell = (MatchNotificationsNewMessageCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            //   cell.headlineMessage.text = @"No Upcoming Matches";
            cell.usernameLabel.hidden = YES;
            cell.messageLabel.hidden = YES;
        //    [cell.backgroundView makeRoundedBorderWithColor:[UIColor redColor]];
            return cell;
            
        }
        
        static NSString *CellIdentifier = @"New Message Cell";
        MatchNotificationsNewMessageCell *cell = (MatchNotificationsNewMessageCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        
        MatchPlayerNews *mpn = [self.newMessages objectAtIndex:indexPath.row];
        
     //   cell.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.2];
        cell.usernameLabel.text = mpn.player.userName;
        cell.usernameLabel.textColor = [UIColor peacock];
        
        if ([mpn.playerMessage length] < 38)
            cell.messageLabel.text = [NSString stringWithFormat:@"\"%@\"", mpn.playerMessage];
        else
            cell.messageLabel.text = [NSString stringWithFormat:@"\"%@...\"", [mpn.playerMessage substringWithRange:NSMakeRange(0, 38)]];
        return cell;
        
    } else if (self.pageToDisplay == SHOW_HISTORY_PAGE) {
        static NSString *CellIdentifier = @"Past Cell";
        NumPastMatchesCell *cell = (NumPastMatchesCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
  
        
        cell.numPastMatches.text = [NSString stringWithFormat:@"%ld", self.numPastMatches];
        cell.numWins.text = [NSString stringWithFormat:@"%ld", self.numWins];
        cell.numLosses.text = [NSString stringWithFormat:@"%ld", self.numLosses];
        cell.numTies.text = [NSString stringWithFormat:@"%ld", self.numTies];
        
        if (self.numWins == 1)
            cell.winLabel.text = @"Win";
        else
            cell.winLabel.text = @"Wins";
        
        if (self.numLosses == 1)
            cell.lossLabel.text = @"Loss";
        else
            cell.lossLabel.text = @"Losses";
        
        if (self.numTies == 1)
            cell.tieLabel.text = @"Tie";
        else
            cell.tieLabel.text = @"Ties";
        
        if (self.numPastMatches == 1)
            cell.matchLabel.text = @"Match";
        else
            cell.matchLabel.text = @"Matches";
        
        cell.winValue = self.numWins;
        cell.lossValue = self.numLosses;
        cell.tieValue = self.numTies;
        cell.voidValue = self.numPastMatches - (self.numWins + self.numLosses + self.numTies);
        cell.numUnofficial.text = [NSString stringWithFormat:@"%ld", self.numPastMatches - (self.numWins + self.numLosses + self.numTies)];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"MMM d"];
        
        NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
        [timeFormatter setDateFormat:@"h:mm a"];
        
        [cell.viewListButton makeCircleWithColor:[UIColor peacock] andRadius:5];
        
        
  //      cell.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.2];
        
        if ([self.lastMatchCompetitor isKindOfClass:[NSNull class]] || self.lastMatchCompetitor == nil) {
            cell.lastMatchDate.hidden = YES;
            cell.vsPlayer.hidden = YES;
            cell.horizBar.hidden = YES;
        } else {
            cell.lastMatchDate.text = [NSString stringWithFormat:@"Last Match %@ at %@", [dateFormatter stringFromDate:self.dateOfLastMatch], [timeFormatter stringFromDate: self.dateOfLastMatch]];
            
            cell.vsPlayer.text = [NSString stringWithFormat:@"vs %@", self.lastMatchCompetitor.userName];
            
            NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                    baseString:self.lastMatchCompetitor.profileBaseString];
            
            [cell.profilePic setImageWithURL:[NSURL URLWithString:url]
                         placeholderImage:[UIImage imageNamed:@"avatar round"]];
            [UIImage makeRoundedImage:cell.profilePic withRadius:15];

        }

        [cell loadPie];
        [cell.pieChartLeft reloadData];
        [cell.pieChartRight reloadData];
        
        return cell;
    }
    
    return nil; // this will crash
    
}


- (void)setMatchNotifications2
{
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.labelText = @"Loading";
    
    [IOSRequest fetchMatchNotifications:[self.preferences getUserID]
                           onCompletion:^(NSDictionary *results, NSError *error) {
                               
                               if (error)
                                   dispatch_async(dispatch_get_main_queue(), ^{
                                      [MBProgressHUD hideHUDForView:self.view animated:YES];
                                   });
                               
                               [self.alertNotifications removeAllObjects];
                               [self.upcomingNotifications removeAllObjects];
                               [self.todaysNotifications removeAllObjects];
                               // Match Alerts
                               id matchAlertsObject = (id)results[@"matches_needing_action_data"];
                               self.totalAlerts = [matchAlertsObject[@"total_count"] integerValue];
                               // Need to sort the data array on the server side
                               for (id alertSubObject in matchAlertsObject[@"data"]) {
                                   
                                   NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
                                   [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                                   NSDate *alertDateTime = [[formatter dateFromString:alertSubObject[@"ts"]] toLocalTime];
                                   
                                   if ([alertSubObject[@"match_category"] isEqualToString:@"individual"]) {
                                       IndividualMatch *scoreIndividualMatch = [[IndividualMatch alloc] init];
                                       [scoreIndividualMatch setMatchID:[alertSubObject[@"match_id"] integerValue]];
                                       [scoreIndividualMatch setMatchName:alertSubObject[@"match_name"]];
                                       [scoreIndividualMatch setDateTime:alertDateTime];
                                       [scoreIndividualMatch setMatchNotification:alertSubObject[@"match_notification"]];
                                       
                                       MatchPlayer *player2 = [MatchPlayer makeMatchCompetitorObject:alertSubObject[@"competitors"]
                                                                                           forPlayer:2
                                                                                        forSportName:alertSubObject[@"sport_name"]
                                                                                             andType:alertSubObject[@"sport_type"]];
                                       
                                       MatchPlayer *player1 = [MatchPlayer makeMatchCompetitorObject:alertSubObject[@"competitors"]
                                                                                           forPlayer:1
                                                                                        forSportName:alertSubObject[@"sport_name"]
                                                                                             andType:alertSubObject[@"sport_type"]];
                                       [scoreIndividualMatch setPlayer1:player1];
                                       [scoreIndividualMatch setPlayer2:player2];
                                       Sport *sport = [[Sport alloc] initWithSportDetails:[alertSubObject[@"sport_id"] integerValue]
                                                                                  andName:alertSubObject[@"sport_name"]
                                                                                  andType:alertSubObject[@"sport_type"]];
                                       
                                       [scoreIndividualMatch setSport:sport];
                                       [scoreIndividualMatch setMatchStatus:alertSubObject[@"match_status"]];
                                       [self.alertNotifications addObject:scoreIndividualMatch];
                                   } else {
                                       PickupMatch *alertPickupMatch = [[PickupMatch alloc] init];
                                       [alertPickupMatch setMatchID:[alertSubObject[@"match_id"] integerValue]];
                                       [alertPickupMatch setMatchName:alertSubObject[@"match_name"]];
                                       [alertPickupMatch setDateTime:alertDateTime];
                                       [alertPickupMatch setMatchNotification:alertSubObject[@"match_notification"]];
                                       [alertPickupMatch setMatchCreatorID:[alertSubObject[@"creator_id"] integerValue]];
                                       
                                       NSMutableArray *competitorArray = [PickupMatch makeCompetitorArray:alertSubObject[@"competitors"]
                                                                                             forSportName:alertSubObject[@"sport_name"]
                                                                                                  andType:alertSubObject[@"sport_type"]];
                                       [alertPickupMatch setCompetitorArray:competitorArray];
                                       
                                       Sport *sport = [[Sport alloc] initWithSportDetails:[alertSubObject[@"sport_id"] integerValue]
                                                                                  andName:alertSubObject[@"sport_name"]
                                                                                  andType:alertSubObject[@"sport_type"]];
                                       
                                       [alertPickupMatch setSport:sport];
                                       [alertPickupMatch setMatchStatus:alertSubObject[@"match_status"]];
                                       
                                       [self.alertNotifications addObject:alertPickupMatch];
                                   }
                               }
                               
                               // Upcoming Matches - more complicated because we have to discern between individual and pickupmatches.
                               // Then have to sort the array
                               
                               id upcomingMatchesObject = (id)results[@"upcoming_matches_data"];
                               self.totalUpcoming = [upcomingMatchesObject[@"total_count"] integerValue];
                               
                               for (id upcomingSubObject in upcomingMatchesObject[@"data"]) {
                                   NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
                                   [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                                   NSDate *upcomingDateTime = [[formatter dateFromString:upcomingSubObject[@"ts"]] toLocalTime];
                                   
                                   if ([upcomingSubObject[@"match_category"] isEqualToString:@"individual"]) {
                                       IndividualMatch *upcomingIndividualMatch = [[IndividualMatch alloc] init];
                                       [upcomingIndividualMatch setMatchID:[upcomingSubObject[@"match_id"] integerValue]];
                                       [upcomingIndividualMatch setMatchName:upcomingSubObject[@"match_name"]];
                                       [upcomingIndividualMatch setDateTime:upcomingDateTime];
                                       [upcomingIndividualMatch setMatchStatus:upcomingSubObject[@"match_status"]];
                                       Sport *sport = [[Sport alloc] init];
                                       
                                       [sport setSportName:upcomingSubObject[@"sport_name"]];
                                       [sport setSportType:upcomingSubObject[@"sport_type"]];
                                       upcomingIndividualMatch.sport = sport;
                                       
                                       MatchPlayer *player2 = [MatchPlayer makeMatchCompetitorObject:upcomingSubObject[@"competitors"]
                                                                                           forPlayer:2
                                                                                        forSportName:upcomingSubObject[@"sport_name"]
                                                                                             andType:upcomingSubObject[@"sport_type"]];
                                       
                                       MatchPlayer *player1 = [MatchPlayer makeMatchCompetitorObject:upcomingSubObject[@"competitors"]
                                                                                           forPlayer:1
                                                                                        forSportName:upcomingSubObject[@"sport_name"]
                                                                                             andType:upcomingSubObject[@"sport_type"]];
                                       [upcomingIndividualMatch setPlayer1:player1];
                                       [upcomingIndividualMatch setPlayer2:player2];

                                       Venue *matchVenue = [[Venue alloc] initVenueWithName:upcomingSubObject[@"venue_name"] andVenueID:[upcomingSubObject[@"venue_id"] integerValue]];
                                       [upcomingIndividualMatch setVenue:matchVenue];
                                       
                                       [self.upcomingNotifications addObject:upcomingIndividualMatch];
                                       
                                   } else { // can expect to be a pickup match@property (nonatomic, strong) NSMutableArray *upcomingNotifications;
                                       PickupMatch *upcomingPickupMatch = [[PickupMatch alloc] init];
                                       [upcomingPickupMatch setMatchID:[upcomingSubObject[@"match_id"] integerValue]];
                                       [upcomingPickupMatch setMatchName:upcomingSubObject[@"match_name"]];
                                       [upcomingPickupMatch setDateTime:upcomingDateTime];
                                       [upcomingPickupMatch setMatchStatus:upcomingSubObject[@"match_status"]];
                                       Sport *sport = [[Sport alloc] init];
                                       [sport setSportName:upcomingSubObject[@"sport_name"]];
                                       [sport setSportType:upcomingSubObject[@"sport_type"]];
                                       upcomingPickupMatch.sport = sport;
                                       
                                       NSMutableArray *competitorArray = [PickupMatch makeCompetitorArray:upcomingSubObject[@"competitors"]
                                                                                             forSportName:upcomingSubObject[@"sport_name"]
                                                                                                  andType:upcomingSubObject[@"sport_type"]];
                                       [upcomingPickupMatch setCompetitorArray:competitorArray];

                                       Venue *matchVenue = [[Venue alloc] initVenueWithName:upcomingSubObject[@"venue_name"] andVenueID:[upcomingSubObject[@"venue_id"] integerValue]];
                                       [upcomingPickupMatch setVenue:matchVenue];
                                       
                                       [self.upcomingNotifications addObject:upcomingPickupMatch];
                                       
                                   }
                                   
                                   // Now have to sort the array based on dates
                                   
                                   
                               }
                               
                               // Todays Matches is similar to Upcoming Matches in that it must discern between Individual and Pickup matches.
                               // However, only one object is created because there is only room to display one match at top of page
                               
                               id todaysMatchesObject = (id)results[@"todays_matches_data"];
                               self.totalToday = [todaysMatchesObject[@"total_count"] integerValue];
                               
                               for (id todaySubObject in todaysMatchesObject[@"data"]) { // Should only be one object in array
                                   NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
                                   [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                                   NSDate *todayDateTime = [[formatter dateFromString:todaySubObject[@"ts"]] toLocalTime];
                                   
                                   if ([todaySubObject[@"match_category"] isEqualToString:@"individual"]) {
                                       Sport *sport = [[Sport alloc] initWithSportDetails:[todaySubObject[@"sport_id"] integerValue]
                                                                                  andName:todaySubObject[@"sport_name"]
                                                                                  andType:todaySubObject[@"sport_type"]];
                                       
                                       Venue *venue = [[Venue alloc] initVenueWithName:todaySubObject[@"venue_name"]
                                                                            andVenueID:[todaySubObject[@"venue_id"] integerValue]];
                                       
                                       IndividualMatch *todayIndividualMatch = [[IndividualMatch alloc] init];
                                       [todayIndividualMatch setMatchID:[todaySubObject[@"match_id"] integerValue]];
                                       [todayIndividualMatch setMatchName:todaySubObject[@"match_name"]];
                                       [todayIndividualMatch setDateTime:todayDateTime];
                                       [todayIndividualMatch setSport:sport];
                                       [todayIndividualMatch setVenue:venue];
                                       
                                       [self.todaysNotifications addObject:todayIndividualMatch];
                                   } else {
                                       // for pickup match
                                       
                                       Sport *sport = [[Sport alloc] initWithSportDetails:[todaySubObject[@"sport_id"] integerValue]
                                                                                  andName:todaySubObject[@"sport_name"]
                                                                                  andType:todaySubObject[@"sport_type"]];
                                       
                                       Venue *venue = [[Venue alloc] initVenueWithName:todaySubObject[@"venue_name"]
                                                                            andVenueID:[todaySubObject[@"venue_id"] integerValue]];
                                       
                                       PickupMatch *todayPickupMatch = [[PickupMatch alloc] init];
                                       [todayPickupMatch setMatchID:[todaySubObject[@"match_id"] integerValue]];
                                       [todayPickupMatch setMatchName:todaySubObject[@"match_name"]];
                                       [todayPickupMatch setDateTime:todayDateTime];
                                       [todayPickupMatch setSport:sport];
                                       [todayPickupMatch setVenue:venue];
                                       
                                       [self.todaysNotifications addObject:todayPickupMatch];
                                   }
                                   
                               }
                               
                               self.numPastMatches = [results[@"num_past_matches"] integerValue];
                               
                               [self showTwoMessages:results[@"two_new_messages"]];
                               self.totalMessages = [results[@"num_new_messages"] integerValue];
                               
                               self.numWins = [results[@"num_wins"] integerValue];
                               self.numLosses = [results[@"num_losses"] integerValue];
                               self.numTies = [results[@"num_ties"] integerValue];
                               
                               id lastMatchObj = results[@"last_match"];
                               
                               if (![lastMatchObj isKindOfClass:[NSNull class]]) {
                                   id otherPlayerObj = lastMatchObj[@"other_player"];
                                   
                                   
                                   self.dateOfLastMatch = [MyHelpers getDateTimeFromString:lastMatchObj[@"ts"]];
                                   
                                   self.lastMatchCompetitor = [[Player alloc] initWithPlayerDetails:[otherPlayerObj[@"user_id"] integerValue]
                                                                                        andUserName:otherPlayerObj[@"username"]];

                               }
                               
                               // Have to sort and only keep the top result to display in header of page
                               
                               [self sortNotificationArrays]; // Sorts all the notification arrays
                               
                               dispatch_async(dispatch_get_main_queue(), ^{
                      
                                   if (self.pageToDisplay == SHOW_URGENT_PAGE && [self.alertNotifications count] == 0) {
                                        [self displayErrorOverlay:SHOW_URGENT_PAGE];
                                       
                                   } else if (self.pageToDisplay == SHOW_UPCOMING_PAGE && [self.upcomingNotifications count] == 0) {
                                       [self displayErrorOverlay:SHOW_UPCOMING_PAGE];
                                   } else if (self.pageToDisplay == SHOW_HISTORY_PAGE && self.numPastMatches == 0) {
                                       [self displayErrorOverlay:SHOW_HISTORY_PAGE];
                                   }
                                   
                                   self.todayHeaderView.hidden = NO;
                                   self.numNewMessages = [results[@"num_new_messages"] integerValue];
                                   [self setButtonsNumNewMessages];
                                   
                                   if (self.totalAlerts) {
                                       [[self.navigationController tabBarItem] setBadgeValue:[NSString stringWithFormat:@"%lu", self.totalAlerts]];
                                       [UIApplication sharedApplication].applicationIconBadgeNumber = self.totalAlerts;
                                   } else {
                                       [[self.navigationController tabBarItem] setBadgeValue:nil];
                                       [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
                                   }
                                   [MBProgressHUD hideHUDForView:self.view animated:YES];
                                   [self adjustMenuButtons];
                                   
                                   [self.tableView reloadData];
                                   
                               
                               });
                    
                           }];
 
    double delayInSeconds = 10.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        //code to be executed on the main queue after delay
        [MBProgressHUD hideHUDForView:self.view animated:YES];// just
    });
    
   
    
}

// Sorts the notifications array based on date

- (void)sortNotificationArrays
{
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dateTime"
                                                 ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    // Sort Alert Notifications
    NSArray *sortedArray1;
    sortedArray1 = [self.alertNotifications sortedArrayUsingDescriptors:sortDescriptors];
    self.alertNotifications = [NSMutableArray arrayWithArray:sortedArray1];
    
    // Sort Upcoming Matches
    NSArray *sortedArray2;
    sortedArray2 = [self.upcomingNotifications sortedArrayUsingDescriptors:sortDescriptors];
    self.upcomingNotifications = [NSMutableArray arrayWithArray:sortedArray2];
    
    // Sort Todays Matches
    NSArray *sortedArray3;
    sortedArray3 = [self.todaysNotifications sortedArrayUsingDescriptors:sortDescriptors];
    self.todaysNotifications = [NSMutableArray arrayWithArray:sortedArray3];
    
    // Short Messages
//    NSArray *sortedArray4;
//    sortedArray4 = [self.newMessages sortedArrayUsingDescriptors:sortDescriptors];
//    self.newMessages = [NSMutableArray arrayWithArray:sortedArray4];
    
}

-(void)refreshView
{
    [self.urgentNotifications removeAllObjects];
    [self.upcomingNotifications removeAllObjects];
    [self.otherNotifications removeAllObjects];
    [self.newMessages removeAllObjects];
    
    [self.errorOverlay removeFromSuperview];
    self.errorOverlay = nil;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateFormat:@"MMM d, h:mm a"];
    
    //    [self setMatchNotifications];
    [self setMatchNotifications2];
    

    [self.refreshControl endRefreshing];
}

- (IBAction)viewListButton:(id)sender
{
    if (self.numPastMatches > 0) {
        self.matchListTypeToShowOnNextSegue = LOAD_PAST_MATCHES;
        [self performSegueWithIdentifier:@"Matches List Segue" sender:self];
    }
}
- (IBAction)goToIndividualMatch:(UIButton *)sender
{
    if (!self.showNoResultsUpcomingFlag && [self.upcomingNotifications count] > 0) {
        id match = [self.upcomingNotifications objectAtIndex:sender.tag];
        if ([match isKindOfClass:[IndividualMatch class]]) {
            self.selectedIndexPath = [NSIndexPath indexPathForRow:0 inSection:sender.tag];
            [self performSegueWithIdentifier:@"Individual Match Detail Segue" sender:self];
        }
    }
}

- (IBAction)goToPickupMatch:(UIButton *)sender
{
    if (!self.showNoResultsUpcomingFlag && [self.upcomingNotifications count] > 0) {
        id match = [self.upcomingNotifications objectAtIndex:sender.tag];
        if ([match isKindOfClass:[PickupMatch class]]) {
            self.selectedIndexPath = [NSIndexPath indexPathForRow:0 inSection:sender.tag];
            [self performSegueWithIdentifier:@"Pickup Match Detail Segue" sender:self];
            
        }
    }
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedIndexPath = indexPath;
    
    if (self.pageToDisplay == SHOW_URGENT_PAGE) {
    //    if (indexPath.section == 0) {
            if (!self.showNoResultsUrgentFlag) {
                Match *iMatch = [self.alertNotifications objectAtIndex:indexPath.row];
                
                if ([iMatch.matchNotification isEqualToString:@"Record your match score"] &&
                    [iMatch isKindOfClass:[IndividualMatch class]]) {
                    [self performSegueWithIdentifier:@"Record Score For Match Segue" sender:self];
                } else if ([iMatch.matchNotification isEqualToString:@"Confirm your match score"] &&
                           [iMatch isKindOfClass:[IndividualMatch class]]){
                    [self performSegueWithIdentifier:@"Confirm Score For Match Segue" sender:self];
                } else if ([iMatch.matchNotification isEqualToString:@"Accept or Decline Challenge"] &&
                           [iMatch isKindOfClass:[IndividualMatch class]]) {
                    [self performSegueWithIdentifier:@"Individual Match Detail Segue" sender:self];
                } else if ([iMatch isKindOfClass:[PickupMatch class]]) {
                    [self performSegueWithIdentifier:@"Pickup Match Detail Segue" sender:self];
                }
            }
            

    } else if (self.pageToDisplay == SHOW_UPCOMING_PAGE) {
       // do nothing
        
    } else if (self.pageToDisplay == SHOW_HISTORY_PAGE) {
        if (self.numPastMatches > 0) {
            self.matchListTypeToShowOnNextSegue = LOAD_PAST_MATCHES;
            [self performSegueWithIdentifier:@"Matches List Segue" sender:self];
        }
    } else if (self.pageToDisplay == -1) {
    //    if (!self.showNoResultsMessagesFlag) {
            [self performSegueWithIdentifier:@"New Messages Detail Segue" sender:self];
    //    }
    }
  
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Individual Match Detail Segue"]) {
        IndividualMatchDetailTVC *controller = segue.destinationViewController;
        
        if (self.pageToDisplay == SHOW_UPCOMING_PAGE)  { // upcoming match section
            IndividualMatch *match = [self.upcomingNotifications objectAtIndex:self.selectedIndexPath.section];
            controller.matchID = match.matchID;
        } else if (self.pageToDisplay == SHOW_URGENT_PAGE) {
            IndividualMatch *match = [self.alertNotifications objectAtIndex:self.selectedIndexPath.row];
            controller.matchID = match.matchID;
        } else if (self.selectedIndexPath.section == -1) {
            IndividualMatch *match = [self.todaysNotifications objectAtIndex:0];
            controller.matchID = match.matchID;
        }
    } else if ([segue.identifier isEqualToString:@"Pickup Match Detail Segue"]) {
        PickupMatchDetailTVC *controller = segue.destinationViewController;
        
        if (self.pageToDisplay == SHOW_UPCOMING_PAGE) {
            PickupMatch *match = [self.upcomingNotifications objectAtIndex:self.selectedIndexPath.section];
            controller.matchID = match.matchID;
        } else if (self.pageToDisplay == SHOW_URGENT_PAGE) {
            PickupMatch *match = [self.alertNotifications objectAtIndex:self.selectedIndexPath.row];
            controller.matchID = match.matchID;
        } else if (self.selectedIndexPath.section == -1) {
            PickupMatch *match = [self.todaysNotifications objectAtIndex:0];
            controller.matchID = match.matchID;
        }
    } else if ([segue.identifier isEqualToString:@"Record Score For Match Segue"]) {
        RecordIndMatchScoreIntroMessage *controller = segue.destinationViewController;
        IndividualMatch *match = [self.alertNotifications objectAtIndex:self.selectedIndexPath.row];
        controller.matchID = match.matchID;
        
    } else if ([segue.identifier isEqualToString:@"Confirm Score For Match Segue"]) {
        ConfirmIndMatchScoreIntroMessage *controller = segue.destinationViewController;
        IndividualMatch *match = [self.alertNotifications objectAtIndex:self.selectedIndexPath.row];
        controller.matchID = match.matchID;
    } else if ([segue.identifier isEqualToString:@"Matches List Segue"]) {
        MatchListTVC *controller = segue.destinationViewController;
        controller.loadMatchType = self.matchListTypeToShowOnNextSegue;
    } else if ([segue.identifier isEqualToString:@"Force Push Record Score For Match Segue"]) {
        RecordIndMatchScoreIntroMessage *controller = segue.destinationViewController;
        controller.matchID = [self.preferences getMatchNotificationsForeSegueMatchID];
        [self.preferences setMatchNotificationsForceSegueWithAction:@"" andMatchID:0]; // reset
    } else if ([segue.identifier isEqualToString:@"Force Push Confirm Score For Match Segue"]) {
        ConfirmIndMatchScoreIntroMessage *controller = segue.destinationViewController;
        controller.matchID = [self.preferences getMatchNotificationsForeSegueMatchID];
        [self.preferences setMatchNotificationsForceSegueWithAction:@"" andMatchID:0]; // reset
    } else if ([segue.identifier isEqualToString:@"Force Push Individual Match Detail Segue"]) {
        IndividualMatchDetailTVC *controller = segue.destinationViewController;
        controller.matchID = [self.preferences getMatchNotificationsForeSegueMatchID];
    } else if ([segue.identifier isEqualToString:@"Force Push Pickup Match Detail Segue"]) {
        PickupMatchDetailTVC *controller = segue.destinationViewController;
        controller.matchID = [self.preferences getMatchNotificationsForeSegueMatchID];
    } else if ([segue.identifier isEqualToString:@"Force Push Pickup Match Detail Segue"]) {
        PickupMatchDetailTVC *controller = segue.destinationViewController;
        controller.matchID = [self.preferences getMatchNotificationsForeSegueMatchID];
    } else if ([segue.identifier isEqualToString:@"New Messages Detail Segue"]) {
        MatchHistoryTVC *controller = segue.destinationViewController;
        MatchPlayerNews *mpn = [self.newMessages objectAtIndex:self.selectedIndexPath.row];
        controller.playerMatch = mpn.match;
    } else if ([segue.identifier isEqualToString:@"Force Push Match Result Level Segue"]) {
        ConfirmIndMatchScoreSuccessVC *controller = segue.destinationViewController;
        controller.matchID = [self.preferences getMatchNotificationsForeSegueMatchID];
    } else if ([segue.identifier isEqualToString:@"Show Match Messages Segue"]) {
        ComposeNewMessageIntroTVC *controller = segue.destinationViewController;
        controller.filterUserID = 0;
    } else if ([segue.identifier isEqualToString:@"Force Push Venue Page Details Segue"]) {
        VenueDetailTVC *controller = segue.destinationViewController;
        controller.venueID = [self.preferences getMatchNotificationsForeSegueVenueID];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
 //   CGFloat fl = 0;
    
    if (self.pageToDisplay == SHOW_URGENT_PAGE) {
        if ([self.alertNotifications count] > 0)
            return 79;
        
    } else if (self.pageToDisplay == SHOW_UPCOMING_PAGE) {
        if ([self.upcomingNotifications count] > 0)
            return 281; // 74
    } else if (self.pageToDisplay == SHOW_HISTORY_PAGE) {
        if (self.numPastMatches > 0)
          //  return 170;
            return 568;
    }
    return 0;
    
   
}



-(void)setRoundedView:(UIView *)roundedView toDiameter:(float)newSize;
{
    CGPoint saveCenter = roundedView.center;
    CGRect newFrame = CGRectMake(roundedView.frame.origin.x, roundedView.frame.origin.y, newSize, newSize);
    roundedView.frame = newFrame;
    roundedView.layer.cornerRadius = newSize / 2.0;
    roundedView.center = saveCenter;
}



- (void)moreAlertMatches
{
    [self performSegueWithIdentifier:@"Record Confirm Score List Segue" sender:self];
}

- (void)moreUpcomingMatches
{
    self.matchListTypeToShowOnNextSegue = LOAD_UPCOMING_MATCHES;
    [self performSegueWithIdentifier:@"Matches List Segue" sender:self];
}

- (void)moreNewMessages
{
    [self performSegueWithIdentifier:@"New Messages Segue" sender:self];
}
- (IBAction)todayMoreButtonAction:(id)sender
{
    self.matchListTypeToShowOnNextSegue = LOAD_TODAYS_MATCHES;
    [self performSegueWithIdentifier:@"Matches List Segue" sender:self];
}

- (void)setBackgroundImage:(NSUInteger)imageIndex
{
 
    
}
- (IBAction)goToFeaturedTodaysEvent:(id)sender
{
    Match *todaysFeaturedMatch = [self.todaysNotifications objectAtIndex:0];
    
    if ([todaysFeaturedMatch isKindOfClass:[IndividualMatch class]]) {
        self.selectedIndexPath = [NSIndexPath indexPathForRow:0 inSection:-1];
        [self performSegueWithIdentifier:@"Individual Match Detail Segue" sender:self];
    } else if ([todaysFeaturedMatch isKindOfClass:[PickupMatch class]]) {
        self.selectedIndexPath = [NSIndexPath indexPathForRow:0 inSection:-1];
        [self performSegueWithIdentifier:@"Pickup Match Detail Segue" sender:self];
    }
}
- (IBAction)viewNewMessages:(id)sender
{
    [self performSegueWithIdentifier:@"New Messages Segue" sender:self];
}

- (void)viewNewMessages2
{
    if (self.numNewMessages > 0)
        [self performSegueWithIdentifier:@"New Messages Segue" sender:self];
    else
        [self performSegueWithIdentifier:@"Show Match Messages Segue" sender:self];
}

- (void)setButtonsNumNewMessages
{
    // Initialize NKNumberBadgeView...
    MKNumberBadgeView *number = [[MKNumberBadgeView alloc] initWithFrame:CGRectMake(17, 00, 30,20)];
    number.value = self.numNewMessages;
    
    if (self.numNewMessages == 0)
        number.hidden = YES;
    
    // Allocate UIButton
    UIButton *btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 30, 30);
    btn.layer.cornerRadius = 8;
    [btn setImage:[[UIImage imageNamed:@"Message In Bar"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]  forState:UIControlStateNormal];
    btn.imageView.tintColor = [UIColor midLightGray];
    
    //  [btn setTitle:@"Button" forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(viewNewMessages2) forControlEvents:UIControlEventTouchUpInside];
    //[btn setBackgroundColor:[UIColor blueColor]];
    [btn setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.1 alpha:0.2]];
    btn.titleLabel.font = [UIFont systemFontOfSize:13];
    //[btn setFont:[UIFont systemFontOfSize:13]];
    [btn addSubview:number]; //Add NKNumberBadgeView as a subview on UIButton
    
    // Initialize UIBarbuttonitem...
    UIBarButtonItem *proe = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = proe;
}

- (void)showTwoMessages:(id)messagesArrayObj
{
    for (id messageObj in messagesArrayObj) {
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *dateTime = [[formatter dateFromString:messageObj[@"ts"]] toLocalTime];
        
        Player *postingPlayer = [[Player alloc] initWithPlayerDetails:[messageObj[@"player_id"] integerValue]
                                                          andUserName:messageObj[@"username"]
                                                  andProfilePicString:nil];
        
        // Only gonna set two values in Individual Match for now
        IndividualMatch *iMatch = [[IndividualMatch alloc] init];
        [iMatch setMatchID:[messageObj[@"match_id"] integerValue]];
        [iMatch setMatchName:messageObj[@"match_name"]];
        
        MatchPlayerNews *mpn = [[MatchPlayerNews alloc] initWithDetails:iMatch
                                                               postDate:dateTime
                                                        headLineMessage:[self.preferences getUserName]
                                                         generalMessage:nil
                                                              forPlayer:postingPlayer
                                                            withMessage:messageObj[@"comment"]];
        [self.newMessages addObject:mpn];
    }
}


- (void)displayErrorOverlay:(NSInteger)pageNum
{
    self.errorOverlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, [UIScreen mainScreen].bounds.size.height)];
    self.errorOverlay.backgroundColor = [UIColor lightLightGray];
    self.errorOverlay.tag = 1;
    
    UILabel *firstLine = [[UILabel alloc] initWithFrame:CGRectMake(30, ([UIScreen mainScreen].bounds.size.height / 2) - 115, 260, 30)];
    firstLine.textColor = [UIColor darkGrayColor];
    firstLine.font = [UIFont systemFontOfSize:23];
    
    UILabel *secondLine = [[UILabel alloc] initWithFrame:CGRectMake(30, ([UIScreen mainScreen].bounds.size.height / 2) - 100, 260, 80)];
    secondLine.textColor = [UIColor lightGrayColor];
    secondLine.font = [UIFont systemFontOfSize:17];
    
    secondLine.numberOfLines = 0;
    
    switch (pageNum) {
        case SHOW_URGENT_PAGE:
            firstLine.text = @"No Urgent Actions";
            secondLine.text = @"You do not have any matches that require action.";
            break;
        case SHOW_UPCOMING_PAGE:
            firstLine.text = @"No Upcoming Matches";
            secondLine.text = @"You do not have any matches coming up.";
            break;
        case SHOW_HISTORY_PAGE:
            firstLine.text = @"No Past Matches";
            secondLine.text = @"You have not played in any matches yet.";
            break;
    }
    [self.errorOverlay addSubview:firstLine];
    [self.errorOverlay addSubview:secondLine];
    
    [self.tableView addSubview:self.errorOverlay];
}



@end
