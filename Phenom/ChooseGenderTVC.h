//
//  ChooseGenderTVC.h
//  Vaiden
//
//  Created by James Chung on 11/7/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomBaseTVC.h"


@class ChooseGenderTVC;

@protocol ChooseGenderTVC_Delegate <NSObject>
- (void)setGenderWithController:(ChooseGenderTVC *)controller
               withGenderString:(NSString *)genderString;
@end

@interface ChooseGenderTVC : CustomBaseTVC


@property (nonatomic, weak) id <ChooseGenderTVC_Delegate> delegate;


@end
