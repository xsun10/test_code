//
//  ShowAlertsMatchCell.h
//  Vaiden
//
//  Created by James Chung on 5/24/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShowAlertsMatchCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *headlineMessage;
@property (nonatomic, weak) IBOutlet UILabel *description;
@property (weak, nonatomic) IBOutlet UIImageView *notePic;
@property (weak, nonatomic) IBOutlet UILabel *sportLabel;
@property (weak, nonatomic) IBOutlet UIButton *goButton;

@end
