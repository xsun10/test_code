//
//  VDCheckInStatsCell.h
//  Vaiden
//
//  Created by James Chung on 5/3/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VDCheckInStatsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *statCheckinsForHourLabel;
@property (weak, nonatomic) IBOutlet UILabel *statCheckinsForHourForFollowingLabel;
@property (weak, nonatomic) IBOutlet UILabel *statCheckinsAllTimeLabel;
@property (weak, nonatomic) IBOutlet UIView *myBackgroundView;
@end
