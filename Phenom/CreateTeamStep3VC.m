//
//  CreateTeamStep3VC.m
//  Vaiden
//
//  Created by James Chung on 4/6/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "CreateTeamStep3VC.h"
#import "CreateTeamStep3IconCell.h"
#import "IOSRequest.h"


@interface CreateTeamStep3VC ()
@property (weak, nonatomic) IBOutlet UICollectionView *clipArtCollectionView;
@property (nonatomic, strong) UIBarButtonItem *doneButton;
@property (nonatomic, strong) UIView *tempView;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIImageView *headlinePic;
@property (nonatomic, strong) NSString *selectedHeadlinePicName;
@property (weak, nonatomic) IBOutlet UIScrollView *pageScrollView;

@end

@implementation CreateTeamStep3VC



- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
    self.clipArtCollectionView.delegate = self;
    self.clipArtCollectionView.dataSource = self;
    
    [self setDoneButton];
    
    [self.clipArtCollectionView reloadData];
    self.selectedHeadlinePicName = [self getTeamClipArtByIndex:0];
    self.headlinePic.image = [UIImage imageNamed:self.selectedHeadlinePicName];
}

- (void)showTempSpinner
{
    self.tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    self.tempView.backgroundColor = [UIColor lightGrayColor];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] init];
    [self.activityIndicator setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width/2.0, [UIScreen mainScreen].bounds.size.height/2.0 )];
    [self.activityIndicator startAnimating];
    [self.tempView addSubview:self.activityIndicator];
    
    [self.view addSubview:self.tempView];
    
}

- (void)setDoneButton
{
    UIButton *btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 50, 21);
    btn.layer.borderColor = [UIColor whiteColor].CGColor;
    btn.layer.borderWidth = 1.0;
    
    [btn addTarget:self action:@selector(done:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundColor:[UIColor clearColor]];
    [btn setTitle:@"DONE" forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-MediumItalic" size:12.0]];
    
    // Initialize UIBarbuttonitem...
    self.doneButton = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = self.doneButton;
}

- (void)done:(UIButton *)sender
{
    self.nTeam.picStringName = self.selectedHeadlinePicName;
    self.doneButton.enabled = NO;
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.labelText = @"Creating";

    [IOSRequest createTeam:self.nTeam onCompletion:^(NSDictionary *results) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            self.doneButton.enabled = YES;
            
            if ([results[@"outcome"] isEqualToString:@"success"]) {
                [self performSegueWithIdentifier:@"Step 4 Create Team Segue" sender:self];
            } else if ([results[@"outcome"] isEqualToString:@"failure"]) {
                UIAlertView *alert = [[UIAlertView alloc] init];
                alert.title = @"Team Creation Error";
                alert.message = @"There was a problem completing your request. Please try again later.";
                [alert addButtonWithTitle:@"OK"];
                [alert show];
            }
            
            
        });
    }];
    
    double delayInSeconds = 10.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        //code to be executed on the main queue after delay
        self.doneButton.enabled = YES;
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    });
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 3;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Art Cell";
    
    CreateTeamStep3IconCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    cell.clipArtPic.image = [UIImage imageNamed:[self getTeamClipArtByIndex:indexPath.row]];
    
    return cell;
}

// then update the main image drawing
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedHeadlinePicName = [self getTeamClipArtByIndex:indexPath.row];
    self.headlinePic.image = [UIImage imageNamed:self.selectedHeadlinePicName];
    
}

- (NSString *)getTeamClipArtByIndex:(NSInteger)myIndex
{
    switch (myIndex) {
        case 0:
            return @"Team CA 1";
            break;
        case 1:
            return @"Team CA 2";
            break;
        case 2:
            return @"Team CA 3";
            break;
        default:
            break;
    }
    return nil;
}

@end
