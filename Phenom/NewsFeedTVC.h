//
//  NewsFeedTVC.h
//  Phenom
//
//  Created by James Chung on 4/23/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "NewsfeedPaginator.h"
#import "CommentsTVC.h"
#import "NewsIndividualMatchPostCell.h"
#import "MatchNewsFinalScoreUpdateCell.h"
#import "MatchNewsScoreUpdateCell.h"
#import "MatchPlayerNews.h"
#import "MatchNewsMatchUpdateCell.h"
#import "MatchNewsPlayerUpdateCell.h"
#import "CustomBaseTVC.h"
#import "NewsPickupMatchPostCell.h"

@interface NewsFeedTVC : CustomBaseTVC <MBProgressHUDDelegate, NMPaginatorDelegate, UIScrollViewDelegate, CommentsTVC_Delegate, NewsIndividualMatchPostCell_delegate, MatchNewsFinalScoreUpdateCell_delegate, MatchNewsScoreUpdateCell_delegate, MatchNewsPlayerUpdateCell_delegate, MatchNewsMatchUpdateCell_delegate, NewsPickupMatchPostCell_delegate>
{
    MBProgressHUD *HUD;
}

@property (nonatomic, strong) NewsfeedPaginator *newsfeedPaginator;
@property (nonatomic, strong) UILabel *footerLabel;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
@property (nonatomic) NSInteger newsfeedDetailID;


@end
