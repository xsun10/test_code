//
//  FBShareRequestDialog.h
//  Vaiden
//
//  Created by Turbo on 7/11/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FBShareRequestDialogDelegate <NSObject>
-(void)startShareImage;
@end

@interface FBShareRequestDialog : UIViewController

@property (nonatomic, strong) UIImage *image;

@property (nonatomic, weak) id <FBShareRequestDialogDelegate>delegate;

@end
