//
//  TextPostCell.m
//  Phenom
//
//  Created by James Chung on 6/3/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "TextPostCell.h"

@implementation TextPostCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.shareIcon.image= [[UIImage imageNamed:@"Speech Bubble"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [self.shareIcon setTintColor:[UIColor lightGrayColor]];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
