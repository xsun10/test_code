//
//  LDTrainingCell.h
//  Vaiden
//
//  Created by James Chung on 6/6/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LDTrainingCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *trainingLabel;

@end
