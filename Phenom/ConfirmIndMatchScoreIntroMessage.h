//
//  ConfirmIndMatchScoreIntroMessage.h
//  Phenom
//
//  Created by James Chung on 7/29/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IndividualMatch.h"
#import "MBProgressHUD.h"
#import "CustomBaseVC.h"

@interface ConfirmIndMatchScoreIntroMessage : CustomBaseVC <MBProgressHUDDelegate>\
{
    MBProgressHUD *HUD;
}

@property (nonatomic) NSUInteger matchID;

@end
