//
//  IMDMatchMessageCell.h
//  Vaiden
//
//  Created by James Chung on 12/26/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IMDMatchMessageCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *profilePic;
@property (weak, nonatomic) IBOutlet UITextView *messageTextView;
@property (weak, nonatomic) IBOutlet UIView *myBackgroundView;
@end
