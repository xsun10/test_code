//
//  AddDirectIndividualMatchTVC.h
//  Phenom
//
//  Created by James Chung on 5/14/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VenuesTVC.h"
#import "RequiredMatchExperienceTVC.h"
#import "ChooseIndividualSportTVC.h"
#import "CustomBaseTVC.h"

@interface AddDirectIndividualMatchTVC : CustomBaseTVC  < VenueTVC_Delegate, ChooseIndividualSportTVC_Delegate>


@property (nonatomic) NSInteger userIDToChallenge;
@property (nonatomic, strong) NSString *usernameToChallenge;
@property (nonatomic, strong) NSString *challengeUserProfileBaseString;

@end
