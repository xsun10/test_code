//
//  TeamMatchDetailTVC.m
//  Vaiden
//
//  Created by James Chung on 4/16/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "TeamMatchDetailTVC.h"
#import "IOSRequest.h"
#import "UserPreferences.h"
#import "MyHelpers.h"
#import "NSDate+Utilities.h"
#import "Venue+MKAnnotation.h"
#import <FacebookSDK/FacebookSDK.h>
#import "MapOverlayVC.h"
#import "S3Tools.h"
#import "AFNetworking.h"
#import "UIImage+ProportionalFill.h"
#import <QuartzCore/QuartzCore.h>
#import "UIImage+ProportionalFill.h"
#import "UIView+Manipulate.h"
#import "UIColor+VaidenColors.h"
#import <EventKit/EventKit.h>
#import "PlayerDetailTVC.h"



@interface TeamMatchDetailTVC () <UIActionSheetDelegate>

@property (nonatomic, strong) UserPreferences *preferences;
@property (nonatomic, strong) TeamMatch *tMatch;
@property (nonatomic) BOOL canShowVenue;
@property (nonatomic) BOOL isVenueTemporary;
@property (nonatomic) BOOL readyToLoadAll;
@property (nonatomic, strong) UIView *tempView;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
@property (nonatomic) NSUInteger pageDisplayConfiguration;
@property (nonatomic) NSUInteger buttonDisplayConfiguration; // the constants are in IMDHeaderInfoCell
@property (weak, nonatomic) IBOutlet UIImageView *mapImage;


@end

@implementation TeamMatchDetailTVC


#define CELL_CONFIG_DEFAULT 0
#define CELL_CONFIG_WITH_NO_SCORE 1 // Do not show score but show match details including status message
#define CELL_CONFIG_WITH_SCORE 2 // Show score, updated levels, and match details but no status message

#define CTE_MATCH_STATUS_CELL 0
#define CTE_HEADER_INFO_CELL 1
#define CTE_MATCH_DETAILS_CELL 2
#define CTE_SESSION_USER_STATUS_CELL 3
#define CTE_LATEST_NEWS_CELL 4
#define CTE_SPORTS_LEVELS_CELL 5
#define CTE_PROBABILITY_CELL 6
#define CTE_MATCH_MESSAGE_CELL 7
#define CTE_FINAL_SCORE_CELL 8

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.readyToLoadAll = NO;
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(refreshView) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshControl];
}

- (void)refreshView
{
    [self loadMatchData];
    [self.refreshControl endRefreshing];
}



- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self loadPage];
    [self refreshView];
    
    //   [self loadMatchData];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if ([[self.preferences getMatchNotificationsForceSegueAction] isEqualToString:@"TH"]) {
        [self.preferences setMatchNotificationsForceSegueWithAction:@"" andMatchID:0];
        [self performSegueWithIdentifier:@"Messages Segue" sender:self];
    } else {
        [self zoomMapViewToFitAnnotations:self.mapView animated:NO];
    }
}

- (void)loadPage
{
    [self.tempView removeFromSuperview];
    self.tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    self.tempView.backgroundColor = [UIColor lightGrayColor];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] init];
    [self.activityIndicator setCenter:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2 - 50)];
    [self.activityIndicator startAnimating];
    [self.tempView addSubview:self.activityIndicator];
    
    [self.view addSubview:self.tempView];
}

- (void)loadMatchData
{
    [IOSRequest fetchTeamMatchDetails:self.matchID
                         onCompletion:^(NSDictionary *object) {
                             
                             // For match history obj match_history_recent_object
                       /*      id historyObj = object[@"match_history_recent_object"];
                             
                             NSDate *historyPostDate = [MyHelpers getDateTimeFromString:historyObj[@"ts"]];
                             
                             if ([historyObj[@"player_id"] integerValue] == 0) {
                                 self.matchNews = [[MatchNews alloc] initWithDetails:self.individualMatch
                                                                            postDate:historyPostDate
                                                                     headLineMessage:self.individualMatch.matchName
                                                                      generalMessage:historyObj[@"general_message"]];
                                 
                             } else {
                                 Player *postPlayer = [[Player alloc] initWithPlayerDetails:[historyObj[@"player_id"] integerValue]
                                                                                andUserName:historyObj[@"username"]];
                                 [postPlayer setProfileBaseString:historyObj[@"profile_pic_string"]];
                                 
                                 self.matchNews = [[MatchPlayerNews alloc] initWithDetails:self.individualMatch
                                                                                  postDate:historyPostDate
                                                                           headLineMessage:historyObj[@"username"]
                                                                            generalMessage:historyObj[@"general_message"]
                                                                                 forPlayer:postPlayer
                                                                               withMessage:historyObj[@"player_message"]];
                             }
                             
                             // End match history object entry
                         */
                             NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
                             [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                             NSDate *dateTime = [[formatter dateFromString:object[@"date_time"]] toLocalTime];
                             
                             Venue *venue = [[Venue alloc] initVenueWithName:object[@"venue_name"]
                                                                  andVenueID:[object[@"venue_id"] integerValue]
                                                                  andAddress:object[@"venue_street"]
                                                                     andCity:object[@"venue_city"]
                                                                andStateName:object[@"venue_state"]
                                                                  andStateID:[object[@"venue_state_id"] integerValue]
                                                                 andLatitude:[object[@"latitude"] floatValue]
                                                                andLongitude:[object[@"longitude"] floatValue]
                                                              andDescription:object[@"venue_description"]
                                                                  andCreator:[object[@"venue_creator_id"] integerValue]];
                             
                             Sport *sport = [[Sport alloc] initWithSportDetails:object[@"sport_name"]
                                                                        andType:object[@"sport_type"]];
                             [sport setSportID:[object[@"sport_id"] integerValue]];
                             
                           
                             id teamObj1 = object[@"teams_array"][0];
                             id teamObj2 = object[@"teams_array"][1];
                             
                             Team *team1 = [[Team alloc] initWithTeamDetails:[teamObj1[@"team_id"] integerValue]
                                                                 andTeamName:teamObj1[@"team_name"]
                                                                  andCaptain:nil
                                                                andPicString:teamObj1[@"logo_string"]];
                             
                             Team *team2 = [[Team alloc] initWithTeamDetails:[teamObj2[@"team_id"] integerValue]
                                                                 andTeamName:teamObj2[@"team_name"]
                                                                  andCaptain:nil
                                                                andPicString:teamObj2[@"logo_string"]];
                             
                             
                             MatchType *mType = [[MatchType alloc] initWithMatchType:[object[@"match_type_id"] integerValue]
                                                                     withDescription:object[@"match_type_description"]];
                             
                             self.tMatch = [[TeamMatch alloc]
                                                     initMatchWithDetails:[object[@"match_id"] integerValue]
                                                     andMatchName:object[@"match_name"]
                                                     isPrivate:[object[@"is_private"] boolValue]
                                                     withSport:sport
                                                     withMessage:object[@"message"]
                                                     onDateTime:dateTime
                                                     atVenue:venue
                                                     createdByUser:[object[@"creator_id"] integerValue]
                                                    withTeam1:team1
                                                withTeam2:team2
                                                andActive:[object[@"active"] boolValue]
                                                andMatchType:mType];
                             
                             
                             
                             [self.tMatch setMatchStatus:object[@"match_status"]];
                         //    [self.individualMatch setPlayerProbabilities:object[@"probabilities"]];
                             
                             /* For Scores */
                             /*
                             id scoreSummaryObject = object[@"score_summary"];
                             
                             for (id roundScoreObj in scoreSummaryObject[@"match_scores"]) {
                                 
                                 // Hack to make sure that player1 user id is same for individualmatch.scoreIndMatch values and individualmatch.player1
                                 // Same thing goes for player2
                                 
                                 if ([roundScoreObj[@"player1_id"] integerValue] == self.individualMatch.player1.userID) {
                                     [self.individualMatch.scoreIndMatch addRoundScore:[roundScoreObj[@"round"] integerValue]
                                                                            forPlayer1:[roundScoreObj[@"player1_id"] integerValue]
                                                                      withScorePlayer1:[roundScoreObj[@"score_player1"] integerValue]
                                                                            forPlayer2:[roundScoreObj[@"player2_id"] integerValue]
                                                                      withScorePlayer2:[roundScoreObj[@"score_player2"] integerValue]];
                                 } else {
                                     [self.individualMatch.scoreIndMatch addRoundScore:[roundScoreObj[@"round"] integerValue]
                                                                            forPlayer1:[roundScoreObj[@"player2_id"] integerValue]
                                                                      withScorePlayer1:[roundScoreObj[@"score_player2"] integerValue]
                                                                            forPlayer2:[roundScoreObj[@"player1_id"] integerValue]
                                                                      withScorePlayer2:[roundScoreObj[@"score_player1"] integerValue]];
                                 }
                                 
                                 
                                 [self.individualMatch.scoreIndMatch setVersion:[scoreSummaryObject[@"version"] integerValue]];
                                 [self.individualMatch.scoreIndMatch setScoreRecordedBy:[scoreSummaryObject[@"recorder_user_id"] integerValue]];
                                 [self.individualMatch.scoreIndMatch setScoreConfirmedBy:[scoreSummaryObject[@"confirmer_user_id"] integerValue]];
                                 [self.individualMatch.scoreIndMatch setScoreDisputedBy:[scoreSummaryObject[@"disputor_user_id"] integerValue]];
                                 [self.individualMatch.scoreIndMatch setWasFirstVersionIndividualScoreRecordedBySessionUserAndDisputedByOtherUser:[scoreSummaryObject[@"was_session_user_score_overruled"] boolValue]];
                                 
                                 
                             }*/
                             
                             /* End For Scores */
                             
                             //   if ([object[@"can_show_venue"] integerValue] == 1)
                             //       self.canShowVenue = YES;
                             //   else
                             //       self.canShowVenue = NO;
                             self.canShowVenue = [object[@"can_show_venue"] boolValue];
                             self.isVenueTemporary = [object[@"is_venue_temporary"] boolValue];
                             
                             dispatch_async(dispatch_get_main_queue(), ^ {
                                 
                                 [self setMap];
                               
                                 [self setDisplayConfiguration]; // new!
                                 [self.activityIndicator stopAnimating];
                                 self.tempView.hidden = YES;
                                 self.readyToLoadAll = YES;
                                 
                                 [self.tableView reloadData];
                          
                             });
                             
                             
                             
                             
                             

                         }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.readyToLoadAll) return 1;
    /*
    if (self.readyToLoadAll) {
        if (self.pageDisplayConfiguration == CELL_CONFIG_WITH_SCORE && [self.tMatch.message length] > 0)
            return 8;
        else if (self.pageDisplayConfiguration == CELL_CONFIG_WITH_SCORE)
            return 7;
        else if (self.pageDisplayConfiguration == CELL_CONFIG_WITH_NO_SCORE && [self.tMatch.message length] > 0)
            return 8;
        else if (self.pageDisplayConfiguration == CELL_CONFIG_WITH_NO_SCORE)
            return 7;
    }*/
    return 0;
}


#define FONT_SIZE 12.0f
#define CELL_CONTENT_WIDTH 260.0f
#define CELL_CONTENT_MARGIN 30.0f

- (CGFloat)getVenueDescriptionContentHeight
{
    // Get a CGSize for the width and, effectively, unlimited height
    CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH - (CELL_CONTENT_MARGIN * 2), 20000.0f);
    // Get the size of the text given the CGSize we just made as a constraint
    CGSize size = [self.tMatch.venue.description sizeWithFont:[UIFont systemFontOfSize:FONT_SIZE] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    // Get the height of our measurement, with a minimum of 71 (standard cell size)
    CGFloat height = MAX(size.height, 65.0f);
    // return the height, with a bit of extra padding in
    return height + (CELL_CONTENT_MARGIN * 2) - 90;
    
}


- (void)setMap
{
    [self.mapView removeAnnotations:self.mapView.annotations];
    [self.mapView addAnnotation:self.tMatch.venue];
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger rowToShow = [self computeCellToDisplay:indexPath.row];
    
    switch (rowToShow) {
       /* case CTE_MATCH_STATUS_CELL:
           
            if (![self.individualMatch isScoreConfirmed]  &&
                [[self.individualMatch getMatchStatusMessage:[self.preferences getUserID]] length] > 0) {
                return 27;
            } else if ([self.individualMatch.scoreIndMatch.roundScoresIndMatch count] == 0  &&
                       [[self.individualMatch getMatchStatusMessage:[self.preferences getUserID]] length] == 0) {
                return 0;
            } else if ([self.individualMatch getWinner].userID == [self.preferences getUserID]) {
                return 56;
            } else if ([self.individualMatch getLoser].userID == [self.preferences getUserID]) {
                return 56;
            } else if ([self.individualMatch isTie]) {
                return 56;
            } else if ([[self.individualMatch getMatchStatusMessage:[self.preferences getUserID]] length] == 0) {
                return 0;
            } else {
                return 27;
            }
            
            break;*/
        case CTE_HEADER_INFO_CELL:
            if (self.buttonDisplayConfiguration == BUTTON_CONFIG_MINIMAL_DISPLAY)
                return 230;
            else
                return 310; // 300
            break;
    /*    case CTE_MATCH_DETAILS_CELL:
            if (self.canShowVenue && !self.isVenueTemporary)
                return 496; // 476
            else if (self.canShowVenue && self.isVenueTemporary) {
                
                return [self getVenueDescriptionContentHeight] + 310; // 290
                
                //    return 340;
            } else
                return 290; // 270
            break;
        case CTE_SESSION_USER_STATUS_CELL:
            
            if ([[self.individualMatch getSessionUserStatusMessage:[self.preferences getUserID]] isEqualToString:@"You did not play in this match"] ||
                ![self.individualMatch canSessionUserJoinMatch:[self.preferences getUserID]])
                return 0;
            else
                return 140; // 119
            break;
        case CTE_LATEST_NEWS_CELL:
            // return 131;
            return 0;
            break;
        case CTE_SPORTS_LEVELS_CELL:
            return 170; // 150
            break;
        case CTE_PROBABILITY_CELL:
            return 153; // 133
            break;
        case CTE_MATCH_MESSAGE_CELL:
            return 231; // 211
            break;
        case CTE_FINAL_SCORE_CELL:
            return 170; // 149
            break;*/
        default:
            break;
    }
    return 0;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{/*
    if([segue.identifier isEqualToString:@"RSVP Individual Match Segue"]){
        RSVPForMatchTVC *controller = (RSVPForMatchTVC *)segue.destinationViewController;
        controller.matchObject = self.individualMatch;
        controller.acceptDecline = self.responseType;
        
    }  else if ([segue.identifier isEqualToString:@"large map view segue"]) {
        MapOverlayVC *controller = (MapOverlayVC *)segue.destinationViewController;
        controller.venue = self.individualMatch.venue;
    } else if ([segue.identifier isEqualToString:@"Match Creator Profile Segue"]) {
        PlayerDetailTVC *controller = (PlayerDetailTVC *)segue.destinationViewController;
        controller.playerUserID = self.individualMatch.matchCreatorID;
        controller.profilePicString = self.individualMatch.matchCreatorProfileBaseString;
    } else if ([segue.identifier isEqualToString:@"Match Comments Segue"]) {
        MatchCommentsTVC *controller = (MatchCommentsTVC *)segue.destinationViewController;
        controller.matchCategory = @"Individual";
        controller.matchID = self.individualMatch.matchID;
        
        // Only players who have confirmed to play in the match can comment in the match
        if ([self checkLoggedInUserConfirmed])
            controller.shouldAllowToComment = YES;
        else
            controller.shouldAllowToComment = NO;
    } else if ([segue.identifier isEqualToString:@"Messages Segue"]) {
        MatchHistoryTVC *controller = segue.destinationViewController;
        controller.playerMatch = self.individualMatch;
    } else if ([segue.identifier isEqualToString:@"Record Score Segue"]) {
        RecordIndMatchScoreIntroMessage *controller = segue.destinationViewController;
        controller.matchID = self.individualMatch.matchID;
    } else if ([segue.identifier isEqualToString:@"Confirm Score Segue"]) {
        ConfirmIndMatchScoreIntroMessage *controller = segue.destinationViewController;
        controller.matchID = self.individualMatch.matchID;
    } else if ([segue.identifier isEqualToString:@"Leaderboard Segue"]) {
        LeaderboardVC *controller = segue.destinationViewController;
        controller.filterSportID = self.individualMatch.sport.sportID;
        controller.filterSportName = self.individualMatch.sport.sportName;
    } else if ([segue.identifier isEqualToString:@"Venue Detail Segue"]) {
        VenueDetailTVC *controller = segue.destinationViewController;
        controller.venueID = self.individualMatch.venue.venueID;
    } else if([segue.identifier isEqualToString:@"Player Detail Segue"]) {
        PlayerDetailTVC *controller = segue.destinationViewController;
        controller.playerUserID = self.selectedPlayerToDisplayDetail;
    }*/
}


- (void)recordScore
{
    [self performSegueWithIdentifier:@"Record Score Segue" sender:self];
}

- (void)confirmScore
{
    [self performSegueWithIdentifier:@"Confirm Score Segue" sender:self];
}

/******* Button Display Conditions ****************/
/**************************************************/

// For the action sheet
#define CANCEL_BUTTON_ACTIONSHEET @"Cancel "

#define TITLE_OF_ACTIONSHEET_FOR_JOIN_MATCH @"By joining you agree to show up to this match on time and to record your score after playing the match"
#define JOIN_MATCH @"Join Match"
#define JOIN_MATCH_ADD_TO_CALENDAR @"Join Match + Add To Calendar"

#define TITLE_OF_ACTIONSHEET_FOR_DECLINE_MATCH @"By declining, you are notifying the other player that you do not wish to play in the match"
#define DECLINE_MATCH @"Decline Match"

#define TITLE_OF_ACTIONSHEET_FOR_CANCEL_MATCH @"By cancelling, you are voiding this match.  Your opponent will be notified of the match cancellation"
#define CANCEL_MATCH @"Cancel Match"

#define TITLE_OF_ACTIONSHEET_FOR_WITHDRAW_MATCH @"By withdrawing, you are notifying the other player that you no longer wish to play in the match"
#define WITHDRAW_MATCH @"Withdraw From Match"

#define TITLE_OF_ACTIONSHEET_FOR_VOID_MATCH @"Voiding this match will render it inactive"
#define VOID_MATCH @"Void Match"

#define TITLE_OF_ACTIONSHEET_FOR_DISPUTE_SCORE @"Disputing the score will void the match.  The score will not be recorded and your sports level will not change."
#define DISPUTE_SCORE @"Dispute Score"



- (void)showActionsheet:(NSString *)actionType
{
    if ([actionType isEqualToString:JOIN_MATCH]) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                      initWithTitle:TITLE_OF_ACTIONSHEET_FOR_JOIN_MATCH delegate:self
                                      cancelButtonTitle:CANCEL_BUTTON_ACTIONSHEET destructiveButtonTitle:nil
                                      otherButtonTitles:JOIN_MATCH_ADD_TO_CALENDAR, JOIN_MATCH,  nil];
        [actionSheet showFromTabBar:self.tabBarController.tabBar];
        
    } else if ([actionType isEqualToString:DECLINE_MATCH]) {
        
        UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                      initWithTitle:TITLE_OF_ACTIONSHEET_FOR_DECLINE_MATCH delegate:self
                                      cancelButtonTitle:CANCEL_BUTTON_ACTIONSHEET destructiveButtonTitle:nil
                                      otherButtonTitles:DECLINE_MATCH,  nil];
        [actionSheet showFromTabBar:self.tabBarController.tabBar];
        
    } else if ([actionType isEqualToString:CANCEL_MATCH]) {
        
        UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                      initWithTitle:TITLE_OF_ACTIONSHEET_FOR_CANCEL_MATCH delegate:self
                                      cancelButtonTitle:CANCEL_BUTTON_ACTIONSHEET destructiveButtonTitle:nil
                                      otherButtonTitles:CANCEL_MATCH,  nil];
        [actionSheet showFromTabBar:self.tabBarController.tabBar];
        
    } else if ([actionType isEqualToString:WITHDRAW_MATCH]) {
        
        UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                      initWithTitle:TITLE_OF_ACTIONSHEET_FOR_WITHDRAW_MATCH delegate:self
                                      cancelButtonTitle:CANCEL_BUTTON_ACTIONSHEET destructiveButtonTitle:nil
                                      otherButtonTitles:WITHDRAW_MATCH,  nil];
        [actionSheet showFromTabBar:self.tabBarController.tabBar];
        
    } else if ([actionType isEqualToString:VOID_MATCH]) {
        
        UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                      initWithTitle:TITLE_OF_ACTIONSHEET_FOR_VOID_MATCH delegate:self
                                      cancelButtonTitle:CANCEL_BUTTON_ACTIONSHEET destructiveButtonTitle:nil
                                      otherButtonTitles:VOID_MATCH,  nil];
        [actionSheet showFromTabBar:self.tabBarController.tabBar];
        
    } else if ([actionType isEqualToString:DISPUTE_SCORE]) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                      initWithTitle:TITLE_OF_ACTIONSHEET_FOR_DISPUTE_SCORE delegate:self
                                      cancelButtonTitle:CANCEL_BUTTON_ACTIONSHEET
                                      destructiveButtonTitle:nil
                                      otherButtonTitles:DISPUTE_SCORE, nil];
        [actionSheet showFromTabBar:self.tabBarController.tabBar];
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == actionSheet.destructiveButtonIndex) {
        [self.view.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    } else {
        NSString *choice = [actionSheet buttonTitleAtIndex:buttonIndex];
        
        if ([choice isEqualToString:VOID_MATCH]) {
            [self setMatchAsNeverOccurred];
        } else if ([choice isEqualToString:JOIN_MATCH]) {
            [self joinMatchAction];
        } else if ([choice isEqualToString:JOIN_MATCH_ADD_TO_CALENDAR]) {
            [self addMatchToCalendar];
        } else if ([choice isEqualToString:CANCEL_MATCH]) {
            [self cancelMatchAction];
        } else if ([choice isEqualToString:DECLINE_MATCH]) {
            [self declineMatchAction];
        } else if ([choice isEqualToString:WITHDRAW_MATCH]) {
            [self withdrawRSVP];
        } else if ([choice isEqualToString:DISPUTE_SCORE]) {
            [self setMatchAsDisputed];
        }
    }
}



- (void)cancelMatchAction
{
    //    self.responseType = @"Cancel";
    //    [self performSegueWithIdentifier:@"RSVP Individual Match Segue" sender:self];
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.labelText = @"Cancelling Match";
    [IOSRequest cancelMatchWithID:self.tMatch.matchID
                           byUser:[self.preferences getUserID]
                         withType:@"individual" onCompletion:^(NSDictionary *results) {
                             if ([results[@"error_code"] isEqualToString:@"NOT_MATCH_CREATOR"]) {
                                 dispatch_async(dispatch_get_main_queue(), ^{
                                     [MBProgressHUD hideHUDForView:self.view animated:YES];
                                     [self.preferences setNewsfeedRefreshState:YES];
                                     [self.preferences setMatchNotificationRefreshState:YES];
                                     UIAlertView *alert = [[UIAlertView alloc] init];
                                     alert.title = @"Not Match Creator";
                                     alert.message = @"You are not the match creator.  You cannot cancel this match.";
                                     [alert addButtonWithTitle:@"OK"];
                                     [alert show];
                                     [self.navigationController popViewControllerAnimated:YES];
                                 });
                             } else if ([results[@"error_code"] isEqualToString:@"PAST_MATCH"]) {
                                 dispatch_async(dispatch_get_main_queue(), ^{
                                     [MBProgressHUD hideHUDForView:self.view animated:YES];
                                     [self.preferences setNewsfeedRefreshState:YES];
                                     [self.preferences setMatchNotificationRefreshState:YES];
                                     UIAlertView *alert = [[UIAlertView alloc] init];
                                     alert.title = @"Match Expired";
                                     alert.message = @"You cannot cancel this match because it has expired";
                                     [alert addButtonWithTitle:@"OK"];
                                     [alert show];
                                     [self.navigationController popViewControllerAnimated:YES];
                                 });
                             } else {
                                 dispatch_async(dispatch_get_main_queue(), ^{
                                     [self loadPage];
                                     [self loadMatchData];
                                     [MBProgressHUD hideHUDForView:self.view animated:YES];
                                     //           self.response = CANCELLED_MATCH_INDIVIDUALMATCH;
                                     [self.preferences setMatchNotificationRefreshState:YES];
                                     [self.preferences setNewsfeedRefreshState:YES];
                                 });
                             }
                         }];
    //   double delayInSeconds = 3.0;
    //   dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    //   dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
    //code to be executed on the main queue after delay
    //      [MBProgressHUD hideHUDForView:self.view animated:YES];// just in case
    //  });
}

- (void)joinMatchAction
{
    //    self.responseType = @"Accept";
    //    [self performSegueWithIdentifier:@"RSVP Individual Match Segue" sender:self];
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.labelText = @"Submitting RSVP";
    [IOSRequest rsvpForIndividualMatch:self.tMatch.matchID
                              withRSVP:2
                            andComment:@""
                               forUser:[self.preferences getUserID]
                          onCompletion:^(NSDictionary * results) {
                              
                              if ([results[@"outcome"] isEqualToString:@"failure"]) {
                                  UIAlertView *alert = [[UIAlertView alloc] init];
                                  alert.title = @"RSVP Error";
                                  alert.message = @"Sorry but there was a problem in submitting your request.  Please retry.";
                                  [alert addButtonWithTitle:@"OK"];
                                  [alert show];
                              } else if ([results[@"error_code"] isEqualToString:@"BAD_PERMISSIONS"]) {
                                  dispatch_async(dispatch_get_main_queue(), ^{
                                      [MBProgressHUD hideHUDForView:self.view animated:YES];
                                      [self.preferences setNewsfeedRefreshState:YES];
                                      [self.preferences setMatchNotificationRefreshState:YES];
                                      UIAlertView *alert = [[UIAlertView alloc] init];
                                      alert.title = @"RSVP Error";
                                      alert.message = @"Sorry but you cannot RSVP at this time";
                                      [alert addButtonWithTitle:@"OK"];
                                      [alert show];
                                      [self.navigationController popViewControllerAnimated:YES];
                                  });
                              } else if ([results[@"error_code"] isEqualToString:@"PAST_MATCH"]) {
                                  dispatch_async(dispatch_get_main_queue(), ^{
                                      [MBProgressHUD hideHUDForView:self.view animated:YES];
                                      [self.preferences setNewsfeedRefreshState:YES];
                                      [self.preferences setMatchNotificationRefreshState:YES];
                                      UIAlertView *alert = [[UIAlertView alloc] init];
                                      alert.title = @"Match Expired";
                                      alert.message = @"You cannot RSVP for this match because it has expired";
                                      [alert addButtonWithTitle:@"OK"];
                                      [alert show];
                                      [self.navigationController popViewControllerAnimated:YES];
                                  });
                              } else {
                                  dispatch_async(dispatch_get_main_queue(), ^{
                                      [self loadPage];
                                      [self loadMatchData];
                                      [MBProgressHUD hideHUDForView:self.view animated:YES];
                                      //                                     self.response = ACCEPTED_RSVP_INDIVIDUALMATCH;
                                      [self.preferences setMatchNotificationRefreshState:YES];
                                      [self.preferences setNewsfeedRefreshState:YES];
                                  });
                              }
                          }];
    
}

- (void)declineMatchAction
{
    //    self.responseType = @"Decline";
    //    [self performSegueWithIdentifier:@"RSVP Individual Match Segue" sender:self];
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.labelText = @"Submitting RSVP";
    [IOSRequest rsvpForIndividualMatch:self.tMatch.matchID
                              withRSVP:3
                            andComment:@""
                               forUser:[self.preferences getUserID]
                          onCompletion:^(NSDictionary * results) {
                              if ([results[@"error_code"] isEqualToString:@"BAD_PERMISSIONS"]) {
                                  dispatch_async(dispatch_get_main_queue(), ^{
                                      [MBProgressHUD hideHUDForView:self.view animated:YES];
                                      [self.preferences setNewsfeedRefreshState:YES];
                                      [self.preferences setMatchNotificationRefreshState:YES];
                                      UIAlertView *alert = [[UIAlertView alloc] init];
                                      alert.title = @"RSVP Error";
                                      alert.message = @"Sorry but you cannot RSVP at this time";
                                      [alert addButtonWithTitle:@"OK"];
                                      [alert show];
                                      [self.navigationController popViewControllerAnimated:YES];
                                  });
                              } else if ([results[@"error_code"] isEqualToString:@"PAST_MATCH"]) {
                                  dispatch_async(dispatch_get_main_queue(), ^{
                                      [MBProgressHUD hideHUDForView:self.view animated:YES];
                                      [self.preferences setNewsfeedRefreshState:YES];
                                      [self.preferences setMatchNotificationRefreshState:YES];
                                      UIAlertView *alert = [[UIAlertView alloc] init];
                                      alert.title = @"Match Expired";
                                      alert.message = @"You cannot RSVP for this match because it has expired";
                                      [alert addButtonWithTitle:@"OK"];
                                      [alert show];
                                      [self.navigationController popViewControllerAnimated:YES];
                                  });
                              } else {
                                  
                                  dispatch_async(dispatch_get_main_queue(), ^{
                                      [self loadPage];
                                      [self loadMatchData];
                                      [MBProgressHUD hideHUDForView:self.view animated:YES];
                                      //                                      self.response = DECLINED_RSVP_INDIVIDUALMATCH;
                                      [self.preferences setMatchNotificationRefreshState:YES];
                                      [self.preferences setNewsfeedRefreshState:YES];
                                      
                                  });
                              }
                          }];
    
}

- (void)withdrawRSVP
{
    //    self.responseType = @"Withdraw";
    //    [self performSegueWithIdentifier:@"RSVP Individual Match Segue" sender:self];
    [self declineMatchAction];
}

- (void)matchNewsAction
{
    [self performSegueWithIdentifier:@"Messages Segue" sender:self];
}

- (void)setMatchAsNeverOccurred
{
    HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    HUD.labelText = @"Updating Match Status";
    [IOSRequest setIndividualMatchAsNeverOccurred:self.tMatch.matchID
                                           byUser:[self.preferences getUserID]
                                     onCompletion:^(NSDictionary *results) {
                                         
                                         dispatch_async(dispatch_get_main_queue(), ^{
                                             [self loadPage];
                                             [self loadMatchData];
                                             [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
                                         });
                                     }];
}

- (void)setMatchAsDisputed
{
    HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    HUD.labelText = @"Updating Match Status";
    [IOSRequest setMatchAsDisputed:self.tMatch.matchID
                        disputedBy:[self.preferences getUserID]
                      onCompletion:^(NSDictionary *results) {
                          
                          dispatch_async(dispatch_get_main_queue(), ^{
                              [MBProgressHUD hideHUDForView:self.view animated:YES];
                              
                              if ([results[@"outcome"] isEqualToString:@"success"]) {
                                  [self.preferences setMatchNotificationRefreshState:YES];
                                  [self.preferences setNewsfeedRefreshState:YES];
                                  
                                  [self loadPage];
                                  [self loadMatchData];
                                  [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
                                  
                                  
                                  //     [self performSegueWithIdentifier:@"Excessive Dispute Segue" sender:self];
                                  
                              } else {
                                  UIAlertView *alert = [[UIAlertView alloc] init];
                                  alert.title = @"Dispute Score Error";
                                  alert.message = @"There was a problem in submitting your request.  Please try again later.";
                                  [alert addButtonWithTitle:@"OK"];
                                  [alert show];
                              }
                              
                          });
                          
                      }];
}

- (void)addMatchToCalendar
{
    EKEventStore *eventStore = [[EKEventStore alloc] init];
    
    [eventStore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
        
        EKEvent *event  = [EKEvent eventWithEventStore:eventStore];
        event.title     = self.tMatch.matchName;
        event.notes     = [NSString stringWithFormat:@"%@ Challenge Match with %@", [self.tMatch.sport.sportName capitalizedString], [[self.tMatch getOtherTeamThatDoesntHaveCaptain:[self.preferences getUserID]] teamName]];
        event.startDate = self.tMatch.dateTime;
        event.endDate   = [[NSDate alloc] initWithTimeInterval:3600 sinceDate:self.tMatch.dateTime];
        event.location = self.tMatch.venue.venueName;
        
        [event setCalendar:[eventStore defaultCalendarForNewEvents]];
        NSError *err;
        [eventStore saveEvent:event span:EKSpanThisEvent error:&err];
        
        [self joinMatchAction];
        
    }];
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger selectedCode = [self computeCellToDisplay:indexPath.row];
    
    if (selectedCode == CTE_HEADER_INFO_CELL) {
        
    }
    
    if (selectedCode == CTE_HEADER_INFO_CELL) {
        static NSString *CellIdentifier = @"Header Cell";
        TMDHeaderInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        
        cell.delegate = self;
        
        cell.matchNameLabel.text = self.tMatch.matchName;
        cell.vsLabel.text = [NSString stringWithFormat:@"%@ vs %@", self.tMatch.team1.teamName, self.tMatch.team2.teamName];
        
        // player 1 image
    /*
        NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                baseString:self.individualMatch.player1.profileBaseString];
        
        [cell.player1Pic setImageWithURL:[NSURL URLWithString:url]
                        placeholderImage:[UIImage imageNamed:@"avatar round"]];
        
        [UIImage makeRoundedImage:cell.player1Pic withRadius:35];
        
        [cell.player1Pic.layer setBorderColor:[UIColor whiteColor].CGColor];
        [cell.player1Pic.layer setBorderWidth:1.0f];
        
        UIButton *player1PicButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 70, 70)];
        [player1PicButton addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
        player1PicButton.tag = self.individualMatch.player1.userID;
        [cell.player1Pic addSubview:player1PicButton];
        cell.player1Pic.userInteractionEnabled = YES;
        
        
        // player 2 image
        
        NSString *url2 = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                 baseString:self.individualMatch.player2.profileBaseString];
        
        [cell.player2Pic setImageWithURL:[NSURL URLWithString:url2]
                        placeholderImage:[UIImage imageNamed:@"avatar round"]];
        
        [UIImage makeRoundedImage:cell.player2Pic withRadius:35];
        
        [cell.player2Pic.layer setBorderColor:[UIColor whiteColor].CGColor];
        [cell.player2Pic.layer setBorderWidth:1.0f];
        
        UIButton *player2PicButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 70, 70)];
        [player2PicButton addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
        player2PicButton.tag = self.individualMatch.player2.userID;
        [cell.player2Pic addSubview:player2PicButton];
        cell.player2Pic.userInteractionEnabled = YES;*/
        
        cell.sportLabel.text = [NSString stringWithFormat:@"%@ Match", [self.tMatch.sport.sportName capitalizedString]];
   /*     [cell configureButtonDisplay:self.buttonDisplayConfiguration];
        
        cell.sportIcon.image = [[self.tMatch.sport getSportIcon] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        cell.sportIcon.tintColor = [UIColor whiteColor];
        
        cell.backgroundPic.image = [self.individualMatch getMatchDetailPageBackground];
        [cell.buttonCollectionView reloadData];
        */
        return cell;
    } /* else if (selectedCode == CTE_MATCH_DETAILS_CELL) {
        static NSString *CellIdentifier = @"Match Details Cell";
        IMDMatchDetailsCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        
        NSString *dateText;
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"MMMM d, YYYY"];
        dateText = [dateFormatter stringFromDate:self.individualMatch.dateTime];
        
        NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
        [timeFormatter setDateFormat:@"h:mm a"];
        
        cell.dateLabel.text = dateText;
        cell.timeLabel.text = [timeFormatter stringFromDate:self.individualMatch.dateTime];
        
        [cell.dateTimeIconBackground makeCircleWithColorAndBackground:[UIColor peacock] andRadius:12];
        cell.dateTimeIconImage.image = [[UIImage imageNamed:@"Calendar Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        cell.dateTimeIconImage.tintColor = [UIColor whiteColor];
        
        [cell.locationIconBackground makeCircleWithColorAndBackground:[UIColor sienna] andRadius:12];
        cell.locationIconImage.image = [[UIImage imageNamed:@"Location Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        cell.locationIconImage.tintColor = [UIColor whiteColor];
        
        if (self.canShowVenue && !self.isVenueTemporary) {
            cell.venueNameLabel.text = self.individualMatch.venue.venueName;
            
            if ([self.individualMatch.venue.streetAddress length] > 0) {
                cell.streetNameLabel.text = self.individualMatch.venue.streetAddress;
                
                if ([self.individualMatch.venue.city length] > 0 && [self.individualMatch.venue.stateName length] > 0) {
                    cell.cityStateLabel.text = [NSString stringWithFormat:@"%@, %@", self.individualMatch.venue.city, self.individualMatch.venue.stateName ];
                } else if ([self.individualMatch.venue.city length] > 0 && [self.individualMatch.venue.stateName length] == 0) {
                    cell.cityStateLabel.text = self.individualMatch.venue.city;
                } else if ([self.individualMatch.venue.city length] == 0 && [self.individualMatch.venue.stateName length] > 0) {
                    cell.cityStateLabel.text = self.individualMatch.venue.stateName;
                }
            } else {
                if ([self.individualMatch.venue.city length] > 0 && [self.individualMatch.venue.stateName length] > 0) {
                    cell.streetNameLabel.text = [NSString stringWithFormat:@"%@, %@", self.individualMatch.venue.city, self.individualMatch.venue.stateName ];
                } else if ([self.individualMatch.venue.city length] > 0 && [self.individualMatch.venue.stateName length] == 0) {
                    cell.streetNameLabel.text = self.individualMatch.venue.city;
                } else if ([self.individualMatch.venue.city length] == 0 && [self.individualMatch.venue.stateName length] > 0) {
                    cell.streetNameLabel.text = self.individualMatch.venue.stateName;
                } else {
                    cell.streetNameLabel.text = [NSString stringWithFormat:@"Longitude %f", self.individualMatch.venue.longitude];
                    cell.cityStateLabel.text = [NSString stringWithFormat:@"Latitude %f", self.individualMatch.venue.latitude];
                }
                
            }
            
            
            
            //   if (self.individualMatch.venue.venueImageThumb == nil) {
            NSArray *annotations = self.mapView.annotations;
            CLLocationCoordinate2D coordinate = [(id <MKAnnotation>)annotations[0] coordinate];
            NSString *staticMapUrl = [NSString stringWithFormat:@"http://maps.google.com/maps/api/staticmap?markers=color:red|%f,%f&%@&sensor=true",coordinate.latitude, coordinate.longitude,@"zoom=16&size=320x158"];
            NSURL *mapUrl = [NSURL URLWithString:[staticMapUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
            cell.venueImage.image = [UIImage imageWithData: [NSData dataWithContentsOfURL:mapUrl]];
            cell.venueImage.userInteractionEnabled = YES;
            UITapGestureRecognizer *pgr = [[UITapGestureRecognizer alloc]
                                           initWithTarget:self action:@selector(handleTap:)];
            //    pgr.delegate = self.view;
            [cell.venueImage addGestureRecognizer:pgr];
        } else if (self.canShowVenue && self.isVenueTemporary)  {
            cell.venueImage.hidden = YES;
            cell.venueNameLabel.hidden = YES;
            cell.streetNameLabel.hidden = YES;
            cell.cityStateLabel.hidden = YES;
            cell.detailsButton.hidden = YES;
            
            UILabel *venueName = [[UILabel alloc] initWithFrame:CGRectMake(30, 200, 260, 25)];
            venueName.text = self.individualMatch.venue.venueName;
            venueName.font = [UIFont systemFontOfSize:20];
            venueName.textColor = [UIColor darkGrayColor];
            
            UILabel *cityState = [[UILabel alloc] initWithFrame:CGRectMake(30, 225, 260, 21)];
            cityState.text = [NSString stringWithFormat:@"%@, %@", self.individualMatch.venue.city, self.individualMatch.venue.stateName];
            cityState.font = [UIFont systemFontOfSize:14];
            cityState.textColor = [UIColor lightGrayColor];
            
            UILabel *venueDescriptionTitle = [[UILabel alloc] initWithFrame:CGRectMake(30, 255, 260, 20)];
            venueDescriptionTitle.text = @"Venue Description";
            venueDescriptionTitle.font = [UIFont systemFontOfSize:15];
            venueDescriptionTitle.textColor = [UIColor darkGrayColor];
            
            UILabel *venueDescription = [[UILabel alloc] initWithFrame:CGRectMake(30, 275, 260, [self getVenueDescriptionContentHeight])];
            venueDescription.text = self.individualMatch.venue.description;
            venueDescription.font = [UIFont systemFontOfSize:FONT_SIZE];
            venueDescription.textColor = [UIColor lightGrayColor];
            venueDescription.numberOfLines = 0;
            venueDescription.lineBreakMode = NSLineBreakByWordWrapping;
            
            
            
            [cell addSubview:venueName];
            [cell addSubview:cityState];
            [cell addSubview:venueDescriptionTitle];
            [cell addSubview:venueDescription];
            
        } else {
            // session user must not have rights to view the location so hide it.
            cell.venueImage.hidden = YES;
            cell.venueNameLabel.hidden = YES;
            cell.streetNameLabel.hidden = YES;
            cell.cityStateLabel.hidden = YES;
            cell.detailsButton.hidden = YES;
            
            UILabel *tempLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 200, 280, 40)];
            tempLabel.text = @"This venue is private and can only be viewed by match participants";
            tempLabel.font = [UIFont systemFontOfSize:13];
            tempLabel.textColor = [UIColor midGray];
            tempLabel.textAlignment = NSTextAlignmentCenter;
            tempLabel.numberOfLines = 2;
            
            [cell addSubview:tempLabel];
            
            
        }
        
        [cell.myBackgroundView makeEdgyDropshadow:[UIColor whiteColor]];
        
        return cell;
        
        
    } else if (selectedCode == CTE_SESSION_USER_STATUS_CELL) {
        static NSString *CellIdentifier = @"Status Cell";
        IMDSessionUserStatusCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        
        [cell.statusIconBackground makeCircleWithColorAndBackground:[UIColor purpleColor] andRadius:12];
        cell.statusIconImage.image = [[UIImage imageNamed:@"Bulb Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        cell.statusIconImage.tintColor = [UIColor whiteColor];
        
        NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                baseString:[self.preferences getProfilePicString]];
        
        [cell.profilePic setImageWithURL:[NSURL URLWithString:url]
                        placeholderImage:[UIImage imageNamed:@"avatar round"]];
        
        [UIImage makeRoundedImage:cell.profilePic withRadius:20];
        
        UIButton *playerPicButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
        [playerPicButton addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
        playerPicButton.tag = self.individualMatch.player1.userID;
        [cell.profilePic addSubview:playerPicButton];
        cell.profilePic.userInteractionEnabled = YES;
        
        
        [cell.myBackgroundView makeEdgyDropshadow:[UIColor whiteColor]];
        
        
        cell.playStatusLabel.text = [self.individualMatch getSessionUserStatusMessage:[self.preferences getUserID]];
        
        
        return cell;
        
    } else if (selectedCode == CTE_MATCH_STATUS_CELL) {
        
        if (![self.individualMatch isScoreConfirmed]) {
            static NSString *CellIdentifier = @"Match Status Cell";
            IMDMatchStatusCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
            
            [cell.circleIconView makeCircleWithColorAndBackground:[UIColor whiteColor] andRadius:5];
            cell.matchStatusLabel.text = [self.individualMatch getMatchStatusMessage:[self.preferences getUserID]];
            
            return  cell;
            
        } else if ([self.individualMatch getWinner].userID == [self.preferences getUserID]) {
            static NSString *CellIdentifier = @"Match Outcome Cell";
            IMDOutcomeStatusCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
            cell.outcomeLabel.text = @"You Won!";
            return cell;
        } else if ([self.individualMatch getLoser].userID == [self.preferences getUserID]) {
            static NSString *CellIdentifier = @"Match Outcome Cell";
            IMDOutcomeStatusCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
            cell.outcomeLabel.text = @"You Lost";
            return cell;
        } else if ([self.individualMatch isTie]) {
            static NSString *CellIdentifier = @"Match Outcome Cell";
            IMDOutcomeStatusCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
            cell.outcomeLabel.text = @"It's a tie!";
            return cell;
        } else {
            static NSString *CellIdentifier = @"Match Status Cell";
            IMDMatchStatusCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
            
            //  [cell.circleIconView makeCircleWithColorAndBackground:[UIColor salmonColor] andRadius:5];
            cell.matchStatusLabel.text = [self.individualMatch getMatchStatusMessage:[self.preferences getUserID]];
            
            return  cell;
        }
    } else if (selectedCode == CTE_LATEST_NEWS_CELL) {
        static NSString *CellIdentifier = @"News Cell";
        IMDTheLatestNewsCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        
        [cell.newsIconBackground makeCircleWithColorAndBackground:[UIColor redColor] andRadius:12];
        
        cell.newsIconPic.image = [[UIImage imageNamed:@"news tab"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        cell.newsIconPic.tintColor = [UIColor whiteColor];
        
        NSDateFormatter *historyDateFormatter = [[NSDateFormatter alloc]init];
        [historyDateFormatter setDateFormat:@"MMMM d, YYYY h:mm a"];
        cell.datetimeLabel.text = [historyDateFormatter stringFromDate:self.matchNews.newsDate];
        
        if ([self.matchNews isKindOfClass:[MatchPlayerNews class]]) {
            MatchPlayerNews *mpn = (MatchPlayerNews *)self.matchNews;
            cell.nameLabel.text = mpn.player.userName;
            NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                    baseString:mpn.player.profileBaseString];
            
            [cell.picImage setImageWithURL:[NSURL URLWithString:url]
                          placeholderImage:[UIImage imageNamed:@"avatar round"]];
            [UIImage makeRoundedImage:cell.picImage withRadius:20];
            
            cell.subheadlineLabel.text = mpn.matchGeneralMessage;
            
            
        } else {
            cell.picImage.image = [self.individualMatch.sport getSportIcon];
            cell.subheadlineLabel.text = self.matchNews.matchGeneralMessage;
            cell.nameLabel.text = self.individualMatch.matchName;
            
        }
        return cell;
    } else if (selectedCode == CTE_SPORTS_LEVELS_CELL) {
        static NSString *CellIdentifier = @"Level Cell";
        IMDSportsLevelsCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        
        MatchPlayer *player1 = self.individualMatch.player1;
        
        cell.player1LevelLabel.text = [self getLevelForPlayer:player1 withLabel:1];
        cell.player1UsernameLabel.text = player1.userName;
        
        MatchPlayer *player2 = self.individualMatch.player2;
        //  self.user2Username.text = player2.userName;
        cell.player2LevelLabel.text = [self getLevelForPlayer:player2 withLabel:2];
        cell.player2UsernameLabel.text = player2.userName;
        
        cell.sportIcon.image = [self.individualMatch.sport getSportIcon];
        cell.sportLabel.text = [self.individualMatch.sport.sportName capitalizedString];
        
        [cell.sportsLevelPicBackground makeCircleWithColorAndBackground:[UIColor peacock] andRadius:12];
        
        cell.sportsLevelPic.image = [[UIImage imageNamed:@"Level Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        cell.sportsLevelPic.tintColor = [UIColor whiteColor];
        
        [cell.myBackgroundView makeEdgyDropshadow:[UIColor whiteColor]];
        
        
        return cell;
    } else if (selectedCode == CTE_PROBABILITY_CELL) {
        static NSString *CellIdentifier = @"Probability Cell";
        IMDProbabilityCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        
        NSString *probString;
        NSInteger winnerID;
        
        if (self.individualMatch.probabilityPlayer1Wins > self.individualMatch.probabilityPlayer2Wins) {
            probString = self.individualMatch.player1.profileBaseString;
            winnerID = self.individualMatch.player1.userID;
            cell.numProbabilityLabel.text = [NSString stringWithFormat:@"%d%%", (int)(self.individualMatch.probabilityPlayer1Wins * 100)];
        } else if (self.individualMatch.probabilityPlayer2Wins > self.individualMatch.probabilityPlayer1Wins) {
            probString = self.individualMatch.player2.profileBaseString;
            winnerID = self.individualMatch.player2.userID;
            cell.numProbabilityLabel.text = [NSString stringWithFormat:@"%d%%", (int)(self.individualMatch.probabilityPlayer2Wins * 100)];
        } else {
            probString = self.individualMatch.player1.profileBaseString;
            winnerID = self.individualMatch.player1.userID;
            cell.numProbabilityLabel.text = [NSString stringWithFormat:@"%d%%", (int)(self.individualMatch.probabilityPlayer1Wins * 100)];
        }
        
        NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                baseString:probString];
        
        [cell.winnerPic setImageWithURL:[NSURL URLWithString:url]
                       placeholderImage:[UIImage imageNamed:@"avatar round"]];
        
        [UIImage makeRoundedImage:cell.winnerPic withRadius:20];
        
        UIButton *playerPicButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
        [playerPicButton addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
        playerPicButton.tag = winnerID;
        [cell.winnerPic addSubview:playerPicButton];
        cell.winnerPic.userInteractionEnabled = YES;
        
        [cell.probabilityIconPicBackground makeCircleWithColorAndBackground:[UIColor lightGrayColor] andRadius:12];
        
        cell.probabilityIconPic.image = [[UIImage imageNamed:@"Rate Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        cell.probabilityIconPic.tintColor = [UIColor whiteColor];
        [cell.myBackgroundView makeEdgyDropshadow:[UIColor whiteColor]];
        
        return cell;
    } else if (selectedCode == CTE_MATCH_MESSAGE_CELL) {
        static NSString *CellIdentifier = @"Match Message Cell";
        IMDMatchMessageCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        
        NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                baseString:self.individualMatch.matchCreatorProfileBaseString];
        [cell.profilePic setImageWithURL:[NSURL URLWithString:url]
                        placeholderImage:[UIImage imageNamed:@"avatar round"]];
        [UIImage makeRoundedImage:cell.profilePic withRadius:20];
        
        cell.messageTextView.text = self.individualMatch.message;
        [cell.myBackgroundView makeEdgyDropshadow:[UIColor whiteColor]];
        
        return cell;
    } else if (selectedCode == CTE_FINAL_SCORE_CELL) {
        static NSString *CellIdentifier = @"Score Cell";
        IMDFinalScoreCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        
        [cell.iconBackground makeCircleWithColorAndBackground:[UIColor lightGreenColor] andRadius:12];
        
        cell.iconPic.image = [[UIImage imageNamed:@"Digits Display"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        cell.iconPic.tintColor = [UIColor whiteColor];
        
        cell.individualMatch = self.individualMatch;
        cell.player1ID = self.individualMatch.player1.userID;
        cell.player2ID = self.individualMatch.player2.userID;
        
        cell.usernameLabel1.text = self.individualMatch.player1.userName;
        cell.usernameLabel2.text = self.individualMatch.player2.userName;
        [cell.myBackgroundView makeEdgyDropshadow:[UIColor whiteColor]];
        
        [cell.scoreCV reloadData];
        
        return cell;
    }*/
    
    return nil;
}

- (NSUInteger)computeCellToDisplay:(NSUInteger)indexRow
{
    NSUInteger displayCell = 0;
    
    if (self.pageDisplayConfiguration == CELL_CONFIG_WITH_NO_SCORE) {
        
        switch(indexRow) {
            case 0:
                displayCell = CTE_MATCH_STATUS_CELL;
                break;
            case 1:
                displayCell = CTE_HEADER_INFO_CELL;
                break;
            case 2:
                displayCell = CTE_MATCH_DETAILS_CELL;
                break;
            case 3:
                displayCell = CTE_SESSION_USER_STATUS_CELL;
                break;
            case 4:
                displayCell = CTE_LATEST_NEWS_CELL;
                break;
            case 5:
                displayCell = CTE_SPORTS_LEVELS_CELL;
                break;
            case 6:
                displayCell = CTE_PROBABILITY_CELL;
                break;
            case 7:
                displayCell = CTE_MATCH_MESSAGE_CELL;
                break;
                
            default:
                displayCell = 0;
                break;
        }
    } else if (self.pageDisplayConfiguration == CELL_CONFIG_WITH_SCORE) {
        switch(indexRow) {
            case 0:
                displayCell = CTE_MATCH_STATUS_CELL;
                break;
            case 1:
                displayCell = CTE_HEADER_INFO_CELL;
                break;
            case 2:
                displayCell = CTE_FINAL_SCORE_CELL;
                break;
            case 3:
                displayCell = CTE_MATCH_DETAILS_CELL;
                break;
            case 4:
                displayCell = CTE_SESSION_USER_STATUS_CELL;
                break;
            case 5:
                displayCell = CTE_LATEST_NEWS_CELL;
                break;
            case 6:
                displayCell = CTE_SPORTS_LEVELS_CELL;
                break;
            case 7:
                displayCell = CTE_PROBABILITY_CELL;
                break;
            case 8:
                displayCell = CTE_MATCH_MESSAGE_CELL;
                break;
                
            default:
                displayCell = 0;
                break;
        }
    }
    
    return  displayCell;
}


- (void)setDisplayConfiguration
{
    // first set the match status message
    /*
    MatchPlayer *sessionPlayer = nil;
    MatchPlayer *otherPlayer = nil;
    
    // Get other player
    if ([self.preferences getUserID] == self.individualMatch.player1.userID) {
        sessionPlayer = self.individualMatch.player1;
        otherPlayer = self.individualMatch.player2;
    } else  {
        otherPlayer = self.individualMatch.player1;
        sessionPlayer = self.individualMatch.player2;
    }*/
    
    
    
    if ([self.tMatch isUserATeamCaptain:[self.preferences getUserID]]) { // right now we display buttons only for team captains
        // Set session user status message
        
        Team *sessionCaptainsTeam = [self.tMatch getTeamForCaptain:[self.preferences getUserID]];
      //  Team *otherCaptainsTeam = [self.tMatch getOtherTeamThatDoesntHaveCaptain:[self.preferences getUserID]];

        
        NSDate *thirtyMinAfter = [self.tMatch.dateTime dateByAddingTimeInterval:1800];
        
        if ((([[NSDate date] compare:thirtyMinAfter] == NSOrderedAscending && [self.tMatch.matchStatus isEqualToString:@"UNCONFIRMED"])
             || [[NSDate date] compare:self.tMatch.dateTime] == NSOrderedAscending)
            && self.tMatch.scoreTeamMatch.scoreRecordedBy == 0) {
            
            if ([self.tMatch.matchStatus isEqualToString:@"CANCELLED"]) { // Cancelled match but date is still in future
                
                self.pageDisplayConfiguration = CELL_CONFIG_WITH_NO_SCORE;
                self.buttonDisplayConfiguration = BUTTON_CONFIG_CANCELLED_MATCH;
                
            } else if (self.tMatch.matchCreatorID == [self.preferences getUserID]) {// now set the session player status message
                
                //      self.sessionUserNumericStatusCode = SESSION_USER_IS_MATCH_CREATOR;
                self.pageDisplayConfiguration = CELL_CONFIG_WITH_NO_SCORE;
                self.buttonDisplayConfiguration = BUTTON_CONFIG_MATCH_CREATOR;
                //            [self showButtonsForMatchCreator];
                
            } else if ([sessionCaptainsTeam.status isEqualToString:@"ACCEPTED"]) {
                //      self.sessionUserNumericStatusCode = SESSION_USER_IS_CONFIRMED;
                
                self.pageDisplayConfiguration = CELL_CONFIG_WITH_NO_SCORE;
                self.buttonDisplayConfiguration = BUTTON_CONFIG_CONFIRMED_INVITED_CAPTAIN;
                
                //          [self showButtonsForConfirmedMatchParticipant];
                
            } else if ([sessionCaptainsTeam.status isEqualToString:@"UNCONFIRMED"]) {
                //       self.sessionUserNumericStatusCode = SESSION_USER_IS_UNCONFIRMED;
                self.pageDisplayConfiguration = CELL_CONFIG_WITH_NO_SCORE;
                self.buttonDisplayConfiguration = BUTTON_CONFIG_UNCONFIRMED_INVITED_CAPTAIN;
                //    [self showButtonsForUnconfirmedMatchParticipant];
                
            } else if ([sessionCaptainsTeam.status isEqualToString:@"DECLINED"]) {
                //        self.sessionUserNumericStatusCode = SESSION_USER_IS_UNCONFIRMED;
                
                self.pageDisplayConfiguration = CELL_CONFIG_WITH_NO_SCORE;
                self.buttonDisplayConfiguration = BUTTON_CONFIG_UNCONFIRMED_INVITED_CAPTAIN;
                
                // [self showButtonsForUnconfirmedMatchParticipant];
            }
            
        } else {
            // match in past
            if ([self.tMatch.matchStatus isEqualToString:@"NEVER_OCCURRED"]
                || [self.tMatch.matchStatus isEqualToString:@"UNCONFIRMED"]
                || [self.tMatch.matchStatus isEqualToString:@"CANCELLED"]) {
                
                self.pageDisplayConfiguration = CELL_CONFIG_WITH_NO_SCORE;
                
                //    [self showEmptyScore];
                self.buttonDisplayConfiguration = BUTTON_CONFIG_EMPTY_SCORE;
                
            } else if ([self.tMatch isUserATeamCaptain:[self.preferences getUserID]]
                       && [self.tMatch isScoreDisputed]
                       && self.tMatch.scoreTeamMatch.scoreDisputedBy == [self.preferences getUserID]) {
                
                self.pageDisplayConfiguration = CELL_CONFIG_WITH_NO_SCORE;
                
                //    [self showEmptyScore]; // score was disputed
                self.buttonDisplayConfiguration = BUTTON_CONFIG_EMPTY_SCORE;
                
            } else if ([self.tMatch isUserATeamCaptain:[self.preferences getUserID]]
                       && [self.tMatch isScoreDisputed]
                       && self.tMatch.scoreTeamMatch.scoreDisputedBy != [self.preferences getUserID]) {
                
                self.pageDisplayConfiguration = CELL_CONFIG_WITH_NO_SCORE;
                self.buttonDisplayConfiguration = BUTTON_CONFIG_EMPTY_SCORE;
                
                //   [self showEmptyScore]; // score was disputed
            } else if ([self.tMatch isUserATeamCaptain:[self.preferences getUserID]]
                       && [self.tMatch isScoreConfirmed]
                       && self.tMatch.scoreTeamMatch.scoreConfirmedBy == [self.preferences getUserID]) {
                
                self.pageDisplayConfiguration = CELL_CONFIG_WITH_SCORE;
                self.buttonDisplayConfiguration = BUTTON_CONFIG_SHOW_SCORE;
                
                // [self showScore];
            } else if ([self.tMatch isUserATeamCaptain:[self.preferences getUserID]]
                       && [self.tMatch isScoreConfirmed]
                       && self.tMatch.scoreTeamMatch.scoreConfirmedBy != [self.preferences getUserID]) {
                
                self.pageDisplayConfiguration = CELL_CONFIG_WITH_SCORE;
                self.buttonDisplayConfiguration = BUTTON_CONFIG_SHOW_SCORE;
                
                //    [self showScore];
            } else if ([self.tMatch isUserATeamCaptain:[self.preferences getUserID]]
                       &&[self.tMatch isScoreRecorded]
                       && self.tMatch.scoreTeamMatch.scoreRecordedBy == [self.preferences getUserID]) {
                
                self.pageDisplayConfiguration = CELL_CONFIG_WITH_NO_SCORE;
                self.buttonDisplayConfiguration = BUTTON_CONFIG_EMPTY_SCORE;
                
                //    [self showEmptyScore];
            } else if ([self.tMatch isUserATeamCaptain:[self.preferences getUserID]]
                       && [self.tMatch isScoreRecorded]
                       && self.tMatch.scoreTeamMatch.scoreRecordedBy != [self.preferences getUserID]) {
                
                self.pageDisplayConfiguration = CELL_CONFIG_WITH_NO_SCORE;
                self.buttonDisplayConfiguration = BUTTON_CONFIG_CONFIRM_BUTTONS;
                
                //                [self showConfirmButtons];
            } else if ([self.tMatch isUserATeamCaptain:[self.preferences getUserID]]
                       && ![self.tMatch isScoreRecorded]) {
                
                self.pageDisplayConfiguration = CELL_CONFIG_WITH_NO_SCORE;
                self.buttonDisplayConfiguration = BUTTON_CONFIG_RECORD_BUTTONS;
                //                [self showRecordButtons];
            } else {
                
                self.pageDisplayConfiguration = CELL_CONFIG_WITH_NO_SCORE;
                self.buttonDisplayConfiguration = BUTTON_CONFIG_EMPTY_MATCH_OUTCOME;
                //              [self showEmptyMatchOutcome];
            }
            
        }
        
        
    } else {
        if ([self.tMatch isScoreConfirmed]) {
            
            self.pageDisplayConfiguration = CELL_CONFIG_WITH_SCORE;
            self.buttonDisplayConfiguration = BUTTON_CONFIG_MINIMAL_DISPLAY;  // was BUTTON_CONFIG_SHOW_SCORE
            //   [self showScore];
        } else {
            // Hide buttons because session user is not a match participant (not invited player)
            
            self.pageDisplayConfiguration = CELL_CONFIG_WITH_NO_SCORE;
            self.buttonDisplayConfiguration = BUTTON_CONFIG_MINIMAL_DISPLAY; // BUTTON_CONFIG_EMPTY_SCORE
            //        [self showEmptyScore];
        }
        //     self.notificationDot.hidden = YES;
        //     self.topStatusMessage.hidden = YES;
        
    }
    
    
}


#define MINIMUM_ZOOM_ARC 0.014 //approximately 1 miles (1 degree of arc ~= 69 miles)
#define ANNOTATION_REGION_PAD_FACTOR 1.15
#define MAX_DEGREES_ARC 360
//size the mapView region to fit its annotations
- (void)zoomMapViewToFitAnnotations:(MKMapView *)mapView animated:(BOOL)animated
{
    NSArray *annotations = mapView.annotations;
    int count = [mapView.annotations count];
    if ( count == 0) { return; } //bail if no annotations
    
    //convert NSArray of id <MKAnnotation> into an MKCoordinateRegion that can be used to set the map size
    //can't use NSArray with MKMapPoint because MKMapPoint is not an id
    MKMapPoint points[count]; //C array of MKMapPoint struct
    for( int i=0; i<count; i++ ) //load points C array by converting coordinates to points
    {
        CLLocationCoordinate2D coordinate = [(id <MKAnnotation>)annotations[i] coordinate];
        points[i] = MKMapPointForCoordinate(coordinate);
    }
    //create MKMapRect from array of MKMapPoint
    MKMapRect mapRect = [[MKPolygon polygonWithPoints:points count:count] boundingMapRect];
    //convert MKCoordinateRegion from MKMapRect
    MKCoordinateRegion region = MKCoordinateRegionForMapRect(mapRect);
    
    //add padding so pins aren't scrunched on the edges
    region.span.latitudeDelta  *= ANNOTATION_REGION_PAD_FACTOR;
    region.span.longitudeDelta *= ANNOTATION_REGION_PAD_FACTOR;
    //but padding can't be bigger than the world
    if( region.span.latitudeDelta > MAX_DEGREES_ARC ) { region.span.latitudeDelta  = MAX_DEGREES_ARC; }
    if( region.span.longitudeDelta > MAX_DEGREES_ARC ){ region.span.longitudeDelta = MAX_DEGREES_ARC; }
    
    //and don't zoom in stupid-close on small samples
    if( region.span.latitudeDelta  < MINIMUM_ZOOM_ARC ) { region.span.latitudeDelta  = MINIMUM_ZOOM_ARC; }
    if( region.span.longitudeDelta < MINIMUM_ZOOM_ARC ) { region.span.longitudeDelta = MINIMUM_ZOOM_ARC; }
    //and if there is a sample of 1 we want the max zoom-in instead of max zoom-out
    if( count == 1 )
    {
        region.span.latitudeDelta = MINIMUM_ZOOM_ARC;
        region.span.longitudeDelta = MINIMUM_ZOOM_ARC;
    }
    [self.mapView setRegion:region animated:animated];
}

- (void)assignMapImage
{
    NSArray *annotations = self.mapView.annotations;
    CLLocationCoordinate2D coordinate = [(id <MKAnnotation>)annotations[0] coordinate];
    NSString *staticMapUrl = [NSString stringWithFormat:@"http://maps.google.com/maps/api/staticmap?markers=color:red|%f,%f&%@&sensor=true",coordinate.latitude, coordinate.longitude,@"zoom=16&size=320x120"];
    NSURL *mapUrl = [NSURL URLWithString:[staticMapUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    self.mapImage.image = [UIImage imageWithData: [NSData dataWithContentsOfURL:mapUrl]];
    self.mapImage.userInteractionEnabled = YES;
    UITapGestureRecognizer *pgr = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self action:@selector(handleTap:)];
    //    pgr.delegate = self.view;
    [self.mapImage addGestureRecognizer:pgr];
    
    
    
}


// Only match participants can view messages
- (void)displayMessageSegueError
{
    UIAlertView *alert = [[UIAlertView alloc] init];
    alert.title = @"You cannot view match messages";
    alert.message = @"Sorry, but only match participants can view the match messages.";
    [alert addButtonWithTitle:@"OK"];
    [alert show];
}



- (void)startFBShare
{
    
    /*
     
     if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
     
     SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
     
     [controller setInitialText:@"First post from my iPhone app"];
     [controller addURL:[NSURL URLWithString:@"http://www.appcoda.com"]];
     [controller addImage:[UIImage imageNamed:@"socialsharing-facebook-image.jpg"]];
     
     [self presentViewController:controller animated:YES completion:Nil];
     
     }
     */
    if ([FBSession.activeSession.permissions indexOfObject:@"publish_actions"] == NSNotFound) {
        // Request publish_actions
        [FBSession.activeSession requestNewPublishPermissions:[NSArray arrayWithObject:@"publish_actions"]
                                              defaultAudience:FBSessionDefaultAudienceFriends
                                            completionHandler:^(FBSession *session, NSError *error) {
                                                __block NSString *alertText;
                                                __block NSString *alertTitle;
                                                if (!error) {
                                                    if ([FBSession.activeSession.permissions
                                                         indexOfObject:@"publish_actions"] == NSNotFound){
                                                        // Permission not granted, tell the user we will not publish
                                                        alertTitle = @"Permission not granted";
                                                        alertText = @"Your action will not be published to Facebook.";
                                                        [[[UIAlertView alloc] initWithTitle:@"title"
                                                                                    message:@"text"
                                                                                   delegate:self
                                                                          cancelButtonTitle:@"OK!"
                                                                          otherButtonTitles:nil] show];
                                                    } else {
                                                        // Permission granted, publish the OG story
                                                        [self presentFBDialog];
                                                    }
                                                    
                                                } else {
                                                    // There was an error, handle it
                                                    // See https://developers.facebook.com/docs/ios/errors/
                                                }
                                            }];
        
    } else {
        // permission exists
        [self presentFBDialog];
    }
    
    
    
    
    
}


- (void)presentFBDialog
{
    // Present share dialog

    // Put together the dialog parameters
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   [NSString stringWithFormat:@"%@ vs %@", self.tMatch.team1.teamName, self.tMatch.team2.teamName], @"name",
                                   @"Vaiden is a community for athletes to play their favorite sports using a social platform.", @"caption",
                                   [NSString stringWithFormat:@"Download the iPhone Vaiden App to learn more about this %@ Challenge Match", [self.tMatch.sport.sportName capitalizedString]], @"description",
                                   @"https://itunes.apple.com/us/app/vaiden/id796761406?ls=1&mt=8", @"link",
                                   @"https://s3.amazonaws.com/vaderen_pictures/vaiden_fb_icon.png", @"picture",
                                   nil];
    
    // Show the feed dialog
    [FBWebDialogs presentFeedDialogModallyWithSession:nil
                                           parameters:params
                                              handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
                                                  if (error) {
                                                      // An error occurred, we need to handle the error
                                                      // See: https://developers.facebook.com/docs/ios/errors
                                                      //                         NSLog([NSString stringWithFormat:@"Error publishing story: %@", error.description]);
                                                  } else {
                                                      if (result == FBWebDialogResultDialogNotCompleted) {
                                                          // User cancelled.
                                                          NSLog(@"User cancelled.");
                                                      } else {
                                                          // Handle the publish feed callback
                                                          NSDictionary *urlParams = [self parseURLParams:[resultURL query]];
                                                          
                                                          if (![urlParams valueForKey:@"post_id"]) {
                                                              // User cancelled.
                                                              NSLog(@"User cancelled.");
                                                              
                                                          } else {
                                                              // User clicked the Share button
                                                              NSString *result = [NSString stringWithFormat: @"Posted story, id: %@", [urlParams valueForKey:@"post_id"]];
                                                              NSLog(@"result %@", result);
                                                          }
                                                      }
                                                  }
                                              }];
}

// A function for parsing URL parameters returned by the Feed Dialog.
- (NSDictionary*)parseURLParams:(NSString *)query {
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *pair in pairs) {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        NSString *val =
        [kv[1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        params[kv[0]] = val;
    }
    return params;
}


- (IBAction)venueDetailAction:(id)sender
{
    [self performSegueWithIdentifier:@"Venue Detail Segue" sender:self];
}
/*
- (IBAction)theLatestNewsSegue:(id)sender
{
    if ([self.individualMatch isUserMatchParticipant:[self.preferences getUserID]]) {
        [self performSegueWithIdentifier:@"Messages Segue" sender:self];
    }
}*/

/*
- (void)goToProfile:(UIButton *)sender
{
    self.selectedPlayerToDisplayDetail = sender.tag;
    [self performSegueWithIdentifier:@"Player Detail Segue" sender:self];
}

*/
@end
