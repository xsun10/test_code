//
//  University.h
//  Vaiden
//
//  Created by James Chung on 2/21/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface University : NSObject


@property (nonatomic) NSUInteger universityID;
@property (nonatomic, strong) NSString *universityName;
@property (nonatomic) BOOL isSelected;

- (University *)initWithUniversityDetails:(NSUInteger)universityID
                                  andName:(NSString *)universityName;

@end
