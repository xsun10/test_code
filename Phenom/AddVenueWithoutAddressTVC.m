//
//  AddVenueWithoutAddressTVC.m
//  Vaiden
//
//  Created by James Chung on 12/5/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "AddVenueWithoutAddressTVC.h"
#import "Sport.h"
#import "UITextView+FormValidations.h"
#import "UITextField+FormValidations.h"
#import "UserPreferences.h"
#import "IOSRequest.h"
#import "SetVenuePicsVC.h"

@interface AddVenueWithoutAddressTVC ()

@property (weak, nonatomic) IBOutlet UILabel *longitudeLabel;
@property (weak, nonatomic) IBOutlet UILabel *latitudeLabel;
@property (nonatomic) NSInteger proximityRange;
@property (nonatomic, strong) NSString *sportsCodedString;
@property (weak, nonatomic) IBOutlet UITextView *descriptionField;
@property (nonatomic) BOOL descriptionFieldEmpty;
@property (weak, nonatomic) IBOutlet UITextField *venueNameField;
@property (nonatomic, strong) UserPreferences *preferences;
@property (nonatomic) CGFloat longitude;
@property (nonatomic) CGFloat latitude;
@property (nonatomic) NSInteger venueID;
@property (weak, nonatomic) IBOutlet UISwitch *isPrivateSwitch;
@property (strong, nonatomic)  UIBarButtonItem *doneButton;
@end

@implementation AddVenueWithoutAddressTVC

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.descriptionField.text = @"Venue Description";
    self.descriptionField.textColor = [UIColor lightGrayColor];
    self.descriptionFieldEmpty = YES;
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];

    //[self setDoneButton];
    [self initLocation];

    
}


/*- (void)setDoneButton
{
    UIButton *btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 50, 21);
    btn.layer.borderColor = [UIColor whiteColor].CGColor;
    btn.layer.borderWidth = 1.0;
    
    [btn addTarget:self action:@selector(done:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundColor:[UIColor clearColor]];
    [btn setTitle:@"DONE" forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-MediumItalic" size:12.0]];
    
    // Initialize UIBarbuttonitem...
    self.doneButton = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = self.doneButton;
}*/


- (void)initLocation
{
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    self.proximityRange = 10;
    [locationManager startUpdatingLocation];
    
    
}

- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations {
    CLLocation *location = locations[0];
    NSLog(@"user location: lat%f - lon%f", location.coordinate.latitude, location.coordinate.longitude);
    [locationManager stopUpdatingLocation];
    
    self.longitudeLabel.text = [NSString stringWithFormat:@"%f", location.coordinate.longitude];
    self.latitudeLabel.text = [NSString stringWithFormat:@"%f", location.coordinate.latitude];
    
    self.longitude = location.coordinate.longitude;
    self.latitude = location.coordinate.latitude;
    
}


- (void)setViewController:(AddVenueSportsTVC *)controller withSports:(NSMutableArray *)sports
{
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:2]];
    
    if ([sports count] > 1)
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%lu sports", [sports count]];
    else if ([sports count] == 1)
        cell.detailTextLabel.text = [[sports objectAtIndex:0] sportName];
    else
        cell.detailTextLabel.text = @"";
    
    
    self.sportsCodedString = @"";
    
    for (int i = 0; i < [sports count]; i++) {
        if (i == 0)
            self.sportsCodedString = [[sports objectAtIndex:i] sportName];
        else
            self.sportsCodedString = [NSString stringWithFormat:@"%@_%@", self.sportsCodedString, [[sports objectAtIndex:i] sportName]];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Choose Sports Segue"]) {
        AddVenueSportsTVC *controller = (AddVenueSportsTVC *)segue.destinationViewController;
        controller.delegate = self;
    } else if ([segue.identifier isEqualToString:@"Set Venue Pics Segue"]) {
        SetVenuePicsVC *controller = (SetVenuePicsVC *)segue.destinationViewController;
        controller.venueID = self.venueID;
        controller.numVCsToPopBack = 3;
    }
}

- (BOOL)textViewShouldBeginEditing:(UITextView*)textView {
    if (self.descriptionFieldEmpty) {
        self.descriptionField.text = @"";
        self.descriptionField.textColor = [UIColor blackColor];
        self.descriptionFieldEmpty = NO;
        [self.descriptionField becomeFirstResponder];
    }
    return YES;
}

// did not use this method...
- (void) textViewDidEndEditing:(UITextView*)textView {
    if(self.descriptionField.text.length == 0){
        self.descriptionField.textColor = [UIColor lightGrayColor];
        self.descriptionField.text = @"Description";
        self.descriptionFieldEmpty = YES;
    }
}
- (IBAction)done:(id)sender
{
    if ([self isFormOK]) {
        self.doneButton.enabled = NO;
        HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        HUD.labelText = @"Saving";
        
        NSString *venueDescription;
        if ([self.descriptionField.text isEqualToString:@"Venue Description"]) {
            venueDescription = @"";
        } else {
            venueDescription = self.descriptionField.text;
        }
        
        [IOSRequest saveVenueWithCoordinatesForUser:[self.preferences getUserID]
                                          longitude:self.longitude
                                           latitude:self.latitude
                                          venueName:self.venueNameField.text
                                        description:venueDescription
                                         withSports:self.sportsCodedString
                                          isPrivate:self.isPrivateSwitch.on
                                       onCompletion:^(NSDictionary *complete) {
                                           
                                           dispatch_async(dispatch_get_main_queue(), ^{
                                               self.doneButton.enabled = YES;
                                               [MBProgressHUD hideHUDForView:self.view animated:YES];
                                               if ([complete[@"reachedMaxError"] isEqualToString:@"TRUE"]) {
                                                   UIAlertView *alert = [[UIAlertView alloc] init];
                                                   alert.title = @"Venue Creation Error";
                                                   alert.message = @"You have reached your max venues.  Please delete a venue before creating another.";
                                                   [alert addButtonWithTitle:@"OK"];
                                                   [alert show];
                                                   
                                               } else if ([complete[@"outcome"] isEqualToString:@"failure"]) {
                                                   
                                                   UIAlertView *alert = [[UIAlertView alloc] init];
                                                   alert.title = @"Venue Creation Error";
                                                   alert.message = @"User not located.";
                                                   [alert addButtonWithTitle:@"OK"];
                                                   [alert show];
                                               } else {
                                   //                [self.delegate setMyVenueViewController:self];
                                                   self.venueID = [complete[@"venue_id"] integerValue];
                                                   
                                                   if (self.isCreatingMatch) {
                                                       [self.delegate setViewController:self andVenueID:self.venueID andVenueName:self.venueNameField.text];
                                                       NSInteger currentIndex = [self.navigationController.viewControllers indexOfObject:self];
                                                       [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:currentIndex-3] animated:YES];
                                                   } else { // only add pics if not creating match
                                                       [self.preferences setVenuesListPageRefreshState:YES];
                                                       [self performSegueWithIdentifier:@"Set Venue Pics Segue" sender:self];
                                                   }
                                                   
                                               }
                                               
                                           });
                                           
                                       }];
        double delayInSeconds = 10.0;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            //code to be executed on the main queue after delay
            self.doneButton.enabled = YES;
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        });
        
    }
}


- (BOOL)isFormOK
{
    if ([self.venueNameField.text length] == 0) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Venue Name Needed";
        alert.message = @"Please enter the official name of the venue.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
        return NO;
    }
    
    if (![self.venueNameField checkLocationName]) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Venue Name Not Valid";
        alert.message = @"The venue name you entered does not fit our formats.  Please retry.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
        return NO;
    }
    
    
    UITableViewCell *sportsCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:3]];
    if ([sportsCell.detailTextLabel.text isEqualToString:@"Detail"]) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Sports Needed";
        alert.message = @"Please enter the sports that can be played at this venue.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
        return NO;
    }
    
    if (![self.descriptionField checkVenueDescription]) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Description Not Valid";
        alert.message = @"The venue description you entered does not fit our formats.  Please retry.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
        return NO;
    }
    return YES
    ;
}


- (IBAction)descriptionFieldTap:(id)sender
{
    [self textViewShouldBeginEditing:self.descriptionField];
}

- (IBAction)clearVenueName:(id)sender
{
    self.venueNameField.text = @"";
    [self.venueNameField resignFirstResponder];
}
@end
