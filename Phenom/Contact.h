//
//  Contact.h
//  Phenom
//
//  Created by James Chung on 6/9/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "Player.h"

@interface Contact : Player

#define CONTACT_TYPE_BROWSE 0
#define CONTACT_TYPE_FOLLOWING 1
#define CONTACT_TYPE_NEARBY 2
#define CONTACT_TYPE_SEARCH 3

#define NOT_FOLLOWING_STATUS 0 // this is when session user is the follower
#define IS_FOLLOWING_STATUS 1 // this is when session user is the follower
#define PENDING_FOLLOWING_STATUS 2 // this is when session user is the follower
#define APPROVED_FOLLOWER_STATUS 3 // this is when session user is the LEADER

@property (nonatomic) NSInteger contactType;
@property (nonatomic) NSInteger followingStatus; // was followingFlag
@property (nonatomic) BOOL approvalRequiredToFollow;



- (Contact *) initWithContactDetails:(NSInteger)userID
                       andProfileStr:(NSString *)profileBaseString
                         andUserName:(NSString *)userName
                         andFullName:(NSString *)fullName
                         andLocation:(NSString *)location
                            andAbout:(NSString *)about
                            andCoins:(float)coins
                        contactType :(NSInteger)contactType
                     followingStatus:(NSInteger)followingStatus;

@end
