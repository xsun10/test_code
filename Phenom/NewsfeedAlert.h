//
//  NewsfeedAlert.h
//  Vaiden
//
//  Created by James Chung on 6/1/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "PlayerAlert.h"

@interface NewsfeedAlert : PlayerAlert

@property (nonatomic) NSInteger newsfeedID;
@property (nonatomic) NSInteger newsfeedPostType;
@property (nonatomic, strong) NSString *supplementaryName;

@property (nonatomic) NSInteger commentID; // only used for comment alerts (type 5)

- (NewsfeedAlert *) initWithPlayerAlertDetails:(NSInteger)userID
                                 andUserName:(NSString *)userName
                         andProfilePicString:(NSString *)profilePicString
                                  andMessage:(NSString *)message
                                andAlertType:(NSInteger)alertType
                                andAlertDate:(NSDate *)alertDate
                                andWasViewed:(BOOL)wasViewed
                                 andNewsfeedID:(NSInteger)newsfeedID
                           andNewsfeedPostType:(NSInteger)newsfeedPostType;

- (NSString *)getStringRepresentationOfNewsfeedType;
@end
