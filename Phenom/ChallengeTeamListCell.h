//
//  ChallengeTeamListCell.h
//  Vaiden
//
//  Created by James Chung on 4/10/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChallengeTeamListCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *teamLogo;
@property (weak, nonatomic) IBOutlet UILabel *teamNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *contactsDescriptionLabel;
@property (weak, nonatomic) IBOutlet UIButton *challengeButton;

@end
