//
//  HowItWorksView.m
//  Phenom
//
//  Created by James Chung on 3/15/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "HowItWorksView.h"

@implementation HowItWorksView

- (void) setup
{
    
}

- (void) awakeFromNib
{
    [self setup];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    [self setup];
    return self;
}

- (void) setSlideImageFile:(NSString *)slideImageFile
{
    _slideImageFile = slideImageFile;
    [self setNeedsDisplay];
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    [[UIImage imageNamed:self.slideImageFile] drawInRect:self.bounds];
    
    
}


@end
