//
//  Comment.m
//  Phenom
//
//  Created by James Chung on 5/8/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "Comment.h"

@implementation Comment


- (Comment *)initWithCommentDetails:(NSInteger)commentID
                     andCommentText:(NSString *)commentText
                           byUserID:(NSInteger)userID
                        andUsername:(NSString *)username
                      andProfileStr:(NSString *)profileBaseString 
                             onDate:(NSDate *)postDate
{
    self = [super init];
    
    if (self) {
        _commentID = commentID;
        _commentText = commentText;
        _userID = userID;
        _username = username;
        _profileBaseString = profileBaseString;
        _postDate = postDate;
    }
    
    return self;
}


@end
