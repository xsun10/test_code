//
//  MyTeamsListCell.h
//  Vaiden
//
//  Created by James Chung on 4/7/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyTeamsListCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *teamPic;
@property (weak, nonatomic) IBOutlet UILabel *teamNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *numParticipantsLabel;

@end
