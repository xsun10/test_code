//
//  CompetitionNewsCell.h
//  Vaiden
//
//  Created by James Chung on 3/6/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CompetitionNewsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *profilePic;
@property (weak, nonatomic) IBOutlet UILabel *sharedByLabel;
@property (weak, nonatomic) IBOutlet UILabel *postDateLabel;
@property (weak, nonatomic) IBOutlet UIImageView *sportIcon;

@property (weak, nonatomic) IBOutlet UIButton *detailsButton;
@property (weak, nonatomic) IBOutlet UILabel *numCommentLabel;
@property (weak, nonatomic) IBOutlet UILabel *commentTextLabel;
@property (weak, nonatomic) IBOutlet UIButton *commentButton;
@property (weak, nonatomic) IBOutlet UILabel *numShareLabel;
@property (weak, nonatomic) IBOutlet UILabel *shareTextLabel;
@property (weak, nonatomic) IBOutlet UIButton *shareButton;
@property (weak, nonatomic) IBOutlet UIImageView *shareIcon;
@property (weak, nonatomic) IBOutlet UIImageView *commentIcon;
@property (weak, nonatomic) IBOutlet UIView *myCellBackgroundView;
@property (weak, nonatomic) IBOutlet UIView *commentButtonBackground;
@property (weak, nonatomic) IBOutlet UIView *shareButtonBackground;
@property (weak, nonatomic) IBOutlet UILabel *detailCompetitionTextLabel;
@property (weak, nonatomic) IBOutlet UIButton *actionButtonHandle;
@property (weak, nonatomic) IBOutlet UIView *likeButtonBackground;
@property (weak, nonatomic) IBOutlet UIImageView *likeIcon;
@property (weak, nonatomic) IBOutlet UIButton *likeButton;
@property (weak, nonatomic) IBOutlet UILabel *likeNumberLabel;
@end
