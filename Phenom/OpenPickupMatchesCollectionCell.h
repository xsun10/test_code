//
//  OpenPickupMatchesCollectionCell.h
//  Phenom
//
//  Created by James Chung on 7/19/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OpenPickupMatchesCollectionCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *profilePic;
@end
