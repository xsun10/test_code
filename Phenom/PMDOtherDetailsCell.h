//
//  PMDOtherDetailsCell.h
//  Vaiden
//
//  Created by James Chung on 1/16/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PMDOtherDetailsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *otherDetailsIconBackground;
@property (weak, nonatomic) IBOutlet UIImageView *otherDetailsIconImage;
@property (weak, nonatomic) IBOutlet UIImageView *requiredExperienceIcon;
@property (weak, nonatomic) IBOutlet UIImageView *genderIcon;
@property (weak, nonatomic) IBOutlet UILabel *experienceLabel;
@property (weak, nonatomic) IBOutlet UILabel *genderLabel;
@property (weak, nonatomic) IBOutlet UIView *myBackgroundView;

@end
