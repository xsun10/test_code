//
//  NewsIndividualMatchPostCell.h
//  Phenom
//
//  Created by James Chung on 4/24/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewsfeedMatchHeadingCV.h"


@class NewsIndividualMatchPostCell;

@protocol NewsIndividualMatchPostCell_delegate <NSObject>
-(void)setViewController:(NewsIndividualMatchPostCell *)controller withUserIDToSegue:(NSInteger)userID;
@end


@interface NewsIndividualMatchPostCell : UITableViewCell <UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, weak) id <NewsIndividualMatchPostCell_delegate> delegate;
@property (weak, nonatomic) IBOutlet UIImageView *newsfeedIcon;

@property (weak, nonatomic) IBOutlet UIView *commentButtonBackground;
@property (weak, nonatomic) IBOutlet UIView *shareButtonBackground;
@property (weak, nonatomic) IBOutlet UIButton *commentButton;
@property (weak, nonatomic) IBOutlet UILabel *commentNumberLabel;
@property (weak, nonatomic) IBOutlet UIImageView *commentBubbleImage;
@property (weak, nonatomic) IBOutlet UIImageView *shareButtonImage;
@property (weak, nonatomic) IBOutlet UILabel *sharesNumberLabel;

@property (weak, nonatomic) IBOutlet UIView *vsCircle;
@property (weak, nonatomic) IBOutlet UIView *section3View;

@property (weak, nonatomic) IBOutlet UIView *myBackgroundViewCell;


@property (weak, nonatomic) IBOutlet UILabel *timeFrameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *userImage1;
@property (weak, nonatomic) IBOutlet UIImageView *userImage2;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel1;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel2;
@property (weak, nonatomic) IBOutlet UILabel *levelUser1;
@property (weak, nonatomic) IBOutlet UILabel *levelUser2;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;

@property (weak, nonatomic) IBOutlet UIView *smallCircleView;

@property (weak, nonatomic) IBOutlet UIButton *sharesButton;

@property (weak, nonatomic) IBOutlet UIButton *remixButton;
@property (weak, nonatomic) IBOutlet UIImageView *timerIcon;
@property (weak, nonatomic) IBOutlet UIView *challengeLabelBackground;
@property (weak, nonatomic) IBOutlet UILabel *sharedByLabel;
@property (weak, nonatomic) IBOutlet UIImageView *shareIcon;
@property (weak, nonatomic) IBOutlet UIImageView *sportIcon;
@property (weak, nonatomic) IBOutlet UILabel *matchNameLabel;
@property (weak, nonatomic) IBOutlet NewsfeedMatchHeadingCV *playerCV;
@property (weak, nonatomic) IBOutlet UIView *sportIconBackground;
@property (weak, nonatomic) IBOutlet UIView *percentBackground;
@property (weak, nonatomic) IBOutlet UILabel *headlineMessageLabel;
@property (weak, nonatomic) IBOutlet UIView *backgroundViewArea;
@property (weak, nonatomic) IBOutlet UIView *detailButtonView;
@property (weak, nonatomic) IBOutlet UIButton *detailButton;
@property (weak, nonatomic) IBOutlet UIImageView *sportBackgroundImage;

@property (weak, nonatomic) IBOutlet UIView *shareDot;
@property (weak, nonatomic) IBOutlet NewsfeedMatchHeadingCV *matchHeadingCV;
@property (nonatomic, strong) NSString *matchCategory;
@property (weak, nonatomic) IBOutlet UILabel *sportLabel;
@property (nonatomic, strong) NSMutableArray *competitors;
@property (weak, nonatomic) IBOutlet UIView *headerBackground;
@property (weak, nonatomic) IBOutlet UILabel *headlineSportLabel;
@property (weak, nonatomic) IBOutlet UIImageView *headlineSportPic;
@property (weak, nonatomic) IBOutlet UIView *headlineSportPicBackground;

@property (weak, nonatomic) IBOutlet UIImageView *matchStatusIcon;
@property (weak, nonatomic) IBOutlet UILabel *matchStatus;
@property (weak, nonatomic) IBOutlet UIButton *actionButtonHandle;

@property (weak, nonatomic) IBOutlet UIView *likeButtonBackground;
@property (weak, nonatomic) IBOutlet UIImageView *likeIcon;
@property (weak, nonatomic) IBOutlet UILabel *likeNumberLabel;
@property (weak, nonatomic) IBOutlet UIButton *likeButton;
@property (weak, nonatomic) IBOutlet UIButton *viewLikesButton;
@property (weak, nonatomic) IBOutlet UIButton *viewCommentsButton;



- (void)clearMyCV;
@end
