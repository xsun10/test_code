//
//  PlayerAlertFollow.h
//  Vaiden
//
//  Created by James Chung on 5/16/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "PlayerAlert.h"

@interface PlayerAlertFollow : PlayerAlert

- (PlayerAlertFollow *) initWithPlayerAlertDetails:(NSInteger)userID
                                       andUserName:(NSString *)userName
                               andProfilePicString:(NSString *)profilePicString
                                        andMessage:(NSString *)message
                                      andAlertType:(NSInteger)alertType
                                      andAlertDate:(NSDate *)alertDate
                                      andWasViewed:(BOOL)wasViewed
                                   andFollowStatus:(NSInteger)followStatus;
@end
