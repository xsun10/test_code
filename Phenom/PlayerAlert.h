//
//  PlayerAlert.h
//  Vaiden
//
//  Created by James Chung on 5/6/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Contact.h"

@interface PlayerAlert : NSObject

@property (nonatomic, strong) Contact *refPlayer;
@property (nonatomic, strong) NSString *message;
@property (nonatomic) NSInteger alertType;
@property (nonatomic, strong) NSDate *alertDateTime;
@property (nonatomic) BOOL wasViewed;


- (PlayerAlert *) initWithPlayerAlertDetails:(NSInteger)userID
                                 andUserName:(NSString *)userName
                         andProfilePicString:(NSString *)profilePicString
                                  andMessage:(NSString *)message
                                andAlertType:(NSInteger)alertType
                                andAlertDate:(NSDate *)alertDate
                                andWasViewed:(BOOL)wasViewed;

@end
