//
//  TeamDetailTVC.m
//  Vaiden
//
//  Created by James Chung on 4/7/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "TeamDetailTVC.h"
#import "TDHeaderCell.h"
#import "TDMembersCell.h"
#import "IOSRequest.h"
#import "UserPreferences.h"
#import "InviteMoreTeamMembersTVC.h"

@interface TeamDetailTVC ()

@property (nonatomic, strong) UIView *tempView;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
@property (nonatomic, strong) UserPreferences *preferences;
@property (nonatomic, strong) Team *theTeam;


@end

@implementation TeamDetailTVC

#define HEADER_CELL 0
#define BUTTONS_CELL 1
#define MEMBERS_CELL 2

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];

    
    [self loadTeamDetails];
}

- (void)loadTeamDetails
{
    [self showTempSpinner];
    
    self.tableView.scrollEnabled = NO;
    [IOSRequest getTeamDetails:self.teamID
                  onCompletion:^(NSDictionary *results) {
                      
                      self.theTeam = [[Team alloc] initWithTeamDetails:[results[@"team_id"] integerValue]
                                                           andTeamName:results[@"team_name"]
                                                            andCaptain:nil andPicString:results[@"logo"]];
                      [self.theTeam loadTeamMembersFromServerObject:results[@"members"]];
                      [self.theTeam setCaptainFromTeamMembersArray:[results[@"captain_id"]integerValue]];
                      
                      dispatch_async(dispatch_get_main_queue(), ^{
                         
                          [self.tableView reloadData];
                          [self.activityIndicator stopAnimating];
                          self.tempView.hidden = YES;
                          self.tableView.scrollEnabled = YES;
                      });
                  }];
}


- (void)showTempSpinner
{
    self.tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    self.tempView.backgroundColor = [UIColor lightGrayColor];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] init];
    [self.activityIndicator setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width/2.0, [UIScreen mainScreen].bounds.size.height/2.0 - 50)];
    [self.activityIndicator startAnimating];
    [self.tempView addSubview:self.activityIndicator];
    
    [self.view addSubview:self.tempView];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case HEADER_CELL:
            return 199.0;
            break;
        
        case BUTTONS_CELL:
            if ([self.preferences getUserID] == self.theTeam.captain.userID ||
                [self.theTeam isSessionUserAlreadyConfirmedMember:[self.preferences getUserID]] ||
                [self.theTeam isSessionUserUnconfirmedAndAllowedToJoinTeam:[self.preferences getUserID]]) {
                return 63.0;
            } else {
                return 0.0;
            }
            // else don't display buttons
            break;
            
        case MEMBERS_CELL:
            return 175.0;
            break;
            
        default:
            break;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == HEADER_CELL) {
        NSString static *CellIdentifier = @"Header Cell";
        
        TDHeaderCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        
        cell.teamLogo.image = [UIImage imageNamed:self.theTeam.picStringName];
        cell.teamNameLabel.text = self.theTeam.teamName;
    
        return cell;
    } else if (indexPath.row == BUTTONS_CELL) {
        NSString static *CellIdentifier = @"Buttons Cell";
        
        TDButtonsCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        cell.delegate = self;
        [cell clearAllButtonsFromContentView];
        [cell displayUserButtonsForTeam:self.theTeam andSessionUser:[self.preferences getUserID]];
        
        return cell;
        
    } else if (indexPath.row == MEMBERS_CELL) {
        NSString static *CellIdentifier = @"Members Cell";
        
        TDMembersCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        cell.numMembersLabel.text = [NSString stringWithFormat:@"%ld Members", [self.theTeam getNumConfirmedMembers]];
        
        [cell myInitializer:self.theTeam.players];
        
        return cell;
    }
    
    return nil;
}

#define CANCEL_BUTTON_ACTIONSHEET @"Cancel "

#define TITLE_OF_ACTIONSHEET_DELETE_TEAM @"Your team will be removed permanently if you proceed"
#define DELETE_TEAM @"Delete Team"

#define TITLE_OF_ACTIONSHEET_LEAVE_TEAM @"By leaving this team, you will no longer benefit from changes in the team's sports level"
#define LEAVE_TEAM @"Leave Team"

#define TITLE_OF_ACTIONSHEET_JOIN_TEAM @"By joining this team, you agree to be an active participant in team matches"
#define JOIN_TEAM @"Join Team"

- (void)setWithCellInfo:(TDButtonsCell *)cellObj withActionString:(NSString *)actionString
{
    if ([actionString isEqualToString:@"DELETE"]) {
        [self showActionsheet:DELETE_TEAM];
    } else if ([actionString isEqualToString:@"INVITE"]) {
        [self performSegueWithIdentifier:@"Invite Segue" sender:self];
    } else if ([actionString isEqualToString:@"LEAVE"]) {
        [self showActionsheet:LEAVE_TEAM];
    } else if ([actionString isEqualToString:@"JOIN"]) {
        [self showActionsheet:JOIN_TEAM];
    }
}



- (void)showActionsheet:(NSString *)actionType
{
    if ([actionType isEqualToString:DELETE_TEAM]) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                      initWithTitle:TITLE_OF_ACTIONSHEET_DELETE_TEAM delegate:self
                                      cancelButtonTitle:CANCEL_BUTTON_ACTIONSHEET destructiveButtonTitle:nil
                                      otherButtonTitles:DELETE_TEAM,  nil];
        [actionSheet showFromTabBar:self.tabBarController.tabBar];
        
    } else if ([actionType isEqualToString:LEAVE_TEAM]) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                      initWithTitle:TITLE_OF_ACTIONSHEET_LEAVE_TEAM delegate:self
                                      cancelButtonTitle:CANCEL_BUTTON_ACTIONSHEET destructiveButtonTitle:nil
                                      otherButtonTitles:LEAVE_TEAM,  nil];
        [actionSheet showFromTabBar:self.tabBarController.tabBar];
    } else if ([actionType isEqualToString:JOIN_TEAM]) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                      initWithTitle:TITLE_OF_ACTIONSHEET_JOIN_TEAM delegate:self
                                      cancelButtonTitle:CANCEL_BUTTON_ACTIONSHEET
                                      destructiveButtonTitle:nil
                                      otherButtonTitles:JOIN_TEAM, nil];
        [actionSheet showFromTabBar:self.tabBarController.tabBar];
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == actionSheet.destructiveButtonIndex) {
        [self.view.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    } else {
        NSString *choice = [actionSheet buttonTitleAtIndex:buttonIndex];
        
        if ([choice isEqualToString:JOIN_TEAM]) {
            [self joinTeamAction];
        } else if ([choice isEqualToString:LEAVE_TEAM]) {
            [self leaveTeamAction];
        } else if ([choice isEqualToString:DELETE_TEAM]) {
            [self deleteTeamAction];
        }
    }
}


- (void)joinTeamAction
{
    if (![self.preferences isVerifiedAccount]) {
        [self performSegueWithIdentifier:@"Check Verification Segue" sender:self];
    } else {
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.labelText = @"Joining Team";
    [IOSRequest joinTeam:self.theTeam.teamID
                withUser:[self.preferences getUserID]
            onCompletion:^(NSDictionary *results) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    if ([results[@"outcome"] isEqualToString:@"success"])
                        [self loadTeamDetails];
                    else {
                        UIAlertView *alert = [[UIAlertView alloc] init];
                        alert.title = @"Join Error";
                        alert.message = @"There was an error completing your request.  Please retry.";
                        [alert addButtonWithTitle:@"OK"];
                        [alert show];
                    }
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                    
                });
            }];
    
    double delayInSeconds = 10.0;
       dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
       dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
           // code to be executed on the main queue after delay
         [MBProgressHUD hideHUDForView:self.view animated:YES];// just in case
      });
    }
}

- (void)leaveTeamAction
{
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.labelText = @"Leaving Team";
    [IOSRequest leaveTeam:self.theTeam.teamID
                 withUser:[self.preferences getUserID]
             onCompletion:^(NSDictionary *results) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    if ([results[@"outcome"] isEqualToString:@"success"])
                        [self loadTeamDetails];
                    else {
                        UIAlertView *alert = [[UIAlertView alloc] init];
                        alert.title = @"Leave Error";
                        alert.message = @"There was an error completing your request.  Please retry.";
                        [alert addButtonWithTitle:@"OK"];
                        [alert show];
                    }
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                    
                });
            }];
    
    double delayInSeconds = 10.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        // code to be executed on the main queue after delay
        [MBProgressHUD hideHUDForView:self.view animated:YES];// just in case
    });
}

- (void)deleteTeamAction
{
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.labelText = @"Deleting Team";
    
    [IOSRequest deleteTeam:self.theTeam.teamID
                    byUser:[self.preferences getUserID]
              onCompletion:^(NSDictionary *results) {
                  
                  dispatch_async(dispatch_get_main_queue(), ^{
                     
                      if ([results[@"outcome"] isEqualToString:@"success"]) {
                          [self.navigationController popViewControllerAnimated:YES];
                      } else {
                          UIAlertView *alert = [[UIAlertView alloc] init];
                          alert.title = @"Delete Error";
                          alert.message = @"There was an error completing your request.  Please retry.";
                          [alert addButtonWithTitle:@"OK"];
                          [alert show];
                      }
                      [MBProgressHUD hideHUDForView:self.view animated:YES];
                  });
              }];
    double delayInSeconds = 10.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        // code to be executed on the main queue after delay
        [MBProgressHUD hideHUDForView:self.view animated:YES];// just in case
    });
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Invite Segue"]) {
        InviteMoreTeamMembersTVC *controller = segue.destinationViewController;
        controller.teamID = self.theTeam.teamID;
    }
}
@end
