//
//  CreateAccountViewController.h
//  Phenom
//
//  Created by James Chung on 3/19/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomBaseTVC.h"
#import "MBProgressHUD.h"

@interface CreateAccountViewController : CustomBaseTVC <MBProgressHUDDelegate> {
    MBProgressHUD *HUD;
    NSOperationQueue         *operationQueue;
}

@end
