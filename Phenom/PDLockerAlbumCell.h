//
//  PDLockerAlbumCell.h
//  Vaiden
//
//  Created by James Chung on 5/10/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDLockerAlbumCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *sportLabel;
@property (weak, nonatomic) IBOutlet UILabel *seasonYearLabel;
@property (weak, nonatomic) IBOutlet UIImageView *lockerImage;
@property (weak, nonatomic) IBOutlet UILabel *numPicsLabel;
@end
