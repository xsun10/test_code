//
//  StatTagNSMutableArray.m
//  Vaiden
//
//  Created by James Chung on 6/5/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "StatsGroup.h"
#import "StatTag.h"
#import "StatTagCombination.h"

@interface StatsGroup()

@property (nonatomic, strong) NSMutableArray *statsSingleTagArray;
@property (nonatomic, strong) NSMutableArray *statsTagCombinationArray;
@end

@implementation StatsGroup



- (StatsGroup *)initWithServerObject:(NSArray *)results
{
    self = [super init];
    
    if (self) {
        _statsSingleTagArray = [[NSMutableArray alloc] init];
        _statsArray = [[NSMutableArray alloc] init];
        _statsTagCombinationArray = [[NSMutableArray alloc] init];
        
        if (![results isKindOfClass:[NSNull class]] && [results count] > 0 ) {
            
            for (id obj in results) {
                
                if ([obj isKindOfClass:[StatsGroup class]])
                    continue;
                
                // first check if already exits
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"statLink LIKE %@ AND subCategory LIKE %@", obj[@"stat_link"], obj[@"stat_subcategory"]];
                NSArray *filteredArray = [NSArray arrayWithArray:[_statsSingleTagArray filteredArrayUsingPredicate:predicate]];
                
                if ([filteredArray count] > 0) {
                    StatTag *stat1 = [filteredArray objectAtIndex:0]; // only one element should be in filtered array
                    [_statsSingleTagArray removeObject:stat1]; // remove from main array
                    
                    // now create the stat tagged that is linked to stat1
                    StatTag *stat2 = [[StatTag alloc] initWithStatTag:[obj[@"stat_id"] integerValue]
                                                       andStatTagName:obj[@"stat_name"]
                                                      andStatTagValue:obj[@"stat_value"]
                                                          andStatLink:obj[@"stat_link"]
                                                      andStatLinkType:obj[@"stat_linktype"]
                                                       andSubCategory:obj[@"stat_subcategory"]
                                                   andShowSubcategory:[obj[@"stat_showsubcategory"] boolValue]];
                    
                    StatTagCombination *stc = nil;
                    
                    if ([obj[@"stat_linktype"] isEqualToString:@"numerator"]) {
                        stc = [[StatTagCombination alloc] initWithStat1:stat2 andStat2:stat1];
                    } else {
                        stc = [[StatTagCombination alloc] initWithStat1:stat1 andStat2:stat2];
                    }
                    
                    
                    [_statsTagCombinationArray addObject:stc];
                    
                    
                } else {
                    
                    
                    StatTag *stat = [[StatTag alloc] initWithStatTag:[obj[@"stat_id"] integerValue]
                                                      andStatTagName:obj[@"stat_name"]
                                                     andStatTagValue:obj[@"stat_value"]
                                                         andStatLink:obj[@"stat_link"]
                                                     andStatLinkType:obj[@"stat_linktype"]
                                                      andSubCategory:obj[@"stat_subcategory"]
                                                  andShowSubcategory:[obj[@"stat_showsubcategory"] boolValue]];
                    [_statsSingleTagArray addObject:stat];
                    
                }
                
                
            }
            
            
            
            
        }

        
    }
    
    NSMutableSet *set = [NSMutableSet setWithArray:_statsTagCombinationArray];
    [set addObjectsFromArray:_statsSingleTagArray];
    
    _statsArray = [set allObjects];
    
    return self;
}





@end
