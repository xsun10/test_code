//
//  TDMembersCell.h
//  Vaiden
//
//  Created by James Chung on 4/7/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TDMembersCell : UITableViewCell <UICollectionViewDataSource, UICollectionViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *numMembersLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *membersCV;
@property (nonatomic, strong) NSArray *members;

- (void)myInitializer:(NSArray *)membersArray;
@end
