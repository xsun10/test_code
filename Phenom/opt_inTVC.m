//
//  opt_inTVC.m
//  Vaiden
//
//  Created by Turbo on 7/29/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "opt_inTVC.h"
#import "UserPreferences.h"
#import "IOSRequest.h"

@interface opt_inTVC ()
@property (nonatomic, strong) UserPreferences *preferences;
@property (strong, nonatomic)  UIBarButtonItem *doneButton;
@property (weak, nonatomic) IBOutlet UILabel *noteLabel;
@property (weak, nonatomic) IBOutlet UILabel *optLabel;
@property (weak, nonatomic) IBOutlet UISwitch *switcher;
@property (nonatomic, strong) UIView *tempView;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;


@end

@implementation opt_inTVC

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    [self showTempSpinner];
    [self loadOptSettings];
  
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 1;
}
- (IBAction)Done:(id)sender {
    [IOSRequest setOptSettingsForUser:[self.preferences getUserID]
                                                      andApprovalFlag:self.switcher.isOn
                                                         onCompletion:^(NSDictionary *results) {
                                                             
                                                             dispatch_async(dispatch_get_main_queue(), ^{
                                                                 if ([results[@"outcome"] isEqualToString:@"failure"]) {
                                                                     UIAlertView *alert = [[UIAlertView alloc] init];
                                                                     alert.title = @"Save Error";
                                                                     alert.message = @"There was a problem completing your request.  Please try again later.";
                                                                     [alert addButtonWithTitle:@"OK"];
                                                                     [alert show];
                                                                 } else {
                                                                     [self.navigationController popViewControllerAnimated:YES];
                                                                 }
                                                             });
                                                             
                                                         }];

}

- (void)showTempSpinner
{
    self.tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    self.tempView.backgroundColor = [UIColor lightGrayColor];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] init];
    [self.activityIndicator setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width/2.0, [UIScreen mainScreen].bounds.size.height/2.0 )];
    [self.activityIndicator startAnimating];
    [self.tempView addSubview:self.activityIndicator];
    
    [self.view addSubview:self.tempView];
    
}

/*- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"optCell" forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void)loadOptSettings
{
    [IOSRequest getOptSettingsForUser:[self.preferences getUserID]
                             onCompletion:^(NSDictionary *results) {
                                 
                                 dispatch_async(dispatch_get_main_queue(), ^{
                                     
                                     [self.activityIndicator stopAnimating];
                                     self.tempView.hidden = YES;
                                     
                                     if ([results[@"outcome"] isEqualToString:@"failure"]) {
                                         
                                     } else {
                                         if( ![results[@"approval_flag"] isEqualToString:@"in"] ){
                                             self.noteLabel.text=@"          By opting back in, you agree to the terms and conditionsof our contest, attest that you are at least 13 years of age, and accept all responsibility for actions that may relate to your amateur status.";
                                             self.noteLabel.font=[UIFont fontWithName:@"Helvetica" size:13];
                                             self.optLabel.text=@"Opt-In";
                                         }
                                     }
                                 });
                             }];
}

@end
