//
//  S3Tools2.m
//  Vaiden
//
//  Created by James Chung on 5/9/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//
// THIS FILE IS DIFFERENT THAN S3TOOLS IN THAT S3TOOLS2 SEPARATES THE AMAZON SERVER IMAGE UPLOAD FROM THE INFO UPLOAD TO THE VAIDEN SERVERS.

#import "S3Tools2.h"
#import "AmazonClientManager.h"
#import "IOSRequest.h"
#import <ImageIO/ImageIO.h>

@implementation S3Tools2

//#define PICTURES_BUCKET @"vaderen_pictures"
//#define VENUES_BUCKET @"vaderen_venues"
#define PICTURES_BUCKET @"test_pictures"
#define VENUES_BUCKET @"test_venues"

// for newsfeed posts (pics, text, and video uploads)
-(id)initWithData:(NSData *)theData
          forUser:(NSInteger)userID
         dataType:(NSString *)dataType
andBaseFileNameString:(NSString *)fileNameBaseString
       andFileExt:(NSString *)fileExtension
{
    self = [super init];
    if (self)
    {
        _fileData = theData ;
        _userID = userID;
        _dataType = dataType;
        _fileNameBase = fileNameBaseString;
      /*
        if ([dataType isEqualToString:@"venue_thumbnail"] || [dataType isEqualToString:@"venue_pic"]) {
            _fileNameBase = [self getFileNameBaseStringWithType:dataType forVenue:userID];
        } else {
            _fileNameBase = [self getFileNameBaseStringWithType:dataType forUser:userID];
        }*/
        
        _fileExtension = fileExtension;
        
    }
    
    return self;
}


- (void)start
{
    NSString *bucketName = [self getBucketName];
    self.keyName = [self getModifiedFilenameFromBase];
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        
        // Upload image data.  Remember to set the content type.
        S3PutObjectRequest *por = [[S3PutObjectRequest alloc] initWithKey:self.keyName
                                                                 inBucket:bucketName];
        
        por.contentType = @"image/jpeg";
        por.data        = self.fileData;
        // Put the image data into the specified s3 bucket and object.
        [[AmazonClientManager s3] putObject:por];
        
    });
    
    
}

- (NSString *)getBucketName
{
    if ([self.dataType isEqualToString:@"profile"] ||
        [self.dataType isEqualToString:@"thumbnail"] ||
        [self.dataType isEqualToString:@"thumbnail_retina"] ||
        [self.dataType isEqualToString:@"photo"]  ||
        [self.dataType isEqualToString:@"photo_regsize"] ||
        [self.dataType isEqualToString:@"photo_regsize_retina"] ||
        [self.dataType isEqualToString:@"photo_thumbnail"] ||
        [self.dataType isEqualToString:@"photo_thumbnail_retina"]) {
        
        //     return @"vaderen_pictures"; // default
        return PICTURES_BUCKET;
        
    } else if ([self.dataType isEqualToString:@"highlight_video"] ||
               [self.dataType isEqualToString:@"trick_video"]) {
        return @"vaderen_videos";
    } else if ([self.dataType isEqualToString:@"venue_thumbnail"] ||
               [self.dataType isEqualToString:@"venue_pic"]) {
        //   return @"vaderen_venues";
        return VENUES_BUCKET;
    }
    
    
    //  return @"vaderen_pictures"; // default
    return PICTURES_BUCKET;
}

- (NSString *)getModifiedFilenameFromBase
{
    NSString *suffix = @"";
    NSString *resultString = @"";
    
    if ([self.dataType isEqualToString:@"thumbnail"]) {
        suffix = @"_thumbnail";
    } else if ([self.dataType isEqualToString:@"thumbnail_retina"]) {
        suffix = @"_thumbnail@2x";
    } else if ([self.dataType isEqualToString:@"photo_regsize"]) {
        suffix = @"_photo_regsize";
    } else if ([self.dataType isEqualToString:@"photo_regsize_retina"]) {
        suffix = @"_photo_regsize@2x";
    } else if ([self.dataType isEqualToString:@"photo_thumbnail"]) {
        suffix = @"_photo_thumbnail";
    } else if ([self.dataType isEqualToString:@"photo_thumbnail_retina"]) {
        suffix = @"_photo_thumbnail@2x";
    }
    
    if ([self.dataType isEqualToString:@"profile"] ||
        [self.dataType isEqualToString:@"thumbnail"] ||
        [self.dataType isEqualToString:@"thumbnail_retina"] ||
        [self.dataType isEqualToString:@"photo"] ||
        [self.dataType isEqualToString:@"photo_regsize"] ||
        [self.dataType isEqualToString:@"photo_regsize_retina"] ||
        [self.dataType isEqualToString:@"photo_thumbnail"] ||
        [self.dataType isEqualToString:@"photo_thumbnail_retina"]) {
        
        resultString = [NSString stringWithFormat:@"%@%@.%@", self.fileNameBase, suffix, self.fileExtension];
        
    } else if ([self.dataType isEqualToString:@"highlight_video"] ||
               [self.dataType isEqualToString:@"trick_video"]) {
        resultString = [NSString stringWithFormat:@"%@.%@", self.fileNameBase, @"mp4"];
    } else if ([self.dataType isEqualToString:@"venue_thumbnail"] ||
               [self.dataType isEqualToString:@"venue_pic"]) {
        resultString = [NSString stringWithFormat:@"%@%@.%@", self.fileNameBase, suffix, self.fileExtension];
    }
    
    NSLog(@"newstring = %@", resultString);
    
    return  resultString;
}

+ (NSMutableDictionary *)getFileNameBaseStringWithType:(NSString *)dataType forUser:(NSInteger)userID
{
    NSTimeInterval timeStamp = [[NSDate date] timeIntervalSince1970];
    // NSTimeInterval is defined as double
    NSNumber *timeStampObj = @(timeStamp); // get timestamp
    
    int r = arc4random() % 1000000; // random number
    
    NSString *ts = [NSString stringWithFormat:@"%ld%d", [timeStampObj integerValue], r];
    NSString *complete = [NSString stringWithFormat:@"%ld/%@/%@", userID, dataType, ts];
    
    NSMutableDictionary *returnDictionary = [[NSMutableDictionary alloc] init];
    [returnDictionary setObject:ts forKey:@"image_name"];
    [returnDictionary setObject:complete forKey:@"base_string"];
    
    return returnDictionary;
  //  return [NSString stringWithFormat:@"%ld/%@/%ld%d", userID, dataType, [timeStampObj integerValue], r]; // default
}


+ (NSString *)getFileNameBaseStringWithType:(NSString *)dataType forVenue:(NSInteger)venueID
{
    NSTimeInterval timeStamp = [[NSDate date] timeIntervalSince1970];
    // NSTimeInterval is defined as double
    NSNumber *timeStampObj = @(timeStamp); // get timestamp
    
    int r = arc4random() % 1000000; // random number
    
    return [NSString stringWithFormat:@"%ld/%@/%ld%d", venueID, dataType, [timeStampObj integerValue], r]; // default
    
}

// this call method takes a base file string and appends a suffix (if necessary such as 'thumbnail')
// and also a file extension

+ (NSString *)getFileNameStringWithType:(NSString *)dataType
                             baseString:(NSString *)baseFileStringIn

{
    /*
     * Will use facebook image if no image is set.
     */
    if ([baseFileStringIn rangeOfString:@"http"].location != NSNotFound) {
        return baseFileStringIn;
    }
    
    // Otherwise, must be an image saved to Amazon
    
    if ([baseFileStringIn isKindOfClass:[NSNull class]]) return @"";
    
    NSString *suffix = @"";
    NSString *fileExt = [baseFileStringIn pathExtension];
    
    // need to remove the filename extension
    NSString *baseFileString = [baseFileStringIn stringByDeletingPathExtension];
    
    if ([dataType isEqualToString:@"thumbnail"]) {
        suffix = @"_thumbnail";
    } else if ([dataType isEqualToString:@"thumbnail_retina"]) {
        suffix = @"_thumbnail@2x";
    } else if ([dataType isEqualToString:@"photo_regsize"]) {
        suffix = @"_photo_regsize";
    } else if ([dataType isEqualToString:@"photo_regsize_retina"]) {
        suffix = @"_photo_regsize@2x";
    } else if ([dataType isEqualToString:@"photo_thumbnail"]) {
        suffix = @"_photo_thumbnail";
    } else if ([dataType isEqualToString:@"photo_thumbnail_retina"]) {
        suffix = @"_photo_thumbnail@2x";
    }
    
    NSString *resultString = @"";
    
    if ([dataType isEqualToString:@"profile"] ||
        [dataType isEqualToString:@"thumbnail"] ||
        [dataType isEqualToString:@"thumbnail_retina"] ||
        [dataType isEqualToString:@"photo"] ||
        [dataType isEqualToString:@"photo_regsize"] ||
        [dataType isEqualToString:@"photo_regsize_retina"] ||
        [dataType isEqualToString:@"photo_thumbnail"] ||
        [dataType isEqualToString:@"photo_thumbnail_retina"]) {
        
        resultString = [NSString stringWithFormat:@"%@%@%@.%@", [S3Tools2 getBaseBucketURLForDataType:dataType], baseFileString, suffix, fileExt];
        
    } else if ([dataType isEqualToString:@"highlight_video"] ||
               [dataType isEqualToString:@"trick_video"]) {
        resultString = [NSString stringWithFormat:@"%@%@%@.mp4", [S3Tools2 getBaseBucketURLForDataType:dataType], baseFileString, suffix];
    } else if ([dataType isEqualToString:@"venue_thumbnail"] ||
               [dataType isEqualToString:@"venue_pic"]) {
        resultString = [NSString stringWithFormat:@"%@%@%@.%@", [S3Tools2 getBaseBucketURLForDataType:dataType], baseFileString, suffix, fileExt];
    }
    
    return resultString;
}

+ (NSString *)getBaseBucketURLForDataType:(NSString *)dataType
{
    // should do someplace closer than west 2...
    //     return @"https:/d6scf6uttw5kd.cloudfront.net/"; // default
    
    
    if ([dataType isEqualToString:@"profile"] ||
        [dataType isEqualToString:@"thumbnail"] ||
        [dataType isEqualToString:@"thumbnail_retina"] ||
        [dataType isEqualToString:@"photo"] ||
        [dataType isEqualToString:@"photo_regsize"] ||
        [dataType isEqualToString:@"photo_regsize_retina"] ||
        [dataType isEqualToString:@"photo_thumbnail"] ||
        [dataType isEqualToString:@"photo_thumbnail_retina"]) {
        
        return [NSString stringWithFormat:@"https://s3.amazonaws.com/%@/", PICTURES_BUCKET ];
        
    } else if ([dataType isEqualToString:@"highlight_video"] ||
               [dataType isEqualToString:@"trick_video"]) {
        
        return @"http://d9ks8a70ejduv.cloudfront.net/";
    } else if ([dataType isEqualToString:@"venue_thumbnail"] ||
               [dataType isEqualToString:@"venue_pic"]) {
        return [NSString stringWithFormat:@"https://s3.amazonaws.com/%@/", VENUES_BUCKET];
    }
    return nil;
    
}
@end
