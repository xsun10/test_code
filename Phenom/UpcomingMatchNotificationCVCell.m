//
//  UpcomingMatchNotificationCVCell.m
//  Vaiden
//
//  Created by James Chung on 2/5/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "UpcomingMatchNotificationCVCell.h"

@implementation UpcomingMatchNotificationCVCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
