//
//  CombinTagTVCell.h
//  Vaiden
//
//  Created by Turbo on 7/8/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CombinTagTVCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *statName;
@property (weak, nonatomic) IBOutlet UILabel *data;

@end
