//
//  ExperienceVC.m
//  Vaiden
//
//  Created by Turbo on 7/28/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "ExperienceVC.h"
#import "ChangeExperienceTVC.h"
#import "IOSRequest.h"
#import "UserPreferences.h"
#import "ExperienceTVCell.h"

@interface ExperienceVC () <ExperienceDelegate, UITableViewDataSource, UITableViewDelegate>
@property (assign) NSString *type;
@property (weak, nonatomic) IBOutlet UITableView *expTable;
@property (weak, nonatomic) IBOutlet UITableView *awdTable;
@property (nonatomic, strong) UserPreferences *preferences;
@property (nonatomic, strong) NSMutableArray *exp;
@property (nonatomic, strong) NSMutableArray *awd;
@property (weak, nonatomic) IBOutlet UIScrollView *pageScrollView;

@property (nonatomic) NSInteger changeID;
@property (nonatomic) NSString *name;
@property (nonatomic) NSString *date;
@property (nonatomic) NSString *sport;
@property (nonatomic) BOOL change;
@end

@implementation ExperienceVC
#define EXPERIENCE @"exp"
#define AWARD @"awd"

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (NSMutableArray *)exp
{
    if (!_exp) _exp = [[NSMutableArray alloc] init];
    return _exp;
}

- (NSMutableArray *)awd
{
    if (!_awd) _awd = [[NSMutableArray alloc] init];
    return _awd;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.expTable.delegate = self;
    self.expTable.dataSource = self;
    self.awdTable.delegate = self;
    self.awdTable.dataSource = self;
    
    [self setExtraCellLineHidden:self.expTable];
    [self setExtraCellLineHidden:self.awdTable];
    
    [self fetchExperienceAndAward];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.change = NO;
}

- (void)setExtraCellLineHidden: (UITableView *)tableView{
    UIView *view =[[UIView alloc]init];
    view.backgroundColor = [UIColor clearColor];
    [tableView setTableFooterView:view];
    [tableView setTableHeaderView:view];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    self.pageScrollView.contentSize = CGSizeMake(320.0, 575);
}

- (IBAction)addExpClicked:(id)sender {
    self.change = NO;
    self.type = EXPERIENCE;
    [self performSegueWithIdentifier:@"change_segue" sender:sender];
}

- (IBAction)addAwdClicked:(id)sender {
    self.change = NO;
    self.type = AWARD;
    [self performSegueWithIdentifier:@"change_segue" sender:sender];
}

#pragma mark - datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([tableView isEqual:self.expTable]) {
        return self.exp.count;
    } else {
        return self.awd.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView isEqual:self.expTable]) {
        ExperienceTVCell *cell = [tableView dequeueReusableCellWithIdentifier:@"exp_cell"];
        cell.tag = [[self.exp objectAtIndex:indexPath.row][@"id"] integerValue];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.title.text = [self.exp objectAtIndex:indexPath.row][@"team_name"];
        cell.time.text = [NSString stringWithFormat:@"%@ to %@",[self.exp objectAtIndex:indexPath.row][@"start"],[self.exp objectAtIndex:indexPath.row][@"end"]];
        cell.sport.text = [self.exp objectAtIndex:indexPath.row][@"sport"];
        
        return cell;
    } else {
        ExperienceTVCell *cell = [tableView dequeueReusableCellWithIdentifier:@"awd_cell"];
        cell.tag = [[self.awd objectAtIndex:indexPath.row][@"id"] integerValue];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.title.text = [self.awd objectAtIndex:indexPath.row][@"award_name"];
        cell.time.text = [self.awd objectAtIndex:indexPath.row][@"date"];
        cell.sport.text = [self.awd objectAtIndex:indexPath.row][@"sport"];
        
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 64;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.change = YES;
    if ([tableView isEqual:self.expTable]) {
        ExperienceTVCell *cell = (ExperienceTVCell *)[tableView cellForRowAtIndexPath:indexPath];
        self.type = EXPERIENCE;
        self.changeID = cell.tag;
        self.name = cell.title.text;
        self.date = cell.time.text;
        self.sport = cell.sport.text;
        [self performSegueWithIdentifier:@"change_segue" sender:self];
    } else {
        ExperienceTVCell *cell = (ExperienceTVCell *)[tableView cellForRowAtIndexPath:indexPath];
        self.type = AWARD;
        self.changeID = cell.tag;
        self.name = cell.title.text;
        self.date = cell.time.text;
        self.sport = cell.sport.text;
        [self performSegueWithIdentifier:@"change_segue" sender:self];
    }
}

- (void)fetchExperienceAndAward
{
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.labelText = @"Loading...";
    [IOSRequest fetchExperienceForUser:[self.preferences getUserID]
                                  onCompletion:^(NSDictionary *result) {
                                      if ([result[@"outcome"] isEqualToString:@"failure"]) {
                                          UIAlertView *alert = [[UIAlertView alloc] init];
                                          alert.title = @"Submission Error";
                                          alert.message = @"There was a problem submitting your request.  Please try again.";
                                          [alert addButtonWithTitle:@"OK"];
                                          [alert show];
                                      } else if ([result[@"outcome"] isEqualToString:@"success"]) {
                                          [self.exp removeAllObjects];
                                          self.type = EXPERIENCE;
                                          for (id object in result[@"list"]) {
                                              [self.exp addObject:object];
                                          }
                                          [self.expTable reloadData];
                                          [IOSRequest fetchAwardForUser:[self.preferences getUserID]
                                                           onCompletion:^(NSDictionary *res) {
                                                               if ([res[@"outcome"] isEqualToString:@"failure"]) {
                                                                   UIAlertView *alert = [[UIAlertView alloc] init];
                                                                   alert.title = @"Submission Error";
                                                                   alert.message = @"There was a problem submitting your request.  Please try again.";
                                                                   [alert addButtonWithTitle:@"OK"];
                                                                   [alert show];
                                                               } else if ([res[@"outcome"] isEqualToString:@"success"]) {
                                                                   [self.awd removeAllObjects];
                                                                   self.type = AWARD;
                                                                   for (id object in res[@"list"]) {
                                                                       [self.awd addObject:object];
                                                                   }
                                                                   [self.awdTable reloadData];
                                                                   [MBProgressHUD hideHUDForView:self.view animated:YES];
                                                               }
                                                           }];
                                      }
                                  }];
    double delayInSeconds = 10.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        //code to be executed on the main queue after delay
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    });
}

- (void)fetchExperience
{
    [IOSRequest fetchExperienceForUser:[self.preferences getUserID]
                          onCompletion:^(NSDictionary *result) {
                              if ([result[@"outcome"] isEqualToString:@"failure"]) {
                                  UIAlertView *alert = [[UIAlertView alloc] init];
                                  alert.title = @"Submission Error";
                                  alert.message = @"There was a problem submitting your request.  Please try again.";
                                  [alert addButtonWithTitle:@"OK"];
                                  [alert show];
                              } else if ([result[@"outcome"] isEqualToString:@"success"]) {
                                  [self.exp removeAllObjects];
                                  self.type = EXPERIENCE;
                                  for (id object in result[@"list"]) {
                                      [self.exp addObject:object];
                                  }
                                  [self.expTable reloadData];
                                  [MBProgressHUD hideHUDForView:self.view animated:YES];
                              }
                          }];
}

- (void)fetchAward
{
    [IOSRequest fetchAwardForUser:[self.preferences getUserID]
                     onCompletion:^(NSDictionary *res) {
                         if ([res[@"outcome"] isEqualToString:@"failure"]) {
                             UIAlertView *alert = [[UIAlertView alloc] init];
                             alert.title = @"Submission Error";
                             alert.message = @"There was a problem submitting your request.  Please try again.";
                             [alert addButtonWithTitle:@"OK"];
                             [alert show];
                         } else if ([res[@"outcome"] isEqualToString:@"success"]) {
                             [self.awd removeAllObjects];
                             self.type = AWARD;
                             for (id object in res[@"list"]) {
                                 [self.awd addObject:object];
                             }
                             [self.awdTable reloadData];
                             [MBProgressHUD hideHUDForView:self.view animated:YES];
                         }
                     }];
    double delayInSeconds = 10.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        //code to be executed on the main queue after delay
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    });
}

#pragma mark - Segue
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"change_segue"]) {
        ChangeExperienceTVC *controller = (ChangeExperienceTVC *)segue.destinationViewController;
        controller.type = self.type;
        if (self.change) {
            controller.changeID = self.changeID;
            controller.name = self.name;
            controller.time = self.date;
            controller.sportname = self.sport;
            controller.isChange = self.change;
        }
        controller.delegate = self;
    }
}

- (void)reloadTable:(NSString *)type
{
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.labelText = @"Loading...";
    if ([type isEqualToString:EXPERIENCE]) {
        [self fetchExperience];
    } else if ([type isEqualToString:AWARD]) {
        [self fetchAward];
    }
}

@end
