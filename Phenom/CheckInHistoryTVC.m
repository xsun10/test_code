//
//  CheckInHistoryTVC.m
//  Vaiden
//
//  Created by James Chung on 4/24/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "CheckInHistoryTVC.h"
#import "CheckInHistoryTVCell.h"
#import "IOSRequest.h"
#import "UserPreferences.h"
#import "NSDate+Utilities.h"
#import "S3Tools.h"
#import "AFNetworking.h"
#import "UIImage+ProportionalFill.h"
#import "VenueCheckIn.h"
#import "UIColor+VaidenColors.h"
#import "PlayerDetailTVC.h"

@interface CheckInHistoryTVC ()

@property (nonatomic, strong) NSMutableArray *checkInArray;
@property (nonatomic, strong) UserPreferences *preferences;
@property (nonatomic, strong) UIView *tempView;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
@property (nonatomic) NSInteger selectedUserID;
@property (nonatomic, strong) UIView *overlay;

@end

@implementation CheckInHistoryTVC

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (NSMutableArray *)checkInArray
{
    if (!_checkInArray) _checkInArray = [[NSMutableArray alloc] init];
    return _checkInArray;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
    [self showTempSpinner];
    
    [self loadCheckins];
}

- (void)showTempSpinner
{
    self.tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    self.tempView.backgroundColor = [UIColor lightGrayColor];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] init];
    [self.activityIndicator setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width/2.0, [UIScreen mainScreen].bounds.size.height/2.0 )];
    [self.activityIndicator startAnimating];
    [self.tempView addSubview:self.activityIndicator];
    
    [self.view addSubview:self.tempView];
    
}

- (void)displayErrorOverlay
{
    self.overlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, [UIScreen mainScreen].bounds.size.height)];
    self.overlay.backgroundColor = [UIColor lightLightGray];
    
    UIImage *img = [UIImage imageNamed:@"no_checkin"];
    UIImageView *icon = [[UIImageView alloc] initWithImage:img];
    [icon setFrame:CGRectMake(140, 100, 50, 50)];
    [icon setContentMode:UIViewContentModeCenter];
    
    UILabel *firstLine = [[UILabel alloc] initWithFrame:CGRectMake(30, 180, 260, 21)];
    firstLine.textColor = [UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:1.0];
    firstLine.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:15];
    firstLine.text = @"No Check-Ins";
    [firstLine setTextAlignment:NSTextAlignmentCenter];
    
    UILabel *secondLine = [[UILabel alloc] initWithFrame:CGRectMake(25, 200, 280, 42)];
    secondLine.textColor = [UIColor colorWithRed:170/255.0 green:170/255.0 blue:170/255.0 alpha:1.0];
    secondLine.font = [UIFont fontWithName:@"HelveticaNeue" size:13];
    
    if (self.showFollowingInCheckinHistory)
        secondLine.text = @"No one you follow has checked into this venue within the last hour. Be the first!";
    else
        secondLine.text = @"No one has checked into this venue yet.  Be the first to check-in!";
    
    secondLine.numberOfLines = 0;
    [secondLine setTextAlignment:NSTextAlignmentCenter];
    
    [self.overlay addSubview:icon];
    [self.overlay addSubview:firstLine];
    [self.overlay addSubview:secondLine];
    
    [self.tableView addSubview:self.overlay];
}

- (void)loadCheckins
{
    [IOSRequest getVenueCheckIns:self.venueID
                 withSessionUser:[self.preferences getUserID]
           showFollowingListOnly:self.showFollowingInCheckinHistory
                    onCompletion:^(NSMutableArray *results) {
                     
                     for (id object in results) {
                         NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
                         [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                         NSDate *dateTime = [[formatter dateFromString:object[@"check_in_ts"]] toLocalTime];
                         
                         Player *checkInPlayer = [[Player alloc] initWithPlayerDetails:[object[@"user_id"] integerValue]
                                                                           andUserName:object[@"username"]
                                                                   andProfilePicString:object[@"profile_pic_string"]];
                         
                         VenueCheckIn *vci = [[VenueCheckIn alloc] initWithCheckInID:[object[@"checkin_id"] integerValue]
                                                                   checkedInDateTime:dateTime
                                                                    checkedInMessage:object[@"message"]
                                                                            isPublic:[object[@"is_public"] boolValue]
                                                                               isNow:[object[@"is_now"] boolValue]
                                                                       checkedInUser:checkInPlayer
                                                                      checkedInSport:[[Sport alloc] initWithSportDetails:[object[@"sport_id"] integerValue]
                                                                                                                 andName:object[@"sport_name"]
                                                                                                                 andType:nil]];
                         [self.checkInArray addObject:vci];
                         
                     }
                     

                     
                     dispatch_async(dispatch_get_main_queue(), ^{
                         [self.activityIndicator stopAnimating];
                         self.tempView.hidden = YES;

                         if ([results count] == 0)
                             [self displayErrorOverlay];
                         else {
                             self.overlay.hidden = YES;
                             [self.overlay removeFromSuperview];
                         }
                         [self.tableView reloadData];
                     });
                     
                 }];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.checkInArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    VenueCheckIn *vci = [self.checkInArray objectAtIndex:indexPath.row];
    
    if ([vci.message length] == 0)
        return 65;
    
    return 80 + [self calculateHeightForTextPost:indexPath.row];
}

#define CELL_CONTENT_WIDTH 275
#define CELL_CONTENT_MARGIN 22.5
#define FONT_SIZE 13 // is actually 13

- (CGFloat)calculateHeightForTextPost:(NSUInteger)row
{
    VenueCheckIn *vci = [self.checkInArray objectAtIndex:row];
    NSString *text = vci.message;
    
    CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH, 1000.0f);
    
    /********** because sizeWithFont deprecated *************/
    
    NSMutableDictionary *attributes = [NSMutableDictionary new];
    [attributes setObject:[UIFont systemFontOfSize:FONT_SIZE] forKey:NSFontAttributeName];
    
    CGRect sizeRect = [text boundingRectWithSize:constraint options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
    
    CGSize size = sizeRect.size;
    //      CGSize size = [text sizeWithFont:[UIFont systemFontOfSize:FONT_SIZE] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    
    /********* end because sizeWithFont deprecated *********/
    
    return size.height;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CheckInHistoryTVCell *cell = [tableView dequeueReusableCellWithIdentifier:@"History Cell" forIndexPath:indexPath];
    
    VenueCheckIn *vci = [self.checkInArray objectAtIndex:indexPath.row];
    
    NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ %@", vci.checkedInUser.userName, [vci getCheckedInStatusMessage]]];

    [string addAttribute:NSForegroundColorAttributeName value:[UIColor goldColor] range:NSMakeRange(0, [vci.checkedInUser.userName length])];
    cell.usernameLabel.attributedText = string;
    
    NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                            baseString:vci.checkedInUser.profileBaseString];
    
    [cell.profilePic setImageWithURL:[NSURL URLWithString:url]
        placeholderImage:[UIImage imageNamed:@"avatar square"]];
    
    [UIImage makeRoundedImage:cell.profilePic withRadius:10];
    
    // profile pic button
    UIButton *picButton1 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    [picButton1 addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
    picButton1.tag = vci.checkedInUser.userID;
    [cell.profilePic addSubview:picButton1];
    cell.profilePic.userInteractionEnabled = YES;
    // end profile pic button
    
    cell.statusMessageLabel.text = [vci getCheckedInStatusMessage];
    
    if ([vci.message length] > 0) {
        cell.personalMessageLabel.hidden = NO;
        cell.personalMessageLabel.text =[NSString stringWithFormat:@"\"%@\"", vci.message];
    } else
        cell.personalMessageLabel.hidden = YES;
    
    return cell;
}

- (void)goToProfile:(UIButton *)sender
{
    self.selectedUserID = sender.tag;
    [self performSegueWithIdentifier:@"Player Detail Segue" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Player Detail Segue"]) {
        PlayerDetailTVC *controller = segue.destinationViewController;
        controller.playerUserID = self.selectedUserID;
    }
}

@end
