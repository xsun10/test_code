//
//  AddTemporaryVenueTVC.h
//  Vaiden
//
//  Created by James Chung on 12/30/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChooseStatesCDTVC.h"
#import "AddVenueSportsTVC.h"
#import "MBProgressHUD.h"
#import "CustomBaseTVC.h"

@class AddTemporaryVenueTVC;

@protocol AddTemporaryVenueTVC_Delegate <NSObject, MBProgressHUDDelegate>

- (void)setViewController:(AddTemporaryVenueTVC *)controller withVenueID:(NSInteger)venueID andVenueName:(NSString *)venueName;

@end

@interface AddTemporaryVenueTVC : CustomBaseTVC <ChooseStatesCDTVC_Delegate, AddVenueSportsTVC_Delegate>
{
    MBProgressHUD *HUD;
}

@property (nonatomic) NSInteger numVCsToPop;
@property (nonatomic, weak) id <AddTemporaryVenueTVC_Delegate>delegate;

@end


