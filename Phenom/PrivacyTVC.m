//
//  PrivacyTVC.m
//  Vaiden
//
//  Created by James Chung on 5/6/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "PrivacyTVC.h"
#import "UserPreferences.h"
#import "IOSRequest.h"

@interface PrivacyTVC ()

@property (nonatomic, strong) UserPreferences *preferences;
@property (strong, nonatomic)  UIBarButtonItem *doneButton;
@property (weak, nonatomic) IBOutlet UISwitch *followRequestOptionSwitch;
@property (nonatomic, strong) UIView *tempView;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;

@end

@implementation PrivacyTVC

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
    
    //[self setDoneButton];
    [self showTempSpinner];
    [self loadPrivacySettings];
}

- (void)showTempSpinner
{
    self.tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    self.tempView.backgroundColor = [UIColor lightGrayColor];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] init];
    [self.activityIndicator setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width/2.0, [UIScreen mainScreen].bounds.size.height/2.0 )];
    [self.activityIndicator startAnimating];
    [self.tempView addSubview:self.activityIndicator];
    
    [self.view addSubview:self.tempView];
    
}


/*- (void)setDoneButton
{
    UIButton *btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 50, 21);
    btn.layer.borderColor = [UIColor whiteColor].CGColor;
    btn.layer.borderWidth = 1.0;
    
    [btn addTarget:self action:@selector(done:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundColor:[UIColor clearColor]];
    [btn setTitle:@"DONE" forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-MediumItalic" size:12.0]];
    
    // Initialize UIBarbuttonitem...
    self.doneButton = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = self.doneButton;
}*/

- (IBAction)done:(id)sender
{
    [IOSRequest setPrivacySettingFollowRequestRequiresApprovalForUser:[self.preferences getUserID]
                                                      andApprovalFlag:self.followRequestOptionSwitch.isOn
                                                         onCompletion:^(NSDictionary *results) {
                                                             
                                                             dispatch_async(dispatch_get_main_queue(), ^{
                                                                 if ([results[@"outcome"] isEqualToString:@"failure"]) {
                                                                     UIAlertView *alert = [[UIAlertView alloc] init];
                                                                     alert.title = @"Save Error";
                                                                     alert.message = @"There was a problem completing your request.  Please try again later.";
                                                                     [alert addButtonWithTitle:@"OK"];
                                                                     [alert show];
                                                                 } else {
                                                                     [self.navigationController popViewControllerAnimated:YES];
                                                                 }
                                                             });
                                                             
                                                         }];
}

- (void)loadPrivacySettings
{
    [IOSRequest getPrivacySettingsForUser:[self.preferences getUserID]
                             onCompletion:^(NSDictionary *results) {
                                 
                                 dispatch_async(dispatch_get_main_queue(), ^{
                                     
                                     [self.activityIndicator stopAnimating];
                                     self.tempView.hidden = YES;
                                    
                                     if ([results[@"outcome"] isEqualToString:@"failure"]) {
                                         
                                     } else {
                                         [self.followRequestOptionSwitch setOn:[results[@"approval_flag"] boolValue] animated:YES];
                                     }
                                 });
                             }];
}

@end
