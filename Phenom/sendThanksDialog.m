//
//  sendThanksDialog.m
//  Vaiden
//
//  Created by Turbo on 7/17/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "sendThanksDialog.h"
#import "UIView+Manipulate.h"
#import "IOSRequest.h"
#import "UserPreferences.h"

@interface sendThanksDialog ()
@property (weak, nonatomic) IBOutlet UIButton *send;
@property (weak, nonatomic) IBOutlet UIButton *cancel;

@property (strong, nonatomic) UserPreferences *preferences;
@end

@implementation sendThanksDialog

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"%d", self.alertID);
    [self.send makeRoundedBorderWithRadius:3];
    [self.cancel makeRoundedBorderWithRadius:3];
}

- (IBAction)send:(id)sender {
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.labelText = @"Sending...";
    [IOSRequest thankFromUser:[self.preferences getUserID]
                       toUser:self.targetID
                     forAlert:self.alertID
                 onCompletion:^(NSDictionary *result) {
                     if ([result[@"outcome"] isEqualToString:@"failure"]) {
                         UIAlertView *alert = [[UIAlertView alloc] init];
                         alert.title = @"Submission Error";
                         alert.message = @"There was a problem submitting your request.  Please try again.";
                         [alert addButtonWithTitle:@"OK"];
                         [alert show];
                     } else if ([result[@"outcome"] isEqualToString:@"not_found"]) {
                         UIAlertView *alert = [[UIAlertView alloc] init];
                         alert.title = @"Submission Error";
                         alert.message = @"There was a problem submitting your request.  Please try again.";
                         [alert addButtonWithTitle:@"OK"];
                         [alert show];
                     } else if ([result[@"outcome"] isEqualToString:@"success"]) {
                         HUD.labelText = @" Sent! ";
                         double delayInSeconds = 1.0; // 3 seconds to dismiss the loading view
                         dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
                         dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                             //code to be executed on the main queue after delay
                             [MBProgressHUD hideHUDForView:self.view animated:YES];
                             // Dismiss the view if there is no vote selected
                             [self dismissViewControllerAnimated:NO completion:nil];
                         });
                     }
                 }];
    
    double delayInSeconds = 10.0; // 10 seconds to dismiss the loading view
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        //code to be executed on the main queue after delay
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    });
}


- (IBAction)cancel:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}

@end
