//
//  PickupMatchesNeedingActionsCell.m
//  Vaiden
//
//  Created by James Chung on 10/14/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "PickupMatchesNeedingActionsCell.h"
#import "PickupMatchesNeedingActionsCollectionCell.h"
#import "MatchPlayer.h"
#import "S3Tools.h"
#import "AFNetworking.h"
#import "UIImage+ProportionalFill.h"

@implementation PickupMatchesNeedingActionsCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)awakeFromNib
{
    self.playerCV.delegate = self;
    self.playerCV.dataSource = self;
}


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.competitors count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Player Image Cell";
    
    PickupMatchesNeedingActionsCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"status = %@", @"ACCEPTED"];
    NSArray *matchParticipants = [self.competitors filteredArrayUsingPredicate:predicate];
    
    if (indexPath.item < [matchParticipants count]) {
        MatchPlayer *player = matchParticipants[indexPath.item];
        

        NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                baseString:player.profileBaseString];
        
        [cell.profilePic setImageWithURL:[NSURL URLWithString:url]
                        placeholderImage:[UIImage imageNamed:@"avatar round"]];
        
        [UIImage makeRoundedImage:cell.profilePic withRadius:15];
        
    } else {
        [cell.profilePic setImage:[UIImage imageNamed:@"avatar round slot"]];
    }
    return cell;
}
@end
