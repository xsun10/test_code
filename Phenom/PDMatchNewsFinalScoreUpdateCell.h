//
//  PDMatchNewsFinalScoreUpdateCell.h
//  Vaiden
//
//  Created by James Chung on 2/23/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewsfeedMatchHeadingCV.h"
#import "IndividualMatch.h"

@class PDMatchNewsFinalScoreUpdateCell;

@protocol PDMatchNewsFinalScoreUpdateCell_delegate <NSObject>
-(void)setPDMatchNewsFinalScoreUpdateCellParentViewController:(PDMatchNewsFinalScoreUpdateCell *)controller withUserIDToSegue:(NSInteger)userID;
@end


@interface PDMatchNewsFinalScoreUpdateCell : UITableViewCell <UICollectionViewDataSource, UICollectionViewDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *newsfeedIcon;
@property (nonatomic, weak) id <PDMatchNewsFinalScoreUpdateCell_delegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *matchNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *player1Username;
@property (weak, nonatomic) IBOutlet UIButton *player1ImageButton;
@property (weak, nonatomic) IBOutlet UIButton *player2ImageButton;
@property (weak, nonatomic) IBOutlet UILabel *player2Username;
@property (weak, nonatomic) IBOutlet UIButton *detailsButton;
@property (weak, nonatomic) IBOutlet UILabel *sportLevelsHeader;
@property (weak, nonatomic) IBOutlet UILabel *player1UsernameV2;
@property (weak, nonatomic) IBOutlet UILabel *player2UsernameV2;

@property (weak, nonatomic) IBOutlet NewsfeedMatchHeadingCV *matchHeadingCV;
@property (weak, nonatomic) IBOutlet UIView *centerBackgroundView;
@property (weak, nonatomic) IBOutlet UIView *detailButtonView;
@property (weak, nonatomic) IBOutlet UILabel *sharedByLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *scoreCV;
@property (weak, nonatomic) IBOutlet UIImageView *sportIcon;
@property (weak, nonatomic) IBOutlet UILabel *sportLabel;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UILabel *postDateTimeLabel;
@property (weak, nonatomic) IBOutlet UIView *vsCircleView;
@property (weak, nonatomic) IBOutlet UIView *sportIconBackground;

@property (nonatomic, strong) NSString *matchCategory;
@property (nonatomic, strong) NSMutableArray *competitors;
@property (nonatomic, strong) IndividualMatch *iMatch;
@property (weak, nonatomic) IBOutlet UILabel *commentNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *commentTextLabel;
@property (weak, nonatomic) IBOutlet UIButton *commentButton;
@property (weak, nonatomic) IBOutlet UILabel *sharesNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *sharesTextLabel;
@property (weak, nonatomic) IBOutlet UIButton *sharesButton;
@property (weak, nonatomic) IBOutlet UIImageView *sportPicBackground;
@property (weak, nonatomic) IBOutlet UIImageView *userImage1;
@property (weak, nonatomic) IBOutlet UIImageView *userImage2;
@property (weak, nonatomic) IBOutlet UIView *shareButtonView;
@property (weak, nonatomic) IBOutlet UIImageView *shareButtonIcon;
@property (weak, nonatomic) IBOutlet UIView *commentButtonView;
@property (weak, nonatomic) IBOutlet UIImageView *commentButtonIcon;
@property (weak, nonatomic) IBOutlet UIButton *actionButtonHandle;

@property (weak, nonatomic) IBOutlet UIView *mainBackgroundViewArea;
@property (weak, nonatomic) IBOutlet UIView *smallCircleView;
@property (weak, nonatomic) IBOutlet UIButton *viewCommentsButton;

@property (weak, nonatomic) IBOutlet UIView *likeButtonBackground;
@property (weak, nonatomic) IBOutlet UIImageView *likeIcon;
@property (weak, nonatomic) IBOutlet UILabel *likeNumberLabel;
@property (weak, nonatomic) IBOutlet UIButton *likeButton;
@property (weak, nonatomic) IBOutlet UIButton *viewLikesButton;

@end
