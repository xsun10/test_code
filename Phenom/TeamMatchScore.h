//
//  TeamMatchScore.h
//  Vaiden
//
//  Created by James Chung on 4/16/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TeamMatchScore : NSObject

@property (nonatomic, strong) NSMutableArray *roundScoresTeamMatch;

// up to two versions of a match score are allowed
@property (nonatomic) NSInteger scoreRecordedBy;
@property (nonatomic) NSInteger scoreConfirmedBy;
@property (nonatomic) NSInteger scoreDisputedBy;
// so we know to display message to session user stating their score has been disputed
@property (nonatomic) BOOL wasFirstVersionTeamScoreRecordedBySessionUserAndDisputedByOtherUser;

@property (nonatomic) NSInteger version;

- (TeamMatchScore *) initWithMatchScore;

- (void)addRoundScore:(NSInteger)numRound
           forTeam1:(NSInteger)team1ID
     withScoreForTeam1:(NSInteger)scoreTeam1
           forTeam2:(NSInteger)team2ID
     withScoreForTeam2:(NSInteger)scoreTeam2;

- (NSMutableDictionary *)getNetScore;

@end
