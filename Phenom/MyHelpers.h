//
//  MyHelpers.h
//  Phenom
//
//  Created by James Chung on 5/1/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Player.h"

@interface MyHelpers : NSObject

+ (NSDate *)getDateTimeFromString:(NSString *)dateTimeString;

+ (NSMutableArray *)makeCompetitorArray:(NSArray *)competitorObjList forSportName:(NSString *)sportName andType:(NSString *)sportType;

+ (NSMutableArray *)getConfirmedList:(id)confirmedObjectList;

+ (NSMutableArray *)makeExperienceArray:(NSArray *)experienceObjectList;

+ (NSString *)getParsedWinRecordForString:(NSString *)winLossRecord;

+ (NSString *)getParsedLossRecordForString:(NSString *)winLossRecord;

+ (NSString *)getNewPicURLIfFacebookSquareSize:(NSString *)originalURL;

@end
