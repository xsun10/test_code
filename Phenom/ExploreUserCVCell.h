//
//  ExploreUserCVCell.h
//  Vaiden
//
//  Created by Turbo on 7/25/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExploreUserCVCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *image;

@end
