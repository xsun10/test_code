//
//  CheckInHistoryTVCell.h
//  Vaiden
//
//  Created by James Chung on 4/24/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CheckInHistoryTVCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *statusMessageLabel;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *profilePic;

@property (weak, nonatomic) IBOutlet UILabel *personalMessageLabel;
@end
