//
//  SendSupportEmailVC.m
//  Vaiden
//
//  Created by James Chung on 9/18/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "SendSupportEmailVC.h"

@interface SendSupportEmailVC ()

@end

@implementation SendSupportEmailVC


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self actionEmailComposer];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
    
  //  self.navigationController.navigationBar.tintColor = [UIColor whiteColor];

}


- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)actionEmailComposer
{
    if ([MFMailComposeViewController canSendMail]) {
        
        MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
        mailViewController.mailComposeDelegate = self;
        [mailViewController setToRecipients:@[@"support@vaiden.com"]];
        [mailViewController setSubject:@"Vaiden Support"];
        [mailViewController setMessageBody:[self myMessage] isHTML:YES];
        
        mailViewController.navigationBar.barTintColor = [UIColor blackColor];
        mailViewController.navigationBar.tintColor = [UIColor whiteColor];
        [mailViewController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
        mailViewController.navigationBar.translucent = NO;
        
        [self presentViewController:mailViewController animated:YES completion:nil];
        
    }
    
    else {
        
        NSLog(@"Device is unable to send email in its current state.");
    }
}

- (NSString *)myMessage
{
  //  NSString *test = @"<html><table><tr><td style=/background-color:black;\">hello</td><td>yes</td></tr></table></html>";
    return @"";
}
- (NSString *)getMessage
{
    NSString *ln1 = @"<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">";
    NSString *ln2 = @"<html xmlns=\"http://www.w3.org/1999/xhtml\">";
    NSString *ln3 = @"<head>";
    NSString *ln4 = @"<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />";
    NSString *ln5 = @"<meta name=\"viewport\" content=\"initial-scale=1.0\">";
    NSString *ln6 = @"<meta name=\"format-detection\" content=\"telephone=no\">";
    NSString *ln7 = @"<title>Slick: Responsive Email Templates</title>";
    NSString *ln8 = @"<style type=\"text/css\">";
    
    NSString *ln9 = @".ReadMsgBody { width: 100%; background-color: #ffffff;}";
    NSString *ln10 = @".ExternalClass {width: 100%; background-color: #ffffff;}";
    NSString *ln11 = @".ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height:100%;}";
    NSString *ln12 = @"html{width: 100%; }";
    NSString *ln13 = @"body {-webkit-text-size-adjust:none; -ms-text-size-adjust:none; }";
    NSString *ln14 = @"body {margin:0; padding:0;}";
    NSString *ln15 = @"table {border-spacing:0;}";
    NSString *ln16 = @"img{display:block !important;}";
    
    NSString *ln17 = @"table td {border-collapse:collapse;}";
    NSString *ln18 = @".yshortcuts a {border-bottom: none !important;}";
    
    
    
    
    
    
    
    
    NSString *ln19 = @"</style>";
    NSString *ln20 = @"</head>";
    
    NSString *ln21 = @"<body leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" >";
    NSString *ln22 = @"<!--start 100% wrapper (white background) -->";
    NSString *ln23 = @"<table width=\"100%\" height=";
    NSString *ln24 = @"\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" >";
    
    NSString *ln25 = @"<tr>";
    NSString *ln26 = @"<td align=\"center\" valign=\"top\" bgcolor=\"#ffffff\" style=\"background-color: #ffffff; border-bottom: 1px solid #d6d6d6;\" >";
    
    NSString *ln27 = @"<table width=\"590\"  align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"container\" bgcolor=\"#ffffff\">";
    NSString *ln28 = @"<tr>";
    NSString *ln29 = @"<td valign=\"top\" >";
    
    NSString *ln30 = @"<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"center\">";
    NSString *ln31 = @"<tr>";
    NSString *ln32 = @"<td height=\"5\"></td>";
    NSString *ln33 = @"</tr>";
    NSString *ln34 = @"</table>";
    NSString *ln35 = @"</body>";
    NSString *ln36 = @"</html>";
    
    NSString *returnMessage = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@%@", ln1, ln2, ln3,ln4, ln5, ln6, ln7, ln8, ln9, ln10, ln11, ln12, ln13, ln14, ln15, ln16, ln17, ln18, ln19, ln20, ln21, ln22, ln23, ln24, ln25, ln26, ln27, ln28, ln29, ln30, ln31, ln32, ln33, ln34, ln35, ln36 ];
    return  returnMessage;
    

}

@end
