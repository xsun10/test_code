//
//  AddIndividualMatchIntroVC.h
//  Vaiden
//
//  Created by James Chung on 11/20/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Player.h"
#import "CustomBaseVC.h"

@interface AddIndividualMatchIntroVC : CustomBaseVC

@property (nonatomic, strong) Player *player;

@end
