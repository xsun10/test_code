//
//  ChooseEventTagTVC.h
//  Vaiden
//
//  Created by James Chung on 5/28/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "CustomBaseTVC.h"
#import "EventTag.h"

@class ChooseEventTagTVC;

@protocol ChooseEventTagTVC_Delegate <NSObject>

- (void)setWithViewController:(ChooseEventTagTVC *)controller withEvent:(EventTag *)event;

@end


@interface ChooseEventTagTVC : CustomBaseTVC

@property (nonatomic, weak) id <ChooseEventTagTVC_Delegate> delegate;

@property (nonatomic) NSInteger sportID;
@end
