//
//  ChooseOneSportTVCell.h
//  Vaiden
//
//  Created by James Chung on 4/23/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChooseOneSportTVCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *sportPic;
@property (weak, nonatomic) IBOutlet UILabel *sportNameLabel;
@end
