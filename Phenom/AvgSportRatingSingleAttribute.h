//
//  AvgSportRating.h
//  Vaiden
//
//  Created by James Chung on 10/4/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AvgSportRatingSingleAttribute : NSObject

@property (nonatomic) float avgRating;
@property (nonatomic, strong) NSString *sportRatingsType;
@property (nonatomic) NSUInteger sportRatingsTypeID;
@property (nonatomic) NSUInteger numRatings;


-(AvgSportRatingSingleAttribute *)initWithAvg:(float)avgRating
                withNumRatings:(NSUInteger)numRatings
           forSportRatingsType:(NSString *)sportRatingsType
        withSportRatingsTypeID:(NSUInteger)sportRatingsTypeID;

- (UIImage *)getRatingImage;
@end
