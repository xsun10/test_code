//
//  RatingsVenuesListCell.m
//  Phenom
//
//  Created by James Chung on 7/5/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "RatingsVenuesListCell.h"

@implementation RatingsVenuesListCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (CGFloat)heightForData:(NSString *)comment andDate:(NSString *)dateString
{
    
    NSInteger commentTag = 1;
//    NSInteger dateTag = 2;
    
    
    if ([self viewWithTag:commentTag] == nil) {
//        itemTitle = [[UILabel alloc] initWithFrame:CGRectMake(65.0f, 28.0f, 243.0f, 200.0f)];
        self.commentLabel.frame = CGRectMake(65.0f, 69.0f, 243.0f, 400.0f);
        self.commentLabel.text = comment;
        self.commentLabel.adjustsFontSizeToFitWidth = NO;
        self.commentLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        self.commentLabel.font = [UIFont systemFontOfSize:13.0];
        self.commentLabel.lineBreakMode = NSLineBreakByWordWrapping;
        self.commentLabel.numberOfLines = 0;
        [self.commentLabel sizeToFit];
        
        
 //       [cell addSubview:itemTitle];
        
        
        float frameHeight = self.commentLabel.frame.size.height + 80;
        
        if (frameHeight >= 93)
            return frameHeight;
        else
            return 93;
    }
    
    return 93;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
