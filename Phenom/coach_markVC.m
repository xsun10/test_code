//
//  coach_markVC.m
//  Vaiden
//
//  Created by Turbo on 8/5/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "coach_markVC.h"
@interface coach_markVC ()
@property (weak, nonatomic) IBOutlet UILabel *NEXTlabel;
@property (weak, nonatomic) IBOutlet UIImageView *images;
@property (weak, nonatomic) IBOutlet UIView *backview;
@property (weak, nonatomic) IBOutlet UIButton *button;
@property(nonatomic,weak)NSString *title;

@end

@implementation coach_markVC
static int page=0;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] init];
    [tapGesture addTarget:self action:@selector(handleTap:)];
    [self.view addGestureRecognizer:tapGesture];
    NSString * imagename;
    if([self.title isEqualToString:@"coach_main"]){
      imagename= @"coach0";
    } else if([self.title isEqualToString:@"coach_discover"]){
    imagename= @"coach_discover";
    } else if([self.title isEqualToString:@"coach_rank"]){
        imagename= @"coach_rank";
    } else if([self.title isEqualToString:@"coach_support"]){
        imagename= @"coach_supporters";
    } else{ imagename= @"coach_boosting";
}
   
    UIImage *image = [UIImage imageNamed:imagename];
    self.images.image=image;
    
    self.backview.alpha=0;
    self.NEXTlabel.alpha=0;
    self.images.alpha=0;
    self.button.alpha=0;
  
}

-(void)viewDidAppear:(BOOL)animated
{
  
    [UIView animateWithDuration:1 animations:^ {
        self.backview.alpha=0.75;
        self.NEXTlabel.alpha=1;
        self.images.alpha=1;
        self.button.alpha=1;
    }];
}

- (void) handleTap:(UITapGestureRecognizer *)gesture
{
    if([self.title isEqualToString:@"coach_main"]){
    if(page<4){		
    page++;
    NSString * imagename = [[NSString alloc] initWithFormat:@"coach%d", page];
     UIImage *image = [UIImage imageNamed:imagename];
        self.images.image=image;}
    else {
        page=0;
        [UIView animateWithDuration:1 animations:^ {
            self.backview.alpha=0;
            self.NEXTlabel.alpha=0;
            self.images.alpha=0;
            self.button.alpha=0;
        } completion:^(BOOL finished) {
            [self dismissViewControllerAnimated:NO completion:nil];
        }];
    }
    }
    else if([self.title isEqualToString:@"coach_support"]){
        if(page<1){
        page++;
        NSString * imagename = @"coach_supported";
        UIImage *image = [UIImage imageNamed:imagename];
        self.images.image=image;}
    else {
        page=0;
        [UIView animateWithDuration:1 animations:^ {
            self.backview.alpha=0;
            self.NEXTlabel.alpha=0;
            self.images.alpha=0;
            self.button.alpha=0;
        } completion:^(BOOL finished) {
            [self dismissViewControllerAnimated:NO completion:nil];
        }];
    }}
    else{[UIView animateWithDuration:1 animations:^ {
        self.backview.alpha=0;
        self.NEXTlabel.alpha=0;
        self.images.alpha=0;
        self.button.alpha=0;
    } completion:^(BOOL finished) {
        [self dismissViewControllerAnimated:NO completion:nil];
    }];
}
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)close:(id)sender {
    [UIView animateWithDuration:1 animations:^ {
        self.backview.alpha=0;
        self.NEXTlabel.alpha=0;
        self.images.alpha=0;
        self.button.alpha=0;
    } completion:^(BOOL finished) {
        [self dismissViewControllerAnimated:NO completion:nil];
    }];

}

@end
