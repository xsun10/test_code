//
//  MatchHistoryObj.h
//  Vaiden
//
//  Created by James Chung on 1/29/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Player.h"

@interface MatchHistoryObj : NSObject

@property (nonatomic) NSUInteger matchHistoryID;
@property (nonatomic, strong) Player *participant;
@property (nonatomic, strong) NSString *update;
@property (nonatomic, strong) NSDate *occurrenceDate;

- (MatchHistoryObj *) initWithHistoryObject:(id)dataObject;



@end
