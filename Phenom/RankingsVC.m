//
//  RankingsVC.m
//  Vaiden
//
//  Created by Turbo on 7/21/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "RankingsVC.h"
#import "UIView+Manipulate.h"
#import "UserPreferences.h"
#import "IOSRequest.h"
#import "RankingsTCell.h"
#import "S3Tools.h"
#import "UIImageView+AFNetworking.h"
#import "UIImage+ProportionalFill.h"
#import "UIColor+VaidenColors.h"
#import "PlayerDetailTVC.h"

@interface RankingsVC () <UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *pageScrollView;
@property (nonatomic, strong) UserPreferences *preferences;
@property (weak, nonatomic) IBOutlet UIView *northeast;
@property (weak, nonatomic) IBOutlet UIView *midwest;
@property (weak, nonatomic) IBOutlet UIView *south;
@property (weak, nonatomic) IBOutlet UIView *southwest;
@property (weak, nonatomic) IBOutlet UIView *west;

@property (weak, nonatomic) IBOutlet UIImageView *map;
@property (nonatomic) NSString * region;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSMutableArray * result;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableHeight;

@property (assign) NSInteger targetID;
@property (strong, nonatomic) UIView *overlay;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftpadding;
@property (weak, nonatomic) IBOutlet UIView *test;
@end

@implementation RankingsVC

#define NORTHEAST @"Northeast"
#define SOUTHEAST @"Southeast"
#define MIDWEST @"Midwest"
#define SOUTH @"South"
#define WEST @"West"

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (NSMutableArray *)result
{
    if (!_result) _result = [[NSMutableArray alloc] init];
    return _result;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
    self.modalPresentationStyle = UIModalPresentationCurrentContext;
    self.tabBarController.modalPresentationStyle = UIModalPresentationCurrentContext;

    [self.northeast makeRoundedBorderWithRadius:3];
    [self.midwest makeRoundedBorderWithRadius:3];
    [self.south makeRoundedBorderWithRadius:3];
    [self.southwest makeRoundedBorderWithRadius:3];
    [self.west makeRoundedBorderWithRadius:3];
    
    [self checkRegionForUser];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    [self.pageScrollView setBounces:NO];
    [self.tableHeight setConstant:[[UIScreen mainScreen] bounds].size.height - 322];
    [self.tableView setBounces:YES];
    
    // Init the overlay view
    self.overlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, self.tableView.frame.size.height)];
    self.overlay.backgroundColor = [UIColor whiteColor];
    
    UIImage *img = [UIImage imageNamed:@"attention_grey"];
    UIImageView *icon = [[UIImageView alloc] initWithImage:img];
    [icon setFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width/2 - 10,30,20,20)];
    [icon setContentMode:UIViewContentModeCenter];
    
    UILabel *textLine = [[UILabel alloc] initWithFrame:CGRectMake(0, 50, self.tableView.frame.size.width, 21)];
    textLine.textColor = [UIColor colorWithRed:170/255.0 green:170/255.0 blue:170/255.0 alpha:1.0];
    textLine.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:11];
    textLine.text = @"There are no Vaiden users in this region.";
    [textLine setTextAlignment:NSTextAlignmentCenter];
    
    [self.overlay addSubview:icon];
    [self.overlay addSubview:textLine];
    
    [self setExtraCellLineHidden:self.tableView];
    [self firstlogin];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // set back button
    self.navigationController.navigationBar.topItem.title = @"RANKINGS";
    NSLog(@"%f",self.test.frame.origin.x);
}

- (void) viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    //[self.pageScrollView setContentSize:CGSizeMake(320, 542)];
}

- (void) checkRegionForUser
{
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.labelText = @"Loading...";
    
    [IOSRequest getLocationForUser:[self.preferences getUserID]
                      onCompletion:^(NSDictionary *result) {
                          NSLog(@"%@",result);
                          if ([result[@"outcome"] isEqualToString:@"success"]) {
                              NSArray* addresses = [(NSString *)result[@"location"] componentsSeparatedByString: @","];
                              NSString* state = [addresses objectAtIndex: 1];
                              [self getRegionForState:state]; // Translate the state code into region
                              [self updatePage];
                              [self getRankInRegion];
                          } else if ([result[@"outcome"] isEqualToString:@"failure"]) {
                              UIAlertView *alert = [[UIAlertView alloc] init];
                              alert.title = @"Submission Error";
                              alert.message = @"There was a problem submitting your request.  Please try again.";
                              [alert addButtonWithTitle:@"OK"];
                              [alert show];
                          }
                      }];
    // Delay 10s to dismiss the overlay
    double delayInSeconds = 10.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        //code to be executed on the main queue after delay
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    });
}

- (void) getRegionForState:(NSString *)state
{
    if ([state isEqualToString:@"CT"] || [state isEqualToString:@"DE"] || [state isEqualToString:@"ME"] || [state isEqualToString:@"MD"] || [state isEqualToString:@"MA"] || [state isEqualToString:@"NH"] || [state isEqualToString:@"NJ"] || [state isEqualToString:@"NY"] || [state isEqualToString:@"PA"] || [state isEqualToString:@"RI"] || [state isEqualToString:@"VT"]) {
        self.region = NORTHEAST;
    } else if ([state isEqualToString:@"AL"] || [state isEqualToString:@"AR"] || [state isEqualToString:@"FL"] || [state isEqualToString:@"GA"] || [state isEqualToString:@"KY"] || [state isEqualToString:@"LA"] || [state isEqualToString:@"MS"] || [state isEqualToString:@"NC"] || [state isEqualToString:@"SC"] || [state isEqualToString:@"TN"] || [state isEqualToString:@"VA"] || [state isEqualToString:@"WV"]) {
        self.region = SOUTHEAST;
    } else if ([state isEqualToString:@"IL"] || [state isEqualToString:@"IN"] || [state isEqualToString:@"IA"] || [state isEqualToString:@"KS"] || [state isEqualToString:@"NE"] || [state isEqualToString:@"ND"] || [state isEqualToString:@"MI"] || [state isEqualToString:@"MN"] || [state isEqualToString:@"MO"] || [state isEqualToString:@"OH"] || [state isEqualToString:@"SD"]) {
        self.region = MIDWEST;
    } else if ([state isEqualToString:@"AZ"] || [state isEqualToString:@"NM"] || [state isEqualToString:@"OK"] || [state isEqualToString:@"TX"]) {
        self.region = SOUTH;
    } else if ([state isEqualToString:@"AK"] || [state isEqualToString:@"CA"] || [state isEqualToString:@"CO"] || [state isEqualToString:@"HI"] || [state isEqualToString:@"ID"] || [state isEqualToString:@"MT"] || [state isEqualToString:@"NV"] || [state isEqualToString:@"UT"] || [state isEqualToString:@"WA"] || [state isEqualToString:@"WY"]) {
        self.region = WEST;
    }
}

- (void) updatePage
{
    if ([self.region isEqualToString:NORTHEAST]) {
        self.map.image = [UIImage imageNamed:@"northeast_selected"];
        [self.northeast setBackgroundColor:[UIColor colorWithRed:137/255.0 green:110/255.0 blue:57/255.0 alpha:0.7]];
        [self.west setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.midwest setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.south setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.southwest setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
    } else if ([self.region isEqualToString:SOUTHEAST]) {
        self.map.image = [UIImage imageNamed:@"southeast_selected"];
        [self.southwest setBackgroundColor:[UIColor colorWithRed:137/255.0 green:110/255.0 blue:57/255.0 alpha:0.7]];
        [self.northeast setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.midwest setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.south setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.west setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
    } else if ([self.region isEqualToString:MIDWEST]) {
        self.map.image = [UIImage imageNamed:@"midwest_selected"];
        [self.midwest setBackgroundColor:[UIColor colorWithRed:137/255.0 green:110/255.0 blue:57/255.0 alpha:0.7]];
        [self.northeast setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.west setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.south setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.southwest setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
    } else if ([self.region isEqualToString:SOUTH]) {
        self.map.image = [UIImage imageNamed:@"south_selected"];
        [self.south setBackgroundColor:[UIColor colorWithRed:137/255.0 green:110/255.0 blue:57/255.0 alpha:0.7]];
        [self.northeast setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.midwest setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.west setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.southwest setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
    } else if ([self.region isEqualToString:WEST]) {
        self.map.image = [UIImage imageNamed:@"west_selected"];
        [self.west setBackgroundColor:[UIColor colorWithRed:137/255.0 green:110/255.0 blue:57/255.0 alpha:0.7]];
        [self.northeast setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.midwest setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.south setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.southwest setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
    }
}

- (void) firstlogin
{
    [IOSRequest getLoginNums:[self.preferences getUserID]
                onCompletion:^(NSDictionary *results) {
                    NSLog(@"%@",results);
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if([results[@"num"] isEqualToString:@"1"]){
                            [self performSegueWithIdentifier:@"coach_segue" sender:self];
                            
                        }
                        
                    });
                }];
    
}

- (void)getRankInRegion
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.labelText = @"Loading...";
    
    [IOSRequest getRankingsInRegion:self.region
                           withUser:[self.preferences getUserID]
                       onCompletion:^(NSDictionary *result) {
                           if ([result[@"outcome"] isEqualToString:@"success"]) {
                               [self.result removeAllObjects];
                               NSDictionary * array = result[@"list"];
                               for (id obj in array) {
                                   [self.result addObject:obj];
                               }
                               
                               NSLog(@"%@", self.result);
                               [self.tableView reloadData];
                               if (self.result.count == 0) {
                                   [self addOverlay];
                               } else {
                                   [self removeOverlay];
                               }
                               [MBProgressHUD hideHUDForView:self.view animated:YES];
                           } else if ([result[@"outcome"] isEqualToString:@"failure"]) {
                               UIAlertView *alert = [[UIAlertView alloc] init];
                               alert.title = @"Submission Error";
                               alert.message = @"There was a problem submitting your request.  Please try again.";
                               [alert addButtonWithTitle:@"OK"];
                               [alert show];
                           }
                       }];
    // Delay 10s to dismiss the overlay
    double delayInSeconds = 10.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        //code to be executed on the main queue after delay
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    });
}

- (void)addOverlay
{
    [self.tableView setUserInteractionEnabled:NO];
    [self.tableView addSubview:self.overlay];
}

- (void)removeOverlay
{
    [self.tableView setUserInteractionEnabled:YES];
    [self.overlay removeFromSuperview];
}

#pragma - mark table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.result.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RankingsTCell *cell = (RankingsTCell *)[tableView dequeueReusableCellWithIdentifier:@"vote_cell"];
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    if ([[self.result objectAtIndex:indexPath.row][@"user_id"] integerValue] == [self.preferences getUserID]) {
        cell.backgroundColor = [UIColor colorWithRed:34/255.0 green:34/255.0 blue:34/255.0 alpha:1.0];
        cell.title.textColor = [UIColor whiteColor];
        cell.votes.textColor = [UIColor whiteColor];
    } else if (indexPath.row % 2 == 0) {
        cell.backgroundColor = [UIColor whiteColor];
        cell.title.textColor = [UIColor colorWithRed:34/255.0 green:34/255.0 blue:34/255.0 alpha:1.0];
        cell.votes.textColor = [UIColor colorWithRed:170/255.0 green:170/255.0 blue:170/255.0 alpha:1.0];
    } else {
        cell.backgroundColor = [UIColor colorWithRed:239/255.0 green:239/255.0 blue:244/255.0 alpha:1.0];
        cell.title.textColor = [UIColor colorWithRed:34/255.0 green:34/255.0 blue:34/255.0 alpha:1.0];
        cell.votes.textColor = [UIColor colorWithRed:170/255.0 green:170/255.0 blue:170/255.0 alpha:1.0];
    }
    
    NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                            baseString:[self.result objectAtIndex:indexPath.row][@"profilePicBaseString"]];
    
    [cell.profilePic setImageWithURL:[NSURL URLWithString:url]
                    placeholderImage:[UIImage imageNamed:@"avatar round"]];
    
    [UIImage makeRoundedImage:cell.profilePic withRadius:18];
    
    if (indexPath.row>9) {
        cell.title.text = [NSString stringWithFormat:@"%@. %@", [self.result objectAtIndex:indexPath.row][@"rank"], [self.result objectAtIndex:indexPath.row][@"fullname"]];
    } else {
        NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%d. %@", [[self.result objectAtIndex:indexPath.row][@"rank"] integerValue], [self.result objectAtIndex:indexPath.row][@"fullname"]]];
        [string addAttribute:NSForegroundColorAttributeName value:[UIColor goldColor] range:NSMakeRange(0, [NSString stringWithFormat:@"%d", [[self.result objectAtIndex:indexPath.row][@"rank"] integerValue]].length)];
        cell.title.attributedText = string;
    }
    
    cell.votes.text = [NSString stringWithFormat:@"%@ VOTES",[self.result objectAtIndex:indexPath.row][@"votes"]];
    
    cell.picButton.tag = [[self.result objectAtIndex:indexPath.row][@"user_id"] integerValue];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.targetID = [[self.result objectAtIndex:indexPath.row][@"user_id"] integerValue];
    [self performSegueWithIdentifier:@"profile_from_rank_segue" sender:self];
}

- (void)setExtraCellLineHidden: (UITableView *)tableView
{
    UIView *view =[ [UIView alloc]init];
    view.backgroundColor = [UIColor clearColor];
    [tableView setTableFooterView:view];
}

#pragma - mark actions
- (IBAction)westClicked:(id)sender {
    if (![self.region isEqualToString: WEST]) {
        self.region = WEST;
        self.map.image = [UIImage imageNamed:@"west_selected"];
        [self.west setBackgroundColor:[UIColor colorWithRed:137/255.0 green:110/255.0 blue:57/255.0 alpha:0.7]];
        [self.northeast setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.midwest setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.south setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.southwest setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self getRankInRegion];
    }
}

- (IBAction)northeastClicked:(id)sender {
    if (![self.region isEqualToString: NORTHEAST]) {
        self.region = NORTHEAST;
        self.map.image = [UIImage imageNamed:@"northeast_selected"];
        [self.northeast setBackgroundColor:[UIColor colorWithRed:137/255.0 green:110/255.0 blue:57/255.0 alpha:0.7]];
        [self.west setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.midwest setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.south setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.southwest setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self getRankInRegion];
    }
}

- (IBAction)midwestClicked:(id)sender {
    if (![self.region isEqualToString: MIDWEST]) {
        self.region = MIDWEST;
        self.map.image = [UIImage imageNamed:@"midwest_selected"];
        [self.midwest setBackgroundColor:[UIColor colorWithRed:137/255.0 green:110/255.0 blue:57/255.0 alpha:0.7]];
        [self.northeast setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.west setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.south setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.southwest setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self getRankInRegion];
    }
}

- (IBAction)southClicked:(id)sender {
    if (![self.region isEqualToString: SOUTH]) {
        self.region = SOUTH;
        self.map.image = [UIImage imageNamed:@"south_selected"];
        [self.south setBackgroundColor:[UIColor colorWithRed:137/255.0 green:110/255.0 blue:57/255.0 alpha:0.7]];
        [self.northeast setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.midwest setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.west setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.southwest setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self getRankInRegion];
    }
}

- (IBAction)southeastClicked:(id)sender {
    if (![self.region isEqualToString: SOUTHEAST]) {
        self.region = SOUTHEAST;
        self.map.image = [UIImage imageNamed:@"southeast_selected"];
        [self.southwest setBackgroundColor:[UIColor colorWithRed:137/255.0 green:110/255.0 blue:57/255.0 alpha:0.7]];
        [self.northeast setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.midwest setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.south setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.west setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self getRankInRegion];
    }
}

/*- (IBAction)profilePicButtonClicked:(id)sender {
    //[self performSegueWithIdentifier:@"profile_from_rank_segue" sender:sender];
}*/

#pragma - mark segue
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(UIButton *)sender
{
    if ([segue.identifier isEqualToString:@"profile_from_rank_segue"]) {
        PlayerDetailTVC *controller = segue.destinationViewController;
        controller.playerUserID = self.targetID;
        controller.fromComment = YES;
    }
    if([segue.identifier isEqualToString:@"coach_segue"])  {
        id theSegue = segue.destinationViewController;
        [theSegue setValue:@"coach_rank" forKey:@"title"];
    }
}
@end
