//
//  MatchNewsScoreUpdateScoreCVCell.h
//  Vaiden
//
//  Created by James Chung on 12/23/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MatchNewsScoreUpdateScoreCVCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;

@end
