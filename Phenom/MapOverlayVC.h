//
//  MapOverlayVC.h
//  Phenom
//
//  Created by James Chung on 5/16/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "MapViewVC.h"
#import "Venue.h"

@interface MapOverlayVC : MapViewVC

@property (nonatomic, strong) Venue *venue;

@end
