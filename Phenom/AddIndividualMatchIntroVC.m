//
//  AddIndividualMatchIntroVC.m
//  Vaiden
//
//  Created by James Chung on 11/20/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "AddIndividualMatchIntroVC.h"
#import "UIColor+VaidenColors.h"
#import "UIView+Manipulate.h"
#import "AddDirectIndividualMatchTVC.h"

@interface AddIndividualMatchIntroVC ()
@property (weak, nonatomic) IBOutlet UIView *iconBackground;
@property (weak, nonatomic) IBOutlet UIImageView *iconImage;
@property (weak, nonatomic) IBOutlet UILabel *challengeMatchHeadline;
@property (weak, nonatomic) IBOutlet UIScrollView *pageScrollView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *startButtonHandle;

@end

@implementation AddIndividualMatchIntroVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];

    
    [self.iconBackground makeCircleWithColorAndBackground:[UIColor peacock] andRadius:35];
    self.iconBackground.layer.borderWidth = 2.0;
    self.iconBackground.layer.borderColor = [UIColor whiteColor].CGColor;
    self.iconImage.image = [[UIImage imageNamed:@"Player Large"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.iconImage setTintColor:[UIColor whiteColor]];
    self.challengeMatchHeadline.textColor = [UIColor peacock];
   // [self.startButtonHandle setTitleTextAttributes:@{
   //                                      NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-LightItalic" size:14.0],
   //                                      NSForegroundColorAttributeName: [UIColor whiteColor]
   //                                      } forState:UIControlStateNormal];
    [self setStartButton];
}

- (void)setStartButton
{
    UIButton *btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 50, 21);
    btn.layer.borderColor = [UIColor whiteColor].CGColor;
    btn.layer.borderWidth = 1.0;
    
    [btn addTarget:self action:@selector(startAction:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundColor:[UIColor clearColor]];
    [btn setTitle:@"START" forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-MediumItalic" size:12.0]];
    
    // Initialize UIBarbuttonitem...
    UIBarButtonItem *proe = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = proe;
}



- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    self.pageScrollView.contentSize = CGSizeMake(320.0, 600.0);
}

- (IBAction)startAction:(id)sender
{
    [self performSegueWithIdentifier:@"Challenge Segue" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Challenge Segue"]) {
        AddDirectIndividualMatchTVC *controller = segue.destinationViewController;
        controller.userIDToChallenge = self.player.userID;
        controller.usernameToChallenge = self.player.userName;
        controller.challengeUserProfileBaseString = self.player.profileBaseString;
    }
}

@end
