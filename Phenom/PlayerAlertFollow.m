//
//  PlayerAlertFollow.m
//  Vaiden
//
//  Created by James Chung on 5/16/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "PlayerAlertFollow.h"

@implementation PlayerAlertFollow

- (PlayerAlertFollow *) initWithPlayerAlertDetails:(NSInteger)userID
                                 andUserName:(NSString *)userName
                         andProfilePicString:(NSString *)profilePicString
                                  andMessage:(NSString *)message
                                andAlertType:(NSInteger)alertType
                                andAlertDate:(NSDate *)alertDate
                                andWasViewed:(BOOL)wasViewed
                             andFollowStatus:(NSInteger)followStatus
{
        self = [super initWithPlayerAlertDetails:userID
                                     andUserName:userName
                             andProfilePicString:profilePicString
                                      andMessage:message
                                    andAlertType:alertType
                                    andAlertDate:alertDate
                                    andWasViewed:wasViewed];
    if (self) {
        self.refPlayer.followingStatus = followStatus;

    }
    
    return self;
}
@end
