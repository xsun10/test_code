//
//  PlayerVideoCell.h
//  Phenom
//
//  Created by James Chung on 6/4/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlayerVideoCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *thumbnail;
@property (weak, nonatomic) IBOutlet UILabel *sportLabel;

@end
