//
//  UITextView+FormValidations.h
//  Vaiden
//
//  Created by James Chung on 12/6/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextView (FormValidations)

- (BOOL)checkVenueDescription;

- (BOOL)checkMatchDescription;

@end
