//
//  VoteAlert.m
//  Vaiden
//
//  Created by Turbo on 7/17/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "VoteAlert.h"

@implementation VoteAlert
- (VoteAlert *) initWithVoteAlertDetails:(NSInteger)alertID
                                fromUser:(NSInteger)userID
                             andUserName:(NSString *)userName
                     andProfilePicString:(NSString *)profilePicString
                              andMessage:(NSString *)message
                            andAlertType:(NSInteger)alertType
                            andAlertDate:(NSDate *)alertDate
                            andWasViewed:(BOOL)wasViewed
                           andVoteNumber:(NSInteger)votes
                                 andSent:(NSInteger)sent
{
    self = [super init];
    
    if (self) {
        _refPlayer = [[Contact alloc] initWithPlayerDetails:userID
                                                andUserName:userName
                                        andProfilePicString:profilePicString];
        _message = message;
        _alertType = alertType;
        _wasViewed = wasViewed;
        _alertDateTime = alertDate;
        _votes = votes;
        _alertID = alertID;
        _sent = sent;
    }
    
    return self;
}

- (VoteAlert *) initWithVoteAlertThanksDetails:(NSInteger)userID
                                   andUserName:(NSString *)userName
                           andProfilePicString:(NSString *)profilePicString
                                    andMessage:(NSString *)message
                                  andAlertType:(NSInteger)alertType
                                  andAlertDate:(NSDate *)alertDate
                                  andWasViewed:(BOOL)wasViewed
{
    self = [super init];
    
    if (self) {
        _refPlayer = [[Contact alloc] initWithPlayerDetails:userID
                                                andUserName:userName
                                        andProfilePicString:profilePicString];
        _message = message;
        _alertType = alertType;
        _wasViewed = wasViewed;
        _alertDateTime = alertDate;
    }
    
    return self;
}

@end
