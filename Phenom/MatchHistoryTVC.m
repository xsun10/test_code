//
//  MatchHistoryTVC.m
//  Vaiden
//
//  Created by James Chung on 10/21/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "MatchHistoryTVC.h"
#import "IOSRequest.h"
#import "MatchHistoryCell.h"
#import "MatchHistoryWithCommentCell.h"
#import "MatchHistoryWithRightCommentCell.h"
#import "MatchNews.h"
#import "MatchPlayerNews.h"
#import "S3Tools.h"
#import "AFNetworking.h"
#import "UIImage+ProportionalFill.h"
#import "MyHelpers.h"
#import "UserPreferences.h"
#import "NSDate+Utilities.h"
#import "UIView+Bubbles.h"

@interface MatchHistoryTVC ()
@property (nonatomic, strong) NSMutableArray *matchHistoryPosts;

@property (nonatomic, strong) UITextField *textField;
@property (nonatomic, strong) UIToolbar *toolbar;
@property (nonatomic, strong) UserPreferences *preferences;

@property (nonatomic, strong) UIBarButtonItem *doneButton;
@property (weak, nonatomic) IBOutlet UIImageView *infoIcon;
@property (weak, nonatomic) IBOutlet UIView *fixedBar;



@end

@implementation MatchHistoryTVC

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
    
    [self.preferences setMatchNotificationRefreshState:YES];

    [self.matchHistoryPosts removeAllObjects];
    
    if ([self.playerMatch isKindOfClass:[IndividualMatch class]]) {
        [self loadIndividualMatchHistory];
    } else if ([self.playerMatch isKindOfClass:[PickupMatch class]]) {
        [self loadPickupMatchHistory];
    }
    
    if (self.shouldShowKeyboard)
        [self replaceBackButtonToShowDoneButton];
    
    self.infoIcon.image = [[UIImage imageNamed:@"Info Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.infoIcon setTintColor:[UIColor darkGrayColor]];
    
  
    
}


- (void)replaceBackButtonToShowDoneButton
{
//    self.navigationItem.title = @"Messages";
    
    UIButton *btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 60, 30);
    btn.layer.cornerRadius = 8;
    [btn setBackgroundColor:[UIColor darkGrayColor]];

    [btn setTitle:@"Done" forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(popToRootAction) forControlEvents:UIControlEventTouchUpInside];
    btn.titleLabel.font = [UIFont systemFontOfSize:15];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    // Initialize UIBarbuttonitem...
    UIBarButtonItem *proe = [[UIBarButtonItem alloc] initWithCustomView:btn];

    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -10;// it was -6 in iOS 6
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer, proe/*this will be the button which u actually need*/, nil] animated:NO];
    
 //   self.navigationItem.leftBarButtonItem = proe;

}

- (void)popToRootAction
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

// Match History

- (NSMutableArray *)matchHistoryPosts
{
    if (!_matchHistoryPosts) _matchHistoryPosts = [[NSMutableArray alloc] init];
    return _matchHistoryPosts;
}

- (void)loadIndividualMatchHistory
{
    [IOSRequest getIndividualMatchHistory:self.playerMatch.matchID
                             onCompletion:^(NSMutableArray *results) {
                                 
                                 for (id object in results) {
                                     IndividualMatch *im = [[IndividualMatch alloc] init];
                                     [im setMatchName:object[@"match_name"]];
                                     [im setMatchID:[object[@"match_id"] integerValue]];
                                     NSDate *postDate = [MyHelpers getDateTimeFromString:object[@"ts"]];
                                     
                                     
                                     
                                     if ([object[@"player_id"] integerValue] != 0) {
                                         // Then this is MatchPlayerNews object
                                         
                                         Player *player = [[Player alloc] initWithPlayerDetails:[object[@"player_id"] integerValue]
                                                                                    andUserName:object[@"username"]];
                                         [player setProfileBaseString:object[@"profile_pic_string"]];
                                         
                                         MatchPlayerNews *mpn = [[MatchPlayerNews alloc] initWithDetails:im
                                                                                                postDate:postDate
                                                                                         headLineMessage:object[@"username"]
                                                                                          generalMessage:object[@"general_message"]
                                                                                               forPlayer:player
                                                                                             withMessage:object[@"player_message"]]; //  latter message should be comments
                                         [self.matchHistoryPosts addObject:mpn];
                                         
                                     } else {
                                         // must be general match news post
                                         MatchNews *mn = [[MatchNews alloc] initWithDetails:im
                                                                                   postDate:postDate
                                                                            headLineMessage:@"Match Update"
                                                                             generalMessage:object[@"general_message"]];
                                         [self.matchHistoryPosts addObject:mn];
                                         
                                     }
                                 }
                                 
                                 dispatch_async(dispatch_get_main_queue(), ^{
                                     [self.tableView reloadData];
                                     if ([self.matchHistoryPosts count] > 5) {
                                         NSIndexPath *indexPath = [NSIndexPath indexPathForRow:([self.matchHistoryPosts count] - 1) inSection:0];
                                         [self.tableView scrollToRowAtIndexPath:indexPath
                                                              atScrollPosition:UITableViewScrollPositionBottom
                                                                      animated:NO];
                                     }

                                 });
                                 
                             }];
}

- (void)loadPickupMatchHistory
{
    [IOSRequest getPickupMatchHistory:self.playerMatch.matchID
                             onCompletion:^(NSMutableArray *results) {
                                 
                                 for (id object in results) {
                                     PickupMatch *pm = [[PickupMatch alloc] init];
                                     [pm setMatchName:object[@"match_name"]];
                                     [pm setMatchID:[object[@"match_id"] integerValue]];
                                     NSDate *postDate = [MyHelpers getDateTimeFromString:object[@"ts"]];
                                     
                                     
                                     
                                     if ([object[@"player_id"] integerValue] != 0) {
                                         // Then this is MatchPlayerNews object
                                         
                                         Player *player = [[Player alloc] initWithPlayerDetails:[object[@"player_id"] integerValue]
                                                                                    andUserName:object[@"username"]];
                                         [player setProfileBaseString:object[@"profile_pic_string"]];
                                         
                                         MatchPlayerNews *mpn = [[MatchPlayerNews alloc] initWithDetails:pm
                                                                                                postDate:postDate
                                                                                         headLineMessage:object[@"username"]
                                                                                          generalMessage:object[@"general_message"]
                                                                                               forPlayer:player
                                                                                             withMessage:object[@"player_message"]]; //  latter message should be comments
                                         [self.matchHistoryPosts addObject:mpn];
                                         
                                     } else {
                                         // must be general match news post
                                         MatchNews *mn = [[MatchNews alloc] initWithDetails:pm
                                                                                   postDate:postDate
                                                                            headLineMessage:@"Match Update"
                                                                             generalMessage:object[@"general_message"]];
                                         [self.matchHistoryPosts addObject:mn];
                                         
                                     }
                                 }
                                 
                                 dispatch_async(dispatch_get_main_queue(), ^{
                                     [self.tableView reloadData];
                                 });
                                 
                             }];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.matchHistoryPosts count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    id object = [self.matchHistoryPosts objectAtIndex:indexPath.row];
    
    
    // Need to check to see if there is a comment in the post. If there is a comment, will make it into MatchHistoryWithCommentCell...
    // Otherwise, use MatchHistoryCell
    
    BOOL hasComment = NO;
    
    if ([object isKindOfClass:[MatchPlayerNews class]]) {
        MatchPlayerNews *mpn = (MatchPlayerNews *)object;
        if ([mpn.playerMessage length] > 0)
            hasComment = YES;
        
    }
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMM d, YYYY h:mm a"];

    
    if ([object isKindOfClass:[MatchPlayerNews class]] && hasComment) {
       
        MatchPlayerNews *mpn = (MatchPlayerNews *)object;
        
        NSString*	s;
        CGSize		caps		= CGSizeMake(24, 13);
        UIFont*		font		= [UIFont systemFontOfSize:14];
        
     //   UIView*		bubble;
        
        // Create bubble
        s = mpn.playerMessage;
 //       if ([self.playerMatch isKindOfClass:[IndividualMatch class]]) {
      //      IndividualMatch *iMatch = (IndividualMatch *)self.playerMatch;
            
            if (mpn.player.userID != [self.preferences getUserID]) {
                static NSString *CellIdentifier = @"History With Comment Cell";
                
                MatchHistoryWithCommentCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
                
                [cell.messageBubble removeFromSuperview];
                cell.messageBubble = nil;

                CGFloat		padTRBL[4]	= {4, 16, 7, 22};
                cell.messageBubble = [UIView makeBubbleWithWidth:180 font:font text:s caps:caps padding:padTRBL withPosition:@"left"];
                cell.messageBubble.frame = CGRectMake(55, 35, cell.messageBubble.frame.size.width, cell.messageBubble.frame.size.height);
                
                cell.nameLabel.text = mpn.player.userName;
                //     cell.subheadLabel.text = mpn.matchGeneralMessage;
                
                NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                        baseString:mpn.player.profileBaseString];
                
                [cell.picView setImageWithURL:[NSURL URLWithString:url]
                             placeholderImage:[UIImage imageNamed:@"avatar round"]];
                [UIImage makeRoundedImage:cell.picView withRadius:20];
                
                UILabel *dateTime = [[UILabel alloc] initWithFrame:CGRectMake(60, cell.messageBubble.frame.size.height + 35, 120, 15)];
                dateTime.font = [UIFont fontWithName:@"HelveticaNeue" size:11];
                dateTime.text = [dateFormatter stringFromDate:mpn.newsDate];
                dateTime.textColor = [UIColor lightGrayColor];
                
                [cell.contentView addSubview:cell.messageBubble];
                [cell.contentView addSubview:dateTime];
                
                return cell;

            } else {
                static NSString *CellIdentifier = @"History With Right Comment Cell";
                
                MatchHistoryWithRightCommentCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
                [cell.messageBubble removeFromSuperview];
                cell.messageBubble = nil;

                CGFloat		padTRBL[4]	= {4, 22, 7, 16};
                cell.messageBubble = [UIView makeBubbleWithWidth:180 font:font text:s caps:caps padding:padTRBL withPosition:@"right"];
                cell.messageBubble.frame = CGRectMake(265, 35, -cell.messageBubble.frame.size.width, cell.messageBubble.frame.size.height);
                
                cell.nameLabel.text = mpn.player.userName;
                
                NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                        baseString:mpn.player.profileBaseString];
                
                [cell.picView setImageWithURL:[NSURL URLWithString:url]
                             placeholderImage:[UIImage imageNamed:@"avatar round"]];
                [UIImage makeRoundedImage:cell.picView withRadius:20];
                
                UILabel *dateTime = [[UILabel alloc] initWithFrame:CGRectMake(140, cell.messageBubble.frame.size.height + 35, 120, 15)];
                dateTime.font = [UIFont fontWithName:@"HelveticaNeue" size:11];
                dateTime.text = [dateFormatter stringFromDate:mpn.newsDate];
                dateTime.textColor = [UIColor lightGrayColor];
                dateTime.textAlignment = NSTextAlignmentRight;
                
                [cell.contentView addSubview:cell.messageBubble];
                [cell.contentView addSubview:dateTime];

                return cell;
                

            }
   /*     } else if ([self.playerMatch isKindOfClass:[PickupMatch class]]) {
            static NSString *CellIdentifier = @"History With Right Comment Cell";
            
            MatchHistoryWithRightCommentCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
            [cell.messageBubble removeFromSuperview];
            cell.messageBubble = nil;
            
            CGFloat		padTRBL[4]	= {4, 22, 7, 16};
            cell.messageBubble = [UIView makeBubbleWithWidth:180 font:font text:s caps:caps padding:padTRBL withPosition:@"right"];
            cell.messageBubble.frame = CGRectMake(265, 35, -cell.messageBubble.frame.size.width, cell.messageBubble.frame.size.height);
            
            cell.nameLabel.text = mpn.player.userName;
            
            NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                    baseString:mpn.player.profileBaseString];
            
            [cell.picView setImageWithURL:[NSURL URLWithString:url]
                         placeholderImage:[UIImage imageNamed:@"avatar round"]];
            [UIImage makeRoundedImage:cell.picView withRadius:20];
            
            UILabel *dateTime = [[UILabel alloc] initWithFrame:CGRectMake(140, cell.messageBubble.frame.size.height + 35, 120, 15)];
            dateTime.font = [UIFont fontWithName:@"HelveticaNeue" size:11];
            dateTime.text = [dateFormatter stringFromDate:mpn.newsDate];
            dateTime.textColor = [UIColor lightGrayColor];
            dateTime.textAlignment = NSTextAlignmentRight;
            
            [cell.contentView addSubview:cell.messageBubble];
            [cell.contentView addSubview:dateTime];
            
            return cell;
            
        } */
        
        
 //   } else if ([object isKindOfClass:[MatchNews class]]) {
        /*
    } else if ([object isKindOfClass:[MatchPlayerNews class]]) {
        
        static NSString *CellIdentifier = @"History Cell";
        MatchHistoryCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        MatchPlayerNews *mpn = (MatchPlayerNews *)object;
        
        cell.headlineLabel.text = mpn.player.userName;
        cell.subheadLabel.text = mpn.matchGeneralMessage;
        NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                baseString:mpn.player.profileBaseString];
        
        [cell.picView setImageWithURL:[NSURL URLWithString:url]
                     placeholderImage:[UIImage imageNamed:@"avatar round"]];
        [UIImage makeRoundedImage:cell.picView withRadius:20];
        
        cell.dateTimeLabel.text = [dateFormatter stringFromDate:mpn.newsDate];
        
        return cell;

        
    } else {
        static NSString *CellIdentifier = @"History Cell";

        MatchHistoryCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        MatchNews *mn = (MatchNews *)object;
        cell.headlineLabel.text = mn.match.matchName;
        cell.subheadLabel.text = mn.matchGeneralMessage;
        cell.picView.image = [self.playerMatch.sport getSportIcon];
        cell.dateTimeLabel.text = [dateFormatter stringFromDate:mn.newsDate];
        
        return cell; */
    }

    return nil;
    
}
#define FONT_SIZE 14.0f // was 17
#define CELL_CONTENT_WIDTH 180.0f // was 280
#define CELL_CONTENT_MARGIN 20.0f

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    id object = [self.matchHistoryPosts objectAtIndex:indexPath.row];

    if ([object isKindOfClass:[MatchPlayerNews class]]) {
        MatchPlayerNews *mpn = (MatchPlayerNews *)object;
        if ([mpn.playerMessage length] > 0) {
           
            NSString *text = mpn.playerMessage;
            
            
            // Get a CGSize for the width and, effectively, unlimited height
            CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH - (CELL_CONTENT_MARGIN * 2), 20000.0f);
            // Get the size of the text given the CGSize we just made as a constraint
            CGSize size = [text sizeWithFont:[UIFont systemFontOfSize:FONT_SIZE] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
            // Get the height of our measurement, with a minimum of 71 (standard cell size)
            CGFloat height = MAX(size.height + 70, 95.0f);
            // return the height, with a bit of extra padding in
      
            return height + (CELL_CONTENT_MARGIN * 2) - 60;

            
        } else {
            return 62;
        }
    } else if ([object isKindOfClass:[MatchNews class]]) {
        MatchNews *mn = (MatchNews *)object;
        if ([mn.headlineMessageLabel length] > 0) {
            NSString *text = mn.headlineMessageLabel;
            
            // Get a CGSize for the width and, effectively, unlimited height
            CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH - (CELL_CONTENT_MARGIN * 2), 20000.0f);
            // Get the size of the text given the CGSize we just made as a constraint
            CGSize size = [text sizeWithFont:[UIFont systemFontOfSize:FONT_SIZE] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
            // Get the height of our measurement, with a minimum of 71 (standard cell size)
            CGFloat height = MAX(size.height + 75, 80.0f);
            // return the height, with a bit of extra padding in
            
//            return height + (CELL_CONTENT_MARGIN * 2) - 20;
     //       return 62;
             return height + (CELL_CONTENT_MARGIN * 2) - 40;
        }
    }
    
    return  85;
}



- (IBAction)composeComment:(id)sender
{
    [self composeCommentImplementation];
    if ([self.matchHistoryPosts count] >= 1) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:([self.matchHistoryPosts count] - 1) inSection:0];
        [self.tableView scrollToRowAtIndexPath:indexPath
                          atScrollPosition:UITableViewScrollPositionBottom
                                  animated:YES];
    }

}

- (void)composeCommentImplementation
{
    self.toolbar = [[UIToolbar alloc] init];
    [self.toolbar setBarStyle:UIBarStyleBlack];
    [self.toolbar sizeToFit];
    
    self.textField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 235, 30)];
    self.textField.backgroundColor = [UIColor whiteColor];
    self.textField.borderStyle = UITextBorderStyleRoundedRect;
    self.textField.font = [UIFont fontWithName:@"System" size:12];
    
    UIBarButtonItem *textFieldItem = [[UIBarButtonItem alloc] initWithCustomView:self.textField];
    
    
    
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    // Since the buttons can be any width we use a thin image with a stretchable center point
    UIImage *buttonImage = [[UIImage imageNamed:@"blue_button_med"] stretchableImageWithLeftCapWidth:5 topCapHeight:0];
    UIImage *buttonPressedImage = [[UIImage imageNamed:@"blue_button_med.png"] stretchableImageWithLeftCapWidth:5 topCapHeight:0];
    
    [[button titleLabel] setFont:[UIFont boldSystemFontOfSize:12.0]];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [button setTitleShadowColor:[UIColor colorWithWhite:1.0 alpha:0.7] forState:UIControlStateNormal];
    [button setTitleShadowColor:[UIColor clearColor] forState:UIControlStateHighlighted];
    [[button titleLabel] setShadowOffset:CGSizeMake(0.0, 1.0)];
    
    CGRect buttonFrame = [button frame];
    NSString *t = @"Submit";
    
    buttonFrame.size.width = [t sizeWithFont:[UIFont boldSystemFontOfSize:12.0]].width + 24.0;
    buttonFrame.size.height = 30;
    [button setFrame:buttonFrame];
    
    [button setBackgroundImage:buttonImage forState:UIControlStateNormal];
    [button setBackgroundImage:buttonPressedImage forState:UIControlStateHighlighted];
    
    [button setTitle:t forState:UIControlStateNormal];
    
    [button addTarget:self action:@selector(recordComment) forControlEvents:UIControlEventTouchUpInside];
    
    self.doneButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    
    [self.doneButton setTitleTextAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:12]} forState:UIControlStateNormal];
    
    
    NSArray *itemsArray = @[textFieldItem, self.doneButton];
    
    
    [self.toolbar setItems:itemsArray];
    [self.view.window addSubview:self.toolbar];
    
    [self.textField setInputAccessoryView:self.toolbar];
    [self.textField becomeFirstResponder]; [self.textField becomeFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.textField resignFirstResponder];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (self.shouldShowKeyboard)
        [self composeCommentImplementation];
    else
        [self.textField becomeFirstResponder];
    
}

- (void)recordComment
{
    if ([self.textField.text length] > 200) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Message Posting Error";
        alert.message = @"Sorry but your message cannot exceed 200 characters in length.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
    } else if ([self.textField.text length] == 0) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Message Posting Error";
        alert.message = @"Sorry but you cannot submit a blank message";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
    } else {
        NSString *matchCategory = @"";
    
        if ([self.playerMatch isKindOfClass:[IndividualMatch class]]) {
            matchCategory = @"individual";
        } else {
            matchCategory = @"pickup";
        }
    
        self.doneButton.enabled = NO;
        HUD = [MBProgressHUD showHUDAddedTo:self.view.superview animated:YES];
        HUD.labelText = @"Submitting";
        [self.textField resignFirstResponder];

        [IOSRequest recordComment:self.textField.text
                       byUser:[self.preferences getUserID]
                     forMatch:self.playerMatch.matchID
            withMatchCategory:matchCategory
                 onCompletion:^(NSDictionary *results) {
                     
                     if (![results[@"outcome"] isEqualToString:@"failure"]) {
                         NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
                         [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                         NSDate *dateTime = [[formatter dateFromString:results[@"ts"]] toLocalTime];
                         
                         Player *sessionPlayer = [[Player alloc] initWithPlayerDetails:[self.preferences getUserID]
                                                                           andUserName:[self.preferences getUserName] andProfilePicString:[self.preferences getProfilePicString]];
                         
                         MatchPlayerNews *mpn = [[MatchPlayerNews alloc] initWithDetails:self.playerMatch
                                                                                postDate:dateTime
                                                                         headLineMessage:[self.preferences getUserName]
                                                                          generalMessage:@"commented on this match" forPlayer:sessionPlayer
                                                                             withMessage:self.textField.text];
                         [self.matchHistoryPosts addObject:mpn];

                     }
                     
                     dispatch_async(dispatch_get_main_queue(), ^{
                         [MBProgressHUD hideHUDForView:self.view.superview animated:YES];
                         self.doneButton.enabled = YES;
                         
                         if (![results[@"outcome"] isEqualToString:@"failure"]) {
                             [self.tableView reloadData];
                             NSIndexPath *indexPath = [NSIndexPath indexPathForRow:([self.matchHistoryPosts count] - 1) inSection:0];
                             [self.tableView scrollToRowAtIndexPath:indexPath
                                               atScrollPosition:UITableViewScrollPositionBottom
                                                       animated:YES];
                         } else {
                             UIAlertView *alert = [[UIAlertView alloc] init];
                             alert.title = @"Message Recording Error";
                             alert.message = @"There was an error in submitting your message.  Please try again.";
                             [alert addButtonWithTitle:@"OK"];
                             [alert show];
                         }
                     });
                     
                 }];
        
        double delayInSeconds = 10.0;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            //code to be executed on the main queue after delay
            [MBProgressHUD hideHUDForView:self.view.superview animated:YES];// just
        });
    }
}


@end
