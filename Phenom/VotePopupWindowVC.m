//
//  VotePopupWindowVC.m
//  Vaiden
//
//  Created by Turbo on 7/15/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "VotePopupWindowVC.h"
#import "UIView+Manipulate.h"
#import "IOSRequest.h"
#import "UserPreferences.h"

@interface VotePopupWindowVC () <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIView *window;
@property (weak, nonatomic) IBOutlet UIView *inputView;
@property (weak, nonatomic) IBOutlet UIButton *voteButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;

@property (weak, nonatomic) IBOutlet UITextField *numberTextField;
@property (weak, nonatomic) IBOutlet UILabel *totalVoteNumber;

@property (strong, nonatomic) UserPreferences *preferences;
@property (nonatomic) BOOL isVoted;
@property (nonatomic, strong) NSTimer *longPressTimer;

@property (weak, nonatomic) IBOutlet UIButton *upButton;
@property (weak, nonatomic) IBOutlet UIButton *downButton;
@end

@implementation VotePopupWindowVC

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // UI config
    [self.voteButton makeRoundedBorderWithRadius:3];
    [self.cancelButton makeRoundedBorderWithRadius:3];
    [self.window makeRoundedBorderWithRadius:5];
    [self.inputView makeRoundedBorderWithRadius:5];
    [self.inputView.layer setBorderColor:[[UIColor colorWithRed:239/255.0 green:239/255.0 blue:244/255.0 alpha:1.0] CGColor]];
    
    [self.numberTextField setKeyboardType:UIKeyboardTypeNumberPad];
    self.numberTextField.delegate = self;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] init];
    [tapGesture addTarget:self action:@selector(handleTap:)];
    [self.view addGestureRecognizer:tapGesture];
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPress:)];
    [self.upButton addGestureRecognizer:longPress];
    UILongPressGestureRecognizer *longPress2 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPress2:)];
    [self.downButton addGestureRecognizer:longPress2];

    
    // Set the total votes for user
    self.isVoted = NO;
    [self getTotalVoteForUser];
}

- (void) handleTap:(UITapGestureRecognizer *)gesture
{
    [self.numberTextField resignFirstResponder];
}

- (void) longPress:(UILongPressGestureRecognizer *)gesture
{
    // The long press gesture recognizer has been, well, recognized
    if (gesture.state == UIGestureRecognizerStateBegan) {
        if (self.longPressTimer) {
            [self.longPressTimer invalidate];
            self.longPressTimer = nil;
        }
        
        self.longPressTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(longPressTimer:) userInfo:nil repeats:YES];
    }
    
    if (gesture.state == UIGestureRecognizerStateEnded || gesture.state == UIGestureRecognizerStateCancelled) {
        [self.longPressTimer invalidate];
        self.longPressTimer = nil;
    }
}

- (void) longPress2:(UILongPressGestureRecognizer *)gesture
{
    // The long press gesture recognizer has been, well, recognized
    if (gesture.state == UIGestureRecognizerStateBegan) {
        if (self.longPressTimer) {
            [self.longPressTimer invalidate];
            self.longPressTimer = nil;
        }
        // Repeat add the number when holding the button
        self.longPressTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(longPressTimer2:) userInfo:nil repeats:YES];
    }
    
    if (gesture.state == UIGestureRecognizerStateEnded || gesture.state == UIGestureRecognizerStateCancelled) {
        [self.longPressTimer invalidate];
        self.longPressTimer = nil;
    }
}

-(void)longPressTimer:(NSTimer*)timer {
    NSInteger num = [self.numberTextField.text integerValue];
    if (num < [self.totalVoteNumber.text integerValue]) {
        self.numberTextField.text = [NSString stringWithFormat:@"%d", ++num];
    }
}

-(void)longPressTimer2:(NSTimer*)timer {
    NSInteger num = [self.numberTextField.text integerValue];
    if (num > 0) {
        self.numberTextField.text = [NSString stringWithFormat:@"%d", --num];
    }
}

- (void) getTotalVoteForUser
{
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.labelText = @"Loading...";
    
    [IOSRequest getTotalVoteForUser:[self.preferences getUserID]
                       onCompletion:^(NSDictionary *result) {
                           NSLog(@"%@", result);
                           if ([result[@"outcome"] isEqualToString:@"failure"]) {
                               UIAlertView *alert = [[UIAlertView alloc] init];
                               alert.title = @"Submission Error";
                               alert.message = @"There was a problem submitting your request.  Please try again.";
                               [alert addButtonWithTitle:@"OK"];
                               [alert show];
                           } else if ([result[@"outcome"] isEqualToString:@"not_found"]) {
                               UIAlertView *alert = [[UIAlertView alloc] init];
                               alert.title = @"Submission Error";
                               alert.message = @"There was a problem submitting your request.  Please try again.";
                               [alert addButtonWithTitle:@"OK"];
                               [alert show];
                           } else if ([result[@"outcome"] isEqualToString:@"no_result"]) {
                               [self setTotalVoteNumber:self.totalVoteNumber withNumber:0];
                           } else if ([result[@"outcome"] isEqualToString:@"success"]) {
                               if ([(NSString *)result[@"voted"] isEqualToString:@"1"]) {
                                   self.isVoted = YES;
                               } else {
                                   self.isVoted = NO;
                               }
                               [self setTotalVoteNumber:self.totalVoteNumber withNumber:[(NSString *)result[@"vote_num"] integerValue]];
                           }
                           [MBProgressHUD hideHUDForView:self.view animated:YES];
                       }];
    
    double delayInSeconds = 10.0; // 10seconds to dismiss the loading view
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        //code to be executed on the main queue after delay
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    });
}

- (void) setTotalVoteNumber:(UILabel *)label withNumber:(NSInteger)num
{
    NSInteger total = 0;
    if (!self.isVoted) {
        total = num + 1; // 1 for expiring vote each day
        if (total > 1) {
            label.text = [NSString stringWithFormat:@"%d votes",total];
        } else {
            label.text = [NSString stringWithFormat:@"%d vote",total];
        }
    } else {
        if (num > 1) {
            label.text = [NSString stringWithFormat:@"%d votes",num];
        } else {
            label.text = [NSString stringWithFormat:@"%d vote",num];
        }
    }
}

#pragma - mark Textfield Delegate
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField.text.length == 0) {
        textField.text = @"0";
    } if([textField.text integerValue] > [self.totalVoteNumber.text integerValue]) {
        textField.text = [NSString stringWithFormat:@"%d", [self.totalVoteNumber.text integerValue]]; // Save the maximum of what they have
    } else {
        textField.text = [NSString stringWithFormat:@"%d", [textField.text integerValue]]; // Cancel the first zero of the input
    }
}

#pragma - mark ACTION
- (IBAction)upButtonClicked:(id)sender {
    NSInteger total = [self.totalVoteNumber.text integerValue];
    NSInteger current = [self.numberTextField.text integerValue];
    if (current < total) {
        self.numberTextField.text = [NSString stringWithFormat:@"%d", ++current];
    }
}

- (IBAction)downButtonClicked:(id)sender {
    NSInteger current = [self.numberTextField.text integerValue];
    if (current > 0) {
        self.numberTextField.text = [NSString stringWithFormat:@"%d", --current];
    }
}

- (IBAction)vote:(id)sender {
    if ([self.numberTextField.text integerValue] > 0) {
        HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        HUD.labelText = @"Voting...";
        
        [IOSRequest voteFromUser:[self.preferences getUserID] toUser:self.targetID withVotes:[self.numberTextField.text integerValue] onCompletion:^(NSDictionary *result) {
            if ([result[@"outcome"] isEqualToString:@"failure"]) {
                UIAlertView *alert = [[UIAlertView alloc] init];
                alert.title = @"Submission Error1";
                alert.message = @"There was a problem submitting your request.  Please try again.";
                [alert addButtonWithTitle:@"OK"];
                [alert show];
            } else if ([result[@"outcome"] isEqualToString:@"no_target"]) {
                UIAlertView *alert = [[UIAlertView alloc] init];
                alert.title = @"Submission Error2";
                alert.message = @"There was a problem submitting your request.  Please try again.";
                [alert addButtonWithTitle:@"OK"];
                [alert show];
            } else if ([result[@"outcome"] isEqualToString:@"not_found"]) {
                UIAlertView *alert = [[UIAlertView alloc] init];
                alert.title = @"Submission Error3";
                alert.message = @"There was a problem submitting your request.  Please try again.";
                [alert addButtonWithTitle:@"OK"];
                [alert show];
            } else if ([result[@"outcome"] isEqualToString:@"success"]) {
                HUD.labelText = @" Voted! ";
                [self.delegate refreshVoteInfo];
                double delayInSeconds = 1.0; // 3 seconds to dismiss the loading view
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                    //code to be executed on the main queue after delay
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                    // Dismiss the view if there is no vote selected
                    [self dismissViewControllerAnimated:NO completion:nil];
                });
            }
        }];
        double delayInSeconds = 10.0; // 10 seconds to dismiss the loading view
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            //code to be executed on the main queue after delay
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        });
    } else {
        // Dismiss the view if there is no vote selected
        [self dismissViewControllerAnimated:NO completion:nil];
    }
}

- (IBAction)cancel:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}

@end
