//
//  CreateTeamStep2TVC.h
//  Vaiden
//
//  Created by James Chung on 4/6/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "CustomBaseTVC.h"
#import "Team.h"

@interface CreateTeamStep2TVC : CustomBaseTVC

@property (nonatomic, strong) Team *nTeam;

@end
