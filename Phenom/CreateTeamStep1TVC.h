//
//  CreateTeamStep1TVC.h
//  Vaiden
//
//  Created by James Chung on 4/6/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomBaseTVC.h"
#import "MBProgressHUD.h"

@interface CreateTeamStep1TVC : CustomBaseTVC <MBProgressHUDDelegate>
{
    MBProgressHUD *HUD;
}

@end
