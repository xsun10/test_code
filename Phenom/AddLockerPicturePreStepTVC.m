//
//  AddLockerPicturePreStepTVC.m
//  Vaiden
//
//  Created by James Chung on 5/27/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "AddLockerPicturePreStepTVC.h"
#import "AddLockerPictureVC.h"
#import "UIView+Manipulate.h"

@interface AddLockerPicturePreStepVC ()
@property (weak, nonatomic) IBOutlet UIImageView *picPreview;
@property (nonatomic) BOOL isCompetition;

@property (weak, nonatomic) IBOutlet UIView *selectBox;
@property (weak, nonatomic) IBOutlet UIScrollView *pageView;
@end

@implementation AddLockerPicturePreStepVC


- (void)viewDidLoad
{
    [super viewDidLoad];

    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
    
    /* Make round corner */
    [self.selectBox makeRoundedBorderWithRadius:5];
    
    self.picPreview.image = self.viObject.imageHandle;
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    self.pageView.contentSize = CGSizeMake(320.0, 600);
}

- (IBAction)yesIsClicked:(id)sender
{
    self.isCompetition = YES;
    [self performSegueWithIdentifier:@"Add Pic Segue" sender:self];
}

- (IBAction)noIsClicked:(id)sender
{
    self.isCompetition = NO;
    [self performSegueWithIdentifier:@"Add Pic Segue" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Add Pic Segue"]) {
        AddLockerPictureVC *controller = segue.destinationViewController;
        controller.viObject = self.viObject;
        controller.regImageSizeUploader = self.regImageSizeUploader;
        controller.regImageRetinaSizeUploader = self.regImageRetinaSizeUploader;
        controller.thumbnailImageSizeUploader = self.thumbnailImageSizeUploader;
        controller.thumbnailImageRetinaSizeUploader = self.thumbnailImageRetinaSizeUploader;
        controller.imageFileNameBaseString = self.imageFileNameBaseString;
        controller.imageFileName = self.imageFileName;
        controller.isCompetition = self.isCompetition;
    }
}

/*- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    if ([cell.textLabel.text isEqualToString:@"YES"]) {
        self.isCompetition = YES;
    } else if ([cell.textLabel.text isEqualToString:@"NO"]) {
        self.isCompetition = NO;
    }
    [self performSegueWithIdentifier:@"Add Pic Segue" sender:self];
}*/
@end
