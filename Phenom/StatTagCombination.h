//
//  StatTagCombination.h
//  Vaiden
//
//  Created by James Chung on 6/5/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StatTag.h"

@interface StatTagCombination : NSObject

@property (nonatomic, strong) StatTag *numerator;
@property (nonatomic, strong) StatTag *denominator;

- (StatTagCombination *)initWithStat1:(StatTag *)numerator
                             andStat2:(StatTag *)denominator;


@end
