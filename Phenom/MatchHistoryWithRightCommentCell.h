//
//  MatchHistoryWithRightCommentCell.h
//  Vaiden
//
//  Created by James Chung on 12/23/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MatchHistoryWithRightCommentCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *picView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@property (nonatomic, strong) UIView *messageBubble;
@end
