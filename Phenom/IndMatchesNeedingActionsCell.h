//
//  ScoresToRecordConfirmCell.h
//  Phenom
//
//  Created by James Chung on 5/13/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IndMatchesNeedingActionsCell : UITableViewCell
/*
@property (weak, nonatomic) IBOutlet UIImageView *profilePic;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UILabel *headlineLabel;
@property (weak, nonatomic) IBOutlet UILabel *probabilityLabel;
@property (weak, nonatomic) IBOutlet UILabel *probabilityMessage;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *venueLabel;
@property (weak, nonatomic) IBOutlet UILabel *streetLabel;
@property (weak, nonatomic) IBOutlet UILabel *cityStateLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusMessageLabel;
@property (weak, nonatomic) IBOutlet UIButton *scoreButtonOutlet;
 
*/

@property (weak, nonatomic) IBOutlet UILabel *matchHeading;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UIImageView *opponentPic;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UIView *typeBackground;
@property (weak, nonatomic) IBOutlet UILabel *typeAbbrev;
@property (weak, nonatomic) IBOutlet UILabel *typeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *sessionUserProfilePic;
@property (weak, nonatomic) IBOutlet UILabel *sportLabel;
@property (weak, nonatomic) IBOutlet UIImageView *forwardIcon;
@property (weak, nonatomic) IBOutlet UIView *barBackground;
@property (weak, nonatomic) IBOutlet UIView *iconBackgroundView;
@property (weak, nonatomic) IBOutlet UIImageView *alertIcon;
@property (weak, nonatomic) IBOutlet UIView *alertIconBackground;
@property (weak, nonatomic) IBOutlet UIImageView *sportIcon;
@property (weak, nonatomic) IBOutlet UILabel *matchCategoryLabel;
@property (weak, nonatomic) IBOutlet UIImageView *starIcon;

@end
