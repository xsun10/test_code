//
//  SportRatingsListCell.h
//  Vaiden
//
//  Created by James Chung on 10/7/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SportRatingsListCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *profilePic;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UILabel *commentLabel;
@property (weak, nonatomic) IBOutlet UIImageView *starIcons;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@end
