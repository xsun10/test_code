//
//  TrainingTag.h
//  Vaiden
//
//  Created by James Chung on 5/27/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TrainingTag : NSObject

- (TrainingTag *)initWithTrainingTagInfo:(NSInteger)trainingTagID
                      andTrainingTagName:(NSString *)trainingTagName;

@property (nonatomic) NSInteger trainingTagID;
@property (nonatomic, strong) NSString *trainingTagName;
@property (nonatomic) BOOL isSelected;

@end
