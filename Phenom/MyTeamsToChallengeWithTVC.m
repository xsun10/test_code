//
//  MyTeamsToChallengeWithTVC.m
//  Vaiden
//
//  Created by James Chung on 4/10/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "MyTeamsToChallengeWithTVC.h"
#import "UserPreferences.h"
#import "IOSRequest.h"
#import "MyTeamsToChallengeWithCell.h"
#import "ChallengeTeamListTVC.h"
#import "UIColor+VaidenColors.h"
#import "UIView+Manipulate.h"

@interface MyTeamsToChallengeWithTVC ()

@property (nonatomic, strong) NSMutableArray *teamsSessionUserIsCaptainOf;
@property (nonatomic, strong) UserPreferences *preferences;
@property (nonatomic, strong) UIView *tempView;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
@property (nonatomic, strong) UIView *overlay;

@property (nonatomic) NSInteger selectedRow;


@end

@implementation MyTeamsToChallengeWithTVC

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (NSMutableArray *)teamsSessionUserIsCaptainOf
{
    if (!_teamsSessionUserIsCaptainOf) _teamsSessionUserIsCaptainOf = [[NSMutableArray alloc] init];
    return _teamsSessionUserIsCaptainOf;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self loadTeamsSessionUserIsCaptainOf];
}


- (void)showTempSpinner
{
    self.tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    self.tempView.backgroundColor = [UIColor lightGrayColor];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] init];
    [self.activityIndicator setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width/2.0, [UIScreen mainScreen].bounds.size.height/2.0 - 50)];
    [self.activityIndicator startAnimating];
    [self.tempView addSubview:self.activityIndicator];
    
    [self.view addSubview:self.tempView];
    
}

- (void)displayErrorOverlay
{
    if (self.overlay == nil) {
        self.overlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, [UIScreen mainScreen].bounds.size.height)];
        self.overlay.backgroundColor = [UIColor lightLightGray];
        
        UILabel *firstLine = [[UILabel alloc] initWithFrame:CGRectMake(30, ([UIScreen mainScreen].bounds.size.height / 2) - 115, 260, 30)];
        firstLine.textColor = [UIColor darkGrayColor];
        firstLine.font = [UIFont systemFontOfSize:23];
        firstLine.text = @"No Teams To Manage";
        
        UILabel *secondLine = [[UILabel alloc] initWithFrame:CGRectMake(30, ([UIScreen mainScreen].bounds.size.height / 2) - 90, 260, 100)];
        secondLine.textColor = [UIColor lightGrayColor];
        secondLine.font = [UIFont systemFontOfSize:17];
        secondLine.text = @"You are not captain of any teams.  To become a captain, you can create a new team.";
        secondLine.numberOfLines = 0;
        
        UIButton *createButton = [[UIButton alloc] initWithFrame:CGRectMake(27, ([UIScreen mainScreen].bounds.size.height / 2) + 20  , 210, 40)];
        [createButton setTitle:@"Create Team" forState:UIControlStateNormal];
        [createButton setTitleColor:[UIColor peacock] forState:UIControlStateNormal];
        createButton.titleLabel.font = [UIFont systemFontOfSize:16.0];
        [createButton addTarget:self action:@selector(createNewTeam) forControlEvents:UIControlEventTouchUpInside];
        
        
        [createButton makeRoundedBorderWithRadius:3];
        [createButton.layer setBorderWidth:1.0];
        [createButton.layer setBorderColor:[[UIColor peacock] CGColor]];
        
        [self.overlay addSubview:firstLine];
        [self.overlay addSubview:secondLine];
        [self.overlay addSubview:createButton];
        
        [self.tableView addSubview:self.overlay];
    } else {
        self.overlay.hidden = NO;
    }
}

- (void)createNewTeam
{
    [self performSegueWithIdentifier:@"Create Captain Team Segue" sender:self];
}

- (void)loadTeamsSessionUserIsCaptainOf
{
    [self showTempSpinner];
    [self.teamsSessionUserIsCaptainOf removeAllObjects];
    
    [IOSRequest getSessionUserCaptainTeams:[self.preferences getUserID]
                              onCompletion:^(NSMutableArray *results) {
                                  
                                  for (id object in results) {
                                      Team *t = [[Team alloc] initWithTeamDetails:[object[@"team_id"] integerValue]
                                                                      andTeamName:object[@"team_name"]
                                                                       andCaptain:nil
                                                                     andPicString:object[@"logo_string"]];
                                      [t setNumMembers:[object[@"num_members"] integerValue]];
                                      [self.teamsSessionUserIsCaptainOf addObject:t];
                                  }
                                  
                                  if ([results count] == 0)
                                      [self displayErrorOverlay];
                                  else
                                      self.overlay.hidden = YES;
                                  
                                  dispatch_async(dispatch_get_main_queue(), ^{
                                      [self.activityIndicator stopAnimating];
                                      self.tempView.hidden = YES;

                                      [self.tableView reloadData];
                                  });
                              }];
}

- (IBAction)useButtonAction:(UIButton *)sender
{
    self.selectedRow = sender.tag;
    [self performSegueWithIdentifier:@"Choose Other Team Segue" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Choose Other Team Segue"]) {
        ChallengeTeamListTVC *controller = segue.destinationViewController;
        Team *t = [self.teamsSessionUserIsCaptainOf objectAtIndex:self.selectedRow];
        
        controller.sessionUserTeamToUse = t;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70.0f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.teamsSessionUserIsCaptainOf count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString static *CellIdentifier = @"Team Cell";
    
    MyTeamsToChallengeWithCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    Team *t = [self.teamsSessionUserIsCaptainOf objectAtIndex:indexPath.row];
    
    cell.teamNameLabel.text = t.teamName;
    cell.teamLogo.image = [UIImage imageNamed:t.picStringName];
    cell.numMembersLabel.text = [NSString stringWithFormat:@"%ld members", t.numMembers];
    cell.useButton.tag = indexPath.row;
    [cell.useButton makeRoundedBorderWithRadius:3];
    
    return cell;
}

@end
