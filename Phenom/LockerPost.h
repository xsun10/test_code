//
//  LockerPost.h
//  Vaiden
//
//  Created by James Chung on 5/12/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "News.h"
#import "LockerPic.h"

@interface LockerPost : News

//@property (nonatomic, strong) NSString *imageBaseFileString;
//@property (nonatomic) NSInteger lockerID;
@property (nonatomic, strong) LockerPic *lockPic;

- (LockerPost *)initWithNewsDetails:(NSInteger)newsMakerUserID
                       andUserName:(NSString *)newsMakerUserName
                     andProfileStr:(NSString *)profileBaseString
                          withDate:(NSDate *)newsDate
                       andPostType:(NSInteger)postType
                         andNewsID:(NSInteger)newsID
                       andTextPost:(NSString *)textPost
                andImageFileString:(NSString *)imageBaseFileString
                    andNumComments:(NSUInteger)numComments
                     andNumRemixes:(NSUInteger)numRemixes
                        andLockerID:(NSInteger)lockerID;

- (UIImage *)getNewsIcon;

@end
