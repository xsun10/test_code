//
//  PlayerDetailNewsfeedPaginator.m
//  Vaiden
//
//  Created by James Chung on 2/21/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "PlayerDetailNewsfeedPaginator.h"

#import "PickupMatchCreationNews.h"
#import "IndividualMatchCreationNews.h"
#import "MyHelpers.h"
#import "IOSRequest.h"
#import "UserPreferences.h"
#import "TextPost.h"
#import "ImagePost.h"
#import "S3Tools.h"
#import "UIImage+ProportionalFill.h"
#import "MatchPlayerNewsOfficialFinishedIndividualMatch.h"
#import "NSDate+Utilities.h"
#import "CompetitionNews.h"
#import "VenueKingNews.h"
#import "VenueCheckinNews.h"
#import "LockerAlbumCover.h"
#import "LockerPost.h"
#import "StatTag.h"
#import "VoteReceiving.h"
#import "TrainingTag.h"

@interface PlayerDetailNewsfeedPaginator() {
}

// protected properties

@property (nonatomic, strong) UserPreferences *preferences;
@property (nonatomic, strong) NSMutableArray *newsPosts;
@property (nonatomic, strong) NSMutableArray *lockerAlbums;
@property (nonatomic) NSInteger playerID;

@end

@implementation PlayerDetailNewsfeedPaginator



#define PICKUP_MATCH_CREATION 1
#define INDIVIDUAL_MATCH_CREATION 2
#define INDIVIDUAL_MATCH_CONFIRMATION 3
#define INDIVIDUAL_MATCH_RESULT 4
#define USER_TEXT_POST 5
#define USER_TEXT_POST_WITH_IMAGE 6
#define INDIVIDUAL_MATCH_UPDATE 7
#define PICKUP_MATCH_UPDATE 8
#define INDIVIDUAL_MATCH_PLAYER_UPDATE 9
#define PICKUP_MATCH_PLAYER_UPDATE 10
#define INDIVIDUAL_MATCH_RECORD_SCORE_UPDATE 11
#define INDIVIDUAL_MATCH_CONFIRM_SCORE_UPDATE 12
#define INDIVIDUAL_MATCH_DISPUTE_SCORE_UPDATE 13
#define COMPETITION_RSVP 14
#define VENUE_KING_UPDATE 15
#define TEAM_CREATION 16
#define TEAM_MATCH_CREATION 17
#define VENUE_CHECKIN 18
#define USER_LOCKER_POST 19
#define VOTE_RECEIVING 20
#define VOTE_GIVING 21
#define SENT_THANKS 22
#define LEVEL_UP 23


- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (NSMutableArray *)newsPosts
{
    if (!_newsPosts) _newsPosts = [[NSMutableArray alloc] init];
    return _newsPosts;
}

- (NSMutableArray *)lockerAlbums
{
    if (!_lockerAlbums) _lockerAlbums = [[NSMutableArray alloc] init];
    return _lockerAlbums;
}

- (id)initWithPDNPageSize:(NSInteger)pageSize delegate:(id<NMPaginatorDelegate>)paginatorDelegate andPlayerID:(NSInteger)playerID
{
    if (self = [super initWithPageSize:pageSize delegate:paginatorDelegate]) {
        _playerID = playerID;
        _noPermissions = NO;
        
     
    }
    return self;
}

- (void)fetchFirstPageWithTab:(NSInteger)tabToShow
{
    [self reset];
    
    self.tabToShow = tabToShow;

    [self fetchNextPage];
    
}
- (void)fetchResultsWithPage:(NSInteger)page pageSize:(NSInteger)pageSize
{
    if (self.tabToShow == PLAYER_DETAIL_SHOW_NEWS_POSTS) {
    [IOSRequest fetchUserInfo:self.playerID
                bySessionUser:[self.preferences getUserID]
                  withPageNum:page
                 andSpanCells:pageSize
                 onCompletion:^(NSDictionary *resultDictionary) {
                     [self.newsPosts removeAllObjects];
                     NSLog(@"%@", resultDictionary[@"player_newsfeed"]);
                     if ([resultDictionary[@"player_newsfeed"] isKindOfClass:[NSString class]] && [resultDictionary[@"player_newsfeed"] isEqualToString:@"NO_PERMISSIONS"]) {
                         self.noPermissions = YES;
                     } else {
        
        for (id object in resultDictionary[@"player_newsfeed"]) {
            if ([object[@"post_type"] integerValue] == PICKUP_MATCH_CREATION) {
                [self loadPickupMatch:object];
            } else if ([object[@"post_type"] integerValue] == INDIVIDUAL_MATCH_CREATION) {
                [self loadIndividualMatches:object];
            } else if ([object[@"post_type"] integerValue] == INDIVIDUAL_MATCH_CONFIRMATION) {
                [self loadIndividualMatches:object];
            } else if ([object[@"post_type"] integerValue ] == INDIVIDUAL_MATCH_RESULT) {
                [self loadIndividualMatches:object];
            } else if ([object[@"post_type"] integerValue ] == USER_TEXT_POST) {
                      [self loadUserGeneratedPosts:object];
            } else if ([object[@"post_type"] integerValue] == USER_TEXT_POST_WITH_IMAGE) {
                      [self loadUserGeneratedPosts:object];
            } else if ([object[@"post_type"] integerValue] == INDIVIDUAL_MATCH_UPDATE) {
                
                [self loadIndividualMatches:object];
            } else if ([object[@"post_type"] integerValue] == PICKUP_MATCH_UPDATE) {
                
                [self loadPickupMatch:object];
            } else if ([object[@"post_type"] integerValue] == INDIVIDUAL_MATCH_PLAYER_UPDATE) {
                
                [self loadIndividualMatches:object];
            } else if ([object[@"post_type"] integerValue] == PICKUP_MATCH_PLAYER_UPDATE) {
                
                [self loadPickupMatch:object];
            } else if ([object[@"post_type"] integerValue] == INDIVIDUAL_MATCH_RECORD_SCORE_UPDATE) {
                
                [self loadIndividualMatches:object];
            } else if ([object[@"post_type"] integerValue] == INDIVIDUAL_MATCH_CONFIRM_SCORE_UPDATE) {
                     [self loadMatchPlayerNewsOfficialFinishedIndividualMatch:object];
            } else if ([object[@"post_type"] integerValue] == INDIVIDUAL_MATCH_DISPUTE_SCORE_UPDATE) {
                
                [self loadIndividualMatches:object];
            } else if ([object[@"post_type"] integerValue] == COMPETITION_RSVP) {
                [self loadCompetitionRSVP:object];
            } else if ([object[@"post_type"] integerValue] == VENUE_KING_UPDATE) {
                [self loadVenueKingNews:object];
            } else if ([object[@"post_type"] integerValue] == TEAM_CREATION) {
                
            } else if ([object[@"post_type"] integerValue] == TEAM_MATCH_CREATION) {
            //    [self loadTeamMatches:object];
            } else if ([object[@"post_type"] integerValue] == VENUE_CHECKIN) {
                [self loadVenueCheckin:object];
            } else if ([object[@"post_type"] integerValue] == USER_LOCKER_POST) {
                [self loadLockerPost:object];
            } else if ([object[@"post_type"] integerValue] == VOTE_RECEIVING) {
                [self loadVoteNews:object withType:@"receiving"];
            } else if ([object[@"post_type"] integerValue] == VOTE_GIVING) {
                [self loadVoteNews:object withType:@"giving"];
            } else if ([object[@"post_type"] integerValue] == SENT_THANKS) {
                [self loadVoteNews:object withType:@"thanks"];
            } else if ([object[@"post_type"] integerValue] == LEVEL_UP) {
                [self loadVoteNews:object withType:@"level"];
            }
            
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self receivedResults:self.newsPosts total:100];
        });        //       [self endRefreshWhenSynced];
                     }
    }];
    } else if (self.tabToShow == PLAYER_DETAIL_SHOW_LOCKER_COVERS) {
        [self.lockerAlbums removeAllObjects];
        [IOSRequest fetchLockerAlbumCovers:self.playerID
                          andSessionUserID:[self.preferences getUserID]
                               withPageNum:page
                                   andSpan:pageSize
                              onCompletion:^(NSDictionary *results) {
                                  
                                  dispatch_async(dispatch_get_main_queue(), ^{
                                      if ([results[@"outcome"] isKindOfClass:[NSString class]] && [results[@"outcome"] isEqualToString:@"NO_PERMISSIONS"]) {
                                          self.noPermissions = YES;
                                      } else {
                                          [self loadLockerAlbumCovers:results[@"data"]];
                                          [self receivedResults:self.lockerAlbums total:100];
                                      }
                                  });
                                  
                              }];
    } else if (self.tabToShow == PLAYER_DETAIL_SHOW_MATCHES_ONLY) {
        [self.newsPosts removeAllObjects];
        NSLog(@"%d, %d",self.playerID, [self.preferences getUserID]);
        [IOSRequest fetchPlayerMatchHistoryForProfilePage:self.playerID
                                               andPageNum:page
                                             andSpanCells:pageSize
                                         andSessionUserID:[self.preferences getUserID]
                                             onCompletion:^(NSDictionary *results) {
                                                 dispatch_async(dispatch_get_main_queue(), ^{
                                                     if ([results[@"outcome"] isKindOfClass:[NSString class]] && [results[@"outcome"] isEqualToString:@"NO_PERMISSIONS"]) {
                                                         self.noPermissions = YES;
                                                     } else {

                                                         for (id object in results[@"match_feed"]) {
                                                             if ([object[@"post_type"] integerValue] == PICKUP_MATCH_CREATION) {
                                                                 [self loadPickupMatch:object];
                                                             } else if ([object[@"post_type"] integerValue] == INDIVIDUAL_MATCH_CREATION) {
                                                                 [self loadIndividualMatches:object];
                                                             } else if ([object[@"post_type"] integerValue] == INDIVIDUAL_MATCH_CONFIRMATION) {
                                                                 [self loadIndividualMatches:object];
                                                             } else if ([object[@"post_type"] integerValue ] == INDIVIDUAL_MATCH_RESULT) {
                                                                 [self loadIndividualMatches:object];
                                                             }  else if ([object[@"post_type"] integerValue] == INDIVIDUAL_MATCH_UPDATE) {
                                                                 
                                                                 [self loadIndividualMatches:object];
                                                             } else if ([object[@"post_type"] integerValue] == PICKUP_MATCH_UPDATE) {
                                                                 
                                                                 [self loadPickupMatch:object];
                                                             } else if ([object[@"post_type"] integerValue] == INDIVIDUAL_MATCH_PLAYER_UPDATE) {
                                                                 
                                                                 [self loadIndividualMatches:object];
                                                             } else if ([object[@"post_type"] integerValue] == PICKUP_MATCH_PLAYER_UPDATE) {
                                                                 
                                                                 [self loadPickupMatch:object];
                                                             } else if ([object[@"post_type"] integerValue] == INDIVIDUAL_MATCH_RECORD_SCORE_UPDATE) {
                                                                 
                                                                 [self loadIndividualMatches:object];
                                                             } else if ([object[@"post_type"] integerValue] == INDIVIDUAL_MATCH_CONFIRM_SCORE_UPDATE) {
                                                                 [self loadMatchPlayerNewsOfficialFinishedIndividualMatch:object];
                                                             } else if ([object[@"post_type"] integerValue] == INDIVIDUAL_MATCH_DISPUTE_SCORE_UPDATE) {
                                                                 
                                                                 [self loadIndividualMatches:object];
                                                             } else if ([object[@"post_type"] integerValue] == COMPETITION_RSVP) {
                                                                 [self loadCompetitionRSVP:object];
                                                             } else if ([object[@"post_type"] integerValue] == VENUE_KING_UPDATE) {
                                                                 [self loadVenueKingNews:object];
                                                             } else if ([object[@"post_type"] integerValue] == TEAM_CREATION) {
                                                                 
                                                             } else if ([object[@"post_type"] integerValue] == TEAM_MATCH_CREATION) {
                                                                 //    [self loadTeamMatches:object];
                                                             } else if ([object[@"post_type"] integerValue] == VENUE_CHECKIN) {
                                                                 [self loadVenueCheckin:object];
                                                             } else if ([object[@"post_type"] integerValue] == USER_LOCKER_POST) {
                                                                 [self loadLockerPost:object];
                                                             }
                                                             
                                                         }
                                                         
                                                         
                                                         
                                                         
                                                         
                                                         
                                                         
                                                         [self receivedResults:self.newsPosts total:100];
                                                     }
                                                 });
                                                 
                                             }];
        
        
    }
    
}

- (void)loadVoteNews:(NSDictionary *)object withType:(NSString *)type
{
    id dataObj = object[@"data_obj"];
    
    NSDate *newsfeedDateTime = [MyHelpers getDateTimeFromString:object[@"newsfeed_ts"]];
    
    
    VoteReceiving *iPost = [[VoteReceiving alloc] initWithNewsDetails:[object[@"post_user_id"] integerValue]
                                                          andUserName:object[@"post_username"]
                                                        andProfileStr:object[@"profile_pic_string"]
                                                             withDate:newsfeedDateTime
                                                          andPostType:[object[@"post_type"] integerValue]
                                                            andNewsID:[object[@"newsfeed_id"] integerValue]
                                                          andTextPost:dataObj[@"text_post"]
                                                          andTargetID:[dataObj[@"target_user_id"] integerValue]
                                                    andTargetUsername:dataObj[@"target_username"]
                                                  andtargetProfileStr:dataObj[@"target_profile_base_string"]
                                                       andNumComments:[object[@"num_comments"] integerValue]
                                                        andNumRemixes:[object[@"num_remixes"] integerValue]
                                                          andLockerID:[dataObj[@"locker_id"] integerValue]
                                                              andType:type];
    
    id subObj = object[@"remix_details"];
    
    iPost.sharedByPlayer = [[Player alloc] initWithPlayerDetails:[subObj[@"user_id"] integerValue] andUserName:subObj[@"username"]];
    iPost.didSessionUserRemixPost = [object[@"did_session_user_remix_post"] boolValue];
    
    iPost.numLikes = [object[@"num_likes"] integerValue];
    iPost.doesSessionUserLikePost = [object[@"does_session_user_like_post"] boolValue];
    
    [self.newsPosts addObject:iPost];
}

- (void)loadCompetitionRSVP:(NSDictionary *)object
{
    id dataObj = object[@"data_obj"];
    
    NSDate *newsfeedDateTime    = [MyHelpers getDateTimeFromString:object[@"newsfeed_ts"]];
    
    
    CompetitionNews *cNews = [[CompetitionNews alloc] initWithNewsID:[object[@"post_user_id"] integerValue]
                                                         andUsername:object[@"post_username"]
                                                       andProfileStr:object[@"profile_pic_string"]
                                                            withDate:newsfeedDateTime
                                                         andPostType:[object[@"post_type"] integerValue]
                                                      andNumComments:[object[@"num_comments"] integerValue]
                                                       andNumRemixes:[object[@"num_remixes"] integerValue]
                                                           andNewsID:[object[@"newsfeed_id"] integerValue]
                                                   andUniversityName:dataObj[@"college_name"]
                                           andUniversityOrganization:dataObj[@"college_organization"]];
    
    id subObj = object[@"remix_details"];
    
    cNews.sharedByPlayer = [[Player alloc] initWithPlayerDetails:[subObj[@"user_id"] integerValue] andUserName:subObj[@"username"]];
    cNews.didSessionUserRemixPost = [object[@"did_session_user_remix_post"] boolValue];
    
    cNews.numLikes = [object[@"num_likes"] integerValue];
    cNews.doesSessionUserLikePost = [object[@"does_session_user_like_post"] boolValue];
    
    [self.newsPosts addObject:cNews];
    
    
}

- (void)loadVenueKingNews:(NSDictionary *)object
{
    id dataObj = object[@"data_obj"];
    
    NSDate *newsfeedDateTime    = [MyHelpers getDateTimeFromString:object[@"newsfeed_ts"]];
    
    Venue *vkVenue = [[Venue alloc] initVenueWithName:dataObj[@"venue_name"]
                                           andVenueID:[dataObj[@"venue_id"] integerValue]];
    [vkVenue setCity:dataObj[@"city"]];
    [vkVenue setStateName:dataObj[@"state"]];
    
    Sport *vkSport = [[Sport alloc] initWithSportDetails:[dataObj[@"sport_id"] integerValue]
                                                 andName:dataObj[@"sport_name"]
                                                 andType:@"individual"];
    
    VenueKingNews *vNews = [[VenueKingNews alloc] initWithNewsID:[object[@"post_user_id"] integerValue]
                                                     andUsername:object[@"post_username"]
                                                   andProfileStr:object[@"profile_pic_string"]
                                                        withDate:newsfeedDateTime
                                                     andPostType:[object[@"post_type"] integerValue]
                                                  andNumComments:[object[@"num_comments"] integerValue]
                                                   andNumRemixes:[object[@"num_remixes"] integerValue]
                                                       andNewsID:[object[@"newsfeed_id"] integerValue]
                                                        andVenue:vkVenue
                                                        andSport:vkSport];
    
    id subObj = object[@"remix_details"];
    
    vNews.sharedByPlayer = [[Player alloc] initWithPlayerDetails:[subObj[@"user_id"] integerValue] andUserName:subObj[@"username"]];
    vNews.didSessionUserRemixPost = [object[@"did_session_user_remix_post"] boolValue];
    
    vNews.numLikes = [object[@"num_likes"] integerValue];
    vNews.doesSessionUserLikePost = [object[@"does_session_user_like_post"] boolValue];
    
    [self.newsPosts addObject:vNews];
}


- (void)loadVenueCheckin:(NSDictionary *)object
{
    id dataObj = object[@"data_obj"];
    
    NSDate *newsfeedDateTime    = [MyHelpers getDateTimeFromString:object[@"newsfeed_ts"]];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *dateTime = [[formatter dateFromString:dataObj[@"check_in_ts"]] toLocalTime];
    
    Player *checkInPlayer = [[Player alloc] initWithPlayerDetails:[dataObj[@"user_id"] integerValue]
                                                      andUserName:dataObj[@"username"]
                                              andProfilePicString:dataObj[@"profile_pic_string"]];
    
    VenueCheckIn *vci = [[VenueCheckIn alloc] initWithCheckInID:[dataObj[@"checkin_id"] integerValue]
                                              checkedInDateTime:dateTime
                                               checkedInMessage:dataObj[@"message"]
                                                       isPublic:[dataObj[@"is_public"] boolValue]
                                                          isNow:[dataObj[@"is_now"] boolValue]
                                                  checkedInUser:checkInPlayer
                                                 checkedInSport:[[Sport alloc] initWithSportDetails:[dataObj[@"sport_id"] integerValue]
                                                                                            andName:dataObj[@"sport_name"]
                                                                                            andType:nil]];
    
    Venue *vkVenue = [[Venue alloc] initVenueWithName:dataObj[@"venue_name"]
                                           andVenueID:[dataObj[@"venue_id"] integerValue]];
    
    
    VenueCheckinNews *vNews = [[VenueCheckinNews alloc] initWithNewsID:[object[@"post_user_id"] integerValue]
                                                           andUsername:object[@"post_username"]
                                                         andProfileStr:object[@"profile_pic_string"]
                                                              withDate:newsfeedDateTime
                                                           andPostType:[object[@"post_type"] integerValue]
                                                        andNumComments:[object[@"num_comments"] integerValue]
                                                         andNumRemixes:[object[@"num_remixes"] integerValue]
                                                             andNewsID:[object[@"newsfeed_id"] integerValue]
                                                              andVenue:vkVenue
                                                       andVenueCheckIn:vci];
    
    
    id subObj = object[@"remix_details"];
    
    vNews.sharedByPlayer = [[Player alloc] initWithPlayerDetails:[subObj[@"user_id"] integerValue] andUserName:subObj[@"username"]];
    vNews.didSessionUserRemixPost = [object[@"did_session_user_remix_post"] boolValue];
    
    vNews.numLikes = [object[@"num_likes"] integerValue];
    vNews.doesSessionUserLikePost = [object[@"does_session_user_like_post"] boolValue];
    
    [self.newsPosts addObject:vNews];
    
}

- (void)loadPickupMatch:(NSDictionary *)object
{
    id dataObj = object[@"data_obj"];
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *matchDateTime = [[formatter dateFromString:dataObj[@"ps_ts"]] toLocalTime];
    
    NSDateFormatter *formatter2 = [[NSDateFormatter alloc]init];
    [formatter2 setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *newsfeedDateTime = [[formatter2 dateFromString:object[@"newsfeed_ts"]] toLocalTime];
    
    
    PickupMatchCreationNews *pNews = [[PickupMatchCreationNews alloc]
                                      initWithPickupMatchCreationNewsDetails:[dataObj[@"match_id"] integerValue]
                                      andMatchName:dataObj[@"match_name"]
                                      matchCreatorID:[dataObj[@"creator_id"] integerValue]
                                      matchCreatorUserName:dataObj[@"creator_username"]
                                      andProfileStr:dataObj[@"creator_profile_pic_string"]
                                      matchDate:matchDateTime
                                      newsPostDate:newsfeedDateTime
                                      andPostType:1 //[object[@"post_type"] integerValue]
                                      atVenueID:[dataObj[@"venue_id"] integerValue]
                                      atVenueName:dataObj[@"venue_name"]
                                      withSportID:[dataObj[@"sport_id"] integerValue]
                                      andSportName:dataObj[@"sport_name"]
                                      andSportType:dataObj[@"sport_type"]
                                      isPrivate:[dataObj[@"is_private"] boolValue]
                                      andMatchStatus:dataObj[@"match_status"]
                                      minCompetitors:[dataObj[@"min_competitors"] integerValue]
                                      requiredExperience:[dataObj[@"required_experience"] mutableCopy] // I really don't need NSMutableArray...Just doing this to get rid of warning...in future, should just change requiredExperience to an NSArray.
                                      andMatchTypeID:[dataObj[@"match_type_id"] integerValue]
                                      andMatchTypeDesc:dataObj[@"match_type_description"]
                                      andNewsID:[object[@"newsfeed_id"] integerValue]
                                      andNumComments:[object[@"num_comments"] integerValue]
                                      andNumRemixes:[object[@"num_remixes"] integerValue]];
    [pNews.pickupMatch setGender:dataObj[@"gender"]];
    
    id subObj = object[@"remix_details"];
    
    [pNews.pickupMatch loadCompetitorArrayFromObject:dataObj[@"competitors"]];
    
    pNews.sharedByPlayer = [[Player alloc] initWithPlayerDetails:[subObj[@"user_id"] integerValue] andUserName:subObj[@"username"]];
    
    pNews.didSessionUserRemixPost = [object[@"did_session_user_remix_post"] boolValue];
    
    //  pNews.pickupMatch.matchHistoryArray = [[NSMutableArray alloc] init];
    [pNews.pickupMatch setMatchHistoryWithDataObj:dataObj[@"match_history"]];
    pNews.numLikes = [object[@"num_likes"] integerValue];
    pNews.doesSessionUserLikePost = [object[@"does_session_user_like_post"] boolValue];
    
    [self.newsPosts addObject:pNews];
    
}

- (void)loadIndividualMatches:(NSDictionary *)object
{
    id dataObj = object[@"data_obj"];
    
    NSDate *matchDateTime       = [MyHelpers getDateTimeFromString:dataObj[@"match_ts"]];
    NSDate *newsfeedDateTime    = [MyHelpers getDateTimeFromString:object[@"newsfeed_ts"]];
    
    Sport *sport = [[Sport alloc] initWithSportDetails:dataObj[@"sport_name"] andType:dataObj[@"sport_type"]];
    [sport setGameUnitName:dataObj[@"game_unit_name"]];
    [sport setSetUnitName:dataObj[@"set_unit_name"]];
    [sport setLargerScoreWins:[dataObj[@"score_order"] boolValue]];
    
    Venue *venue = [[Venue alloc] initVenueWithName:dataObj[@"venue_name"] andVenueID:[dataObj[@"venue_id"] integerValue]];
    
    
    IndividualMatch *individualMatch = [[IndividualMatch alloc] initMatchWithDetails:[dataObj[@"match_id"] integerValue]
                                                                        andMatchName:dataObj[@"match_name"]
                                                                           isPrivate:[dataObj[@"is_private"] boolValue]
                                                                           withSport:sport
                                                                         withMessage:dataObj[@"message"]
                                                                          onDateTime:matchDateTime
                                                                             atVenue:venue
                                                                       createdByUser:[dataObj[@"creator_id"] integerValue]
                                                                    withMatchPlayer1:nil
                                                                    withMatchPlayer2:nil
                                                                           andActive:YES
                                                           andProbabilityPlayer1Wins:0 // was prob_user1
                                                           andProbabilityPlayer2Wins:0]; // was prob_user2
    [individualMatch setMatchStatus:dataObj[@"match_status"]];
    
    
    /////////////NEW
    
    id scoreSummaryObject = dataObj[@"score_summary"];
    
    if (scoreSummaryObject[@"recorder_user_id"] != nil && ![scoreSummaryObject[@"recorder_user_id"] isKindOfClass:[NSNull class]]) {
        for (id roundScoreObj in scoreSummaryObject[@"match_scores"]) { // really should be no need to loop because only 1 version allowed for now.
            
            MatchPlayer *player1 = [[MatchPlayer alloc] initWithPlayerDetails:[roundScoreObj[@"player1_id"] integerValue]
                                                                  andUserName:roundScoreObj[@"player1_username"]];
            
            MatchPlayer *player2 = [[MatchPlayer alloc] initWithPlayerDetails:[roundScoreObj[@"player2_id"] integerValue]
                                                                  andUserName:roundScoreObj[@"player2_username"]];
            [player1 setProfileBaseString:roundScoreObj[@"player1_profile_pic_string"]];
            [player2 setProfileBaseString:roundScoreObj[@"player2_profile_pic_string"]];
            
            PlayerSport *ps1 = [[PlayerSport alloc] initWithSportDetails:[dataObj[@"sport_id"] integerValue]
                                                                 andName:dataObj[@"sport_name"]
                                                                 andType:@"individual"
                                                                andLevel:roundScoreObj[@"player1_level"]
                                                          andSportRecord:nil];
            [player1 addPlayerSport:ps1];
            
            PlayerSport *ps2 = [[PlayerSport alloc] initWithSportDetails:[dataObj[@"sport_id"] integerValue]
                                                                 andName:dataObj[@"sport_name"]
                                                                 andType:@"individual"
                                                                andLevel:roundScoreObj[@"player2_level"]
                                                          andSportRecord:nil];
            [player2 addPlayerSport:ps2];
            
            [individualMatch setPlayer1:player1];
            [individualMatch setPlayer2:player2];
            
            [individualMatch.scoreIndMatch addRoundScore:[roundScoreObj[@"round"] integerValue]
                                              forPlayer1:[roundScoreObj[@"player1_id"] integerValue]
                                        withScorePlayer1:[roundScoreObj[@"score_player1"] integerValue]
                                              forPlayer2:[roundScoreObj[@"player2_id"] integerValue]
                                        withScorePlayer2:[roundScoreObj[@"score_player2"] integerValue]];
            [individualMatch.scoreIndMatch setVersion:[scoreSummaryObject[@"version"] integerValue]];
            [individualMatch.scoreIndMatch setScoreRecordedBy:[scoreSummaryObject[@"recorder_user_id"] integerValue]];
            [individualMatch.scoreIndMatch setScoreConfirmedBy:[scoreSummaryObject[@"confirmer_user_id"] integerValue]];
            [individualMatch.scoreIndMatch setScoreDisputedBy:[scoreSummaryObject[@"disputor_user_id"] integerValue]];
            [individualMatch.scoreIndMatch setWasFirstVersionIndividualScoreRecordedBySessionUserAndDisputedByOtherUser:[scoreSummaryObject[@"was_session_user_score_overruled"] boolValue]];
        }
    } else {
        
        // START OLD
        MatchPlayer *player2 = [self makeMatchCompetitorObject:dataObj[@"competitorArray"]
                                                  forSportName:dataObj[@"sport_name"]
                                                       andType:dataObj[@"sport_type"]];
        
        
        MatchPlayer *player1 = [[MatchPlayer alloc] initWithPlayerDetails:[dataObj[@"creator_id"] integerValue]
                                                            andProfileStr:dataObj[@"creator_profile_pic_string"]
                                                              andUserName:dataObj[@"creator_username"]
                                                              andFullName:nil
                                                              andLocation:nil
                                                                 andAbout:nil
                                                                 andCoins:0
                                                                andStatus:nil];
        
        Sport *player1Sport = [[PlayerSport alloc] initWithSportDetails:dataObj[@"sport_name"]
                                                                andType:dataObj[@"sport_type"]
                                                               andLevel:dataObj[@"creator_level"]
                                                         andSportRecord:dataObj[@"win_loss_record"]];
        
        [player1.playerSports addObject:player1Sport];
        
        [individualMatch setPlayer1:player1];
        [individualMatch setPlayer2:player2];
        
        // END OLD
        
    }
    /////////////END NEW
    
    IndividualMatchCreationNews *iNews = [[IndividualMatchCreationNews alloc] initWithIndividualMatchCreationNewsDetails:individualMatch
                                                                                                    matchCreatorUserName:dataObj[@"creator_username"]
                                                                                                           andProfileStr:dataObj[@"creator_profile_pic_string"]
                                                                                                            newsPostDate:newsfeedDateTime
                                                                                                             andPostType: 2 // [object[@"post_type"] integerValue]
                                                                                                               andNewsID:[object[@"newsfeed_id"] integerValue]
                                                                                                          andNumComments:[object[@"num_comments"] integerValue]
                                                                                                           andNumRemixes:[object[@"num_remixes"] integerValue]];
    id subObj = object[@"remix_details"];
    
    iNews.sharedByPlayer = [[Player alloc] initWithPlayerDetails:[subObj[@"user_id"] integerValue] andUserName:subObj[@"username"]];
    
    iNews.didSessionUserRemixPost = [object[@"did_session_user_remix_post"] boolValue];
    [iNews setProfileBaseString:dataObj[@"creator_profile_pic_string"]];
    [iNews.individualMatch setMatchHistoryWithDataObj:dataObj[@"match_history"]];
    iNews.numLikes = [object[@"num_likes"] integerValue];
    iNews.doesSessionUserLikePost = [object[@"does_session_user_like_post"] boolValue];
    
    [self.newsPosts addObject:iNews];
    
    
    
    
    
}


- (void)loadMatchPlayerNewsOfficialFinishedIndividualMatch:(NSDictionary *)object
{
    id dataObj = object[@"data_obj"];
    
    NSDate *newsDateTime = [MyHelpers getDateTimeFromString:object[@"newsfeed_ts"]];
    
    if ([dataObj[@"match_category"] isEqualToString:@"individual"]) {
        
        Sport *sp = [[Sport alloc] initWithSportDetails:[dataObj[@"sport_id"] integerValue]
                                                andName:dataObj[@"sport_name"]
                                                andType:@"individual"];
        [sp setGameUnitName:dataObj[@"game_unit_name"]];
        [sp setSetUnitName:dataObj[@"set_unit_name"]];
        [sp setLargerScoreWins:[dataObj[@"score_order"] boolValue]];
        
        IndividualMatch *iMatch = [[IndividualMatch alloc] init];
        [iMatch setMatchID:[dataObj[@"match_id"] integerValue]];
        [iMatch setMatchName:dataObj[@"match_name"]];
        [iMatch setMatchStatus:dataObj[@"match_status"]];
        [iMatch setSport:sp];
        
        Player *player = [[Player alloc] initWithPlayerDetails:[dataObj[@"player_id"] integerValue]
                                                   andUserName:dataObj[@"username"]
                                           andProfilePicString:dataObj[@"profile_pic_string"]];
        
        
        id scoreSummaryObject = dataObj[@"score_summary"];
        
        for (id roundScoreObj in scoreSummaryObject[@"match_scores"]) { // really should be no need to loop because only 1 version allowed for now.
            
            MatchPlayer *player1 = [[MatchPlayer alloc] initWithPlayerDetails:[roundScoreObj[@"player1_id"] integerValue]
                                                                  andUserName:roundScoreObj[@"player1_username"]];
            MatchPlayer *player2 = [[MatchPlayer alloc] initWithPlayerDetails:[roundScoreObj[@"player2_id"] integerValue]
                                                                  andUserName:roundScoreObj[@"player2_username"]];
            [player1 setProfileBaseString:roundScoreObj[@"player1_profile_pic_string"]];
            [player2 setProfileBaseString:roundScoreObj[@"player2_profile_pic_string"]];
            
            PlayerSport *ps1 = [[PlayerSport alloc] initWithSportDetails:[dataObj[@"sport_id"] integerValue]
                                                                 andName:dataObj[@"sport_name"]
                                                                 andType:@"individual"
                                                                andLevel:roundScoreObj[@"player1_level"]
                                                          andSportRecord:nil];
            [player1 addPlayerSport:ps1];
            
            PlayerSport *ps2 = [[PlayerSport alloc] initWithSportDetails:[dataObj[@"sport_id"] integerValue]
                                                                 andName:dataObj[@"sport_name"]
                                                                 andType:@"individual"
                                                                andLevel:roundScoreObj[@"player2_level"]
                                                          andSportRecord:nil];
            [player2 addPlayerSport:ps2];
            
            [iMatch setPlayer1:player1];
            [iMatch setPlayer2:player2];
            
            [iMatch.scoreIndMatch addRoundScore:[roundScoreObj[@"round"] integerValue]
                                     forPlayer1:[roundScoreObj[@"player1_id"] integerValue]
                               withScorePlayer1:[roundScoreObj[@"score_player1"] integerValue]
                                     forPlayer2:[roundScoreObj[@"player2_id"] integerValue]
                               withScorePlayer2:[roundScoreObj[@"score_player2"] integerValue]];
            [iMatch.scoreIndMatch setVersion:[scoreSummaryObject[@"version"] integerValue]];
            [iMatch.scoreIndMatch setScoreRecordedBy:[scoreSummaryObject[@"recorder_user_id"] integerValue]];
            [iMatch.scoreIndMatch setScoreConfirmedBy:[scoreSummaryObject[@"confirmer_user_id"] integerValue]];
            [iMatch.scoreIndMatch setScoreDisputedBy:[scoreSummaryObject[@"disputor_user_id"] integerValue]];
            [iMatch.scoreIndMatch setWasFirstVersionIndividualScoreRecordedBySessionUserAndDisputedByOtherUser:[scoreSummaryObject[@"was_session_user_score_overruled"] boolValue]];
        }
        
        iMatch.matchHistoryArray = [[NSMutableArray alloc] init];
        [iMatch setMatchHistoryWithDataObj:dataObj[@"match_history"]];
        
        
        MatchPlayerNewsOfficialFinishedIndividualMatch *mpn = [[MatchPlayerNewsOfficialFinishedIndividualMatch alloc] initWithDetails:iMatch
                                                                                                                             postDate:newsDateTime
                                                                                                                      headLineMessage:@"Match Update"
                                                                                                                       generalMessage:dataObj[@"general_message"]
                                                                                                                            forPlayer:player
                                                                                                                          withMessage:dataObj[@"comment"]
                                                                                                                 andCoinTransferValue:[dataObj[@"coins_earned"] integerValue]];
        
        [mpn.match setMatchCreatorID:[dataObj[@"creator_id"] integerValue]];
        [mpn.match setMatchCreatorUsername:[iMatch getPlayerForID:[dataObj[@"creator_id"] integerValue]].userName];
        
        [mpn setNumComments:[object[@"num_comments"] integerValue]];
        [mpn setNumRemixes:[object[@"num_remixes"] integerValue]];
        [mpn setNewsID:[object[@"newsfeed_id"] integerValue]];
        
        id subObj = object[@"remix_details"];
        
        mpn.sharedByPlayer = [[Player alloc] initWithPlayerDetails:[subObj[@"user_id"] integerValue] andUserName:subObj[@"username"]];
        mpn.didSessionUserRemixPost = [object[@"did_session_user_remix_post"] boolValue];
        
        mpn.numLikes = [object[@"num_likes"] integerValue];
        mpn.doesSessionUserLikePost = [object[@"does_session_user_like_post"] boolValue];
        
        [self.newsPosts addObject:mpn];
        
    }
}

- (void)loadUserGeneratedPosts:(NSDictionary *)object
{
    id dataObj = object[@"data_obj"];
    
    NSDate *newsfeedDateTime    = [MyHelpers getDateTimeFromString:object[@"newsfeed_ts"]];
    
    if (dataObj[@"file_type"] ==  [NSNull null]) { // most likely a text post
        
        TextPost *tPost = [[TextPost alloc] initWithNewsDetails:[object[@"post_user_id"] integerValue]
                                                    andUserName:object[@"post_username"]
                                                  andProfileStr:object[@"profile_pic_string"]
                                                       withDate:newsfeedDateTime
                                                    andPostType:[object[@"post_type"] integerValue]
                                                      andNewsID:[object[@"newsfeed_id"] integerValue]
                                                    andTextPost:dataObj[@"text_post"]
                                                 andNumComments:[object[@"num_comments"] integerValue]
                                                  andNumRemixes:[object[@"num_remixes"] integerValue]];
        
        id subObj = object[@"remix_details"];
        
        tPost.sharedByPlayer = [[Player alloc] initWithPlayerDetails:[subObj[@"user_id"] integerValue] andUserName:subObj[@"username"]];
        tPost.didSessionUserRemixPost = [object[@"did_session_user_remix_post"] boolValue];
        [self.newsPosts addObject:tPost];
        
    } else if ([dataObj[@"file_type"] isEqualToString:@"photo"]) {
        
        
        ImagePost *iPost = [[ImagePost alloc] initWithNewsDetails:[object[@"post_user_id"] integerValue]
                                                      andUserName:object[@"post_username"]
                                                    andProfileStr:object[@"profile_pic_string"]
                                                         withDate:newsfeedDateTime
                                                      andPostType:[object[@"post_type"] integerValue]
                                                        andNewsID:[object[@"newsfeed_id"] integerValue]
                                                      andTextPost:dataObj[@"text_post"]
                                               andImageFileString:dataObj[@"filestring"]
                                                   andNumComments:[object[@"num_comments"] integerValue]
                                                    andNumRemixes:[object[@"num_remixes"] integerValue]];
        
        id subObj = object[@"remix_details"];
        
        iPost.sharedByPlayer = [[Player alloc] initWithPlayerDetails:[subObj[@"user_id"] integerValue] andUserName:subObj[@"username"]];
        iPost.didSessionUserRemixPost = [object[@"did_session_user_remix_post"] boolValue];
        
        iPost.numLikes = [object[@"num_likes"] integerValue];
        iPost.doesSessionUserLikePost = [object[@"does_session_user_like_post"] boolValue];
        
        [self.newsPosts addObject:iPost];
        
    } 
    
    
}

- (void)loadLockerPost:(NSDictionary *)object
{
    id dataObj = object[@"data_obj"];
    
        NSDate *newsfeedDateTime    = [MyHelpers getDateTimeFromString:object[@"newsfeed_ts"]];
    
    
        LockerPost *iPost = [[LockerPost alloc] initWithNewsDetails:[object[@"post_user_id"] integerValue]
                                                    andUserName:object[@"post_username"]
                                                  andProfileStr:object[@"profile_pic_string"]
                                                       withDate:newsfeedDateTime
                                                    andPostType:[object[@"post_type"] integerValue]
                                                      andNewsID:[object[@"newsfeed_id"] integerValue]
                                                    andTextPost:dataObj[@"text_post"]
                                             andImageFileString:dataObj[@"filestring"]
                                                 andNumComments:[object[@"num_comments"] integerValue]
                                                  andNumRemixes:[object[@"num_remixes"] integerValue]
                                                        andLockerID:[dataObj[@"locker_id"] integerValue]];
    
        id subObj = object[@"remix_details"];
    
        iPost.sharedByPlayer = [[Player alloc] initWithPlayerDetails:[subObj[@"user_id"] integerValue] andUserName:subObj[@"username"]];
        iPost.didSessionUserRemixPost = [object[@"did_session_user_remix_post"] boolValue];
    
    iPost.numLikes = [object[@"num_likes"] integerValue];
    iPost.doesSessionUserLikePost = [object[@"does_session_user_like_post"] boolValue];
    
    /*
    for (id statObj in dataObj[@"stat_tags"]) {
        StatTag *tag = [[StatTag alloc] initWithStatTag:[statObj[@"stat_id"] integerValue]
                                         andStatTagName:statObj[@"stat_name"]
                                        andStatTagValue:statObj[@"stat_value"]
                                            andStatLink:statObj[@"stat_link"]
                                        andStatLinkType:statObj[@"stat_linktype"]
                                         andSubCategory:statObj[@"stat_subcategory"]
                                     andShowSubcategory:[statObj[@"stat_showsubcategory"] boolValue]];
        [iPost.lockPic.statsTagsArray addObject:tag];
    }*/
    
    iPost.lockPic.statsGrp = [[StatsGroup alloc] initWithServerObject:dataObj[@"stat_tags"]];
    
    for (id trainingObj in dataObj[@"training_tags"]) {
        TrainingTag *tag = [[TrainingTag alloc] initWithTrainingTagInfo:[trainingObj[@"training_id"] integerValue]
                                                     andTrainingTagName:trainingObj[@"training_name"]];
        [iPost.lockPic.trainingTagsArray addObject:tag];
    }

    
        [self.newsPosts addObject:iPost];
    
}


- (MatchPlayer *)makeMatchCompetitorObject:(id)object forSportName:(NSString *)sportName andType:(NSString *)sportType
{
    
    MatchPlayer *player = [[MatchPlayer alloc] initWithPlayerDetails:[object[@"competitor_id"] integerValue]
                                                       andProfileStr:object[@"profile_pic_string"]
                                                         andUserName:object[@"username"]
                                                         andFullName:nil
                                                         andLocation:nil
                                                            andAbout:nil
                                                            andCoins:0
                                                           andStatus:nil];
    
    
    PlayerSport *sport = [[PlayerSport alloc] initWithSportDetails:sportName
                                                           andType:sportType
                                                          andLevel:object[@"level"]
                                                    andSportRecord:object[@"win_loss_record"]];
    [player.playerSports addObject:sport];
    
    
    return player;
}

- (void)loadLockerAlbumCovers:(id)serverObject
{
    for (id object in serverObject) {
        LockerAlbumCover *lac = [[LockerAlbumCover alloc] initWithLockerAlbumCoverDetails:[object[@"locker_id"] integerValue]
                                                                               andCreator:[object[@"creator_id"] integerValue]
                                                                             andImageName:object[@"image_name"]
                                                                                andSeason:object[@"season"]
                                                                                  andYear:[object[@"year"] integerValue]
                                                                                 andSport:[[Sport alloc] initWithSportDetails:[object[@"sport_id"] integerValue]
                                                                                                                      andName:object[@"sport_name"]
                                                                                                                      andType:nil]
                                                                               andNumPics:[object[@"num_pics"] integerValue]];
        [self.lockerAlbums addObject:lac];
    }
}

@end
