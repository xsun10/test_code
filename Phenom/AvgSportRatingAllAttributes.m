//
//  AvgSportRatingAllAttributes.m
//  Vaiden
//
//  Created by James Chung on 10/7/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "AvgSportRatingAllAttributes.h"

@implementation AvgSportRatingAllAttributes


-(AvgSportRatingAllAttributes *)initWithAvg:(float)avgRating
                               withNumRatings:(NSUInteger)numRatings


{
    self = [super init];
    
    if (self) {
        _avgRating = avgRating;
        _numRatings = numRatings;
        
    }
    
    return self;
}

@end
