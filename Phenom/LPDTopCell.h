//
//  LPDTopCell.h
//  Vaiden
//
//  Created by James Chung on 5/13/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LPDTopCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *numPropsLabel;
@property (weak, nonatomic) IBOutlet UILabel *numCommentsLabel;
@property (weak, nonatomic) IBOutlet UIImageView *creatorPic;
@property (weak, nonatomic) IBOutlet UILabel *creatorUsernameLabel;
@property (weak, nonatomic) IBOutlet UILabel *creatorFullnameLabel;
@property (weak, nonatomic) IBOutlet UILabel *captionLabel;
@property (weak, nonatomic) IBOutlet UIButton *commentButtonHandle;
@property (weak, nonatomic) IBOutlet UIButton *likeButtonHandle;
@property (weak, nonatomic) IBOutlet UIView *smallDot;
@property (weak, nonatomic) IBOutlet UIImageView *likeIcon;
@property (weak, nonatomic) IBOutlet UIImageView *commentIcon;
@end
