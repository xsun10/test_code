//
//  LandingSlideVC.m
//  Phenom
//
//  Created by James Chung on 6/1/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "LandingSlideVC.h"
#import "UserPreferences.h"
#import "LandingSlideCVCell.h"
#import <FacebookSDK/FacebookSDK.h>
#import "IOSRequest.h"
#import "MatchNotificationsVC.h"
#import "VaidenBaseTabBarController.h"
#import "NSDate+Utilities.h"
#import "UIView+Manipulate.h"

@interface LandingSlideVC ()
@property (weak, nonatomic) IBOutlet UICollectionView *slideCollectionView;
@property (weak, nonatomic) IBOutlet UIPageControl *slidePageControl;
@property (nonatomic, strong) UserPreferences *preferences;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage2;
@property (nonatomic, strong) NSMutableArray *slides;
@property (weak, nonatomic) IBOutlet UIImageView *rightArrowImage;
@property (weak, nonatomic) IBOutlet UILabel *learnLabel;
@property (nonatomic, strong) UIStoryboard *sb;
@property (weak, nonatomic) IBOutlet UIButton *fbButton;

@property (nonatomic, strong) UIViewController *mainViewController;
@property (nonatomic, strong) UINavigationController *navController;
@property (weak, nonatomic) IBOutlet UIButton *registerButton;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;

@end

@implementation LandingSlideVC

- (UIStoryboard *)sb
{
    if (!_sb) _sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    return _sb;
}

- (NSMutableArray *)slides
{
    if (!_slides) _slides = [[NSMutableArray alloc] init];
    return _slides;
}

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}


- (void)loadBaseView
{
    self.mainViewController = self;
 //   self.navController = [[UINavigationController alloc]
  //                         initWithRootViewController:self.mainViewController];
    self.navController = self.navigationController;
    

    if (FBSession.activeSession.state == FBSessionStateCreatedTokenLoaded) {
        
        [self openSession];
    } else {
        //    [self showLoginView];
    }
    
}


- (void)openSession
{
    [FBSession openActiveSessionWithReadPermissions:@[@"basic_info", @"user_location",
                                                      @"user_birthday",
                                                      @"user_likes",
                                                      @"email"
                                                    ]
                                       allowLoginUI:YES
                                  completionHandler:
     ^(FBSession *session,
       FBSessionState state, NSError *error) {
         [self sessionStateChanged:session state:state error:error];
         
         
         [FBRequestConnection
          startForMeWithCompletionHandler:^(FBRequestConnection *connection,
                                            id<FBGraphUser> user,
                                            NSError *error) {
              if (!error) {
                  NSString *userInfo = @"";
                  
                  userInfo = [userInfo
                              stringByAppendingString:
                              [NSString stringWithFormat:@"ID: %@\n\n",
                               user.id]];
                  
                  userInfo = [userInfo
                              stringByAppendingString:
                              [NSString stringWithFormat:@"Email: %@\n\n",
                               user[@"email"]]];
                  
                  userInfo = [userInfo
                              stringByAppendingString:
                              [NSString stringWithFormat:@"Gender: %@\n\n",
                               user[@"gender"]]];
                  
                  
                  // Example: typed access (name)
                  // - no special permissions required
                  userInfo = [userInfo
                              stringByAppendingString:
                              [NSString stringWithFormat:@"Name: %@\n\n",
                               user.name]];
                  
                  // Example: typed access, (birthday)
                  // - requires user_birthday permission
                  userInfo = [userInfo
                              stringByAppendingString:
                              [NSString stringWithFormat:@"Birthday: %@\n\n",
                               user.birthday]];
                  
                  // this block is used to convert the birthday to GTC time
                  NSDateFormatter *df = [[NSDateFormatter alloc] init];
                  [df setDateFormat:@"MM/dd/yyyy"];
                  NSDate *myDate = [df dateFromString: user.birthday];
                  NSDate *nsDateBirthday = [myDate toGlobalTime];
                  
                  NSDateFormatter *df2 = [[NSDateFormatter alloc] init];
                  [df2 setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
                  
                  NSString *stringBirthday = [[df2 stringFromDate: nsDateBirthday] substringWithRange:NSMakeRange(0, 10)];
                  
                  // end block to convert the birthday to GTC time
                  
                  // Example: partially typed access, to location field,
                  // name key (location)
                  // - requires user_location permission
                  userInfo = [userInfo
                              stringByAppendingString:
                              [NSString stringWithFormat:@"Location: %@\n\n",
                               user.location[@"name"]]];
                  
                  // Example: access via key (locale)
                  // - no special permissions required
                  userInfo = [userInfo
                              stringByAppendingString:
                              [NSString stringWithFormat:@"Locale: %@\n\n",
                               user[@"locale"]]];
                  
                  
                  
                  
                  
                  // Example: access via key for array (languages)
                  // - requires user_likes permission
                  if (user[@"languages"]) {
                      NSArray *languages = user[@"languages"];
                      NSMutableArray *languageNames = [[NSMutableArray alloc] init];
                      for (int i = 0; i < [languages count]; i++) {
                          languageNames[i] = languages[i][@"name"];
                      }
                      userInfo = [userInfo
                                  stringByAppendingString:
                                  [NSString stringWithFormat:@"Languages: %@\n\n",
                                   languageNames]];
                  }
                  
                  // Display the user info
                  //               self.userInfoTextView.text = userInfo;
                                    
                  [IOSRequest createOrUpdateFBUser:[[NSNumber alloc] initWithLongLong:[user.id longLongValue]]
                                        fbFullname:user.name
                                           fbEmail:user[@"email"]
                                        fbLocation:user.location[@"name"]
                                             fbDOB:stringBirthday
                                          fbGender:user[@"gender"]
                                    withProfilePic:[self makeFBPicString:[user.id integerValue]]
                                    andDeviceToken:[self.preferences getDeviceToken]
                                      onCompletion:^(NSDictionary *results) {
                                          if (results == nil
                                              || [results isKindOfClass:[NSNull class]]
                                              || [results[@"outcome"] isEqualToString:@"failure"]) {
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                  UIAlertView *alertView = [[UIAlertView alloc]
                                                                            initWithTitle:@"Login Error"
                                                                            message:error.localizedDescription
                                                                            delegate:nil
                                                                            cancelButtonTitle:@"OK"
                                                                            otherButtonTitles:nil];
                                                  [alertView show];
                                              });
                                          } else {
                                              [self.preferences logInSession:[results[@"user_id"] integerValue]
                                                                andPicString:results[@"profile_pic_string"]
                                                                 andUserName:results[@"username"]
                                                            andVerifiedState:[results[@"verified"] boolValue]];
                                              [self.preferences setUserPhoneNumber:results[@"phone_number"]];
                                              [[NSUserDefaults standardUserDefaults] setObject:user[@"email"] forKey:@"email"];
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                  self.fbButton.enabled = YES;
                                                  if ([results[@"username"] isKindOfClass:[NSNull class]] || [results[@"username"] length] == 0) {
                                                      [self performSegueWithIdentifier:@"Enter Username Segue" sender:self];
                                                  } else if ([results[@"num_logins"] integerValue] > 1) {
                                                      
                                                          [self performSegueWithIdentifier:@"Already Logged In Segue" sender:self];

                                                      //   [self performSegueWithIdentifier:@"Intro Segue" sender:self];
                                                  } else {
                                                      [self performSegueWithIdentifier:@"Intro Segue" sender:self];
                                                  }
                                              });
                                          }     
                                      }];
              } else  if (error) {
                  // Handle errors
                  [self handleAuthError:error];
              }
              self.fbButton.enabled = YES;
             
              
          }];
     }];
   
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Already Logged In Segue"]) {
        VaidenBaseTabBarController *controller = segue.destinationViewController;
        controller.showVPage = YES;
    }
}

- (void)handleAuthError:(NSError *)error
{
    NSString *alertText;
    NSString *alertTitle;
    
    if ([FBErrorUtility shouldNotifyUserForError:error] == YES){
        // Error requires people using you app to make an action outside your app to recover
        alertTitle = @"Something went wrong";
        alertText = [FBErrorUtility userMessageForError:error];
        [self showMessage:alertText withTitle:alertTitle];
        
    } else {
        // You need to find more information to handle the error within your app
        if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryUserCancelled) {
            //The user refused to log in into your app, either ignore or...
            alertTitle = @"Login cancelled";
            alertText = @"You need to login to access this part of the app";
            [self showMessage:alertText withTitle:alertTitle];
            
        } else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryAuthenticationReopenSession){
            // We need to handle session closures that happen outside of the app
            alertTitle = @"Session Error";
            alertText = @"Your current session is no longer valid. Please log in again.";
            [self showMessage:alertText withTitle:alertTitle];
            
        } else {
            // All other errors that can happen need retries
            // Show the user a generic error message
  //          alertTitle = @"Something went wrong";
  //          alertText = @"Please retry";
  //          [self showMessage:alertText withTitle:alertTitle];
        }
    }
}

- (void)showMessage:(NSString *)text withTitle:(NSString *)title
{
    [[[UIAlertView alloc] initWithTitle:title
                                message:text
                               delegate:self
                      cancelButtonTitle:@"OK"
                      otherButtonTitles:nil] show];
}

- (NSString *)makeFBPicString:(NSUInteger)fbID
{
    return [NSString stringWithFormat:@"http://graph.facebook.com/%lu/picture?type=square", fbID];
}

- (IBAction)FBLogin:(id)sender
{
    NSString *versionOnPhone = [NSString stringWithFormat:@"%@", [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]];
    
    [IOSRequest checkIfAppNeedsUpdateForPhoneVersion:versionOnPhone
                                        onCompletion:^(NSDictionary *results) {
        
        if ([results[@"need_update"] boolValue]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertView *alert = [[UIAlertView alloc] init];
                alert.title = @"Update Available";
                alert.message = @"A newer version of the Vaiden App is available.  Please download it from the App Store to proceed.";
                [alert addButtonWithTitle:@"OK"];
                [alert show];
            });
            
        } else {
            self.fbButton.enabled = NO;
            [self openSession];
        }
    }];
    
    
}

- (void)sessionStateChanged:(FBSession *)session
                      state:(FBSessionState) state
                      error:(NSError *)error
{
    switch (state) {
        case FBSessionStateOpen: {
     //       UIViewController *topViewController =
     //       [self.navController topViewController];
            
            
        }
            break;
        case FBSessionStateClosed:
        case FBSessionStateClosedLoginFailed:
            
            [FBSession.activeSession closeAndClearTokenInformation];
            
        //    [self showLoginView];
            break;
        default:
            break;
    }
    
    if (error) {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Error"
                                  message:error.localizedDescription
                                  delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    self.preferences = nil;
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
    
    [self.preferences setHasMatchNotificationsPageBeenLoadedAtLeastOnce:NO];
    self.slideCollectionView.delegate = self;
    self.slideCollectionView.dataSource = self;
    self.slideCollectionView.decelerationRate = UIScrollViewDecelerationRateFast;
    
    [self.registerButton makeRoundedBorderWithRadius:3];
    [self.loginButton makeRoundedBorderWithRadius:3];
    [self.fbButton makeRoundedBorderWithRadius:3];
    
    [self initializeSlides];
    
    // initialize background view
    UIImage * toImage = (self.slides)[0];
    [UIView transitionWithView:self.view
                      duration:0.5
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        self.backgroundImage.image = toImage;
                    } completion:nil];
    [self.backgroundImage setContentMode:UIViewContentModeCenter];
    
    [self.slidePageControl setCurrentPage:0];
    // end initialize background view
    
    [self.slideCollectionView reloadData];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden: YES animated:NO];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];

}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone)
    {
        if ([[UIScreen mainScreen] bounds].size.height == 568)
        {
            
            //iphone 5
            self.slidePageControl.frame = CGRectOffset(self.slidePageControl.frame, 0, -20);
            
            
        }
        else
        {
            
            self.slideCollectionView.frame = CGRectOffset(self.slideCollectionView.frame, 0, -12);
            
            
        }
    }
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    NSString *versionOnPhone = [NSString stringWithFormat:@"%@", [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]];
    
    [IOSRequest checkIfAppNeedsUpdateForPhoneVersion:versionOnPhone
                                        onCompletion:^(NSDictionary *results) {
        
        if ([results[@"need_update"] boolValue]) {
            UIAlertView *alert = [[UIAlertView alloc] init];
            alert.title = @"Update Available";
            alert.message = @"A newer version of the Vaiden App is available.  Please download it from the App Store to proceed.";
            [alert addButtonWithTitle:@"OK"];
            [alert show];
        } else {
            [self loadBaseView];
        }
    }];
    
}

- (void)initializeSlides
{
    [self.slides addObject:[UIImage imageNamed:@"Roman Slide"]];
//    [self.slides addObject:[UIImage imageNamed:@"Landing Slide 1"]];
//    [self.slides addObject:[UIImage imageNamed:@"Landing Slide 2"]];
//    [self.slides addObject:[UIImage imageNamed:@"Landing Slide 3"]];
//    [self.slides addObject:[UIImage imageNamed:@"Landing Slide 4"]];
//    [self.slides addObject:[UIImage imageNamed:@"Landing Slide 5"]];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
  //  return 5;
    return 1;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Landing Cell";
    
    LandingSlideCVCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    
   
    
    cell.titleLabel.text = [self getTitleText:indexPath.row];
    
    if ([cell.titleLabel.text isEqualToString:@"VAIDEN"]) {
        cell.titleLabel.font = [UIFont fontWithName:@"TrebuchetMS" size:35.0f];
    } else {
        cell.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-CondensedBold" size:25.0f];
    }
    cell.messageLabel.text = [self getMessageText:indexPath.row];


    
    
    
    return cell;
}

- (NSString *)getTitleText:(NSInteger)row
{
    NSString *resultText = @"";

    switch (row)
    {
        case 0: resultText = @"VAIDEN";
            break;
        case 1: resultText = @"Challenge Players";
            break;
        case 2: resultText = @"Get A Sports Level";
            break;
        case 3: resultText = @"Find Players";
            break;
        case 4: resultText = @"Get Featured";
            break;
    }
 //   return resultText;
    return @"";
}

- (NSString *)getMessageText:(NSInteger)row
{
    NSString *resultText = @"";
    
    switch (row)
    {
        case 0: resultText = @"Vaiden is a community for amateur athletes to play their favorite sports using a social platform.";
            self.rightArrowImage.hidden = NO;
            self.learnLabel.hidden = NO;
            break;
        case 1: resultText = @"See how you stack up against other players.  Challege them 1 vs 1 in individual sports.";
            self.rightArrowImage.hidden = NO;
            self.learnLabel.hidden = NO;
            break;
        case 2: resultText = @"Compete and you'll earn a sports level that indicates your ability to play a sport.";
            self.rightArrowImage.hidden = NO;
            self.learnLabel.hidden = NO;
            break;
        case 3: resultText = @"Use Vaiden to find players in your area that play sports at your level.";
            self.rightArrowImage.hidden = NO;
            self.learnLabel.hidden = NO;
            break;
        case 4: resultText = @"Get to the top of our leaderboard and you can be eligible to be featured by Vaiden.";
            self.rightArrowImage.hidden = YES;
            self.learnLabel.hidden = YES;
            break;
    }
    
  //  return resultText;
    return @"";
}

- (void)setMyBackgroundImage:(NSInteger)row
{/*
    switch (row)
    {
        case 0: self.backgroundImage.image = [UIImage imageNamed:@"landing_slide1.jpg"];
            break;
        case 1: self.backgroundImage.image = [UIImage imageNamed:@"landing_slide2.jpg"];
            break;
        case 2: self.backgroundImage.image = [UIImage imageNamed:@"landing_slide3.jpg"];
            break;
        case 3: self.backgroundImage.image = [UIImage imageNamed:@"landing_slide4.jpg"];
            
    }*/
}


- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (IBAction)loginAction:(id)sender
{
    [self performSegueWithIdentifier:@"Log in Segue" sender:self];
}

- (IBAction)registerAction:(id)sender
{
    [self performSegueWithIdentifier:@"Create Account Segue" sender:self];
}

- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath{
    LandingSlideCVCell* currentCell = ([[collectionView visibleCells]count] > 0) ? [collectionView visibleCells][0] : nil;
    
    if(cell != nil){
        NSInteger rowIndex = [collectionView indexPathForCell:currentCell].row;
        
        UIImage * toImage = (self.slides)[rowIndex];
        [UIView transitionWithView:self.view
                          duration:0.5
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{
                            self.backgroundImage.image = toImage;
                        } completion:nil];
        
        [self.slidePageControl setCurrentPage:rowIndex];

    }
}

@end
