//
//  CommentDialog.h
//  Vaiden
//
//  Created by Turbo on 7/11/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CommentDialogDelegate <NSObject>
-(void)startSumitComments:(NSString *)comments;
@end

@interface CommentDialog : UIViewController
@property (nonatomic, weak) id <CommentDialogDelegate>delegate;

@property (assign) BOOL isTextPost;
@end
