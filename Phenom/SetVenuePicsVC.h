//
//  SetVenuePicsVC.h
//  Vaiden
//
//  Created by James Chung on 10/8/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "CustomBaseVC.h"

@interface SetVenuePicsVC : CustomBaseVC <UICollectionViewDataSource, UICollectionViewDelegate, MBProgressHUDDelegate>
{
NSOperationQueue         *operationQueue;
MBProgressHUD *HUD;

}
@property (nonatomic) NSUInteger venueID;
@property (nonatomic) NSInteger numVCsToPopBack;

@end
