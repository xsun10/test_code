//
//  CompeteSplashTVC.m
//  Vaiden
//
//  Created by James Chung on 6/1/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "CompeteSplashTVC.h"
#import "UIView+Manipulate.h"
#import "ChallangeMatchHowItWorksVC.h"

@interface CompeteSplashTVC ()
@property (weak, nonatomic) IBOutlet UIButton *challengeGetStartedButton;
@property (weak, nonatomic) IBOutlet UIButton *pickupScheduleButton;
@property (weak, nonatomic) IBOutlet UIButton *pickupMatchesNearbyButton;
@property (weak, nonatomic) IBOutlet UIButton *howItWorkBtn1;
@property (weak, nonatomic) IBOutlet UIButton *howItWorkBtn2;

@end

@implementation CompeteSplashTVC


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];

    
    [self.challengeGetStartedButton makeRoundedBorderWithRadius:3];
    [self.pickupMatchesNearbyButton makeRoundedBorderWithRadius:3];
    [self.pickupScheduleButton makeRoundedBorderWithRadius:3];
    [self.howItWorkBtn1 makeRoundedBorderWithRadius:3];
    [self.howItWorkBtn2 makeRoundedBorderWithRadius:3];
}
- (IBAction)challengeGetStartedAction:(id)sender
{
    [self performSegueWithIdentifier:@"Challenge Segue" sender:self];
}

- (IBAction)pickupScheduleAMatchAction:(id)sender
{
    [self performSegueWithIdentifier:@"Add Pickup Segue" sender:self];
}

- (IBAction)pickupMatchesNearbyAction:(id)sender
{
    [self performSegueWithIdentifier:@"Pickup Nearby Segue" sender:self];
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    ChallangeMatchHowItWorksVC *vc = [segue destinationViewController];
    if ([segue.identifier isEqualToString:@"challange_segue"]) {
        [vc setSegueName:@"challange"];
    } else if ([segue.identifier isEqualToString:@"pickup_segue"]) {
        [vc setSegueName:@"pickup"];
    }
}
@end
