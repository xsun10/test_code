//
//  HowItWorksCell.h
//  Vaiden
//
//  Created by Turbo on 8/4/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HowItWorksCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UIImageView *left_icon;
@property (weak, nonatomic) IBOutlet UIImageView *right_icon;

@end
