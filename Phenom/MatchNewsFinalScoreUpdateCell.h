//
//  MatchNewsFinalScoreUpdateCell.h
//  Vaiden
//
//  Created by James Chung on 11/1/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewsfeedMatchHeadingCV.h"
#import "IndividualMatch.h"

@class MatchNewsFinalScoreUpdateCell;

@protocol MatchNewsFinalScoreUpdateCell_delegate <NSObject>
-(void)setMatchNewsFinalScoreUpdateCellParentViewController:(MatchNewsFinalScoreUpdateCell *)controller withUserIDToSegue:(NSInteger)userID;
@end


@interface MatchNewsFinalScoreUpdateCell : UITableViewCell <UICollectionViewDataSource, UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *newsfeedIcon;
@property (nonatomic, weak) id <MatchNewsFinalScoreUpdateCell_delegate> delegate;

@property (weak, nonatomic) IBOutlet UIView *myBackgroundViewCell;
@property (weak, nonatomic) IBOutlet UILabel *matchNameLabel;
@property (weak, nonatomic) IBOutlet UIView *picViewBackground;
@property (weak, nonatomic) IBOutlet UIImageView *picView;
@property (weak, nonatomic) IBOutlet UILabel *headlineMessage;
@property (weak, nonatomic) IBOutlet UILabel *dateTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *scoreStatusLabel;
@property (weak, nonatomic) IBOutlet UILabel *player1Username;
@property (weak, nonatomic) IBOutlet UILabel *player2Username;
@property (weak, nonatomic) IBOutlet UILabel *player1ScoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *player2ScoreLabel;
@property (weak, nonatomic) IBOutlet UIButton *detailsButton;
@property (weak, nonatomic) IBOutlet UILabel *supplementaryMessageLabel;

@property (weak, nonatomic) IBOutlet UILabel *sportLevelsHeader;
@property (weak, nonatomic) IBOutlet UILabel *player1UsernameV2;
@property (weak, nonatomic) IBOutlet UILabel *player2UsernameV2;
@property (weak, nonatomic) IBOutlet UILabel *player1Level;
@property (weak, nonatomic) IBOutlet UILabel *player2Level;
@property (weak, nonatomic) IBOutlet UILabel *winningPlayerUsername;
@property (weak, nonatomic) IBOutlet UILabel *losingPlayerUsername;
@property (weak, nonatomic) IBOutlet NewsfeedMatchHeadingCV *matchHeadingCV;
@property (weak, nonatomic) IBOutlet UIButton *actionButtonHandle;

@property (weak, nonatomic) IBOutlet UILabel *commentNumberLabel;
@property (weak, nonatomic) IBOutlet UIButton *commentButton;
@property (weak, nonatomic) IBOutlet UIImageView *commentButtonIcon;

@property (weak, nonatomic) IBOutlet UILabel *postTimeLabel;

@property (weak, nonatomic) IBOutlet UIImageView *userImage1;
@property (weak, nonatomic) IBOutlet UIImageView *userImage2;

@property (weak, nonatomic) IBOutlet UILabel *numCommentLabel;
@property (weak, nonatomic) IBOutlet UILabel *sharedByLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *scoreCV;
@property (weak, nonatomic) IBOutlet UIImageView *sportIcon;
@property (weak, nonatomic) IBOutlet UILabel *sportLabel;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;

@property (weak, nonatomic) IBOutlet UIView *vsCircle;
@property (nonatomic, strong) NSString *matchCategory;
@property (nonatomic, strong) NSMutableArray *competitors;
@property (nonatomic, strong) IndividualMatch *iMatch;

@property (weak, nonatomic) IBOutlet UIView *likeButtonBackground;
@property (weak, nonatomic) IBOutlet UIImageView *likeIcon;
@property (weak, nonatomic) IBOutlet UILabel *likeNumberLabel;
@property (weak, nonatomic) IBOutlet UIButton *likeButton;
@property (weak, nonatomic) IBOutlet UIButton *viewLikesButton;
@property (weak, nonatomic) IBOutlet UIButton *viewCommentsButton;
@property (weak, nonatomic) IBOutlet UIView *smallCircleView;
@property (weak, nonatomic) IBOutlet UIView *sportIconBackground;


@end
