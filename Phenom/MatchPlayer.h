//
//  MatchPlayer.h
//  Phenom
//
//  Created by James Chung on 5/1/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "Player.h"

@interface MatchPlayer : Player

@property (nonatomic, strong) NSString *status;
@property (nonatomic) BOOL wasInvited;

- (MatchPlayer *) initWithPlayerDetails:(NSInteger)userID
                          andProfileStr:(NSString *)profileBaseString
                            andUserName:(NSString *)userName
                            andFullName:(NSString *)fullName
                            andLocation:(NSString *)location
                               andAbout:(NSString *)about
                               andCoins:(float)coins
                              andStatus:(NSString *)status;

- (MatchPlayer *) initWithPlayerDetails:(NSInteger)userID
                            andUserName:(NSString *)userName;

+ (MatchPlayer *)makeMatchCompetitorObject:(NSArray *)objectArray
                                 forPlayer:(NSInteger)playerNum
                              forSportName:(NSString *)sportName
                                   andType:(NSString *)sportType;

//- (NSString *)getProfileStringOfCompetitor:(NSInteger)competitorID inArray:(NSArray *)competitorArray;

@end
