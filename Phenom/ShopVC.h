//
//  ShopVC.h
//  Vaiden
//
//  Created by James Chung on 2/7/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomBaseVC.h"

@interface ShopVC : CustomBaseVC

@property (nonatomic, strong) NSString *url;

@end
