//
//  ChooseIndividualSportCell.h
//  Vaiden
//
//  Created by James Chung on 8/5/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChooseIndividualSportCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *sportIcon;
@property (weak, nonatomic) IBOutlet UILabel *sportName;

@end
