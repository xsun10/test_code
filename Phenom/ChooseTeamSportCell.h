//
//  ChooseTeamSportCell.h
//  Vaiden
//
//  Created by James Chung on 4/10/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChooseTeamSportCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *sportIcon;
@property (weak, nonatomic) IBOutlet UILabel *sportName;

@end
