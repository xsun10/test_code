//
//  CheckInTVC.m
//  Vaiden
//
//  Created by James Chung on 4/22/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

// NOTE: A PLAYER CAN ONLY CHECK IN ONCE AT A VENUE WITHIN 24 HOUR PERIOS.  ALSO, THEY MUST BE WITHIN 2 MILES OF THE VENUE

#import "CheckInTVC.h"
#import <FacebookSDK/FacebookSDK.h>
#import "UserPreferences.h"
#import "ChooseOneSportTVC.h"
#import "IOSRequest.h"
#import "NSDate+Utilities.h"
#import "UITextView+FormValidations.h"

@interface CheckInTVC () <UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UISegmentedControl *timeSegmentControl;
@property (nonatomic, strong) NSMutableArray *contactsArrayCopy;
@property (nonatomic) UIDatePicker *datePicker;
@property (nonatomic) NSInteger datePickerHeight;
@property (nonatomic) BOOL datePickerIsShowing;
@property (nonatomic, strong) UIButton *closeDatePickerButton;
@property (nonatomic, strong) UIView *closeDatePickerView;
@property (weak, nonatomic) IBOutlet UILabel *checkInDateTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *venueNameLabel;
@property (weak, nonatomic) IBOutlet UISwitch *newsfeedSwitchControl;
@property (weak, nonatomic) IBOutlet UISwitch *facebookSwitchControl;
@property (nonatomic, strong) UserPreferences *preferences;
@property (weak, nonatomic) IBOutlet UITextView *messageTextView;
@property (nonatomic, strong) UILabel *messageTextRemaining;
@property (nonatomic, strong) UIBarButtonItem *doneBarButton;
@property (nonatomic) CGFloat longitude;
@property (nonatomic) CGFloat latitude;

// Final info
@property (nonatomic, strong) Sport *chosenSport;
@property (nonatomic, strong) NSDate *chosenDateTime;

@end

@implementation CheckInTVC

#define TIME_SEGMENT_NOW 0
#define TIME_SEGMENT_FUTURE 1

#define DATE_SECTION 0
#define SPORT_SECTION 1
#define MESSAGE_SECTION 2
#define NOTIFY_SECTION 3

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (UIDatePicker *)datePicker
{
    if (!_datePicker) _datePicker = [[UIDatePicker alloc] init];
    return _datePicker;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
    
    self.venueNameLabel.text = self.venueName;
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    self.datePicker.datePickerMode = UIDatePickerModeDateAndTime;
    self.datePicker.maximumDate = [NSDate dateWithTimeIntervalSinceNow:86400];
    self.datePicker.minimumDate = [NSDate date];
    self.datePickerHeight = 95; // was 44
    self.datePickerIsShowing = NO;
    
    self.messageTextView.delegate = self;
    
    //[self setDoneButton];
    [self setDefaultTimes];
    [self setMessageTextRemainingDefault];
}

/*- (void)setDoneButton
{
    UIButton *btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 50, 21);
    btn.layer.borderColor = [UIColor whiteColor].CGColor;
    btn.layer.borderWidth = 1.0;
    
    [btn addTarget:self action:@selector(doneAction:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundColor:[UIColor clearColor]];
    [btn setTitle:@"DONE" forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-MediumItalic" size:12.0]];
    
    // Initialize UIBarbuttonitem...
    self.doneBarButton = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = self.doneBarButton;
}*/

- (IBAction)doneAction:(id)sender
{
    [self.messageTextView resignFirstResponder];
    if ([self validateInputs]) {
        [self initLocation];
  
    }
}

- (BOOL)validateInputs
{
    if (!self.chosenSport) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"CheckIn Error";
        alert.message = @"Please choose a sport";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
        
        return  NO;
    } else if ([self.messageTextView.text length] > 150) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"CheckIn Error";
        alert.message = @"Please limit your message size to 150 characters";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
        
        return NO;
    }
    
    return YES;
}

- (void)setMessageTextRemainingDefault
{
    self.messageTextRemaining = [[UILabel alloc] initWithFrame:CGRectMake(220, 475, 80, 21)];
    self.messageTextRemaining.textAlignment = NSTextAlignmentRight;
    self.messageTextRemaining.text = @"150 Remaining";
    self.messageTextRemaining.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:11];
    self.messageTextRemaining.textColor = [UIColor lightGrayColor];
    
    [self.tableView addSubview:self.messageTextRemaining];
    
    
}

- (void)setDefaultTimes
{
    self.timeSegmentControl.selectedSegmentIndex = TIME_SEGMENT_NOW;
    NSDate *myDate = [NSDate date];
    self.chosenDateTime = myDate;
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"M/dd/yyyy hh:mm a"];
    self.checkInDateTimeLabel.text = [dateFormat stringFromDate:myDate];

}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Contacts Segue"]) {
        ChooseContactsUtilityTVC *controller = segue.destinationViewController;
        controller.delegate = self;
        controller.pageIsForLockerPeopleTag = NO;
        controller.maxContactsSelected = 10;
        controller.errorOverlayLongMessage = @"You do not have any contacts who have notifications turned on.  Please follow other players or invite your friends to join.";
    } else if ([segue.identifier isEqualToString:@"Sport Segue"]) {
        ChooseOneSportTVC *controller = segue.destinationViewController;
        controller.delegate = self;
        
    }
}

- (void)setWithAllContactsController:(ChooseContactsUtilityTVC *)controller withContactsArray:(NSMutableArray *)contactsArray
{
    UITableViewCell *notifyCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:NOTIFY_SECTION]];
    
    NSString *contactsSingularPlural;
    
    if ([contactsArray count] == 1)
        contactsSingularPlural = @"Contact";
    else
        contactsSingularPlural = @"Contacts";
    
    notifyCell.detailTextLabel.text = [NSString stringWithFormat:@"%ld %@", [contactsArray count], contactsSingularPlural];
    
    self.contactsArrayCopy = [NSMutableArray arrayWithArray:contactsArray];
}

- (IBAction)changeTimeSegment:(UISegmentedControl *)sender
{
    [self.messageTextView resignFirstResponder];
    
    if (self.timeSegmentControl.selectedSegmentIndex == TIME_SEGMENT_NOW) {
        self.timeSegmentControl.hidden = NO;
        
        NSDate *myDate = [NSDate date];
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"M/dd/yyyy hh:mm a"];
        self.checkInDateTimeLabel.text = [dateFormat stringFromDate:myDate];
                
    } else if (self.timeSegmentControl.selectedSegmentIndex == TIME_SEGMENT_FUTURE) {
        // Can check in up to 24 hours in advance
        
        self.timeSegmentControl.hidden = YES;
        self.checkInDateTimeLabel.hidden = YES;
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:DATE_SECTION];
        
        UITableViewCell* cell = (UITableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];

        if (indexPath.section == DATE_SECTION && !self.datePickerIsShowing) {
            
            
            [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:DATE_SECTION] atScrollPosition:UITableViewScrollPositionTop animated:YES];
            
            self.datePicker.alpha = 0;
            [self.tableView beginUpdates];
            self.datePickerHeight = 95 + 190; // 95 was 44
            
            
            [self.tableView endUpdates];
            
            
            [cell addSubview:self.datePicker];
            
            
            [UIView animateWithDuration:0.2 animations:^{
                self.datePicker.alpha = 1;
                self.datePickerIsShowing = YES;
                
                self.closeDatePickerView = [[UIView alloc] initWithFrame:CGRectMake(0, cell.frame.origin.y, 320, 40)];
                self.closeDatePickerView.backgroundColor = [UIColor darkGrayColor];
                self.closeDatePickerView.alpha = 0.5;
                
                self.closeDatePickerButton = [[UIButton alloc] initWithFrame:CGRectMake(260,  7, 50, 25)];
                [self.closeDatePickerButton setTitle:@"CLOSE" forState:UIControlStateNormal];
                [self.closeDatePickerButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-MediumItalic" size:11]];
                self.closeDatePickerButton.layer.borderColor = [UIColor lightGrayColor].CGColor;
                self.closeDatePickerButton.layer.borderWidth = 1.0;
                
                [self.closeDatePickerButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
                [self.closeDatePickerButton addTarget:self action:@selector(tapDate:) forControlEvents:UIControlEventTouchUpInside];
                
                [self.closeDatePickerView addSubview:self.closeDatePickerButton];
                [self.tableView addSubview:self.closeDatePickerView];
                
            } completion:nil];
            
            
        } else if (indexPath.section == DATE_SECTION && self.datePickerIsShowing) {
            
        }

        
    }
}

- (void)tapDate:(UIButton *)sender
{
      [UIView animateWithDuration:0.5 animations:^(void) {
        self.closeDatePickerView.alpha = 0.0;
          
        if (self.datePickerIsShowing) {
            [self performCloseCell:[NSIndexPath indexPathForRow:0 inSection:DATE_SECTION]];
            self.datePickerIsShowing = NO;
        }
    }];
    
    self.timeSegmentControl.selectedSegmentIndex = 0;
    
}

- (void)performCloseCell:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == DATE_SECTION && self.datePickerIsShowing) {
        
        [UIView animateWithDuration:0.2 animations:^{self.datePicker.alpha = 0;} completion:^(BOOL finished){[self.datePicker removeFromSuperview];}];
    //    UITableViewCell *cell = (UITableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
        NSDate *myDate = self.datePicker.date;
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        //    [dateFormat setDateFormat:@"MMM d yyyy"];
        [dateFormat setDateFormat:@"M/dd/yyyy hh:mm a"];
        self.checkInDateTimeLabel.text = [dateFormat stringFromDate:myDate];
  //      cell.textLabel.text = @"Date";
        
        
        self.timeSegmentControl.hidden = NO;
        self.checkInDateTimeLabel.hidden = NO;
        self.chosenDateTime = myDate;
        
 //       self.individualMatch.dateTime = myDate;
        
        
        [self.tableView beginUpdates];
        
        if (indexPath.section == DATE_SECTION)
            self.datePickerHeight = 95; // was 44
        
        [self.tableView endUpdates];
    }
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row == 0 && indexPath.section == DATE_SECTION) {
            return self.datePickerHeight;
       
    } else if (indexPath.row == 0 && indexPath.section == MESSAGE_SECTION) {
        return 95;
    }
    
    return 44;
}

- (void)textViewDidChange:(UITextView *)textView
{
    self.messageTextRemaining.text = [NSString stringWithFormat:@"%ld Remaining", 150 - [textView.text length]];
   
    if ([textView.text length] > 150)
        self.messageTextRemaining.textColor = [UIColor redColor];
    else
        self.messageTextRemaining.textColor = [UIColor lightGrayColor];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.messageTextView resignFirstResponder];
    
}

- (IBAction)facebookSwitchAction:(UISwitch *)sender
{
    [self.messageTextView resignFirstResponder];
    if (sender.on) {
        [self startFBShare];
    }
}


- (void)startFBShare
{
    
    if (FBSession.activeSession.isOpen && [FBSession.activeSession.permissions indexOfObject:@"publish_actions"] == NSNotFound) {
        // Request publish_actions
        [FBSession.activeSession requestNewPublishPermissions:[NSArray arrayWithObject:@"publish_actions"]
                                              defaultAudience:FBSessionDefaultAudienceFriends
                                            completionHandler:^(FBSession *session, NSError *error) {
                                                __block NSString *alertText;
                                                __block NSString *alertTitle;
                                                if (!error) {
                                                    if ([FBSession.activeSession.permissions
                                                         indexOfObject:@"publish_actions"] == NSNotFound){
                                                        // Permission not granted, tell the user we will not publish
                                                        alertTitle = @"Permission not granted";
                                                        alertText = @"Your action will not be published to Facebook.";
                                                        [[[UIAlertView alloc] initWithTitle:@"title"
                                                                                    message:@"text"
                                                                                   delegate:self
                                                                          cancelButtonTitle:@"OK!"
                                                                          otherButtonTitles:nil] show];
                                                    } else {
                                                        // Permission granted, publish the OG story
                                                        [self presentFBDialog];
                                                    }
                                                    
                                                } else {
                                                    // There was an error, handle it
                                                    // See https://developers.facebook.com/docs/ios/errors/
                                                }
                                            }];
        
    } else {
        // permission exists
        [self presentFBDialog];
    }
    
    
    
    
    
}

- (void)presentFBDialog
{
    NSString *checkedInTitle = @"";
    
    if (self.timeSegmentControl.selectedSegmentIndex == TIME_SEGMENT_NOW) {
        if (self.chosenSport)
            checkedInTitle = [NSString stringWithFormat:@"@%@ checked in at %@ to play %@", [self.preferences getUserName], self.venueName, [self.chosenSport.sportName capitalizedString]];
        else
            checkedInTitle = [NSString stringWithFormat:@"@%@ checked in at %@", [self.preferences getUserName], self.venueName];
    } else {
        if (self.chosenSport)
            checkedInTitle = [NSString stringWithFormat:@"@%@ will be at %@ at %@ to play %@", [self.preferences getUserName], self.venueName, self.checkInDateTimeLabel.text, [self.chosenSport.sportName capitalizedString]];
        else
            checkedInTitle = [NSString stringWithFormat:@"@%@ will be at %@ at %@", [self.preferences getUserName], self.venueName, self.checkInDateTimeLabel.text];
    }
    
    NSString *playerMessage = @"";
    
    if ([self.messageTextView.text length] > 0)
        playerMessage = [NSString stringWithFormat:@"%@ says: \"%@\"", [self.preferences getUserName], self.messageTextView.text];
    else
        playerMessage = @"";
    
    // Put together the dialog parameters
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   checkedInTitle, @"name",
                                   playerMessage , @"caption",
                                   @"Vaiden is a new athletic brand that combines Sports + Technology + Social", @"description",
                                   @"https://itunes.apple.com/us/app/vaiden/id796761406?ls=1&mt=8", @"link",
                                   @"https://s3.amazonaws.com/vaderen_pictures/FBCheckIn.jpg", @"picture",
                                   nil];
    
    // Show the feed dialog
    [FBWebDialogs presentFeedDialogModallyWithSession:nil
                                           parameters:params
                                              handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
                                                  if (error) {
                                                      // An error occurred, we need to handle the error
                                                      // See: https://developers.facebook.com/docs/ios/errors
                                                      //      NSLog([NSString stringWithFormat:@"Error publishing story: %@", error.description]);
                                                  } else {
                                                      if (result == FBWebDialogResultDialogNotCompleted) {
                                                          // User cancelled.
                                                          NSLog(@"User cancelled.");
                                                      } else {
                                                          // Handle the publish feed callback
                                                          NSDictionary *urlParams = [self parseURLParams:[resultURL query]];
                                                          
                                                          if (![urlParams valueForKey:@"post_id"]) {
                                                              // User cancelled.
                                                              NSLog(@"User cancelled.");
                                                              
                                                          } else {
                                                              // User clicked the Share button
                                                              NSString *result = [NSString stringWithFormat: @"Posted story, id: %@", [urlParams valueForKey:@"post_id"]];
                                                              NSLog(@"result %@", result);
                                                          }
                                                      }
                                                  }
                                              }];
}

// A function for parsing URL parameters returned by the Feed Dialog.
- (NSDictionary*)parseURLParams:(NSString *)query {
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *pair in pairs) {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        NSString *val =
        [kv[1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        params[kv[0]] = val;
    }
    return params;
}


- (IBAction)tapOffMessageTextView:(id)sender
{
    [self.messageTextView resignFirstResponder];
}

- (void)setViewController:(ChooseOneSportTVC *)controller withSport:(Sport *)sp
{
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:SPORT_SECTION]];
    
    cell.detailTextLabel.text = [sp.sportName capitalizedString];
    self.chosenSport = sp;
}


- (void)initLocation
{
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    [locationManager startUpdatingLocation];
    
    
}

- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations {
    CLLocation *location = locations[0];
    NSLog(@"user location: lat%f - lon%f", location.coordinate.latitude, location.coordinate.longitude);
    [locationManager stopUpdatingLocation];
    
    self.longitude = location.coordinate.longitude;
    self.latitude = location.coordinate.latitude;
    
    // now can submit check in to server
    
    [self sendToServer];
    
}

- (void)sendToServer
{
    NSInteger isNow = 1;
    
    if (self.timeSegmentControl.selectedSegmentIndex == TIME_SEGMENT_NOW)
        isNow = 1;
    else
        isNow = 0;
    
    NSInteger postToNewsfeed = 1;
    
    if (self.newsfeedSwitchControl.isEnabled) {
        postToNewsfeed = 1;
    } else {
        postToNewsfeed = 0;
    }

    self.doneBarButton.enabled = NO;
    [self.messageTextRemaining resignFirstResponder];
    
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.labelText = @"Checking In";
    
    [IOSRequest checkInUser:[self.preferences getUserID]
                atLongitude:self.longitude
                 atLatitude:self.latitude
                    atVenue:self.venueID
                 atDateTime:[self.chosenDateTime toGlobalTime]
                      isNow:isNow
                  withSport:self.chosenSport.sportID
                 andMessage:self.messageTextView.text
                  andNotify:self.contactsArrayCopy
             postToNewsfeed:postToNewsfeed
               onCompletion:^(NSDictionary *results) {
                   
                   dispatch_async(dispatch_get_main_queue(), ^{
                       self.doneBarButton.enabled = YES;
                       [MBProgressHUD hideHUDForView:self.view animated:YES];
                       
                       if ([results[@"outcome"] isEqualToString:@"success"]) {
                           [self.preferences setNewsfeedRefreshState:YES];
                           [self.preferences setProfilePageRefreshState:YES];
                           
                           [self.navigationController popToRootViewControllerAnimated:YES];
                           
                       } else if ([results[@"outcome"] isEqualToString:@"TOO_FAR"]) {
                           UIAlertView *alert = [[UIAlertView alloc] init];
                           alert.title = @"CheckIn Error";
                           alert.message = @"You are not close enough to venue to check in";
                           [alert addButtonWithTitle:@"OK"];
                           [alert show];
                       } else if ([results[@"outcome"] isEqualToString:@"TOO_MUCH"]) {
                           UIAlertView *alert = [[UIAlertView alloc] init];
                           alert.title = @"CheckIn Error";
                           alert.message = @"You can only check in to a venue once within 6 hours";
                           [alert addButtonWithTitle:@"OK"];
                           [alert show];
                       } else if ([results[@"outcome"] isEqualToString:@"failure"]) {
                           UIAlertView *alert = [[UIAlertView alloc] init];
                           alert.title = @"CheckIn Error";
                           alert.message = @"There was an error completing your request.  Please try again.";
                           [alert addButtonWithTitle:@"OK"];
                           [alert show];
                       }
                   });
               }];
    
    double delayInSeconds = 10.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        //code to be executed on the main queue after delay
        self.doneBarButton.enabled = YES;
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    });
}

@end
