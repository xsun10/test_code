//
//  MatchType.h
//  Vaiden
//
//  Created by James Chung on 8/6/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MatchType : NSObject

@property (nonatomic, strong) NSString *description;
@property (nonatomic) NSInteger matchTypeID;

- (MatchType *) initWithMatchType:(NSInteger)matchTypeID
                  withDescription:(NSString *)description;


@end
