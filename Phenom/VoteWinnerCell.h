//
//  VoteWinnerCell.h
//  Vaiden
//
//  Created by Turbo on 8/1/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VoteWinnerCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *outerView;
@property (weak, nonatomic) IBOutlet UIView *innerView;
@property (weak, nonatomic) IBOutlet UILabel *username1;
@property (weak, nonatomic) IBOutlet UILabel *username2;
@property (weak, nonatomic) IBOutlet UILabel *username3;
@property (weak, nonatomic) IBOutlet UILabel *username4;
@property (weak, nonatomic) IBOutlet UILabel *username5;
@property (weak, nonatomic) IBOutlet UIImageView *pic1;
@property (weak, nonatomic) IBOutlet UIImageView *smallPic3;
@property (weak, nonatomic) IBOutlet UIImageView *smallPic1;
@property (weak, nonatomic) IBOutlet UIImageView *smallPic2;
@property (weak, nonatomic) IBOutlet UIImageView *smallPic4;
@property (weak, nonatomic) IBOutlet UILabel *money3;
@property (weak, nonatomic) IBOutlet UILabel *money4;
@property (weak, nonatomic) IBOutlet UILabel *money5;
@property (weak, nonatomic) IBOutlet UILabel *money2;
@property (weak, nonatomic) IBOutlet UILabel *money1;
@property (weak, nonatomic) IBOutlet UIView *smallCircle;
@property (weak, nonatomic) IBOutlet UIImageView *likeIcon;
@property (weak, nonatomic) IBOutlet UIButton *likeButton;
@property (weak, nonatomic) IBOutlet UIImageView *commentIcon;
@property (weak, nonatomic) IBOutlet UIButton *commentButton;
@property (weak, nonatomic) IBOutlet UIButton *actionSheet;
@property (weak, nonatomic) IBOutlet UIButton *seeProps;
@property (weak, nonatomic) IBOutlet UIButton *seeComments;
@property (weak, nonatomic) IBOutlet UILabel *likeNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *commentNumberLabel;

@end
