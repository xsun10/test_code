//
//  RateVenueTVC.m
//  Phenom
//
//  Created by James Chung on 7/3/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "RateVenueTVC.h"
#import "IOSRequest.h"
#import "UserPreferences.h"

@interface RateVenueTVC ()

@property (nonatomic, strong) NSIndexPath *selectedRatingIndexPath;
@property (weak, nonatomic) IBOutlet UITextView *commentTextView;
@property (nonatomic, strong) UserPreferences *preferences;
@property (nonatomic, strong) UIView *tempView;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
@property (nonatomic, strong) UIBarButtonItem *doneButton;

@end

@implementation RateVenueTVC

#define  RATING_SECTION 0

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

#define ROW_EXCELLENT 0
#define ROW_VERYGOOD 1
#define ROW_AVERAGE 2
#define ROW_BELOWAVERAGE 3
#define ROW_WEAK 4

- (void)showTempSpinner
{
    self.tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    self.tempView.backgroundColor = [UIColor lightGrayColor];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] init];
    [self.activityIndicator setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width/2.0, [UIScreen mainScreen].bounds.size.height/2.0 )];
    [self.activityIndicator startAnimating];
    [self.tempView addSubview:self.activityIndicator];
    
    self.tableView.scrollEnabled = NO;
    [self.view addSubview:self.tempView];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self showTempSpinner];
    //[self setDoneButton];
    
    [IOSRequest fetchRatingForVenue:self.venueID
                             byUser:[self.preferences getUserID]
                       onCompletion:^(NSDictionary *results) {
                           
                           dispatch_async(dispatch_get_main_queue(), ^{
                              
                               if (![results[@"comment"] isKindOfClass:[NSNull class]] && [results[@"comment"] length] > 0) {
                                   self.commentTextView.text = results[@"comment"];
                               }
                               
                               if (![results[@"rating"] isKindOfClass:[NSNull class]]) {
                                   NSInteger selectedRow = -1;
                                   
                                   switch([results[@"rating"] integerValue]) {
                                       case 1:
                                           selectedRow = ROW_WEAK;
                                           break;
                                       case 2:
                                           selectedRow = ROW_BELOWAVERAGE;
                                           break;
                                       case 3:
                                           selectedRow = ROW_AVERAGE;
                                           break;
                                       case 4:
                                           selectedRow = ROW_VERYGOOD;
                                           break;
                                       case 5:
                                           selectedRow = ROW_EXCELLENT;
                                           break;
                                       default:
                                           selectedRow = -1;
                                           break;
                                           
                                   }
                                
                                   if (selectedRow != -1) {
                                       UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:selectedRow inSection:0]];
                                       cell.accessoryType = UITableViewCellAccessoryCheckmark;
                                       self.selectedRatingIndexPath = [NSIndexPath indexPathForRow:selectedRow inSection:0];
                                      // [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:selectedRow inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];

                                   }
                               }
                               [self.activityIndicator stopAnimating];
                               self.tempView.hidden = YES;
                               self.tableView.scrollEnabled = YES;

                               
                           });
                           
                       }];

}

/*- (void)setDoneButton
{
    UIButton *btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 50, 21);
    btn.layer.borderColor = [UIColor whiteColor].CGColor;
    btn.layer.borderWidth = 1.0;
    
    [btn addTarget:self action:@selector(doneAction:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundColor:[UIColor clearColor]];
    [btn setTitle:@"DONE" forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-MediumItalic" size:12.0]];
    
    // Initialize UIBarbuttonitem...
    self.doneButton = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = self.doneButton;
}*/

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == RATING_SECTION) {
        UITableViewCell *oldCell = [self.tableView cellForRowAtIndexPath:self.selectedRatingIndexPath];
        oldCell.accessoryType = UITableViewCellAccessoryNone;
        
        UITableViewCell *newCell = [self.tableView cellForRowAtIndexPath:indexPath];
        newCell.accessoryType = UITableViewCellAccessoryCheckmark;
        
        self.selectedRatingIndexPath = indexPath;
        [self.commentTextView resignFirstResponder];
  
    }
}
// submit rating
- (IBAction)doneAction:(id)sender
{
    if ([self.commentTextView.text length] > 200) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Comment Posting Error";
        alert.message = @"Sorry but your comment cannot exceed 250 characters in length.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
        
    } else {
        
    NSInteger rating = 0;
    
    for (int i = 0; i < 5; i++ ) {
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
        if (cell.accessoryType == UITableViewCellAccessoryCheckmark) {
            
            switch (i) {
                case 0:
                    rating = 5;
                    break;
                case 1:
                    rating = 4;
                    break;
                case 2:
                    rating = 3;
                    break;
                case 3:
                    rating = 2;
                    break;
                case 4:
                    rating = 1;
                    break;
                default:
                    rating = 0;
                    break;
            }
            break;
        }
    }
        
        
        if (rating == 0) {
            UIAlertView *alert = [[UIAlertView alloc] init];
            alert.title = @"Rating Posting Error";
            alert.message = @"Please enter a rating before submitting";
            [alert addButtonWithTitle:@"OK"];
            [alert show];
        } else {
            HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            HUD.labelText = @"Saving";
            self.doneButton.enabled = NO;
        
            [IOSRequest submitVenueRating:rating
                            withComment:self.commentTextView.text
                           byUser:[self.preferences getUserID]
                         forVenue:self.venueID
                     onCompletion:^(NSDictionary *results) {
                         dispatch_async(dispatch_get_main_queue(), ^ {
                             [MBProgressHUD hideHUDForView:self.view animated:YES];
                             self.doneButton.enabled = YES;

                             if ([results[@"outcome"] isEqualToString:@"failure"]) {
                             
                                 UIAlertView *alert = [[UIAlertView alloc] init];
                                 alert.title = @"Submit Error";
                                 alert.message = @"There was an error completing your request.  Please retry.";
                                 [alert addButtonWithTitle:@"OK"];
                                 [alert show];
                             } else {
                                 [self.preferences setVenuesListPageRefreshState:YES];
                                 [self.navigationController popViewControllerAnimated:YES];
                             }
                         });

                     }];
        
            double delayInSeconds = 10.0;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                //code to be executed on the main queue after delay
                self.doneButton.enabled = YES;
                [MBProgressHUD hideHUDForView:self.view animated:YES];
            });
        }
    }
    
}

@end
