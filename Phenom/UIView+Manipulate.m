//
//  UIView+Manipulate.m
//  Vaiden
//
//  Created by James Chung on 8/27/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "UIView+Manipulate.h"
#import <QuartzCore/QuartzCore.h>
#import "UIColor+VaidenColors.h"

@implementation UIView (Manipulate)


- (void)makeRoundedBorderWithColor:(UIColor *)myColor
{
    [[self layer] setBorderWidth:1.0f];
    [[self layer] setBorderColor:myColor.CGColor];
    self.layer.cornerRadius = 5; // this value vary as per your desire
    self.clipsToBounds = YES;
    
}

- (void)makeRoundedBorderwithColorAndDropshadow:(UIColor *)myColor
{
  //  [[self layer] setBorderWidth:2.0f];
//        [[self layer] setBorderColor:myColor.CGColor];
    [[self layer] setBorderColor:[UIColor midLightGray].CGColor];
        self.layer.cornerRadius = 5; // this value vary as per your desire
        self.clipsToBounds = YES;
    self.backgroundColor = [UIColor lightLightGray];
        /*
        UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:self.bounds];
        self.layer.masksToBounds = NO;
        self.layer.shadowColor = [UIColor blackColor].CGColor;
        self.layer.shadowOffset = CGSizeMake(0.0f, 2.0f);
        self.layer.shadowOpacity = 0.2f;
        self.layer.shadowPath = shadowPath.CGPath;*/
        
}

- (void)makeEdgyDropshadow:(UIColor *)myColor
{
      [[self layer] setBorderWidth:1.2f];
    [[self layer] setBorderColor:[UIColor midLightGray].CGColor];
   // self.layer.cornerRadius = 5; // this value vary as per your desire
    self.clipsToBounds = YES;
    self.backgroundColor = myColor;
       
}

- (void)makeCircleWithColor:(UIColor *)myColor andRadius:(float)myRadius
{
    [[self layer] setBorderWidth:1.0f];
    [[self layer] setBorderColor:myColor.CGColor];
    self.layer.cornerRadius = myRadius; // this value vary as per your desire
    self.clipsToBounds = YES;
}

- (void)makeCircleWithColorAndBackground:(UIColor *)myColor andRadius:(float)myRadius
{
    [[self layer] setBorderWidth:1.0f];
    [[self layer] setBorderColor:myColor.CGColor];
    [[self layer] setBackgroundColor:myColor.CGColor];
    self.layer.cornerRadius = myRadius; // this value vary as per your desire
    self.clipsToBounds = YES;
}

- (void)makeRoundedBorderWithRadius:(float)myRadius
{
    [[self layer] setBorderWidth:1.0f];
    [[self layer] setBorderColor:[UIColor clearColor].CGColor];
    self.layer.cornerRadius = myRadius; // this value vary as per your desire
    self.clipsToBounds = YES;
}

@end
