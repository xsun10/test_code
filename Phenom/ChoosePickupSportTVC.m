//
//  ChoosePickupSportTVC.m
//  Phenom
//
//  Created by James Chung on 7/12/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "ChoosePickupSportTVC.h"
#import "UserPreferences.h"
#import "IOSRequest.h"
#import "ChoosePickupSportCell.h"
#import "UIColor+VaidenColors.h"

@interface ChoosePickupSportTVC ()

@property (nonatomic, strong) NSMutableArray *sports;
@property (nonatomic, strong) UserPreferences *preferences;
@property (nonatomic, strong) UIView *tempView;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;

@end

@implementation ChoosePickupSportTVC

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (NSMutableArray *)sports
{
    if (!_sports) _sports = [[NSMutableArray alloc] init];
    return _sports;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self showTempSpinner];
    [self loadSports];
    
}

- (void)showTempSpinner
{
    self.tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    self.tempView.backgroundColor = [UIColor lightGrayColor];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] init];
    [self.activityIndicator setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width/2.0, [UIScreen mainScreen].bounds.size.height/2.0 - 50)];
    [self.activityIndicator startAnimating];
    [self.tempView addSubview:self.activityIndicator];
    
    [self.view addSubview:self.tempView];
    
    self.tableView.scrollEnabled = NO;
    
}

- (void)loadSports
{
    [IOSRequest fetchPickupSportsList:[self.preferences getUserID]
                         onCompletion:^(NSMutableArray *results) {
                                 for (id object in results) {
                                     Sport *sport = [[Sport alloc] initWithSportDetails:[object[@"sport_id"] integerValue]
                                                                                andName:object[@"sport_name"]
                                                                                andType:@"pickup"];
                                     [self.sports addObject:sport];
                                     
                                 }
                                 
                                 dispatch_async(dispatch_get_main_queue(), ^ {
                                     [self.tableView reloadData];
                                     [self.activityIndicator stopAnimating];
                                     self.tempView.hidden = YES;
                                     self.tableView.scrollEnabled = YES;
                                     
                                 });
                             }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.sports count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Sport Cell";
    ChoosePickupSportCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    Sport *sport = [self.sports objectAtIndex:indexPath.row];
    
//    cell.textLabel.text = sport.sportName;
//    cell.imageView.image = [sport getSportIconDark];
    
    cell.sportName.text = [sport.sportName capitalizedString];
    cell.sportIcon.image = [[sport getSportIcon] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [cell.sportIcon setTintColor:[UIColor newBlueDark]];
    
    // Configure the cell...
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Sport *sport = [self.sports objectAtIndex:indexPath.row];
    
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
    
    [self.delegate setWithSportFromVC:self withSport:sport];
    [self.navigationController popViewControllerAnimated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60.0;
}
@end
