//
//  PDTextPostCell.h
//  Vaiden
//
//  Created by James Chung on 2/23/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDTextPostCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *profilePic;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateTimeLabel;

@property (weak, nonatomic) IBOutlet UILabel *userTextPostLabel;
@property (weak, nonatomic) IBOutlet UILabel *remixDescriptionLabel;
@property (weak, nonatomic) IBOutlet UIView *shareDot;
@property (weak, nonatomic) IBOutlet UILabel *sharedByLabel;
@property (weak, nonatomic) IBOutlet UILabel *commentNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *commentTextLabel;
@property (weak, nonatomic) IBOutlet UIButton *commentButton;
@property (weak, nonatomic) IBOutlet UILabel *shareNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *shareTextLabel;
@property (weak, nonatomic) IBOutlet UIButton *shareButton;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UIView *mainBackgroundViewArea;
@property (weak, nonatomic) IBOutlet UIImageView *commentButtonIcon;
@property (weak, nonatomic) IBOutlet UIView *commentButtonBackgroundView;
@property (weak, nonatomic) IBOutlet UIView *shareButtonBackgroundView;
@property (weak, nonatomic) IBOutlet UIImageView *shareButtonIcon;
@property (weak, nonatomic) IBOutlet UIButton *actionButtonHandle;
@property (weak, nonatomic) IBOutlet UIView *likeButtonBackground;
@property (weak, nonatomic) IBOutlet UIImageView *likeIcon;
@property (weak, nonatomic) IBOutlet UILabel *likeNumberLabel;
@property (weak, nonatomic) IBOutlet UIButton *likeButton;

@end
