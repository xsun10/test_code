//
//  StatTag.h
//  Vaiden
//
//  Created by James Chung on 5/27/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StatTag : NSObject

@property (nonatomic) NSInteger statTagID;
@property (nonatomic) NSString *statTagName;
@property (nonatomic, strong) NSString *statTagValue;
@property (nonatomic, strong) NSString *statLink; // ex field goals (this will link "made" and "attempted")
@property (nonatomic, strong) NSString *statLinkType; // can be either "numerator", "denominator", or blank.
                                                      // Using statLink and statLinkType we can compute things like field goals made / field goals made

@property (nonatomic, strong) NSString *subCategory;  // ex offense or defense
@property (nonatomic) BOOL showSubCategory;

@property (nonatomic) BOOL isSet;

- (StatTag *)initWithStatTag:(NSInteger)statTagID
              andStatTagName:(NSString *)statTagName
             andStatTagValue:(NSString *)statTagValue
                 andStatLink:(NSString *)statLink
             andStatLinkType:(NSString *)statLinkType
              andSubCategory:(NSString *)subCategory
          andShowSubcategory:(BOOL)showSubCategory;

@end
