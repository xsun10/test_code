//
//  RatePlayerSportActionTVC.m
//  Vaiden
//
//  Created by James Chung on 10/7/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "RatePlayerSportActionTVC.h"
#import "IOSRequest.h"
#import "UserPreferences.h"
#import "RatePlayerSportActionCell.h"

@interface RatePlayerSportActionTVC ()
@property (weak, nonatomic) IBOutlet UITextView *commentTextView;
@property (nonatomic, strong) UserPreferences *preferences;
@property (weak, nonatomic) IBOutlet UILabel *typeCommentLabel;
@property (weak, nonatomic) IBOutlet RatePlayerSportActionCell *cellExcellent;
@property (weak, nonatomic) IBOutlet RatePlayerSportActionCell *cellAboveAverage;
@property (weak, nonatomic) IBOutlet RatePlayerSportActionCell *cellAverage;
@property (weak, nonatomic) IBOutlet RatePlayerSportActionCell *cellBelowAverage;
@property (weak, nonatomic) IBOutlet RatePlayerSportActionCell *cellWeak;
@property (weak, nonatomic) IBOutlet RatePlayerSportActionCell *cellNoRating;
@property (nonatomic, strong) UIBarButtonItem *doneButton;

@property (nonatomic) NSUInteger selectedRow;

@end

@implementation RatePlayerSportActionTVC

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];

    
//    self.selectedRow = 5; // the no rating value
    
    self.commentTextView.text = self.sRating.comment;
    [ self setClearsSelectionOnViewWillAppear:NO ];
    if ([self.sRating.comment length] > 0)
        self.typeCommentLabel.hidden = YES;
    [self setRatingForRow];
    //[self setDoneButton];
    self.navigationItem.title = [self.srTypeName capitalizedString];

 
}

/*- (void)setDoneButton
{
    UIButton *btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 50, 21);
    btn.layer.borderColor = [UIColor whiteColor].CGColor;
    btn.layer.borderWidth = 1.0;
    
    [btn addTarget:self action:@selector(doneAction:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundColor:[UIColor clearColor]];
    [btn setTitle:@"DONE" forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-MediumItalic" size:12.0]];
    
    // Initialize UIBarbuttonitem...
    self.doneButton = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = self.doneButton;
}*/

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedRow = indexPath.row;
}

- (IBAction)onTapTextView:(id)sender
{
    self.typeCommentLabel.hidden = YES;
    [self.commentTextView becomeFirstResponder];
    [self.tableView setContentOffset:CGPointMake(0, [UIScreen mainScreen].bounds.size.height-350) animated:YES];

}
- (IBAction)onTapRestOfView:(id)sender
{
    [self.commentTextView resignFirstResponder];
}

- (IBAction)doneAction:(id)sender
{
    // Need to save to server
    
    if ([self.commentTextView.text length] > 250) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Comment Posting Error";
        alert.message = @"Sorry but your comment cannot exceed 250 characters in length.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
    } else {
    NSUInteger rating = [self getRatingForRow:self.selectedRow];
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.labelText = @"Submitting";
    self.doneButton.enabled = NO;
    [IOSRequest saveSportRatingForSkillAttribute:self.srTypeID
                                 withSessionUser:[self.preferences getUserID]
                                   andTargetUser:self.sRating.ratedPlayerID
                                      withRating:rating
                                      andComment:self.commentTextView.text
                                    onCompletion:^(NSDictionary *results) {
                                        
                                        
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            [self.preferences setSportRatingsSummaryPageRefreshState:YES];
                                            
                                            [self.delegate setRatePlayerSportActionViewController:self
                                                                                  withSportRating:rating
                                                                                       andComment:self.commentTextView.text];
                                            [self.navigationController popViewControllerAnimated:YES];
                                            [MBProgressHUD hideHUDForView:self.view animated:YES];
                                            self.doneButton.enabled = YES;
                                            
                                            
                                            
                                        });
                                    }];
        double delayInSeconds = 10.0;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            //code to be executed on the main queue after delay
            self.doneButton.enabled = YES;
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        });
    }
    
}

- (NSUInteger)getRatingForRow:(NSUInteger)selectedRow
{
    switch (self.selectedRow) {
        case 0:
            return 5;
            break;
        case 1:
            return 4;
            break;
        case 2:
            return 3;
            break;
        case 3:
            return 2;
            break;
        case 4:
            return 1;
            break;
        case 5:
            return 0;
            break;
    }
    return 0;
}

- (void)setRatingForRow
{
    
    switch ([[NSNumber numberWithFloat:self.sRating.rating] integerValue]) {
        case 0:
    //        self.cellNoRating.selected = YES;
    //        [self.cellNoRating setSelected:YES animated:YES];
            self.selectedRow = 5;
            break;
        case 1:
    //        self.cellWeak.selected = YES;
    //        [self.cellWeak setSelected:YES animated:YES];
            self.selectedRow = 4;
            break;
        case 2:
    //        self.cellBelowAverage.selected = YES;
    //        [self.cellBelowAverage setSelected:YES animated:YES];
            self.selectedRow = 3;
            break;
        case 3:
    //        self.cellAverage.selected = YES;
    //        [self.cellAverage setSelected:YES animated:YES];
            self.selectedRow = 2;
            break;
        case 4:
    //        self.cellAboveAverage.selected = YES;
    //        [self.cellAboveAverage setSelected:YES animated:YES];
            self.selectedRow = 1;
            break;
        case 5:
    //        self.cellExcellent.selected = YES;
    //        [self.cellExcellent setSelected:YES animated:YES];
            self.selectedRow = 0;
            break;
        default:
            self.selectedRow = 5;
            break;
    }
    [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:self.selectedRow inSection:0] animated:NO scrollPosition:UITableViewScrollPositionTop];

    
}
@end
