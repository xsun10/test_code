//
//  ShowLikesTVC.m
//  Vaiden
//
//  Created by James Chung on 5/13/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "ShowLikesTVC.h"
#import "ShowLikesCell.h"
#import "IOSRequest.h"
#import "S3Tools.h"
#import "AFNetworking.h"
#import "PlayerDetailTVC.h"
#import "UIColor+VaidenColors.h"

@interface ShowLikesTVC ()

@property (nonatomic, strong) NSMutableArray *postLikes;
@property (nonatomic) NSInteger indexOfUser;
@property (nonatomic, strong) UIView *overlay;
@property (nonatomic, strong) UIView *tempView;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
@end

@implementation ShowLikesTVC

- (NSMutableArray *)postLikes
{
    if (!_postLikes) _postLikes = [[NSMutableArray alloc] init];
    return _postLikes;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
    [self showTempSpinner];

    [self loadLikes];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.postLikes count];
}

- (void)loadLikes
{
    [IOSRequest getPostLikes:self.newsfeedID
                onCompletion:^(NSMutableArray *results) {
                    
                    for (id object in results) {
                        Player *p = [[Player alloc] initWithPlayerDetails:[object[@"user_id"] integerValue]
                                                              andUserName:object[@"username"]
                                                      andProfilePicString:object[@"profile_pic_string"]];
                        [self.postLikes addObject:p];
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                       
                        [self.activityIndicator stopAnimating];
                        self.tempView.hidden = YES;

                        if ([self.postLikes count] == 0)
                            [self displayErrorOverlay];
                        else
                            [self.tableView reloadData];
                    });
                    
                }];
}

- (void)showTempSpinner
{
    self.tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    self.tempView.backgroundColor = [UIColor lightGrayColor];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] init];
    [self.activityIndicator setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width/2.0, [UIScreen mainScreen].bounds.size.height/2.0 )];
    [self.activityIndicator startAnimating];
    [self.tempView addSubview:self.activityIndicator];
    
    [self.view addSubview:self.tempView];
    
}


- (void)displayErrorOverlay
{
    self.overlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, [UIScreen mainScreen].bounds.size.height)];
    self.overlay.backgroundColor = [UIColor lightLightGray];
    
    UIImage *img = [UIImage imageNamed:@"no_prop"];
    UIImageView *icon = [[UIImageView alloc] initWithImage:img];
    [icon setFrame:CGRectMake(140, 100, 50, 50)];
    [icon setContentMode:UIViewContentModeCenter];
    
    UILabel *firstLine = [[UILabel alloc] initWithFrame:CGRectMake(30, 180, 260, 21)];
    firstLine.textColor = [UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:1.0];
    firstLine.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:15];
    firstLine.text = @"No Props Yet";
    [firstLine setTextAlignment:NSTextAlignmentCenter];
    
    UILabel *secondLine = [[UILabel alloc] initWithFrame:CGRectMake(25, 200, 280, 21)];
    secondLine.textColor = [UIColor colorWithRed:170/255.0 green:170/255.0 blue:170/255.0 alpha:1.0];
    secondLine.font = [UIFont fontWithName:@"HelveticaNeue" size:13];
    secondLine.text = @"This post has not received any props yet";
    secondLine.numberOfLines = 0;
    [secondLine setTextAlignment:NSTextAlignmentCenter];
    
    [self.overlay addSubview:icon];
    [self.overlay addSubview:firstLine];
    [self.overlay addSubview:secondLine];
    
    [self.tableView addSubview:self.overlay];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ShowLikesCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Like Cell" forIndexPath:indexPath];
    
    Player *p = [self.postLikes objectAtIndex:indexPath.row];
    
    cell.usernameLabel.text = [NSString stringWithFormat:@"@%@", p.userName];
    
    if ([p.fullName length] > 0)
        cell.fullnameLabel.text = p.fullName;
    else
        cell.fullnameLabel.text = p.userName;
    
    NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                            baseString:p.profileBaseString];
    
    [cell.profilePic setImageWithURL:[NSURL URLWithString:url]
                    placeholderImage:[UIImage imageNamed:@"avatar square"]];

    
    return cell;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Player Detail Segue"]) {
        PlayerDetailTVC *controller = segue.destinationViewController;
        Player *p = [self.postLikes objectAtIndex:self.indexOfUser];
        controller.playerUserID = p.userID;
        controller.profilePicString = p.profileBaseString;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.indexOfUser = indexPath.row;
    [self performSegueWithIdentifier:@"Player Detail Segue" sender:self];
}
@end
