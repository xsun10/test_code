//
//  ShirtLandingVC.m
//  Vaiden
//
//  Created by James Chung on 1/31/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "ShirtLandingVC.h"
/*#import "UIButton+RoundBorder.h"
#import "IOSRequest.h"
#import "UIColor+VaidenColors.h"
#import "PlayerDetailTVC.h"
#import "NSDate+Utilities.h"
#import "UIView+Manipulate.h"
#import "ShopVC.h"*/

@interface ShirtLandingVC ()
@property (weak, nonatomic) IBOutlet UIScrollView *pageScrollView;

/*@property (weak, nonatomic) IBOutlet UILabel *featuredPlayerName;
@property (weak, nonatomic) IBOutlet UILabel *featuredPlayerLevel;
@property (nonatomic) NSInteger featuredUserID;
@property (weak, nonatomic) IBOutlet UIButton *learnMoreButton;
@property (nonatomic, strong) NSString *featuredPlayerProfilePicString;
@property (weak, nonatomic) IBOutlet UIButton *featuredPlayerButton;
@property (nonatomic, strong) UIView *tempView;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIButton *buyItNowButton;
@property (weak, nonatomic) IBOutlet UIButton *visitButton;
@property (weak, nonatomic) IBOutlet UILabel *raisedLabel;
@property (weak, nonatomic) IBOutlet UILabel *daysLabel;
@property (weak, nonatomic) IBOutlet UILabel *lastUpdatedLabel;
@property (weak, nonatomic) IBOutlet UIView *raisedBackgroundView;
@property (weak, nonatomic) IBOutlet UIView *daysBackgroundView;
@property (nonatomic, strong) NSString *url;*/

@end

@implementation ShirtLandingVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Set the round corner for views
    /*[self.visitButton makeRoundedBorderWithRadius:3];
    [self.raisedBackgroundView makeRoundedBorderWithRadius:3];
    [self.daysBackgroundView makeRoundedBorderWithRadius:3];*/
    
//    [self.learnMoreButton makeRoundedBorderWithRadius:3];
    
    //self.navigationController.navigationBar.tintColor = [UIColor whiteColor];

	// Do any additional setup after loading the view.
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
    
  //  [self.buyItNowButton makeRoundBorderWithColor:[UIColor whiteColor]];
  //  self.buyItNowButton.backgroundColor = [UIColor clearColor];
  //  [self.buyItNowButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    //[self showTempSpinner];
    
    /*[IOSRequest getKickstarterInfo:0
                      onCompletion:^(NSDictionary *results) {
                          dispatch_async(dispatch_get_main_queue(), ^{
                              [self.activityIndicator stopAnimating];
                              self.tempView.hidden = YES;
                              
                              NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
                              [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                              NSDate *date = [[formatter dateFromString:results[@"ts"]] toLocalTime];
                              
                              self.raisedLabel.text = results[@"amount_funded"];
                              self.daysLabel.text = results[@"num_days"];
                              self.url = results[@"url"];
                              self.lastUpdatedLabel.text = [NSString stringWithFormat:@"Last Updated: %@", [formatter stringFromDate: date]];
                             
                              
                          });
                          
                      }];*/

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    
    /*
 self.featuredPlayerLevel.text = @"";
    self.featuredPlayerName.text = @"";
    
    self.featuredPlayerButton.enabled = NO;
    [IOSRequest fetchFeaturedPlayerForSport:1 // basketball
                               onCompletion:^(NSDictionary *results) {
                                   
                                   dispatch_async(dispatch_get_main_queue(), ^{
                                       [self.activityIndicator stopAnimating];
                                       self.tempView.hidden = YES;
                                    //   self.featuredPlayerName.text = results[@"username"];
                                       self.featuredPlayerName.text = @"Darryl Nable";
                                       self.featuredPlayerLevel.text = [NSString stringWithFormat:@"Level %ld", [results[@"level"] integerValue]];
                                       self.featuredUserID = [results[@"user_id"] integerValue];
                                       self.featuredPlayerProfilePicString = results[@"profile_pic_string"];
                                       self.featuredPlayerButton.enabled = YES;
                                   });
                                   
                               }];*/
}
/*- (IBAction)visitKickstarter:(id)sender
{
    [self performSegueWithIdentifier:@"Kickstarter Segue" sender:self];
}*/

/*- (void)showTempSpinner
{
    [self.tempView removeFromSuperview];
    self.tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    self.tempView.backgroundColor = [UIColor lightGrayColor];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] init];
    [self.activityIndicator setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width/2.0, [UIScreen mainScreen].bounds.size.height/2.0 )];
    [self.activityIndicator startAnimating];
    [self.tempView addSubview:self.activityIndicator];
    
    [self.view addSubview:self.tempView];
    
}*/

/*- (IBAction)goToFeaturedPlayer:(id)sender
{
  //  [self performSegueWithIdentifier:@"Featured Player Segue" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Featured Player Segue"]) {
        PlayerDetailTVC *controller = segue.destinationViewController;
        controller.playerUserID = self.featuredUserID;
        controller.profilePicString = self.featuredPlayerProfilePicString;
    } else if ([segue.identifier isEqualToString:@"Kickstarter Segue"]) {
        ShopVC *controller = segue.destinationViewController;
        controller.url = self.url;
    }
}*/

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    CGRect screenSize = [[UIScreen mainScreen] bounds];
    self.pageScrollView.contentSize = CGSizeMake(screenSize.size.width, screenSize.size.height); // change the content to fit the screen
}

@end
