//
//  SetNewPasswordTVC.m
//  Vaiden
//
//  Created by James Chung on 4/3/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "SetNewPasswordTVC.h"
#import "UITextField+FormValidations.h"
#import "IOSRequest.h"
#import "UserPreferences.h"


@interface SetNewPasswordTVC ()
@property (weak, nonatomic) IBOutlet UITableViewCell *oldPassTVCell;
@property (weak, nonatomic) IBOutlet UITextField *oldPassTF;
@property (weak, nonatomic) IBOutlet UITextField *retypePassTF;
@property (weak, nonatomic) IBOutlet UITextField *theNewPassTF;
@property (strong, nonatomic)  UIBarButtonItem *doneButton;
@property (nonatomic, strong) UIView *tempView;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;

@property (nonatomic, strong) UserPreferences *preferences;


@end

@implementation SetNewPasswordTVC

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //[self setDoneButton];
    [self showTempSpinner];
    
    [IOSRequest checkPasswordExistsForUser:[self.preferences getUserID] onCompletion:^(NSDictionary *results) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.activityIndicator stopAnimating];
            self.tempView.hidden = YES;
            
            if ([results[@"outcome"] isEqualToString:@"absent"]) {
            
           
                // A password has not been set yet
                UILabel *oldPassFrame = [[UILabel alloc] initWithFrame:CGRectMake(self.oldPassTF.frame.origin.x,
                                                                                  self.oldPassTF.frame.origin.y,
                                                                                  self.oldPassTF.frame.size.width,
                                                                                  self.oldPassTF.frame.size.height)];
                oldPassFrame.text = @"No password has been set yet";
                oldPassFrame.font = [UIFont fontWithName:@"HelveticaNeue-Italic" size:12];
                [self.oldPassTVCell.contentView addSubview:oldPassFrame];
                
                self.oldPassTF.hidden = YES;

            }

        });
    }];
    double delayInSeconds = 10.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self.activityIndicator stopAnimating];
        self.tempView.hidden = YES;
    });
}

- (void)showTempSpinner
{
    self.tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    self.tempView.backgroundColor = [UIColor lightGrayColor];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] init];
    [self.activityIndicator setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width/2.0, [UIScreen mainScreen].bounds.size.height/2.0 )];
    [self.activityIndicator startAnimating];
    [self.tempView addSubview:self.activityIndicator];
    
    [self.view addSubview:self.tempView];
    
}


/*- (void)setDoneButton
{
    UIButton *btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 50, 21);
    btn.layer.borderColor = [UIColor whiteColor].CGColor;
    btn.layer.borderWidth = 1.0;
    
    [btn addTarget:self action:@selector(SetMyNewPassword:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundColor:[UIColor clearColor]];
    [btn setTitle:@"Done" forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-MediumItalic" size:12.0]];
    
    // Initialize UIBarbuttonitem...
    self.doneButton = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = self.doneButton;
}*/

- (IBAction)SetMyNewPassword:(id)sender
{
   
    
    if (![self.theNewPassTF.text isEqualToString:self.retypePassTF.text]) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Password Error";
        alert.message = @"Your new password field does not match the retype field.  Please retry.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
    } else if (![self.theNewPassTF checkPassword]) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Password Error";
        alert.message = @"Please enter a password between 6 and 20 letters with alphanumeric or special characters.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
    } else {
        HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        HUD.labelText = @"Updating";
        self.doneButton.enabled = NO;
        
        [IOSRequest setNewPasswordWithOldPassword:self.oldPassTF.text
                               andNewPassword:self.theNewPassTF.text
                                      forUser:[self.preferences getUserID]
                                 onCompletion:^(NSDictionary *results) {
                                     
                                     dispatch_async(dispatch_get_main_queue(), ^{
                                         
                                         self.doneButton.enabled = YES;
                                          [MBProgressHUD hideHUDForView:self.view animated:YES];
                                         
                                         UIAlertView *alert = [[UIAlertView alloc] init];
                                         
                                         if ([results[@"outcome"] isEqualToString:@"success"]) {
                                             
                                             [self.navigationController popViewControllerAnimated:YES];
                                         
                                         } else if ([results[@"outcome"] isEqualToString:@"failure"]) {
                                             alert.title = @"Password Set Error";
                                             alert.message = @"There was an error completing your request.  Please try again later.";
                                             [alert addButtonWithTitle:@"OK"];
                                             [alert show];
                                         } else if ([results[@"outcome"] isEqualToString:@"not_found"]) {
                                             alert.title = @"Original Password Incorrect";
                                             alert.message = @"The old password you entered is incorrect.  Please try again.";
                                             [alert addButtonWithTitle:@"OK"];
                                             [alert show];
                                         }
                                     });
                                 }];
        double delayInSeconds = 10.0;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            //code to be executed on the main queue after delay
            self.doneButton.enabled = YES;
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        });

    }
    
    
}
- (IBAction)tapOffC:(id)sender
{
    [self.oldPassTF resignFirstResponder];
    [self.theNewPassTF resignFirstResponder];
    [self.retypePassTF resignFirstResponder];
    
}

@end
