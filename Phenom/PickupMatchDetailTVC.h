//
//  PickupMatchDetailTVC.h
//  Phenom
//
//  Created by James Chung on 4/23/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PickupMatch.h"
#import "MapViewTVC.h"
#import "MBProgressHUD.h"
#import "PMDMatchCompetitorsCell.h"
#import "PMDHeaderInfoCell.h"


@interface PickupMatchDetailTVC : MapViewTVC <MBProgressHUDDelegate, PMDHeaderInfoCell_Delegate>
{
    MBProgressHUD *HUD;
}

//@property (weak, nonatomic) IBOutlet MKMapView *mapview;
@property (nonatomic) NSUInteger matchID;

- (void)reload;

@end
