//
//  WhoCanChallengeUserTVC.m
//  Vaiden
//
//  Created by James Chung on 12/1/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "WhoCanChallengeUserTVC.h"
#import "UserPreferences.h"
#import "IOSRequest.h"

@interface WhoCanChallengeUserTVC ()

@property (nonatomic, strong) UserPreferences *preferences;
@property (nonatomic, strong) NSString *whoCanChallengeUser;
@property (nonatomic, strong) UIView *tempView;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;

@end

@implementation WhoCanChallengeUserTVC

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self showTempSpinner];

    [IOSRequest whoCanChallengeUser:[self.preferences getUserID]
                       onCompletion:^(NSDictionary *results) {
                           dispatch_async(dispatch_get_main_queue(), ^{
                               [self.activityIndicator stopAnimating];
                               self.tempView.hidden = YES;
                               
                               if ([results[@"who_can_challenge"] isEqualToString:@"following"]){
                                   UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
                                   cell.accessoryType = UITableViewCellAccessoryCheckmark;
                               } else {
                                   UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
                                   cell.accessoryType = UITableViewCellAccessoryCheckmark;
                               }
                           });
                           
                       }];

}

- (void)showTempSpinner
{
    self.tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    self.tempView.backgroundColor = [UIColor lightGrayColor];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] init];
    [self.activityIndicator setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width/2.0, [UIScreen mainScreen].bounds.size.height/2.0 - 50)];
    [self.activityIndicator startAnimating];
    [self.tempView addSubview:self.activityIndicator];
    
    [self.view addSubview:self.tempView];
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.labelText = @"Updating";
    
    UITableViewCell *selectedCell = [self.tableView cellForRowAtIndexPath:indexPath];
    selectedCell.accessoryType = UITableViewCellAccessoryCheckmark;
    NSString *newWhoCanChallenge = @"";
    
    if (indexPath.row == 0) {
        UITableViewCell *otherCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
        otherCell.accessoryType = UITableViewCellAccessoryNone;
        newWhoCanChallenge = @"anyone";
        
    } else if (indexPath.row == 1) {
        UITableViewCell *otherCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        otherCell.accessoryType = UITableViewCellAccessoryNone;
        newWhoCanChallenge = @"following";
    }
    
    [IOSRequest setWhoCanChallengeUser:[self.preferences getUserID]
                            playersAre:newWhoCanChallenge
                          onCompletion:^(NSDictionary *results) {
                              
                              dispatch_async(dispatch_get_main_queue(), ^{
                                  [MBProgressHUD hideHUDForView:self.view animated:YES];
                                  [self.navigationController popViewControllerAnimated:YES];
                              });
                          }];
/*    double delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        //code to be executed on the main queue after delay
        [MBProgressHUD hideHUDForView:self.view animated:YES];// just
    });*/
}
@end
