//
//  VDCheckInsCell.h
//  Vaiden
//
//  Created by James Chung on 4/24/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VDCheckInsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *locationIcon;
@property (weak, nonatomic) IBOutlet UIView *myBackgroundView;

@end
