//
//  StatTagCombination.m
//  Vaiden
//
//  Created by James Chung on 6/5/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "StatTagCombination.h"

@implementation StatTagCombination

- (StatTagCombination *)initWithStat1:(StatTag *)numerator andStat2:(StatTag *)denominator
{
    self = [super init];
    
    if (self) {
        _numerator = numerator;
        _denominator = denominator;
    }
    return self;
}

@end
