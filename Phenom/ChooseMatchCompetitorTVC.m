//
//  ChooseMatchCompetitorTVC.m
//  Phenom
//
//  Created by James Chung on 4/5/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "ChooseMatchCompetitorTVC.h"
#import "IOSRequest.h"
#import "ChooseMatchCompetitorCell.h"
#import "MatchCreationSuccessVC.h"
#import "S3Tools.h"
#import "AFNetworking.h"
#import "UserPreferences.h"
#import "Contact.h"
#import "NSDate+Utilities.h"
#import "UIImage+ProportionalFill.h"

@interface ChooseMatchCompetitorTVC ()

@property (nonatomic, strong) NSMutableArray *competitors;
@property (nonatomic, strong) NSMutableArray *nearbyList;
@property (nonatomic, strong) UserPreferences *preferences;

@property (nonatomic, strong) NSMutableArray *chosenCompetitors;
@property (nonatomic) BOOL didLoadFollowingList;
@property (nonatomic) BOOL didLoadNearbyList;

@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentPlayer;
@property (nonatomic) NSInteger proximityRange;

@property (nonatomic, strong) UIView *tempView;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
@property (nonatomic, strong) UIBarButtonItem *doneButton;

@property (weak, nonatomic) IBOutlet UILabel *tintInfo;
@end

@implementation ChooseMatchCompetitorTVC

#define  FOLLOWING_SEG 0
#define NEARBY_SEG 1

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (NSMutableArray *)competitors
{
    if (!_competitors) _competitors = [[NSMutableArray alloc] init];
    return _competitors;
}

- (NSMutableArray *)nearbyList
{
    if (!_nearbyList) _nearbyList = [[NSMutableArray alloc] init];
    return _nearbyList;
}

- (NSMutableArray *)chosenCompetitors
{
    if (!_chosenCompetitors) _chosenCompetitors = [[NSMutableArray alloc] init];
    return _chosenCompetitors;
    
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self fetchMatchCompetitors];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
    
    self.didLoadFollowingList = NO;
    self.didLoadNearbyList = NO;
    [self showTempSpinner];
    self.tableView.scrollEnabled = NO;
    //[self setDoneButton];
}

/*- (void)setDoneButton
{
    UIButton *btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 50, 21);
    btn.layer.borderColor = [UIColor whiteColor].CGColor;
    btn.layer.borderWidth = 1.0;
    
    [btn addTarget:self action:@selector(done:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundColor:[UIColor clearColor]];
    [btn setTitle:@"DONE" forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-MediumItalic" size:12.0]];
    
    // Initialize UIBarbuttonitem...
    self.doneButton = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = self.doneButton;
}*/

- (void)showTempSpinner
{
    self.tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    self.tempView.backgroundColor = [UIColor lightGrayColor];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] init];
    [self.activityIndicator setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width/2.0, [UIScreen mainScreen].bounds.size.height/2.0 - 50)];
    [self.activityIndicator startAnimating];
    [self.tempView addSubview:self.activityIndicator];
    
    [self.view addSubview:self.tempView];
    
}

- (void)fetchMatchCompetitors
{
    [self.competitors removeAllObjects];
    [self.chosenCompetitors removeAllObjects];
    

//    [IOSRequest fetchUserFollowingListForUser:self.userID
//                                withSportName:self.pickupMatch.sport.sportName
//                                 andSportType:self.pickupMatch.sport.sportType
//                                 onCompletion:^(NSMutableArray *competitorList) {
    
//  [IOSRequest fetchUserFollowingList:self.userID
//                       sessionUserID:[self.preferences getUserID]
//                        onCompletion:^(NSMutableArray *competitorList) {
  [IOSRequest fetchUserFollowingList:[self.preferences getUserID]
                             bySport:self.pickupMatch.sport.sportID
                        onCompletion:^(NSMutableArray *competitorList) {
                            
                            
        for (id object in competitorList) {
            
            Contact *competitor = [[Contact alloc] initWithContactDetails:[object[@"user_id"] integerValue]
                                                            andProfileStr:object[@"profile_pic_string"]
                                                              andUserName:object[@"username"]
                                                              andFullName:object[@"fullname"]
                                                              andLocation:object[@"location"]
                                                                 andAbout:object[@"about"]
                                                                 andCoins:[object[@"coins"] floatValue]
                                                              contactType:CONTACT_TYPE_FOLLOWING
                                                            followingStatus:IS_FOLLOWING_STATUS];
            /*
                        Player *competitor = [[Player alloc] initWithPlayerDetails:[object[@"user_id"] integerValue]
                                                                     andProfileStr:object[@"profile_pic_string"]
                                                                       andUserName:object[@"username"]
                                                                       andFullName:object[@"fullname"]
                                                                           andLocation:object[@"locatin"]
                                                                          andAbout:object[@"about"]];*/
                        [self.competitors addObject:competitor];
 
            id sportObj = object[@"sport"];
            
            PlayerSport *pSport = [[PlayerSport alloc] initWithSportDetails:sportObj[@"sport_name"]
                                                                   andType:@""
                                                                  andLevel:sportObj[@"level"]
                                                            andSportRecord:sportObj[@"win_loss_record"]];
            [pSport setSportID:self.pickupMatch.sport.sportID];
            
            [competitor addPlayerSport:pSport];
            
        }
                            dispatch_async(dispatch_get_main_queue(), ^{
                                self.didLoadFollowingList = YES;
                                [self.tableView reloadData];
                                [self.activityIndicator stopAnimating];
                                self.tempView.hidden = YES;
                                self.tableView.scrollEnabled = YES;
                            });
                                     
       
    }];
}

- (IBAction)done:(id)sender
{    
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.labelText = @"Creating Match";
    
    self.doneButton.enabled = NO;
    
    for (Contact *opponent in self.competitors) {
        if (opponent.tag == 1)
            [self.chosenCompetitors addObject:opponent];
    }
    
    for (Contact *opponent in self.nearbyList) {
        
        if (opponent.tag == 1 && ![self.chosenCompetitors containsObject:opponent])
            [self.chosenCompetitors addObject:opponent];
    }
    
    self.pickupMatch.competitorArray = self.chosenCompetitors;
    
    self.pickupMatch.dateTime = [self.pickupMatch.dateTime toGlobalTime]; // change to GMT before submitting to server
    
    [IOSRequest createPickupMatchWithInfo:self.pickupMatch onCompletion:^(NSDictionary *results) {
            if (![results[@"outcome"] isEqualToString:@"failure"]) {
                [self.preferences setMatchNotificationRefreshState:YES];
                [self.preferences setNewsfeedRefreshState:YES];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                
                    [self performSegueWithIdentifier: @"Pickup Match Success" sender: self];
                    self.doneButton.enabled = YES;
                });
                
                NSLog(@"Pickup Match inserted ok");
            } else {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                    self.doneButton.enabled = YES;
                    
                    UIAlertView *alert = [[UIAlertView alloc] init];
                    alert.title = @"Problem Creating Match";
                    alert.message = @"There was a problem creating your match.  Please try again later.";
                    [alert addButtonWithTitle:@"OK"];
                    [alert show];

                });
                
                NSLog(@"Pickup Match Insert Error:%@", results[@"error"]);
            }
        
    }];
    double delayInSeconds = 10.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        //code to be executed on the main queue after delay
        [MBProgressHUD hideHUDForView:self.view animated:YES];// just in case
        self.doneButton.enabled = YES;
    });
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int num = 0;
    switch (self.segmentPlayer.selectedSegmentIndex) {
        case FOLLOWING_SEG:
            num = [self.competitors count];
            if (num == 0) {
                self.tintInfo.text = @"No players in contacts play this sport.";
            } else {
                self.tintInfo.text = @"Choose players to invite to your match.";
            }
            return num;
            break;
        case NEARBY_SEG:
            num = [self.nearbyList count];
            if (num == 0) {
                self.tintInfo.text = @"No players nearby play this sport.";
            } else {
                self.tintInfo.text = @"Choose players to invite to your match.";
            }
            return num;
            break;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Opponent Cell";
    ChooseMatchCompetitorCell *cell = (ChooseMatchCompetitorCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    
    Contact *opponent = nil;
    
    switch (self.segmentPlayer.selectedSegmentIndex) {
        case FOLLOWING_SEG:
            opponent = (self.competitors)[indexPath.row];
            break;
        case NEARBY_SEG:
            opponent = (self.nearbyList)[indexPath.row];
            break;
    }
    
    PlayerSport *sport = [opponent.playerSports objectAtIndex:0]; // only one sport should be in array

    if ([opponent computeTotalNumMatchesPlayedForSport:sport.sportID] > 0)
        cell.levelLabel.text = [NSString stringWithFormat:@"%ld",  [sport.level integerValue]];
    else
        cell.levelLabel.text = @"0";
    
    cell.sportLabel.text = [self.pickupMatch.sport.sportName capitalizedString];
    
    if (opponent.tag == 1) // selected
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    else // not selected
        cell.accessoryType = UITableViewCellAccessoryNone;
    
    cell.opponentUsernameLabel.text = opponent.userName;
    
    NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                            baseString:opponent.profileBaseString];
    
    [cell.opponentImage setImageWithURL:[NSURL URLWithString:url]
                      placeholderImage:[UIImage imageNamed:@"avatar round"]];
    [UIImage makeRoundedImage:cell.opponentImage withRadius:25];
    
    return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
     ChooseMatchCompetitorCell *cell = (ChooseMatchCompetitorCell *)[tableView cellForRowAtIndexPath:indexPath];
    
    Contact *opponent = nil;
    
    switch (self.segmentPlayer.selectedSegmentIndex) {
        case FOLLOWING_SEG:
            opponent = (self.competitors)[indexPath.row];
            break;
        case NEARBY_SEG:
            opponent = (self.nearbyList)[indexPath.row];
            break;
    }
    
    
    if (cell.accessoryType == UITableViewCellAccessoryCheckmark) {
        cell.accessoryType = UITableViewCellAccessoryNone;
        opponent.tag = 0;
    } else {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        opponent.tag = 1;
    }
}


- (void)initLocation
{
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
    self.proximityRange = 10;
    
    
}

- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations {
    CLLocation *location = locations[0];
    
    [self.nearbyList removeAllObjects];
    [self.chosenCompetitors removeAllObjects];
    
    [locationManager stopUpdatingLocation];
    //    [self.playersTableView setUserInteractionEnabled:YES];
    
    
    
 //   [IOSRequest fetchProximityPlayers:[self.preferences getUserID]
 //                             andLong:location.coordinate.longitude
 //                              andLat:location.coordinate.latitude
 //                           withRange:self.proximityRange
 //                        onCompletion:^(NSMutableArray *resultArray) {
   [IOSRequest fetchProximityPlayers:[self.preferences getUserID]
                             andLong:location.coordinate.longitude
                              andLat:location.coordinate.latitude
                           withRange:self.proximityRange
                       filterBySport:self.pickupMatch.sport.sportID
                             orderBy:1
                        onCompletion:^(NSMutableArray *resultArray) {
        
        
                             [self.nearbyList removeAllObjects];
                             for (id object in resultArray) {
                                 if ([object[@"user_id"] integerValue] == [self.preferences getUserID]) continue;
                                 
                                 Contact *vContact = [[Contact alloc]
                                                      initWithContactDetails:[object[@"user_id"] integerValue]
                                                      andProfileStr:object[@"profile_pic_string"]
                                                      andUserName:object[@"username"]
                                                      andFullName:object[@"fullname"]
                                                      andLocation:object[@"location"]
                                                      andAbout:object[@"about"]
                                                      andCoins:[object[@"coins"] floatValue]
                                                      contactType:CONTACT_TYPE_NEARBY
                                                      followingStatus:[object[@"follow_status"] integerValue]];
                                 
                                 id sportObj = object[@"sports"];
                                 
                                 PlayerSport *pSport = [[PlayerSport alloc] initWithSportDetails:sportObj[@"sport_name"]
                                                                                         andType:@""
                                                                                        andLevel:sportObj[@"level"]
                                                                                  andSportRecord:sportObj[@"win_loss_record"]];
                                 [pSport setSportID:self.pickupMatch.sport.sportID];
                                 
                                 [vContact addPlayerSport:pSport];

                                 
                                 [self.nearbyList addObject:vContact];
                                 
                             }
                             
                             
                             
                             
                             dispatch_async(dispatch_get_main_queue(), ^ {
                                 self.didLoadNearbyList = YES;
                                 [self.tableView reloadData];
                                 [self.activityIndicator stopAnimating];
                                 self.tempView.hidden = YES;
                             });
                             
                             
                             
                         }];
    
    
}
- (IBAction)segmentChanged:(id)sender
{
    switch (self.segmentPlayer.selectedSegmentIndex) {
   
        case 0:
            
            if (!self.didLoadFollowingList) {
                [self showTempSpinner];
                
                [self fetchMatchCompetitors];
            } else
                [self.tableView reloadData];
            break;
            
        case 1:
            
            if (!self.didLoadNearbyList) {
                [self showTempSpinner];
                
                [self initLocation];
                [locationManager startUpdatingLocation];
            } else
                [self.tableView reloadData];
            break;
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Pickup Match Success"]) {
        MatchCreationSuccessVC *controller = segue.destinationViewController;
        controller.matchCategory = @"pickup";
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70.0;
}

@end
