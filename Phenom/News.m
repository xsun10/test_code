//
//  News.m
//  Phenom
//
//  Created by James Chung on 4/23/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "News.h"

@implementation News

- (News *)initWithNewsDetails:(NSInteger)newsMakerUserID
                  andUserName:(NSString *)newsMakerUserName
              andProfileStr:(NSString *)profileBaseString
                     withDate:(NSDate *)newsDate
                  andPostType:(NSInteger)postType
               andNumComments:(NSUInteger)numComments
                andNumRemixes:(NSUInteger)numRemixes
                    andNewsID:(NSInteger)newsID

{
    self = [super init];
    
    if (self) {
        _newsMakerUserID = newsMakerUserID;
        _newsMakerUserName = newsMakerUserName;
        _profileBaseString = profileBaseString;
        _newsDate = newsDate;
        _postType = postType;
        _numComments = numComments;
        _numRemixes = numRemixes;
        _newsID = newsID;
        _textPost = @"";
    }
    return self;
}

// initializer with text post
- (News *)initWithNewsDetails:(NSInteger)newsMakerUserID
                  andUserName:(NSString *)newsMakerUserName
                andProfileStr:(NSString *)profileBaseString
                     withDate:(NSDate *)newsDate
                  andPostType:(NSInteger)postType
               andNumComments:(NSUInteger)numComments
                andNumRemixes:(NSUInteger)numRemixes
                    andNewsID:(NSInteger)newsID
                  andTextPost:(NSString *)textPost
{
    self = [super init];
    
    if (self) {
        _newsMakerUserID = newsMakerUserID;
        _newsMakerUserName = newsMakerUserName;
        _profileBaseString = profileBaseString;
        _newsDate = newsDate;
        _postType = postType;
        _newsID = newsID;
        _numComments = numComments;
        _numRemixes = numRemixes;
        _textPost = textPost;
    }
    return self;
}

// Implemented this as a temp hack
- (News *)initWithMinDetails:(NSDate *)newsDate
{
    self = [super init];
    
    if (self) {
        _newsDate = newsDate;
        _newsMakerUserName = @"";
        _profileBaseString = @"";
        _postType = 0;
        _newsID = 0;
        _numComments = 0;
        _numRemixes = 0;
        _textPost = @"";
    }
    return  self;
}

// Returns an Error Code string if NO...and returns OK if post can be shared

- (NSString *)canPostBeSharedBySessionUser:(NSInteger)sessionUserID
{
    if (self.newsMakerUserID == sessionUserID)
        return @"Sorry, you cannot share your own posts."; // error string
    
    return @"OK";
}

- (UIImage *)getNewsIcon
{
    return nil;
}

@end
