//
//  ChooseProfileGenderTVC.h
//  Vaiden
//
//  Created by James Chung on 12/21/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomBaseTVC.h"

@class ChooseProfileGenderTVC;

@protocol ChooseProfileGenderTVC_Delegate <NSObject>

- (void)setProfileGenderWithController:(ChooseProfileGenderTVC *)controller
                      withGenderString:(NSString *)genderString;
@end

@interface ChooseProfileGenderTVC : CustomBaseTVC

@property (nonatomic, weak) id <ChooseProfileGenderTVC_Delegate> delegate;
@end
