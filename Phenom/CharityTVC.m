//
//  CharityTVC.m
//  Vaiden
//
//  Created by Turbo on 7/24/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "CharityTVC.h"
#import "UserPreferences.h"
#import "IOSRequest.h"

@interface CharityTVC () <charityDelegate>
@property (nonatomic, strong) UserPreferences *preferences;
@property (nonatomic, strong) NSMutableArray *data;
@property (nonatomic, strong) NSString *charity;
@end

@implementation CharityTVC

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (NSMutableArray *)data
{
    if (!_data) _data = [[NSMutableArray alloc] init];
    return _data;
}

- (NSString *)charity
{
    if (!_charity) _charity = [[NSString alloc] init];
    return _charity;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
    self.modalPresentationStyle = UIModalPresentationCurrentContext;
    self.tabBarController.modalPresentationStyle = UIModalPresentationCurrentContext;
    [self loadPage];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [IOSRequest displayCharity:[self.preferences getUserID]
                  onCompletion:^(NSDictionary *results) {
                      dispatch_async(dispatch_get_main_queue(), ^{
                       self.charity=results[@"charity_id"];
                          if([self.charity isKindOfClass:[NSNull class]]){
                              self.charity=@"";
                          }
                          for(int i=0;i<[self.data count];i++){
                              UITableViewCell *otherCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
                              otherCell.accessoryType = UITableViewCellAccessoryNone;
                              if([self.charity isEqualToString: otherCell.textLabel.text]){
                                  otherCell.accessoryType = UITableViewCellAccessoryCheckmark;}
                              
                          }
                          
                      });
                      
                  }];

    
            //[self.tableView reloadData];

}

- (void)loadPage {
    [IOSRequest displayCharity:[self.preferences getUserID]
                  onCompletion:^(NSDictionary *results) {
                      dispatch_async(dispatch_get_main_queue(), ^{
                          for(id obj in results[@"charity"]){
                              [self.data addObject:obj];
                          }
                         
                              self.charity=results[@"charity_id"];
                          if([self.charity isKindOfClass:[NSNull class]]){
                              self.charity=@"";
                          }
                          UIBarButtonItem *rightButton = [[UIBarButtonItem alloc]
                                                          initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                                          target:self
                                                          action:@selector(myAction:)];
                          self.navigationItem.rightBarButtonItem = rightButton;
                                                   [self.tableView reloadData];
                          
                      });
                      
                  }];
    
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.data count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CharityCell" forIndexPath:indexPath];
    
    // Configure the cell...
    cell.textLabel.text=[self.data objectAtIndex:indexPath.row];
    cell.textLabel.font=[UIFont fontWithName: @"Helvetica Neue" size: 13.0];
    if([[self.data objectAtIndex:indexPath.row] isEqualToString:self.charity]){
        cell.accessoryType = UITableViewCellAccessoryCheckmark;}


    return cell;
}

- (void)holdUI{ HUD = [MBProgressHUD showHUDAddedTo:self.tableView animated:YES];
                HUD.labelText = @"Updating";
                [MBProgressHUD hideHUDForView:self.tableView animated:YES];
  
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    HUD = [MBProgressHUD showHUDAddedTo:self.tableView animated:YES];
    HUD.labelText = @"Updating";
    NSString *newCharityName = @"";
    UITableViewCell *selectedCell = [self.tableView cellForRowAtIndexPath:indexPath];
    if( selectedCell.accessoryType == UITableViewCellAccessoryCheckmark){
        selectedCell.accessoryType = UITableViewCellAccessoryNone;
        newCharityName = @"";
    }
    else{
     
    for(int i=0;i<[self.data count];i++){
        UITableViewCell *otherCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
        otherCell.accessoryType = UITableViewCellAccessoryNone;
    }
        selectedCell.accessoryType = UITableViewCellAccessoryCheckmark;
        newCharityName = [self.data objectAtIndex:indexPath.row];
    }
   
    [IOSRequest setCharity:[self.preferences getUserID]
                            charity:newCharityName
                          onCompletion:^(NSDictionary *results) {
                              
                              dispatch_async(dispatch_get_main_queue(), ^{
                                  [MBProgressHUD hideHUDForView:self.tableView animated:YES];
                                 // [self.navigationController popViewControllerAnimated:YES];

                               
                              });
                          }];
  
}

-(void)myAction:(UIBarButtonItem *)button
{
    [self performSegueWithIdentifier:@"popCharity" sender:self];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/




- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    CharityVC *pop=segue.destinationViewController;
    pop.delegate=self;
    
}


@end
