//
//  VDSportsCell.m
//  Vaiden
//
//  Created by James Chung on 3/3/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "VDSportsCell.h"
#import "VenueDetailSportsCVC.h"
#import "Sport.h"

@implementation VDSportsCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
       
    }
    return self;
}

- (void)initializeCollectionView
{
    self.sportsCollectionView.delegate = self;
    self.sportsCollectionView.dataSource = self;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.sportsArray count];

}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Sport Cell";
        
    VenueDetailSportsCVC *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    Sport *sport = [self.sportsArray objectAtIndex:indexPath.row];
        
    cell.sportIcon.image = [[sport getSportIcon] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [cell.sportIcon setTintColor:[UIColor darkGrayColor]];
    
    cell.sportName.text = [sport.sportName capitalizedString];
        
    return cell;
  
}

@end
