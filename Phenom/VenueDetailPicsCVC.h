//
//  VenueDetailPicsCVC.h
//  Vaiden
//
//  Created by James Chung on 10/9/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VenueDetailPicsCVC : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *venuePic;

@end
