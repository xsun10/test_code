//
//  UITextView+FormValidations.m
//  Vaiden
//
//  Created by James Chung on 12/6/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "UITextView+FormValidations.h"

@implementation UITextView (FormValidations)


- (BOOL)checkWithRegex:(NSString *)regexString
{
    
    BOOL success = YES;
    
    if(!self.text)
    {
        success = NO;
    }
    else if(regexString)
    {
        NSError *error = NULL;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:regexString options:NSRegularExpressionCaseInsensitive error:&error];
        if(!error)
        {
            NSUInteger numberOfMatches = [regex numberOfMatchesInString:self.text options:0 range:NSMakeRange(0, self.text.length)];
            success = numberOfMatches == 1;
        }
    }
    
    return success;
}

- (BOOL)checkVenueDescription
{
 //   NSString *regexString = @"^[a-zA-Z0-9\\s.\\-\\'\\!\\?\\,\\:\\&\\$\\@]+$";
    
 //   if (nil == self.text)
 //       self.text = [NSString string];
    
 //   return [self checkWithRegex:regexString];
    return YES;
}

- (BOOL)checkMatchDescription
{
//    NSString *regexString = @"^[a-zA-Z0-9\\s.\\-\\'\\!\\?\\,\\:\\&\\$\\@]+$";
    
//    if (nil == self.text)
//        self.text = [NSString string];
    
//    return [self checkWithRegex:regexString];
    return NO;
}

@end
