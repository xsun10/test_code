//
//  vedioVC.m
//  video test
//
//  Created by Turbo on 7/29/14.
//  Copyright (c) 2014 Turbo. All rights reserved.
//

#import "vedioVC.h"
#import <AVFoundation/AVFoundation.h>
#import "PhenomAppDelegate.h"
@interface vedioVC ()
@property (weak, nonatomic) IBOutlet UIView *playerView;
@property (weak, nonatomic) IBOutlet UIImageView *tab1;
@property (weak, nonatomic) IBOutlet UIImageView *tab2;
@property (weak, nonatomic) IBOutlet UIImageView *tab3;
@property (weak, nonatomic) IBOutlet UIImageView *tab4;
@property (nonatomic, strong) UIView *tempView;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
@property (nonatomic, strong)  AVPlayer* mp;
@property (nonatomic, strong)  AVPlayerLayer *playerLayer;
@property (nonatomic, strong)  AVAsset *composition;
@end

@implementation vedioVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self showTempSpinner];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reloadRunningManVideo)
                                                 name:@"reloadRunningManVideo"
                                               object:nil];

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,
                                             (unsigned long)NULL), ^(void) {
        self.composition = [self makeAssetComposition];
        
        // create an AVPlayer with your composition
        
                dispatch_async(dispatch_get_main_queue(), ^(void) {
                    [NSTimer scheduledTimerWithTimeInterval:4.0f target:self selector:@selector(change) userInfo:nil repeats:YES];

                     self.mp = [AVPlayer playerWithPlayerItem:[AVPlayerItem playerItemWithAsset:self.composition]];
                    
                    // Add the player to your UserInterface
                    // Create a PlayerLayer:
                    self.playerLayer = [AVPlayerLayer playerLayerWithPlayer:self.mp];
                    
                    // integrate it to your view. Here you can customize your player (Fullscreen, or a small preview)
                    self.playerLayer.frame = self.playerView.bounds;
                    
                    self.playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
                    [self.playerView.layer addSublayer:self.playerLayer];
                    //And finally play your video:
                    
                    [self.mp play];
                    [self.activityIndicator stopAnimating];
                    self.tempView.hidden = YES;
                });
    });
    
}

- (void)reloadRunningManVideo
{   if(self.mp){
    self.mp = nil;
    self.playerLayer = nil;}
    
    self.mp = [AVPlayer playerWithPlayerItem:[AVPlayerItem playerItemWithAsset:self.composition]];
    
    // Add the player to your UserInterface
    // Create a PlayerLayer:
    self.playerLayer = [AVPlayerLayer playerLayerWithPlayer:self.mp];
    
    // integrate it to your view. Here you can customize your player (Fullscreen, or a small preview)
    self.playerLayer.frame = self.playerView.bounds;
    self.playerLayer.bounds = self.playerView.bounds;
    self.playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [self.playerView.layer addSublayer:self.playerLayer];
    //And finally play your video:
  
      [self.mp play];
}

-(BOOL)prefersStatusBarHidden { return YES; }
- (void)viewDidAppear:(BOOL)animated
{
       dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,
                                             (unsigned long)NULL), ^(void) {
         [super viewDidAppear:animated];
        // create an AVPlayer with your composition
        
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            //[self reloadRunningManVideo];
            [self.mp play];
                    });
    });

}

- (void)viewWillDisappear:(BOOL)animated
{
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,
                                             (unsigned long)NULL), ^(void) {
        [super viewWillDisappear:animated];
        // create an AVPlayer with your composition
        
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            [self.mp pause];
           
        });
    });

}



-(void)change{
    [UIView animateWithDuration:2 animations:^{
        if(self.tab1.alpha==1){
            self.tab1.alpha = 0;
            self.tab2.alpha = 1;
        } else  if(self.tab2.alpha==1){
            self.tab3.alpha = 1;
            self.tab2.alpha = 0;
        } else if(self.tab3.alpha==1){
            self.tab4.alpha = 1;
            self.tab3.alpha = 0;
        } else{
            self.tab4.alpha = 0;
            self.tab1.alpha = 1;
        }
        
        
    } completion:nil];
}
- (void)showTempSpinner
{
    self.tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    self.tempView.backgroundColor = [UIColor lightGrayColor];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] init];
    [self.activityIndicator setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width/2.0, [UIScreen mainScreen].bounds.size.height/2.0 )];
    [self.activityIndicator startAnimating];
    [self.tempView addSubview:self.activityIndicator];
    
    [self.view addSubview:self.tempView];
    
}



- (AVAsset*) makeAssetComposition {
    
    int numOfCopies = 600;
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"running" ofType:@"mp4"];
    NSURL *sourceMovieURL = [NSURL fileURLWithPath:filePath];
    
    AVMutableComposition *composition = [[AVMutableComposition alloc] init] ;
    AVURLAsset* sourceAsset = [AVURLAsset URLAssetWithURL:sourceMovieURL options:nil];
    
    // calculate time
    CMTimeRange editRange = CMTimeRangeMake(CMTimeMake(0, 1000), CMTimeMake(sourceAsset.duration.value, sourceAsset.duration.timescale));
    
    NSError *editError;
    
    // and add into your composition
    BOOL result = [composition insertTimeRange:editRange ofAsset:sourceAsset atTime:composition.duration error:&editError];
    
    if (result) {
        for (int i = 0; i < numOfCopies; i++) {
            [composition insertTimeRange:editRange ofAsset:sourceAsset atTime:composition.duration error:&editError];
        }
    }
    
    return composition;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
