//
//  Venue+MKAnnotation.m
//  Phenom
//
//  Created by James Chung on 5/7/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "Venue+MKAnnotation.h"

@implementation Venue (MKAnnotation)

/*
- (CLLocationCoordinate2D)coordinate
{
    CLLocationCoordinate2D coordinate;
    
    coordinate.latitude = [self.latitude doubleValue];
    coordinate.longitude = [self.longitude doubleValue];
    
    return coordinate;
}
*/
/*
- (CLLocationCoordinate2D) coordinate
{
    NSString *address = [NSString stringWithFormat:@"%@", self.stateName];
    
    double latitude = 0, longitude = 0;
    NSString *esc_addr =  [address stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *req = [NSString stringWithFormat:@"http://maps.google.com/maps/api/geocode/json?sensor=false&address=%@", esc_addr];
    
    NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:req] encoding:NSUTF8StringEncoding error:NULL];
    if (result) {
        NSScanner *scanner = [NSScanner scannerWithString:result];
        if ([scanner scanUpToString:@"\"lat\" :" intoString:nil] && [scanner scanString:@"\"lat\" :" intoString:nil]) {
            [scanner scanDouble:&latitude];
            if ([scanner scanUpToString:@"\"lng\" :" intoString:nil] && [scanner scanString:@"\"lng\" :" intoString:nil]) {
                [scanner scanDouble:&longitude];
            }
        }
    }
    CLLocationCoordinate2D center;
    center.latitude = latitude;
    center.longitude = longitude;
    
    NSLog (@"latitude %f", latitude);
    NSLog (@"longitutd %f", longitude);
    return center;
}

*/
- (CLLocationCoordinate2D) coordinate
{
    if ([self.streetAddress length] != 0) {
        NSString *address = [NSString stringWithFormat:@"%@ %@, %@",self.streetAddress, self.city, self.stateName];
        
        double latitude = 0, longitude = 0;
        NSString *esc_addr =  [address stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *req = [NSString stringWithFormat:@"http://maps.google.com/maps/api/geocode/json?sensor=false&address=%@", esc_addr];
        NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:req] encoding:NSUTF8StringEncoding error:NULL];
        if (result) {
            NSScanner *scanner = [NSScanner scannerWithString:result];
            if ([scanner scanUpToString:@"\"lat\" :" intoString:nil] && [scanner scanString:@"\"lat\" :" intoString:nil]) {
                [scanner scanDouble:&latitude];
                if ([scanner scanUpToString:@"\"lng\" :" intoString:nil] && [scanner scanString:@"\"lng\" :" intoString:nil]) {
                    [scanner scanDouble:&longitude];
                }
            }
        }
        CLLocationCoordinate2D center;
        center.latitude = latitude;
        center.longitude = longitude;
    
        NSLog (@"latitude %f", latitude);
        NSLog (@"longitutd %f", longitude);
            return center;
    }
    
    // revision: 12/6/13
    // otherwise rely on longitude latitude...
    // actually probably don't need to set with street address and city...can just use coordinates
    // Leaving it for now because do not know yet which is the most accurate option...
    
    CLLocationCoordinate2D center;
    center.latitude = self.latitude;
    center.longitude = self.longitude;
    return  center;
    
}


- (NSString *)title
{
    return self.venueName;
}

- (NSString *)subtitle
{
    return self.city;
}

// this blocks main thread
- (UIImage *)thumbnail
{
  //  return [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:self.thumbnailURLString]]];
    return nil;
}

@end
