//
//  ShowAlertsTagCVCell.h
//  Vaiden
//
//  Created by James Chung on 5/16/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShowAlertsTagCVCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *tagImage;
@property (weak, nonatomic) IBOutlet UIButton *picButton;

@end
