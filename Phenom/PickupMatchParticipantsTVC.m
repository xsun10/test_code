//
//  PickupMatchParticipantsTVC.m
//  Phenom
//
//  Created by James Chung on 4/10/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "PickupMatchParticipantsTVC.h"
#import "PickupMatchParticipantsCell.h"
#import "IOSRequest.h"
#import "S3Tools.h"
#import "AFNetworking.h"
#import "UIImage+ProportionalFill.h"

@interface PickupMatchParticipantsTVC ()

@end

@implementation PickupMatchParticipantsTVC



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.playerList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Individual Participants Cell";
    PickupMatchParticipantsCell *cell = (PickupMatchParticipantsCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    MatchPlayer *player = (self.playerList)[indexPath.row];
    
    cell.playerUsernameLabel.text = player.userName;
    
    PlayerSport *sport = [PlayerSport getSportObjectForName:self.matchSport inArray:player.playerSports];
    
    cell.playerExperienceLevelLabel.text =  [NSString stringWithFormat:@"Level %@", sport.level];
    cell.status.text = player.status;
    
 //   [IOSRequest getPhoto:1 forID:player.userID onCompletion:^(UIImage *image) {
 //       dispatch_async(dispatch_get_main_queue(), ^{
 //           cell.playerImage.image = image;
   //     });
  //  }];
    
 /*   [IOSRequest getProfilePicCredentials:player.userID onCompletion:^(NSDictionary *results) {
        NSString *fileString = results[@"filestring"];
        
         NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                baseString:fileString];
        
        [cell.playerImage setImageWithURL:[NSURL URLWithString:url]
                        placeholderImage:[UIImage imageNamed:@"avatar.jpg"]];
                
    }];*/
    
    NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                            baseString:player.profileBaseString];
    
    [cell.playerImage setImageWithURL:[NSURL URLWithString:url]
                    placeholderImage:[UIImage imageNamed:@"avatar round"]];
    [UIImage makeRoundedImage:cell.playerImage withRadius:20];

    
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

@end
