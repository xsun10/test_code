//
//  UniversityChallengeSplashVC.m
//  Vaiden
//
//  Created by James Chung on 2/28/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "UniversityChallengeSplashVC.h"
#import "IOSRequest.h"
#import "UserPreferences.h"
#import "UniversityChallengeRankingsTVC.h"
#import "UniversityChallengeDetailsVC.h"
#import <FacebookSDK/FacebookSDK.h>
#import "UIColor+VaidenColors.h"
#import "UIView+Manipulate.h"

@interface UniversityChallengeSplashVC ()
@property (weak, nonatomic) IBOutlet UIScrollView *pageScrollView;
@property (nonatomic, strong) UserPreferences *preferences;
@property (weak, nonatomic) IBOutlet UIButton *registerButton;
@property (nonatomic, strong) UIView *tempView;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;

@property (nonatomic) NSInteger competitionID;
@property (nonatomic) NSInteger universityID;
@property (nonatomic, strong) NSString *universityName;
@property (nonatomic) BOOL canRegister;
@property (weak, nonatomic) IBOutlet UIView *registrationStatusView;
@property (nonatomic, strong) UIView *fixedBar;
@property (weak, nonatomic) IBOutlet UIButton *learnmoreButton;

@end

@implementation UniversityChallengeSplashVC

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return  _preferences;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
    
    [self.learnmoreButton makeCircleWithColor:[UIColor newBlueLight] andRadius:3];
    [self.registerButton makeCircleWithColor:[UIColor newBlueLight] andRadius:3];
    
   }

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self showTempSpinner];
    [IOSRequest checkIfRegisteredForCompetitionWithUser:[self.preferences getUserID]
                                           onCompletion:^(NSDictionary *results) {
                                               self.competitionID = [results[@"competition_id"] integerValue];
                                               self.universityID = [results[@"university_id"] integerValue];
                                               self.universityName = results[@"university_name"];
                                               
                                               dispatch_async(dispatch_get_main_queue(), ^ {
                                                   
                                                   if ([results[@"status"] isEqualToString:@"registered"]) {
                                                       self.canRegister = NO;
                                                       [self showFixedBar];
                                                       [self.registerButton setTitle:@"RANKINGS" forState:UIControlStateNormal];
                                                   } else {
                                                       self.canRegister = YES;
                                                       self.fixedBar.hidden = YES;
                                                       [self.registerButton setTitle:@"REGISTER" forState:UIControlStateNormal];
                                                   }
                                                   [self.activityIndicator stopAnimating];
                                                   self.tempView.hidden = YES;

                                               });
                                               
                                           }];
    
}

- (void)showFixedBar
{
    self.fixedBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 27)]; // was 35 height
    self.fixedBar.backgroundColor = [UIColor newBlueLight];
    
    [self.view addSubview:self.fixedBar];
    
    UILabel *statusLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, 290, 27)]; // was 35 height
    statusLabel.textColor = [UIColor whiteColor];
    statusLabel.font = [UIFont fontWithName:@"HelveticaNeue-MediumItalic" size:13];
    statusLabel.text = @"You have registered for this event";
    
    [self.fixedBar addSubview:statusLabel];
    

}
- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    self.pageScrollView.contentSize = CGSizeMake(320.0, 720.0);
}

- (void)showTempSpinner
{
    self.tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, [UIScreen mainScreen].bounds.size.height)];
    self.tempView.backgroundColor = [UIColor lightGrayColor];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] init];
    [self.activityIndicator setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width/2.0, [UIScreen mainScreen].bounds.size.height/2.0 )];
    [self.activityIndicator startAnimating];
    [self.tempView addSubview:self.activityIndicator];
    
    [self.view addSubview:self.tempView];
    
}


- (IBAction)regsiterAction:(UIButton *)sender
{
    if ([sender.titleLabel.text isEqualToString:@"REGISTER"])
        [self performSegueWithIdentifier:@"Register Segue" sender:self];
    else if ([sender.titleLabel.text isEqualToString:@"RANKINGS"])
        [self performSegueWithIdentifier:@"Rankings Segue" sender:self];
}

- (IBAction)learnAction:(id)sender
{
    [self performSegueWithIdentifier:@"University Details" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Rankings Segue"]) {
        UniversityChallengeRankingsTVC *controller = segue.destinationViewController;
        controller.competitionID = self.competitionID;
        controller.universityID = self.universityID;
        controller.universityName = self.universityName;
    } else if ([segue.identifier isEqualToString:@"University Details"]) {
        UniversityChallengeDetailsVC *controller = segue.destinationViewController;
        controller.canRegister = self.canRegister;
        controller.competitionID = self.competitionID;
        controller.universityID = self.universityID;
        controller.universityName = self.universityName;
    }
}

- (IBAction)shareAction:(id)sender
{
    [self startFBShare];
}

- (void)startFBShare
{
 
    if (FBSession.activeSession.isOpen && [FBSession.activeSession.permissions indexOfObject:@"publish_actions"] == NSNotFound) {
        // Request publish_actions
        [FBSession.activeSession requestNewPublishPermissions:[NSArray arrayWithObject:@"publish_actions"]
                                              defaultAudience:FBSessionDefaultAudienceFriends
                                            completionHandler:^(FBSession *session, NSError *error) {
                                                __block NSString *alertText;
                                                __block NSString *alertTitle;
                                                if (!error) {
                                                    if ([FBSession.activeSession.permissions
                                                         indexOfObject:@"publish_actions"] == NSNotFound){
                                                        // Permission not granted, tell the user we will not publish
                                                        alertTitle = @"Permission not granted";
                                                        alertText = @"Your action will not be published to Facebook.";
                                                        [[[UIAlertView alloc] initWithTitle:@"title"
                                                                                    message:@"text"
                                                                                   delegate:self
                                                                          cancelButtonTitle:@"OK!"
                                                                          otherButtonTitles:nil] show];
                                                    } else {
                                                        // Permission granted, publish the OG story
                                                        [self presentFBDialog];
                                                    }
                                                    
                                                } else {
                                                    // There was an error, handle it
                                                    // See https://developers.facebook.com/docs/ios/errors/
                                                }
                                            }];
        
    } else {
        // permission exists
        [self presentFBDialog];
    }
    
    
    
    
    
}

- (void)presentFBDialog
{
    // Present share dialog
    /*   FBShareDialogParams *params = [[FBShareDialogParams alloc] init];
     params.link = [NSURL URLWithString:@"https://www.vaiden.com"];
     params.name = self.fbTitle;
     params.caption = @"Vaiden is a community for amateur athletes to play their favorite sports using a social platform.";
     params.picture = [NSURL URLWithString:@"https://s3.amazonaws.com/vaderen_pictures/vaiden_fb_icon.png"];
     //  params.picture = self.fbImage;
     params.description = self.fbMessage;
     */
    //
    /*
     [FBDialogs presentShareDialogWithLink:params.link
     name:params.name
     caption:params.caption
     description:params.description
     picture:params.picture
     clientState:nil
     handler:^(FBAppCall *call, NSDictionary *results, NSError *error) {
     if(error) {
     // An error occurred, we need to handle the error
     // See: https://developers.facebook.com/docs/ios/errors
     NSLog([NSString stringWithFormat:@"Error publishing story: %@", error.description]);
     } else {
     // Success
     NSLog(@"result %@", results);
     }
     }];
     */
    // Put together the dialog parameters
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   @"Play 1vs1 BBall and you could win $1,000", @"name",
                                   [NSString stringWithFormat:@"Join @%@ and play in The Intercollegiate 1 vs 1 Basketball Championships", [self.preferences getUserName]] , @"caption",
                                   @"Vaiden is a community for athletes to play their favorite sports using a social platform.", @"description",
                                   @"https://itunes.apple.com/us/app/vaiden/id796761406?ls=1&mt=8", @"link",
                                   @"https://s3.amazonaws.com/vaderen_pictures/competition_fb.jpg", @"picture",
                                   nil];
    
    // Show the feed dialog
    [FBWebDialogs presentFeedDialogModallyWithSession:nil
                                           parameters:params
                                              handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
                                                  if (error) {
                                                      // An error occurred, we need to handle the error
                                                      // See: https://developers.facebook.com/docs/ios/errors
                                               //      NSLog([NSString stringWithFormat:@"Error publishing story: %@", error.description]);
                                                  } else {
                                                      if (result == FBWebDialogResultDialogNotCompleted) {
                                                          // User cancelled.
                                                          NSLog(@"User cancelled.");
                                                      } else {
                                                          // Handle the publish feed callback
                                                          NSDictionary *urlParams = [self parseURLParams:[resultURL query]];
                                                          
                                                          if (![urlParams valueForKey:@"post_id"]) {
                                                              // User cancelled.
                                                              NSLog(@"User cancelled.");
                                                              
                                                          } else {
                                                              // User clicked the Share button
                                                              NSString *result = [NSString stringWithFormat: @"Posted story, id: %@", [urlParams valueForKey:@"post_id"]];
                                                              NSLog(@"result %@", result);
                                                          }
                                                      }
                                                  }
                                              }];
}

// A function for parsing URL parameters returned by the Feed Dialog.
- (NSDictionary*)parseURLParams:(NSString *)query {
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *pair in pairs) {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        NSString *val =
        [kv[1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        params[kv[0]] = val;
    }
    return params;
}


@end
