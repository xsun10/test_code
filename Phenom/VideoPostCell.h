//
//  VideoPostCell.h
//  Phenom
//
//  Created by James Chung on 5/29/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VideoPostCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *profilePic;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *userTextPost;
@property (weak, nonatomic) IBOutlet UIView *viewForVideo;
@property (weak, nonatomic) IBOutlet UIImageView *videoPostIImage;
@property (weak, nonatomic) IBOutlet UIButton *commentButton;
@property (weak, nonatomic) IBOutlet UIImageView *commentIcon;
@property (weak, nonatomic) IBOutlet UILabel *commentLabel;
@property (weak, nonatomic) IBOutlet UIButton *remixButton;
@property (weak, nonatomic) IBOutlet UIImageView *remixIcon;
@property (weak, nonatomic) IBOutlet UILabel *remixLabel;
@property (weak, nonatomic) IBOutlet UIImageView *timerIcon;

@end
