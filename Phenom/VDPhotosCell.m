//
//  VDPhotosCell.m
//  Vaiden
//
//  Created by James Chung on 3/3/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "VDPhotosCell.h"
#import "VenueDetailPicsCVC.h"
#import "VenueImage.h"
#import "S3Tools.h"
#import "UIImage+ProportionalFill.h"
#import "AFNetworking.h"

@implementation VDPhotosCell

- (void)initializePicsCollectionView
{
    self.picsCollectionView.dataSource = self;
    self.picsCollectionView.delegate = self;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.picsArray count];
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"Pic Cell";
        
    VenueDetailPicsCVC *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
        
    cell.venuePic.contentMode = UIViewContentModeScaleAspectFill;
    cell.venuePic.clipsToBounds = YES;
        
    VenueImage *vI = [self.picsArray objectAtIndex:indexPath.row];
        
    NSString *url = [S3Tools getFileNameStringWithType:@"venue_pic"
                                                baseString:vI.venuePicString];
        
    [cell.venuePic setImageWithURL:[NSURL URLWithString:url]
                      placeholderImage:[UIImage imageNamed:@"Landscape Placeholder"]];
        
    UITapGestureRecognizer *pgr = [[UITapGestureRecognizer alloc]
                                       initWithTarget:self action:@selector(handleTapOnVenuePic:)];
    cell.venuePic.userInteractionEnabled = YES;
    [cell.venuePic addGestureRecognizer:pgr];
        
    return  cell;
    
}

- (void)handleTapOnVenuePic:(UITapGestureRecognizer *)tapGestureRecognizer
{
    VenueDetailPicsCVC *cell = (VenueDetailPicsCVC *)tapGestureRecognizer.view.superview.superview;
    CGRect test = cell.bounds;
    CGRect test2 = [cell convertRect:test toView:[cell superview]];
    NSIndexPath *selectedIndexPath = [self.picsCollectionView indexPathForCell:cell];
    CGFloat scrollOffset =  self.picsCollectionView.contentOffset.x;
    
    CGPoint centerP = CGPointMake(test2.origin.x, test2.origin.y);
    CGPoint center = centerP;
   
    [self.delegate setWithPicToShowEnlarged:self withPicNum:selectedIndexPath.row Center:center Offset:scrollOffset];

}


@end
