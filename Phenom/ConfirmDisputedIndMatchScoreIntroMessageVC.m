//
//  ConfirmDisputedIndMatchScoreIntroMessageVC.m
//  Vaiden
//
//  Created by James Chung on 8/5/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "ConfirmDisputedIndMatchScoreIntroMessageVC.h"
#import "UIView+Manipulate.h"
#import "UIImage+ImageEffects.h"
#import "UIColor+VaidenColors.h"

@interface ConfirmDisputedIndMatchScoreIntroMessageVC ()
@property (weak, nonatomic) IBOutlet UILabel *sportLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UIView *disputeIconBackground;
@property (weak, nonatomic) IBOutlet UIImageView *disputeIcon;
@property (weak, nonatomic) IBOutlet UIButton *reviewScoreButtonHandle;
@property (weak, nonatomic) IBOutlet UIImageView *blurredImageBackground;
@property (weak, nonatomic) IBOutlet UIScrollView *pageScrollView;

@end

@implementation ConfirmDisputedIndMatchScoreIntroMessageVC


- (void)viewDidLoad
{
    [super viewDidLoad];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
    [self.pageScrollView setContentInset:UIEdgeInsetsMake(64, 0, 44, 0)];
    [self setPageBlurredBackgroundImage:1];
    
    [self setPageInfo];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    self.pageScrollView.contentSize = CGSizeMake(320.0, 600.0);
    
}

- (void)setPageInfo
{
    self.sportLabel.text = [NSString stringWithFormat:@"%@ Challenge", [self.individualMatch.sport.sportName capitalizedString]];
    self.locationLabel.text = [NSString stringWithFormat:@"at %@", self.individualMatch.venue.venueName];
    
    NSString *dateText = @"";
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMMM d, YYYY"];
    dateText = [dateFormatter stringFromDate:self.individualMatch.dateTime];
    
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    [timeFormatter setDateFormat:@"hh:mm a"];
    self.dateTimeLabel.text = [NSString stringWithFormat:@"%@ at %@", dateText, [timeFormatter stringFromDate: self.individualMatch.dateTime]];
    
    [self.disputeIconBackground makeCircleWithColor:[UIColor sienna] andRadius:45];
    
    self.disputeIcon.image = [[UIImage imageNamed:@"Confused Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.disputeIcon setTintColor:[UIColor whiteColor]];
    
    [self.reviewScoreButtonHandle makeCircleWithColor:[UIColor sienna] andRadius:3];
    [self.reviewScoreButtonHandle setBackgroundColor:[UIColor sienna]];
}

- (IBAction)reviewScoreAction:(id)sender
{
    [self performSegueWithIdentifier:@"Confirm Disputed Score Segue" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Confirm Disputed Score Segue"]) {
      //  ConfirmIndividualMatchScoreTVC *controller = (ConfirmIndividualMatchScoreTVC *)segue.destinationViewController;
      //  controller.individualMatch = self.individualMatch;
    }
}

- (void)setPageBlurredBackgroundImage:(NSUInteger)imageIndex
{
    NSString *effectText = @"";
    UIImage *effectImage = nil;
    UIImage *backImage = [UIImage imageNamed:@"BBall Player Background"];
    
    effectImage = backImage;
    
    switch (imageIndex)
    {
        case 0:
            effectImage = backImage;
            break;
        case 1:
            effectImage = [backImage applyLightEffect];
            effectText = NSLocalizedString(@"Light", @"");
            break;
        case 2:
            effectImage = [backImage applyExtraLightEffect];
            effectText = NSLocalizedString(@"Extra light", @"");
            break;
        case 3:
            effectImage = [backImage applyDarkEffect];
            effectText = NSLocalizedString(@"Dark", @"");
            break;
        case 4:
            effectImage = [backImage applyTintEffectWithColor:[UIColor blueColor]];
            effectText = NSLocalizedString(@"Color tint", @"");
            break;
    }
    
    self.blurredImageBackground.image = effectImage;
    
    
}


@end
