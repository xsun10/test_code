//
//  ImageSegue.m
//  Vaiden
//
//  Created by Turbo on 6/11/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "ImageSegue.h"
#import "S3Tools.h"
#import "UIImageView+AFNetworking.h"
#import "VenuePicDetailVC.h"

@implementation ImageSegue

- (instancetype) initWithIdentifier:(NSString *)identifier
                             source:(UIViewController *)source
                        destination:(UIViewController *)destination
{
    self = [super initWithIdentifier:identifier source:source destination:destination];
    if (self) {
        self.unwind = NO;
    }
    return self;
}

- (void)perform
{
    UIWindow *mainWindow = [[UIApplication sharedApplication].windows objectAtIndex:0];
    
    UIViewController *sourceViewController = self.sourceViewController;
    UIViewController *destinationViewController = self.destinationViewController;
    
    // Add the destination view as a subview, temporarily
    UIImageView *tmpView = [[UIImageView alloc] init];
    
    if (self.unwind == YES) { // Dismiss the image view]
        // Add view to super view temporarily
        //CGRect frame = [sourceViewController.view convertRect:CGRectMake(self.originatingPoint.x-self.scrollOffset, self.originatingPoint.y, self.showupView.image.size.width, self.showupView.image.size.height) toView:sourceViewController.view.window];
        //NSLog(@"target frame: %f, %f, %f, %f",self.showupView.bounds.origin.x, self.showupView.bounds.origin.y,self.showupView.bounds.size.width,self.showupView.bounds.size.height);
        [mainWindow addSubview:self.showupView];
        //[sourceViewController.view.superview insertSubview:destinationViewController.view atIndex:0];
        [UIView animateWithDuration:0.3
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             // Shrink!
                             self.showupView.transform = CGAffineTransformMakeScale(0, 0);
                             self.showupView.center = CGPointMake(self.originFrame.origin.x, self.originFrame.origin.y-20);
                             [self.destinationViewController dismissViewControllerAnimated:YES completion:nil];
                         }
                         completion:^(BOOL finished){
                             self.showupView.hidden = YES;
                             [self.showupView removeFromSuperview]; // remove from temp super view
                         }];
    } else { // The image view show up
        VenuePicDetailVC *controller = (VenuePicDetailVC *)destinationViewController;
        
        //NSLog(@"%f, %f, %f, %f",self.originatingPoint.x, self.originatingPoint.y,self.image.size.width,self.image.size.height);
        CGRect frame = [mainWindow convertRect:CGRectMake(self.originatingPoint.x-self.scrollOffset, self.originatingPoint.y, self.image.size.width, self.image.size.height)
                                      fromView:((UIViewController *)self.sourceViewController).view];
        NSLog(@"original frame: %f, %f, %f, %f",frame.origin.x, frame.origin.y,frame.size.width,frame.size.height);
        tmpView.frame = frame;
        tmpView.image = self.image;
        tmpView.contentMode = UIViewContentModeScaleAspectFill;
        tmpView.clipsToBounds = YES;
        [mainWindow addSubview:tmpView];
        
        // Transformation start scale
        tmpView.transform = CGAffineTransformMakeScale(0.28, 0.20);
        
        CGPoint point = CGPointMake([UIScreen mainScreen].bounds.size.width/2, [UIScreen mainScreen].bounds.size.height/2);
        
        // Store original centre point of the destination view
        //CGPoint originalCenter = destinationViewController.view.center;
        // Set center to start point of the button
        tmpView.center = frame.origin;
        
        [UIView animateWithDuration:0.4
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             // Grow!
                             tmpView.transform = CGAffineTransformMakeScale(1, 1);
                             tmpView.center = point;
                             ((UIViewController *)self.destinationViewController).modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                             [sourceViewController presentViewController:destinationViewController animated:YES completion:NULL]; // present destination view controller
                         }
                         completion:^(BOOL finished){
                             VenuePicDetailVC *vc = (VenuePicDetailVC *)destinationViewController;
                             vc.frame = frame;
                             int64_t delayInSeconds = 1; // Delay one sec
                             dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC/4);
                             dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                                 controller.imageView.hidden = NO;
                                 tmpView.hidden = YES;
                                 [tmpView removeFromSuperview]; // remove from temp super view
                             });
                         }];
    }
}

@end
