//
//  TextPost.m
//  Phenom
//
//  Created by James Chung on 6/3/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "TextPost.h"

@implementation TextPost

- (TextPost *)initWithNewsDetails:(NSInteger)newsMakerUserID
                       andUserName:(NSString *)newsMakerUserName
                     andProfileStr:(NSString *)profileBaseString
                          withDate:(NSDate *)newsDate
                       andPostType:(NSInteger)postType
                         andNewsID:(NSInteger)newsID
                       andTextPost:(NSString *)textPost
                   andNumComments:(NSUInteger)numComments
                    andNumRemixes:(NSUInteger)numRemixes

{
    self = [super initWithNewsDetails:newsMakerUserID
                          andUserName:newsMakerUserName
                        andProfileStr:profileBaseString
                             withDate:newsDate
                          andPostType:postType
                       andNumComments:numComments
                        andNumRemixes:numRemixes
                            andNewsID:newsID
                          andTextPost:textPost];
    
    if (self) {

    }
    return self;
}


@end
