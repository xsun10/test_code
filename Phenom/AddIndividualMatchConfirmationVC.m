//
//  AddIndividualMatchConfirmationVC.m
//  Phenom
//
//  Created by James Chung on 7/15/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "AddIndividualMatchConfirmationVC.h"
#import "IOSRequest.h"
#import "UIImage+ProportionalFill.h"
#import "UIView+Manipulate.h"
#import "UserPreferences.h"
#import "MatchCreationSuccessVC.h"
#import "NSDate+Utilities.h"
#import <EventKit/EventKit.h>
#import "UIColor+VaidenColors.h"

@interface AddIndividualMatchConfirmationVC () <UIActionSheetDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *pageScrollView;
@property (weak, nonatomic) IBOutlet UIView *myContent;
@property (weak, nonatomic) IBOutlet UIView *iconBackground;
@property (weak, nonatomic) IBOutlet UIImageView *legalIcon;
@property (weak, nonatomic) IBOutlet UIButton *agreeButton;
@property (nonatomic, strong) UserPreferences *preferences;
@property (weak, nonatomic) IBOutlet UIView *circle1;
@property (weak, nonatomic) IBOutlet UIView *circle2;
@property (weak, nonatomic) IBOutlet UIView *circle3;

@end

@implementation AddIndividualMatchConfirmationVC

#define TITLE_OF_ACTIONSHEET @"Would you like for this event to be added to your iPhone Calendar?"
#define CANCEL_BUTTON_ACTIONSHEET @"Cancel"
#define CREATE_MATCH @"Create Match"
#define ADD_TO_CALENDAR @"Create Match + Add To Calendar"

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return  _preferences;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
    
    [self.iconBackground  makeCircleWithColor:[UIColor clearColor] andRadius:37];
    self.legalIcon.image = [[UIImage imageNamed:@"Legal Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.legalIcon setTintColor:[UIColor whiteColor]];
    [self.agreeButton makeRoundedBorderWithRadius:3];
    
    [self.circle1 makeCircleWithColor:[UIColor clearColor] andRadius:10];
    [self.circle2 makeCircleWithColor:[UIColor clearColor] andRadius:10];
    [self.circle3 makeCircleWithColor:[UIColor clearColor] andRadius:10];
    
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    self.pageScrollView.contentSize = CGSizeMake(320.0, 670.0);
}
- (IBAction)createMatch:(id)sender
{
    [self.agreeButton setEnabled:NO];
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:TITLE_OF_ACTIONSHEET delegate:self
                                  cancelButtonTitle:CANCEL_BUTTON_ACTIONSHEET destructiveButtonTitle:nil
                                  otherButtonTitles:ADD_TO_CALENDAR, CREATE_MATCH, nil];
    [actionSheet showFromTabBar:self.tabBarController.tabBar];

}

- (void)createAndSaveMatchToServer
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    self.individualMatch.dateTime = [self.individualMatch.dateTime toGlobalTime]; // before submit to server change to GMT
    
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.labelText = @"Creating Match";
    
    self.agreeButton.enabled = NO;
    [IOSRequest createIndividualMatchWithInfo:self.individualMatch onCompletion:^(NSDictionary *results) {
        //     if ([errors[@"error"] isEqualToString:@"Inserted OK"]) {
        [self.preferences setMatchNotificationRefreshState:YES];
        [self.preferences setNewsfeedRefreshState:YES];
        
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                [self.agreeButton setEnabled:YES];
                
                if (![results[@"outcome"] isEqualToString:@"failure"]) {
                    [self performSegueWithIdentifier:@"Challenge Match Success Segue" sender:self];
                } else if ([results[@"limit"] isEqualToString:@"exceeded"]) {
                    UIAlertView *alert = [[UIAlertView alloc] init];
                    alert.title = @"Whoa there!";
                    alert.message = @"You've reached your match creation limit for today.  Please try again tomorrow.";
                    [alert addButtonWithTitle:@"OK"];
                    [alert show];
                } else {
                    UIAlertView *alert = [[UIAlertView alloc] init];
                    alert.title = @"Match Creation Error";
                    alert.message = @"There was an error in creating your match.  Please retry your submission.";
                    [alert addButtonWithTitle:@"OK"];
                    [alert show];
                }
              });
    }];
    double delayInSeconds = 10.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        //code to be executed on the main queue after delay
        self.agreeButton.enabled = YES;
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    });
 
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Challenge Match Success Segue"]) {
        MatchCreationSuccessVC *controller = segue.destinationViewController;
        controller.matchCategory = @"individual";
    }
}



- (void)addMatchToCalendar
{
    EKEventStore *eventStore = [[EKEventStore alloc] init];
    
    [eventStore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
        
        EKEvent *event  = [EKEvent eventWithEventStore:eventStore];
        event.title     = self.individualMatch.matchName;
        event.notes     = [NSString stringWithFormat:@"%@ Challenge Match with %@", [self.individualMatch.sport.sportName capitalizedString], self.individualMatch.player2.userName];
        event.startDate = self.individualMatch.dateTime;
        event.endDate   = [[NSDate alloc] initWithTimeInterval:3600 sinceDate:self.individualMatch.dateTime];
        event.location = self.individualMatch.venue.venueName;
        
        [event setCalendar:[eventStore defaultCalendarForNewEvents]];
        NSError *err;
        [eventStore saveEvent:event span:EKSpanThisEvent error:&err];
        
        [self createAndSaveMatchToServer];
        
    }];
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == actionSheet.destructiveButtonIndex) {
        [self createAndSaveMatchToServer];
    } else {
        NSString *choice = [actionSheet buttonTitleAtIndex:buttonIndex];
        
        if ([choice isEqualToString:ADD_TO_CALENDAR]) {
            [self addMatchToCalendar];
            
        } else if ([choice isEqualToString:CREATE_MATCH]) {
            [self createAndSaveMatchToServer];
        } 
    }
}


@end
