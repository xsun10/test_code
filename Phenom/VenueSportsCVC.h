//
//  VenueSportsCVC.h
//  Phenom
//
//  Created by James Chung on 7/3/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VenueSportsCVC : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *sportIcon;
@property (weak, nonatomic) IBOutlet UILabel *sportName;
@property (weak, nonatomic) IBOutlet UIView *circleIcon;

@end
