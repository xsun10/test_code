//
//  ReportVenueTVC.h
//  Phenom
//
//  Created by James Chung on 7/5/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "CustomBaseTVC.h"

@interface ReportVenueTVC : CustomBaseTVC <MBProgressHUDDelegate>
{
    MBProgressHUD *HUD;
}

@property (nonatomic) NSInteger venueID;

@end
