//
//  RequiredMatchExperienceTVC.h
//  Phenom
//
//  Created by James Chung on 4/4/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomBaseTVC.h"

@class RequiredMatchExperienceTVC;

@protocol RequiredMatchExperienceTVC_Delegate <NSObject>
- (void)setMatchExperienceViewController:(RequiredMatchExperienceTVC *)controller
                          withExperience:(NSMutableArray *)experience
                                 andCode:(NSString *)experienceCode;
@end

@interface RequiredMatchExperienceTVC : CustomBaseTVC

@property (nonatomic, weak) id <RequiredMatchExperienceTVC_Delegate> delegate;

@end
