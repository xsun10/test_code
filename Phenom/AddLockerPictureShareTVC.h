//
//  AddLockerPictureShareTVC.h
//  Vaiden
//
//  Created by James Chung on 5/8/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "CustomBaseTVC.h"
#import "VaidenImageObject.h"
#import "PlayingSeason.h"
#import "PlayingYear.h"
#import "Sport.h"
#import "Venue.h"
#import "S3Tools2.h"
#import "EventTag.h"

@interface AddLockerPictureShareTVC : CustomBaseTVC

@property (nonatomic, strong) VaidenImageObject *viObject;
@property (nonatomic, strong) PlayingSeason *seasonObject;
@property (nonatomic, strong) PlayingYear *yearObject;
@property (nonatomic, strong) Sport *sportObject;
@property (nonatomic, strong) Venue *venueObject;
@property (nonatomic, strong) EventTag *eventObject;
@property (nonatomic, strong) NSMutableArray *contactsToTagArray;
@property (nonatomic, strong) NSMutableArray *statsTagsArray;
@property (nonatomic, strong) NSMutableArray *trainingTagsArray;

@property (nonatomic, strong) S3Tools2 *regImageSizeUploader;
@property (nonatomic, strong) S3Tools2 *regImageRetinaSizeUploader;
@property (nonatomic, strong) S3Tools2 *thumbnailImageSizeUploader;
@property (nonatomic, strong) S3Tools2 *thumbnailImageRetinaSizeUploader;
@property (nonatomic, strong) NSString *imageFileName;
@property (nonatomic, strong) NSString *imageFileNameBaseString;

@end
