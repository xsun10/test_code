//
//  UIView+Bubbles.h
//  Vaiden
//
//  Created by James Chung on 12/23/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Bubbles)

+ (UIView*)makeBubbleWithWidth:(CGFloat)w font:(UIFont*)f text:(NSString*)s caps:(CGSize)caps padding:(CGFloat*)padTRBL withPosition:(NSString *)position;

@end
