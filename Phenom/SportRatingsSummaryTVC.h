//
//  SportRatingsSummaryTVC.h
//  Vaiden
//
//  Created by James Chung on 10/4/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlayerSport.h"
#import "RatePlayerSportTVC.h"
#import "CustomBaseTVC.h"

@interface SportRatingsSummaryTVC : CustomBaseTVC <RatePlayerSportTVC_Delegate>

@property (nonatomic, strong) PlayerSport *sport;
@property (nonatomic, strong) Player *playerToBeRated;
@end
