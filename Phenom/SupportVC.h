//
//  SupportVC.h
//  Vaiden
//
//  Created by Turbo on 7/22/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface SupportVC : UIViewController <MBProgressHUDDelegate>
{
    MBProgressHUD *HUD;
}

@end
