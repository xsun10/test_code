//
//  MapViewVC.h
//  Phenom
//
//  Created by James Chung on 5/16/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "CustomBaseVC.h"

@interface MapViewVC : CustomBaseVC <MKMapViewDelegate>

@property (nonatomic, strong) IBOutlet MKMapView *mapView;

@end
