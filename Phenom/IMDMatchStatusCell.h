//
//  IMDMatchStatusCell.h
//  Vaiden
//
//  Created by James Chung on 12/26/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IMDMatchStatusCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *circleIconView;
@property (weak, nonatomic) IBOutlet UILabel *matchStatusLabel;
@end
