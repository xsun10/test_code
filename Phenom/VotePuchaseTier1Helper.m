//
//  VotePuchaseTier1Helper.m
//  Vaiden
//
//  Created by Turbo on 8/4/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "VotePuchaseTier1Helper.h"

@implementation VotePuchaseTier1Helper

+ (VotePuchaseTier1Helper *)sharedInstance {
    static dispatch_once_t once;
    static VotePuchaseTier1Helper * sharedInstance;
    dispatch_once(&once, ^{
        NSSet * productIdentifiers = [NSSet setWithObjects:
                                      @"com.vaiden.vaidenapp.purchase",
                                      @"com.vaiden.vaidenapp.purchase2",
                                      @"com.vaiden.vaidenapp.purchase3",
                                      @"com.vaiden.vaidenapp.boost",
                                      @"com.vaiden.vaidenapp.boost2",
                                      @"com.vaiden.vaidenapp.boost3",
                                      nil];
        sharedInstance = [[self alloc] initWithProductIdentifiers:productIdentifiers];
    });
    return sharedInstance;
}

@end
