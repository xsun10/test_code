//
//  PickupMatchParticipantsCell.h
//  Phenom
//
//  Created by James Chung on 4/10/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PickupMatchParticipantsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *playerImage;
@property (weak, nonatomic) IBOutlet UILabel *playerUsernameLabel;
@property (weak, nonatomic) IBOutlet UILabel *playerExperienceLevelLabel;
@property (weak, nonatomic) IBOutlet UILabel *status;

@end
