//
//  IndividualMatchScore.m
//  Phenom
//
//  Created by James Chung on 7/29/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "IndividualMatchScore.h"
#import "IndividualMatchRoundScore.h"

@implementation IndividualMatchScore

- (IndividualMatchScore *) initWithMatchScore
{
    self = [super init];
    
    if (self) {
        _roundScoresIndMatch = [[NSMutableArray alloc] init];
 //       _scoreConfirmedBy = 0;
 //       _scoreDisputedBy = 0;
 //       _scoreRecordedBy = 0;
 //       _version = 0;
   
        
    }
    
    return self;
}

- (void)addRoundScore:(NSInteger)numRound
           forPlayer1:(NSInteger)userIDPlayer1
     withScorePlayer1:(NSInteger)scorePlayer1
           forPlayer2:(NSInteger)userIDPlayer2
     withScorePlayer2:(NSInteger)scorePlayer2
{
    IndividualMatchRoundScore *roundScore = [[IndividualMatchRoundScore alloc] initWithRoundScore:numRound
                                                                                     scorePlayer1:scorePlayer1
                                                                                withUserIDPlayer1:userIDPlayer1
                                                                                     scorePlayer2:scorePlayer2
                                                                                withUserIDPlayer2:userIDPlayer2];
    
    [self.roundScoresIndMatch addObject:roundScore];
}

// Certain sports have multiple rounds (ex tennis has multiple "sets" each of which we consider a round.
// A player in tennis can enter scores for each set.
// Sports like basketball only have one round.
//
// This function will return the net score.  So, if the sport has 1 round, then the score for that round will
// be returned.  If the sport has multiple rounds (ex sets in Tennis), then the number of rounds won / lost by
// each user will be returned.

- (NSMutableDictionary *)getNetScore
{
    NSMutableDictionary *results = [[NSMutableDictionary alloc] init];
    
    NSInteger player1TotalScore = 0;
    NSInteger player2TotalScore = 0;
    NSInteger player1ID = 0;
    NSInteger player2ID = 0;
    
    
    if ([self.roundScoresIndMatch count] > 1) {
        
        for (IndividualMatchRoundScore *score in self.roundScoresIndMatch) {
            player1ID = score.userIDPlayer1;
            player2ID = score.userIDPlayer2;
            
            if (score.scorePlayer1 > score.scorePlayer2) {
                player1TotalScore ++;
            } else if (score.scorePlayer2 > score.scorePlayer1) {
                player2TotalScore ++;
            } else if (score.scorePlayer1 == score.scorePlayer2) {
                player1TotalScore ++;
                player2TotalScore ++;
            }
            
        }
    } else {
        // only 1 round / set
        IndividualMatchRoundScore *imrs = [self.roundScoresIndMatch objectAtIndex:0];
        player1TotalScore = imrs.scorePlayer1;
        player2TotalScore = imrs.scorePlayer2;
    }
    
    [results setValue:[NSNumber numberWithInt:player1TotalScore] forKey:@"player1_score"];
    [results setValue:[NSNumber numberWithInt:player2TotalScore] forKey:@"player2_score"];
    [results setValue:[NSNumber numberWithInt:player1ID] forKey:@"player1_id"];
    [results setValue:[NSNumber numberWithInt:player2ID] forKey:@"player2_id"];
    
    
    
    return  results;
}

@end
