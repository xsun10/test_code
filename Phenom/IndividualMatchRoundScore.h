//
//  IndividualMatchRoundScore.h
//  Phenom
//
//  Created by James Chung on 7/23/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RoundScore.h"

@interface IndividualMatchRoundScore : RoundScore

@property (nonatomic) NSInteger scorePlayer1;
@property (nonatomic) NSInteger scorePlayer2;
@property (nonatomic) NSInteger userIDPlayer1;
@property (nonatomic) NSInteger userIDPlayer2;

- (IndividualMatchRoundScore *) initWithRoundScore:(NSInteger)round
                                      scorePlayer1:(NSInteger)scorePlayer1
                                 withUserIDPlayer1:(NSInteger)userIDPlayer1
                                      scorePlayer2:(NSInteger)scorePlayer2
                                 withUserIDPlayer2:(NSInteger)userIDPlayer2;


@end
