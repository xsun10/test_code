//
//  NewsfeedMatchHeadingCVCell.h
//  Vaiden
//
//  Created by James Chung on 11/5/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsfeedMatchHeadingCVCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *playerPic;
@property (weak, nonatomic) IBOutlet UILabel *playerUsername;

@property (weak, nonatomic) IBOutlet UIButton *picButton;
@end
