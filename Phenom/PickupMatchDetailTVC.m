//
//  PickupMatchDetailTVC.m
//  Phenom
//
//  Created by James Chung on 4/23/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "PickupMatchDetailTVC.h"
#import "IOSRequest.h"
#import "PickupMatchParticipantsTVC.h"
#import "UserPreferences.h"
#import "Venue+MKAnnotation.h"
#import "MapOverlayVC.h"
//#import "PickupMatchPlayersCVCell.h"
#import "AFNetworking.h"
#import "S3Tools.h"
#import "UIImage+ProportionalFill.h"
#import "PlayerDetailTVC.h"
#import "UIButton+RoundBorder.h"
#import "UIColor+VaidenColors.h"
#import "MatchHistoryTVC.h"
#import "MatchNews.h"
#import "MatchPlayerNews.h"
#import "MyHelpers.h"
#import "UIView+Manipulate.h"
#import "NSDate+Utilities.h"

#import "PMDMatchStatusCell.h"
#import "PMDMatchDetailsCell.h"
#import "PMDOtherDetailsCell.h"
#import "PMDSessionUserStatusCell.h"
#import "PMDLatestNewsCell.h"
#import "PMDMatchMessageCell.h"
#import <EventKit/EventKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import "VenueDetailTVC.h"



@interface PickupMatchDetailTVC () <UIActionSheetDelegate>

@property (strong, nonatomic) PickupMatch *pickupMatch;

@property (weak, nonatomic) IBOutlet UIImageView *pickupHeadlineImage;

@property (weak, nonatomic) IBOutlet UILabel *sportNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *experienceLabel;
@property (weak, nonatomic) IBOutlet UILabel *venueNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *streetLabel;
@property (weak, nonatomic) IBOutlet UILabel *cityStateLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UIImageView *statusIcon;
@property (weak, nonatomic) IBOutlet UILabel *matchTypeLabel;
@property (weak, nonatomic) IBOutlet UIView *matchStatusBubble;

@property (weak, nonatomic) IBOutlet UIImageView *playerStatusIcon;
@property (weak, nonatomic) IBOutlet UILabel *playerStatus;

@property (weak, nonatomic) IBOutlet UIImageView *sportIcon;

@property (weak, nonatomic) IBOutlet UIButton *actionButton;
@property (weak, nonatomic) IBOutlet UIButton *action2Button;

@property (weak, nonatomic) IBOutlet UILabel *privateLabel;
@property (weak, nonatomic) IBOutlet UIImageView *lockIcon;

@property (weak, nonatomic) IBOutlet UITableViewCell *confirmedCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *invitedCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *minimumPlayersCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *maximumPlayersCell;

@property (weak, nonatomic) IBOutlet UITextView *messageLabel;
@property (strong, nonatomic) UserPreferences *preferences;
@property (weak, nonatomic) IBOutlet UIImageView *mapImage;

@property (strong, nonatomic) NSString *responseType;

@property (weak, nonatomic) IBOutlet UIButton *matchCreatorHandle;
@property (weak, nonatomic) IBOutlet UIImageView *calendarIcon;
@property (weak, nonatomic) IBOutlet UIImageView *experienceIcon;
@property (weak, nonatomic) IBOutlet UIImageView *genderIcon;
@property (weak, nonatomic) IBOutlet UILabel *genderLabel;

@property (nonatomic, strong) UIView *tempView;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIImageView *bulbIcon;

// Top buttons
@property (weak, nonatomic) IBOutlet UIView *button1Background;
@property (weak, nonatomic) IBOutlet UIView *button2Background;
@property (weak, nonatomic) IBOutlet UIView *button3Background;
@property (weak, nonatomic) IBOutlet UIView *button4Background;
@property (weak, nonatomic) IBOutlet UIImageView *button1Icon;
@property (weak, nonatomic) IBOutlet UIImageView *button2Icon;
@property (weak, nonatomic) IBOutlet UIImageView *button3Icon;
@property (weak, nonatomic) IBOutlet UIImageView *button4Icon;
@property (weak, nonatomic) IBOutlet UILabel *button1Label;
@property (weak, nonatomic) IBOutlet UILabel *button2Label;
@property (weak, nonatomic) IBOutlet UILabel *button3Label;
@property (weak, nonatomic) IBOutlet UILabel *button4Label;
@property (weak, nonatomic) IBOutlet UILabel *button2SubLabel;
@property (weak, nonatomic) IBOutlet UILabel *button1SubLabel;

@property (nonatomic) NSUInteger sessionUserNumericStatusCode;

@property (nonatomic, strong) MatchNews *matchNews; // can be type matchPlayerNews as well
@property (weak, nonatomic) IBOutlet UIImageView *newsPic;
@property (weak, nonatomic) IBOutlet UILabel *newsNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *newsSubheadlineLabel;
@property (weak, nonatomic) IBOutlet UILabel *newsDatetimeLabel;

@property (nonatomic) NSUInteger pageDisplayConfiguration;
@property (nonatomic) BOOL canShowVenue;
@property (nonatomic) BOOL isVenueTemporary;
@property (nonatomic) BOOL readyToLoadAll;
@property (nonatomic, strong) UIRefreshControl *refreshControl;
@property (nonatomic) NSInteger selectedPlayerToDisplayDetail;

@end

@implementation PickupMatchDetailTVC

#define CONFIRMED 2
#define DECLINED 3

#define SESSION_USER_IS_MATCH_CREATOR 0
#define SESSION_USER_IS_CONFIRMED 1
#define SESSION_USER_IS_UNCONFIRMED 2

#define CTE_MATCH_STATUS_CELL 0
#define CTE_HEADER_INFO_CELL 1
#define CTE_MATCH_DETAILS_CELL 2
#define CTE_OTHER_MATCH_DETAILS_CELL 3
#define CTE_SESSION_USER_STATUS_CELL 4
#define CTE_PLAYERS_CELL 5
#define CTE_LATEST_NEWS_CELL 6
#define CTE_MATCH_MESSAGE_CELL 7

//#define CELL_CONFIG_DEFAULT 0



// View states (As defined Above and in excel sheet)
#define VIEW_STATE_1 0
#define VIEW_STATE_2 1
#define VIEW_STATE_3 2
#define VIEW_STATE_4 3
#define VIEW_STATE_5 4
#define VIEW_STATE_6 5


// For the action sheet
#define CANCEL_BUTTON_ACTIONSHEET @"Cancel "

#define TITLE_OF_ACTIONSHEET_FOR_JOIN_MATCH @"By joining you agree to show up to this match on time at the designated location"
#define JOIN_MATCH @"Join Match"
#define JOIN_MATCH_ADD_TO_CALENDAR @"Join Match + Add To Calendar"

#define TITLE_OF_ACTIONSHEET_FOR_DECLINE_MATCH @"By declining, you are notifying the other players that you do not wish to play in the match"
#define DECLINE_MATCH @"Decline Match"

#define TITLE_OF_ACTIONSHEET_FOR_CANCEL_MATCH @"By cancelling, you are voiding this match.  Other players will be notified of the match cancellation"
#define CANCEL_MATCH @"Cancel Match"

#define TITLE_OF_ACTIONSHEET_FOR_WITHDRAW_MATCH @"By withdrawing, you are notifying the other players that you no longer wish to play in the match"
#define WITHDRAW_MATCH @"Withdraw From Match"

#define TITLE_OF_ACTIONSHEET_FOR_VOID_MATCH @"Voiding this match will render it inactive"
#define VOID_MATCH @"Void Match"

#define TITLE_OF_ACTIONSHEET_FOR_CONFIRM_MATCH @"Set your match to 'Ready To Play' if enough players have told you they would play, but are unable to RSVP"
#define CONFIRM_MATCH @"Ready To Play"




- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
    

    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(refreshView) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshControl];
    
    
  //  self.pageDisplayConfiguration = CELL_CONFIG_DEFAULT;
    
    // workaround for group table view section header
    self.tableView.contentInset = UIEdgeInsetsMake(-2.0f, 0.0f, 0.0f, 0.0);
}


- (void)refreshView
{
    [self loadMatchData];
    [self.refreshControl endRefreshing];
}


// workaround for group table view section header
- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
   // if (section == 0)
        return 1.0f;
 //   return 32.0f;
}

- (NSString*) tableView:(UITableView *) tableView titleForHeaderInSection:(NSInteger)section
{/*
    if (section == 0) {
        return nil;
    } else {
        // return some string here ...
    }*/
    return nil;
}

// end workaround




- (void)viewWillAppear:(BOOL)animated
{
  //  [self zoomMapViewToFitAnnotations:self.mapView animated:animated];
    //or maybe you would do the call above in the code path that sets the annotations array
    [self refreshView];
    
    [self.tempView removeFromSuperview];
    self.tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    self.tempView.backgroundColor = [UIColor lightGrayColor];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] init];
    [self.activityIndicator setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width/2.0, [UIScreen mainScreen].bounds.size.height/2.0 - 50)];
    [self.activityIndicator startAnimating];
    [self.tempView addSubview:self.activityIndicator];
    
    
    [self.view addSubview:self.tempView];

    
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self setButtonWidthsAndText]; // has to be called here...
}

- (void)loadMatchData
{
    [IOSRequest fetchPickupMatchDetails:self.matchID
                           onCompletion:^(NSDictionary *object) {
                               
                               // For match history obj match_history_recent_object
                               id historyObj = object[@"match_history_recent_object"];
                               
                               NSDate *historyPostDate = [MyHelpers getDateTimeFromString:historyObj[@"ts"]];
                               
                               if ([historyObj[@"player_id"] integerValue] == 0) {
                                   self.matchNews = [[MatchNews alloc] initWithDetails:self.pickupMatch
                                                                              postDate:[historyPostDate toLocalTime]
                                                                       headLineMessage:self.pickupMatch.matchName
                                                                        generalMessage:historyObj[@"general_message"]];
                                   
                               } else {
                                   Player *postPlayer = [[Player alloc] initWithPlayerDetails:[historyObj[@"player_id"] integerValue]
                                                                                  andUserName:historyObj[@"username"]];
                                   [postPlayer setProfileBaseString:historyObj[@"profile_pic_string"]];
                                   
                                   self.matchNews = [[MatchPlayerNews alloc] initWithDetails:self.pickupMatch
                                                                                    postDate:[historyPostDate toLocalTime]
                                                                             headLineMessage:historyObj[@"username"]
                                                                              generalMessage:historyObj[@"general_message"]
                                                                                   forPlayer:postPlayer
                                                                                 withMessage:historyObj[@"player_message"]];
                               }
                               
                               // End match history object entry

                               NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
                               [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                               NSDate *dateTime = [[formatter dateFromString:object[@"date_time"]] toLocalTime];
                               
                               Venue *venue = [[Venue alloc] initVenueWithName:object[@"venue_name"]
                                                                    andVenueID:[object[@"venue_id"] integerValue]
                                                                    andAddress:object[@"venue_street"]
                                                                       andCity:object[@"venue_city"]
                                                                  andStateName:object[@"venue_state"]
                                                                    andStateID:[object[@"venue_state_id"] integerValue]
                                                                   andLatitude:[object[@"latitude"] floatValue]
                                                                  andLongitude:[object[@"longitude"] floatValue]
                                                                andDescription:object[@"description"]
                                                                    andCreator:[object[@"venue_creator_id"] integerValue]];
                               
                               Sport *sport = [[Sport alloc] initWithSportDetails:object[@"sport_name"]
                                                                          andType:object[@"sport_type"]];
                               
                               NSMutableArray *competitorArray = [PickupMatch makeCompetitorArray:object[@"competitors"] forSportName:object[@"sport_name"] andType:object[@"sport_type"]];
                               
                               self.pickupMatch = [[PickupMatch alloc]
                                                      initMatchWithDetails:[object[@"match_id"] integerValue]
                                                      andMatchName:object[@"match_name"]
                                                      isPrivate:[object[@"is_private"] boolValue]
                                                      withSport:sport
                                                      withMessage:object[@"message"]
                                                      onDateTime:dateTime
                                                      atVenue:venue
                                                      createdByUser:[object[@"creator_id"] integerValue]
                                                      withUsername:[PickupMatch getUsernameForID:[object[@"creator_id"] integerValue] inArray:competitorArray]
                                                      andProfileStr:object[@"creator_profile_pic_string"]
                                                      minCompetitors:[object[@"min_competitors"] integerValue]
                                                      withCompetitors:competitorArray
                                                      withExperience:[NSMutableArray arrayWithArray:object[@"experience"]]
                                                      andMatchType:[[MatchType alloc] initWithMatchType:[object[@"match_type_id"] integerValue] withDescription:object[@"match_type_description"]]
                                                      andActive:[object[@"active"] boolValue]
                                                   andMatchStatus:object[@"match_status"]];
                               [self.pickupMatch setGender:object[@"gender"]];
                               
                               self.canShowVenue = [object[@"can_show_venue"] boolValue];
                               self.isVenueTemporary = [object[@"is_venue_temporary"] boolValue];
                               
                                   dispatch_async(dispatch_get_main_queue(), ^{
                                       
                                       [self populateFields];
                                       [self setHistorySection];
                                       [self.activityIndicator stopAnimating];
                    //                   self.pageControl.numberOfPages = self.pickupMatch.minCompetitors;
                    //                   [self.pageControl setCurrentPage:0];
                                       self.tempView.hidden = YES;
                                       self.readyToLoadAll = YES;
                                       [self setDisplayConfiguration];
                                       [self.tableView reloadData];
                                   });

                           }];
   
}

// most of this prob is not used anymore
- (void)populateFields
{
    self.pickupHeadlineImage.image = [UIImage imageNamed:@"Pickup BBall Headline"];
    self.calendarIcon.image = [UIImage imageNamed:@"Calendar Icon 2"];
    self.experienceIcon.image = [UIImage imageNamed:@"Experience Icon"];
    
    self.bulbIcon.image = [[UIImage imageNamed:@"Bulb Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.bulbIcon.tintColor = [UIColor purpleColor];
    self.playerStatus.textColor = [UIColor purpleColor];
    
    [self.matchStatusBubble makeCircleWithColor:[UIColor whiteColor] andRadius:5];
    if (self.pickupMatch.private) {
        self.privateLabel.hidden = NO;
        self.lockIcon.hidden = NO;
        self.lockIcon.image = [[UIImage imageNamed:@"Lock 2"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        self.lockIcon.tintColor = [UIColor whiteColor];
    } else {
        self.privateLabel.hidden = YES;
        self.lockIcon.hidden = YES;
    }
    
    [self.actionButton makeRoundBorderWithColor:[UIColor cyanColor]];
    [self.action2Button makeRoundBorderWithColor:[UIColor cyanColor]];
    
    [IOSRequest fetchUserInfo:self.pickupMatch.matchCreatorID
                bySessionUser:[self.preferences getUserID]
                 onCompletion:^(NSDictionary *valuesContainer) {
        dispatch_async(dispatch_get_main_queue(), ^{
  //          self.matchHeadingLabel.text = valuesContainer[@"username"];
        });
        
    }];
    self.messageLabel.text  = [NSString stringWithFormat:@"\"%@\"", self.pickupMatch.message];
    self.sportNameLabel.text = self.pickupMatch.matchName;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMMM d, YYYY"];
    self.dateLabel.text = [dateFormatter stringFromDate:self.pickupMatch.dateTime];
    
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    [timeFormatter setDateFormat:@"h:mm a"];
    self.timeLabel.text = [timeFormatter stringFromDate: self.pickupMatch.dateTime];
    
    self.venueNameLabel.text = self.pickupMatch.venue.venueName;
    
    if ([self.pickupMatch.venue.streetAddress length] > 0) {
        self.streetLabel.text = self.pickupMatch.venue.streetAddress;
        
        if ([self.pickupMatch.venue.city length] > 0 && [self.pickupMatch.venue.stateName length] > 0) {
            self.cityStateLabel.text = [NSString stringWithFormat:@"%@, %@", self.pickupMatch.venue.city, self.pickupMatch.venue.stateName ];
        } else if ([self.pickupMatch.venue.city length] > 0 && [self.pickupMatch.venue.stateName length] == 0) {
            self.cityStateLabel.text = self.pickupMatch.venue.city;
        } else if ([self.pickupMatch.venue.city length] == 0 && [self.pickupMatch.venue.stateName length] > 0) {
            self.cityStateLabel.text = self.pickupMatch.venue.stateName;
        }
    } else {
        if ([self.pickupMatch.venue.city length] > 0 && [self.pickupMatch.venue.stateName length] > 0) {
            self.streetLabel.text = [NSString stringWithFormat:@"%@, %@", self.pickupMatch.venue.city, self.pickupMatch.venue.stateName ];
        } else if ([self.pickupMatch.venue.city length] > 0 && [self.pickupMatch.venue.stateName length] == 0) {
            self.streetLabel.text = self.pickupMatch.venue.city;
        } else if ([self.pickupMatch.venue.city length] == 0 && [self.pickupMatch.venue.stateName length] > 0) {
            self.streetLabel.text = self.pickupMatch.venue.stateName;
        }

    }
    
    
    
    self.statusLabel.text = [self.pickupMatch getMatchStatus];

//    [self setStatusLabels];
    
    self.sportIcon.image = [[self.pickupMatch.sport getSportIcon] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.sportIcon.tintColor = [UIColor darkGrayColor];
    self.genderLabel.text = [self.pickupMatch.gender capitalizedString];
    

    
    
    self.experienceLabel.text = [[PickupMatch getExperienceString:self.pickupMatch] capitalizedString];
    [self.matchCreatorHandle setTitle:self.pickupMatch.matchCreatorUsername forState:UIControlStateNormal];
    
    // set the buttons
    self.confirmedCell.textLabel.text = [NSString stringWithFormat:@"%ld Confirmed Players", [self.pickupMatch getNumConfirmed]];
    
    self.invitedCell.textLabel.text = [NSString stringWithFormat:@"%ld Invited Players", [self.pickupMatch.competitorArray count]];
    
 //   self.minimumPlayersCell.detailTextLabel.text = [NSString stringWithFormat:@"%d", self.pickupMatch.minCompetitors];
 //   self.maximumPlayersCell.detailTextLabel.text = [NSString stringWithFormat:@"%d", self.pickupMatch.maxCompetitors];
    self.matchTypeLabel.text = [NSString stringWithFormat:@"%@ %@ Pickup Match", self.pickupMatch.matchType.description, [self.pickupMatch.sport.sportName capitalizedString]];
    
    
  
}

- (void)setHistorySection
{
    NSDateFormatter *historyDateFormatter = [[NSDateFormatter alloc]init];
    [historyDateFormatter setDateFormat:@"MMMM d, YYYY h:mm a"];
    self.newsDatetimeLabel.text = [historyDateFormatter stringFromDate:self.matchNews.newsDate];
    
    if ([self.matchNews isKindOfClass:[MatchPlayerNews class]]) {
        MatchPlayerNews *mpn = (MatchPlayerNews *)self.matchNews;
        self.newsNameLabel.text = mpn.player.userName;
        NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                baseString:mpn.player.profileBaseString];
        
        [self.newsPic setImageWithURL:[NSURL URLWithString:url]
                        placeholderImage:[UIImage imageNamed:@"avatar square"]];
        [UIImage makeRoundedImage:self.newsPic withRadius:20];
        
        self.newsSubheadlineLabel.text = mpn.matchGeneralMessage;
        
        
    } else {
        self.newsPic.image = [self.pickupMatch.sport getSportIcon];
        self.newsSubheadlineLabel.text = self.matchNews.matchGeneralMessage;
        self.newsNameLabel.text = self.pickupMatch.matchName;
        
    }
    
}

- (void)setButtonWidthsAndText
{

    if ([self.preferences getUserID] == self.pickupMatch.matchCreatorID) {
        // then show two buttons
        self.actionButton.hidden = NO;
        self.action2Button.hidden = YES;
        
        [self.actionButton setTitle:@"Cancel Match" forState:UIControlStateNormal];
        
  //      if ([self checkLoggedInUserConfirmed]) { // Note: also need to check if private/public
  //          // should be able to change response
  //          [self.actionButton setTitle:@"Cancel RSVP" forState:UIControlStateNormal];
  //      }
    
    } else {

        if ([self.pickupMatch checkLoggedInUserConfirmedBOOL]) { // Note: also need to check if private/public
            // show one button
            self.action2Button.hidden = NO;
            [self.action2Button setTitle:@"Decline Match" forState:UIControlStateNormal];
            self.actionButton.hidden = YES;
            
        } else {
            self.actionButton.hidden = NO;
            self.action2Button.hidden = NO;
            
            [self.actionButton setTitle:@"Join Match" forState:UIControlStateNormal];
            [self.action2Button setTitle:@"Decline Match" forState:UIControlStateNormal];
        }
        
        

        
        
        
    }
}


#define MINIMUM_ZOOM_ARC 0.014 //approximately 1 miles (1 degree of arc ~= 69 miles)
#define ANNOTATION_REGION_PAD_FACTOR 1.15
#define MAX_DEGREES_ARC 360
//size the mapView region to fit its annotations
- (void)zoomMapViewToFitAnnotations:(MKMapView *)mapView animated:(BOOL)animated
{
    NSArray *annotations = mapView.annotations;
    int count = [mapView.annotations count];
    if ( count == 0) { return; } //bail if no annotations
    
    //convert NSArray of id <MKAnnotation> into an MKCoordinateRegion that can be used to set the map size
    //can't use NSArray with MKMapPoint because MKMapPoint is not an id
    MKMapPoint points[count]; //C array of MKMapPoint struct
    for( int i=0; i<count; i++ ) //load points C array by converting coordinates to points
    {
        CLLocationCoordinate2D coordinate = [(id <MKAnnotation>)annotations[i] coordinate];
        points[i] = MKMapPointForCoordinate(coordinate);
    }
    //create MKMapRect from array of MKMapPoint
    MKMapRect mapRect = [[MKPolygon polygonWithPoints:points count:count] boundingMapRect];
    //convert MKCoordinateRegion from MKMapRect
    MKCoordinateRegion region = MKCoordinateRegionForMapRect(mapRect);
    
    //add padding so pins aren't scrunched on the edges
    region.span.latitudeDelta  *= ANNOTATION_REGION_PAD_FACTOR;
    region.span.longitudeDelta *= ANNOTATION_REGION_PAD_FACTOR;
    //but padding can't be bigger than the world
    if( region.span.latitudeDelta > MAX_DEGREES_ARC ) { region.span.latitudeDelta  = MAX_DEGREES_ARC; }
    if( region.span.longitudeDelta > MAX_DEGREES_ARC ){ region.span.longitudeDelta = MAX_DEGREES_ARC; }
    
    //and don't zoom in stupid-close on small samples
    if( region.span.latitudeDelta  < MINIMUM_ZOOM_ARC ) { region.span.latitudeDelta  = MINIMUM_ZOOM_ARC; }
    if( region.span.longitudeDelta < MINIMUM_ZOOM_ARC ) { region.span.longitudeDelta = MINIMUM_ZOOM_ARC; }
    //and if there is a sample of 1 we want the max zoom-in instead of max zoom-out
    if( count == 1 )
    {
        region.span.latitudeDelta = MINIMUM_ZOOM_ARC;
        region.span.longitudeDelta = MINIMUM_ZOOM_ARC;
    }
    [self.mapView setRegion:region animated:animated];
}



- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"Confirmed Individuals Segue"]){
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"status = %@", @"CONFIRMED"];
        NSArray *beginWithB = [self.pickupMatch.competitorArray filteredArrayUsingPredicate:predicate];
        
        PickupMatchParticipantsTVC *controller = (PickupMatchParticipantsTVC *)segue.destinationViewController;
        
        controller.playerList = beginWithB;
        controller.matchSport = self.pickupMatch.sport.sportName;
    } else if([segue.identifier isEqualToString:@"Invited Individuals Segue"]){
        
        PickupMatchParticipantsTVC *controller = (PickupMatchParticipantsTVC *)segue.destinationViewController;
        
        controller.playerList = [self.pickupMatch.competitorArray copy];
        controller.matchSport = self.pickupMatch.sport.sportName;
    } /* else if ([segue.identifier isEqualToString:@"RSVP Pickup Match Segue"]) {
        RSVPForMatchTVC *controller = (RSVPForMatchTVC *)segue.destinationViewController;
        controller.matchObject = self.pickupMatch;
        controller.acceptDecline = self.responseType;
    
    }*/ else if ([segue.identifier isEqualToString:@"large map view segue"]) {
        MapOverlayVC *controller = (MapOverlayVC *)segue.destinationViewController;
        controller.venue = self.pickupMatch.venue;
    } else if ([segue.identifier isEqualToString:@"Match Creator Profile Segue"]) {
        PlayerDetailTVC *controller = (PlayerDetailTVC *)segue.destinationViewController;
        controller.playerUserID = self.pickupMatch.matchCreatorID;
        controller.profilePicString = self.pickupMatch.matchCreatorProfileBaseString;
    } else if ([segue.identifier isEqualToString:@"History Segue"]) {
        MatchHistoryTVC *controller = segue.destinationViewController;
        controller.playerMatch = self.pickupMatch;
    } else if ([segue.identifier isEqualToString:@"Venue Detail Segue"]) {
        VenueDetailTVC *controller = segue.destinationViewController;
        controller.venueID = self.pickupMatch.venue.venueID;
    } else if ([segue.identifier isEqualToString:@"Player Detail Segue"]) {
        PlayerDetailTVC *controller = segue.destinationViewController;
        controller.playerUserID = self.selectedPlayerToDisplayDetail;
    }
}



/*
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self zoomMapViewToFitAnnotations:self.mapView animated:NO];
}
 */


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if ([[self.preferences getMatchNotificationsForceSegueAction] isEqualToString:@"PH"]) {
        [self.preferences setMatchNotificationsForceSegueWithAction:@"" andMatchID:0];
        [self performSegueWithIdentifier:@"History Segue" sender:self];
    } else {
        [self zoomMapViewToFitAnnotations:self.mapView animated:NO];
    }
}

- (void) reload
{
    
//    [self.mapView removeAnnotation:self.mapView.annotations];
//    [self.mapView addAnnotations:photos];
}

/*
- (void)assignMapImage
{
    NSArray *annotations = self.mapView.annotations;
    CLLocationCoordinate2D coordinate = [(id <MKAnnotation>)annotations[0] coordinate];
    NSString *staticMapUrl = [NSString stringWithFormat:@"http://maps.google.com/maps/api/staticmap?markers=color:red|%f,%f&%@&sensor=true",coordinate.latitude, coordinate.longitude,@"zoom=16&size=320x120"];
    NSURL *mapUrl = [NSURL URLWithString:[staticMapUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    self.mapImage.image = [UIImage imageWithData: [NSData dataWithContentsOfURL:mapUrl]];
    self.mapImage.userInteractionEnabled = YES;
    UITapGestureRecognizer *pgr = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self action:@selector(handleTap:)];
    //    pgr.delegate = self.view;
    [self.mapImage addGestureRecognizer:pgr];
    
    
    
}*/

- (void)handleTap:(UITapGestureRecognizer *)tapGestureRecognizer
{
    [self performSegueWithIdentifier:@"large map view segue" sender:self];
}

- (NSString *)getPlayerStatus
{
    
    NSString *sessionUserStatus = [self.pickupMatch getSessionUserStatus];
   
    if (self.pickupMatch.matchCreatorID == [self.preferences getUserID]) {
    
   //     [self showButtonsForMatchCreator];
        self.sessionUserNumericStatusCode = SESSION_USER_IS_MATCH_CREATOR;
    
    } else if ([sessionUserStatus isEqualToString:@"You have not RSVP'd for this match"]) {
        
   //     [self showButtonsForUnconfirmedMatchParticipant];
        self.sessionUserNumericStatusCode = SESSION_USER_IS_UNCONFIRMED;
        
    } else if ([sessionUserStatus isEqualToString:@"You are confirmed to play in this match"]) {
        
  //      [self showButtonsForConfirmedMatchParticipant];
        self.sessionUserNumericStatusCode = SESSION_USER_IS_CONFIRMED;
        
    } else if ([sessionUserStatus isEqualToString:@"You declined to play in this match"]) {
  //      [self showButtonsForUnconfirmedMatchParticipant];
        self.sessionUserNumericStatusCode = SESSION_USER_IS_UNCONFIRMED;
    }
    
 //   self.playerStatus.text = sessionUserStatus;
    return sessionUserStatus;

}


#define MATCH_NEWS_SECTION 3

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == MATCH_NEWS_SECTION)
        [self performSegueWithIdentifier:@"History Segue" sender:self];
}

/*
// Same thing as setting the match to ready
- (void)confirmMatchAction
{
    self.responseType = @"Confirm";
    [self performSegueWithIdentifier:@"RSVP Pickup Match Segue" sender:self];
}

- (void)cancelMatchAction
{
    self.responseType = @"Cancel";
    [self performSegueWithIdentifier:@"RSVP Pickup Match Segue" sender:self];
}

- (void)joinMatchAction
{
    self.responseType = @"Accept";
    [self performSegueWithIdentifier:@"RSVP Pickup Match Segue" sender:self];
}

- (void)declineMatchAction
{
    self.responseType = @"Withdraw";
    [self performSegueWithIdentifier:@"RSVP Pickup Match Segue" sender:self];   
}

- (void)withdrawRSVP
{
    self.responseType = @"Withdraw";
    [self performSegueWithIdentifier:@"RSVP Pickup Match Segue" sender:self];
}
*/

- (void)matchNewsAction
{
    [self performSegueWithIdentifier:@"History Segue" sender:self];
}
/******** End Button Display Conditions ***********/

#define FONT_SIZE 12.0f
#define CELL_CONTENT_WIDTH 260.0f
#define CELL_CONTENT_MARGIN 30.0f


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
     if (self.readyToLoadAll) {
         if ([self.pickupMatch.message length] > 0)
             return 8;
         else
             return 7;
     }
    
    return  0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger rowToShow = [self computeCellToDisplay:indexPath.row];
    
    switch (rowToShow) {
        case CTE_MATCH_STATUS_CELL:
            return 30.0;
            break;
        case CTE_HEADER_INFO_CELL:
            return [self computeHeightForHeaderCell] + 15;
            break;
        case CTE_MATCH_DETAILS_CELL:
            if (self.canShowVenue && !self.isVenueTemporary)
                //return 500; // 476
                return 288;
            else if (self.canShowVenue && self.isVenueTemporary) {
                
              //  return [self getVenueDescriptionContentHeight] + 310; // 290
                return 290 - 188;
                //    return 340;
            } else
                return 290 - 188; // 270
            break;

        case CTE_OTHER_MATCH_DETAILS_CELL:
           // return 205.0; // 185
            return 95;
            break;
            
        case CTE_SESSION_USER_STATUS_CELL:
            return 73; // 119
            break;
            
        case CTE_PLAYERS_CELL:
         //   return 222.0; // 184
            return 0;
            break;
            
        case CTE_LATEST_NEWS_CELL:
            return 0; // 135.0
            break;
            
        case CTE_MATCH_MESSAGE_CELL:
            if ([self.pickupMatch.message length] > 0)
                return 190.0; // 211.0
            else
                return 0.0;
            break;
    }
    
    return 0;
}

#define HEADER_NO_BUTTONS 230
#define HEADER_ONE_ROW_BUTTONS 290
#define HEADER_TWO_ROWS_BUTTONS 350

- (CGFloat)computeHeightForHeaderCell
{
    switch (self.pageDisplayConfiguration) {
        case VIEW_STATE_1:
//            return HEADER_TWO_ROWS_BUTTONS;
            return HEADER_ONE_ROW_BUTTONS;
            break;
        case VIEW_STATE_2:
            if (![self.pickupMatch checkLoggedInUserConfirmedBOOL]) {
                return HEADER_NO_BUTTONS;
            } else {
                // eventually might not show if over 10 days ago
                return HEADER_ONE_ROW_BUTTONS;
            }
            
            break;
        case VIEW_STATE_3:
            if (![self.pickupMatch checkLoggedInUserConfirmedBOOL]) {
                return HEADER_ONE_ROW_BUTTONS;
                
                
            } else {
              // return HEADER_TWO_ROWS_BUTTONS;
                return HEADER_ONE_ROW_BUTTONS;

            }
            break;
        case VIEW_STATE_4:
            return HEADER_NO_BUTTONS;
            
            break;
        case VIEW_STATE_5:
            if (![self.pickupMatch checkLoggedInUserConfirmedBOOL]) {
                return HEADER_ONE_ROW_BUTTONS;
                
                
            } else {
              //  return HEADER_TWO_ROWS_BUTTONS;
                return HEADER_ONE_ROW_BUTTONS;

                
            }
            break;
            
        case VIEW_STATE_6:
         //   return HEADER_TWO_ROWS_BUTTONS;
            return HEADER_ONE_ROW_BUTTONS;

            break;
            
        default:
            break;
    }
    return HEADER_NO_BUTTONS;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger selectedCode = [self computeCellToDisplay:indexPath.row];
    
    if (selectedCode == CTE_MATCH_STATUS_CELL) {
        static NSString *CellIdentifier = @"Match Status Cell";
        
        PMDMatchStatusCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        [cell.circleIconView makeCircleWithColorAndBackground:[UIColor whiteColor] andRadius:5];
        
        if ([[self.pickupMatch getMatchStatus] isEqualToString:@"This match needs players"]) {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"status = %@", @"ACCEPTED"];
            NSArray *matchParticipants = [self.pickupMatch.competitorArray filteredArrayUsingPredicate:predicate];
            
            cell.matchStatusLabel.text = [NSString stringWithFormat:@"%ld Joined %ld More Players Needed", [matchParticipants count], self.pickupMatch.minCompetitors - [matchParticipants count]];
            
        }  else
            cell.matchStatusLabel.text = [self.pickupMatch getMatchStatus];
        
        return cell;

    } else if (selectedCode == CTE_HEADER_INFO_CELL) {
        static NSString *CellIdentifier = @"Header Cell";
        PMDHeaderInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        
        cell.delegate = self;
        
        cell.sportLabel.text = [self.pickupMatch.sport.sportName capitalizedString];
        cell.matchNameLabel.text = self.pickupMatch.matchName;
        cell.matchTypeLabel.text = [NSString stringWithFormat:@"%@ %@ Pickup Match", self.pickupMatch.matchType.description, [self.pickupMatch.sport.sportName capitalizedString]];
        cell.sportIcon.image = [[self.pickupMatch.sport getSportIcon] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        cell.sportIcon.tintColor = [UIColor whiteColor];
        
    
    //    [self showButtonsForTableCell:cell];
        cell.isSessionUserConfirmed = [self.pickupMatch checkLoggedInUserConfirmedBOOL];
        
        if (self.pickupMatch.private)
            cell.privatePublicLabel.text = @"You must be invited to join this match";
        else
            cell.privatePublicLabel.text = @"Anyone can join this match";
        
        cell.backgroundPic.image = [self.pickupMatch getMatchDetailPageBackground];
        [cell configureButtonDisplay:self.pageDisplayConfiguration];
        [cell.buttonCollectionView reloadData];
        
        
        cell.pickupMatch = self.pickupMatch;
        cell.sessionUserID = [self.preferences getUserID];
        [cell initPlayers];
        [cell.playerCollectionView reloadData];
        cell.delegate = self;
  //      [cell.myBackgroundView makeEdgyDropshadow:[UIColor whiteColor]];
        
        return cell;
        
    } else if (selectedCode == CTE_MATCH_DETAILS_CELL) {
        static NSString *CellIdentifier = @"Match Details Cell";
        PMDMatchDetailsCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        
        NSString *dateText;
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"MMMM d, YYYY"];
        dateText = [dateFormatter stringFromDate:self.pickupMatch.dateTime];
        
        NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
        [timeFormatter setDateFormat:@"h:mm a"];
        
        cell.dateLabel.text = dateText;
        cell.timeLabel.text = [timeFormatter stringFromDate:self.pickupMatch.dateTime];
        
//        cell.dateTimeIconImage.image = [[UIImage imageNamed:@"Calendar Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
 //       cell.dateTimeIconImage.tintColor = [UIColor whiteColor];
        
      
        
        if (self.canShowVenue && !self.isVenueTemporary) {
            cell.venueNameLabel.text = self.pickupMatch.venue.venueName;
            
            if ([self.pickupMatch.venue.streetAddress length] > 0) {
                cell.streetNameLabel.text = self.pickupMatch.venue.streetAddress;
                
                if ([self.pickupMatch.venue.city length] > 0 && [self.pickupMatch.venue.stateName length] > 0) {
                    cell.cityStateLabel.text = [NSString stringWithFormat:@"%@, %@", self.pickupMatch.venue.city, self.pickupMatch.venue.stateName ];
                } else if ([self.pickupMatch.venue.city length] > 0 && [self.pickupMatch.venue.stateName length] == 0) {
                    cell.cityStateLabel.text = self.pickupMatch.venue.city;
                } else if ([self.pickupMatch.venue.city length] == 0 && [self.pickupMatch.venue.stateName length] > 0) {
                    cell.cityStateLabel.text = self.pickupMatch.venue.stateName;
                }
            } else {
                if ([self.pickupMatch.venue.city length] > 0 && [self.pickupMatch.venue.stateName length] > 0) {
                    cell.streetNameLabel.text = [NSString stringWithFormat:@"%@, %@", self.pickupMatch.venue.city, self.pickupMatch.venue.stateName ];
                } else if ([self.pickupMatch.venue.city length] > 0 && [self.pickupMatch.venue.stateName length] == 0) {
                    cell.streetNameLabel.text = self.pickupMatch.venue.city;
                } else if ([self.pickupMatch.venue.city length] == 0 && [self.pickupMatch.venue.stateName length] > 0) {
                    cell.streetNameLabel.text = self.pickupMatch.venue.stateName;
                } else {
                    cell.streetNameLabel.text = [NSString stringWithFormat:@"Longitude %f", self.pickupMatch.venue.longitude];
                    cell.cityStateLabel.text = [NSString stringWithFormat:@"Latitude %f", self.pickupMatch.venue.latitude];
                }
                
            }
            
            if (cell.venueImage.tag != 1) {
                [self.mapView removeAnnotations:self.mapView.annotations];
                [self.mapView addAnnotation:self.pickupMatch.venue];

           // [self assignMapImage];

            
            //   if (self.individualMatch.venue.venueImageThumb == nil) {
                NSArray *annotations = self.mapView.annotations;
                CLLocationCoordinate2D coordinate = [(id <MKAnnotation>)annotations[0] coordinate];
                NSString *staticMapUrl = [NSString stringWithFormat:@"http://maps.google.com/maps/api/staticmap?markers=color:red|%f,%f&%@&sensor=true",coordinate.latitude, coordinate.longitude,@"zoom=16&size=320x158"];
                NSURL *mapUrl = [NSURL URLWithString:[staticMapUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
                cell.venueImage.image = [UIImage imageWithData: [NSData dataWithContentsOfURL:mapUrl]];
                cell.venueImage.userInteractionEnabled = YES;
                UITapGestureRecognizer *pgr = [[UITapGestureRecognizer alloc]
                                           initWithTarget:self action:@selector(handleTap:)];
            //    pgr.delegate = self.view;
                [cell.venueImage addGestureRecognizer:pgr];
                cell.venueImage.tag = 1;
            }
            
        } else if (self.canShowVenue && self.isVenueTemporary)  {
            cell.venueImage.hidden = YES;
         //   cell.venueNameLabel.hidden = YES;
            cell.streetNameLabel.hidden = YES;
            cell.cityStateLabel.hidden = YES;
            cell.detailsButton.hidden = YES;
            
            cell.venueNameLabel.text = self.pickupMatch.venue.venueName;
            /*
            UILabel *venueName = [[UILabel alloc] initWithFrame:CGRectMake(30, 200, 260, 25)];
            venueName.text = self.pickupMatch.venue.venueName;
            venueName.font = [UIFont systemFontOfSize:20];
            venueName.textColor = [UIColor darkGrayColor];
            
            UILabel *cityState = [[UILabel alloc] initWithFrame:CGRectMake(30, 225, 260, 21)];
            cityState.text = [NSString stringWithFormat:@"%@, %@", self.pickupMatch.venue.city, self.pickupMatch.venue.stateName];
            cityState.font = [UIFont systemFontOfSize:14];
            cityState.textColor = [UIColor lightGrayColor];
            
            UILabel *venueDescriptionTitle = [[UILabel alloc] initWithFrame:CGRectMake(30, 255, 260, 20)];
            venueDescriptionTitle.text = @"Venue Description";
            venueDescriptionTitle.font = [UIFont systemFontOfSize:15];
            venueDescriptionTitle.textColor = [UIColor darkGrayColor];
            
            UILabel *venueDescription = [[UILabel alloc] initWithFrame:CGRectMake(30, 275, 260, [self getVenueDescriptionContentHeight])];
            venueDescription.text = self.pickupMatch.venue.description;
            venueDescription.font = [UIFont systemFontOfSize:FONT_SIZE];
            venueDescription.textColor = [UIColor lightGrayColor];
            venueDescription.numberOfLines = 0;
            venueDescription.lineBreakMode = NSLineBreakByWordWrapping;
            
            
            
            [cell addSubview:venueName];
            [cell addSubview:cityState];
            [cell addSubview:venueDescriptionTitle];
            [cell addSubview:venueDescription];*/
            
        } else {
            // session user must not have rights to view the location so hide it.
            cell.venueImage.hidden = YES;
         //   cell.venueNameLabel.hidden = YES;
            cell.streetNameLabel.hidden = YES;
            cell.cityStateLabel.hidden = YES;
            cell.detailsButton.hidden = YES;
            /*
            UILabel *tempLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 200, 280, 40)];
            tempLabel.text = @"This venue is private and can only be viewed by match participants";
            tempLabel.font = [UIFont systemFontOfSize:13];
            tempLabel.textColor = [UIColor midGray];
            tempLabel.textAlignment = NSTextAlignmentCenter;
            tempLabel.numberOfLines = 2;
            
            [cell addSubview:tempLabel];
            */
            
            cell.venueNameLabel.text = @"This venue is private";
            
        }
        
        
    //    self.genderIcon.image = [UIImage imageNamed:@"Gender Icon"];
    //    self.genderLabel.text = [self.pickupMatch.gender capitalizedString];
        
        
    //    self.experienceLabel.text = [[PickupMatch getExperienceString:self.pickupMatch] capitalizedString];
        cell.locationIconImage.image = [[UIImage imageNamed:@"Location_icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [cell.locationIconImage setTintColor:[UIColor colorWithRed:34/255.0 green:34/255.0 blue:34/255.0 alpha:1.0]];
        
        [cell.myBackgroundView makeEdgyDropshadow:[UIColor whiteColor]];
        
        return cell;
        
    } else if (selectedCode == CTE_OTHER_MATCH_DETAILS_CELL) {
        static NSString *CellIdentifier = @"Other Details Cell";
        PMDOtherDetailsCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        
   //     cell.genderIcon.image = [[Player getIconForGender:self.pickupMatch.gender] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
   //     cell.genderIcon.tintColor = [UIColor darkGrayColor];
        
        cell.genderLabel.text = [self.pickupMatch.gender capitalizedString];
        
  //      cell.requiredExperienceIcon.image = [UIImage imageNamed:@"Experience Icon"];
        cell.experienceLabel.text = [[PickupMatch getExperienceString:self.pickupMatch] capitalizedString];
        [cell.myBackgroundView makeEdgyDropshadow:[UIColor whiteColor]];
        
        return cell;
    } else if (selectedCode == CTE_SESSION_USER_STATUS_CELL) {
        static NSString *CellIdentifier = @"Session User Status Cell";
        PMDSessionUserStatusCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        
        [cell.statusIconBackground makeCircleWithColorAndBackground:[UIColor purpleColor] andRadius:12];
        cell.statusIconImage.image = [[UIImage imageNamed:@"Bulb Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        cell.statusIconImage.tintColor = [UIColor whiteColor];
        
        NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                baseString:[self.preferences getProfilePicString]];
        
        [cell.profilePic setImageWithURL:[NSURL URLWithString:url]
                        placeholderImage:[UIImage imageNamed:@"avatar square"]];
        
        [UIImage makeRoundedImage:cell.profilePic withRadius:20];
        
        UIButton *playerPicButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
        [playerPicButton addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
        playerPicButton.tag = [self.preferences getUserID];
        [cell.profilePic addSubview:playerPicButton];
        cell.profilePic.userInteractionEnabled = YES;
        
        cell.playStatusLabel.text = [self getPlayerStatus];
        [cell.myBackgroundView makeEdgyDropshadow:[UIColor whiteColor]];

        return cell;
        
    } else if (selectedCode == CTE_PLAYERS_CELL) {
        static NSString *CellIdentifier = @"Match Competitors Cell";
        PMDMatchCompetitorsCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        
        cell.pickupMatch = self.pickupMatch;
        cell.sessionUserID = [self.preferences getUserID];
        [cell initPlayers];
        [cell.collectionView reloadData];
        cell.delegate = self;
        [cell.myBackgroundView makeEdgyDropshadow:[UIColor whiteColor]];

        
        return cell;
    } else if (selectedCode == CTE_LATEST_NEWS_CELL) {
        static NSString *CellIdentifier = @"Latest News Cell";
        PMDLatestNewsCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        
        [cell.newsIconBackground makeCircleWithColorAndBackground:[UIColor redColor] andRadius:12];
        
   //     cell.newsIconPic.image = [[UIImage imageNamed:@"news tab"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
   //     cell.newsIconPic.tintColor = [UIColor whiteColor];
        
        NSDateFormatter *historyDateFormatter = [[NSDateFormatter alloc]init];
        [historyDateFormatter setDateFormat:@"MMMM d, YYYY h:mm a"];
        cell.datetimeLabel.text = [historyDateFormatter stringFromDate:self.matchNews.newsDate];
        
        if ([self.matchNews isKindOfClass:[MatchPlayerNews class]]) {
            MatchPlayerNews *mpn = (MatchPlayerNews *)self.matchNews;
            cell.nameLabel.text = mpn.player.userName;
            NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                    baseString:mpn.player.profileBaseString];
            
            [cell.picImage setImageWithURL:[NSURL URLWithString:url]
                          placeholderImage:[UIImage imageNamed:@"avatar square"]];
            [UIImage makeRoundedImage:cell.picImage withRadius:20];
            
            cell.subheadlineLabel.text = mpn.matchGeneralMessage;
            
            
        } else {
            cell.picImage.image = [self.pickupMatch.sport getSportIcon];
            cell.subheadlineLabel.text = self.matchNews.matchGeneralMessage;
            cell.nameLabel.text = self.pickupMatch.matchName;
            
        }
        return cell;

    } else if (selectedCode == CTE_MATCH_MESSAGE_CELL) {
        static NSString *CellIdentifier = @"Match Message Cell";
        PMDMatchMessageCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        
        cell.messageTextView.text = self.pickupMatch.message;
        [cell.myBackgroundView makeEdgyDropshadow:[UIColor whiteColor]];
        
        return cell;
    }
    
    return nil;
}


- (CGFloat)getVenueDescriptionContentHeight
{
    // Get a CGSize for the width and, effectively, unlimited height
    CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH - (CELL_CONTENT_MARGIN * 2), 20000.0f);
    // Get the size of the text given the CGSize we just made as a constraint
    CGSize size = [self.pickupMatch.venue.description sizeWithFont:[UIFont systemFontOfSize:FONT_SIZE] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    // Get the height of our measurement, with a minimum of 71 (standard cell size)
    CGFloat height = MAX(size.height, 65.0f);
    // return the height, with a bit of extra padding in
    return height + (CELL_CONTENT_MARGIN * 2) - 90;
    
}

- (NSUInteger)computeCellToDisplay:(NSUInteger)indexRow
{
    NSUInteger displayCell = 0;
    
  //  if (self.pageDisplayConfiguration == CELL_CONFIG_DEFAULT) {
        
        switch(indexRow) {
            case 0:
                displayCell = CTE_MATCH_STATUS_CELL;
                break;
            case 1:
                displayCell = CTE_HEADER_INFO_CELL;
                break;
            case 2:
                displayCell = CTE_MATCH_DETAILS_CELL;
                break;
            case 3:
                displayCell = CTE_OTHER_MATCH_DETAILS_CELL;
                break;
            case 4:
                displayCell = CTE_SESSION_USER_STATUS_CELL;
                break;
            case 5:
                displayCell = CTE_PLAYERS_CELL;
                break;
            case 6:
                displayCell = CTE_LATEST_NEWS_CELL;
                break;
            
            case 7:
                displayCell = CTE_MATCH_MESSAGE_CELL;
                break;
                
            default:
                displayCell = 0;
                break;
        }
  //  }
    
    return  displayCell;
}


- (void)setDisplayConfiguration
{
    NSDate *thirtyMinAgo = [self.pickupMatch.dateTime dateByAddingTimeInterval:1800];

    if ([thirtyMinAgo compare:[NSDate date]] == NSOrderedAscending) {
        // Match is Past
        
        if (self.pickupMatch.matchCreatorID == [self.preferences getUserID]) {
            // If session user is match creator
            self.pageDisplayConfiguration = VIEW_STATE_2;
            
        } else if ([self.pickupMatch canSessionUserRSVPForMatch]) {
            // User was either invited or the match is public
            
            self.pageDisplayConfiguration = VIEW_STATE_2;
            
        } else {
            // private match and user not invited
            self.pageDisplayConfiguration = VIEW_STATE_4;
        }
        
        
    } else {
        // Match is in Future
        
        if (self.pickupMatch.matchCreatorID == [self.preferences getUserID]
            && [self.pickupMatch isMatchCancelled]) {
            // Session user is match creator
            self.pageDisplayConfiguration = VIEW_STATE_2;
            
        } else if (self.pickupMatch.matchCreatorID == [self.preferences getUserID]
                   && [self.pickupMatch isMatchConfirmed]) {
            self.pageDisplayConfiguration = VIEW_STATE_6;
            
        } else if (self.pickupMatch.matchCreatorID == [self.preferences getUserID]
                   && ![self.pickupMatch isMatchConfirmed]) {
            self.pageDisplayConfiguration = VIEW_STATE_1;
            
        } else if ([self.pickupMatch wasPlayerInvited:[self.preferences getUserID]]
                   && [self.pickupMatch isMatchCancelled]) {
            // User was invited but match cancelled
            self.pageDisplayConfiguration = VIEW_STATE_2;

        } else if ([self.pickupMatch wasPlayerInvited:[self.preferences getUserID]]) {
            
            if ([[self.pickupMatch getSessionUserCodedStatus] isEqualToString:@"DECLINED"])
                self.pageDisplayConfiguration = VIEW_STATE_4; // for now if user already declined dont show any buttons
            else
                // User was invited
                self.pageDisplayConfiguration = VIEW_STATE_3;
            
        } else if (!self.pickupMatch.private && [self.pickupMatch isMatchCancelled]) {
            // user not invited but match public && cancelled
            self.pageDisplayConfiguration = VIEW_STATE_2;
        
        } else if (!self.pickupMatch.private) {
            // user not invited but match public
            
            if ([[self.pickupMatch getSessionUserCodedStatus] isEqualToString:@"DECLINED"])
                self.pageDisplayConfiguration = VIEW_STATE_4; // for now if user already declined dont show any buttons
            else
                self.pageDisplayConfiguration = VIEW_STATE_5;
            
        } else {
            // match must be private and user not invited
            self.pageDisplayConfiguration = VIEW_STATE_4;
        }
        
    }
    
}
/*
- (void) showButtonsForTableCell:(PMDHeaderInfoCell *)cell
{
    switch (self.pageDisplayConfiguration) {
        case VIEW_STATE_1:
            
            [cell.button1 setTitle:@"Ready To Play" forState:UIControlStateNormal];
            [cell.button1 makeRoundBorderWithColor:[UIColor whiteColor]];
            
            [cell.button2 setTitle:@"Cancel Match" forState:UIControlStateNormal];
            [cell.button2 makeRoundBorderWithColor:[UIColor whiteColor]];
            
            [cell.button3 setTitle:@"Message" forState:UIControlStateNormal];
            [cell.button3 makeRoundBorderWithColor:[UIColor whiteColor]];
            
            [cell.button4 setTitle:@"Share" forState:UIControlStateNormal];
            [cell.button4 makeRoundBorderWithColor:[UIColor whiteColor]];
            
            cell.button1.hidden = NO;
            cell.button2.hidden = NO;
            cell.button3.hidden = NO;
            cell.button4.hidden = NO;
            
            
            break;
        case VIEW_STATE_2:
            if (![self.pickupMatch checkLoggedInUserConfirmedBOOL]) {
                cell.button1.hidden = YES;
                cell.button2.hidden = YES;
                cell.button3.hidden = YES;
                cell.button4.hidden = YES;
            } else {
            // eventually might not show if over 10 days ago
                [cell.button1 setTitle:@"Message" forState:UIControlStateNormal];
                [cell.button1 makeRoundBorderWithColor:[UIColor whiteColor]];
            
                [cell.button2 setTitle:@"Share" forState:UIControlStateNormal];
                [cell.button2 makeRoundBorderWithColor:[UIColor whiteColor]];
            
                cell.button1.hidden = NO;
                cell.button2.hidden = NO;
                cell.button3.hidden = YES;
                cell.button4.hidden = YES;
            }
            
            break;
        case VIEW_STATE_3:
            if (![self.pickupMatch checkLoggedInUserConfirmedBOOL]) {
                [cell.button1 setTitle:@"Join" forState:UIControlStateNormal];
                [cell.button1 makeRoundBorderWithColor:[UIColor whiteColor]];
                
                [cell.button2 setTitle:@"Decline" forState:UIControlStateNormal];
                [cell.button2 makeRoundBorderWithColor:[UIColor whiteColor]];
                
                cell.button1.hidden = NO;
                cell.button2.hidden = NO;
                cell.button3.hidden = YES;
                cell.button4.hidden = YES;
                
                
            } else {
                [cell.button1 setTitle:@"Withdraw" forState:UIControlStateNormal];
                [cell.button1 makeRoundBorderWithColor:[UIColor whiteColor]];
                
                [cell.button2 setTitle:@"Message" forState:UIControlStateNormal];
                [cell.button2 makeRoundBorderWithColor:[UIColor whiteColor]];
                
                [cell.button3 setTitle:@"Share" forState:UIControlStateNormal];
                [cell.button3 makeRoundBorderWithColor:[UIColor whiteColor]];
                
                cell.button1.hidden = NO;
                cell.button2.hidden = NO;
                cell.button3.hidden = NO;
                cell.button4.hidden = YES;
            }
            break;
        case VIEW_STATE_4:
            cell.button1.hidden = YES;
            cell.button2.hidden = YES;
            cell.button3.hidden = YES;
            cell.button4.hidden = YES;
            
            break;
        case VIEW_STATE_5:
            if (![self.pickupMatch checkLoggedInUserConfirmedBOOL]) {
                [cell.button1 setTitle:@"Join" forState:UIControlStateNormal];
                [cell.button1 makeRoundBorderWithColor:[UIColor whiteColor]];
                
                [cell.button2 setTitle:@"Share" forState:UIControlStateNormal];
                [cell.button2 makeRoundBorderWithColor:[UIColor whiteColor]];
                
            //    [cell.button3 setTitle:@"Share" forState:UIControlStateNormal];
            //    [cell.button3 makeRoundBorderWithColor:[UIColor whiteColor]];
                
                cell.button1.hidden = NO;
                cell.button2.hidden = NO;
                cell.button3.hidden = YES;
                cell.button4.hidden = YES;
                
                
            } else {
                [cell.button1 setTitle:@"Withdraw" forState:UIControlStateNormal];
                [cell.button1 makeRoundBorderWithColor:[UIColor whiteColor]];
                
                [cell.button2 setTitle:@"Message" forState:UIControlStateNormal];
                [cell.button2 makeRoundBorderWithColor:[UIColor whiteColor]];
                
                [cell.button3 setTitle:@"Share" forState:UIControlStateNormal];
                [cell.button3 makeRoundBorderWithColor:[UIColor whiteColor]];
                
                cell.button1.hidden = NO;
                cell.button2.hidden = NO;
                cell.button3.hidden = NO;
                cell.button4.hidden = YES;
                
            }
            break;
        case VIEW_STATE_6:
            [cell.button1 setTitle:@"Cancel Match" forState:UIControlStateNormal];
            [cell.button1 makeRoundBorderWithColor:[UIColor whiteColor]];
            
            [cell.button2 setTitle:@"Message" forState:UIControlStateNormal];
            [cell.button2 makeRoundBorderWithColor:[UIColor whiteColor]];
            
            [cell.button3 setTitle:@"Share" forState:UIControlStateNormal];
            [cell.button3 makeRoundBorderWithColor:[UIColor whiteColor]];
            
            cell.button1.hidden = NO;
            cell.button2.hidden = NO;
            cell.button3.hidden = NO;
            cell.button4.hidden = YES;
            
            
        default:
            break;
    }
}
*/

- (void)showActionsheet:(NSString *)actionType
{
    if ([actionType isEqualToString:JOIN_MATCH]) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                      initWithTitle:TITLE_OF_ACTIONSHEET_FOR_JOIN_MATCH delegate:self
                                      cancelButtonTitle:CANCEL_BUTTON_ACTIONSHEET destructiveButtonTitle:nil
                                      otherButtonTitles:JOIN_MATCH_ADD_TO_CALENDAR, JOIN_MATCH,  nil];
        [actionSheet showFromTabBar:self.tabBarController.tabBar];
        
    } else if ([actionType isEqualToString:DECLINE_MATCH]) {
        
        UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                      initWithTitle:TITLE_OF_ACTIONSHEET_FOR_DECLINE_MATCH delegate:self
                                      cancelButtonTitle:CANCEL_BUTTON_ACTIONSHEET destructiveButtonTitle:nil
                                      otherButtonTitles:DECLINE_MATCH,  nil];
        [actionSheet showFromTabBar:self.tabBarController.tabBar];
        
    } else if ([actionType isEqualToString:CANCEL_MATCH]) {
        
        UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                      initWithTitle:TITLE_OF_ACTIONSHEET_FOR_CANCEL_MATCH delegate:self
                                      cancelButtonTitle:CANCEL_BUTTON_ACTIONSHEET destructiveButtonTitle:nil
                                      otherButtonTitles:CANCEL_MATCH,  nil];
        [actionSheet showFromTabBar:self.tabBarController.tabBar];
        
    } else if ([actionType isEqualToString:WITHDRAW_MATCH]) {
        
        UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                      initWithTitle:TITLE_OF_ACTIONSHEET_FOR_WITHDRAW_MATCH delegate:self
                                      cancelButtonTitle:CANCEL_BUTTON_ACTIONSHEET destructiveButtonTitle:nil
                                      otherButtonTitles:WITHDRAW_MATCH,  nil];
        [actionSheet showFromTabBar:self.tabBarController.tabBar];
        
    } else if ([actionType isEqualToString:VOID_MATCH]) {
        
        UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                      initWithTitle:TITLE_OF_ACTIONSHEET_FOR_VOID_MATCH delegate:self
                                      cancelButtonTitle:CANCEL_BUTTON_ACTIONSHEET destructiveButtonTitle:nil
                                      otherButtonTitles:VOID_MATCH,  nil];
        [actionSheet showFromTabBar:self.tabBarController.tabBar];
        
    } else if ([actionType isEqualToString:CONFIRM_MATCH]) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                      initWithTitle:TITLE_OF_ACTIONSHEET_FOR_CONFIRM_MATCH delegate:self
                                      cancelButtonTitle:CANCEL_BUTTON_ACTIONSHEET destructiveButtonTitle:nil
                                      otherButtonTitles:CONFIRM_MATCH, nil];
        [actionSheet showFromTabBar:self.tabBarController.tabBar];
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == actionSheet.destructiveButtonIndex) {
        [self.view.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    } else {
        NSString *choice = [actionSheet buttonTitleAtIndex:buttonIndex];
        
        if ([choice isEqualToString:VOID_MATCH]) {
            
            
        } else if ([choice isEqualToString:JOIN_MATCH]) {
            [self joinMatchAction];
        } else if ([choice isEqualToString:JOIN_MATCH_ADD_TO_CALENDAR]) {
            [self addMatchToCalendar];
        } else if ([choice isEqualToString:CANCEL_MATCH]) {
            [self cancelMatchAction];
        } else if ([choice isEqualToString:DECLINE_MATCH]) {
            [self declineMatchAction];
        } else if ([choice isEqualToString:WITHDRAW_MATCH]) {
            [self declineMatchAction];
        } else if ([choice isEqualToString:CONFIRM_MATCH]) {
            [self confirmMatchAction];
        }
    }
}

- (void)addMatchToCalendar
{
    EKEventStore *eventStore = [[EKEventStore alloc] init];
    
    [eventStore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
        
        EKEvent *event  = [EKEvent eventWithEventStore:eventStore];
        event.title     = self.pickupMatch.matchName;
        event.notes     = [NSString stringWithFormat:@"%@ Pickup Match", [self.pickupMatch.sport.sportName capitalizedString]];
        event.startDate = self.pickupMatch.dateTime;
        event.endDate   = [[NSDate alloc] initWithTimeInterval:3600 sinceDate:self.pickupMatch.dateTime];
        event.location = self.pickupMatch.venue.venueName;
        
        [event setCalendar:[eventStore defaultCalendarForNewEvents]];
        NSError *err;
        [eventStore saveEvent:event span:EKSpanThisEvent error:&err];
        
        [self joinMatchAction];
        
    }];
}
/*
- (IBAction)button1Action:(UIButton *)sender
{
    if ([sender.titleLabel.text isEqualToString:@"Ready To Play"]) {
        [self showActionsheet:CONFIRM_MATCH];
    } else if ([sender.titleLabel.text isEqualToString:@"Join"]) {
        [self showActionsheet:JOIN_MATCH];
    } else if ([sender.titleLabel.text isEqualToString:@"Withdraw"]) {
        [self showActionsheet:WITHDRAW_MATCH];
    } else if ([sender.titleLabel.text isEqualToString:@"Message"]) {
        [self performSegueWithIdentifier:@"History Segue" sender:self];
    } else if ([sender.titleLabel.text isEqualToString:@"Cancel Match"]) {
        [self showActionsheet:CANCEL_MATCH];
    }
}

- (IBAction)button2Action:(UIButton *)sender
{
    if ([sender.titleLabel.text isEqualToString:@"Cancel Match"]) {
        [self showActionsheet:CANCEL_MATCH];
    } else if ([sender.titleLabel.text isEqualToString:@"Message"]) {
        [self performSegueWithIdentifier:@"History Segue" sender:self];
    } else if ([sender.titleLabel.text isEqualToString:@"Share"]) {
        [self startFBShare];
    } else if ([sender.titleLabel.text isEqualToString:@"Decline"]) {
        [self showActionsheet:DECLINE_MATCH];
    }
}

- (IBAction)button3Action:(UIButton *)sender
{
    if ([sender.titleLabel.text isEqualToString:@"Share"]) {
        [self startFBShare];
    } else if ([sender.titleLabel.text isEqualToString:@"Message"]) {
        [self performSegueWithIdentifier:@"History Segue" sender:self];
    }
}

- (IBAction)button4Action:(UIButton *)sender
{
    if ([sender.titleLabel.text isEqualToString:@"Share"]) {
        [self startFBShare];
    }
}

*/

- (void)startFBShare
{
    if ([[FBSession activeSession] isOpen]) { // Facebook session is opened
        if ([FBSession.activeSession.permissions indexOfObject:@"publish_actions"] == NSNotFound) {
        // Request publish_actions
            [FBSession.activeSession requestNewPublishPermissions:[NSArray arrayWithObject:@"publish_actions"]
                                              defaultAudience:FBSessionDefaultAudienceFriends
                                            completionHandler:^(FBSession *session, NSError *error) {
                                                __block NSString *alertText;
                                                __block NSString *alertTitle;
                                                if (!error) {
                                                    if ([FBSession.activeSession.permissions
                                                         indexOfObject:@"publish_actions"] == NSNotFound){
                                                        // Permission not granted, tell the user we will not publish
                                                        alertTitle = @"Permission not granted";
                                                        alertText = @"Your action will not be published to Facebook.";
                                                        [[[UIAlertView alloc] initWithTitle:@"title"
                                                                                    message:@"text"
                                                                                   delegate:self
                                                                          cancelButtonTitle:@"OK!"
                                                                          otherButtonTitles:nil] show];
                                                    } else {
                                                        // Permission granted, publish the OG story
                                                        [self presentFBDialog];
                                                    }
                                                    
                                                } else {
                                                    // There was an error, handle it
                                                    // See https://developers.facebook.com/docs/ios/errors/
                                                }
                                            }];
        
        } else {
            // permission exists
            [self presentFBDialog];
        }
    } else {
        // Create a new session
        [self openFBSession];
    }
}

/*
 - (void)presentFBDialog
{
    // Present share dialog
    FBShareDialogParams *params = [[FBShareDialogParams alloc] init];
    params.link = [NSURL URLWithString:@"https://www.vaiden.com"];
    params.name = [NSString stringWithFormat:@"%@ Pickup Match", [self.pickupMatch.sport.sportName capitalizedString]];
    params.caption = @"Vaiden is a community for amateur athletes to play their favorite sports using a social platform.";
    params.picture = [NSURL URLWithString:@"https://s3.amazonaws.com/vaderen_pictures/vaiden_fb_icon.png"];
    //  params.picture = self.fbImage;
    params.description = [NSString stringWithFormat:@"%@ shared a pickup match with you", [self.preferences getUserName]];
    
    //
    
    [FBDialogs presentShareDialogWithLink:params.link
                                     name:params.name
                                  caption:params.caption
                              description:params.description
                                  picture:params.picture
                              clientState:nil
                                  handler:^(FBAppCall *call, NSDictionary *results, NSError *error) {
                                      if(error) {
                                          // An error occurred, we need to handle the error
                                          // See: https://developers.facebook.com/docs/ios/errors
                                          NSLog([NSString stringWithFormat:@"Error publishing story: %@", error.description]);
                                      } else {
                                          // Success
                                          NSLog(@"result %@", results);
                                      }
                                  }];
}
*/

- (void)openFBSession
{
    // if the session is closed, then we open it here, and establish a handler for state changes
    [FBSession openActiveSessionWithReadPermissions:nil
                                       allowLoginUI:YES
                                  completionHandler:^(FBSession *session,
                                                      FBSessionState state,
                                                      NSError *error) {
                                      if (error) {
                                          UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                                              message:error.localizedDescription
                                                                                             delegate:nil
                                                                                    cancelButtonTitle:@"OK"
                                                                                    otherButtonTitles:nil];
                                          [alertView show];
                                      } else if (session.isOpen) {
                                          [self presentFBDialog];
                                      }
                                  }];
}

- (void)presentFBDialog
{
    // Present share dialog
    /*   FBShareDialogParams *params = [[FBShareDialogParams alloc] init];
     params.link = [NSURL URLWithString:@"https://www.vaiden.com"];
     params.name = self.fbTitle;
     params.caption = @"Vaiden is a community for amateur athletes to play their favorite sports using a social platform.";
     params.picture = [NSURL URLWithString:@"https://s3.amazonaws.com/vaderen_pictures/vaiden_fb_icon.png"];
     //  params.picture = self.fbImage;
     params.description = self.fbMessage;
     */
    //
    /*
     [FBDialogs presentShareDialogWithLink:params.link
     name:params.name
     caption:params.caption
     description:params.description
     picture:params.picture
     clientState:nil
     handler:^(FBAppCall *call, NSDictionary *results, NSError *error) {
     if(error) {
     // An error occurred, we need to handle the error
     // See: https://developers.facebook.com/docs/ios/errors
     NSLog([NSString stringWithFormat:@"Error publishing story: %@", error.description]);
     } else {
     // Success
     NSLog(@"result %@", results);
     }
     }];
     */
    // Put together the dialog parameters
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   [NSString stringWithFormat:@"%@ Pickup Match", [self.pickupMatch.sport.sportName capitalizedString]], @"name",
                                   @"Vaiden is a community for athletes to play their favorite sports using a social platform.", @"caption",
                                   [NSString stringWithFormat:@"%@ shared a pickup match with you", [self.preferences getUserName]], @"description",
                                   @"https://itunes.apple.com/us/app/vaiden/id796761406?ls=1&mt=8", @"link",
                                   @"https://s3.amazonaws.com/vaderen_pictures/vaiden_fb_icon.png", @"picture",
                                   nil];
    
    // Show the feed dialog
    [FBWebDialogs presentFeedDialogModallyWithSession:nil
                                           parameters:params
                                              handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
                                                  if (error) {
                                                      // An error occurred, we need to handle the error
                                                      // See: https://developers.facebook.com/docs/ios/errors
                                       //               NSLog([NSString stringWithFormat:@"Error publishing story: %@", error.description]);
                                                  } else {
                                                      if (result == FBWebDialogResultDialogNotCompleted) {
                                                          // User cancelled.
                                                          NSLog(@"User cancelled.");
                                                      } else {
                                                          // Handle the publish feed callback
                                                          NSDictionary *urlParams = [self parseURLParams:[resultURL query]];
                                                          
                                                          if (![urlParams valueForKey:@"post_id"]) {
                                                              // User cancelled.
                                                              NSLog(@"User cancelled.");
                                                              
                                                          } else {
                                                              // User clicked the Share button
                                                              NSString *result = [NSString stringWithFormat: @"Posted story, id: %@", [urlParams valueForKey:@"post_id"]];
                                                              NSLog(@"result %@", result);
                                                          }
                                                      }
                                                  }
                                              }];
}

// A function for parsing URL parameters returned by the Feed Dialog.
- (NSDictionary*)parseURLParams:(NSString *)query {
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *pair in pairs) {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        NSString *val =
        [kv[1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        params[kv[0]] = val;
    }
    return params;
}



- (void)joinMatchAction
{
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.labelText = @"Submitting RSVP";
    [IOSRequest rsvpForPickupMatch:self.pickupMatch.matchID
                          withRSVP:2 // CONFIRMED STATUS FOR INDIVIDUAL PLAYER
                        andComment:@""
                           forUser:[self.preferences getUserID]
                      onCompletion:^(NSDictionary * results) {
                          if ([results[@"error_code"] isEqualToString:@"BAD_PERMISSIONS"]
                              && [results[@"outcome"] isEqualToString:@"failure"]) {
                              dispatch_async(dispatch_get_main_queue(), ^{
                                  [MBProgressHUD hideHUDForView:self.view animated:YES];
                                  [self.preferences setNewsfeedRefreshState:YES];
                                  [self.preferences setMatchNotificationRefreshState:YES];
                                  UIAlertView *alert = [[UIAlertView alloc] init];
                                  alert.title = @"RSVP Error";
                                  alert.message = @"Sorry but you cannot RSVP at this time";
                                  [alert addButtonWithTitle:@"OK"];
                                  [alert show];
                                  [self.navigationController popViewControllerAnimated:YES];
                              });
                          } else if ([results[@"error_code"] isEqualToString:@"PAST_MATCH"]
                                      && [results[@"outcome"] isEqualToString:@"failure"]) {
                              dispatch_async(dispatch_get_main_queue(), ^{
                                  [MBProgressHUD hideHUDForView:self.view animated:YES];
                                  [self.preferences setNewsfeedRefreshState:YES];
                                  [self.preferences setMatchNotificationRefreshState:YES];
                                  UIAlertView *alert = [[UIAlertView alloc] init];
                                  alert.title = @"Match Expired";
                                  alert.message = @"You cannot RSVP for this match because it has expired";
                                  [alert addButtonWithTitle:@"OK"];
                                  [alert show];
                                  [self.navigationController popViewControllerAnimated:YES];
                              });
                          } else if ([results[@"outcome"] isEqualToString:@"failure"]) {
                              
                              dispatch_async(dispatch_get_main_queue(), ^{
                                 [MBProgressHUD hideHUDForView:self.view animated:YES];
                                  UIAlertView *alert = [[UIAlertView alloc] init];
                                  alert.title = @"RSVP Error";
                                  alert.message = @"There was a problem submitting your request.  Please retry.";
                                  [alert addButtonWithTitle:@"OK"];
                                  [alert show];
                                  
                              });
                          } else {
                          
                              dispatch_async(dispatch_get_main_queue(), ^{
                                  [MBProgressHUD hideHUDForView:self.view animated:YES];
                //                  self.response = ACCEPTED_RSVP_PICKUPMATCH;
                                  [self.preferences setMatchNotificationRefreshState:YES];
                                  [self.preferences setNewsfeedRefreshState:YES];
                                  [self refreshView];
//                                  [self performSegueWithIdentifier:@"RSVP Result Segue" sender:self];
                              });
                          }
                      }];
    double delayInSeconds = 10.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        //code to be executed on the main queue after delay
        [MBProgressHUD hideHUDForView:self.view animated:YES];// just in case
    });
    
    
}

- (void)declineMatchAction
{
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.labelText = @"Submitting RSVP";
    [IOSRequest rsvpForPickupMatch:self.pickupMatch.matchID
                          withRSVP:3
                        andComment:@""
                           forUser:[self.preferences getUserID]
                      onCompletion:^(NSDictionary * results) {
                          if ([results[@"error_code"] isEqualToString:@"BAD_PERMISSIONS"]) {
                              dispatch_async(dispatch_get_main_queue(), ^{
                                  [MBProgressHUD hideHUDForView:self.view animated:YES];
                                  [self.preferences setNewsfeedRefreshState:YES];
                                  [self.preferences setMatchNotificationRefreshState:YES];
                                  UIAlertView *alert = [[UIAlertView alloc] init];
                                  alert.title = @"RSVP Error";
                                  alert.message = @"Sorry but you cannot RSVP at this time";
                                  [alert addButtonWithTitle:@"OK"];
                                  [alert show];
                                  [self.navigationController popViewControllerAnimated:YES];
                              });
                          } else if ([results[@"error_code"] isEqualToString:@"PAST_MATCH"]) {
                              dispatch_async(dispatch_get_main_queue(), ^{
                                  [MBProgressHUD hideHUDForView:self.view animated:YES];
                                  [self.preferences setNewsfeedRefreshState:YES];
                                  [self.preferences setMatchNotificationRefreshState:YES];
                                  UIAlertView *alert = [[UIAlertView alloc] init];
                                  alert.title = @"Match Expired";
                                  alert.message = @"You cannot RSVP for this match because it has expired";
                                  [alert addButtonWithTitle:@"OK"];
                                  [alert show];
                                  [self.navigationController popViewControllerAnimated:YES];
                              });
                          } else {
                              
                              dispatch_async(dispatch_get_main_queue(), ^{
                                  [MBProgressHUD hideHUDForView:self.view animated:YES];
                          //        self.response = DECLINED_RSVP_PICKUPMATCH;
                                  [self.preferences setMatchNotificationRefreshState:YES];
                                  [self.preferences setNewsfeedRefreshState:YES];
 //                                 [self performSegueWithIdentifier:@"RSVP Result Segue" sender:self];
                                  [self refreshView];
                              });
                          }
                      }];
   double delayInSeconds = 10.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        //code to be executed on the main queue after delay
        [MBProgressHUD hideHUDForView:self.view animated:YES];// just in case
    });
}

- (void)cancelMatchAction
{
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.labelText = @"Cancelling Match";
    [IOSRequest cancelMatchWithID:self.pickupMatch.matchID
                           byUser:[self.preferences getUserID]
                         withType:@"pickup"
                     onCompletion:^(NSDictionary *results) {
                         
                         if ([results[@"error_code"] isEqualToString:@"NOT_MATCH_CREATOR"]
                             && [results[@"outcome"] isEqualToString:@"failure"]) {
                             dispatch_async(dispatch_get_main_queue(), ^{
                                 [MBProgressHUD hideHUDForView:self.view animated:YES];
                                 [self.preferences setNewsfeedRefreshState:YES];
                                 [self.preferences setMatchNotificationRefreshState:YES];
                                 UIAlertView *alert = [[UIAlertView alloc] init];
                                 alert.title = @"Not Match Creator";
                                 alert.message = @"You are not the match creator.  You cannot cancel this match.";
                                 [alert addButtonWithTitle:@"OK"];
                                 [alert show];
                                 [self.navigationController popViewControllerAnimated:YES];
                             });
                         } else if ([results[@"error_code"] isEqualToString:@"PAST_MATCH"]
                                    && [results[@"outcome"] isEqualToString:@"failure"]) {
                             dispatch_async(dispatch_get_main_queue(), ^{
                                 [MBProgressHUD hideHUDForView:self.view animated:YES];
                                 [self.preferences setNewsfeedRefreshState:YES];
                                 [self.preferences setMatchNotificationRefreshState:YES];
                                 UIAlertView *alert = [[UIAlertView alloc] init];
                                 alert.title = @"Match Expired";
                                 alert.message = @"You cannot cancel this match because it has expired";
                                 [alert addButtonWithTitle:@"OK"];
                                 [alert show];
                                 [self.navigationController popViewControllerAnimated:YES];
                             });
                         } else if ([results[@"outcome"] isEqualToString:@"failure"]) {
                             dispatch_async(dispatch_get_main_queue(), ^{
                                 [MBProgressHUD hideHUDForView:self.view animated:YES];
                                
                                 UIAlertView *alert = [[UIAlertView alloc] init];
                                 alert.title = @"Match Cancel Error";
                                 alert.message = @"There was a problem completing your request.  Please try again.";
                                 [alert addButtonWithTitle:@"OK"];
                                 [alert show];
                             });
                         } else {
                             
                             // must be successful
                         
                         
                             dispatch_async(dispatch_get_main_queue(), ^{
                                 [MBProgressHUD hideHUDForView:self.view animated:YES];
           //                      self.response = CANCELLED_MATCH_PICKUPMATCH;
                                 [self.preferences setMatchNotificationRefreshState:YES];
                                 [self.preferences setNewsfeedRefreshState:YES];
            //                     [self performSegueWithIdentifier:@"RSVP Result Segue" sender:self];
                                 [self refreshView];
                                 
                             });
                         }
                         
                     }];
   double delayInSeconds = 10.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        //code to be executed on the main queue after delay
        [MBProgressHUD hideHUDForView:self.view animated:YES];// just in case
    });
    
}

- (void)confirmMatchAction
{
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.labelText = @"Submitting Request";
    [IOSRequest confirmMatchWithID:self.pickupMatch.matchID
                            byUser:[self.preferences getUserID]
                          withType:@"pickup"
                      onCompletion:^(NSDictionary * results) {
                          if ([results[@"error_code"] isEqualToString:@"NOT_MATCH_CREATOR"]) {
                              dispatch_async(dispatch_get_main_queue(), ^{
                                  [MBProgressHUD hideHUDForView:self.view animated:YES];
                                  [self.preferences setNewsfeedRefreshState:YES];
                                  [self.preferences setMatchNotificationRefreshState:YES];
                                  UIAlertView *alert = [[UIAlertView alloc] init];
                                  alert.title = @"Not Match Creator";
                                  alert.message = @"You are not the match creator.  You cannot confirm this match.";
                                  [alert addButtonWithTitle:@"OK"];
                                  [alert show];
                                  [self.navigationController popViewControllerAnimated:YES];
                              });
                          } else if ([results[@"error_code"] isEqualToString:@"PAST_MATCH"]) {
                              dispatch_async(dispatch_get_main_queue(), ^{
                                  [MBProgressHUD hideHUDForView:self.view animated:YES];
                                  [self.preferences setNewsfeedRefreshState:YES];
                                  [self.preferences setMatchNotificationRefreshState:YES];
                                  UIAlertView *alert = [[UIAlertView alloc] init];
                                  alert.title = @"Match Is Past";
                                  alert.message = @"You cannot confirm this match because it is in the past";
                                  [alert addButtonWithTitle:@"OK"];
                                  [alert show];
                                  [self.navigationController popViewControllerAnimated:YES];
                              });
                          } else {
                              
                              dispatch_async(dispatch_get_main_queue(), ^{
                                  [MBProgressHUD hideHUDForView:self.view animated:YES];
              //                    self.response = CONFIRMED_MATCH_PICKUPMATCH;
                                  [self.preferences setMatchNotificationRefreshState:YES];
                                  [self.preferences setNewsfeedRefreshState:YES];
              //                    [self performSegueWithIdentifier:@"RSVP Result Segue" sender:self];
                                  [self refreshView];
                              });
                          }
                      }];
    double delayInSeconds = 10.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        //code to be executed on the main queue after delay
        [MBProgressHUD hideHUDForView:self.view animated:YES];// just in case
    });
}

- (IBAction)venueDetailAction:(id)sender
{
    [self performSegueWithIdentifier:@"Venue Detail Segue" sender:self];
}

- (void)goToProfile:(UIButton *)sender
{
    self.selectedPlayerToDisplayDetail = sender.tag;
    [self performSegueWithIdentifier:@"Player Detail Segue" sender:self];
}
/*
- (void)setPMDMatchCompetitorsCellViewController:(PMDMatchCompetitorsCell *)controller withUserIDToSegue:(NSInteger)userID
{
    self.selectedPlayerToDisplayDetail = userID;
    [self performSegueWithIdentifier:@"Player Detail Segue" sender:self];
}
*/

- (void)setWithProfileIDForVC:(PMDHeaderInfoCell *)controller withUserIDToSegue:(NSInteger)userID
{
    self.selectedPlayerToDisplayDetail = userID;
    [self performSegueWithIdentifier:@"Player Detail Segue" sender:self];
}

- (void)setWithButtonMessage:(PMDHeaderInfoCell *)controller withMessage:(NSString *)messageAction
{
    if ([messageAction isEqual:@"Join"]) {
        [self showActionsheet:JOIN_MATCH];
        
    } else if ([messageAction isEqual:@"Withdraw"]) {
        [self showActionsheet:WITHDRAW_MATCH];
    } else if ([messageAction isEqual:@"Cancel Match"]) {
        [self showActionsheet:CANCEL_MATCH];
    } else if ([messageAction isEqual:@"Ready To Play"]) {
        [self showActionsheet:CONFIRM_MATCH];

    } else if ([messageAction isEqualToString:@"Message"]) {
     //   if ([self.pickupMatch checkLoggedInUserConfirmedBOOL]) {
     //       [self performSegueWithIdentifier:@"History Segue" sender:self];
     //   }
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Coming Soon...";
        alert.message = @"This feature is in the works.  It will be added soon...";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
    } else if ([messageAction isEqualToString:@"Decline"]) {
        [self showActionsheet:DECLINE_MATCH];
    } else if ([messageAction isEqualToString:@"Share"]) {
        [self startFBShare];
    }
    
}
@end
