//
//  NearbyPlayersVC.h
//  Phenom
//
//  Created by James Chung on 7/14/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "ChooseIndividualSportTVC.h"
#import "CustomBaseVC.h"

@interface NearbyPlayersVC : CustomBaseVC <CLLocationManagerDelegate, UITableViewDelegate, UITableViewDataSource, ChooseIndividualSportTVC_Delegate>
{
    CLLocationManager *locationManager;
}
@end
