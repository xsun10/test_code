//
//  TeamMatchDetailTVC.h
//  Vaiden
//
//  Created by James Chung on 4/16/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "CustomBaseTVC.h"
#import "IndividualMatch.h"
#import "MapViewTVC.h"
#import "MBProgressHUD.h"
#import "TMDHeaderInfoCell.h"
#import "MapViewTVC.h"

@interface TeamMatchDetailTVC : MapViewTVC <MBProgressHUDDelegate, TMDHeaderInfoCell_Delegate>
{
    MBProgressHUD *HUD;
}


@property (nonatomic) NSUInteger matchID;


@end
