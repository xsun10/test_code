//
//  VenueDetailTVC.h
//  Phenom
//
//  Created by James Chung on 7/2/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MapViewTVC.h"
#import "Venue.h"
#import "VDMapCell.h"
#import "VDKingCell.h"
#import "VDSportsCell.h"
#import "VDDescriptionCell.h"
#import "VDPhotosCell.h"

@class VenueDetailTVC;

@protocol VenueDetailTVC_Delegate <NSObject>
-(void)setVenueDetailTViewController:(VenueDetailTVC *)controller withVenue:(Venue *)venue;
@end



@interface VenueDetailTVC : MapViewTVC <VDMapCell_Delegate, VDPhotosCell_Delegate, VDKingCell_Delegate>

@property (nonatomic) NSInteger venueID;
@property (nonatomic) BOOL showChooseButton;
@property (nonatomic, weak) id <VenueDetailTVC_Delegate> delegate;

@end
