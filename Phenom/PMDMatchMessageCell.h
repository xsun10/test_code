//
//  PMDMatchMessageCell.h
//  Vaiden
//
//  Created by James Chung on 1/17/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PMDMatchMessageCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *profilePic;
@property (weak, nonatomic) IBOutlet UITextView *messageTextView;
@property (weak, nonatomic) IBOutlet UIView *myBackgroundView;

@end
