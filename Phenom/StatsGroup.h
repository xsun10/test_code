//
//  StatTagNSMutableArray.h
//  Vaiden
//
//  Created by James Chung on 6/5/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StatsGroup : NSObject

@property (nonatomic, strong) NSArray *statsArray;


- (StatsGroup *)initWithServerObject:(NSArray *)results;

@end
