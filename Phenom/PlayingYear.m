//
//  PlayingYear.m
//  Vaiden
//
//  Created by James Chung on 5/8/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "PlayingYear.h"

@implementation PlayingYear

- (PlayingYear *)initWithYear:(NSString *)year
{
    self = [super init];
    
    if (self) {
        _year = year;
    }
    return self;
    
}

- (void)setWithCurrentYear
{
    NSDate *date = [NSDate date];
    NSCalendar * calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *dateComponents = [calendar components:NSYearCalendarUnit fromDate:date];
    
    self.year = [NSString stringWithFormat:@"%ld", [dateComponents year]];
}

@end
