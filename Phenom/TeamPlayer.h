//
//  TeamPlayer.h
//  Vaiden
//
//  Created by James Chung on 4/7/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "Player.h"

@interface TeamPlayer : Player

@property (nonatomic, strong) NSString *memberStatus;

- (TeamPlayer *) initWithTeamPlayerDetails:(NSInteger)userID
                               andUserName:(NSString *)userName
                       andProfilePicString:(NSString *)profileBaseString
                           andMemberStatus:(NSString *)memberStatus;
@end
