//
//  AddTeamMatchTVC.m
//  Vaiden
//
//  Created by James Chung on 4/10/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "AddTeamMatchTVC.h"
#import "UserPreferences.h"
#import "TeamMatch.h"
#import "UIColor+VaidenColors.h"
#import "IOSRequest.h"
#import "MatchCreationSuccessVC.h"

@interface AddTeamMatchTVC ()

@property (nonatomic, strong) UserPreferences *preferences;
@property (weak, nonatomic) IBOutlet UIImageView *team2Icon;
@property (weak, nonatomic) IBOutlet UIImageView *team1Icon;
@property (weak, nonatomic) IBOutlet UILabel *team1NameLabel;
@property (weak, nonatomic) IBOutlet UILabel *team2NameLabel;

@property (weak, nonatomic) IBOutlet UITableViewCell *matchTypeCell;
@property (nonatomic, strong) TeamMatch *tMatch;
@property (weak, nonatomic) IBOutlet UITableViewCell *sportCell;

@property (nonatomic) UIDatePicker *datePicker;
@property (nonatomic) NSInteger datePickerHeight;
@property (nonatomic) BOOL datePickerIsShowing;
@property (nonatomic, strong) UIButton *closeDatePickerButton;
@property (nonatomic, strong) UIView *closeDatePickerView;
@property (weak, nonatomic) IBOutlet UITextField *matchNameTextField;
@property (weak, nonatomic) IBOutlet UITextView *matchMessageTextArea;
@property (strong, nonatomic)  UIBarButtonItem *doneButton;

@end

@implementation AddTeamMatchTVC

#define CHOOSE_SPORT_CELL 1
#define CHOOSE_MATCH_TYPE_CELL 2
#define CHOOSE_DATE_CELL 3
#define CHOOSE_VENUE_CELL 4

// For the action sheet
#define CANCEL_BUTTON_ACTIONSHEET @"Cancel "

#define TITLE_OF_ACTIONSHEET_FOR_CREATE_MATCH @"By creating this match you agree to adhere to our Player Guidelines."
#define CREATE_MATCH @"Create Match"


- (UIDatePicker *)datePicker
{
    if (!_datePicker) _datePicker = [[UIDatePicker alloc] init];
    return _datePicker;
}


- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
    
    

    
    self.tMatch = [[TeamMatch alloc] init];
    
    [self setTeamAttributes];
    
    self.datePicker.datePickerMode = UIDatePickerModeDateAndTime;
    self.datePicker.maximumDate = [NSDate dateWithTimeIntervalSinceNow:7776000];
    self.datePicker.minimumDate = [NSDate date];
    self.datePickerHeight = 44;
    self.datePickerIsShowing = NO;
    
    
    self.matchTypeCell.userInteractionEnabled = NO;
    self.matchTypeCell.textLabel.textColor = [UIColor lightGrayColor];
    self.matchTypeCell.detailTextLabel.textColor = [UIColor lightGrayColor];
    //[self setDoneButton];
    
}


/*- (void)setDoneButton
{
    UIButton *btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 50, 21);
    btn.layer.borderColor = [UIColor whiteColor].CGColor;
    btn.layer.borderWidth = 1.0;
    
    [btn addTarget:self action:@selector(done:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundColor:[UIColor clearColor]];
    [btn setTitle:@"DONE" forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-MediumItalic" size:12.0]];
    
    // Initialize UIBarbuttonitem...
    self.doneButton = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = self.doneButton;
}*/

- (IBAction)done:(id)sender
{
    [self showActionsheet:CREATE_MATCH];
}

- (void)setTeamAttributes
{
    self.team1NameLabel.text = self.team1.teamName;
    self.team2NameLabel.text = self.team2.teamName;
    
    self.team1Icon.image = [UIImage imageNamed:self.team1.picStringName];
    self.team2Icon.image = [UIImage imageNamed:self.team2.picStringName];
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.matchMessageTextArea resignFirstResponder];
    [self.matchNameTextField resignFirstResponder];
    
    UITableViewCell* cell = (UITableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    
 //   [self.matchNameTextField resignFirstResponder];
 //   [self.messageTextBlock resignFirstResponder];
    
    if (indexPath.row == CHOOSE_SPORT_CELL && indexPath.section == 0) {
        [self performSegueWithIdentifier:@"Choose Sport Segue" sender:self];
    } else if (indexPath.row == CHOOSE_MATCH_TYPE_CELL && indexPath.section == 0) {
        [self performSegueWithIdentifier:@"Choose Match Type Segue" sender:self];
    } else if (indexPath.row == CHOOSE_DATE_CELL && indexPath.section == 0 && !self.datePickerIsShowing) {
        
        cell.textLabel.text = @"";
        cell.detailTextLabel.text = @"";
        cell.accessoryType = UITableViewCellAccessoryNone;
        
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
        
        self.datePicker.alpha = 0;
        [self.tableView beginUpdates];
        self.datePickerHeight = 44 + 190;
        [self.tableView endUpdates];
        
        
        [cell addSubview:self.datePicker];
        [UIView animateWithDuration:0.2 animations:^{
            self.datePicker.alpha = 1;
            self.closeDatePickerView = [[UIView alloc] initWithFrame:CGRectMake(0, cell.frame.origin.y, 320, 40)];
            self.closeDatePickerView.backgroundColor = [UIColor darkGrayColor];
            self.closeDatePickerView.alpha = 0.5;
            
            self.closeDatePickerButton = [[UIButton alloc] initWithFrame:CGRectMake(260,  7, 50, 25)];
            [self.closeDatePickerButton setTitle:@"CLOSE" forState:UIControlStateNormal];
            [self.closeDatePickerButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-MediumItalic" size:11]];
            self.closeDatePickerButton.layer.borderColor = [UIColor lightGrayColor].CGColor;
            self.closeDatePickerButton.layer.borderWidth = 1.0;
            
            [self.closeDatePickerButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
            [self.closeDatePickerButton addTarget:self action:@selector(tapDate:) forControlEvents:UIControlEventTouchUpInside];
            
            [self.closeDatePickerView addSubview:self.closeDatePickerButton];
            [self.tableView addSubview:self.closeDatePickerView];
            
        } completion:nil];
        self.datePickerIsShowing = YES;
        
        
    } else if (indexPath.row == CHOOSE_DATE_CELL && indexPath.section == 0 && self.datePickerIsShowing) {
        //      [self performCloseCell:indexPath];
        //      self.datePickerIsShowing = NO;
        //      cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    } else if (indexPath.row == CHOOSE_VENUE_CELL && indexPath.section == 0) {
        [self performSegueWithIdentifier:@"Choose Venue Segue" sender:self];
    }
}


- (void)tapDate:(UIButton *)sender
{
 //   [self.matchNameTextField resignFirstResponder];
 //   [self.messageTextBlock resignFirstResponder];
    
    [UIView animateWithDuration:0.5 animations:^(void) {
        self.closeDatePickerView.alpha = 0.0;
        
        if (self.datePickerIsShowing) {
            [self performCloseCell:[NSIndexPath indexPathForRow:CHOOSE_DATE_CELL inSection:0]];
            self.datePickerIsShowing = NO;
        }
    }];
    
    
    
}

/*

- (NSIndexPath *)tableView:(UITableView *)tableView willDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.closeDatePickerButton.hidden = YES;
    self.closeDatePickerView.hidden = YES;
    self.closeDatePickerView = nil;
    self.closeDatePickerButton = nil;
    
    
    [self performCloseCell:indexPath];
    self.datePickerIsShowing = NO;
    
    return indexPath;
}*/

- (void)performCloseCell:(NSIndexPath *)indexPath
{
    
    if (indexPath.row == CHOOSE_DATE_CELL && indexPath.section == 0 && self.datePickerIsShowing) {
        
        [UIView animateWithDuration:0.2 animations:^{self.datePicker.alpha = 0;} completion:^(BOOL finished){[self.datePicker removeFromSuperview];}];
        UITableViewCell *cell = (UITableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
        NSDate *myDate = self.datePicker.date;
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MM/dd/yyyy hh:mm a"];
        cell.detailTextLabel.text = [dateFormat stringFromDate:myDate];
        cell.textLabel.text = @"Date";
        self.tMatch.dateTime = myDate;
        
        
        [self.tableView beginUpdates];
        
        if (indexPath.row == CHOOSE_DATE_CELL)
            self.datePickerHeight = 44;
        
        [self.tableView endUpdates];
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row == CHOOSE_DATE_CELL && indexPath.section == 0) {
        return self.datePickerHeight;
    } else if (indexPath.row == 0 && indexPath.section == 1) { // message text area
        return 170;
    }
    
    return 44;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Choose Sport Segue"]) {
        ChooseTeamSportTVC *controller = segue.destinationViewController;
        controller.delegate = self;
    } else if ([segue.identifier isEqualToString:@"Choose Match Type Segue"]) {
        ChooseMatchTypeTVC *controller = segue.destinationViewController;
        controller.delegate = self;
        controller.sportID = self.tMatch.sport.sportID;
        
    } else if ([segue.identifier isEqualToString:@"Choose Venue Segue"]) {
        VenuesTVC *controller = segue.destinationViewController;
        controller.delegate = self;
        controller.showChooseButton = YES;
        controller.showMakeTempVenueButtonOnSubsequentVC = YES;
    } else if ([segue.identifier isEqualToString:@"Team Match Creation Success"]) {
        MatchCreationSuccessVC *controller = segue.destinationViewController;
        controller.matchCategory = @"team";
    }
}
- (void)setWithSportFromVC:(ChooseTeamSportTVC *)controller withSport:(Sport *)sport
{
    self.sportCell.detailTextLabel.text = [sport.sportName capitalizedString];
    [self.tMatch setSport:sport];
    
    
    self.matchTypeCell.userInteractionEnabled = YES;
    self.matchTypeCell.textLabel.textColor = [UIColor blackColor];
    self.matchTypeCell.detailTextLabel.textColor = [UIColor newBlueLight];
    
}

- (void)setWithMatchType:(ChooseMatchTypeTVC *)controller withMatchType:(MatchType *)mType
{
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:CHOOSE_MATCH_TYPE_CELL inSection:0]];
    cell.detailTextLabel.text = mType.description;
    [self.tMatch setMatchType:mType];
}

- (void)setVenueViewController:(VenuesTVC *)controller withVenue:(Venue *)venue
{
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:CHOOSE_VENUE_CELL inSection:0]];
    cell.detailTextLabel.text = venue.venueName;
    [self.tMatch setVenue:venue];
}


- (void)showActionsheet:(NSString *)actionType
{
    if ([actionType isEqualToString:CREATE_MATCH]) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                      initWithTitle:TITLE_OF_ACTIONSHEET_FOR_CREATE_MATCH delegate:self
                                      cancelButtonTitle:CANCEL_BUTTON_ACTIONSHEET destructiveButtonTitle:nil
                                      otherButtonTitles:CREATE_MATCH,  nil];
        [actionSheet showFromTabBar:self.tabBarController.tabBar];
        
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == actionSheet.destructiveButtonIndex) {
        [self.view.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    } else {
        NSString *choice = [actionSheet buttonTitleAtIndex:buttonIndex];
        
        if ([choice isEqualToString:CREATE_MATCH]) {
            [self createMatch];
        }
    }
}

- (void)createMatch
{
    self.tMatch.team2 = self.team2;
    self.tMatch.team1 = self.team1;
    self.tMatch.matchCreatorID = [self.preferences getUserID];
    self.tMatch.matchName = self.matchNameTextField.text;
    self.tMatch.message = self.matchMessageTextArea.text;
    
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.labelText = @"Creating Match";
    self.doneButton.enabled = NO;

    
    [IOSRequest createTeamMatchWithInfo:self.tMatch
                           onCompletion:^(NSDictionary *results) {
                               
                               dispatch_async(dispatch_get_main_queue(), ^{
                                  
                                   self.doneButton.enabled = YES;
                                   [MBProgressHUD hideHUDForView:self.view animated:YES];
                                   
                                   if ([results[@"outcome"] isEqualToString:@"success"]) {
                                       [self performSegueWithIdentifier:@"Team Match Creation Success" sender:self];
                                   } else if ([results[@"limit"] isEqualToString:@"exceeded"]) {
                                       UIAlertView *alert = [[UIAlertView alloc] init];
                                       alert.title = @"Match Creation Limit Reach";
                                       alert.message = @"You have reached the limit of matches you can create for a given day.  Please try again tomorrow.";
                                       [alert addButtonWithTitle:@"OK"];
                                       [alert show];
                                   } else {
                                       UIAlertView *alert = [[UIAlertView alloc] init];
                                       alert.title = @"Match Creation Error Error";
                                       alert.message = @"There was a problem completing your request.  Please try again later.";
                                       [alert addButtonWithTitle:@"OK"];
                                       [alert show];
                                   }
                               });
                           }];
    
    double delayInSeconds = 10.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        //code to be executed on the main queue after delay
        self.doneButton.enabled = YES;
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    });
    
    
}

@end
