//
//  MatchesNearbyTVC.h
//  Phenom
//
//  Created by James Chung on 5/22/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "CustomBaseTVC.h"

@interface MatchesNearbyTVC : CustomBaseTVC <CLLocationManagerDelegate>
{
    CLLocationManager *locationManager;
}

@end
