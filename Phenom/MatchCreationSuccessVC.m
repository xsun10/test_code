//
//  MatchCreationSuccessVC.m
//  Phenom
//
//  Created by James Chung on 4/8/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "MatchCreationSuccessVC.h"
#import "UIButton+RoundBorder.h"
#import "MatchCreationSuccessCVCell.h"
#import "UIColor+VaidenColors.h"
#import "UIView+Manipulate.h"
#import "UserPreferences.h"
#import "ChallangeMatchHowItWorksVC.h"
#import "UIImage+ImageEffects.h"

@interface MatchCreationSuccessVC ()
@property (weak, nonatomic) IBOutlet UIButton *okButton;
@property (weak, nonatomic) IBOutlet UICollectionView *slidesCollectionView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (nonatomic) NSInteger numPages;
@property (weak, nonatomic) IBOutlet UILabel *nextStepsLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UIView *highlightBar;
@property (nonatomic, strong) UserPreferences *preferences;
@property (weak, nonatomic) IBOutlet UIButton *howItWorksButton;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@end


@implementation MatchCreationSuccessVC

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];

    //self.slidesCollectionView.delegate = self;
    //self.slidesCollectionView.dataSource = self;
    
    //self.highlightBar.backgroundColor = [UIColor peacock];
	// Do any additional setup after loading the view.
 //   [self.okButton makeRoundBorderWithColor:[UIColor colorWithRed:255.0/255.0 green:102.0/255.0 blue:102.0/255.0 alpha:1.0]];
    
    //[self.pageControl setNumberOfPages:self.numPages];
    //[self.pageControl setCurrentPageIndicatorTintColor:[UIColor peacock]];
    //self.nextStepsLabel.textColor = [UIColor peacock];
 //   [self.slidesCollectionView reloadData];
    
    [self.navigationItem setHidesBackButton:YES animated:YES];
    //[self setDoneButton];
    
    [self setBlurredBackgroundImage:1];
    
    [self.howItWorksButton makeRoundedBorderWithRadius:3];
}

- (void)setBlurredBackgroundImage:(NSUInteger)imageIndex
{
    NSString *effectText = @"";
    UIImage *effectImage = nil;
    
    effectImage = self.imageView.image;
    
    switch (imageIndex)
    {
        case 0:
            effectImage = self.imageView.image;
            break;
        case 1:
            effectImage = [self.imageView.image applyMidLightEffectLessBlur];
            effectText = NSLocalizedString(@"Light", @"");
            //           self.effectLabel.textColor = [UIColor whiteColor];
            break;
        case 2:
            effectImage = [self.imageView.image applyExtraLightEffect];
            effectText = NSLocalizedString(@"Extra light", @"");
            //            self.effectLabel.textColor = [UIColor lightGrayColor];
            break;
        case 3:
            effectImage = [self.imageView.image applyDarkEffect];
            effectText = NSLocalizedString(@"Dark", @"");
            //           self.effectLabel.textColor = [UIColor darkGrayColor];
            break;
        case 4:
            effectImage = [self.imageView.image applyTintEffectWithColor:[UIColor blueColor]];
            effectText = NSLocalizedString(@"Color tint", @"");
            //           self.effectLabel.textColor = [UIColor darkGrayColor];
            break;
    }
    
    self.imageView.image = effectImage;
    
}


- (IBAction)buttonClicked:(id)sender {
    [self performSegueWithIdentifier:@"how_it_works_segue" sender:self];
}

/*- (void)setDoneButton
{
    UIButton *btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 50, 21);
    btn.layer.borderColor = [UIColor whiteColor].CGColor;
    btn.layer.borderWidth = 1.0;
    
    [btn addTarget:self action:@selector(okAction:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundColor:[UIColor clearColor]];
    [btn setTitle:@"DONE" forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-MediumItalic" size:12.0]];
    
    // Initialize UIBarbuttonitem...
    UIBarButtonItem *proe = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = proe;
}*/



- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    self.scrollView.contentSize = CGSizeMake(320.0, 600.0);
}

- (IBAction)okAction:(id)sender
{
    [self.preferences setNewsfeedRefreshState:YES];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"how_it_works_segue"]) {
        ChallangeMatchHowItWorksVC *vc = segue.destinationViewController;
        if ([self.matchCategory isEqualToString:@"individual"]) {
            [vc setSegueName:@"challange_success"];
        } else if ([self.matchCategory isEqualToString:@"pickup"]) {
            [vc setSegueName:@"pickup_success"];
        }
    }
}

/*- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.numPages;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"My Cell";
    
    MatchCreationSuccessCVCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    [self setSlidePage:indexPath.row forCell:cell];
    cell.slideNum.text = [NSString stringWithFormat:@"Step %ld", indexPath.row + 1];
    [cell.slideNumBackground makeCircleWithColorAndBackground:[UIColor peacock] andRadius:15];
    [self.pageControl setCurrentPage:indexPath.row];
    
    return  cell;
}

-(void)setSlidePage:(NSInteger)pageNum forCell:(MatchCreationSuccessCVCell *)cell
{
    if ([self.matchCategory isEqualToString:@"individual"]) {
        switch (pageNum) {
            case 0:
                cell.headingLabel.text = @"Opponent Confirmation";
                cell.slideText.text = @"Your match is still Pending until your opponent accepts your challenge.";
                cell.iconPic.image = [[UIImage imageNamed:@"Clock Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor darkGrayColor]];
                break;
            case 1:
                cell.headingLabel.text = @"Play Your Game";
                cell.slideText.text = @"Once your opponent accepts your challenge, you will meet to play your match at the designated date / time and location.";
                cell.iconPic.image = [[UIImage imageNamed:@"Player Large"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor darkGrayColor]];
                break;
            case 2:
                cell.headingLabel.text = @"Record & Confirm Score";
                cell.slideText.text = @"After playing your match, one person must record the match score in the Vaiden app.  The other player must confirm the score.";
                cell.iconPic.image = [[UIImage imageNamed:@"Record Cloud Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor darkGrayColor]];
                break;
            case 3:
                cell.headingLabel.text = @"New Sports Level";
                cell.slideText.text = @"Once the score is confirmed, the match will become official. All players' sport levels will be updated.";
                cell.iconPic.image = [[UIImage imageNamed:@"Level Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor darkGrayColor]];
                break;
                
        }
    } else if ([self.matchCategory isEqualToString:@"pickup"]) {
        switch (pageNum) {
            case 0:
                cell.headingLabel.text = @"Pending RSVP's";
                cell.slideText.text = @"Your match is Pending until enough players RSVP to join it.  The number of players needed is determined by the type of match you play.";
                cell.iconPic.image = [[UIImage imageNamed:@"Clock Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor darkGrayColor]];
                break;
            case 1:
                cell.headingLabel.text = @"Ready To Play";
                cell.slideText.text = @"Once enough people RSVP to play, your match will become Ready To Play.";
                cell.iconPic.image = [[UIImage imageNamed:@"Check Mark"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor darkGrayColor]];
                break;
            case 2:
                cell.headingLabel.text = @"Play In The Pickup Match";
                cell.slideText.text = @"Show up for your match at the designated date / time and location";
                cell.iconPic.image = [[UIImage imageNamed:@"Player Large"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor darkGrayColor]];
                break;
                
        }
    } else if ([self.matchCategory isEqualToString:@"team"]) {
        switch (pageNum) {
            case 0:
                cell.headingLabel.text = @"Opposing Team Confirmation";
                cell.slideText.text = @"Your match is still Pending until the opposing team captain accepts your challenge.";
                cell.iconPic.image = [[UIImage imageNamed:@"Clock Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor darkGrayColor]];
                break;
                
            case 1:
                cell.headingLabel.text = @"Play Your Game";
                cell.slideText.text = @"Once the other team accepts your challenge, you will meet to play your match at the designated date / time and location. As captain, it is your responsiblity to make sure your team members show up to the match.";
                cell.iconPic.image = [[UIImage imageNamed:@"Player Large"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor darkGrayColor]];

                break;
                
            case 2:
                cell.headingLabel.text = @"Record & Confirm Score";
                cell.slideText.text = @"After playing your match, one team captain must record the match score in the Vaiden app.  The other team captain must confirm the score.";
                cell.iconPic.image = [[UIImage imageNamed:@"Record Cloud Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor darkGrayColor]];

                break;
                
            case 3:
                cell.headingLabel.text = @"New Sports Level";
                cell.slideText.text = @"Once the score is confirmed, the match will become official. All teams' sports levels will be updated.";
                cell.iconPic.image = [[UIImage imageNamed:@"Level Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor darkGrayColor]];
                break;
            default:
                break;
        }
    }
    
}*/

@end
