//
//  ChooseSeasonTVC.m
//  Vaiden
//
//  Created by James Chung on 5/8/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "ChooseSeasonTVC.h"

@interface ChooseSeasonTVC ()

@end

@implementation ChooseSeasonTVC


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.tableView reloadData];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Season Cell" forIndexPath:indexPath];
    
    cell.textLabel.text = [[self getSeasonForRow:indexPath.row] capitalizedString];
    
    if ([[self getSeasonForRow:indexPath.row] isEqualToString:self.chosenSeason]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    
    return cell;
}

- (NSString *)getSeasonForRow:(NSInteger)row
{
    if (row == 0) {
        return @"winter";
    } else if (row == 1) {
        return @"spring";
    } else if (row == 2) {
        return @"summer";
    } else if (row == 3) {
        return @"fall";
    }
    return @"";
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    for (int i=0; i < 3; i++) { // first clear
        UITableViewCell *clearCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
        clearCell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
    
    [self.delegate setWithViewController:self withSeason:cell.textLabel.text];
    [self.navigationController popViewControllerAnimated:YES];
}

@end
