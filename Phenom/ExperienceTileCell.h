//
//  ExperienceTileCell.h
//  Vaiden
//
//  Created by Turbo on 7/28/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExperienceTileCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleName;
@property (weak, nonatomic) IBOutlet UITextField *titleInput;

@end
