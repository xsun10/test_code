//
//  PDNewsPickupMatchPostCell.m
//  Vaiden
//
//  Created by James Chung on 2/22/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "PDNewsPickupMatchPostCell.h"
#import "NewsfeedMatchHeadingCVCell.h"
#import "S3Tools.h"
#import "AFNetworking.h"
#import "UIImage+ProportionalFill.h"


@implementation PDNewsPickupMatchPostCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (CGFloat)cellsMargin {
    
    return 4;
    
}

- (void)layoutSubviews
{
    self.matchHeadingCV.delegate = self;
    self.matchHeadingCV.dataSource = self;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    if ([self.matchCategory isEqualToString:@"individual"]) {
        return 3;
    } else {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"status = %@", @"ACCEPTED"];
        NSArray *matchParticipants = [self.competitors filteredArrayUsingPredicate:predicate];
        
        return [matchParticipants count];
    }
    
    return 0;
}

- (IBAction)goToProfile:(UIButton *)sender
{
    [self.delegate setPDNewsPickupMatchPostCellViewController:self withUserIDToSegue:sender.tag];
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Competitor Pics";
    
    NewsfeedMatchHeadingCVCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    
    Player *p = nil;
    
    if ([self.matchCategory isEqualToString:@"individual"]) {
        if (indexPath.row == 1) {
            // show "vs" not the pic
            cell.playerPic.hidden = YES;
            UILabel *vsLabel = [[UILabel alloc] initWithFrame:CGRectMake(8, 0, 50, 50)];
            vsLabel.text = @"VS";
            [vsLabel setFont:[UIFont fontWithName:@"HelveticaNeue-BoldItalic" size:22]];
            
            [cell addSubview:vsLabel];
            
            
        } else {
            
            
            if (indexPath.row == 2)
                p  = [self.competitors objectAtIndex:1];
            else
                p = [self.competitors objectAtIndex:0];
            
            NSString *urlMain = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                        baseString:p.profileBaseString];
            
            [cell.playerPic setImageWithURL:[NSURL URLWithString:urlMain]
                           placeholderImage:[UIImage imageNamed:@"avatar square"]];
            [UIImage makeRoundedImage:cell.playerPic withRadius:25];
        }
        
        return  cell;
    } else {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"status = %@", @"ACCEPTED"];
        NSArray *matchParticipants = [self.competitors filteredArrayUsingPredicate:predicate];
        
        //   if (indexPath.item < [matchParticipants count]) {
        p = matchParticipants[indexPath.row];
        
        
        //      p = [self.competitors objectAtIndex:indexPath.row];
        NSString *urlMain = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                    baseString:p.profileBaseString];
        
        [cell.playerPic setImageWithURL:[NSURL URLWithString:urlMain]
                       placeholderImage:[UIImage imageNamed:@"avatar square"]];
    //    [UIImage makeRoundedImage:cell.playerPic withRadius:25];
        
        cell.playerPic.layer.borderColor = [UIColor whiteColor].CGColor;
        cell.playerPic.layer.borderWidth = 1;
        
        // profile pic button
        //UIButton *picButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];
        //[picButton addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
        cell.picButton.tag = p.userID;
        //[cell.playerPic addSubview:picButton];
        if (p.userID == self.currentUserID) { // users themselves
            cell.picButton.hidden = YES;
        } else {
            cell.picButton.hidden = NO;
        }
        cell.playerPic.userInteractionEnabled = YES;
        cell.playerUsername.text = p.userName;
        
        return  cell;
    }
    
    return nil;
}

@end
