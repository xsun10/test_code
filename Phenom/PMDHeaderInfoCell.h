//
//  PMDHeaderInfoCell.h
//  Vaiden
//
//  Created by James Chung on 1/16/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMDHeaderInfoButtonCVCell.h"
#import "PickupMatch.h"


@class PMDHeaderInfoCell;

@protocol PMDHeaderInfoCell_Delegate <NSObject>
- (void)setWithButtonMessage:(PMDHeaderInfoCell *)controller withMessage:(NSString *)messageAction;
- (void)setWithProfileIDForVC:(PMDHeaderInfoCell *)controller withUserIDToSegue:(NSInteger)userID;
@end


@interface PMDHeaderInfoCell : UITableViewCell <UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, weak) id <PMDHeaderInfoCell_Delegate> delegate;


@property (weak, nonatomic) IBOutlet UIImageView *backgroundPic;
@property (weak, nonatomic) IBOutlet UILabel *matchNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *matchTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *sportLabel;
@property (weak, nonatomic) IBOutlet UILabel *privatePublicLabel;
@property (weak, nonatomic) IBOutlet UIImageView *sportIcon;

@property (weak, nonatomic) IBOutlet UIButton *button1;
@property (weak, nonatomic) IBOutlet UIButton *button2;
@property (weak, nonatomic) IBOutlet UIButton *button3;
@property (weak, nonatomic) IBOutlet UIButton *button4;
@property (nonatomic, strong) PickupMatch *pickupMatch;
@property (nonatomic) NSInteger sessionUserID;
@property (weak, nonatomic) IBOutlet UILabel *numJoinedOrNeededLabel;

@property (nonatomic) BOOL isSessionUserConfirmed;

- (void)configureButtonDisplay:(NSInteger)matchButtonConfig;
@property (weak, nonatomic) IBOutlet UICollectionView *buttonCollectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *playerCollectionView;


- (void)initPlayers;



@end
