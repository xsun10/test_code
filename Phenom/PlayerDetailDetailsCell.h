//
//  PlayerDetailDetailsCell.h
//  Vaiden
//
//  Created by James Chung on 3/17/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlayerDetailDetailsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *genderLabel;
@property (weak, nonatomic) IBOutlet UILabel *ageLabel;
@property (weak, nonatomic) IBOutlet UILabel *heightLabel;
@property (weak, nonatomic) IBOutlet UILabel *aboutTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *aboutUsernameLabel;
@end
