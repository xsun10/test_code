//
//  MatchNewsScoreUpdateCell.h
//  Vaiden
//
//  Created by James Chung on 10/30/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewsfeedMatchHeadingCV.h"
#import "IndividualMatch.h"

@class MatchNewsScoreUpdateCell;

@protocol MatchNewsScoreUpdateCell_delegate <NSObject>
-(void)setMatchNewsScoreUpdateCellParentViewController:(MatchNewsScoreUpdateCell *)controller withUserIDToSegue:(NSInteger)userID;
@end





@interface MatchNewsScoreUpdateCell : UITableViewCell <UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, weak) id <MatchNewsScoreUpdateCell_delegate> delegate;

@property (weak, nonatomic) IBOutlet UILabel *matchNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *picView;
@property (weak, nonatomic) IBOutlet UIView *picViewBackground;
@property (weak, nonatomic) IBOutlet UILabel *headlineMessage;
@property (weak, nonatomic) IBOutlet UILabel *dateTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *scoreStatusLabel;
@property (weak, nonatomic) IBOutlet UILabel *player1Username;
@property (weak, nonatomic) IBOutlet UILabel *player2Username;
@property (weak, nonatomic) IBOutlet UILabel *player1ScoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *player2ScoreLabel;
@property (weak, nonatomic) IBOutlet UIButton *detailsButton;
@property (weak, nonatomic) IBOutlet UILabel *supplementaryMessageLabel;
@property (weak, nonatomic) IBOutlet NewsfeedMatchHeadingCV *matchHeadingCV;
@property (weak, nonatomic) IBOutlet UICollectionView *scoresCV;


@property (weak, nonatomic) IBOutlet UIView *detailButtonView;
@property (weak, nonatomic) IBOutlet UIView *shareButtonView;
@property (weak, nonatomic) IBOutlet UIView *commentButtonView;
@property (weak, nonatomic) IBOutlet UILabel *numCommentLabel;
@property (weak, nonatomic) IBOutlet UILabel *numRemixLabel;
@property (weak, nonatomic) IBOutlet UIView *headlineBackground;
@property (weak, nonatomic) IBOutlet UIButton *commentButton;
@property (weak, nonatomic) IBOutlet UIButton *remixButton;
@property (weak, nonatomic) IBOutlet UIImageView *shareIcon;
@property (weak, nonatomic) IBOutlet UILabel *sharedByLabel;
@property (weak, nonatomic) IBOutlet UIView *shareDot;

@property (nonatomic, strong) NSString *matchCategory;
@property (nonatomic, strong) NSMutableArray *competitors;
@property (weak, nonatomic) IBOutlet UIView *centerBackgroundView;

//@property (nonatomic, strong) IndividualMatchScore *matchScore;
@property (nonatomic, strong ) IndividualMatch *iMatch;

@end
