//
//  NoResultTVC.m
//  Vaiden
//
//  Created by Turbo on 6/13/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "NoResultTVC.h"

@implementation NoResultTVC

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
