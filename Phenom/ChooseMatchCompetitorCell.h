//
//  ChooseMatchCompetitorCell.h
//  Phenom
//
//  Created by James Chung on 4/5/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChooseMatchCompetitorCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *opponentImage;
@property (weak, nonatomic) IBOutlet UILabel *opponentUsernameLabel;
//@property (weak, nonatomic) IBOutlet UILabel *opponentLocationLabel;
@property (weak, nonatomic) IBOutlet UILabel *levelLabel;
@property (weak, nonatomic) IBOutlet UILabel *sportLabel;

@end
