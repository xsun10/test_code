//
//  VenueCell.h
//  Phenom
//
//  Created by James Chung on 5/23/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VenueCell : UITableViewCell <UICollectionViewDataSource, UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *venueNameLabel;
//@property (weak, nonatomic) IBOutlet UIImageView *mavpView;
@property (weak, nonatomic) IBOutlet UILabel *streetLabel;
@property (weak, nonatomic) IBOutlet UILabel *cityStateLabel;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *sportsCV;
@property (weak, nonatomic) IBOutlet UIButton *viewDetailsButton;
@property (weak, nonatomic) IBOutlet UIButton *chooseButton;
@property (weak, nonatomic) IBOutlet UIButton *favoriteButton;
@property (weak, nonatomic) IBOutlet UIImageView *starRating;
@property (weak, nonatomic) IBOutlet UIImageView *venuePic;
@property (weak, nonatomic) IBOutlet UIImageView *blackFade;

@property (nonatomic, strong) NSMutableArray *sportsArray;

@end
