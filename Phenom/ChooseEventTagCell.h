//
//  ChooseEventTagCell.h
//  Vaiden
//
//  Created by James Chung on 5/28/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChooseEventTagCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *eventNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *eventAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *eventDateTimeLabel;

@end
