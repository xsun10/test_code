//
//  CreateTeamStep3IconCell.h
//  Vaiden
//
//  Created by James Chung on 4/7/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreateTeamStep3IconCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *clipArtPic;

@end
