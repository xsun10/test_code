//
//  PlayerAlertTag.m
//  Vaiden
//
//  Created by James Chung on 5/16/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "PlayerAlertTag.h"

@implementation PlayerAlertTag

- (PlayerAlertTag *)initWithPlayerAlertTagDetails:(NSInteger)userID
                                      andUserName:(NSString *)userName
                              andProfilePicString:(NSString *)profilePicString
                                       andMessage:(NSString *)message
                                     andAlertType:(NSInteger)alertType
                                     andAlertDate:(NSDate *)alertDate
                                     andWasViewed:(BOOL)wasViewed
                                     withLockerID:(NSInteger)lockerID
                                     andImageName:(NSString *)imageName
{
    self = [super initWithPlayerAlertDetails:userID
                                 andUserName:userName
                         andProfilePicString:profilePicString
                                  andMessage:message
                                andAlertType:alertType
                                andAlertDate:alertDate
                                andWasViewed:wasViewed];
    
    if (self) {
        _tagDataArray = [[NSMutableArray alloc] init];
        LockerPic *lp = [[LockerPic alloc] init];
        [lp setLockerID:lockerID];
        [lp setImageFileName:imageName];
        [_tagDataArray addObject:lp];
    }
    
    return self;
}
@end
