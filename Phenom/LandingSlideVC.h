//
//  LandingSlideVC.h
//  Phenom
//
//  Created by James Chung on 6/1/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomBaseVC.h"

@interface LandingSlideVC : CustomBaseVC <UICollectionViewDataSource, UICollectionViewDelegate>

@end
