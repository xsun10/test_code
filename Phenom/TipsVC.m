//
//  TipsVC.m
//  Vaiden
//
//  Created by Turbo on 8/4/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "TipsVC.h"
#import "TipsCell.h"
@interface TipsVC ()
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@end

@implementation TipsVC
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationBar.topItem.title = @"";
    // Do any additional setup after loading the view.
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    BOOL isIPhone = [[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone;
    BOOL isIPhone5 = isIPhone && ([[UIScreen mainScreen] bounds].size.height > 480.0);
    if (isIPhone5) {
        
    } else {
        // [self.collectionHeight setConstant:480];
    }
    
    
}


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 4;
}

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section;
{
    return 1;
}



- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    TipsCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"CollectionCellID" forIndexPath:indexPath];
    
    // load the image for this cell
    NSString * imagename = [[NSString alloc] initWithFormat:@"Tips%d", indexPath.section];
    
    
    UIImage *image = [UIImage imageNamed:imagename];
    NSLog(@"%@",image);
    cell.image.image = image;
    NSLog(@"%@",cell);
    if(indexPath.section==4){
    cell.image.backgroundColor=[UIColor whiteColor];
        cell.image.alpha=0;
        cell.image.hidden=YES;
    
    }
    
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    if(indexPath.section==4){
    return CGSizeMake(320.f, 100.f);
    }
    return CGSizeMake(320.f, 450.f);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
