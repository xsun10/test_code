//
//  IMDHeaderInfoCell.h
//  Vaiden
//
//  Created by James Chung on 12/26/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@class IMDHeaderInfoCell; 

@protocol IMDHeaderInfoCell_Delegate <NSObject>
- (void)setWithButtonMessage:(IMDHeaderInfoCell *)controller withMessage:(NSString *)messageAction;

@end



@interface IMDHeaderInfoCell : UITableViewCell <UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, weak) id <IMDHeaderInfoCell_Delegate> delegate;


@property (weak, nonatomic) IBOutlet UIImageView *player1Pic;
@property (weak, nonatomic) IBOutlet UIImageView *player2Pic;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundPic;
@property (weak, nonatomic) IBOutlet UILabel *matchNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *vsLabel;
@property (weak, nonatomic) IBOutlet UILabel *sportLabel;
@property (weak, nonatomic) IBOutlet UIButton *button1;
@property (weak, nonatomic) IBOutlet UIButton *button2;
@property (weak, nonatomic) IBOutlet UIButton *button3;
@property (weak, nonatomic) IBOutlet UIButton *button4;
@property (weak, nonatomic) IBOutlet UICollectionView *buttonCollectionView;
@property (weak, nonatomic) IBOutlet UIImageView *sportIcon;
@property (weak, nonatomic) IBOutlet UILabel *username1Label;
@property (weak, nonatomic) IBOutlet UILabel *username2Label;
@property (weak, nonatomic) IBOutlet UIView *usernameBackgroundArea;

- (void)configureButtonDisplay:(NSInteger)matchButtonConfig;

#define BUTTON_CONFIG_EMPTY_SCORE 0
#define BUTTON_CONFIG_SHOW_SCORE 1
#define BUTTON_CONFIG_CONFIRM_BUTTONS 2
#define BUTTON_CONFIG_RECORD_BUTTONS 3
#define BUTTON_CONFIG_EMPTY_MATCH_OUTCOME 4
#define BUTTON_CONFIG_CANCELLED_MATCH 5
#define BUTTON_CONFIG_MATCH_CREATOR 6
#define BUTTON_CONFIG_CONFIRMED_MATCH_PARTICIPANT 7
#define BUTTON_CONFIG_UNCONFIRMED_MATCH_PARTICIPANT 8
#define BUTTON_CONFIG_MINIMAL_DISPLAY 9
@end
