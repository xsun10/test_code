//
//  LPDTaggedUsersCell.m
//  Vaiden
//
//  Created by James Chung on 5/14/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "LPDTaggedUsersCell.h"
#import "PlayersTaggedCell.h"
#import "S3Tools.h"
#import "AFNetworking.h"
#import "Player.h"

@implementation LPDTaggedUsersCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)myInit
{
    self.usersCollectionView.delegate = self;
    self.usersCollectionView.dataSource = self;
}
- (void)awakeFromNib
{
    // Initialization code
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.taggedUsers count];
}

- (void)goToProfile:(UIButton *)sender
{
    [self.delegate setWithCell:self withUserID:sender.tag];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PlayersTaggedCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Tagged User Cell" forIndexPath:indexPath];
    
    Player *p = [self.taggedUsers objectAtIndex:indexPath.row];
    
    NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                            baseString:p.profileBaseString];
    
    [cell.profilePic setImageWithURL:[NSURL URLWithString:url]
                    placeholderImage:[UIImage imageNamed:@"avatar square"]];
    
    
    UIButton *picButton1 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    [picButton1 addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
    picButton1.tag = p.userID;
    [cell.profilePic addSubview:picButton1];
    cell.profilePic.userInteractionEnabled = YES;

    
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    Player *p = [self.taggedUsers objectAtIndex:indexPath.row];
    
    [self.delegate setWithCell:self withUserID:p.userID];
}
@end
