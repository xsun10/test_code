//
//  VotePuchaseTier1Helper.h
//  Vaiden
//
//  Created by Turbo on 8/4/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "IAPHelper.h"

@interface VotePuchaseTier1Helper : IAPHelper

+ (VotePuchaseTier1Helper *)sharedInstance;

@end
