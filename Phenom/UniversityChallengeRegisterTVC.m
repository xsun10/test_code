//
//  UniversityChallengeRegisterTVC.m
//  Vaiden
//
//  Created by James Chung on 2/28/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "UniversityChallengeRegisterTVC.h"
#import "IOSRequest.h"
#import "UserPreferences.h"
#import "University.h"
#import "UITextField+FormValidations.h"

@interface UniversityChallengeRegisterTVC ()
@property (weak, nonatomic) IBOutlet UITextField *textField1; // school email address
@property (weak, nonatomic) IBOutlet UITextField *textField2; // school organization
@property (nonatomic, strong) UserPreferences *preferences;

@property (nonatomic, strong) University *selectedUniversity;
@property (nonatomic, strong) UIBarButtonItem *doneButton;

@end

@implementation UniversityChallengeRegisterTVC

#define UNIVERSITY_ROW 0

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];

    //[self setDoneButton];
}


/*- (void)setDoneButton
{
    UIButton *btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 50, 21);
    btn.layer.borderColor = [UIColor whiteColor].CGColor;
    btn.layer.borderWidth = 1.0;
    
    [btn addTarget:self action:@selector(doneAction) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundColor:[UIColor clearColor]];
    [btn setTitle:@"DONE" forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-MediumItalic" size:12.0]];
    
    // Initialize UIBarbuttonitem...
    self.doneButton = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = self.doneButton;
}*/

- (IBAction)doneAction:(id)sender
{
    [self.textField1 resignFirstResponder];
    [self.textField2 resignFirstResponder];
    
    if ([self validateFields]) {
        
        self.doneButton.enabled = NO;
        HUD = [MBProgressHUD showHUDAddedTo:self.view.superview animated:YES];
        HUD.labelText = @"Registering";

        [IOSRequest registerForCompetitionUser:[self.preferences getUserID]
                              withUniversityID:self.selectedUniversity.universityID
                                andSchoolEmail:self.textField1.text
                         andSchoolOrganization:self.textField2.text
                                  onCompletion:^(NSDictionary *results) {
                                      
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          
                                          self.doneButton.enabled = YES;
                                          [MBProgressHUD hideHUDForView:self.view animated:YES];// just

                                          if ([results[@"outcome"] isEqualToString:@"failure"]) {
                                              
                                          } else {
                                              [self performSegueWithIdentifier:@"success segue" sender:self];
                                          }
                                      });
                                  }];
    }
    double delayInSeconds = 10.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        //code to be executed on the main queue after delay
        self.doneButton.enabled = YES;
        [MBProgressHUD hideHUDForView:self.view animated:YES];// just
    });
}

- (BOOL)validateFields
{
    BOOL result = YES;
    
    if (![self.textField1 checkEmail] || [self.textField1.text rangeOfString:@".edu"].location == NSNotFound) {
      //  result = NO;
        result = YES;
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Invalid Email";
        alert.message = @"Please enter a valid .edu email address";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
    } else if (![self.textField2 checkMatchName] && [self.textField2.text length] > 0) {
        result = NO;
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Invalid Organization";
        alert.message = @"Please enter a valid organization name";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
    } else if ([self.selectedUniversity.universityName length] == 0) {
        result = NO;
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"University Missing";
        alert.message = @"Please choose your university";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
    }
    return result;
}

- (IBAction)tapElsewhereOnTableView:(id)sender
{
    [self.textField1 resignFirstResponder];
    [self.textField2 resignFirstResponder];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
        [self performSegueWithIdentifier:@"University List Segue" sender:self];
}
@end
