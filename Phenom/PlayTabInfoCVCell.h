//
//  PlayTabInfoCVCell.h
//  Vaiden
//
//  Created by James Chung on 8/23/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlayTabInfoCVCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UILabel *headingLabel;

@end
