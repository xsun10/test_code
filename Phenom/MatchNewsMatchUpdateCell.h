//
//  MatchNewsMatchUpdateCell.h
//  Vaiden
//
//  Created by James Chung on 10/18/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewsfeedMatchHeadingCV.h"

@class MatchNewsMatchUpdateCell;

@protocol MatchNewsMatchUpdateCell_delegate <NSObject>
-(void)setMatchNewsMatchUpdateCellParentViewController:(MatchNewsMatchUpdateCell *)controller withUserIDToSegue:(NSInteger)userID;
@end




@interface MatchNewsMatchUpdateCell : UITableViewCell <UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic, weak) id <MatchNewsMatchUpdateCell_delegate> delegate;


@property (weak, nonatomic) IBOutlet UIImageView *sportIcon;
@property (weak, nonatomic) IBOutlet UIView *sportIconBackground;
@property (weak, nonatomic) IBOutlet UILabel *matchName;
@property (weak, nonatomic) IBOutlet UILabel *headlineMessageLabel;
@property (weak, nonatomic) IBOutlet UILabel *generalMessageLabel;
@property (weak, nonatomic) IBOutlet UILabel *matchNameLabel2;
@property (weak, nonatomic) IBOutlet UILabel *matchDateTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *matchLocationLabel;
@property (weak, nonatomic) IBOutlet UIImageView *calendarIcon;
@property (weak, nonatomic) IBOutlet UIImageView *locationIcon;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIView *messageBackgroundView;
@property (weak, nonatomic) IBOutlet UIButton *detailButton;
@property (weak, nonatomic) IBOutlet UILabel *matchCategoryLabel;
@property (weak, nonatomic) IBOutlet NewsfeedMatchHeadingCV *matchHeadingCV;
@property (weak, nonatomic) IBOutlet UIImageView *bulbIcon;
@property (weak, nonatomic) IBOutlet UIView *bulbBackground;
@property (weak, nonatomic) IBOutlet UIView *detailButtonView;
@property (weak, nonatomic) IBOutlet UIView *commentButtonView;
@property (weak, nonatomic) IBOutlet UIView *shareButtonView;
@property (weak, nonatomic) IBOutlet UILabel *numCommentLabel;
@property (weak, nonatomic) IBOutlet UILabel *numRemixLabel;
@property (weak, nonatomic) IBOutlet UILabel *headlineAbbreviation;
@property (weak, nonatomic) IBOutlet UILabel *headlineMatchType;
@property (weak, nonatomic) IBOutlet UIView *headlineBackground;
@property (weak, nonatomic) IBOutlet UIButton *remixButton;
@property (weak, nonatomic) IBOutlet UIImageView *shareIcon;
@property (weak, nonatomic) IBOutlet UILabel *sharedByLabel;
@property (weak, nonatomic) IBOutlet UIView *shareDot;

@property (weak, nonatomic) IBOutlet UIButton *commentButton;
@property (nonatomic, strong) NSString *matchCategory;
@property (nonatomic, strong) NSMutableArray *competitors;
@end
