//
//  NewsfeedPaginator.h
//  Vaiden
//
//  Created by James Chung on 9/6/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "NMPaginator.h"

@interface NewsfeedPaginator : NMPaginator

#define NEWSFEED_MYNEWS_SEGMENT 0
#define NEWSFEED_BROWSE_SEGMENT 1
#define NEWSFEED_DETAIL_POST 2

@property (nonatomic) NSInteger tabToShow;
@property (nonatomic) NSInteger newsfeedDetailID;

- (void)fetchFirstPageWithTab:(NSInteger)tabToShow andNewsfeedDetailID:(NSInteger)newsfeedDetailID;

@end
