//
//  ChooseMatchTypeTVC.m
//  Vaiden
//
//  Created by James Chung on 8/6/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "ChooseMatchTypeTVC.h"
#import "MatchType.h"
#import "IOSRequest.h"

@interface ChooseMatchTypeTVC ()

@property (nonatomic, strong) NSMutableArray *matchTypesArray;
@property (nonatomic, strong) UIView *tempView;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
@end

@implementation ChooseMatchTypeTVC

- (NSMutableArray *)matchTypesArray
{
    if (!_matchTypesArray) _matchTypesArray = [[NSMutableArray alloc] init];
    return _matchTypesArray;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
    
    [self showTempSpinner];
    self.tableView.scrollEnabled = NO;
    
    [IOSRequest getMatchTypesForSport:self.sportID
                            onCompletion:^(NSMutableArray *results) {
        
                                for (id object in results) {
        MatchType *mType = [[MatchType alloc] initWithMatchType:[object[@"match_type_id"] integerValue]
                                                withDescription:object[@"match_type_description"]];
                                    [self.matchTypesArray addObject:mType];
                                }
                                dispatch_async(dispatch_get_main_queue(), ^ {
                                    [self.tableView reloadData];
                                    [self.activityIndicator stopAnimating];
                                    self.tempView.hidden = YES;
                                    self.tableView.scrollEnabled = YES;
                                
                                });
                                
    }];
}

- (void)showTempSpinner
{
    self.tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    self.tempView.backgroundColor = [UIColor lightGrayColor];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] init];
    [self.activityIndicator setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width/2.0, [UIScreen mainScreen].bounds.size.height/2.0 - 50)];
    [self.activityIndicator startAnimating];
    [self.tempView addSubview:self.activityIndicator];
    
    [self.view addSubview:self.tempView];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.matchTypesArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Type Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    MatchType *mType = [self.matchTypesArray objectAtIndex:indexPath.row];
    
    cell.textLabel.text = mType.description;
    
    // Configure the cell...
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MatchType *mType = [self.matchTypesArray objectAtIndex:indexPath.row];
    
    [self.delegate setWithMatchType:self withMatchType:mType];
    [self.navigationController popViewControllerAnimated:YES];
    
}


@end
