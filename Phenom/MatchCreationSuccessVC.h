//
//  MatchCreationSuccessVC.h
//  Phenom
//
//  Created by James Chung on 4/8/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomBaseVC.h"

@interface MatchCreationSuccessVC : CustomBaseVC //<UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, strong) NSString *matchCategory; // either 'individual' or 'pickup' or 'team'

@end
