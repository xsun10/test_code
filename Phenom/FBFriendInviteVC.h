//
//  FBFriendInviteVC.h
//  Vaiden
//
//  Created by James Chung on 9/18/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import "CustomBaseVC.h"

@interface FBFriendInviteVC : CustomBaseVC <FBFriendPickerDelegate>

- (IBAction)pickFriendsButtonClick:(id)sender;

@end
