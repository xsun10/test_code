//
//  PMDSessionUserStatusCell.h
//  Vaiden
//
//  Created by James Chung on 1/17/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PMDSessionUserStatusCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *statusIconBackground;
@property (weak, nonatomic) IBOutlet UIImageView *statusIconImage;
@property (weak, nonatomic) IBOutlet UIImageView *profilePic;
@property (weak, nonatomic) IBOutlet UILabel *playStatusLabel;
@property (weak, nonatomic) IBOutlet UIView *myBackgroundView;


@end
