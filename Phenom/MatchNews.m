//
//  MatchNews.m
//  Vaiden
//
//  Created by James Chung on 10/18/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "MatchNews.h"
#import "IndividualMatch.h"

@implementation MatchNews

- (MatchNews *)initWithDetails:(Match *)matchInfo
                      postDate:(NSDate *)newsDate
               headLineMessage:(NSString *)headlineMessage
                generalMessage:(NSString *)generalMessage

{
    self = [super initWithMinDetails:newsDate];
    
    if (self) {
        _match = matchInfo;
        _headlineMessageLabel = headlineMessage;
        _matchGeneralMessage = generalMessage;
    }
    return self;
}

// Returns an Error Code string if NO...and returns OK if post can be shared

- (NSString *)canPostBeSharedBySessionUser:(NSInteger)sessionUserID
{
    if (self.newsMakerUserID == sessionUserID) {
        return @"Sorry, you cannot share your own posts"; // error code
    } else if ([self.match isKindOfClass:[IndividualMatch class]]) {
        IndividualMatch *im = (IndividualMatch *)self.match;
        if (im.private == YES)
            return @"Private matches cannot be shared";
        
        if (im.player1.userID == sessionUserID || im.player2.userID == sessionUserID)
            return @"Your followers already see this post";
    
    }
    
    return @"OK";
}

@end
