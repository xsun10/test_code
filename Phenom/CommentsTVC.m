//
//  CommentsTVC.m
//  Phenom
//
//  Created by James Chung on 5/6/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "CommentsTVC.h"
#import "IOSRequest.h"
#import "UserPreferences.h"
#import "CommentCell.h"
#import "NewsComment.h"
#import "S3Tools.h"
#import "AFNetworking.h"
#import "UIImage+ProportionalFill.h"
#import "NSDate+Utilities.h"
#import "UIColor+VaidenColors.h"
#import "PlayerDetailTVC.h"
#import "CommentDialog.h"

@interface CommentsTVC () <CommentDialogDelegate>

@property (nonatomic, strong) NSMutableArray *comments;
@property (nonatomic, strong) UserPreferences *preferences;
@property (weak, nonatomic) IBOutlet UITextField *userTextInput;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UILabel *commentLabel;

@property (nonatomic, strong) UITextField *textField;
@property (nonatomic, strong) UIToolbar *toolbar;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *composeButton;
@property (nonatomic, strong) UIView *overlay;
@property (nonatomic, strong) UIView *tempView;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;

@property (nonatomic) NSString * text;
@end

@implementation CommentsTVC

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (NSMutableArray *)comments
{
    if (!_comments) _comments = [[NSMutableArray alloc] init];
    return _comments;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
    [self.comments removeAllObjects];
    [self setCommentTable];
    
    //[self composeCommentImplementation];
  
}

- (void)showTempSpinner
{
    self.tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    self.tempView.backgroundColor = [UIColor lightGrayColor];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] init];
    [self.activityIndicator setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width/2.0, [UIScreen mainScreen].bounds.size.height/2.0 )];
    [self.activityIndicator startAnimating];
    [self.tempView addSubview:self.activityIndicator];
    
    [self.view addSubview:self.tempView];
    
}


- (void)displayErrorOverlay
{
    [self.overlay removeFromSuperview];
    self.overlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, [UIScreen mainScreen].bounds.size.height)];
    self.overlay.backgroundColor = [UIColor lightLightGray];
    
    UIImage *img = [UIImage imageNamed:@"no_comments"];
    UIImageView *icon = [[UIImageView alloc] initWithImage:img];
    [icon setFrame:CGRectMake(140, 100, 50, 50)];
    [icon setContentMode:UIViewContentModeCenter];
    
    UILabel *firstLine = [[UILabel alloc] initWithFrame:CGRectMake(30, 180, 260, 21)];
    firstLine.textColor = [UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:1.0];
    firstLine.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:15];
    firstLine.text = @"No Comments Yet";
    [firstLine setTextAlignment:NSTextAlignmentCenter];
    
    UILabel *secondLine = [[UILabel alloc] initWithFrame:CGRectMake(25, 200, 280, 21)];
    secondLine.textColor = [UIColor colorWithRed:170/255.0 green:170/255.0 blue:170/255.0 alpha:1.0];
    secondLine.font = [UIFont fontWithName:@"HelveticaNeue" size:13];
    secondLine.text = @"This post does not have any comments yet.";
    secondLine.numberOfLines = 0;
    [secondLine setTextAlignment:NSTextAlignmentCenter];
    
    [self.overlay addSubview:icon];
    [self.overlay addSubview:firstLine];
    [self.overlay addSubview:secondLine];
    
    [self.tableView addSubview:self.overlay];
}

- (void)composeCommentImplementation
{
    
    self.toolbar = [[UIToolbar alloc] init];
    [self.toolbar setBarStyle:UIBarStyleBlack];
    [self.toolbar sizeToFit];
    
    self.textField = [[UITextField alloc] initWithFrame:CGRectMake(50, 0, 185, 30)];
    self.textField.backgroundColor = [UIColor whiteColor];
    self.textField.borderStyle = UITextBorderStyleRoundedRect;
    self.textField.font = [UIFont fontWithName:@"System" size:12];
    
    UIBarButtonItem *textFieldItem = [[UIBarButtonItem alloc] initWithCustomView:self.textField];
    
    
    
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    // Since the buttons can be any width we use a thin image with a stretchable center point
    UIImage *buttonImage = [[UIImage imageNamed:@"blue_button_med"] stretchableImageWithLeftCapWidth:5 topCapHeight:0];
    UIImage *buttonPressedImage = [[UIImage imageNamed:@"blue_button_med.png"] stretchableImageWithLeftCapWidth:5 topCapHeight:0];
    
    [[button titleLabel] setFont:[UIFont boldSystemFontOfSize:12.0]];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [button setTitleShadowColor:[UIColor colorWithWhite:1.0 alpha:0.7] forState:UIControlStateNormal];
    [button setTitleShadowColor:[UIColor clearColor] forState:UIControlStateHighlighted];
    [[button titleLabel] setShadowOffset:CGSizeMake(0.0, 1.0)];
    
    CGRect buttonFrame = [button frame];
    NSString *t = @"Done";
    
    buttonFrame.size.width = [t sizeWithFont:[UIFont boldSystemFontOfSize:12.0]].width + 24.0;
    buttonFrame.size.height = 30;
    [button setFrame:buttonFrame];
    
    [button setBackgroundImage:buttonImage forState:UIControlStateNormal];
    [button setBackgroundImage:buttonPressedImage forState:UIControlStateHighlighted];
    
    [button setTitle:t forState:UIControlStateNormal];
    
    [button addTarget:self action:@selector(recordComment) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    
    
    
    
    
    
    /*
     
     
     UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"                                                                                    style:UIBarButtonItemStylePlain
     target:self
     action:@selector(recordComment)];
     */
    //    [doneButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys: [UIFont fontWithName:@"System" size:10], UITextAttributeFont,nil] forState:UIControlStateNormal];
    [doneButton setTitleTextAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:12]} forState:UIControlStateNormal];
    
    /*
     UIBarButtonItem *fixedSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
     fixedSpace.width = 15;
     
     
     UIBarButtonItem *flexButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
     */
    
    NSArray *itemsArray = @[textFieldItem, doneButton];
    
    
    [self.toolbar setItems:itemsArray];
    [self.view.window addSubview:self.toolbar];
    
    [self.textField setInputAccessoryView:self.toolbar];
    [self.textField becomeFirstResponder]; [self.textField becomeFirstResponder];
    
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self showTempSpinner];
    
    self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
    self.modalPresentationStyle = UIModalPresentationCurrentContext;
    self.tabBarController.modalPresentationStyle = UIModalPresentationCurrentContext;
    /*
    NSMutableArray *toolbarButtons = [self.toolbarItems mutableCopy];
    
    // This is how you remove the button from the toolbar and animate it
    [toolbarButtons removeObject:self.composeButton];
    [self setToolbarItems:toolbarButtons animated:YES];*/
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    //[self.textField resignFirstResponder];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //[self.textField becomeFirstResponder];
    
    //if (self.showKeyboardOnLoad)
        //[self displayCommentDialog];
        //[self composeCommentImplementation];
    
    // set back button
    self.navigationController.navigationBar.topItem.title = @"Comments";
    
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (IBAction)composeAction:(id)sender
{
    [self displayCommentDialog];
    //[self composeCommentImplementation];

}

- (void) displayCommentDialog
{
    [self performSegueWithIdentifier:@"comment_pop_segue" sender:self];
}

- (IBAction)done:(id)sender
{
    [self recordComment];
}

- (IBAction)doneToolbar:(id)sender
{
    [self recordComment];
}

- (void)startSumitComments:(NSString *)comments
{
    self.text = comments;
    [self recordComment];
}
/*
- (IBAction)back:(id)sender
{
  //  [self dismissViewControllerAnimated:YES completion:nil];
    [self.textField resignFirstResponder];
    [self.navigationController popViewControllerAnimated:YES];
}
 */

- (void)setCommentTable
{
    [IOSRequest fetchCommentsforNewsPost:self.newsID
                            onCompletion:^(NSMutableArray *resultArray) {
                           
                                for (id object in resultArray) {
                                    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
                                    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                                    NSDate *dateTime = [[formatter dateFromString:object[@"ts"]] toLocalTime];
                                    
                                        NewsComment *comment = [[NewsComment alloc] initWithCommentDetails:[object[@"comment_id"] integerValue]
                                                                                            andCommentText:object[@"comment_text"]
                                                                                                  byUserID:[object[@"user_id"] integerValue]
                                                                                               andUsername:object[@"username"]
                                                                                             andProfileStr:object[@"profile_pic_string"]
                                                                                               forNewsPost:self.newsID
                                                                                                    onDate:dateTime];
                                                [self.comments addObject:comment];
                                                
                                           //     if ([self.comments count] == [resultArray count]) {
                                                    
                                    
                                               // }
                                
                
                                }
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    [self.activityIndicator stopAnimating];
                                    self.tempView.hidden = YES;
                                    
                                    if ([self.comments count] == 0)
                                        [self displayErrorOverlay];
                                    else
                                        [self.overlay removeFromSuperview];
                                        [self.tableView reloadData];
                                    
                                });
                                
                           
                       }];
}

- (void)recordComment
{
    // Disable the done button before the message is sent.
    /*for (id item in self.toolbar.items) {
        if ([item isKindOfClass:[UIBarButtonItem class]]) {
            UIBarButtonItem *done = (UIBarButtonItem *)item;
            done.enabled = NO;
        }
    }*/
    if ([self.text length] > 0) {
        [IOSRequest recordComment:self.text
                       byUser:[self.preferences getUserID]
                  forNewsPost:self.newsID
                 onCompletion:^(NSMutableArray *resultArray) {
                     dispatch_async(dispatch_get_main_queue(), ^{
                         //[self.overlay removeFromSuperview];
                         //self.overlay.hidden = YES;
                         
                         [self.textField resignFirstResponder];
                         [self.delegate setWithNumCommentsFromVC:self withNumComments:([self.comments count] + 1) forSection:self.currentTableSection];
                      //   [self.preferences setNewsfeedRefreshState:YES];
                    //     [self.navigationController popViewControllerAnimated:YES];
                         [self.comments removeAllObjects];
                         [self setCommentTable];
                    
                     });
                     
                     
                 }];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Comment Error";
        alert.message = @"Please enter a non-blank comment.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
    }
    
}

#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.comments count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Comment Cell";
    CommentCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    Comment *comment = (self.comments)[indexPath.item];
    
    cell.commentLabel.lineBreakMode = NSLineBreakByWordWrapping;
    cell.commentLabel.numberOfLines = 0;
    cell.commentLabel.text = comment.commentText;
    
    cell.commentLabel.frame = CGRectMake(cell.commentLabel.frame.origin.x, cell.commentLabel.frame.origin.y, 240, 0);
    
    [cell.commentLabel sizeToFit];
    
    cell.picButton.tag = comment.userID;
    
    cell.usernameLabel.text = comment.username;
    

    
    /************************************** PROFILE PIC ***************************************/
    NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                            baseString:comment.profileBaseString];
    
    [cell.profilePic setImageWithURL:[NSURL URLWithString:url]
                      placeholderImage:[UIImage imageNamed:@"avatar square"]];
    [UIImage makeRoundedImage:cell.profilePic withRadius:20];
    
    /************************************ END PROFILE PIC *************************************/
    
    NSDate *currentDate = [NSDate date];
    NSTimeInterval timeDifference = [currentDate timeIntervalSinceDate:comment.postDate];
    int minutes = timeDifference / 60;
    int hours = minutes / 60;
    int days = minutes / 1440;
    NSString *timeDiffString = nil;
    
    if (minutes > 1440)
        timeDiffString = [NSString stringWithFormat:@"%dd", days];
    else if (minutes > 60)
        timeDiffString = [NSString stringWithFormat:@"%dh", hours];
    else if (minutes >= 0)
        timeDiffString = [NSString stringWithFormat:@"%dm", minutes];
    else
        timeDiffString = @"0 minutes";
    
    cell.dateLabel.text = timeDiffString;

    
    
    return cell;
}

#define FONT_SIZE 12.0f
#define CELL_CONTENT_WIDTH 200.0f
#define CELL_CONTENT_MARGIN 13.0f

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    NSString *text = nil;
    
    if ((self.comments)[indexPath.row] != nil) {
        Comment *comment = (self.comments)[indexPath.row];
        text = comment.commentText;
        NSLog (@"text is %@", text);
    } else {
        text = @"";
    }
    
    // Get a CGSize for the width and, effectively, unlimited height
    CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH - (CELL_CONTENT_MARGIN * 2), 20000.0f);
    // Get the size of the text given the CGSize we just made as a constraint
    CGSize size = [text sizeWithFont:[UIFont systemFontOfSize:FONT_SIZE] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    // Get the height of our measurement, with a minimum of 71 (standard cell size)
    CGFloat height = MAX(size.height, 65.0f);
    // return the height, with a bit of extra padding in
    return height + (CELL_CONTENT_MARGIN * 2) - 15;
    
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.textField resignFirstResponder];


}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"player_detail_from_comment_segue"]) {
        PlayerDetailTVC *controller = (PlayerDetailTVC *)segue.destinationViewController;
        UIButton * button = (UIButton *)sender;
        controller.playerUserID = button.tag;
        controller.fromComment = YES;
    } else if ([segue.identifier isEqualToString:@"comment_pop_segue"]) {
        CommentDialog *controller = segue.destinationViewController;
        controller.delegate = self;
    }
}

/*
-(void)textViewDidChange:(UITextView *)textView
{
    NSString *text = [textView text];
    NSFont *font = [textView font];
    CGFloat width = [textView frame].size.width;
    
    CGSize size = [text sizeWithFont:font constrainedToSize:CGSizeMake(width, 9999) lineBreakMode:UILineBreakModeWordWrap];
    
}
*/
@end
