//
//  UIColor+VaidenColors.m
//  Vaiden
//
//  Created by James Chung on 10/3/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "UIColor+VaidenColors.h"

@implementation UIColor (VaidenColors)

+ (UIColor *)salmonColor
{
    return [UIColor colorWithRed:255.0/255.0 green:102.0/255.0 blue:102.0/255.0 alpha:1.0];
}

+ (UIColor *)deepSkyBlue
{
    return [UIColor colorWithRed:0.0/255.0 green:191.0/255.0 blue:255.0/255.0 alpha:1.0];
}

+ (UIColor *)peacock
{
    return [UIColor colorWithRed:51.0/255.0 green:161.0/255.0 blue:201.0/255.0 alpha:1.0];
}

+ (UIColor *)peacockDark
{
    return [UIColor colorWithRed:0.0/255.0 green:104.0/255.0 blue:139.0/255.0 alpha:1.0];
}
+ (UIColor *)sienna
{
    return [UIColor colorWithRed:255.0/255.0 green:130.0/255.0 blue:71.0/255.0 alpha:1.0];
}

+ (UIColor *)lightGreenColor
{
    return [UIColor colorWithRed:67.0/255.0 green:205.0/255.0 blue:128.0/255.0 alpha:1.0];
}

+ (UIColor *)burgundyColor
{
    return [UIColor colorWithRed:128.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:1.0];
}

+ (UIColor *)smokeBackground
{
    return [UIColor colorWithRed:220.0/255.0 green:220.0/255.0 blue:220.0/255.0 alpha:1.0];
}

+ (UIColor *)midGray
{
    return [UIColor colorWithRed:209.0/255.0 green:209.0/255.0 blue:209.0/255.0 alpha:1.0];
}

+ (UIColor *)midGray1
{
    return [UIColor colorWithRed:195.0/255.0 green:195.0/255.0 blue:195.0/255.0 alpha:1.0];
}


+ (UIColor *)midGray2
{
    return [UIColor colorWithRed:220.0/255.0 green:220.0/255.0 blue:220.0/255.0 alpha:1.0];
}

+ (UIColor *)lightLightGray
{
    return [UIColor colorWithRed:244.0/255.0 green:244.0/255.0 blue:244.0/255.0 alpha:1.0];
}

+ (UIColor *)midLightGray
{
    return [UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1.0];
}

+ (UIColor *)azureColor
{
    return [UIColor colorWithRed:240.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0];
}

+ (UIColor *)skyBlue
{
    return [UIColor colorWithRed:135.0/255.0 green:206.0/255.0 blue:255.0/255.0 alpha:1.0];
}

+ (UIColor *)iRed
{
    return [UIColor colorWithRed:255.0/255.0 green:106.0/255.0 blue:106.0/255.0 alpha:1.0];
}

+ (UIColor *)emeraldGreen
{
    return [UIColor colorWithRed:0.0/255.0 green:201.0/255.0 blue:87.0/255.0 alpha:1.0];
}

+ (UIColor *)almostBlack
{
    return [UIColor colorWithRed:25.0/255.0 green:25.0/255.0 blue:25.0/255.0 alpha:1.0];
}

+ (UIColor *)springGreen
{
    return [UIColor colorWithRed:0.0/255.0 green:205.0/255.0 blue:102.0/255.0 alpha:1.0];
}

+ (UIColor *)newBlueLight
{
    return [UIColor colorWithRed:62.0/255.0 green:173.0/255.0 blue:219.0/255.0 alpha:1.0];
}

+ (UIColor *)newBlueDark
{
    return [UIColor colorWithRed:41.0/255.0 green:146.0/255.0 blue:215.0/255.0 alpha:1.0];
}

+ (UIColor *)newCharcoal
{
    return [UIColor colorWithRed:51.0/255.0 green:51.0/255.0 blue:51.0/255.0 alpha:1.0];
}


+ (UIColor *)grayBorder1
{
    return [UIColor colorWithRed:215.0/255.0 green:215.0/255.0 blue:215.0/255.0 alpha:1.0];
}

+ (UIColor *)goldColor
{
    return [UIColor colorWithRed:199.0/255.0 green:160.0/255.0 blue:74.0/255.0 alpha:1.0];
}

+ (UIColor *)lightGrayColor2
{
    return [UIColor colorWithRed:239.0/255.0 green:239.0/255.0 blue:244.0/255.0 alpha:1.0];
}

+ (UIColor *)darkGrayColor2
{
    return [UIColor colorWithRed:115.0/255.0 green:115.0/255.0 blue:115.0/255.0 alpha:1.0];
}

+ (UIColor *)darkGrayColor3
{
    return [UIColor colorWithRed:119.0/255.0 green:119.0/255.0 blue:119.0/255.0 alpha:1.0];

}
@end
