//
//  AddPickupMatchIntroVC.m
//  Vaiden
//
//  Created by James Chung on 11/21/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "AddPickupMatchIntroVC.h"
#import "UIColor+VaidenColors.h"
#import "UIView+Manipulate.h"
#import "AddPickupMatchTVC.h"

@interface AddPickupMatchIntroVC ()
@property (weak, nonatomic) IBOutlet UIView *iconBackground;
@property (weak, nonatomic) IBOutlet UIImageView *iconImage;
@property (weak, nonatomic) IBOutlet UILabel *pickupMatchHeadline;
@property (weak, nonatomic) IBOutlet UIScrollView *pageScrollView;

@end

@implementation AddPickupMatchIntroVC


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];

    
    [self.iconBackground makeCircleWithColorAndBackground:[UIColor peacock] andRadius:35];
    self.iconImage.image = [[UIImage imageNamed:@"Player Large"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.iconImage setTintColor:[UIColor whiteColor]];
    self.pickupMatchHeadline.textColor = [UIColor peacock];

	// Do any additional setup after loading the view.
    [self setStartButton];

}
- (void)setStartButton
{
    UIButton *btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 50, 21);
    btn.layer.borderColor = [UIColor whiteColor].CGColor;
    btn.layer.borderWidth = 1.0;
    
    [btn addTarget:self action:@selector(startAction:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundColor:[UIColor clearColor]];
    [btn setTitle:@"START" forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-MediumItalic" size:12.0]];
    
    // Initialize UIBarbuttonitem...
    UIBarButtonItem *proe = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = proe;
}



- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    self.pageScrollView.contentSize = CGSizeMake(320.0, 600.0);
}

- (IBAction)startAction:(id)sender
{
    [self performSegueWithIdentifier:@"New Pickup Match Segue" sender:self];
}


@end
