//
//  QuickInfo2TVC.m
//  Vaiden
//
//  Created by James Chung on 9/30/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "AddSportTVC.h"
#import "UIImage+ImageEffects.h"
#import "IOSRequest.h"
#import "UserPreferences.h"
#import "AddSportCell.h"
#import "UIColor+VaidenColors.h"
#import "StartByAddingFBContacts.h"

@interface AddSportTVC ()

//@property (nonatomic, strong) UIImage *backImage;
@property (nonatomic, strong) NSMutableArray *availSports;
@property (nonatomic, strong) UserPreferences *preferences;
@property (nonatomic) NSInteger checkMarkCount;
@property (nonatomic, strong) NSMutableString *chosenSportIDs;
//@property (weak, nonatomic) IBOutlet UIBarButtonItem *nextButton;
@property (nonatomic, strong) UIBarButtonItem *nextButton;

@end

@implementation AddSportTVC

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (NSMutableArray *)availSports
{
    if (!_availSports) _availSports = [[NSMutableArray alloc] init];
    return _availSports;
}

- (NSMutableString *)chosenSportIDs
{
    if (!_chosenSportIDs) _chosenSportIDs = [[NSMutableString alloc] init];
    return _chosenSportIDs;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];

    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
    
    [IOSRequest fetchAllSports:[self.preferences getUserID] onCompletion:^(NSMutableArray *results) {

                                 [self.availSports removeAllObjects];
                                self.chosenSportIDs = [[NSMutableString alloc] init];
                                 for (id object in results) {
                                     if ([object[@"sport_name"] isEqualToString:@"fitness"])
                                         continue;
                                     
                                     Sport *sp = [[Sport alloc] initWithSportDetails:[object[@"sport_id"] integerValue]
                                                                             andName:object[@"sport_name"]
                                                                             andType:@"individual"];
                                     [self.availSports addObject:sp];

                                 }
                                 dispatch_async(dispatch_get_main_queue(), ^{
                                     [self.tableView reloadData];
                                 });

                                 
                             }];
    
    
 //   [self setNextButton];

}
/*
- (void)setNextButton
{
    UIButton *btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 50, 21);
    btn.layer.borderColor = [UIColor whiteColor].CGColor;
    btn.layer.borderWidth = 1.0;
    
    [btn addTarget:self action:@selector(submit:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundColor:[UIColor clearColor]];
    [btn setTitle:@"NEXT" forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-MediumItalic" size:12.0]];
    
    // Initialize UIBarbuttonitem...
    self.nextButton = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = self.nextButton;
}
*/
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.availSports count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Sport Cell";
    AddSportCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    Sport *sp = [self.availSports objectAtIndex:indexPath.row];
    cell.sportName.text = [sp.sportName capitalizedString];
    
    if (sp.isSelected)
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    else
        cell.accessoryType = UITableViewCellAccessoryNone;
    
    cell.sportIcon.image = [[sp getSportIcon] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [cell.sportIcon setTintColor:[UIColor peacock]];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60.0;
}


- (IBAction)submit:(id)sender
{
    // need to iterate through the cells and add to experiencesAllowed array
    NSInteger counter = 0;
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.labelText = @"Submitting";
    self.nextButton.enabled = NO;
    
    for (int i = 0; i < [self.availSports count]; i++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
        
   //     UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        Sport *sp = [self.availSports objectAtIndex:indexPath.row];
        
        if (sp.isSelected) {
            if (counter == 0)
                [self.chosenSportIDs appendFormat:@"%ld", sp.sportID];
            else
                [self.chosenSportIDs appendFormat:@"-%ld", sp.sportID];
            
            counter ++;
            
        }
    }
    
    [IOSRequest createSportsForUser:[self.preferences getUserID]
                         withSports:self.chosenSportIDs
                       onCompletion:^(NSDictionary *results) {
                           
                           dispatch_async(dispatch_get_main_queue(), ^{
                               [MBProgressHUD hideHUDForView:self.view animated:YES];
                               [self performSegueWithIdentifier:@"Next Segue" sender:self];
                               self.nextButton.enabled = YES;
                           });
                       }];
    double delayInSeconds = 10.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        //code to be executed on the main queue after delay
        self.nextButton.enabled = YES;
        [MBProgressHUD hideHUDForView:self.view animated:YES];// just
    });
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Sport *s = [self.availSports objectAtIndex:indexPath.row];
    UITableViewCell *expCell = [tableView cellForRowAtIndexPath:indexPath];

    if (s.isSelected) {
        s.isSelected = NO;
        expCell.accessoryType = UITableViewCellAccessoryNone;
        self.checkMarkCount --;
    } else {
        s.isSelected = YES;
        expCell.accessoryType = UITableViewCellAccessoryCheckmark;
        self.checkMarkCount ++;
    }
    
    
   /*
    if (expCell.accessoryType != UITableViewCellAccessoryCheckmark) {
       
        
        
    } else {
     
        
    }
    */
    
    
    
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Next Segue"]) {
        StartByAddingFBContacts *controller = segue.destinationViewController;
        controller.isLogin = NO;
    }
}

/*
- (void)setBackgroundImage:(NSUInteger)imageIndex
{
    NSString *effectText = @"";
    UIImage *effectImage = nil;
    
    effectImage = self.backImage;
    
    switch (imageIndex)
    {
        case 0:
            effectImage = self.backImage;
            break;
        case 1:
            effectImage = [self.backImage applyLightEffect];
            effectText = NSLocalizedString(@"Light", @"");
            //           self.effectLabel.textColor = [UIColor whiteColor];
            break;
        case 2:
            effectImage = [self.backImage applyExtraLightEffect];
            effectText = NSLocalizedString(@"Extra light", @"");
            //            self.effectLabel.textColor = [UIColor lightGrayColor];
            break;
        case 3:
            effectImage = [self.backImage applyDarkEffect];
            effectText = NSLocalizedString(@"Dark", @"");
            //           self.effectLabel.textColor = [UIColor darkGrayColor];
            break;
        case 4:
            effectImage = [self.backImage applyTintEffectWithColor:[UIColor blueColor]];
            effectText = NSLocalizedString(@"Color tint", @"");
            //           self.effectLabel.textColor = [UIColor darkGrayColor];
            break;
    }
    
    //    self.infoTableview.backgroundColor = [UIColor colorWithPatternImage:effectImage];
    // self.backImageView.image = effectImage;
    
    UIImageView *tempImageView = [[UIImageView alloc] initWithImage:effectImage];
    [tempImageView setFrame:self.tableView.frame];
    
    self.tableView.backgroundView = tempImageView;
    
}
*/
@end
