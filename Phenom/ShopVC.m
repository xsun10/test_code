//
//  ShopVC.m
//  Vaiden
//
//  Created by James Chung on 2/7/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "ShopVC.h"

@interface ShopVC ()
@property (weak, nonatomic) IBOutlet UIWebView *shopWebView;

@end

@implementation ShopVC


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
    
	
//    NSString *urlAddress = @"https://www.kickstarter.com/projects/1202228764/1471568637?token=7cbc61d0";
    NSURL *url = [[NSURL alloc] initWithString:self.url];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    
    [self.shopWebView loadRequest:requestObj];
    //self.shopWebView.delegate = self;
    self.shopWebView.scalesPageToFit = YES; // Allow zoom in web view
}

- (IBAction)goToCart:(id)sender
{
/*    NSString *urlAddress = @"http://vaiden.myshopify.com/cart";
    NSURL *url = [[NSURL alloc] initWithString:urlAddress];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    
    [self.shopWebView loadRequest:requestObj];*/
}


@end
