//
//  VenueCheckinNews.m
//  Vaiden
//
//  Created by James Chung on 4/25/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "VenueCheckinNews.h"

@implementation VenueCheckinNews

- (VenueCheckinNews *)initWithNewsID:(NSInteger)newsMakerID
                         andUsername:(NSString *)newsMakerUsername
                       andProfileStr:(NSString *)newsMakerProfileString
                            withDate:(NSDate *)newsDate
                         andPostType:(NSInteger)postType
                      andNumComments:(NSInteger)numComments
                       andNumRemixes:(NSInteger)numRemixes
                           andNewsID:(NSInteger)newsID
                            andVenue:(Venue *)venueObj
                     andVenueCheckIn:(VenueCheckIn *)vciObj
{
    self = [super initWithNewsDetails:newsMakerID
                          andUserName:newsMakerUsername
                        andProfileStr:newsMakerProfileString
                             withDate:newsDate
                          andPostType:postType
                       andNumComments:numComments
                        andNumRemixes:numRemixes
                            andNewsID:newsID];
    
    if (self) {
        _venue = venueObj;
        _vci = vciObj;
        
    }
    
    return self;

}
@end
