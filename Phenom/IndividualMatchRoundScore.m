//
//  IndividualMatchScore.m
//  Phenom
//
//  Created by James Chung on 7/23/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "IndividualMatchRoundScore.h"

@implementation IndividualMatchRoundScore


- (IndividualMatchRoundScore *) initWithRoundScore:(NSInteger)round
                                      scorePlayer1:(NSInteger)scorePlayer1
                                 withUserIDPlayer1:(NSInteger)userIDPlayer1
                                      scorePlayer2:(NSInteger)scorePlayer2
                                 withUserIDPlayer2:(NSInteger)userIDPlayer2
{
    self = [super initWithScore:round];
    
    
    if (self) {
        _scorePlayer1 = scorePlayer1;
        _scorePlayer2 = scorePlayer2;
        _userIDPlayer1 = userIDPlayer1;
        _userIDPlayer2 = userIDPlayer2;
    }
    
    return self;
}
@end
