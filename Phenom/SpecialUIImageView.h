//
//  SpecialUIImageView.h
//  Vaiden
//
//  Created by James Chung on 4/24/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SpecialUIImageView : UIImageView

@property (nonatomic) BOOL shouldRemoveFromMySuperview;

- (SpecialUIImageView *)init;


@end
