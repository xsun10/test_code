//
//  TDMembersCell.m
//  Vaiden
//
//  Created by James Chung on 4/7/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "TDMembersCell.h"
#import "TDMembersCVCell.h"
#import "S3Tools.h"
#import "AFNetworking.h"
#import "Player.h"


@implementation TDMembersCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)myInitializer:(NSArray *)membersArray
{
    self.membersCV.delegate = self;
    self.membersCV.dataSource = self;
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"memberStatus = %@", @"CONFIRMED"];
    self.members = [[NSArray alloc] initWithArray:[membersArray filteredArrayUsingPredicate:predicate]];
    
    [self.membersCV reloadData];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.members count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    TDMembersCVCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Player Cell" forIndexPath:indexPath];
    
    Player *p = [self.members objectAtIndex:indexPath.row];
    NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                            baseString:p.profileBaseString];
    
    [cell.profilePic setImageWithURL:[NSURL URLWithString:url]
                       placeholderImage:[UIImage imageNamed:@"avatar square"]];
    
    cell.usernameLabel.text = p.userName;
    
    return cell;
}

@end
