//
//  ImagePostCell.h
//  Phenom
//
//  Created by James Chung on 5/28/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImagePostCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *profilePic;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *userTextPostLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imagePost;
@property (weak, nonatomic) IBOutlet UIView *mainBackgroundViewArea;
@property (weak, nonatomic) IBOutlet UIImageView *newsfeedIcon;

@property (weak, nonatomic) IBOutlet UILabel *commentNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *commentNumberLabel2;
@property (weak, nonatomic) IBOutlet UILabel *likeNumberLabel2;
@property (weak, nonatomic) IBOutlet UILabel *commentTextLabel;
@property (weak, nonatomic) IBOutlet UIButton *commentButton;
@property (weak, nonatomic) IBOutlet UILabel *shareNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *shareTextLabel;
@property (weak, nonatomic) IBOutlet UIButton *shareButton;
@property (weak, nonatomic) IBOutlet UIImageView *shareIcon;
@property (weak, nonatomic) IBOutlet UIView *shareButtonBackgroundView;
@property (weak, nonatomic) IBOutlet UIImageView *commentIcon;
@property (weak, nonatomic) IBOutlet UIView *commentButtonBackgroundView;

@property (weak, nonatomic) IBOutlet UILabel *sharedByLabel;
@property (weak, nonatomic) IBOutlet UIView *shareDot;
@property (weak, nonatomic) IBOutlet UIButton *actionButtonHandle;

@property (weak, nonatomic) IBOutlet UIView *likeButtonBackground;
@property (weak, nonatomic) IBOutlet UIImageView *likeIcon;
@property (weak, nonatomic) IBOutlet UILabel *likeNumberLabel;
@property (weak, nonatomic) IBOutlet UIButton *likeButton;
@property (weak, nonatomic) IBOutlet UIView *smallCircle;
@property (weak, nonatomic) IBOutlet UIView *statBackgroundView;
@property (weak, nonatomic) IBOutlet UIImageView *statIcon;
@property (weak, nonatomic) IBOutlet UILabel *statLabel;
@property (weak, nonatomic) IBOutlet UILabel *statTitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *viewPropsButton;
@property (weak, nonatomic) IBOutlet UIButton *viewCommentsButton;
@property (weak, nonatomic) IBOutlet UIView *buttomBar;
@property (weak, nonatomic) IBOutlet UIView *profileView;
@property (weak, nonatomic) IBOutlet UIButton *viewLockerImageButton;

@property (weak, nonatomic) IBOutlet UILabel *value1;
@property (weak, nonatomic) IBOutlet UILabel *desc1;
@property (weak, nonatomic) IBOutlet UILabel *desc3;
@property (weak, nonatomic) IBOutlet UILabel *value2;
@property (weak, nonatomic) IBOutlet UILabel *desc2;
@property (weak, nonatomic) IBOutlet UILabel *value3;
@property (weak, nonatomic) IBOutlet UIView *sep1;
@property (weak, nonatomic) IBOutlet UIView *sep2;
@property (weak, nonatomic) IBOutlet UIView *line2;
@property (weak, nonatomic) IBOutlet UIView *line1;
@property (weak, nonatomic) IBOutlet UILabel *drills;

// CONSTRAINTS
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleconst1;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *descconst1;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleconst2;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *descconst2;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sepconst1;

@end
