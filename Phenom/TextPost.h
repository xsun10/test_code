//
//  TextPost.h
//  Phenom
//
//  Created by James Chung on 6/3/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "News.h"

@interface TextPost : News

- (TextPost *)initWithNewsDetails:(NSInteger)newsMakerUserID
                      andUserName:(NSString *)newsMakerUserName
                    andProfileStr:(NSString *)profileBaseString
                         withDate:(NSDate *)newsDate
                      andPostType:(NSInteger)postType
                        andNewsID:(NSInteger)newsID
                      andTextPost:(NSString *)textPost
                   andNumComments:(NSUInteger)numComments
                    andNumRemixes:(NSUInteger)numRemixes;
@end
