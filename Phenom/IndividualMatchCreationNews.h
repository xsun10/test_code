//
//  IndividualMatchCreationNews.h
//  Phenom
//
//  Created by James Chung on 4/24/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "News.h"
#import "IndividualMatch.h"

@interface IndividualMatchCreationNews : News


// Note: user1 is always equal to match creator for 1 x 1 matches

@property (nonatomic, strong) IndividualMatch *individualMatch;
//@property (nonatomic, strong) UIImage *profileImageUser2;

- (IndividualMatchCreationNews *) initWithIndividualMatchCreationNewsDetails:(IndividualMatch *)individualMatch
                                                        matchCreatorUserName:(NSString *)matchCreatorUserName
                                                             andProfileStr:(NSString *)profileBaseString
                                                                newsPostDate:(NSDate *)newsPostDate
                                                                 andPostType:(NSInteger)postType
                                                                   andNewsID:(NSInteger)newsID
                                                              andNumComments:(NSUInteger)numComments
                                                               andNumRemixes:(NSUInteger)numRemixes;

@end
