//
//  ProfileViewController.m
//  Phenom
//
//  Created by James Chung on 3/20/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "ProfileSettingsVC.h"
#import "UserPreferences.h"
#import "IOSRequest.h"
#import "UITextField+FormValidations.h"
#import <MobileCoreServices/MobileCoreServices.h>

#import "Constants.h"
#import "Response.h"
#import "AmazonKeyChainWrapper.h"
#import "AmazonClientManager.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "S3Tools.h"
#import "UIImage+ProportionalFill.h"
#import "AFNetworking.h"
#import "VenuesTVC.h"
#import "IsAccountVerifiedVC.h"
#import "ShowFollowingTVC.h"
#import "ShowFollowersTVC.h"
#import "StartByAddingFBContacts.h"


@interface ProfileSettingsVC () <UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (nonatomic, strong) UserPreferences *preferences;
@property (nonatomic, strong) NSDictionary *user;
@property (nonatomic, strong) NSDictionary *edits;

@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UITextField *userNameField;
@property (weak, nonatomic) IBOutlet UITextField *emailField;

@property (nonatomic, strong) NSDictionary *userData;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *saveButton;

@property (weak, nonatomic) UIActionSheet *profilePicActionSheet;
@property (strong, nonatomic)UIPopoverController *imagePickerPopover;
@property (weak, nonatomic) IBOutlet UIButton *changeImageButton;
@property (strong, nonatomic) UIImage *profilePicOriginal;
@property (weak, nonatomic) IBOutlet UITableViewCell *statesButtonCell;

@property (weak, nonatomic) IBOutlet UILabel *aboutLabel;
@property (weak, nonatomic) IBOutlet UITextField *locationField;
@property (weak, nonatomic) IBOutlet UILabel *stateIDHiddenLabel;

@property (nonatomic, strong) NSData *imageData;
@property (nonatomic, strong) NSData *thumbnailImageData;
@property (nonatomic, strong) NSData *thumbnailRetinaImageData;
@property (weak, nonatomic) IBOutlet UIImageView *profilePicOutlet;
@property (nonatomic, strong) NSString *fileExtension;

//@property (nonatomic, strong) NSString *phoneNumber;

@end

@implementation ProfileSettingsVC 

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

// lazy instantiation
- (UserPreferences *) preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];

    return _preferences;
}


- (void)viewDidLoad
{
    [super viewDidLoad];

    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];

    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [self populateFields];

}



- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if ([self.preferences getStateName]) {
 //       self.stateLabel.text = [self.preferences getStateName];
 //       self.stateLabel.textColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1.0];
        self.stateIDHiddenLabel.text = [NSString stringWithFormat:@"%ld", [self.preferences getStateID]];
    }
    
    
    
}

// for some reason, the state is not clearing as expected.  It's clearing when app closes but not
// when view disappears...

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.preferences clearStateInfo];
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)populateFields
{
    __weak ProfileSettingsVC *weakSelf = self;
//    __weak ProfileViewController *weakSelf2 = self;
    
    NSLog(@"ProfileViewController User ID is %ld", [self.preferences getUserID]);
    
    // on profileviewcontroller we disregard whether session user is following himself.
    // this flag, is only used when session user views the profile of another user.
    
    [IOSRequest fetchUserInfo:[self.preferences getUserID]
                bySessionUser:[self.preferences getUserID] 
                 onCompletion:^(NSDictionary *user){
                     dispatch_async(dispatch_get_main_queue(), ^{
                         [weakSelf setUser:user];
         });
     }];

    /*
    [IOSRequest getPhoto:1
                   forID:[self.preferences getUserID]
                 onCompletion:^(UIImage *profilePic){
                     dispatch_async(dispatch_get_main_queue(), ^{
                           [weakSelf2.changeImageButton setImage:profilePic forState:UIControlStateNormal];
                     });
                 }];
     */
    
}

- (IBAction)verifyIdentity:(id)sender
{
    /*
        [IOSRequest checkUserVerified:[self.preferences getUserID]
                         onCompletion:^(NSDictionary *results) {
                             
                             if ([results[@"phone_number"] length] > 0) {
                                 dispatch_async(dispatch_get_main_queue(), ^ {
                                    self.phoneNumber = results[@"phone_number"];
                                     [self performSegueWithIdentifier:@"Is Verified Segue" sender:self];
                                 });
                             } else {
                                 // account must not be verified
                                 
                                 dispatch_async(dispatch_get_main_queue(), ^{
                                     [self performSegueWithIdentifier:@"Verify Segue" sender:self];
                                     
                                 });
                             }
                             
                         }];
     
     */
    if ([self.preferences isVerifiedAccount]) {
        [self performSegueWithIdentifier:@"Is Verified Segue" sender:self];
    } else {
        [self performSegueWithIdentifier:@"Verify Segue" sender:self];
    }



}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Venues Segue"]) {
        VenuesTVC *controller = (VenuesTVC *)segue.destinationViewController;
        controller.showChooseButton = NO;
    } else if ([segue.identifier isEqualToString:@"logout"]) {
            [self.preferences logOutSession];
            
    } else if ([segue.identifier isEqualToString:@"Is Verified Segue"]) {
    //    IsAccountVerifiedVC *controller = (IsAccountVerifiedVC *)segue.destinationViewController;
  //      controller.phoneNumber = self.phoneNumber;
    } else if ([segue.identifier isEqualToString:@"Show Following Segue"]) {
        ShowFollowingTVC *controller = segue.destinationViewController;
        controller.baseUserID = [self.preferences getUserID];
    } else if ([segue.identifier isEqualToString:@"Show Followers Segue"]) {
        ShowFollowersTVC * controller = segue.destinationViewController;
        controller.baseUserID = [self.preferences getUserID];
    } else if ([segue.identifier isEqualToString:@"Invite Friends Segue"]) {
        StartByAddingFBContacts *controller = segue.destinationViewController;
        controller.isLogin = YES;
    }
}
/*
- (IBAction)saveEdit:(UIBarButtonItem *)sender
{
    // not the best validation system...but will improve later. Need better error messages and validations
    if (![self.userNameField checkAlphanumeric]) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Username Error";
        alert.message = @"Username is not valid.  Please enter a username between 3 and 15 letters with only alphanumeric characters.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
        
    } else if (![self.emailField checkEmail]){
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Email Error";
        alert.message = @"Email is not valid.  Please enter another one.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
    } else if ([self.cityField.text length] > 0 && ![self.cityField checkCityName]) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"City Name Error";
        alert.message = @"City is not valid.  Please re-enter your city.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];

        
    } else {
        [self beginEditAccount];
        [self saveEditExecute];
        [self endEditAccount];
    }

    
    }

- (void)saveEditExecute
{
    //    self.saveButton.enabled = NO;
    __weak ProfileViewController *weakSelf = self;
    
    NSLog(@"ProfileViewController (saveEdit method) User ID is %d", [self.preferences getUserID]);
    
    NSLog (@"state ID hidden label value: %d", [self.stateIDHiddenLabel.text integerValue]);
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.labelText = @"Loading";
    
    [IOSRequest saveUserInfoWithUserID:[self.preferences getUserID]
                               andName:self.nameField.text
                           andUserName:self.userNameField.text
                              andEmail:self.emailField.text
                               andLocation.self.
                          onCompletion:^(NSDictionary *edits){
                              dispatch_async(dispatch_get_main_queue(), ^{
                                  [MBProgressHUD hideHUDForView:self.view animated:YES];
                                  NSInteger userID = [self.preferences getUserID];
                                  NSLog(@"User ID is %d", userID);
                                  
                                  if ([edits[@"username_error"] isEqualToString:@"TRUE"]) {
                                      UIAlertView *alert = [[UIAlertView alloc] init];
                                      alert.title = @"Account Creation Error";
                                      alert.message = @"That username already exists. Please choose another";
                                      [alert addButtonWithTitle:@"OK"];
                                      [alert show];
                                  } else if ([edits[@"email_error"] isEqualToString:@"TRUE"]) {
                                      UIAlertView *alert = [[UIAlertView alloc] init];
                                      alert.title = @"Account Creation Error";
                                      alert.message = @"That email already exists. Please enter another";
                                      [alert addButtonWithTitle:@"OK"];
                                      [alert show];
                                  } else {
                                      // can set values
                                      [weakSelf.user setValue:self.nameField.text forKey:@"fullname"];
                                      [weakSelf.user setValue:self.userNameField.text forKey:@"username"];
                                      [weakSelf.user setValue:self.emailField.text forKey:@"email"];
                                      [weakSelf.user setValue:self.cityField.text forKey:@"city"];
                
                           //           [weakSelf.user setValue:self.stateLabel.text forKey:@"state"];
                                  }
                                  
                              });
                          }];
    
    
    if (self.profilePicOriginal) {
    //    [IOSRequest uploadImage:self.profilePicOriginal withUserID:[self.preferences getUserID]];
        [self saveProfilePic];
    }

}

- (void)saveProfilePic
{
    if (![AmazonClientManager hasCredentials]) {
        [[Constants credentialsAlert] show];
        NSLog(@"no credentials");
    }
    else {
        
        NSLog (@"has credentials");
        
        operationQueue = [NSOperationQueue new];
        operationQueue.maxConcurrentOperationCount = 3;
        
        NSString *fileNameBase = [S3Tools getFileNameBaseStringWithType:@"profile" forUser:[self.preferences getUserID]];
        S3Tools *imageUploader1 = [[S3Tools alloc] initWithData:self.imageData
                                                               forUser:[self.preferences getUserID]
                                                             dataType:@"profile"
                                               andBaseFileNameString:fileNameBase
                                                     andFileExt:self.fileExtension];
        [operationQueue addOperation:imageUploader1];
        
        
        S3Tools *imageUploader2 = [[S3Tools alloc] initWithData:self.thumbnailImageData
                                                             forUser:[self.preferences getUserID]
                                                           dataType:@"thumbnail"
                                               andBaseFileNameString:fileNameBase
                                                     andFileExt:self.fileExtension];
        [operationQueue addOperation:imageUploader2];
        
        S3Tools *imageUploader3 = [[S3Tools alloc] initWithData:self.thumbnailRetinaImageData
                                                             forUser:[self.preferences getUserID]
                                                           dataType:@"thumbnail_retina"
                                               andBaseFileNameString:fileNameBase
                                                     andFileExt:self.fileExtension];
        [operationQueue addOperation:imageUploader3];
        
    }
}
- (void)beginEditAccount
{
    NSLog(@"Attempting to Save User Edits");
    self.saveButton.enabled = NO;
    [self.userNameField resignFirstResponder];
    [self.emailField resignFirstResponder];
    [self.nameField resignFirstResponder];
    [self.cityField resignFirstResponder];
    
}

- (void)endEditAccount
{
    self.saveButton.enabled = YES;
}

- (IBAction)cancelButton:(UIBarButtonItem *)sender
{
    self.nameField.text = [self.user[@"fullname"] description];
    self.userNameField.text = [self.user[@"username"] description];
    self.emailField.text = [self.user[@"email"] description];
    self.cityField.text = [self.user[@"city"] description];
    
    if ([[self.user[@"state"] description] isEqualToString:@"<null>"])
        self.stateLabel.text = @"State";
    else  {
        self.stateLabel.text = [self.user[@"state"] description];
        self.stateLabel.textColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1.0];
        self.stateIDHiddenLabel.text = [self.user[@"state_id"] description];
    }
    [self.userNameField resignFirstResponder];
    [self.emailField resignFirstResponder];
    [self.nameField resignFirstResponder];
    [self.cityField resignFirstResponder];
    
    
    // also need to reset image to the one from server
   // [self.changeImageButton setImage:imageView.image forState:UIControlStateNormal];
}


-(void)setUser:(NSDictionary *)user
{
    if (_user != user) {
        _user = user;
    }
    [self updateUIWithUser:user];
}

-(void)updateUIWithUser:(NSDictionary *)user
{
    if (user) {
               self.nameField.text = user[@"fullname"];
               self.userNameField.text = user[@"username"];
               self.emailField.text = user[@"email"];
               self.cityField.text = user[@"city"];
        
                if([[user[@"state"] description] isEqualToString:@"<null>"])
                    self.stateLabel.text = @"State";
                else {
                    self.stateLabel.text = [user[@"state"] description];
                    self.stateLabel.textColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1.0];
                    self.stateIDHiddenLabel.text = [self.user[@"state_id"] description];
                }
        
        NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                baseString:user[@"profile_pic_string"]];
                
        [self.profilePicOutlet setImageWithURL:[NSURL URLWithString:url]
                        placeholderImage:[UIImage imageNamed:@"chroma_key_72_pencil.png"]];
        
        
    }
}

- (IBAction)clearNameField:(id)sender
{
    self.nameField.text = @"";
}

- (IBAction)clearUserNameField:(id)sender
{
    self.userNameField.text = @"";
}

- (IBAction)clearEmailField:(id)sender
{
    self.emailField.text = @"";
}
- (IBAction)clearCityField:(id)sender
{
    self.cityField.text = @"";
}

#define TITLE_OF_ACTIONSHEET @"Change your profile pic"
#define CANCEL_BUTTON_ACTIONSHEET @"Cancel "
#define FROM_CAMERA @"Take a profile pic"
#define FROM_LIBRARY @"Choose From Photo Library"

- (IBAction)changeProfilePic:(id)sender
{
    if (!self.profilePicActionSheet) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                 initWithTitle:TITLE_OF_ACTIONSHEET delegate:self
                                  cancelButtonTitle:CANCEL_BUTTON_ACTIONSHEET destructiveButtonTitle:nil
                                      otherButtonTitles:FROM_CAMERA, FROM_LIBRARY, nil];
        [actionSheet showFromTabBar:self.tabBarController.tabBar];
        
        self.profilePicActionSheet = actionSheet;
    }
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == actionSheet.destructiveButtonIndex) {
        [self.view.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    } else {
        NSString *choice = [actionSheet buttonTitleAtIndex:buttonIndex];
        
        NSLog(@"Choice is: %@", choice);
        if ([choice isEqualToString:FROM_CAMERA]) {
            [self changePicFromCamera];
        } else if ([choice isEqualToString:FROM_LIBRARY]){
            [self changePicFromPhotoLibrary];
        }
    }
}

- (void)changePicFromCamera
{
    [self presentImagePicker:UIImagePickerControllerSourceTypeCamera];
}


- (void) changePicFromPhotoLibrary
{
    [self presentImagePicker:UIImagePickerControllerSourceTypeSavedPhotosAlbum];
}

- (void) presentImagePicker:(UIImagePickerControllerSourceType)sourceType
{
    if ([UIImagePickerController isSourceTypeAvailable:sourceType]) {
        NSArray *availableMediaTypes = [UIImagePickerController availableMediaTypesForSourceType:sourceType];
        if ([availableMediaTypes containsObject:(NSString *)kUTTypeImage]) {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.sourceType = sourceType;
            picker.mediaTypes = @[(NSString *)kUTTypeImage];
            picker.allowsEditing = YES;
            picker.delegate = self;
            
            // present the picker
            
        //    if ((sourceType != UIImagePickerControllerSourceTypeCamera) && (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)) {
         //       self.imagePickerPopover = [[UIPopoverController alloc] initWithContentViewController:picker];
        //        [self.imagePickerPopover presentPopoverFromBarButtonItem:<#(UIBarButtonItem *)#> permittedArrowDirections:<#(UIPopoverArrowDirection)#> animated:<#(BOOL)#>]
        //    }
            [self presentViewController:picker animated:YES completion:nil];
        }
    }
}

- (void) imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

#define MAX_IMAGE_WIDTH 70 // might want to change this to height....

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = info[UIImagePickerControllerEditedImage];
    
    if (!image) image = info[UIImagePickerControllerOriginalImage];
    if (image) {
        self.profilePicOriginal = image;
        UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
        CGRect frame = imageView.frame;
        if (frame.size.width > MAX_IMAGE_WIDTH) {
            frame.size.height = (frame.size.height / frame.size.width) * MAX_IMAGE_WIDTH;
            frame.size.width = MAX_IMAGE_WIDTH;
        }
        imageView.frame = frame;
 //       [self.setRandomLocationForView:imageView];
 //       [self.changeImageButton.imageView addSubview:imageView];
        [self.changeImageButton setImage:imageView.image forState:UIControlStateNormal];
        self.imageData = UIImageJPEGRepresentation(imageView.image, 1.0);
        self.thumbnailImageData = UIImageJPEGRepresentation([image imageCroppedToFitSize:CGSizeMake(70, 70)], 1.0);
        self.thumbnailRetinaImageData = UIImageJPEGRepresentation([image imageCroppedToFitSize:CGSizeMake(140, 140)], 1.0);
        self.fileExtension = @"jpg";
         
 //       self.profilePicOriginal = imageView.image;
    }
    if (self.imagePickerPopover) {
        [self.imagePickerPopover dismissPopoverAnimated:YES];
        self.imagePickerPopover = nil;
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"logout"]) {
        [self.preferences logOutSession];
        
    }
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
 //   UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
 //   cell.userInteractionEnabled = NO;
}

@end
