//
//  ShowAlertsCell.h
//  Vaiden
//
//  Created by James Chung on 5/6/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShowAlertsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *profilePic;
@property (weak, nonatomic) IBOutlet UILabel *updateLabel;
@property (weak, nonatomic) IBOutlet UIButton *followButton;
@end
