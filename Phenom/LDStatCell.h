//
//  LDStatCell.h
//  Vaiden
//
//  Created by James Chung on 5/29/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LDStatCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *sportIcon;
@property (weak, nonatomic) IBOutlet UILabel *featuredStatLabel;
@property (weak, nonatomic) IBOutlet UILabel *otherStatsLabel;
@property (weak, nonatomic) IBOutlet UIButton *likeButtonHandle;
@property (weak, nonatomic) IBOutlet UIButton *commentButtonHandle;
@property (weak, nonatomic) IBOutlet UILabel *statNumLabel;
@property (weak, nonatomic) IBOutlet UIImageView *likeIcon;
@property (weak, nonatomic) IBOutlet UIImageView *commentIcon;

@property (weak, nonatomic) IBOutlet UILabel *statValue1;
@property (weak, nonatomic) IBOutlet UILabel *statDescription1;
@property (weak, nonatomic) IBOutlet UILabel *statValue2;
@property (weak, nonatomic) IBOutlet UILabel *statDescription2;
@property (weak, nonatomic) IBOutlet UILabel *statValue3;
@property (weak, nonatomic) IBOutlet UILabel *statDescription3;
@end
