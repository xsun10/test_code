//
//  States+Create.m
//  Phenom
//
//  Created by James Chung on 4/1/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "States+Create.h"

@implementation States (Create)

+ (States *)statesWithName:(NSString *)stateName
                withAbbrev:(NSString *)stateAbbrev
                     andID:(NSInteger)stateID
    inManagedObjectContext:(NSManagedObjectContext *)context
{
    States *states = nil;
    
    // query database to see if already in there
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"States"];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES]];
    request.predicate = [NSPredicate predicateWithFormat:@"name = %@ AND abbreviation = %@ AND s_id = %@", stateName, stateAbbrev, @(stateID)]; // compound predicate
    
    NSError *error = nil;
    NSArray *matches = [context executeFetchRequest:request error:&error];
    
    if (!matches || ([matches count] > 1)) {
        // handle error
        NSLog(@"Error in statesWithNamewithAbbrevinManagedContext method");
        
    } else if (![matches count]) {
        states = [NSEntityDescription insertNewObjectForEntityForName:@"States" inManagedObjectContext:context];
        
        states.name = stateName;
        states.abbreviation = stateAbbrev;
        states.s_id   = @(stateID);
        NSLog(@"state ID %@", @(stateID));
        
    } else {
        // only one match in databse
        states = [matches lastObject];
    }
    
    return states;
}

@end
