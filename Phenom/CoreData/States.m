//
//  States.m
//  Phenom
//
//  Created by James Chung on 4/1/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "States.h"


@implementation States

@dynamic abbreviation;
@dynamic name;
@dynamic s_id;

@end
