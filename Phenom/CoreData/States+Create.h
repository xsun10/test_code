//
//  States+Create.h
//  Phenom
//
//  Created by James Chung on 4/1/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "States.h"

@interface States (Create)

+ (States *)statesWithName:(NSString *)stateName
                withAbbrev:(NSString *)stateAbbrev
                     andID:(NSInteger)stateID
    inManagedObjectContext:(NSManagedObjectContext *)context;

@end
