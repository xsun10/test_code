//
//  ChooseStatesCDTVC.h
//  Phenom
//
//  Created by James Chung on 4/1/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "CoreDataTableViewController.h"

@class ChooseStatesCDTVC;

@protocol ChooseStatesCDTVC_Delegate <NSObject>
-(void)setMyViewController:(ChooseStatesCDTVC *)controller
             withStateName:(NSString *)stateName
               withStateID:(NSInteger)stateID;

@end

@interface ChooseStatesCDTVC : CoreDataTableViewController

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, weak) id <ChooseStatesCDTVC_Delegate> delegate;

@end
