//
//  ChooseStatesCDTVC.m
//  Phenom
//
//  Created by James Chung on 4/1/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "ChooseStatesCDTVC.h"
#import "States+Create.h"
#import "UserPreferences.h"

@interface ChooseStatesCDTVC ()
@property (nonatomic, strong) UserPreferences *preferences;
@end

@implementation ChooseStatesCDTVC

- (void) setManagedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
    _managedObjectContext = managedObjectContext;
    
    if (managedObjectContext) {
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"States"];
        request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)]];
        request.predicate = nil; // fetching all states
        self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    } else {
        self.fetchedResultsController = nil;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    
    
    cell = [tableView dequeueReusableCellWithIdentifier:@"States"];
        
    States *states = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    cell.textLabel.text = states.name;
    cell.detailTextLabel.hidden = YES;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@", states.s_id];
    

    return cell;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (!self.managedObjectContext) [self useDemoDocument];
}


- (void)useDemoDocument
{
    NSURL *url = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
    url = [url URLByAppendingPathComponent:@"State Document"];
    UIManagedDocument *document = [[UIManagedDocument alloc] initWithFileURL:url];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:[url path]]) {
        // create it
        [document saveToURL:url
           forSaveOperation:UIDocumentSaveForCreating completionHandler:^(BOOL success) {
               if (success) {
                   self.managedObjectContext = document.managedObjectContext;
                   [self refresh];
               }
           }];
    } else if (document.documentState == UIDocumentStateClosed) {
        // open it
        [document openWithCompletionHandler:^(BOOL success) {
            if (success) {
                self.managedObjectContext = document.managedObjectContext;
            }
        }];
    } else {
        // try to use it
        self.managedObjectContext = document.managedObjectContext;
    }
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
        return [[self.fetchedResultsController sections][section] numberOfObjects];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    
    UITableViewCell *stateCell = [tableView cellForRowAtIndexPath:indexPath];
    
    stateCell.accessoryType = UITableViewCellAccessoryCheckmark;
    [self.preferences setStateID:[stateCell.detailTextLabel.text integerValue] withName:stateCell.textLabel.text];
    
    // delegate way:
    [self.delegate setMyViewController:self withStateName:stateCell.textLabel.text withStateID:[stateCell.detailTextLabel.text integerValue]];
    
    // have to store state into server...will need mB loader and also thread IOS call.
    
    [self.navigationController popViewControllerAnimated:YES]; // go back to previous view controller
        
}

// lazy instantiation
- (UserPreferences *) preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    
    return _preferences;
}

- (void)refresh
{
    [self.refreshControl beginRefreshing];
    
    [States statesWithName:@"Alabama" withAbbrev:@"AL" andID:1 inManagedObjectContext:self.managedObjectContext];
    [States statesWithName:@"Alaska" withAbbrev:@"AK" andID:2 inManagedObjectContext:self.managedObjectContext];
    [States statesWithName:@"Arizona" withAbbrev:@"AZ" andID:3 inManagedObjectContext:self.managedObjectContext];
    [States statesWithName:@"Arkansas" withAbbrev:@"AR" andID:4 inManagedObjectContext:self.managedObjectContext];
    [States statesWithName:@"California" withAbbrev:@"CA" andID:5 inManagedObjectContext:self.managedObjectContext];
    [States statesWithName:@"Colorado" withAbbrev:@"CO" andID:6 inManagedObjectContext:self.managedObjectContext];
    [States statesWithName:@"Connecticut" withAbbrev:@"CT" andID:7 inManagedObjectContext:self.managedObjectContext];
    [States statesWithName:@"Delaware" withAbbrev:@"DE" andID:8 inManagedObjectContext:self.managedObjectContext];
    [States statesWithName:@"District of Columbia" withAbbrev:@"DC" andID:9 inManagedObjectContext:self.managedObjectContext];
    [States statesWithName:@"Florida" withAbbrev:@"FL" andID:10 inManagedObjectContext:self.managedObjectContext];
    [States statesWithName:@"Georgia" withAbbrev:@"GA" andID:11 inManagedObjectContext:self.managedObjectContext];
    [States statesWithName:@"Hawaii" withAbbrev:@"HI" andID:12 inManagedObjectContext:self.managedObjectContext];
    [States statesWithName:@"Idaho" withAbbrev:@"ID" andID:13 inManagedObjectContext:self.managedObjectContext];
    [States statesWithName:@"Illinois" withAbbrev:@"IL" andID:14 inManagedObjectContext:self.managedObjectContext];
    [States statesWithName:@"Indiana" withAbbrev:@"IN" andID:15 inManagedObjectContext:self.managedObjectContext];
    [States statesWithName:@"Iowa" withAbbrev:@"IA" andID:16 inManagedObjectContext:self.managedObjectContext];
    [States statesWithName:@"Kansas" withAbbrev:@"KS" andID:17 inManagedObjectContext:self.managedObjectContext];
    [States statesWithName:@"Kentucky" withAbbrev:@"KY" andID:18 inManagedObjectContext:self.managedObjectContext];
    [States statesWithName:@"Louisiana" withAbbrev:@"LA" andID:19 inManagedObjectContext:self.managedObjectContext];
    [States statesWithName:@"Maine" withAbbrev:@"ME" andID:20 inManagedObjectContext:self.managedObjectContext];
    [States statesWithName:@"Maryland" withAbbrev:@"MD" andID:21 inManagedObjectContext:self.managedObjectContext];
    [States statesWithName:@"Massachusetts" withAbbrev:@"MA" andID:22 inManagedObjectContext:self.managedObjectContext];
    [States statesWithName:@"Michigan" withAbbrev:@"MI" andID:23 inManagedObjectContext:self.managedObjectContext];
    [States statesWithName:@"Minnesota" withAbbrev:@"MN" andID:24 inManagedObjectContext:self.managedObjectContext];
    [States statesWithName:@"Mississippi" withAbbrev:@"MS" andID:25 inManagedObjectContext:self.managedObjectContext];
    [States statesWithName:@"Missouri" withAbbrev:@"MO" andID:26 inManagedObjectContext:self.managedObjectContext];
    [States statesWithName:@"Montana" withAbbrev:@"MT" andID:27 inManagedObjectContext:self.managedObjectContext];
    [States statesWithName:@"Nebraska" withAbbrev:@"NE" andID:28 inManagedObjectContext:self.managedObjectContext];
    [States statesWithName:@"Nevada" withAbbrev:@"NV" andID:29 inManagedObjectContext:self.managedObjectContext];
    [States statesWithName:@"New Hampshire" withAbbrev:@"NH" andID:30 inManagedObjectContext:self.managedObjectContext];
    [States statesWithName:@"New Jersey" withAbbrev:@"NJ" andID:31 inManagedObjectContext:self.managedObjectContext];
    [States statesWithName:@"New Mexico" withAbbrev:@"NM" andID:32 inManagedObjectContext:self.managedObjectContext];
    [States statesWithName:@"New York" withAbbrev:@"NY" andID:33 inManagedObjectContext:self.managedObjectContext];
    [States statesWithName:@"North Carolina" withAbbrev:@"NC" andID:34 inManagedObjectContext:self.managedObjectContext];
    [States statesWithName:@"North Dakota" withAbbrev:@"ND" andID:35 inManagedObjectContext:self.managedObjectContext];
    [States statesWithName:@"Ohio" withAbbrev:@"OH" andID:36 inManagedObjectContext:self.managedObjectContext];
    [States statesWithName:@"Oklahoma" withAbbrev:@"OK" andID:37 inManagedObjectContext:self.managedObjectContext];
    [States statesWithName:@"Oregon" withAbbrev:@"OR" andID:38 inManagedObjectContext:self.managedObjectContext];
    [States statesWithName:@"Pennsylvania" withAbbrev:@"PA" andID:39 inManagedObjectContext:self.managedObjectContext];
    [States statesWithName:@"Rhode Island" withAbbrev:@"RI" andID:40 inManagedObjectContext:self.managedObjectContext];
    [States statesWithName:@"South Carolina" withAbbrev:@"SC" andID:41 inManagedObjectContext:self.managedObjectContext];
    [States statesWithName:@"South Dakota" withAbbrev:@"SD" andID:42 inManagedObjectContext:self.managedObjectContext];
    [States statesWithName:@"Tennessee" withAbbrev:@"TN" andID:43 inManagedObjectContext:self.managedObjectContext];
    [States statesWithName:@"Texas" withAbbrev:@"TX" andID:44 inManagedObjectContext:self.managedObjectContext];
    [States statesWithName:@"Utah" withAbbrev:@"UT" andID:45 inManagedObjectContext:self.managedObjectContext];
    [States statesWithName:@"Vermont" withAbbrev:@"VT" andID:46 inManagedObjectContext:self.managedObjectContext];
    [States statesWithName:@"Virginia" withAbbrev:@"VA" andID:47 inManagedObjectContext:self.managedObjectContext];
    [States statesWithName:@"Washington" withAbbrev:@"WA" andID:48 inManagedObjectContext:self.managedObjectContext];
    [States statesWithName:@"West Virginia" withAbbrev:@"WV" andID:49 inManagedObjectContext:self.managedObjectContext];
    [States statesWithName:@"Wisconsin" withAbbrev:@"WI" andID:50 inManagedObjectContext:self.managedObjectContext];
    [States statesWithName:@"Wyoming" withAbbrev:@"WY" andID:51 inManagedObjectContext:self.managedObjectContext];

    
    
    [self.refreshControl endRefreshing];
}

@end
