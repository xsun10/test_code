//
//  PostUserContentVC.h
//  Phenom
//
//  Created by James Chung on 5/20/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChooseHighlightReelVC.h"
#import "ChooseTrickShotVC.h"
#import "MBProgressHUD.h"
#import "CustomBaseVC.h"

@interface PostUserContentVC : CustomBaseVC <UITextViewDelegate, ChooseHighlightReelVC_Delegate, ChooseTrickShotVC_Delegate, MBProgressHUDDelegate>
{
    NSOperationQueue         *operationQueue;
    MBProgressHUD *HUD;
}
@end
