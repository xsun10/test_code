//
//  ChooseMatchCompetitorTVC.h
//  Phenom
//
//  Created by James Chung on 4/5/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PickupMatch.h"
#import "MBProgressHUD.h"
#import <CoreLocation/CoreLocation.h>
#import "CustomBaseTVC.h"


@interface ChooseMatchCompetitorTVC : CustomBaseTVC <MBProgressHUDDelegate, CLLocationManagerDelegate>
{
    MBProgressHUD *HUD;
    CLLocationManager *locationManager;
}


@property (nonatomic, strong) PickupMatch *pickupMatch;
@property (nonatomic) NSInteger userID;

@end
