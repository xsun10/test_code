//
//  VoteShopVC.h
//  Vaiden
//
//  Created by Turbo on 8/4/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface VoteShopVC : UIViewController<MBProgressHUDDelegate> {
    MBProgressHUD *HUD;
}
@property (assign) BOOL fromProfile;
@end
