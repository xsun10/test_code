//
//  PhysicalAttributesTVC.m
//  Phenom
//
//  Created by James Chung on 6/21/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "PhysicalAttributesTVC.h"
#import "PhysicalAttributesCell.h"
#import "IOSRequest.h"
#import "UserPreferences.h"
#import "NSDate+Utilities.h"

@interface PhysicalAttributesTVC ()

@property (nonatomic) UIDatePicker *datePicker;
@property (nonatomic) NSInteger datePickerHeight;
@property (nonatomic) NSInteger selectedRowType;
@property (nonatomic) BOOL datePickerIsShowing;

@property (weak, nonatomic) IBOutlet UISegmentedControl *genderControl;
@property (weak, nonatomic) IBOutlet UITextField *feetTextField;
@property (weak, nonatomic) IBOutlet UITextField *inchesTextField;
@property (weak, nonatomic) IBOutlet UITextField *weightTextField;
@property (nonatomic, strong) UserPreferences *preferences;
@property (nonatomic, strong) UIView *tempView;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;

@property (nonatomic, strong) UIPickerView *pickerView;
@property (nonatomic) NSInteger pickerViewHeight;
@property (nonatomic) BOOL pickerViewIsShowing;
@property (nonatomic, strong) UIBarButtonItem *doneButton;

@property (nonatomic, strong) UIButton *closeDatePickerButton;
@property (nonatomic, strong) UIView *closeDatePickerView;

@property (nonatomic, strong) UIButton *closeHeightPickerButton;
@property (nonatomic, strong) UIView *closeHeightPickerView;

@end

@implementation PhysicalAttributesTVC

#define GENDER_ROW 0
#define AGE_ROW 1
#define HEIGHT_ROW 2
#define WEIGHT_ROW 3

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
    
    [self showTempSpinner];
    self.datePicker.datePickerMode = UIDatePickerModeDate;
    self.datePicker.maximumDate = [NSDate date];
    self.datePicker.minimumDate = [NSDate dateWithTimeIntervalSinceNow:-3155760000];
    self.datePickerHeight = 44;
    self.datePickerIsShowing = NO;
    
    // single tap gesture recognizer
//    UITapGestureRecognizer *tapGestureRecognize = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissDate)];
//    tapGestureRecognize.numberOfTapsRequired = 1;
//    [self.datePicker addGestureRecognizer:tapGestureRecognize];
    
    
    self.pickerView.delegate = self;
    self.pickerView.dataSource = self;
    self.pickerView.showsSelectionIndicator = YES;
    self.pickerViewHeight = 44;
    self.pickerViewIsShowing = NO;
//    UITapGestureRecognizer *tapGestureRecognize2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissPickerView)];
//    tapGestureRecognize2.numberOfTapsRequired = 1;
//    [self.pickerView addGestureRecognizer:tapGestureRecognize2];
    
    //[self setDoneButton];
    [self populateTable];

}

/*- (void)setDoneButton
{
    UIButton *btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 50, 21);
    btn.layer.borderColor = [UIColor whiteColor].CGColor;
    btn.layer.borderWidth = 1.0;
    
    [btn addTarget:self action:@selector(doneAction:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundColor:[UIColor clearColor]];
    [btn setTitle:@"DONE" forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-MediumItalic" size:12.0]];
    
    // Initialize UIBarbuttonitem...
    self.doneButton = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = self.doneButton;
}*/


- (void)showTempSpinner
{
    self.tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    self.tempView.backgroundColor = [UIColor lightGrayColor];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] init];
    [self.activityIndicator setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width/2.0, [UIScreen mainScreen].bounds.size.height/2.0 - 50)];
    [self.activityIndicator startAnimating];
    [self.tempView addSubview:self.activityIndicator];
    
    [self.view addSubview:self.tempView];
    
}

- (void)dismissDate
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:AGE_ROW inSection:0];
    UITableViewCell* cell = (UITableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    
   /*
    if (self.datePickerIsShowing) {
        [self performCloseCell:indexPath];
        self.datePickerIsShowing = NO;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
 */
    
    [UIView animateWithDuration:0.5 animations:^(void) {
        self.closeDatePickerView.alpha = 0.0;
        
        
        if (self.datePickerIsShowing) {
            [self performCloseCell:indexPath];
            self.datePickerIsShowing = NO;
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

        }
    }];
}

- (void)dismissPickerView
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:HEIGHT_ROW inSection:0];
    UITableViewCell *cell = (UITableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
 /*
    if (self.pickerViewIsShowing) {
        [self performCloseCell:indexPath];
        self.pickerViewIsShowing = NO;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        UITableViewCell *heightCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
        heightCell.detailTextLabel.text = [NSString stringWithFormat:@"%d feet %d inches", [self.pickerView selectedRowInComponent:0] + 3, [self.pickerView selectedRowInComponent:1]];

    }
  */
    [UIView animateWithDuration:0.5 animations:^(void) {
        self.closeHeightPickerView.alpha = 0.0;

        if (self.pickerViewIsShowing) {
            [self performCloseCell:indexPath];
            self.pickerViewIsShowing = NO;
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
         //   UITableViewCell *heightCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
            
            
        }
    }];
}

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (UIDatePicker *)datePicker
{
    if (!_datePicker) _datePicker = [[UIDatePicker alloc] init];
    return _datePicker;
}

- (UIPickerView *)pickerView
{
    if (!_pickerView) _pickerView = [[UIPickerView alloc] init];
    return _pickerView;
}

- (void)populateTable
{
    [IOSRequest getPhysicalStatsForUser:[self.preferences getUserID] onCompletion:^(NSDictionary *results) {
        dispatch_async(dispatch_get_main_queue(), ^{
        
            [self.activityIndicator stopAnimating];
            self.tempView.hidden = YES;
   
            
        UITableViewCell *genderCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        genderCell.detailTextLabel.text = [results[@"gender"] capitalizedString];
            
            UITableViewCell *heightCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
            heightCell.detailTextLabel.text = [NSString stringWithFormat:@"%@ feet %@ inches", results[@"ht_feet"], results[@"ht_inches"]];
           
            if ([results[@"ht_feet"] integerValue] >= 3) {
                [self.pickerView selectRow:([results[@"ht_feet"] integerValue] - 3) inComponent:0 animated:NO];
                [self.pickerView selectRow:([results[@"ht_inches"] integerValue]) inComponent:1 animated:NO];
            }
            
      
        self.weightTextField.text = results[@"weight"];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
   //     dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            
            NSDate *date = [[dateFormatter dateFromString:results[@"dob"]] toLocalTime];
        
        
        
        if ([results[@"dob"] length] > 0 && ![results[@"dob"] isEqualToString:@"0000-00-00 00:00:00"]) {
            PhysicalAttributesCell* cell = (PhysicalAttributesCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:AGE_ROW inSection:0]];
            
            NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
            dateFormatter2.dateFormat = @"MMM d yyyy";
            cell.detailTextLabel.text = [dateFormatter2 stringFromDate:date];
            [self.datePicker setDate:date];
            
        }
        
        
        });
    }];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    PhysicalAttributesCell* cell = (PhysicalAttributesCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    
 
    if (indexPath.row == AGE_ROW && !self.datePickerIsShowing) {
                
        self.selectedRowType = AGE_ROW;
        cell.textLabel.text = @"";
        cell.detailTextLabel.text = @"";
        
 //       self.datePicker.showsSelectionIndicator = YES;
        
        self.datePicker.alpha = 0;
        [self.tableView beginUpdates];
        self.datePickerHeight = 44 + 190;
        [self.tableView endUpdates];
        
        
        [cell addSubview:self.datePicker];
        self.datePickerIsShowing = YES;
        
        [UIView animateWithDuration:0.2 animations:^{
            self.datePicker.alpha = 1;
            self.datePickerIsShowing = YES;
            
            self.closeDatePickerView = [[UIView alloc] initWithFrame:CGRectMake(0, cell.frame.origin.y, 320, 40)];
            self.closeDatePickerView.backgroundColor = [UIColor darkGrayColor];
            self.closeDatePickerView.alpha = 0.5;
            
            self.closeDatePickerButton = [[UIButton alloc] initWithFrame:CGRectMake(260,  7, 50, 25)];
            [self.closeDatePickerButton setTitle:@"CLOSE" forState:UIControlStateNormal];
            [self.closeDatePickerButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-MediumItalic" size:11]];
            self.closeDatePickerButton.layer.borderColor = [UIColor lightGrayColor].CGColor;
            self.closeDatePickerButton.layer.borderWidth = 1.0;
            
            [self.closeDatePickerButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
            [self.closeDatePickerButton addTarget:self action:@selector(dismissDate) forControlEvents:UIControlEventTouchUpInside];
            
            [self.closeDatePickerView addSubview:self.closeDatePickerButton];
            [self.tableView addSubview:self.closeDatePickerView];
            
        } completion:nil];

        
    } else if (indexPath.row == HEIGHT_ROW && !self.pickerViewIsShowing) {
        self.selectedRowType = HEIGHT_ROW;
        cell.textLabel.text = @"";
        cell.detailTextLabel.text = @"";
        
        self.pickerView.alpha = 0;
        [self.tableView beginUpdates];
        self.pickerViewHeight = 44 + 190;
        [self.tableView endUpdates];
        
        [cell addSubview:self.pickerView];
//        [UIView animateWithDuration:0.2 animations:^{self.pickerView.alpha = 1;} completion:nil];
        
        
        self.pickerViewIsShowing = YES;
        
        [UIView animateWithDuration:0.2 animations:^{
            self.pickerView.alpha = 1;
            self.pickerViewIsShowing = YES;
            
            self.closeHeightPickerView = [[UIView alloc] initWithFrame:CGRectMake(0, cell.frame.origin.y, 320, 40)];
            self.closeHeightPickerView.backgroundColor = [UIColor darkGrayColor];
            self.closeHeightPickerView.alpha = 0.5;
            
            self.closeHeightPickerButton = [[UIButton alloc] initWithFrame:CGRectMake(260,  7, 50, 25)];
            [self.closeHeightPickerButton setTitle:@"CLOSE" forState:UIControlStateNormal];
            [self.closeHeightPickerButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-MediumItalic" size:11]];
            self.closeHeightPickerButton.layer.borderColor = [UIColor lightGrayColor].CGColor;
            self.closeHeightPickerButton.layer.borderWidth = 1.0;
            
            [self.closeHeightPickerButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
            [self.closeHeightPickerButton addTarget:self action:@selector(dismissPickerView) forControlEvents:UIControlEventTouchUpInside];
            
            [self.closeHeightPickerView addSubview:self.closeHeightPickerButton];
            [self.tableView addSubview:self.closeHeightPickerView];
            
        } completion:nil];
    } else {
     
        self.closeDatePickerView.hidden = YES;
        self.closeDatePickerButton.hidden = YES;
        
        self.closeHeightPickerView.hidden = YES;
        self.closeHeightPickerButton.hidden = YES;
        
        [self.closeHeightPickerView removeFromSuperview];
        [self.closeDatePickerView removeFromSuperview];
        self.closeDatePickerView = nil;
        self.closeDatePickerButton = nil;
        self.closeHeightPickerView = nil;
        self.closeHeightPickerButton = nil;
    }
    
    [self.feetTextField resignFirstResponder];
    [self.inchesTextField resignFirstResponder];
    [self.weightTextField resignFirstResponder];
}

- (NSIndexPath *)tableView:(UITableView *)tableView willDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.closeDatePickerView.hidden = YES;
    self.closeDatePickerButton.hidden = YES;
    self.closeDatePickerView = nil;
    self.closeDatePickerButton = nil;

    self.closeHeightPickerView.hidden = YES;
    self.closeHeightPickerButton.hidden = YES;
    self.closeHeightPickerView = nil;
    self.closeHeightPickerButton = nil;
    
    [self performCloseCell:indexPath];
    self.datePickerIsShowing = NO;
    self.pickerViewIsShowing = NO;
    
    return indexPath;
}

- (void)performCloseCell:(NSIndexPath *)indexPath
{
 

    if (indexPath.row == AGE_ROW && self.datePickerIsShowing) {
        [UIView animateWithDuration:0.2 animations:^{self.datePicker.alpha = 0;} completion:^(BOOL finished){[self.datePicker removeFromSuperview];}];
        PhysicalAttributesCell *cell = (PhysicalAttributesCell *)[self.tableView cellForRowAtIndexPath:indexPath];
        NSDate *myDate = self.datePicker.date;
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MMM d yyyy"];
        cell.detailTextLabel.text = [dateFormat stringFromDate:myDate];
        cell.textLabel.text = @"Date";
    
    
        [self.tableView beginUpdates];
    
        if (indexPath.row == AGE_ROW)
            self.datePickerHeight = 44;
    
        [self.tableView endUpdates];
    } else if (indexPath.row == HEIGHT_ROW && self.pickerViewIsShowing) {
        [UIView animateWithDuration:0.2 animations:^{self.pickerView.alpha = 0;} completion:^(BOOL finished){[self.pickerView removeFromSuperview];}];
        PhysicalAttributesCell *cell = (PhysicalAttributesCell *)[self.tableView cellForRowAtIndexPath:indexPath];
        
        cell.textLabel.text = @"Height";
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%ld feet %ld inches", [self.pickerView selectedRowInComponent:0] + 3, [self.pickerView selectedRowInComponent:1]];

        [self.tableView beginUpdates];
        
     //   cell.detailTextLabel.text = @"testing";
        if (indexPath.row == HEIGHT_ROW) {
            self.pickerViewHeight = 44;
        }
        [self.tableView endUpdates];
        
    }

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row == AGE_ROW) {
        return self.datePickerHeight;
    } else if (indexPath.row == HEIGHT_ROW) {
        return self.pickerViewHeight;
    }
    
    return 44;
}

- (IBAction)touchFeet:(id)sender
{
    self.feetTextField.placeholder = @"";
    [self.inchesTextField resignFirstResponder];
    [self.weightTextField resignFirstResponder];
    [self performCloseCell:[NSIndexPath indexPathForRow:AGE_ROW inSection:0]];
}


- (IBAction)touchInches:(id)sender
{
    self.inchesTextField.placeholder = @"";
    [self.feetTextField resignFirstResponder];
    [self.weightTextField resignFirstResponder];
    [self performCloseCell:[NSIndexPath indexPathForRow:AGE_ROW inSection:0]];
}

- (IBAction)touchWeight:(id)sender
{
    self.weightTextField.placeholder = @"";
    [self.feetTextField resignFirstResponder];
    [self.inchesTextField resignFirstResponder];
    [self performCloseCell:[NSIndexPath indexPathForRow:AGE_ROW inSection:0]];
}



- (IBAction)genderSegmentChanged:(id)sender
{
    [self.feetTextField resignFirstResponder];
    [self.inchesTextField resignFirstResponder];
    [self performCloseCell:[NSIndexPath indexPathForRow:AGE_ROW inSection:0]];
}
- (IBAction)doneAction:(id)sender
{
    self.doneButton.enabled = NO;
    UITableViewCell *genderCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.labelText = @"Saving";
    [IOSRequest savePhysicalStats:[self.preferences getUserID]
                       withGender:[genderCell.detailTextLabel.text lowercaseString]
                          withDOB:[self.datePicker.date toGlobalTime]
                   withHeightFeet:[self.pickerView selectedRowInComponent:0] + 3
                 withHeightInches:[self.pickerView selectedRowInComponent:1]
                       withWeight:0
                     onCompletion:^(NSDictionary *results) {
                         dispatch_async(dispatch_get_main_queue(), ^{
                             
                             [MBProgressHUD hideHUDForView:self.view animated:YES];
                             self.doneButton.enabled = YES;
                             if (![results[@"outcome"] isEqualToString:@"failure"]) {
                                 [self.preferences setProfilePageRefreshState:YES];
                                 [self.navigationController popViewControllerAnimated:YES];
                             } else {
                                 UIAlertView *alert = [[UIAlertView alloc] init];
                                 alert.title = @"Submission Error";
                                 alert.message = @"There was a problem submitting your request.  Please retry.";
                                 [alert addButtonWithTitle:@"OK"];
                                 [alert show];
                             }
                             
                         });
                         
                     }];
    double delayInSeconds = 10.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        //code to be executed on the main queue after delay
        self.doneButton.enabled = YES;
        [MBProgressHUD hideHUDForView:self.view animated:YES];// just
    });
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Gender Selection Segue"]) {
        ChooseProfileGenderTVC *controller = segue.destinationViewController;
        controller.delegate = self;
    }
}

- (void)setProfileGenderWithController:(ChooseProfileGenderTVC *)controller withGenderString:(NSString *)genderString
{
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    cell.detailTextLabel.text = genderString;
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component
{

}

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    NSUInteger numRows = 0;
    
    if (component == 0) {
        numRows = 5;
    } else if (component == 1) {
        numRows = 12;
    }
    
    return numRows;
}

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 2;
}

// tell the picker the title for a given component
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NSString *title;
    
    if (component == 0) {
        title = [NSString stringWithFormat:@"%ld feet", row + 3];
    } else if (component == 1) {
        title = [NSString stringWithFormat:@"%ld inches", row];
    }
    
    return title;
}

// tell the picker the width of each row for a given component
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    int sectionWidth = 120;
    
    return sectionWidth;
}






/*
- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component {
    // Handle the selection
    
    NSString *title;
    
    if (self.selectedRowType != HEIGHT_ROW) {
         title = [[pickerView delegate] pickerView:pickerView titleForRow:row forComponent:component];
    } else {
        title = [NSString stringWithFormat:@"%@ %@", [[pickerView delegate] pickerView:pickerView titleForRow:row forComponent:0],
                 [[pickerView delegate] pickerView:pickerView titleForRow:row forComponent:1]];
    }
   
    PhysicalAttributesCell *cell = (PhysicalAttributesCell *)[self.tableView cellForRowAtIndexPath:[self.tableView indexPathForSelectedRow]];
    
    cell.detailTextLabel.text = title;
    [self performCloseCell:[[self.tableView indexPathForSelectedRow ] row]];
    
//    NSString * title = [[pickerView delegate] pickerView:pickerView titleForRow:row inComponent:component];
//    [text appendFormat:@"Selected item \"%@\" in component %lu\n", title, i];
}

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    NSUInteger numRows = 0;
    
    switch (self.selectedRowType) {
        case GENDER_ROW:
            numRows = 2;
            break;
        case AGE_ROW:
            numRows = 100;
            break;
        case HEIGHT_ROW:
            if (component == 0) {
                numRows = 5;
            } else {
                numRows = 12;
            }
            break;
        case WEIGHT_ROW:
            numRows = 450;
            break;
    }
    return numRows;
}

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    
    NSInteger comp = 1;
    
    switch (self.selectedRowType) {
        case GENDER_ROW:
            comp = 1;
            break;
        case AGE_ROW:
            comp = 1;
            break;
        case HEIGHT_ROW:
            comp = 2;
            break;
        case WEIGHT_ROW:
            comp = 1;
            break;
        default:
            break;
    }
    return comp;
}

// tell the picker the title for a given component
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NSString *title;

    switch (self.selectedRowType) {
        case GENDER_ROW:
            if (row == 0) {
                title = @"Male";
            } else {
                title = @"Female";
            }
            
            break;
        case AGE_ROW:
            title = [NSString stringWithFormat:@"%d years old", row + 1];
            break;
        case HEIGHT_ROW:
            if (component == 0) {
                title = [NSString stringWithFormat:@"%d feet", row + 3];
            } else {
                title = [NSString stringWithFormat:@"%d inches", row + 1];
            }
            break;
        case WEIGHT_ROW:
            title = [NSString stringWithFormat:@"%d pounds", row + 50];
            break;
            
        default:
            break;
    }
    
    return title;
}

// tell the picker the width of each row for a given component
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    int sectionWidth = 110;
    
    return sectionWidth;
}
 */

@end
