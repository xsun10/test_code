//
//  FindNewPlayersTVC.h
//  Phenom
//
//  Created by James Chung on 7/11/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomBaseTVC.h"

@interface FindNewPlayersTVC : CustomBaseTVC <UISearchBarDelegate>

@end
