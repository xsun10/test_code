//
//  VenuesTVC.m
//  Phenom
//
//  Created by James Chung on 4/3/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "VenuesTVC.h"
#import "Venue.h"
#import "IOSRequest.h"
#import "MBProgressHUD.h"
#import "UserPreferences.h"
#import "VenueCell.h"
#import "MapOverlayVC.h"
//#import "VenueDetailTVC.h"
#import "S3Tools.h"
#import "AFNetworking.h"
#import "UIColor+VaidenColors.h"
#import "UIButton+RoundBorder.h"
#import "TempVenueCell.h"
#import "UIView+Manipulate.h"
#import "CheckInTVC.h"

@interface VenuesTVC ()
@property (nonatomic, strong) NSMutableArray *venues;
@property (nonatomic, strong) NSMutableArray *nearbyVenues;

@property (strong, nonatomic) UserPreferences *preferences;

@property (nonatomic) NSInteger proximityRange;

@property (weak, nonatomic) IBOutlet UISegmentedControl *venuesSegmentedControl;
@property (nonatomic) BOOL shouldLoadNearbyVenues;
@property (nonatomic) BOOL shouldLoadFavoriteVenues;

@property (nonatomic) NSInteger selectedSection;
@property (nonatomic) BOOL noResultsToDisplay;
@property (nonatomic, strong) UIView *tempView;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
@property (nonatomic, strong) UIView *overlay;
@property (nonatomic, strong) UIRefreshControl *refreshControl;


@end

@implementation VenuesTVC

#define VENUE_NEARBY_RANGE 50

- (NSMutableArray *)venues
{
    if (!_venues) _venues = [[NSMutableArray alloc] init];
    
    return _venues;
}

- (NSMutableArray *)nearbyVenues
{
    if (!_nearbyVenues) _nearbyVenues = [[NSMutableArray alloc] init];
    return _nearbyVenues;
}

// lazy instantiation
- (UserPreferences *) preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    
    return _preferences;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
    
    self.tableView.sectionFooterHeight = 0.0;
    self.tableView.sectionHeaderHeight = 3.0;
    self.tableView.contentInset = UIEdgeInsetsMake(-36, 0, -20, 0);
    
    [self.preferences setVenuesListPageRefreshState:NO];
 //   [self refreshPage];
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(refreshPage) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshControl];
    
    //[self refreshPage];
}


- (void)refreshPage
{
    [self.refreshControl endRefreshing];

    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Location Services Are Off";
        alert.message = @"Please turn on Location Services in your Phone Settings to use this feature";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
        self.noResultsToDisplay = YES;
    } else {
        self.noResultsToDisplay = NO;
        self.tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
        self.tempView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        
        self.activityIndicator = [[UIActivityIndicatorView alloc] init];
        [self.activityIndicator setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width/2.0, [UIScreen mainScreen].bounds.size.height/2.0 - 50)];
        [self.activityIndicator startAnimating];
        [self.tempView addSubview:self.activityIndicator];
        
        [self.view addSubview:self.tempView];
        self.shouldLoadNearbyVenues = YES;
        //    [self.venues removeAllObjects];
     //   [self fetchVenueInfo];
        [self reloadData];
        self.shouldLoadFavoriteVenues = YES;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [self.overlay removeFromSuperview];
    self.overlay = nil;
    
    
    //if ([self.preferences getVenuesListPageRefreshState]) {
        //[self.preferences setVenuesListPageRefreshState:NO];
        [self refreshPage];
    //}
    
}

#define FAVORITE_VENUES 0
#define NEARBY_VENUES 1

- (IBAction)changeSegmentAction:(id)sender
{
    [self reloadData];
}

- (void)reloadData
{
    if (self.venuesSegmentedControl.selectedSegmentIndex == FAVORITE_VENUES) {
        
    //    self.overlay.hidden = YES;
        [self.overlay removeFromSuperview];
        self.overlay = nil;
        
    //    if (!self.shouldLoadFavoriteVenues) {
     //       [self.tableView reloadData];
    //    } else {
            [self fetchVenueInfo];
    //        self.shouldLoadFavoriteVenues = NO;
    //    }
        
        
    } else if (self.venuesSegmentedControl.selectedSegmentIndex == NEARBY_VENUES) {
        
     //   self.overlay.hidden = YES;
        [self.overlay removeFromSuperview];
        self.overlay = nil;
        
    //    if (!self.shouldLoadNearbyVenues) {
    //        [self.tableView reloadData];
    //    } else {
            [self initLocation];
            self.shouldLoadNearbyVenues = NO;
        self.noResultsToDisplay = NO; // Enable the detail segue for the venue
    //    }
        
    }
}

// this is now going to list the favorite venues for the session user
- (void)fetchVenueInfo
{
    
    [IOSRequest fetchUserVenuesFor:[self.preferences getUserID]
                      onCompletion:^(NSArray *venues){
                          [self.venues removeAllObjects];
                          if ([venues count] > 0) {
                          for (id object in venues) {
                              
                              NSMutableArray *sportArray = [[NSMutableArray alloc] init];
                              
                              for (id sportObj in object[@"venue_sports"]) {
                                  Sport *sport = [[Sport alloc] initWithSportDetails:[sportObj[@"sport_id"] integerValue]
                                                                             andName:sportObj[@"sport_name"]
                                                                             andType:nil];
                                  [sportArray addObject:sport];
                              }
                              
                              AvgRating *avgRating = [[AvgRating alloc] initWithAvgRating:[object[@"venue_avg_rating"] floatValue]
                                                                               andRatings:nil];
                              
                              Venue *localVenue = [[Venue alloc] initVenueWithName:object[@"name"]
                                                                        andVenueID:[object[@"venueID"] integerValue]
                                                                        andAddress:object[@"street"]
                                                                           andCity:object[@"city"]
                                                                      andStateName:object[@"state"]
                                                                        andStateID:[object[@"state_id"] integerValue]
                                                                       andLatitude:[object[@"latitude"] floatValue]
                                                                      andLongitude:[object[@"longitude"] floatValue] 
                                                                    andDescription:object[@"description"]
                                                                        andCreator:[object[@"user_id"] integerValue]
                                                                         andSports:sportArray
                                                                      andAvgRating:avgRating
                                                                                    ];
                              [localVenue setIsTemporary:[object[@"is_temporary"] boolValue]];
                              
                              id viObj = object[@"venue_thumb_pic"];
                              VenueImage *venueImageThumb = [[VenueImage alloc] initWithVenueImageInfo:viObj[@"filestring"]
                                                                                              andImage:nil isDefault:YES];
                              [localVenue setVenueImageThumb:venueImageThumb];
                              localVenue.isSessionUserFavorite = YES;
                              
                              [localVenue setLongitude:[object[@"longitude"] floatValue]];
                              [localVenue setLatitude:[object[@"latitude"] floatValue]];
                              
                              [self.venues addObject:localVenue];
                            }
                          }
                          
                          
                          dispatch_async(dispatch_get_main_queue(), ^{
                              if ([self.venues count] > 0) {
                                  [self.activityIndicator stopAnimating];
                                  self.tempView.hidden = YES;
                                  [self.tableView reloadData];
                              } else if (self.shouldLoadNearbyVenues && [self.venues count] == 0){
                                  self.venuesSegmentedControl.selectedSegmentIndex = NEARBY_VENUES;
                                  [self reloadData];
                              } else {
                                  if ([self.venues count] == 0) {
                                      [self displayErrorOverlay:@"favorite"];
                                      self.noResultsToDisplay = YES;
                                  } else
                                      self.noResultsToDisplay = NO;
                                  
                                  [self.tableView reloadData];
                              }
                              
                             
                          });

    }];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
//    if (self.noResultsToDisplay)
 //       return 1;
    
    if (self.venuesSegmentedControl.selectedSegmentIndex == FAVORITE_VENUES) {
        return [self.venues count];
    } else if (self.venuesSegmentedControl.selectedSegmentIndex == NEARBY_VENUES) {
    
        return [self.nearbyVenues count];
    }

    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Venue *venue = nil;
    
    
    if (self.venuesSegmentedControl.selectedSegmentIndex == FAVORITE_VENUES) {
        venue = (self.venues)[indexPath.section];
    } else if (self.venuesSegmentedControl.selectedSegmentIndex == NEARBY_VENUES) {
        venue = (self.nearbyVenues)[indexPath.section];
    }

 /*   if (venue.isTemporary) {
        static NSString *CellIdentiier = @"Temp Venue Cell";
        TempVenueCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentiier forIndexPath:indexPath];
        
        cell.venueNameLabel.text = venue.venueName;
        cell.venueDescriptionLabel.text = venue.description;
        cell.chooseButton.hidden = !self.showChooseButton;
        cell.tempVenueFlagLabel.textColor = [UIColor salmonColor];
        
        if (self.showChooseButton) {
            cell.chooseButton.layer.borderWidth = 1.0;
            cell.chooseButton.layer.borderColor = [UIColor whiteColor].CGColor;
        }
        return cell;
        
    }  else {
      */
        static NSString *CellIdentifier = @"Venue Cell";
        VenueCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // clear subviews
    if ([cell.contentView subviews]){
        for (UIView *subview in [cell.contentView subviews]) {
            if (subview.tag == 1)
                    [subview removeFromSuperview];
            
        }
    }
    
        cell.venueNameLabel.text = venue.venueName;
        cell.sportsArray = venue.sportsArray;
        
        [cell.sportsCV reloadData];
        
        if (venue.isSessionUserFavorite) {
            [cell.favoriteButton setTitle:@"UnFavorite" forState:UIControlStateNormal];
        } else {
            [cell.favoriteButton setTitle:@"Favorite" forState:UIControlStateNormal];
        }
        
        if ([venue.streetAddress length] > 0) {
            cell.streetLabel.text = venue.streetAddress;
            
            if ([venue.city length] > 0 && [venue.stateName length] > 0) {
                cell.cityStateLabel.text = [NSString stringWithFormat:@"%@, %@", venue.city, venue.stateName ];
            } else if ([venue.city length] > 0 && [venue.stateName length] == 0) {
                cell.cityStateLabel.text = venue.city;
            } else if ([venue.city length] == 0 && [venue.stateName length] > 0) {
                cell.cityStateLabel.text = venue.stateName;
            }
        } else {
            if ([venue.city length] > 0 && [venue.stateName length] > 0) {
                cell.cityStateLabel.text = [NSString stringWithFormat:@"%@, %@", venue.city, venue.stateName ];
                cell.streetLabel.text = @"Street Not Specified";
                cell.streetLabel.font = [UIFont fontWithName:@"HelveticaNeue-LightItalic" size:13];
            } else if ([venue.city length] > 0 && [venue.stateName length] == 0) {
                cell.streetLabel.text = venue.city;
            } else if ([venue.city length] == 0 && [venue.stateName length] > 0) {
                cell.streetLabel.text = venue.stateName;
            } else {
                // show longitude / latitude
                cell.streetLabel.text = [NSString stringWithFormat:@"Longitude: %f", venue.longitude];
                cell.cityStateLabel.text = [NSString stringWithFormat:@"Latitude: %f", venue.latitude];
            }
            
        }
    
    if (venue.isTemporary) {
        UILabel *temporaryLabel = [[UILabel alloc] initWithFrame:CGRectMake(cell.starRating.frame.origin.x, cell.starRating.frame.origin.y, 100, 15)];
        temporaryLabel.text = @"Unofficial Venue";
        temporaryLabel.textColor = [UIColor salmonColor];
        temporaryLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:13];
        temporaryLabel.tag = 1;
        
        [cell.contentView addSubview:temporaryLabel];
        cell.starRating.hidden = YES;
    } else {
        cell.starRating.hidden = NO;
        cell.streetLabel.font = [UIFont systemFontOfSize:14];
    }
        
        if (self.venuesSegmentedControl.selectedSegmentIndex == FAVORITE_VENUES) {
            cell.distanceLabel.hidden = YES;
        } else if (self.venuesSegmentedControl.selectedSegmentIndex == NEARBY_VENUES) {
            cell.distanceLabel.hidden = NO;
            cell.distanceLabel.text = [NSString stringWithFormat:@"Within %.1f miles", venue.distanceFromSessionUser];
        }
        
        cell.starRating.image = [venue.avgRating getRatingImage];
    
    if (self.showChooseButton) {
   //     cell.chooseButton.layer.borderWidth = 1.0;
   //     cell.chooseButton.layer.borderColor = [UIColor whiteColor].CGColor;
   //     cell.chooseButton.backgroundColor = [UIColor clearColor];
        cell.chooseButton.backgroundColor = [UIColor goldColor];
        [cell.chooseButton makeRoundedBorderWithRadius:3];
        [cell.chooseButton setTitle:@"Choose" forState:UIControlStateNormal];
    } else {
        [cell.chooseButton makeRoundedBorderWithRadius:3];
        cell.chooseButton.backgroundColor = [UIColor goldColor];
        [cell.chooseButton setTitle:@"Check-In" forState:UIControlStateNormal];
    }
    
        
      //  [cell.chooseButton makeRoundBorderWithColor:[UIColor whiteColor]];
        
        cell.venuePic.contentMode = UIViewContentModeScaleAspectFill;
        cell.venuePic.clipsToBounds = YES;
        
        if ([venue.venueImageThumb.venuePicString length] > 0) {
            NSString *url = [S3Tools getFileNameStringWithType:@"venue_pic"
                                                    baseString:venue.venueImageThumb.venuePicString];
            
            [cell.venuePic setImageWithURL:[NSURL URLWithString:url]
                          placeholderImage:nil];
            //     cell.blackFade.hidden = NO;
        } else {
            //   cell.blackFade.hidden = YES;
            //    cell.backgroundColor = [UIColor blackColor];
            cell.venuePic.image = [UIImage imageNamed:@"Venue Default"];
            //    cell.venuePic.hidden = YES;
            
        }
        
        
        return cell;
 //   }
    
   
    
    return nil;
}

- (void)handleTap:(UITapGestureRecognizer *)tapGestureRecognizer
{
    [self performSegueWithIdentifier:@"map overlay segue" sender:self];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Venue *myVenue = nil;
    
    if (self.venuesSegmentedControl.selectedSegmentIndex == FAVORITE_VENUES) {
        myVenue = [self.venues objectAtIndex:indexPath.section];
    } else if (self.venuesSegmentedControl.selectedSegmentIndex == NEARBY_VENUES) {
        myVenue = [self.nearbyVenues objectAtIndex:indexPath.section];
    }
    NSLog(@"%hhd", self.noResultsToDisplay);
    if (!self.noResultsToDisplay ) { // Cannot see detail page for temporary venues for now
        self.selectedSection = indexPath.section;
        [self performSegueWithIdentifier:@"Venue Detail Segue" sender:self];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [self.overlay removeFromSuperview];
    self.overlay = nil;
    
    if ([segue.identifier isEqualToString:@"Add Venue Segue"]) {
        AddVenueTVC *controller = (AddVenueTVC *)segue.destinationViewController;
        controller.delegate = self;
        controller.isCreatingMatch = YES;
    } else if ([segue.identifier isEqualToString:@"map overlay segue"]) {
        NSIndexPath *selectedRowIndexPath = [self.tableView indexPathForSelectedRow];
        MapOverlayVC *controller = (MapOverlayVC *)segue.destinationViewController;
        controller.venue = (self.venues)[selectedRowIndexPath.section];
    } else if ([segue.identifier isEqualToString:@"Venue Detail Segue"]) {
         
        VenueDetailTVC *controller = (VenueDetailTVC *)segue.destinationViewController;
        Venue *venue = nil;
        
        if (self.venuesSegmentedControl.selectedSegmentIndex == FAVORITE_VENUES) {
            venue = [self.venues objectAtIndex:self.selectedSection];
        } else if (self.venuesSegmentedControl.selectedSegmentIndex == NEARBY_VENUES) {
            venue = [self.nearbyVenues objectAtIndex:self.selectedSection];
        }
        controller.venueID = venue.venueID;
        controller.showChooseButton = self.showChooseButton;
        controller.delegate = self;
        
    } else if ([segue.identifier isEqualToString:@"Add Venue Option Segue"]) {
        AddVenueIntroVC *controller = segue.destinationViewController;
        controller.delegate = self;
        controller.isCreatingMatch = self.showChooseButton; // if you show choose button you are creating match
    } else if ([segue.identifier isEqualToString:@"Checkin Segue"]) {
        CheckInTVC *controller = segue.destinationViewController;
        
        Venue *venue = nil;
        
        if (self.venuesSegmentedControl.selectedSegmentIndex == FAVORITE_VENUES) {
            venue = [self.venues objectAtIndex:self.selectedSection];
        } else if (self.venuesSegmentedControl.selectedSegmentIndex == NEARBY_VENUES) {
            venue = [self.nearbyVenues objectAtIndex:self.selectedSection];
        }
        controller.venueID = venue.venueID;
        controller.venueName = venue.venueName;
        
    }
}

#define FONT_SIZE 19.0f
#define CELL_CONTENT_WIDTH 193.0
#define CELL_CONTENT_MARGIN 13.0f

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.venues count] > 0 || [self.nearbyVenues count] > 0) {
        
        Venue *myVenue  = nil;
        if (self.venuesSegmentedControl.selectedSegmentIndex == FAVORITE_VENUES) {
            myVenue = [self.venues objectAtIndex:indexPath.section];
        } else if (self.venuesSegmentedControl.selectedSegmentIndex == NEARBY_VENUES) {
            myVenue = [self.nearbyVenues objectAtIndex:indexPath.section];
        }
       
 /*   if (myVenue.isTemporary) {
        NSString *text = myVenue.description;

        // Get a CGSize for the width and, effectively, unlimited height
        CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH - (CELL_CONTENT_MARGIN * 2), 20000.0f);
        // Get the size of the text given the CGSize we just made as a constraint
        CGSize size = [text sizeWithFont:[UIFont systemFontOfSize:FONT_SIZE] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
        // Get the height of our measurement, with a minimum of 71 (standard cell size)
        CGFloat height = MAX(size.height, 65.0f);
        // return the height, with a bit of extra padding in
        return height + (CELL_CONTENT_MARGIN * 2) + 15;
        
    } else*/
        if (self.showChooseButton)
            return 175.0; // 165
        else
            return 175.0; // 150
    
  /*  if (self.noResultsToDisplay) {
        CGRect screenRect = [[UIScreen mainScreen] bounds];
    //    return screenRect.size.height - 150;
        return screenRect.size.height;
    }
*/
    
    return 165;
    }
    
    return  0;
}

- (void)setMyVenueViewController:(AddVenueTVC *)controller andVenueID:(NSInteger)venueID andVenueName:(NSString *)venueName
{
    [self.delegate setVenueViewController:self withVenue:[[Venue alloc] initVenueWithName:venueName andVenueID:venueID]];
//    [self.venues addObject:venue];
 //   self.hiddenMessage.hidden = YES;
//    [self.tableView reloadData];
}

- (UIImage *)setMapImageWithLong:(float)longitude andLat:(float)latitude
{
    NSString *staticMapUrl = [NSString stringWithFormat:@"http://maps.google.com/maps/api/staticmap?markers=color:red|%f,%f&%@&sensor=true",latitude, longitude,@"zoom=16&size=276x167"];
    NSLog (@"%@", staticMapUrl);
    NSURL *mapUrl = [NSURL URLWithString:[staticMapUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    return [UIImage imageWithData: [NSData dataWithContentsOfURL:mapUrl]];
}
    
- (IBAction)chooseAction:(id)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    
    if (self.showChooseButton) {
        
    
        if (self.venuesSegmentedControl.selectedSegmentIndex == FAVORITE_VENUES) {
            [self.delegate setVenueViewController:self withVenue:(self.venues)[indexPath.section]];
        } else if (self.venuesSegmentedControl.selectedSegmentIndex == NEARBY_VENUES) {
            [self.delegate setVenueViewController:self withVenue:(self.nearbyVenues)[indexPath.section]];
        }
    
        // have to store state into server...will need mB loader and also thread IOS call.
    
        [self.navigationController popViewControllerAnimated:YES]; // go back to previous view controller
    
    } else {
        // must be checkin action
        self.selectedSection = indexPath.section;
        
        [self performSegueWithIdentifier:@"Checkin Segue" sender:self];
        
        
    }
}

- (void)setVenueDetailTViewController:(VenueDetailTVC *)controller withVenue:(Venue *)venue
{
    [self.delegate setVenueViewController:self withVenue:venue];
 //   [self.navigationController popViewControllerAnimated:YES];
}

- (void)initLocation
{
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
    self.proximityRange = 10;
    [locationManager startUpdatingLocation];
    
    
}

- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations {
    CLLocation *location = locations[0];
    NSLog(@"user location: lat%f - lon%f", location.coordinate.latitude, location.coordinate.longitude);
    [locationManager stopUpdatingLocation];
    
  
    
    [IOSRequest fetchVenuesNearMe:[self.preferences getUserID]
                         withRange:VENUE_NEARBY_RANGE
                     userLongitude:location.coordinate.longitude
                      userLatitude:location.coordinate.latitude
                      onCompletion:^(NSMutableArray *results) {
                          
                          if (![results isKindOfClass:[NSNull class]]) {
                              if ([results count] > 0) {
                                  [self.nearbyVenues removeAllObjects];
                          
                              for (id object in results) {
                                  NSMutableArray *sportArray = [[NSMutableArray alloc] init];
                              
                                  for (id sportObj in object[@"venue_sports"]) {
                                      Sport *sport = [[Sport alloc] initWithSportDetails:[sportObj[@"sport_id"] integerValue]
                                                                             andName:sportObj[@"sport_name"]
                                                                             andType:nil];
                                      [sportArray addObject:sport];
                                      
                                  }
                              
                                  AvgRating *avgRating = [[AvgRating alloc] initWithAvgRating:[object[@"venue_avg_rating"] floatValue]
                                                                                   andRatings:nil];
                              
                                  Venue *nearbyVenue = [[Venue alloc] initVenueWithName:object[@"name"]
                                                                             andVenueID:[object[@"venueID"] integerValue]
                                                                             andAddress:object[@"street"]
                                                                                andCity:object[@"city"]
                                                                           andStateName:object[@"state"]
                                                                             andStateID:[object[@"state_id"] integerValue]
                                                                            andLatitude:[object[@"latitude"] floatValue]
                                                                           andLongitude:[object[@"longitude"] floatValue]
                                                                         andDescription:nil
                                                                             andCreator:[object[@"creatorID"] integerValue]
                                                                              andSports:sportArray
                                                                           andAvgRating:avgRating
                                                        ];
                                  [nearbyVenue setIsTemporary:[object[@"is_temporary"] boolValue]];
                              
                                  [nearbyVenue setLongitude:[object[@"longitude"] floatValue]];
                                  [nearbyVenue setLatitude:[object[@"latitude"] floatValue]];
                                  
                                  id viObj = object[@"venue_thumb_pic"];
                                  VenueImage *venueImageThumb = [[VenueImage alloc] initWithVenueImageInfo:viObj[@"filestring"]
                                                                                                  andImage:nil isDefault:YES];
                                  [nearbyVenue setVenueImageThumb:venueImageThumb];

                              
                                  nearbyVenue.isSessionUserFavorite = [object[@"is_favorite"] boolValue];
                                  nearbyVenue.distanceFromSessionUser = [object[@"distance"] floatValue];
                                  [self.nearbyVenues addObject:nearbyVenue];
                              }
                          }
                          
                                  dispatch_async(dispatch_get_main_queue(), ^{
                                      
                                      if ([self.nearbyVenues count] == 0) {
                                          self.tableView.scrollEnabled = NO;
                                          self.noResultsToDisplay = YES;
                                          [self displayErrorOverlay:@"nearby"];
                                      } else {
                                          
                                      }
                                      [self.activityIndicator stopAnimating];
                                      self.tempView.hidden = YES;
                           //           self.overlay.hidden = YES;
                                      [self.tableView reloadData];
                                  });
                          } else {
                              dispatch_async(dispatch_get_main_queue(), ^{
                                 
                                  UIAlertView *alert = [[UIAlertView alloc] init];
                                  alert.title = @"Information Retrieval Error";
                                  alert.message = @"There was an error completing your request.  Please retry.";
                                  [alert addButtonWithTitle:@"OK"];
                                  [alert show];
                              });
                          }
                          
                      }];
    
    
}

- (void)displayErrorOverlay:(NSString *)type
{
    //NSLog(@"error overlay displayed!");
    self.overlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, [UIScreen mainScreen].bounds.size.height)];
    self.overlay.hidden = NO;
    self.overlay.backgroundColor = [UIColor lightLightGray];
    
    UIImage *img = [UIImage imageNamed:@"no_checkin"];
    UIImageView *icon = [[UIImageView alloc] initWithImage:img];
    [icon setFrame:CGRectMake(140, 136, 50, 50)];
    [icon setContentMode:UIViewContentModeCenter];
    
    UILabel *firstLine = [[UILabel alloc] initWithFrame:CGRectMake(30, 216, 260, 21)];
    firstLine.textColor = [UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:1.0];
    firstLine.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:15];
    if ([type isEqualToString:@"nearby"])
        firstLine.text = @"No Venues Nearby";
    else
        firstLine.text = @"No Favorite Venues";
    [firstLine setTextAlignment:NSTextAlignmentCenter];
    
    UILabel *secondLine = [[UILabel alloc] initWithFrame:CGRectMake(25, 236, 280, 42)];
    secondLine.textColor = [UIColor colorWithRed:170/255.0 green:170/255.0 blue:170/255.0 alpha:1.0];
    secondLine.font = [UIFont fontWithName:@"HelveticaNeue" size:13];
    
    if ([type isEqualToString:@"nearby"])
        secondLine.text = @"Press the \"+\" and be the first to add a venue in this area";
    else
        secondLine.text = @"You have not added any venues to your favorites list yet";
    
    secondLine.numberOfLines = 0;
    [secondLine setTextAlignment:NSTextAlignmentCenter];
    
    [self.overlay addSubview:icon];
    [self.overlay addSubview:firstLine];
    [self.overlay addSubview:secondLine];
    
    [self.tableView addSubview:self.overlay];
}
/*
- (IBAction)favoriteAction:(id)sender
{
    CGPoint hitPoint = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *hitIndex = [self.tableView indexPathForRowAtPoint:hitPoint];
    
    Venue *venue = nil;
    
    if (self.venuesSegmentedControl.selectedSegmentIndex == FAVORITE_VENUES) {
        venue = [self.venues objectAtIndex:hitIndex.section];
        
        [IOSRequest changeFavoriteStatusForVenue:venue.venueID
                                      withStatus:NO
                                        withUser:[self.preferences getUserID]
                                    onCompletion:^(NSMutableArray *results) {
                                        
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            VenueCell *cell = (VenueCell *)[self.tableView cellForRowAtIndexPath:hitIndex];
                                            [cell.favoriteButton setTitle:@"Favorite" forState:UIControlStateNormal];
                                            self.shouldLoadFavoriteVenues = YES;
                                            self.shouldLoadNearbyVenues = YES;
                                            [self reloadData];
                                        });
                                    }];
        
    } else if (self.venuesSegmentedControl.selectedSegmentIndex == NEARBY_VENUES) {
        venue  = [self.nearbyVenues objectAtIndex:hitIndex.section];
        
        if (venue.isSessionUserFavorite) {
            [IOSRequest changeFavoriteStatusForVenue:venue.venueID
                                          withStatus:NO
                                            withUser:[self.preferences getUserID]
                                        onCompletion:^(NSMutableArray *results) {
                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                VenueCell *cell = (VenueCell *)[self.tableView cellForRowAtIndexPath:hitIndex];
                                                [cell.favoriteButton setTitle:@"Favorite" forState:UIControlStateNormal];
                                                self.shouldLoadNearbyVenues = YES;
                                                self.shouldLoadFavoriteVenues = YES;
                                                [self reloadData];
                                            });
                                            
                                        }];
        } else {
            [IOSRequest changeFavoriteStatusForVenue:venue.venueID
                                          withStatus:YES
                                            withUser:[self.preferences getUserID]
                                        onCompletion:^(NSMutableArray *results) {
                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                VenueCell *cell = (VenueCell *)[self.tableView cellForRowAtIndexPath:hitIndex];
                                                [cell.favoriteButton setTitle:@"UnFavorite" forState:UIControlStateNormal];
                                                self.shouldLoadNearbyVenues = YES;
                                                self.shouldLoadFavoriteVenues = YES;
                                                [self reloadData];
                                            });
                                            
                                        }];
        }
        
    }
    
    
}
*/
- (IBAction)venueDetailAction:(id)sender
{
    CGPoint hitPoint = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *hitIndex = [self.tableView indexPathForRowAtPoint:hitPoint];
    
    self.selectedSection = hitIndex.section;
    
    [self performSegueWithIdentifier:@"Venue Detail Segue" sender:self];
}

- (void)setViewController:(AddVenueIntroVC *)controller withVenueID:(NSInteger)venueID andVenueName:(NSString *)venueName
{
    Venue *v = [[Venue alloc] initVenueWithName:venueName andVenueID:venueID];
    [self.delegate setVenueViewController:self withVenue:v];
}

@end
