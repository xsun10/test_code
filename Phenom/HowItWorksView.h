//
//  HowItWorksView.h
//  Phenom
//
//  Created by James Chung on 3/15/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HowItWorksView : UIView

@property (strong, nonatomic) NSString *slideImageFile;
@end
