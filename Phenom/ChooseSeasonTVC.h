//
//  ChooseSeasonTVC.h
//  Vaiden
//
//  Created by James Chung on 5/8/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "CustomBaseTVC.h"


@class ChooseSeasonTVC;

@protocol ChooseSeasonTVC_Delegate <NSObject>
- (void)setWithViewController:(ChooseSeasonTVC *)controller withSeason:(NSString *)season;

@end

@interface ChooseSeasonTVC : CustomBaseTVC

@property (nonatomic, weak) id <ChooseSeasonTVC_Delegate> delegate;
@property (nonatomic, strong) NSString *chosenSeason;


@end
