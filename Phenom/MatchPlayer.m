//
//  MatchPlayer.m
//  Phenom
//
//  Created by James Chung on 5/1/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "MatchPlayer.h"
#import "Player.h"

@implementation MatchPlayer


- (MatchPlayer *) initWithPlayerDetails:(NSInteger)userID
                     andProfileStr:(NSString *)profileBaseString
                       andUserName:(NSString *)userName
                       andFullName:(NSString *)fullName
                       andLocation:(NSString *)location
                          andAbout:(NSString *)about
                               andCoins:(float)coins
                         andStatus:(NSString *)status
{
    self = [super initWithPlayerDetails:userID
                          andProfileStr:profileBaseString
                            andUserName:userName
                            andFullName:fullName
                            andLocation:location
                               andAbout:about
                               andCoins:coins];
    
    if (self) {
        _status = status;
    }
    return self;
    
}

- (MatchPlayer *) initWithPlayerDetails:(NSInteger)userID
                            andUserName:(NSString *)userName
                            
{
    self = [super initWithPlayerDetails:userID andUserName:userName
            ];
    
    if (self) {
        _status = @"";
    
    }
    return self;
}
    

// Given an object array can make a match player object
+ (MatchPlayer *)makeMatchCompetitorObject:(NSArray *)objectArray
                                 forPlayer:(NSInteger)playerNum
                              forSportName:(NSString *)sportName
                                   andType:(NSString *)sportType
{
    MatchPlayer *player = nil;
    
    NSInteger counter = 0;
    
    
    for (id obj in objectArray) {
        if (counter == playerNum - 1) {
            player = [[MatchPlayer alloc] initWithPlayerDetails:[obj[@"competitor_id"] integerValue]
                                                  andProfileStr:obj[@"profile_pic_string"]
                                                    andUserName:obj[@"username"]
                                                    andFullName:obj[@"fullname"]
                                                    andLocation:obj[@"location"]
                                                       andAbout:obj[@"about"]
                                                       andCoins:[obj[@"coins"] floatValue]
                                                      andStatus:obj[@"status"]];
            
            
            PlayerSport *sport = [[PlayerSport alloc] initWithSportDetails:sportName
                                                                   andType:sportType
                                                                  andLevel:obj[@"level"]
                                                            andSportRecord:obj[@"win_loss_record"]];
            [player.playerSports addObject:sport];
            
            
            
        }
        
        counter ++;
    }
    
    
    return player;
}


/*
- (NSString *)getProfileStringOfCompetitor:(NSInteger)competitorID inArray:(NSArray *)competitorArray
{
    for (MatchPlayer *player in competitorArray) {
        if (player.userID == competitorID) {
            return player.profileBaseString;
        }
    }
    return @"";
}
*/

@end
