//
//  MatchesNearbyCell.m
//  Phenom
//
//  Created by James Chung on 5/22/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "MatchesNearbyCell.h"
#import "MatchesNearbyCollectionCell.h"
#import "MatchPlayer.h"
#import "IOSRequest.h"
#import "AFNetworking.h"
#import "S3Tools.h"
#import "UIImage+ProportionalFill.h"

@implementation MatchesNearbyCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
    }
    return self;
}

- (void)awakeFromNib
{
    self.playerCollectionView.delegate = self;
    self.playerCollectionView.dataSource = self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
  //  return self.minCompetitors;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"status = %@", @"ACCEPTED"];
    NSArray *matchParticipants = [self.competitors filteredArrayUsingPredicate:predicate];
    
    return [matchParticipants count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Player Image Cell";
    
    MatchesNearbyCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"status = %@", @"ACCEPTED"];
    NSArray *matchParticipants = [self.competitors filteredArrayUsingPredicate:predicate];
    
//    if (indexPath.item < [matchParticipants count]) {
        MatchPlayer *player = matchParticipants[indexPath.row];
        
  //      NSString *urlString = [IOSRequest getPhotoURL:1 forID:player.userID];
  //      [cell.profilePic setImageWithURL:[NSURL URLWithString:urlString]
  //                      placeholderImage:[UIImage imageNamed:@"avatar.png"]];
        
        NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                baseString:player.profileBaseString];
        
        [cell.profilePic setImageWithURL:[NSURL URLWithString:url]
                          placeholderImage:[UIImage imageNamed:@"avatar square"]];
        
    //    [UIImage makeRoundedImage:cell.profilePic withRadius:25];
    
        cell.profilePic.layer.borderColor = [UIColor whiteColor].CGColor;
        cell.profilePic.layer.borderWidth = 1;
    cell.usernameLabel.text = player.userName;
    
 //   } else {
 //       [cell.profilePic setImage:[UIImage imageNamed:@"avatar round slot"]];
 //   }
    return cell;
}

@end
