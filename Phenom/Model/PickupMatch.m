//
//  PickupMatch.m
//  Phenom
//
//  Created by James Chung on 4/3/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "PickupMatch.h"
#import "MatchPlayer.h"
#import "UserPreferences.h"

@interface PickupMatch ()
@property (nonatomic, strong) UserPreferences *preferences;

@end

@implementation PickupMatch


- (NSMutableArray *)experienceArray
{
    if (!_experienceArray) _experienceArray = [[NSMutableArray alloc] init];
    return _experienceArray;
}

- (PickupMatch *)init
{
    self = [super init];
    
    if (self) {
        _competitorArray = [[NSMutableArray alloc] init];
        _experienceArray = [[NSMutableArray alloc] init];
    }
    return  self;
}
- (PickupMatch *) initMatchWithDetails:(NSInteger)matchID
                              andMatchName:(NSString *)matchName
                                 isPrivate:(BOOL)isPrivate
                                 withSport:(Sport *)sport
                               withMessage:(NSString *)message
                                onDateTime:(NSDate *)dateTime
                                   atVenue:(Venue *)venue
                             createdByUser:(NSInteger)creatorID
                            minCompetitors:(NSInteger)minCompetitors
                           withCompetitors:(NSMutableArray *)competitorArray
                            withExperience:(NSMutableArray *)experience
                          andMatchType:(MatchType *)matchType
                                 andActive:(BOOL)active
                        andMatchStatus:(NSString *)matchStatus
{
    self = [super initMatchWithDetails:matchID
                          andMatchName:matchName
                             isPrivate:isPrivate
                             withSport:sport
                           withMessage:message
                            onDateTime:dateTime
                               atVenue:venue
                         createdByUser:creatorID
                        andActive:active
                        andMatchStatus:matchStatus];

    if (self) {
        _minCompetitors = minCompetitors;
        _matchType = matchType;
        _competitorArray = [[NSMutableArray alloc] initWithArray:competitorArray];
        _experienceArray = experience;
    }
    return self;
    
}

// with matchCreatorUsername and matchCreatorProfilePic
- (PickupMatch *) initMatchWithDetails:(NSInteger)matchID
                          andMatchName:(NSString *)matchName
                             isPrivate:(BOOL)isPrivate
                             withSport:(Sport *)sport
                           withMessage:(NSString *)message
                            onDateTime:(NSDate *)dateTime
                               atVenue:(Venue *)venue
                         createdByUser:(NSInteger)creatorID
                          withUsername:(NSString *)matchCreatorUsername
                         andProfileStr:(NSString *)matchCreatorProfileBaseString
                        minCompetitors:(NSInteger)minCompetitors
                       withCompetitors:(NSMutableArray *)competitorArray
                        withExperience:(NSMutableArray *)experience
                          andMatchType:(MatchType *)matchType
                             andActive:(BOOL)active
                        andMatchStatus:(NSString *)matchStatus
{
    self = [super initMatchWithDetails:matchID
                          andMatchName:matchName
                             isPrivate:isPrivate
                             withSport:sport
                           withMessage:message
                            onDateTime:dateTime
                               atVenue:venue
                         createdByUser:creatorID
                          withUsername:matchCreatorUsername
                         andProfileStr:matchCreatorProfileBaseString
                             andActive:active
                    andMatchStatus:matchStatus];
    
    if (self) {
        _minCompetitors = minCompetitors;
        _matchType = matchType;
        _competitorArray = [[NSMutableArray alloc] initWithArray:competitorArray];
        _experienceArray = experience;
    }
    return self;
    
}

- (UserPreferences *) preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

+ (NSMutableArray *)makeCompetitorArray:(NSArray *)competitorResults forSportName:(NSString *)sportName andType:(NSString *)sportType
{
    // competitorResults should be an array of arrays
    NSMutableArray *competitorInfoSet = [[NSMutableArray alloc] init];
    
    for (id object in competitorResults) {
        
        MatchPlayer *player = [[MatchPlayer alloc] initWithPlayerDetails:[object[@"competitor_id"] integerValue]
                                                           andProfileStr:object[@"profile_pic_string"]
                                                             andUserName:object[@"username"]
                                                             andFullName:object[@"fullname"]
                                                                 andLocation:object[@"location"]
                                                                andAbout:object[@"about"]
                                                                andCoins:[object[@"coins"] floatValue]
                                                               andStatus:object[@"status"]];
        [player setWasInvited:[object[@"was_invited"] boolValue]];
        
        // Rather than add all sports of the competitor, we are going to add just the sport that is
        // relevant to this match.  In this case, it is an individual sport
        
        PlayerSport *sport = [[PlayerSport alloc] initWithSportDetails:sportName
                                                               andType:sportType
                                                              andLevel:object[@"sport_level"]
                                                        andSportRecord:object[@"win_loss_record"]];
        [player.playerSports addObject:sport];
        
        [competitorInfoSet addObject:player];
        
    }
    
    return competitorInfoSet;
}

// This method go throughs various conditions to determine the match status
/*
+ (NSString *)getStatus:(PickupMatch *)pMatch sessionUserID:(NSInteger)sessionUserID
{
    NSString *resultString = @"";
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"status = %@", @"CONFIRMED"];
    NSArray *matchParticipants = [pMatch.competitorArray filteredArrayUsingPredicate:predicate];

    NSPredicate *sessionUserPredicate = [NSPredicate predicateWithFormat:@"userID = %d", sessionUserID];
    NSArray *sessionUserList = [pMatch.competitorArray filteredArrayUsingPredicate:sessionUserPredicate];
    
    if ([pMatch.dateTime compare:[NSDate date]] == NSOrderedAscending) {
        resultString = @"This match is complete";
        
    }  else if ([sessionUserList count] > 0) { // session user was invited to match
        // session user was invited
         MatchPlayer *sessionPlayer = [sessionUserList objectAtIndex:0];
        
        if ([sessionPlayer.status isEqualToString:@"CONFIRMED"]) {
            resultString = @"You have confirmed to attend this match";
        } else if ([sessionPlayer.status isEqualToString:@"DECLINED"]) {
            resultString = @"You have declined to attend this match";
        } else if (pMatch.minCompetitors > [matchParticipants count]){
            resultString = @"You are welcome to join this match";
        }
    
    } else if (pMatch.minCompetitors <= [matchParticipants count]) { //enough players have confirmed
        // check if match met min competitors
        resultString = @"This match has enough players and is confirmed";
        
    } else if (pMatch.private == YES) { // private and not enough players
        // session user was not invited
        resultString = @"This match is private";
            
    } else if (pMatch.private == NO) { // public and not enough players
        resultString = @"You are welcome to join this match";
    }
    
    return resultString;
}
*/


// Status: UNCONFIRMED / MANUAL CONFIRMED / RSVP CONFIRMED / CANCELLED / NEVER OCCURRED
// UNCONFIRMED: Match has not been manually or RSVP confirmed nor has it been cancelled
// MANUAL_CONFIRMED: Match creator confirmed match regardless of number of RSVP's
// RSVP_CONFIRMED: Enough players RSVP to play in match to meet the matches min competitors requirements
// CANCELLED: Match has been cancelled
// NEVER_OCCURRED: Match never happened


- (BOOL)isMatchConfirmed
{
    /*
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"status = %@", @"CONFIRMED"];
    NSArray *matchParticipants = [self.competitorArray filteredArrayUsingPredicate:predicate];
    
    if ([matchParticipants count] >= self.minCompetitors) {
        return YES;
    }
    
    return NO;*/
    if ([self.matchStatus isEqualToString:@"RSVP_CONFIRMED"] || [self.matchStatus isEqualToString:@"MANUAL_CONFIRMED"]) {
        return YES;
    }
    return NO;
}

- (NSString *)getMatchStatus
{
    NSString *resultString = @"";
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"status = %@", @"ACCEPTED"];
    NSArray *matchParticipants = [self.competitorArray filteredArrayUsingPredicate:predicate];
    
    NSDate *thirtyMinAgo = [self.dateTime dateByAddingTimeInterval:1800];
    
    // Match is in past
    if ([thirtyMinAgo compare:[NSDate date]] == NSOrderedAscending) {
        if (self.isActive && [self isMatchConfirmed]) {
            resultString = @"This match is complete";
            
        } else if ([self.matchStatus isEqualToString:@"NEVER_OCCURRED"]) {
            resultString = @"This match never occurred";
            
        } else if ([self.matchStatus isEqualToString:@"CANCELLED"]) {
            resultString = @"This match was cancelled";
            
        } else if (!self.isActive) {
            resultString = @"This match is inactive";

        } else {
            resultString = @"This match has expired";
        }
    } else {
        
       if ([self.matchStatus isEqualToString:@"CANCELLED"]) {
           resultString = @"This match was cancelled";
        
       } else if (!self.isActive) {
           resultString = @"This match is inactive";
    
       } else if (self.minCompetitors <= [matchParticipants count]) { //enough players have confirmed
           // check if match met min competitors
           resultString = @"This match is ready to play";
        
       }  else if ([self.matchStatus isEqualToString:@"MANUAL_CONFIRMED"] || [self.matchStatus isEqualToString:@"RSVP_CONFIRMED"]) {
           resultString = @"This match is ready to play";
      
       } else { // public and not enough players
           resultString = @"This match needs players";
       
       }
    }
    
    return resultString;

}

- (NSString *)getSessionUserStatus
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userID = %d", [self.preferences getUserID]];
    NSArray *sessionUserInArray = [self.competitorArray filteredArrayUsingPredicate:predicate];
    
    NSString *resultString;
    
    NSDate *thirtyMinAgo = [self.dateTime dateByAddingTimeInterval:1800];
    
    if ([thirtyMinAgo compare:[NSDate date]] == NSOrderedAscending) {
        if ([sessionUserInArray count] > 0) {
            MatchPlayer *sessionUser = [sessionUserInArray objectAtIndex:0];
            
            if ([sessionUser.status isEqualToString:@"UNCONFIRMED"]) {
                resultString = @"You did not RSVP for this match";
                
            } else if ([sessionUser.status isEqualToString:@"ACCEPTED"]) {
                resultString = @"You RSVP'd to play in this match";
                
            } else if ([sessionUser.status isEqualToString:@"DECLINED"]) {
                resultString = @"You declined to play in this match";
                
            } else {
                resultString = @"You did not RSVP for this match";
            }
        } else { // otherwise, the player wasn't invited...If match is public, should have option to join
            resultString = @"You did not RSVP for this match";
        }
    } else {
        
        if ([sessionUserInArray count] > 0) {
            MatchPlayer *sessionUser = [sessionUserInArray objectAtIndex:0];
            
            if ([sessionUser.status isEqualToString:@"UNCONFIRMED"]) {
                resultString = @"You have not RSVP'd for this match";
                
            } else if ([sessionUser.status isEqualToString:@"ACCEPTED"]) {
                resultString = @"You RSVP'd to play in this match";
                
            } else if ([sessionUser.status isEqualToString:@"DECLINED"]) {
                resultString = @"You declined to play in this match";
                
            } else {
                resultString = @"You have not RSVP'd for this match";
            }
        } else { // otherwise, the player wasn't invited...If match is public, should have option to join
            resultString = @"You have not RSVP'd for this match";
        }
    }
    

    return resultString;
}

- (NSString *)getSessionUserCodedStatus
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userID = %d", [self.preferences getUserID]];
    NSArray *sessionUserInArray = [self.competitorArray filteredArrayUsingPredicate:predicate];

    if ([sessionUserInArray count] > 0) {
        MatchPlayer *mp = [sessionUserInArray objectAtIndex:0];
        return mp.status;
    }
    return @"UNCONFIRMED";
}

- (NSInteger)getNumConfirmed
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"status = %@", @"ACCEPTED"];
    NSArray *filteredArray = [self.competitorArray filteredArrayUsingPredicate:predicate];
    
    return [filteredArray count];
}

- (BOOL)checkLoggedInUserConfirmedBOOL
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"status = %@", @"ACCEPTED"];
    NSArray *filteredArray = [self.competitorArray filteredArrayUsingPredicate:predicate];
    
    BOOL myFlag = NO;
    
    for (MatchPlayer *player in filteredArray) {
        if (player.userID == [self.preferences getUserID]) {
            myFlag = YES;
            break;
        }
    }
    return myFlag;
}


+ (NSString *)getExperienceString:(PickupMatch *)pickupMatch
{
    NSString *resultString = @"";
    NSInteger myCounter = 0;
    
    for (id object in pickupMatch.experienceArray) {
        if (myCounter > 0)
            resultString = [resultString stringByAppendingFormat:@", %@", object];
        else
            resultString = object;
        myCounter ++;
    }
    
    return resultString;
}

- (BOOL)canSessionUserRSVPForMatch
{
    if ([self isSessionUserInCompetitorArray])
        return YES;
    else if (!self.private)
        return YES;
    
    return NO;
}

- (BOOL)isSessionUserInCompetitorArray
{
    for (Player *player in self.competitorArray) {
        if (player.userID == [self.preferences getUserID]) {
            return YES;
        }
    }
    return NO;
}

- (Player *)getMatchCreatorObject
{
    Player *player = nil;
    
    for (Player *object in self.competitorArray) {
        if (object.userID == self.matchCreatorID) {
            player = object;
            break;
        }
    }
    
    return player;
}

- (void)loadCompetitorArrayFromObject:(NSArray *)competitorObject
{
    for (id object in competitorObject) {
        
            MatchPlayer *p = [[MatchPlayer alloc] initWithPlayerDetails:[object[@"competitor_id"] integerValue]
                                                        andProfileStr:object[@"profile_pic_string"]
                                                          andUserName:object[@"username"]
                                                          andFullName:object[@"fullname"]
                                                          andLocation:object[@"location"]
                                                             andAbout:object[@"about"]
                                                             andCoins:[object[@"coins"] integerValue]
                                                            andStatus:object[@"status"]];
        [self.competitorArray addObject:p];
        
    }
}

- (BOOL)wasPlayerInvited:(NSInteger)playerID
{
    for (MatchPlayer *mp in self.competitorArray) {
        if (mp.userID == playerID) {
            if (mp.wasInvited)
                return YES;
            else
                return NO;
        }
    }
    return NO;
}

- (MatchPlayer *)getPlayerOtherThanSessionUser:(NSInteger)sessionUserID
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"status = %@", @"ACCEPTED"];
    NSArray *filteredArray = [self.competitorArray filteredArrayUsingPredicate:predicate];

    for (MatchPlayer *p in filteredArray) {
        if (p.userID != sessionUserID)
            return p;
    }
    return  nil;
}
@end
