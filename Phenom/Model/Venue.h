//
//  Venue.h
//  Phenom
//
//  Created by James Chung on 4/3/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AvgRating.h"
#import "VenueImage.h"

@interface Venue : NSObject

@property (nonatomic, strong) NSString *venueName;
@property (nonatomic) NSInteger venueID;
@property (nonatomic, strong) NSString *streetAddress;
@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *stateName;
@property (nonatomic) NSInteger stateID;
@property (nonatomic, strong) NSString *description;
@property (nonatomic) NSInteger creatorID;


@property (nonatomic) float latitude;
@property (nonatomic) float longitude;
@property (nonatomic) float distanceFromSessionUser;
@property (nonatomic) BOOL isSessionUserFavorite;
@property (nonatomic) BOOL isTemporary;

//@property (nonatomic, strong) NSString *thumbnailURLString; 
@property (nonatomic, strong) NSMutableArray *sportsArray;
@property (nonatomic, strong) NSMutableArray *picsArray;
@property (nonatomic, strong) AvgRating *avgRating;
@property (nonatomic, strong) VenueImage *venueImageThumb;
@property (nonatomic, strong) NSMutableArray *venueCheckInsArray;

-(Venue *)initVenueWithName:(NSString *)venueName
                 andVenueID:(NSInteger)venueID
                 andAddress:(NSString *)streetAddress
                    andCity:(NSString *)city
               andStateName:(NSString *)stateName
                 andStateID:(NSInteger)stateID
                andLatitude:(float)latitude
               andLongitude:(float)longitude
             andDescription:(NSString *)description
                 andCreator:(NSInteger)creatorID
                  andSports:(NSMutableArray *)sportsArray
               andAvgRating:(AvgRating *)theRating;


// will have to deprecate this one...
-(Venue *)initVenueWithName:(NSString *)venueName
                 andVenueID:(NSInteger)venueID
                 andAddress:(NSString *)streetAddress
                    andCity:(NSString *)city
               andStateName:(NSString *)stateName
                 andStateID:(NSInteger)stateID
                andLatitude:(float)latitude
               andLongitude:(float)longitude
             andDescription:(NSString *)description
                 andCreator:(NSInteger)creatorID;

- (Venue *)initVenueWithName:(NSString *)venueName
                  andVenueID:(NSInteger)venueID;

@end
