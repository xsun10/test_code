//
//  Match.m
//  Phenom
//
//  Created by James Chung on 4/3/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "Match.h"
#import "MatchPlayer.h"

@implementation Match



- (Match *) initMatchWithDetails:(NSInteger)matchID
                    andMatchName:(NSString *)matchName
                       isPrivate:(BOOL)isPrivate
                       withSport:(Sport *)sport
                     withMessage:(NSString *)message
                      onDateTime:(NSDate *)dateTime
                         atVenue:(Venue *)venue
                   createdByUser:(NSInteger)matchCreatorID
                       andActive:(BOOL)active
               andMatchStatus:(NSString *)matchStatus
{
    self = [super init];
    
    if (self) {
        _matchID = matchID;
        _matchName = matchName;
        _private = isPrivate;
        _sport  = sport;
        _message = message;
        _dateTime = dateTime;
        _venue = venue;
        _matchCreatorID = matchCreatorID;
        _isActive = active;
        _matchStatus = matchStatus;
        _matchCreatorProfileBaseString = nil;
        _matchCreatorUsername = @"";
        _matchHistoryArray = [[NSMutableArray alloc] init];
    }
    
    return self;
}

// with matchCreatorUsername and matchCreatorProfilePic
- (Match *) initMatchWithDetails:(NSInteger)matchID
                    andMatchName:(NSString *)matchName
                       isPrivate:(BOOL)isPrivate
                       withSport:(Sport *)sport
                     withMessage:(NSString *)message
                      onDateTime:(NSDate *)dateTime
                         atVenue:(Venue *)venue
                   createdByUser:(NSInteger)matchCreatorID
                    withUsername:(NSString *)matchCreatorUsername
                   andProfileStr:(NSString *)matchCreatorProfileBaseString
                       andActive:(BOOL)active
                  andMatchStatus:(NSString *)matchStatus
{
    self = [super init];
    
    if (self) {
        _matchID = matchID;
        _matchName = matchName;
        _private = isPrivate;
        _sport  = sport;
        _message = message;
        _dateTime = dateTime;
        _venue = venue;
        _matchCreatorID = matchCreatorID;
        _isActive = active;
        _matchStatus = matchStatus;
        _matchCreatorProfileBaseString = matchCreatorProfileBaseString;
        _matchCreatorUsername = matchCreatorUsername;
        _matchHistoryArray = [[NSMutableArray alloc] init];
        
    }
    
    return self;
}

+ (NSInteger)getIDOfOpponent:(NSArray *)competitors sessionUserID:(NSInteger)sessionUserID
{
    for (id object in competitors) {
        if ([object[@"competitor_id"] integerValue] != sessionUserID) {
            return [object[@"competitor_id"] integerValue];
        }
    }
    return 0;
}

// I could actually get this info from the server...but doing this way for speed's sake
+ (NSString *)getUsernameForID:(NSInteger)userID inArray:(NSMutableArray *)competitors
{
    NSString *username = @"";
    
    for (MatchPlayer *mp in competitors) {
        if (mp.userID == userID)
            return mp.userName;
    }
    
    return username;
}

- (BOOL)isMatchConfirmed
{
    if ([self.matchStatus isEqualToString:@"MANUAL_CONFIRMED"] || [self.matchStatus isEqualToString:@"RSVP_CONFIRMED"])
        return YES;
    
    return NO;
}

- (BOOL)isMatchCancelled
{
    if ([self.matchStatus isEqualToString:@"CANCELLED"])
        return YES;
    return NO;
}

- (void)setMatchHistoryWithDataObj:(id)dataObj
{
    for (id subObject in dataObj) {
        [self.matchHistoryArray addObject:[[MatchHistoryObj alloc] initWithHistoryObject:subObject]];
    }
}

- (UIImage *)getMatchDetailPageBackground
{
    if ([self.sport.sportName isEqualToString:@"basketball"]) {
        return [UIImage imageNamed:@"Basketball Match Background"];
    } else if ([self.sport.sportName isEqualToString:@"tennis"]) {
        return [UIImage imageNamed:@"Tennis Match Background"];
    }  else if ([self.sport.sportName isEqualToString:@"ping-pong"]) {
        return [UIImage imageNamed:@"Match Ping-Pong Background"];
    }
    return [UIImage imageNamed:@"Blue Back"];
}

@end
