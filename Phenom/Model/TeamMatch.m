//
//  TeamMatch.m
//  Phenom
//
//  Created by James Chung on 4/3/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "TeamMatch.h"

@implementation TeamMatch

- (TeamMatch *) initMatchWithDetails:(NSInteger)matchID
                        andMatchName:(NSString *)matchName
                           isPrivate:(BOOL)isPrivate
                           withSport:(Sport *)sport
                         withMessage:(NSString *)message
                          onDateTime:(NSDate *)dateTime
                             atVenue:(Venue *)venue
                       createdByUser:(NSInteger)creatorID
                           withTeam1:(Team *)team1
                           withTeam2:(Team *)team2
                           andActive:(BOOL)active
                        andMatchType:(MatchType*)mType

{
    
    self = [super initMatchWithDetails:matchID
                          andMatchName:matchName
                             isPrivate:isPrivate
                             withSport:sport
                           withMessage:message
                            onDateTime:dateTime
                               atVenue:venue
                         createdByUser:creatorID
                             andActive:active
                        andMatchStatus:@""];
    
    if (self) {
        _team1 = team1;
        _team2 = team2;
        _matchType = mType;
     
  //      _scoreIndMatch = [[IndividualMatchScore alloc] initWithMatchScore];
    }
    return self;
    
}

- (BOOL)isUserATeamCaptain:(NSInteger)userID
{
    if (self.team1.captain.userID == userID || self.team2.captain.userID == userID) {
        return YES;
    }
    return NO;
}

- (Team *)getTeamForCaptain:(NSInteger)userID
{
    if (self.team1.captain.userID == userID) {
        return self.team1;
    } else if (self.team2.captain.userID == userID) {
        return self.team2;
    }
    return nil;
}

- (Team *)getOtherTeamThatDoesntHaveCaptain:(NSInteger)userID
{
    if (self.team1.captain.userID != userID) {
        return self.team1;
    } else if (self.team2.captain.userID != userID) {
        return self.team2;
    }
    return nil;
}

// returns "YES" if
- (BOOL)isScoreConfirmed
{
    if (self.scoreTeamMatch.scoreConfirmedBy != 0)
        return YES;
    return NO;
}

- (BOOL)isScoreRecorded
{
    if (self.scoreTeamMatch.scoreRecordedBy != 0)
        return YES;
    return NO;
}

- (BOOL)isScoreDisputed
{
    if (self.scoreTeamMatch.scoreDisputedBy != 0)
        return YES;
    return NO;
}
@end
