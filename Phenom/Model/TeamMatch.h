//
//  TeamMatch.h
//  Phenom
//
//  Created by James Chung on 4/3/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "Match.h"
#import "Team.h"
#import "MatchType.h"
#import "TeamMatchScore.h"

@interface TeamMatch : Match

@property (nonatomic, strong) Team *team1;
@property (nonatomic, strong) Team *team2;
@property (nonatomic, strong) TeamMatchScore *scoreTeamMatch;

@property (nonatomic, strong) MatchType *matchType;

- (TeamMatch *) initMatchWithDetails:(NSInteger)matchID
                        andMatchName:(NSString *)matchName
                           isPrivate:(BOOL)isPrivate
                           withSport:(Sport *)sport
                         withMessage:(NSString *)message
                          onDateTime:(NSDate *)dateTime
                             atVenue:(Venue *)venue
                       createdByUser:(NSInteger)creatorID
                           withTeam1:(Team *)team1
                           withTeam2:(Team *)team2
                           andActive:(BOOL)active
                        andMatchType:(MatchType*)mType;

- (BOOL)isUserATeamCaptain:(NSInteger)userID;
- (Team *)getTeamForCaptain:(NSInteger)userID;
- (Team *)getOtherTeamThatDoesntHaveCaptain:(NSInteger)userID;

- (BOOL)isScoreConfirmed;
- (BOOL)isScoreRecorded;
- (BOOL)isScoreDisputed;

@end
