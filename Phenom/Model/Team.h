//
//  Team.h
//  Phenom
//
//  Created by James Chung on 4/3/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TeamPlayer.h"

@interface Team : NSObject

@property (nonatomic) NSInteger teamID;
@property (strong, nonatomic) NSMutableArray *players;
@property (strong, nonatomic) Player *captain;
@property (nonatomic, strong) NSString *teamName;
@property (nonatomic, strong) NSString *picStringName;
@property (nonatomic, strong) NSString *contactsWhoAreMembersShortDescription;
@property (nonatomic, strong) NSString *status; // for individual match we actually subclassed MatchPlayer from Player for this...not doing this for teams for simplicity sake

@property (nonatomic) NSInteger numMembers;

- (Team *)initWithTeamDetails:(NSInteger)teamID
                  andTeamName:(NSString *)teamName
                   andCaptain:(TeamPlayer *)teamCaptain
                 andPicString:(NSString *)picString;

// Data must always be in same format when taken from server
- (void)loadTeamMembersFromServerObject:(id)serverArray;

// Iterates through team members and sets the captain Player object if it is found
- (void)setCaptainFromTeamMembersArray:(NSInteger)captainID;

// Returns the number of players who have agreed to be a part of the team
- (NSInteger) getNumConfirmedMembers;

// If session user already confirmed to become a member of team
- (BOOL) isSessionUserAlreadyConfirmedMember:(NSInteger)sessionUserID;

// If session user was invited but did not join team yet
- (BOOL) isSessionUserUnconfirmedAndAllowedToJoinTeam:(NSInteger)sessionUserID;

@end
