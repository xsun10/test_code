//
//  Contacts.h
//  Phenom
//
//  Created by James Chung on 3/26/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

/*
 * This class will be subclassed and not used directly.
 */

#import <Foundation/Foundation.h>

@interface Contacts : NSObject

@property (strong, nonatomic) NSMutableArray *playerArray;
-(NSInteger)numberOfContacts;

@end
