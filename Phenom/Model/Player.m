//
//  Player.m
//  Phenom
//
//  Created by James Chung on 3/22/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "Player.h"
#import "IOSRequest.h"


@interface Player ()

@property (nonatomic, strong) NSDictionary *userInfo;
@end

@implementation Player

- (Player *) initWithPlayerDetails:(NSInteger)userID
                       andUserName:(NSString *)userName
               andProfilePicString:(NSString *)profileBaseString
{
    self = [super init];
    
    if (self) {
        _userID = userID;
        _profileBaseString = profileBaseString;
        _userName = userName;
        _fullName = @"";
        _location = @"";
        _about = @"";
        _coins = 0;
        _isSelected = NO;
        _playerSports = [[NSMutableArray alloc] init];
        _canReceivePushNotifications = NO;
    }
    return self;
    
}

//using this one...
- (Player *) initWithPlayerDetails:(NSInteger)userID
                andProfileStr:(NSString *)profileBaseString
                   andUserName:(NSString *)userName
                       andFullName:(NSString *)fullName
                       andLocation:(NSString *)location
                          andAbout:(NSString *)about
                          andCoins:(float)coins
{
    self = [super init];
    
    if (self) {
        _userID = userID;
        _profileBaseString = profileBaseString;
        _userName = userName;
        _fullName = fullName;
        _location = location;
        _about = about;
        _isSelected = NO;
        _coins = coins;
        _playerSports = [[NSMutableArray alloc] init];
        _canReceivePushNotifications = NO;
    }
    return self;
    
}

- (Player *) initWithPlayerDetails:(NSInteger)userID
                       andUserName:(NSString *)userName
{
    self = [super init];
    
    if (self) {
        _userID = userID;
        _userName = userName;
        _fullName = @"";
        _location = @"";
        _about = @"";
        _isSelected = NO;
        _profileBaseString = nil;
        _playerSports = [[NSMutableArray alloc] init];
        _canReceivePushNotifications = NO;
    }
    return self;
}

- (void) addPlayerSport:(PlayerSport *)sport
{
    [self.playerSports addObject:sport];
}

- (void) setUserInfo:(NSDictionary *)userInfo
{
    if (_userInfo != userInfo) {
        _userInfo = userInfo;
    }
    self.fullName = userInfo[@"fullname"];
    self.userName = userInfo[@"username"];
    self.email    = userInfo[@"email"];
    
    for (id object in userInfo[@"sports"]) {
        PlayerSport *sport = [[PlayerSport alloc] init];
        sport.sportName = object[@"sport_name"];
        sport.sportType = object[@"sport_type"];
        sport.level = object[@"level"];
        
        if ([object[@"sport_type"] isEqualToString:@"individual"]) {
            [self.playerSports addObject:sport];
        }
      //  else if ([object[@"sport_type"] isEqualToString:@"team"]) {
      //      [self.teamSports addObject:sport];
      //  }
        
    }
    NSLog(@"player sports: %lu", [self.playerSports count]);
//    NSLog(@"team sports: %d", [self.teamSports count]);
    
}

- (NSString *)location
{
    if ([_location isKindOfClass:[NSNull class]]) return @"No location specified";
         
    NSString *tempString = [_location stringByReplacingOccurrencesOfString:@" " withString:@""];

    if ([tempString length] == 0 || [_location isEqualToString:@"(null)"]) {
        return @"No location specified";
    }
    return _location;
}


- (NSString *)getLevelForSport:(NSUInteger)sportID
{
    NSString *levelString = @"";
    
    for (PlayerSport *sport in self.playerSports) {
        if (sport.sportID == sportID) {
            levelString = sport.level;
            break;
        }
    }
    return levelString;
}

// This pertains to all sports
- (NSInteger)computeTotalNumMatchesPlayed
{
    NSInteger totalPlayed = 0;

    for (PlayerSport *sport in self.playerSports) {
        totalPlayed += sport.playerRecordForSport.wins + sport.playerRecordForSport.losses + sport.playerRecordForSport.ties;
    }
    
    return  totalPlayed;
}

- (NSInteger)computeTotalNumMatchesPlayedForSport:(NSInteger)sportID
{
    NSInteger totalPlayed = 0;
    
    for (PlayerSport *sport in self.playerSports) {
        if (sport.sportID == sportID)
            totalPlayed = sport.playerRecordForSport.wins + sport.playerRecordForSport.losses + sport.playerRecordForSport.ties;
    }
    
    return  totalPlayed;
}
- (BOOL)isEqual:(id)object
{
    Player *inputtedPlayer = (Player *)object;
    if (inputtedPlayer.userID == self.userID)
        return TRUE;
    
    return FALSE;
}

- (NSUInteger)hash
{
    return self.userID;
}

+ (UIImage *)getIconForGender:(NSString *)genderString
{
    if ([genderString isEqualToString:@"male"]) {
        return [UIImage imageNamed:@"Gender Male Icon"];
    } else if ([genderString isEqualToString:@"female"]) {
        return [UIImage imageNamed:@"Gender Female Icon"];
    } else if ([genderString isEqualToString:@"coed"]) {
        return [UIImage imageNamed:@"Coed Icon"];
    }
    return  nil;

}
@end
