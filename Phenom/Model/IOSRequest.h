//
//  IOSRequest.h
//  Phenom
//
//  Created by James Chung on 3/18/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PickupMatch.h"
#import "IndividualMatch.h"
#import "Team.h"
#import "TeamMatch.h"
#import "EventTag.h"


typedef void(^RequestCompletionHandler)(NSString*,NSError*);
typedef void(^RequestDictionaryCompletionHandler)(NSDictionary*);
typedef void(^RequestImageCompletionHandler)(UIImage*);
typedef void(^RequestArrayCompletionHandler)(NSMutableArray*);
typedef void(^RequestDictionaryCompletionHandler2)(NSDictionary*, NSError*);


@interface IOSRequest : NSObject

+(void)requestPath:(NSString *)path
      onCompletion:(RequestCompletionHandler)complete;

+(void)loginWithUserName:(NSString *)userName
             andPassword:(NSString *)password
          andDeviceToken:(NSString *)deviceToken
            onCompletion:(RequestDictionaryCompletionHandler)complete;

+(void)verifyPhoneNumber:(NSString *)phoneNumber
                  ofUser:(NSUInteger)userID
            onCompletion:(RequestDictionaryCompletionHandler)complete;

+(void)verifyPin:(NSUInteger)pinCode
          ofUser:(NSUInteger)userID
    onCompletion:(RequestDictionaryCompletionHandler)complete;

+(void)checkUserVerified:(NSUInteger)userID
            onCompletion:(RequestDictionaryCompletionHandler)complete;

// Returns the userid
+(void)createOrUpdateFBUser:(NSNumber *)fbID
                 fbFullname:(NSString *)fbFullname
                    fbEmail:(NSString *)fbEmail
                 fbLocation:(NSString *)fbLocation
                      fbDOB:(NSString *)fbDOB
                   fbGender:(NSString *)fbGender
             withProfilePic:(NSString *)profilePic
             andDeviceToken:(NSString *)deviceToken
               onCompletion:(RequestDictionaryCompletionHandler)complete;

+(void)createUsername:(NSString *)username
             andEmail:(NSString *)email
          andPassword:(NSString *)password
          andlocation:(NSString *)location
            andRegion:(NSString *)region
              forUser:(NSUInteger)userID
         onCompletion:(RequestDictionaryCompletionHandler)complete;

+(void)createAccountWithUserName:(NSString *)userName
                     andPassword:(NSString *)password
                 andEmailAddress:(NSString *)email
                     andFullName:(NSString *)fullName
                     andLocation:(NSString *)location
                       andRegion:(NSString *)region
                  andDeviceToken:(NSString *)deviceToken
                    onCompletion:(RequestDictionaryCompletionHandler)complete;

+(void)fetchUserInfo:(NSInteger)userID
       bySessionUser:(NSInteger)myID
        onCompletion:(RequestDictionaryCompletionHandler)complete;

+(void)fetchUserInfo:(NSInteger)userID
       bySessionUser:(NSInteger)myID
         withPageNum:(NSInteger)pageNum
        andSpanCells:(NSInteger)spanCells
        onCompletion:(RequestDictionaryCompletionHandler)complete;

+(void)fetchPlayerMatchHistoryForProfilePage:(NSInteger)userID
                                  andPageNum:(NSInteger)pageNum
                                andSpanCells:(NSInteger)spanCells
                            andSessionUserID:(NSInteger)sessionUserID
                                onCompletion:(RequestDictionaryCompletionHandler)complete;


+(void)saveUserInfoWithUserID:(NSInteger)userID
                      andName:(NSString *)name
                  andUserName:(NSString *)userName
                     andEmail:(NSString *)email
                  andLocation:(NSString *)location
                 andAboutText:(NSString *)about
                 onCompletion:(RequestDictionaryCompletionHandler)complete;

+(void)savePhysicalStats:(NSInteger)userID
              withGender:(NSString *)gender
                 withDOB:(NSDate *)dob
          withHeightFeet:(NSInteger)ht_feet
        withHeightInches:(NSInteger)ht_inches
              withWeight:(NSInteger)weight
            onCompletion:(RequestDictionaryCompletionHandler)complete;

+(void)getPhysicalStatsForUser:(NSInteger)userID
                  onCompletion:(RequestDictionaryCompletionHandler)complete;

// fetches list of followers of userID and shows if session user is following each of the followers.
+(void)fetchUserFollowingList:(NSInteger)userID
                sessionUserID:(NSInteger)sessionUserID
                 onCompletion:(RequestArrayCompletionHandler)complete;

+(void)fetchUserFollowingList:(NSInteger)userID
                      bySport:(NSInteger)sportID
                 onCompletion:(RequestArrayCompletionHandler)complete;

+(void)fetchUserFollowersList:(NSInteger)userID
                sessionUserID:(NSInteger)sessionUserID
                 onCompletion:(RequestArrayCompletionHandler)complete;

/*
 +(void)fetchUserFollowingListForUser:(NSInteger)userID
 withSportName:(NSString *)sportName
 andSportType:(NSString *)sportType
 onCompletion:(RequestArrayCompletionHandler)complete;
 */

+(void)uploadImage:(UIImage *)imageToPost
        withUserID:(NSInteger)userId;
/*
 + (void)getPhoto:(NSInteger)type
 forID:(NSInteger)userID
 onCompletion:(RequestImageCompletionHandler)complete;
 
 + (NSString *)getPhotoURL:(NSInteger)type
 forID:(NSInteger)userID;
 
 */

+(void)savePicCredentials:(NSString *)fileName
                  forUser:(NSInteger)userID
             withFileType:(NSString *)fileType
             onCompletion:(RequestDictionaryCompletionHandler)complete;

+(void)savePicCredentials:(NSString *)fileName
                 forVenue:(NSUInteger)venueID
             withFileType:(NSString *)fileType
             onCompletion:(RequestDictionaryCompletionHandler)complete;

+ (void)fetchVenuePics:(NSInteger)venueID
          onCompletion:(RequestArrayCompletionHandler)complete;


+(void)getProfilePicCredentials:(NSInteger)userID
                   onCompletion:(RequestDictionaryCompletionHandler)complete;





+(void)makeFollowConnectionWithLeader:(NSInteger)leaderID
                          andFollower:(NSInteger)followerID
                         onCompletion:(RequestDictionaryCompletionHandler)complete;

+(void)removeFollowConnectionWithLeader:(NSInteger)leaderID
                            andFollower:(NSInteger)followerID
                           onCompletion:(RequestDictionaryCompletionHandler)complete;

+(void)blockUser:(NSInteger)userIDToBlock
         forUser:(NSInteger)userID
    onCompletion:(RequestDictionaryCompletionHandler)complete;


+(void)fetchProximityPlayers:(NSInteger)userID
                     andLong:(float)longitude
                      andLat:(float)latitude
                   withRange:(NSInteger)range
                onCompletion:(RequestArrayCompletionHandler)complete;

+(void)fetchProximityPlayers:(NSInteger)userID
                     andLong:(float)longitude
                      andLat:(float)latitude
                   withRange:(NSInteger)range
               filterBySport:(NSInteger)sportID
                     orderBy:(NSInteger)orderBy
                onCompletion:(RequestArrayCompletionHandler)complete;

+(void)fetchBrowsePlayers:(NSInteger)userID
             onCompletion:(RequestArrayCompletionHandler)complete;

+ (void)fetchFeaturedPlayerForSport:(NSInteger)sportID
                       onCompletion:(RequestDictionaryCompletionHandler)complete;

+(void)fetchLeaderboardforSport:(NSInteger)sportID
                       withUser:(NSInteger)userID
                   onCompletion:(RequestArrayCompletionHandler)complete;

+(void)fetchPlayerCoinsSummary:(NSInteger)userID
                  onCompletion:(RequestDictionaryCompletionHandler)complete;

+(void)whoCanChallengeUser:(NSInteger)sessionUserID
              onCompletion:(RequestDictionaryCompletionHandler)complete;

+(void)setWhoCanChallengeUser:(NSInteger)sessionUserID
                   playersAre:(NSString *)playersAre
                 onCompletion:(RequestDictionaryCompletionHandler)complete;

+(void)checkOKToChallengeUser:(NSInteger)userID
                bySessionUser:(NSInteger)sessionUserID
                 onCompletion:(RequestDictionaryCompletionHandler)complete;

+(void)fetchParticipatingUniversityList:(NSInteger)userID
                           onCompletion:(RequestArrayCompletionHandler)complete;

+(void)resetPasswordWithEntry:(NSString *)entry
                 onCompletion:(RequestDictionaryCompletionHandler)complete;

+(void)setNewPasswordWithOldPassword:(NSString *)oldPassword
                      andNewPassword:(NSString *)newPassword
                             forUser:(NSInteger)userID
                        onCompletion:(RequestDictionaryCompletionHandler)complete;

+(void)checkPasswordExistsForUser:(NSInteger)userID
                     onCompletion:(RequestDictionaryCompletionHandler)complete;

// Sports
+(void)fetchPlayerSports:(NSInteger)userID
            onCompletion:(RequestArrayCompletionHandler)complete;

+(void)fetchPlayerSportHistory:(NSInteger)userID
                      forSport:(NSString *)sportName
                  onCompletion:(RequestArrayCompletionHandler)complete;


+(void)fetchAllSports:(NSInteger)userID onCompletion:(RequestArrayCompletionHandler)complete;

+(void)fetchAllSportsV2:(NSInteger)userID onCompletion:(RequestDictionaryCompletionHandler)complete;

+(void)fetchAllLevelsonCompletion:(RequestArrayCompletionHandler)complete;

/*
 +(void)saveUserSportWithUser:(NSInteger)userID
 andSport:(NSString *)sportName
 andType:(NSString *)type
 andExperience:(NSString *)experience
 andLevel:(NSInteger)level
 onCompletion:(RequestDictionaryCompletionHandler)complete;
 */

/*
 +(void)fetchCommonSportsForUser:(NSInteger)sessionUserID
 andCompetitor:(NSInteger)competitorID
 onCompletion:(RequestArrayCompletionHandler)complete;
 */

+(void)fetchIndividualSportsList:(NSInteger)userID
                    onCompletion:(RequestArrayCompletionHandler)complete;

+(void)fetchCrossReferencedSportsListBetweenSessionUser:(NSInteger)sessionUserID
                                          andCompetitor:(NSInteger)competitorUserID
                                           onCompletion:(RequestArrayCompletionHandler)complete;

+(void)fetchPickupSportsList:(NSInteger)userID
                onCompletion:(RequestArrayCompletionHandler)complete;

+(void)fetchMyIndividualSportLevels:(NSInteger)userID
                       onCompletion:(RequestArrayCompletionHandler)complete;

+(void)createSportsForUser:(NSInteger)userID
                withSports:(NSString *)sportCodes
              onCompletion:(RequestDictionaryCompletionHandler)complete;

// Venues
+(void)saveVenueForUser:(NSInteger)userID
              venueName:(NSString *)venueName
                 street:(NSString *)street
                   city:(NSString *)city
                stateID:(NSString *)stateID
            description:(NSString *)description
              longitude:(float)longitude
               latitude:(float)latitude
             withSports:(NSString *)sportCodedString
              isPrivate:(BOOL)isPrivate
           onCompletion:(RequestDictionaryCompletionHandler)complete;

+(void)saveVenueWithCoordinatesForUser:(NSInteger)userID
                             longitude:(float)longitude
                              latitude:(float)latitude
                             venueName:(NSString *)venueName
                           description:(NSString *)description
                            withSports:(NSString *)sportCodedString
                             isPrivate:(BOOL)isPrivate
                          onCompletion:(RequestDictionaryCompletionHandler)complete;

+(void)saveTemporaryVenueForUser:(NSInteger)userID
                       venueName:(NSString *)venueName
                            city:(NSString *)city
                         stateID:(NSInteger)stateID
                     description:(NSString *)description
                      withSports:(NSString *)sportCodedString
                    onCompletion:(RequestDictionaryCompletionHandler)complete;

+(void)fetchUserVenuesFor:(NSInteger)useID
             onCompletion:(RequestArrayCompletionHandler)complete;

+(void)fetchVenueDetailForID:(NSInteger)venueID
             withSessionUser:(NSInteger)sessionUserID
                onCompletion:(RequestDictionaryCompletionHandler)complete;

+ (void)fetchVenuesNearMe:(NSInteger)userID
                withRange:(NSInteger)range
            userLongitude:(float)longitude
             userLatitude:(float)latitude
             onCompletion:(RequestArrayCompletionHandler)complete;

+ (void)changeFavoriteStatusForVenue:(NSInteger)venueID
                          withStatus:(BOOL)isFavorite
                            withUser:(NSInteger)userID
                        onCompletion:(RequestDictionaryCompletionHandler)complete;

+ (void)submitVenueRating:(NSInteger)ratingValue
              withComment:(NSString *)comment
                   byUser:(NSInteger)userID
                 forVenue:(NSInteger)venueID
             onCompletion:(RequestDictionaryCompletionHandler)complete;

+ (void)submitVenueFlag:(NSString *)flagType
            withComment:(NSString *)comment
                 byUser:(NSInteger)userID
               forVenue:(NSInteger)venueID
           onCompletion:(RequestDictionaryCompletionHandler)complete;

+(void)fetchRatingsForVenue:(NSInteger)venueID
               onCompletion:(RequestDictionaryCompletionHandler)complete;


+(void)fetchRatingForVenue:(NSInteger)venueID
                    byUser:(NSInteger)userID
              onCompletion:(RequestDictionaryCompletionHandler)complete;

+ (void)fetchAllContacts:(NSInteger)userID
            onCompletion:(RequestArrayCompletionHandler)complete;


+ (void)checkInUser:(NSInteger)userID
        atLongitude:(CGFloat)longitude
         atLatitude:(CGFloat)latitude
            atVenue:(NSInteger)venueID
         atDateTime:(NSDate *)checkInDateTime
              isNow:(NSInteger)isNow
          withSport:(NSInteger)sportID
         andMessage:(NSString *)message
          andNotify:(NSMutableArray *)notifyArray
     postToNewsfeed:(NSInteger)shouldPostToNewsfeed
       onCompletion:(RequestDictionaryCompletionHandler)complete;

+ (void)getVenueCheckIns:(NSInteger)venueID
         withSessionUser:(NSInteger)userID
   showFollowingListOnly:(BOOL)showFollowingListOnly
            onCompletion:(RequestArrayCompletionHandler)complete;

// Matches
+ (void)createPickupMatchWithInfo:(PickupMatch *)iMatch
                     onCompletion:(RequestDictionaryCompletionHandler)complete;

+(void)fetchPickupMatchesForUser:(NSInteger)userID onCompletion:(RequestArrayCompletionHandler)complete;

+ (void)rsvpForPickupMatch:(NSInteger)matchID
                  withRSVP:(NSInteger)rsvp
                andComment:(NSString *)comment
                   forUser:(NSInteger)userID
              onCompletion:(RequestDictionaryCompletionHandler)complete;

+(void)fetchPastPickupMatchesForUser:(NSInteger)userID onCompletion:(RequestArrayCompletionHandler)complete;


+ (void)createIndividualMatchWithInfo:(IndividualMatch *)iMatch
                         onCompletion:(RequestDictionaryCompletionHandler)complete;

+(void)fetchIndividualMatchesForUser:(NSInteger)userID onCompletion:(RequestArrayCompletionHandler)complete;

+ (void)rsvpForIndividualMatch:(NSInteger)matchID
                      withRSVP:(NSInteger)rsvp
                    andComment:(NSString *)comment
                       forUser:(NSInteger)userID
                  onCompletion:(RequestDictionaryCompletionHandler)complete;

+(void)fetchPastMatchesForUser:(NSInteger)userID onCompletion:(RequestDictionaryCompletionHandler)complete;


+(void)cancelMatchWithID:(NSInteger)matchID
                  byUser:(NSInteger)userID
                withType:(NSString *)matchType onCompletion:(RequestDictionaryCompletionHandler)complete;

+(void)confirmMatchWithID:(NSInteger)matchID
                   byUser:(NSInteger)userID
                 withType:(NSString *)matchType
             onCompletion:(RequestDictionaryCompletionHandler)complete;


+ (void)setScoreForIndividualMatch:(NSInteger)matchID
                  withWinningScore:(NSInteger)winnerScore
                    forWinningUser:(NSInteger)winnerUserID
                   withLosingScore:(NSInteger)loserScore
                     forLosingUser:(NSInteger)loserUserID
                         recoredBy:(NSInteger)recordedByID
                      onCompletion:(RequestDictionaryCompletionHandler)complete;

+ (void)setScoreForIndividualMatchWithMultipleRounds:(IndividualMatch *)individualMatch
                                          withScores:(NSDictionary *)scoreDictionary
                                         confirmedBy:(NSInteger)confirmerUserID
                                        onCompletion:(RequestDictionaryCompletionHandler)complete;

+ (void)confirmScoreForIndividualMatchWithMultipleRounds:(NSInteger)matchID
                                              forVersion:(NSInteger)version
                                             confirmedBy:(NSInteger)confirmerUserID
                                            onCompletion:(RequestDictionaryCompletionHandler)complete;

+ (void)disputeScoreForIndividualMatchWithMultipleRounds:(NSInteger)matchID
                                              disputedBy:(NSInteger)userID
                                            onCompletion:(RequestDictionaryCompletionHandler)complete;

+ (void)setMatchAsDisputed:(NSInteger)matchID
                disputedBy:(NSInteger)userID
              onCompletion:(RequestDictionaryCompletionHandler)complete;

/*
 + (void)confirmScoreForIndividualMatch:(NSInteger)matchID
 confirmedBy:(NSInteger)recordedByID
 onCompletion:(RequestDictionaryCompletionHandler)complete;
 */
/*
 + (void)disputeScoreForIndividualMatch:(NSInteger)matchID
 onCompletion:(RequestDictionaryCompletionHandler)complete;
 */

// start new matches ***
+ (void)fetchMatchNotifications:(NSInteger)userID
                   onCompletion:(RequestDictionaryCompletionHandler2)complete;

+ (void)fetchIndividualMatchDetails:(NSInteger)matchID
                        sessionUser:(NSInteger)userID
                       onCompletion:(RequestDictionaryCompletionHandler)complete;

+ (void)fetchPickupMatchDetails:(NSInteger)matchID
                   onCompletion:(RequestDictionaryCompletionHandler)complete;

+ (void)fetchChallengeMatchesOpenToMe:(NSInteger)userID
                         onCompletion:(RequestArrayCompletionHandler)complete;

+ (void)fetchPickupMatchesOpenToMe:(NSInteger)userID
                      onCompletion:(RequestArrayCompletionHandler)complete;

+ (void)fetchMyUpcomingMatches:(NSInteger)userID
                  onCompletion:(RequestDictionaryCompletionHandler)complete;

+ (void)fetchTodaysgMatches:(NSInteger)userID
               onCompletion:(RequestDictionaryCompletionHandler)complete;

+ (void)fetchPendingMatchesIJoined:(NSInteger)userID
                      onCompletion:(RequestDictionaryCompletionHandler)complete;

+ (void)fetchMatchesNeedingScoresOrConfirmations:(NSInteger)userID
                                    onCompletion:(RequestDictionaryCompletionHandler)complete;

+ (void)fetchMatchesNearMe:(NSInteger)userID
                 withRange:(NSInteger)range
             userLongitude:(float)longitude
              userLatitude:(float)latitude
              onCompletion:(RequestArrayCompletionHandler)complete;

+(void)getMatchTypesForSport:(NSInteger)sportID
                onCompletion:(RequestArrayCompletionHandler)complete;

+(void)getIndividualMatchComments:(NSUInteger)matchID onCompletion:(RequestArrayCompletionHandler)complete;

+(void)recordComment:(NSString *)comment
              byUser:(NSInteger)userID
            forMatch:(NSInteger)matchID
   withMatchCategory:(NSString *)matchCategory
        onCompletion:(RequestDictionaryCompletionHandler)complete;


+(void)setIndividualMatchAsNeverOccurred:(NSUInteger)matchID
                                  byUser:(NSUInteger)userID
                            onCompletion:(RequestDictionaryCompletionHandler)complete;

+(void)setPickupMatchAsNeverOccurred:(NSUInteger)matchID
                              byUser:(NSUInteger)userID
                        onCompletion:(RequestDictionaryCompletionHandler)complete;

+(void)doesAlertsPageNeedRefreshForUser:(NSUInteger)userID
                           onCompletion:(RequestDictionaryCompletionHandler)complete;

+(void)getMatchUpdatesForUser:(NSUInteger)userID
                 onCompletion:(RequestArrayCompletionHandler)complete;

+(void)getIndividualMatchHistory:(NSUInteger)matchID
                    onCompletion:(RequestArrayCompletionHandler)complete;

+(void)getPickupMatchHistory:(NSUInteger)matchID
                onCompletion:(RequestArrayCompletionHandler)complete;

+(void)getUpdatedLevelsForUser:(NSUInteger)userID
                      forMatch:(NSUInteger)matchID
                  onCompletion:(RequestDictionaryCompletionHandler)complete;

// User Ratings
+ (void)rateUser:(NSInteger)ratedUserID
          byUser:(NSInteger)raterUserID
      withRating:(NSString *)experienceRating
     withComment:(NSString *)comment
        forMatch:(NSInteger)matchID
    onCompletion:(RequestDictionaryCompletionHandler)complete;

+(void)getSportRatingsForUser:(NSUInteger)userID
                     forSport:(NSUInteger)sportID
                 onCompletion:(RequestArrayCompletionHandler)complete;
/*
 +(void)getSportRatingsForUser:(NSUInteger)forUserID
 byUser:(NSUInteger)byUserID
 withType:(NSUInteger)typeID
 onCompletion:(RequestArrayCompletionHandler)complete;
 */
+(void)getSportRatingsListForUser:(NSUInteger)ratedPlayerID
                sportRatingTypeID:(NSUInteger)srTypeID
                     onCompletion:(RequestArrayCompletionHandler)complete;

+(void)getSkillTypesForSport:(NSUInteger)sportID
             withSessionUser:(NSUInteger)sessionUserID
               andTargetUser:(NSUInteger)targetUserID
                onCompletion:(RequestArrayCompletionHandler)complete;

+(void)saveSportRatingForSkillAttribute:(NSUInteger)srTypeID
                        withSessionUser:(NSUInteger)sessionUserID
                          andTargetUser:(NSUInteger)targetUserID
                             withRating:(float)ratingValue
                             andComment:(NSString *)comment
                           onCompletion:(RequestDictionaryCompletionHandler)complete;

// Newseed
+(void)fetchPickupMatchesNewsfeeds:(NSInteger)userID onCompletion:(RequestArrayCompletionHandler)complete;
+(void)fetchIndividualMatchesNewsfeeds:(NSInteger)userID onCompletion:(RequestArrayCompletionHandler)complete;

+(void)recordComment:(NSString *)comment
              byUser:(NSInteger)userID
         forNewsPost:(NSInteger)newsID
        onCompletion:(RequestArrayCompletionHandler)complete;

+(void)fetchCommentsforNewsPost:(NSInteger)newsID
                   onCompletion:(RequestArrayCompletionHandler)complete;

+(void)saveProbabilityWithValue:(float)probabilityValue
             forIndividualMatch:(NSInteger)matchID
                   favoringUser:(NSInteger)favoredUserID
                         byUser:(NSInteger)ratingUserID
                   onCompletion:(RequestArrayCompletionHandler)complete;

+(void)getProbabilityforMatch:(NSInteger)matchID
                 onCompletion:(RequestArrayCompletionHandler)complete;

+(void)checkIfSessionUserVotedProbabiltyForMatch:(NSInteger)matchID
                                          byUser:(NSInteger)userID
                                    onCompletion:(RequestDictionaryCompletionHandler)complete;

+(void)fetchNewsfeeds:(NSInteger)userID
          withPageNum:(NSInteger)pageNum
              andSpan:(NSInteger)cellSpan
         onCompletion:(RequestArrayCompletionHandler)complete;

+(void)fetchBrowseNewsfeeds:(NSInteger)userID
                withPageNum:(NSInteger)pageNum
                    andSpan:(NSInteger)cellSpan
               onCompletion:(RequestArrayCompletionHandler)complete;

+(void)fetchNewsfeedDetail:(NSUInteger)newsfeedID
                    byUser:(NSInteger)userID onCompletion:(RequestArrayCompletionHandler)complete;


+(void)fetchProfileNewsfeed:(NSInteger)userID withSessionUser:(NSInteger)sessionUserID withPageNum:(NSInteger)pageNum andSpan:(NSInteger)cellSpan onCompletion:(RequestArrayCompletionHandler)complete;

+(void)makeRemixForNewsfeed:(NSUInteger)newsfeedID byUser:(NSInteger)userID onCompletion:(RequestDictionaryCompletionHandler)complete;

// for saving jpgs, videos from newsfeed post

+(void)saveNewsfeedTextPostWithFile:(NSString *)fileName
                            forUser:(NSInteger)userID
                       withFileType:(NSString *)fileType
                           withText:(NSString *)textPostString
                       onCompletion:(RequestDictionaryCompletionHandler)complete;

+(void)saveNewsfeedTextPost:(NSString *)textPostString
                    forUser:(NSInteger)userID
               onCompletion:(RequestDictionaryCompletionHandler)complete;

+ (void)likeNewsfeedPost:(NSInteger)newsfeedID
           sessionUserID:(NSInteger)sessionUserID
            onCompletion:(RequestDictionaryCompletionHandler)complete;

+ (void)UnLikeNewsfeedPost:(NSInteger)newsfeedID
             sessionUserID:(NSInteger)sessionUserID
              onCompletion:(RequestDictionaryCompletionHandler)complete;

+ (void)getPostLikes:(NSInteger)newsfeedID
        onCompletion:(RequestArrayCompletionHandler)complete;

+ (void)deletePost:(NSInteger)newsfeedID
            byUser:(NSInteger)userID
      onCompletion:(RequestDictionaryCompletionHandler)complete;

+ (void)flagPost:(NSInteger)newsfeedID
          byUser:(NSInteger)userID
    withFeedback:(NSString *)feedbackString
    onCompletion:(RequestDictionaryCompletionHandler)complete;


// Search
+(void)searchForUser:(NSString *)searchString notAssociatedWith:(NSInteger)userID onCompletion:(RequestArrayCompletionHandler)complete;

+(void)checkIfAppNeedsUpdateForPhoneVersion:(NSString *)versionOnPhone
                               onCompletion:(RequestDictionaryCompletionHandler)complete;

+ (void)checkFBFriendsAgainstVaidenDBForUser:(NSInteger)userID
                                 withFriends:(NSDictionary *)friendsDictionary
                                onCompletion:(RequestArrayCompletionHandler)complete;

+(void)getNumAlertsForUser:(NSInteger)userID
              onCompletion:(RequestDictionaryCompletionHandler)complete;

+(void)getNewMessagesForUser:(NSInteger)userID
                onCompletion:(RequestArrayCompletionHandler)complete;

+(void)getOpenMatchesForUser:(NSInteger)userID
                onCompletion:(RequestArrayCompletionHandler)complete;

+(void)getOpenMatchesForUser:(NSInteger)userID
                filterByUser:(NSInteger)filterUserID
                onCompletion:(RequestArrayCompletionHandler)complete;

/************************************ COMPETITIONS ************************************************************************************/

+(void)registerForCompetitionUser:(NSInteger)userID
                 withUniversityID:(NSInteger)universityID
                   andSchoolEmail:(NSString *)schoolEmail
            andSchoolOrganization:(NSString *)schoolOrganization
                     onCompletion:(RequestDictionaryCompletionHandler)complete;

+(void)checkIfRegisteredForCompetitionWithUser:(NSInteger)userID
                                  onCompletion:(RequestDictionaryCompletionHandler)complete;

+(void)getSchoolRankingsForUniversity:(NSInteger)universityID
                         onCompletion:(RequestArrayCompletionHandler)complete;

+(void)checkTeamName:(NSString *)teamName
        onCompletion:(RequestDictionaryCompletionHandler)complete;

+(void)createTeam:(Team *)nTeam
     onCompletion:(RequestDictionaryCompletionHandler)complete;

+(void)getSessionUserTeams:(NSInteger)userID
              onCompletion:(RequestArrayCompletionHandler)complete;

+(void)getTeamDetails:(NSInteger)teamID
         onCompletion:(RequestDictionaryCompletionHandler)complete;

+(void)joinTeam:(NSInteger)teamID
       withUser:(NSInteger)userID
   onCompletion:(RequestDictionaryCompletionHandler)complete;

+(void)leaveTeam:(NSInteger)teamID
        withUser:(NSInteger)userID
    onCompletion:(RequestDictionaryCompletionHandler)complete;

// must be captain to delete team
+(void)deleteTeam:(NSInteger)teamID
           byUser:(NSInteger)userID
     onCompletion:(RequestDictionaryCompletionHandler)complete;

+(void)getUninvitedContacts:(NSInteger)userID
                    forTeam:(NSInteger)teamID
               onCompletion:(RequestArrayCompletionHandler)complete;

+(void)setTeamMembersForTeam:(NSInteger)teamID
                 withMembers:(NSMutableArray *)memberArray
                  andCaptain:(NSInteger)captainID
                onCompletion:(RequestDictionaryCompletionHandler)complete;

// returns the teams that contacts (who session user follows) participates in
+(void)getContactsTeamsForUser:(NSInteger)userID
                  onCompletion:(RequestArrayCompletionHandler)complete;

// returns the teams list that session user is captain of
+(void)getSessionUserCaptainTeams:(NSInteger)userID
                     onCompletion:(RequestArrayCompletionHandler)complete;

+(void)getAllTeamSportsonCompletion:(RequestArrayCompletionHandler)complete;

+ (void)createTeamMatchWithInfo:(TeamMatch *)tMatch
                   onCompletion:(RequestDictionaryCompletionHandler)complete;

+ (void)fetchTeamMatchDetails:(NSInteger)matchID
                 onCompletion:(RequestDictionaryCompletionHandler)complete;

+ (void)fetchAlertsForUser:(NSInteger)userID
              onCompletion:(RequestDictionaryCompletionHandler)complete;


+ (void)fetchNumUnreadAlertsForUser:(NSInteger)userID
                       onCompletion:(RequestDictionaryCompletionHandler)complete;

+ (void)setAlertsAsReadForUser:(NSInteger)userID
                  onCompletion:(RequestDictionaryCompletionHandler)complete;

+ (void)approveFollowConnection:(NSInteger)sessionUserID
                     withLeader:(NSInteger)leaderID
                    andFollower:(NSInteger)followerID
                   onCompletion:(RequestDictionaryCompletionHandler)complete;

// Privacy
+ (void)setPrivacySettingFollowRequestRequiresApprovalForUser:(NSInteger)userID
                                              andApprovalFlag:(BOOL)approvalFlag
                                                 onCompletion:(RequestDictionaryCompletionHandler)complete;

+ (void)getPrivacySettingsForUser:(NSInteger)userID
                     onCompletion:(RequestDictionaryCompletionHandler)complete;

+ (void)getLockerTagsWithType:(NSInteger)tagType
                 onCompletion:(RequestArrayCompletionHandler)complete;

+ (void)getTrainingTagsForSport:(NSInteger)sportID
                   onCompletion:(RequestArrayCompletionHandler)complete;

+ (void)getStatTagsForSport:(NSInteger)sportID
               onCompletion:(RequestArrayCompletionHandler)complete;

+ (void)getVenuesWithSimilarNamesTo:(NSString *)venueNameFragment
                       onCompletion:(RequestArrayCompletionHandler)complete;

+(void)saveLockerPostWithImage:(NSString *)imageName
                     createdBy:(NSInteger)creatorID
                    withSeason:(NSString *)seasonTag
                       andYear:(NSString *)yearTag
                      andSport:(NSInteger)sportID
                      andVenue:(NSInteger)venueID
                  andVenueName:(NSString *)venueName
                andTagContacts:(NSMutableArray *)tagContacts
              andShareContacts:(NSMutableArray *)shareContacts
                    shareToAll:(BOOL)shareToAll
                  andStatsTags:(NSMutableArray *)statsTags
               andTrainingTags:(NSMutableArray *)trainingTags
                andUserCaption:(NSString *)userCaption
                   andEventTag:(EventTag *)event
                  onCompletion:(RequestDictionaryCompletionHandler)complete;

+ (void)fetchLockerAlbumCovers:(NSInteger)userID
              andSessionUserID:(NSInteger)sessionUserID
                   withPageNum:(NSInteger)pageNum
                       andSpan:(NSInteger)spanCells
                  onCompletion:(RequestDictionaryCompletionHandler)complete;

+ (void)fetchLockerPicsForSport:(NSInteger)sportID
                        andYear:(NSInteger)picsYear
                      andSeason:(NSString *)picsSeason
                        forUser:(NSInteger)userID
                 andSessionUser:(NSInteger)sessionUserID
                   onCompletion:(RequestArrayCompletionHandler)complete;

+ (void)fetchLockerPicDetail:(NSInteger)lockerID
               sessionUserID:(NSInteger)sessionUserID
                onCompletion:(RequestDictionaryCompletionHandler)complete;

+ (void)makeLockerPostAlbumCover:(NSInteger)lockerID
                        forSport:(NSInteger)sportID
                       andSeason:(NSString *)season
                         andYear:(NSInteger)year
                forsessionUserID:(NSInteger)sessionUserID
                    onCompletion:(RequestDictionaryCompletionHandler)complete;

+ (void)removeLockerUserTagForUser:(NSInteger)userID
                         andlocker:(NSInteger)lockerID
                      onCompletion:(RequestDictionaryCompletionHandler)complete;

+ (void)fetchUserEvents:(NSInteger)userID
           onCompletion:(RequestArrayCompletionHandler)complete;

+ (void)fetchSimilarEvents:(NSString *)stringFragment
                  forSport:(NSInteger)sportID
              onCompletion:(RequestArrayCompletionHandler)complete;

+ (void)getKickstarterInfo:(NSInteger)userID
              onCompletion:(RequestDictionaryCompletionHandler)complete;

/*
 *=================================New Function Added After Jun 26th==========================
 * @Author Xin
 *============================================================================================
 */

+ (void)checkAddressBookFriendsAgainstVaidenDBByName:(NSInteger)userID
                                 withFriends:(NSDictionary *)friendsDictionary
                                onCompletion:(RequestArrayCompletionHandler)complete;

+ (void)sendInviteEmailToFriends:(NSMutableArray *)friends
                           from:(NSInteger)userID
                   onCompletion:(RequestDictionaryCompletionHandler)complete;

+ (void)checkTwitterFriendsAgainstVaidenDBByName:(NSInteger)userID
                                     withFriends:(NSDictionary *)friendsDictionary
                                    onCompletion:(RequestArrayCompletionHandler)complete;

+ (void)saveReferralCode:(NSString *)code
                 ofUser:(NSUInteger)userID
           onCompletion:(RequestDictionaryCompletionHandler)complete;

+ (void)getReferralCodeForUSer:(NSUInteger)userID
            onCompletion:(RequestDictionaryCompletionHandler)complete;

+ (void)getTotalVoteForUser:(NSUInteger)userID
               onCompletion:(RequestDictionaryCompletionHandler)complete;

+ (void)voteFromUser:(NSUInteger)userID
              toUser:(NSUInteger)targetID
           withVotes:(NSInteger)votes
        onCompletion:(RequestDictionaryCompletionHandler)complete;

+ (void)thankFromUser:(NSUInteger)userID
               toUser:(NSUInteger)targetID
             forAlert:(NSUInteger)alertID
         onCompletion:(RequestDictionaryCompletionHandler)complete;

+ (void)getVotesInfoForUser:(NSUInteger)userID
               onCompletion:(RequestDictionaryCompletionHandler)complete;

+ (void)getTimeFromServerWithCompletion:(RequestDictionaryCompletionHandler)complete;

+ (void)getLocationForUser:(NSInteger)userID
              onCompletion:(RequestDictionaryCompletionHandler)complete;

+ (void)getRankingsInRegion:(NSString *)region
                   withUser:(NSInteger)userID
               onCompletion:(RequestDictionaryCompletionHandler)complete;

+ (void)loadReceivingVotesInRegionForUser:(NSInteger)userID
                             onCompletion:(RequestDictionaryCompletionHandler)complete;

+ (void)loadSupportersForUser:(NSInteger)userID
                     inRegion:(NSString *)region
                       forAll:(BOOL)flag
                 onCompletion:(RequestDictionaryCompletionHandler)complete;

+ (void)fetchSupportsForUser:(NSInteger)userID
                      forAll:(BOOL)flag
                onCompletion:(RequestDictionaryCompletionHandler)complete;

+ (void)getVoteDetailForUser:(NSInteger)userID
                onCompletion:(RequestDictionaryCompletionHandler)complete;

+ (void)saveExperenceForUser:(NSInteger)userID
                    withData:(NSDictionary *)data
                onCompletion:(RequestArrayCompletionHandler)complete;

+ (void)saveAwardForUser:(NSInteger)userID
                withData:(NSDictionary *)data
            onCompletion:(RequestArrayCompletionHandler)complete;

+ (void)fetchAwardForUser:(NSInteger)userID
                          onCompletion:(RequestDictionaryCompletionHandler)complete;

+ (void)fetchExperienceForUser:(NSInteger)userID
                  onCompletion:(RequestDictionaryCompletionHandler)complete;

+ (void)saveVotesForUser:(NSInteger)userID
               withVotes:(NSInteger)votes
                andPrice:(CGFloat)price
            onCompletion:(RequestDictionaryCompletionHandler)complete;

+ (void)saveBoostForUser:(NSInteger)userID
                withType:(NSInteger)type
                andPrice:(CGFloat)price
            onCompletion:(RequestDictionaryCompletionHandler)complete;

+ (void)getBoostStatusForUser:(NSInteger)userID
                 onCompletion:(RequestDictionaryCompletionHandler)complete;


+ (void)checkBoostedUserViewed:(NSInteger)userID
                  onCompletion:(RequestDictionaryCompletionHandler)complete;
/*
 *=================================New Function Added After July 24th==========================
 * @Add by Fangzhou
 *============================================================================================
 */

+(void)displayCharity:(NSInteger)sessionUserID
         onCompletion:(RequestDictionaryCompletionHandler)complete;
+(void)setCharity:(NSInteger)sessionUserID
          charity:(NSString *)charity
     onCompletion:(RequestDictionaryCompletionHandler)complete;

+ (void)setOptSettingsForUser:(NSInteger)userID
                                          andApprovalFlag:(BOOL)approvalFlag
                                             onCompletion:(RequestDictionaryCompletionHandler)complete;
+ (void)getOptSettingsForUser:(NSInteger)userID
                 onCompletion:(RequestDictionaryCompletionHandler)complete;
+ (void)getLoginNums:(NSInteger)userID
                 onCompletion:(RequestDictionaryCompletionHandler)complete;

@end
