//
//  Player.h
//  Phenom
//
//  Created by James Chung on 3/22/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PlayerSport.h"
#import "Venue.h"

@interface Player : NSObject

@property (nonatomic) NSInteger userID;
@property (strong, nonatomic) NSString *fullName;
@property (strong, nonatomic) NSString *userName;
@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSString *profileBaseString;

@property (strong, nonatomic) NSString *location;
@property (strong, nonatomic) NSString *about;

@property (strong, nonatomic) NSMutableArray *playerSports;

@property (strong, nonatomic) NSMutableArray *followingList;
@property (strong, nonatomic) NSMutableArray *followerList;

// adding these two properties because there times need these numbers and don't want to have to store large amounts of followers
// in the nsmutablearrays

@property (nonatomic) NSUInteger numFollowers;
@property (nonatomic) NSUInteger numFollowing;

@property (strong, nonatomic) NSMutableArray *favoriteVenues;

//@property (strong, nonatomic) NSString *generalWinLossRecord;

@property (nonatomic) NSInteger tag;
@property (nonatomic) BOOL isSelected;
@property (nonatomic) float coins;
@property (nonatomic) float percentProbabilitySessionUserCanBeatPlayer;
@property (nonatomic) BOOL canReceivePushNotifications;



- (Player *) initWithPlayerDetails:(NSInteger)userID
                     andProfileStr:(NSString *)profileBaseString
                       andUserName:(NSString *)userName
                       andFullName:(NSString *)fullName
                           andLocation:(NSString *)location
                          andAbout:(NSString *)about
                          andCoins:(float)coins;

- (Player *) initWithPlayerDetails:(NSInteger)userID
                       andUserName:(NSString *)userName;

- (Player *) initWithPlayerDetails:(NSInteger)userID
                       andUserName:(NSString *)userName
               andProfilePicString:(NSString *)profileBaseString;


- (void) addPlayerSport:(PlayerSport *)sport;
//- (void) addTeamSport:(Sport *)sport;
- (NSString *)getLevelForSport:(NSUInteger)sportID;
- (NSInteger)computeTotalNumMatchesPlayed;
- (NSInteger)computeTotalNumMatchesPlayedForSport:(NSInteger)sportID;
- (BOOL)isEqual:(id)object;
- (NSUInteger)hash;

+ (UIImage *)getIconForGender:(NSString *)genderString;

@end
