//
//  Match.h
//  Phenom
//
//  Created by James Chung on 4/3/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PlayerSport.h"
#import "Venue.h"
#import "Player.h"
#import "MatchHistoryObj.h"

@interface Match : NSObject

@property (nonatomic) NSInteger matchID;
@property (strong, nonatomic) NSString *matchName;
@property BOOL private;
@property (strong, nonatomic) Sport *sport;
@property (strong, nonatomic) NSString *message;
@property (strong, nonatomic) NSDate *dateTime;
@property (strong, nonatomic) Venue *venue;
@property (nonatomic) NSInteger matchCreatorID;
@property BOOL isActive;
@property (nonatomic, strong) NSString *matchStatus; // UNCONFIRMED / MANUAL_CONFIRMED / RSVP_CONFIRMED / CANCELLED / NEVER_OCCURRED
@property (nonatomic, strong) NSString *matchNotification; // this property should probabaly be placed into a subobject of Individual Match and Pickup Match
@property (nonatomic, strong) NSMutableArray *matchHistoryArray;



@property (nonatomic, strong) NSString *matchCreatorUsername;
@property (nonatomic, strong) NSString *matchCreatorProfileBaseString;


- (Match *) initMatchWithDetails:(NSInteger)matchID
                    andMatchName:(NSString *)matchName
                       isPrivate:(BOOL)isPrivate
                       withSport:(Sport *)sport
                     withMessage:(NSString *)message
                      onDateTime:(NSDate *)dateTime
                         atVenue:(Venue *)venue
                   createdByUser:(NSInteger)matchCreatorID
                       andActive:(BOOL)active
                  andMatchStatus:(NSString *)matchStatus;

// initializer with matchCreatorUsername and matchCreatorProfileBaseString
- (Match *) initMatchWithDetails:(NSInteger)matchID
                    andMatchName:(NSString *)matchName
                       isPrivate:(BOOL)isPrivate
                       withSport:(Sport *)sport
                     withMessage:(NSString *)message
                      onDateTime:(NSDate *)dateTime
                         atVenue:(Venue *)venue
                   createdByUser:(NSInteger)matchCreatorID
                    withUsername:(NSString *)matchCreatorUsername
                   andProfileStr:(NSString *)matchCreatorProfileBaseString
                       andActive:(BOOL)active
                  andMatchStatus:(NSString *)matchStatus;

+ (NSInteger)getIDOfOpponent:(NSArray *)competitors sessionUserID:(NSInteger)sessionUserID;

+ (NSString *)getUsernameForID:(NSInteger)userID inArray:(NSMutableArray *)competitors;

- (BOOL)isMatchConfirmed;
- (BOOL)isMatchCancelled;
- (void)setMatchHistoryWithDataObj:(id)dataObj;
- (UIImage *)getMatchDetailPageBackground;

@end
