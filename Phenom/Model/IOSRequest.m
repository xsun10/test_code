//
//  IOSRequest.m
//  Phenom
//
//  Created by James Chung on 3/18/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "IOSRequest.h"
#import "NSString+WebService.h"
#import "AFNetworking.h"
#import "AFImageRequestOperation.h"
#import "PickupMatch.h"
#import "IndividualMatch.h"
#import "NSDate+Utilities.h"
#import "TrainingTag.h"
#import "StatTag.h"

@implementation IOSRequest

//#define BASE_URL @"https://www.vaiden-mobile.com"
#define BASE_URL @"http://23.23.219.142"
//#define NUM_RETRIES 3
#define NUM_RETRIES_COMMUNICATE 3
#define NUM_RETRIES_UPLOAD 1

+(void)requestPath:(NSString *)path onCompletion:(RequestCompletionHandler)complete
{
    // Background Queue
    NSOperationQueue *backgroundQueue = [[NSOperationQueue alloc] init];
    
    // URL Request
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:path]
                                                  cachePolicy:NSURLCacheStorageAllowedInMemoryOnly
                                              timeoutInterval:10];
    
    // Send Request
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:backgroundQueue
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
                               [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                               NSString *result = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                               if (complete) complete(result,error);
                           }];
    
    
}
/*
 + (void)requestPathAF:(NSString *)basePath withDetailPath:(NSString *)detailPath onCompletion:(RequestCompletionHandler)complete
 {
 [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
 // 1
 NSURL *baseURL = [NSURL URLWithString:basePath];
 NSDictionary *parameters = @{@"format": @"json"};
 
 // 2
 AFHTTPClient *client = [[AFHTTPClient alloc] initWithBaseURL:baseURL];
 [client registerHTTPOperationClass:[AFJSONRequestOperation class]];
 [client setDefaultHeader:@"Accept" value:@"application/json"];
 [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
 
 // 3
 [client getPath:detailPath
 parameters:parameters
 success:^(AFHTTPRequestOperation *operation, id responseObject) {
 [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
 if (complete) complete(responseObject, nil);
 }
 failure:^(AFHTTPRequestOperation *operation, NSError *error) {
 //   UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Error Retrieving Weather"
 //                                               message:[NSString stringWithFormat:@"%@",error]
 //                                              delegate:nil
 //                                      cancelButtonTitle:@"OK" otherButtonTitles:nil];
 //   [av show];
 NSLog (@"Error retrieving info %@", [NSString stringWithFormat:@"%@", error]);
 //     [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
 
 
 
 }
 ];
 
 
 }*/

+ (void)requestPathAF:(NSString *)basePath numTimes:(NSUInteger)ntimes withDetailPath:(NSString *)detailPath onCompletion:(RequestCompletionHandler)complete
{
    if (ntimes == 2)
        NSLog(@"");
    
    if (ntimes <= 0) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Connectivity Error"
                                                     message:@"There seems to be issues connecting to the server.  Please check your connection and try again."
                                                    delegate:nil
                                           cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [av show];
        return;
        
    } else {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        // 1
        NSURL *baseURL = [NSURL URLWithString:basePath];
        NSDictionary *parameters = @{@"format": @"json"};
        
        NSLog(@"%@", detailPath);
        
        
        // 2
        AFHTTPClient *client = [[AFHTTPClient alloc] initWithBaseURL:baseURL];
        [client registerHTTPOperationClass:[AFJSONRequestOperation class]];
        [client setDefaultHeader:@"Accept" value:@"application/json"];
        
        [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
        
        // 3
        [client getPath:detailPath
             parameters:parameters
                success:^(AFHTTPRequestOperation *operation, id responseObject) {
                    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                    if (complete) {
                        complete(responseObject, nil);
                        return;
                    }
                }
                failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    NSLog (@"Error retrieving info %@", [NSString stringWithFormat:@"%@", error]);
                    //     [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                    
                    [self requestPathAF:basePath
                               numTimes:ntimes-1
                         withDetailPath:detailPath
                           onCompletion:complete];
                    
                    
                }
         ];
        
    }
}

+ (void)requestPathAF2:(NSString *)basePath numTimes:(NSUInteger)ntimes withDetailPath:(NSString *)detailPath withParameters:(NSMutableDictionary *)parameters
          onCompletion:(RequestCompletionHandler)complete
{
    //    [parameters setValue:@"json" forKey:@"format"];
    if (ntimes == 2)
        NSLog(@"");
    
    if (ntimes <= 0) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Connectivity Error"
                                                     message:@"There seems to be issues connecting to the server.  Please check your connection and try again."
                                                    delegate:nil
                                           cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [av show];
        return;
        
    } else {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        // 1
        
        NSLog(@"%@/%@", BASE_URL, detailPath);
        
        AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:basePath]];
        [httpClient setParameterEncoding:AFJSONParameterEncoding];
        NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                                path:detailPath
                                                          parameters:parameters];
        
        AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request
                                                                                            success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                                                                // Print the response body in text
                                                                                                //          NSLog(@"Response: %@", [[NSString alloc] initWithData:JSON encoding:NSUTF8StringEncoding]);
                                                                                                
                                                                                                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                                                                                                if (complete) {
                                                                                                    complete(JSON, nil);
                                                                                                    return;
                                                                                                }
                                                                                                
                                                                                            } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                                                                NSLog(@"Error: %@", error);
                                                                                                [self requestPathAF2:basePath
                                                                                                            numTimes:ntimes-1
                                                                                                      withDetailPath:detailPath
                                                                                                      withParameters:parameters
                                                                                                        onCompletion:complete];
                                                                                                
                                                                                            }];
        
        
        
        
        
        [operation start];
        
        
        
        /*
         
         // 2
         AFHTTPClient *client = [[AFHTTPClient alloc] initWithBaseURL:baseURL];
         [client registerHTTPOperationClass:[AFJSONRequestOperation class]];
         [client setDefaultHeader:@"Accept" value:@"application/json"];
         
         [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];
         
         // 3
         [client getPath:detailPath
         parameters:parameters
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
         [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
         if (complete) {
         complete(responseObject, nil);
         return;
         }
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog (@"Error retrieving info %@", [NSString stringWithFormat:@"%@", error]);
         //     [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
         
         [self requestPathAF:basePath
         numTimes:ntimes-1
         withDetailPath:detailPath
         onCompletion:complete];
         
         
         }
         ];*/
        
    }
}


/*
 - (void)requestPath:(AFHTTPClient*)client
 withDetailPath:(NSString *)detailPath
 withParams:(NSDictionary *)paramenters
 numTimes:(NSUInteger)ntimes
 withCompletionHandler:(RequestCompletionHandler)complete
 success:(void (^)(id responseObject))success
 failure:(void (^)(NSError *error))failure
 {
 if (ntimes <= 0) {
 if (failure) {
 //            NSError *error = ...;
 //            failure(error);
 }
 } else {
 [client getPath:detailPath parameters:paramenters success:^(AFHTTPRequestOperation *operation, id responseObject) {
 if (success) {
 //   success(...);
 }
 } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
 [self requestPath:client withDetailPath:detailPath
 withParams:paramenters
 numTimes:ntimes-1
 withCompletionHandler:complete
 success:^(id responseObject) {
 [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
 if (complete) complete(responseObject, nil);
 }
 failure:^(NSError *error) {
 
 }];
 }];
 }
 }*/

+(void)loginWithUserName:(NSString *)userName
             andPassword:(NSString *)password
          andDeviceToken:(NSString *)deviceToken
            onCompletion:(RequestDictionaryCompletionHandler)complete
{
    // Encode ARGS for URL
    userName = [userName URLEncode];
    password = [[password MD5] URLEncode];
    deviceToken = [deviceToken URLEncode];
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/players/logmein.php?user_name=%@&password=%@&device_token=%@",userName,password, deviceToken];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
    
}

+(void)verifyPhoneNumber:(NSString *)phoneNumber
                  ofUser:(NSUInteger)userID
            onCompletion:(RequestDictionaryCompletionHandler)complete
{
    
    NSString *basePath = BASE_URL;
    NSString *detailPath = [NSString stringWithFormat:@"sc/twilio-php/send_verification_code.php?phone_number=%@&user_id=%lu",phoneNumber, userID];
    
    
    [IOSRequest requestPathAF:basePath numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
    
}

+(void)verifyPin:(NSUInteger)pinCode
          ofUser:(NSUInteger)userID
    onCompletion:(RequestDictionaryCompletionHandler)complete
{
    
    NSString *basePath = BASE_URL;
    NSString *detailPath = [NSString stringWithFormat:@"sc/twilio-php/verify_pin.php?pin_code=%lu&user_id=%lu",pinCode, userID];
    
    
    [IOSRequest requestPathAF:basePath numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
    
}

+(void)checkUserVerified:(NSUInteger)userID
            onCompletion:(RequestDictionaryCompletionHandler)complete
{
    
    NSString *basePath = BASE_URL;
    NSString *detailPath = [NSString stringWithFormat:@"sc/twilio-php/check_user_verified.php?user_id=%lu", userID];
    
    
    [IOSRequest requestPathAF:basePath numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
    
}

+(void)createOrUpdateFBUser:(NSNumber *)fbID
                 fbFullname:(NSString *)fbFullname
                    fbEmail:(NSString *)fbEmail
                 fbLocation:(NSString *)fbLocation
                      fbDOB:(NSString *)fbDOB
                   fbGender:(NSString *)fbGender
             withProfilePic:(NSString *)profilePic
             andDeviceToken:(NSString *)deviceToken
               onCompletion:(RequestDictionaryCompletionHandler)complete
{
    
    fbFullname = [fbFullname URLEncode];
    fbEmail = [fbEmail URLEncode];
    fbLocation = [fbLocation URLEncode];
    //   fbDOB = [fbDOB URLEncode];
    fbGender = [fbGender URLEncode];
    profilePic = [profilePic URLEncode];
    
    //   NSString *basePath = BASE_URL;
    NSString *detailPath = [NSString stringWithFormat:@"sc/players/create_or_update_facebook_user.php?fb_id=%llu&fb_fullname=%@&fb_email=%@&fb_location=%@&fb_dob=%@&fb_gender=%@&profile_pic=%@&device_token=%@", fbID.unsignedLongLongValue, fbFullname, fbEmail, fbLocation, fbDOB, fbGender, profilePic, deviceToken];
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
    
}
+(void)createUsername:(NSString *)username
             andEmail:(NSString *)email
          andPassword:(NSString *)password
          andlocation:(NSString *)location
            andRegion:(NSString *)region
              forUser:(NSUInteger)userID
         onCompletion:(RequestDictionaryCompletionHandler)complete
{
    
    username = [username URLEncode];
    email = [email URLEncode];
    location = [location URLEncode];
    region = [region URLEncode];
    password = [[password MD5] URLEncode];
    
    //   NSString *basePath = BASE_URL;
    NSString *detailPath = [NSString stringWithFormat:@"sc/players/create_username.php?username=%@&user_id=%lu&email=%@&password=%@&location=%@&region=%@", username, (unsigned long)userID, email, password, location, region];
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_UPLOAD withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
    
}




+(void)createAccountWithUserName:(NSString *)userName
                     andPassword:(NSString *)password
                 andEmailAddress:(NSString *)email
                     andFullName:(NSString *)fullName
                     andLocation:(NSString *)location
                       andRegion:(NSString *)region
                  andDeviceToken:(NSString *)deviceToken
                    onCompletion:(RequestDictionaryCompletionHandler)complete
{
    // Encode ARGS for URL
    userName = [userName URLEncode];
    password = [[password MD5] URLEncode];
    email = [email URLEncode];
    deviceToken = [deviceToken URLEncode];
    location = [location URLEncode];
    fullName = [fullName URLEncode];
    region = [region URLEncode];
    
    
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/players/create_account.php?user_name=%@&password=%@&email=%@&device_token=%@&location=%@&fullname=%@&region=%@", userName, password, email, deviceToken, location, fullName, region];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_UPLOAD withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
    
}


+(void)fetchUserInfo:(NSInteger)userID
       bySessionUser:(NSInteger)myID
        onCompletion:(RequestDictionaryCompletionHandler)complete
{
    // Encode ARGS for URL
    NSString *userIDStringConversion = [NSString stringWithFormat:@"%ld", userID];
    userIDStringConversion = [userIDStringConversion URLEncode];
    
    NSString *baseURL = [NSString stringWithFormat:@"%@/", BASE_URL];
    NSString *detailPath = [NSString stringWithFormat:@"%@sc/players/fetchuserinfo2_no_newsfeed.php/", baseURL];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:userIDStringConversion forKey:@"user_id"];
    [parameters setValue:[NSString stringWithFormat:@"%ld", myID] forKey:@"my_id"];
    
    [IOSRequest requestPathAF2:baseURL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath withParameters:parameters onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
    
}

+(void)fetchUserInfo:(NSInteger)userID
       bySessionUser:(NSInteger)myID
         withPageNum:(NSInteger)pageNum
        andSpanCells:(NSInteger)spanCells
        onCompletion:(RequestDictionaryCompletionHandler)complete
{
    // Encode ARGS for URL
    NSString *userIDStringConversion = [NSString stringWithFormat:@"%ld", userID];
    userIDStringConversion = [userIDStringConversion URLEncode];
    
    NSString *baseURL = [NSString stringWithFormat:@"%@/", BASE_URL];
    NSString *detailPath = [NSString stringWithFormat:@"%@sc/players/fetchuserinfo2.php/", baseURL];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:userIDStringConversion forKey:@"user_id"];
    [parameters setValue:[NSString stringWithFormat:@"%ld", myID] forKey:@"my_id"];
    [parameters setValue:[NSString stringWithFormat:@"%ld", pageNum] forKey:@"page_num"];
    [parameters setValue:[NSString stringWithFormat:@"%ld", spanCells] forKey:@"span_cells"];
    
    [IOSRequest requestPathAF2:baseURL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath withParameters:parameters onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
    
}

+(void)fetchPlayerMatchHistoryForProfilePage:(NSInteger)userID
                                  andPageNum:(NSInteger)pageNum
                                andSpanCells:(NSInteger)spanCells
                            andSessionUserID:(NSInteger)sessionUserID

                    onCompletion:(RequestDictionaryCompletionHandler)complete
{
    
    
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/players/get_match_history_feed.php?user_id=%ld&page_num=%ld&span_cells=%ld&session_user_id=%ld", userID, pageNum, spanCells, sessionUserID];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_UPLOAD withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
    
}


+(void)saveUserInfoWithUserID:(NSInteger)userID
                      andName:(NSString *)name
                  andUserName:(NSString *)userName
                     andEmail:(NSString *)email
                  andLocation:(NSString *)location
                 andAboutText:(NSString *)about
                 onCompletion:(RequestDictionaryCompletionHandler)complete
{
    // Encode ARGS for URL
    NSString *userIDStringConversion = [NSString stringWithFormat:@"%ld", userID];
    NSLog(@"fetchUserInfo in IOSRequest.m userID in string: %@", userIDStringConversion);
    userIDStringConversion = [userIDStringConversion URLEncode];
    
    name     = [name URLEncode];
    userName = [userName URLEncode];
    email    = [email URLEncode];
    location     = [location URLEncode];
    about = [about URLEncode];
    /*
     NSString *basePath = [NSString stringWithFormat:@"%@/sc/saveuserinfo.php?", BASE_URL];
     NSString *fullPath = [basePath stringByAppendingFormat:@"user_id=%@&name=%@&user_name=%@&email=%@&location=%@&about=%@",userIDStringConversion, name, userName, email, location, about];
     
     NSLog(@"FullPath: %@", fullPath);
     [IOSRequest requestPath:fullPath onCompletion:^(NSString *result, NSError *error){
     if (error || [result isEqualToString:@""]) {
     if (complete) complete(nil);
     } else {
     NSDictionary *user = [result JSON];
     if (complete) complete(user);
     }
     }];
     */
    NSString *detailPath = [NSString stringWithFormat:@"sc/saveuserinfo.php?user_id=%@&name=%@&user_name=%@&email=%@&location=%@&about=%@",userIDStringConversion, name, userName, email, location, about];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
    
}


+(void)savePhysicalStats:(NSInteger)userID
              withGender:(NSString *)gender
                 withDOB:(NSDate *)dob
          withHeightFeet:(NSInteger)ht_feet
        withHeightInches:(NSInteger)ht_inches
              withWeight:(NSInteger)weight
            onCompletion:(RequestDictionaryCompletionHandler)complete
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd";
    NSLog(@"%@", [formatter stringFromDate:dob]);
    
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/settings/save_physical_stats.php?user_id=%ld&gender=%@&ht_feet=%ld&ht_inches=%ld&dob=%@&weight=%ld",userID, gender, ht_feet, ht_inches, [formatter stringFromDate:dob], weight];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *resultArray = (NSDictionary *)result;
            if (complete) complete(resultArray);
        }
    }];
    
}

+(void)getPhysicalStatsForUser:(NSInteger)userID
                  onCompletion:(RequestDictionaryCompletionHandler)complete
{
    NSString *detailPath = [NSString stringWithFormat:@"sc/settings/fetch_physical_stats.php?user_id=%ld",userID];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableDictionary *resultDictionary = (NSMutableDictionary *)result;
            if (complete) complete(resultDictionary);
        }
    }];
    
}

#define DataDownloaderRunMode @"myapp.run_mode"

// gets the list of contacts that a user follows

+(void)fetchUserFollowingList:(NSInteger)userID
                sessionUserID:(NSInteger)sessionUserID
                 onCompletion:(RequestArrayCompletionHandler)complete
{
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/players/get_following.php?user_id=%ld&session_user_id=%ld",userID, sessionUserID];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *resultArray = (NSMutableArray *)result;
            if (complete) complete(resultArray);
        }
    }];
    
    
    
}

+(void)fetchUserFollowingList:(NSInteger)userID
                      bySport:(NSInteger)sportID
                 onCompletion:(RequestArrayCompletionHandler)complete
{
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/players/get_following_by_sport.php?user_id=%ld&sport_id=%ld",userID,sportID];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *resultArray = (NSMutableArray *)result;
            if (complete) complete(resultArray);
        }
    }];
    
    
    
}

// Gets the list of followers for a given user.  The session user id is also passed to this function in the case
// where the session user might want to know whether they follow the users in the list.

+(void)fetchUserFollowersList:(NSInteger)userID
                sessionUserID:(NSInteger)sessionUserID
                 onCompletion:(RequestArrayCompletionHandler)complete
{
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/players/get_followers.php?user_id=%ld&session_user_id=%ld",userID, sessionUserID];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *resultArray = (NSMutableArray *)result;
            if (complete) complete(resultArray);
        }
    }];
    
    
    
}


// right now just for profile image. this function will create small icon sizes via the php script on the server.
+ (void)uploadImage:(UIImage *)image
         withUserID:(NSInteger) userID
{
    //This loads the image into a NSData variable we'll use to send the message with
    NSData *imgData = UIImageJPEGRepresentation(image, 1);
    
    NSString *imgFileName = [[NSString alloc] initWithFormat:@"%ld.jpg", userID];
    
    NSString *bp = [NSString stringWithFormat:@"%@/", BASE_URL];
    AFHTTPClient *client= [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:bp]];;
    
    NSMutableURLRequest *myRequest = [client multipartFormRequestWithMethod:@"POST" path:@"sc/up.php"
                                                                 parameters:nil constructingBodyWithBlock: ^(id <AFMultipartFormData>formData) {
                                                                     [formData appendPartWithFileData:imgData name:@"uploadedfile" fileName:imgFileName mimeType:@"images/jpeg"];
                                                                 }];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:myRequest];
    //    [operation setUploadProgressBlock:^(NSInteger bytesWritten, NSInteger totalBytesWritten, NSInteger totalBytesExpectedToWrite) {
    //
    //       NSLog(@"Sent %d of %d bytes", totalBytesWritten, totalBytesExpectedToWrite);
    
    //	}];
    
	[operation setCompletionBlock:^{
        //		NSLog(@"%@", operation.responseString); //Lets us know the result including failures
	}];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [queue addOperation:operation];
}

/*
 + (void)getPhoto:(NSInteger)type
 forID:(NSInteger)userID
 onCompletion:(RequestImageCompletionHandler)complete
 
 {
 
 NSString *urlString = @"";
 NSString *retinaString;
 
 // Check for retina screen.  If true then have to get bigger image (denoted by @2x")
 if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]
 && [[UIScreen mainScreen] scale] == 2.0) {
 // Retina
 retinaString = @"@2x";
 } else {
 // Not Retina
 retinaString = @"";
 }
 
 
 switch(type) {
 case 1: // profile pic 70px
 urlString = [NSString stringWithFormat:@"%@%d%@%@%@",@"http://54.225.204.45/sc/images/profile/",  userID, @"_70_", retinaString, @".jpg"];
 break;
 case 2: // profile pic 35px
 urlString = [NSString stringWithFormat:@"%@%d%@%@%@",@"http://54.225.204.45/sc/images/profile/",  userID, @"_40_", retinaString, @".jpg"];
 break;
 default:
 urlString = @"";
 
 }
 
 
 NSLog(@"Getting Profile Pic at: %@", urlString);
 // download the photo
 NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
 
 AFImageRequestOperation *operation = [AFImageRequestOperation
 imageRequestOperationWithRequest:request
 imageProcessingBlock: nil
 success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
 
 complete(image);
 
 // notify the table view to reload the table
 //                                             [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadTableView" object:nil];
 }
 failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
 NSLog(@"Error getting photo");
 complete(nil);
 }];
 
 
 NSOperationQueue *queue = [[NSOperationQueue alloc] init];
 [queue addOperation:operation];
 }
 */


+(void)savePicCredentials:(NSString *)fileName
                  forUser:(NSInteger)userID
             withFileType:(NSString *)fileType
             onCompletion:(RequestDictionaryCompletionHandler)complete
{
    
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/s3image/saveimage.php?filename=%@&user_id=%ld&filetype=%@",[fileName URLEncode], userID, fileType];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_UPLOAD withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
    
}

// for venue pics
+(void)savePicCredentials:(NSString *)fileName
                 forVenue:(NSUInteger)venueID
             withFileType:(NSString *)fileType
             onCompletion:(RequestDictionaryCompletionHandler)complete
{
    
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/s3image/saveimage_venue.php?filename=%@&venue_id=%ld&filetype=%@",[fileName URLEncode],venueID, fileType];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_UPLOAD withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
    
    
}


+ (void)fetchVenuePics:(NSInteger)venueID
          onCompletion:(RequestArrayCompletionHandler)complete
{
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/venues/fetch_venue_pics.php?venue_id=%ld",venueID];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *results = (NSMutableArray *)result;
            if (complete) complete(results);
        }
    }];
    
}

+(void)getProfilePicCredentials:(NSInteger)userID
                   onCompletion:(RequestDictionaryCompletionHandler)complete
{
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/s3image/get_profile_pic.php?user_id=%ld", userID];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
    
}





+(void)makeFollowConnectionWithLeader:(NSInteger)leaderID
                          andFollower:(NSInteger)followerID
                         onCompletion:(RequestDictionaryCompletionHandler)complete
{
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/players/make_follow_connection.php?leader_id=%ld&follower_id=%ld",leaderID, followerID];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
    
}

+(void)removeFollowConnectionWithLeader:(NSInteger)leaderID
                            andFollower:(NSInteger)followerID
                           onCompletion:(RequestDictionaryCompletionHandler)complete
{
    
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/players/remove_follow_connection.php?leader_id=%ld&follower_id=%ld",leaderID, followerID];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
    
}

+(void)blockUser:(NSInteger)userIDToBlock
         forUser:(NSInteger)userID
    onCompletion:(RequestDictionaryCompletionHandler)complete
{
    NSString *detailPath = [NSString stringWithFormat:@"sc/players/block_user.php?user_id=%ld&user_id_to_block=%ld",userID, userIDToBlock];
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
    
}

//block_user.php
+(void)fetchProximityPlayers:(NSInteger)userID
                     andLong:(float)longitude
                      andLat:(float)latitude
                   withRange:(NSInteger)range
                onCompletion:(RequestArrayCompletionHandler)complete
{
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/players/get_proximity_players.php?user_id=%ld&long=%f&lat=%f&range=%ld",userID, longitude, latitude, range];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *resultArray = (NSMutableArray *)result;
            if (complete) complete(resultArray);
        }
    }];
    
}


+(void)fetchProximityPlayers:(NSInteger)userID
                     andLong:(float)longitude
                      andLat:(float)latitude
                   withRange:(NSInteger)range
               filterBySport:(NSInteger)sportID
                     orderBy:(NSInteger)orderBy
                onCompletion:(RequestArrayCompletionHandler)complete
{
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/players/get_proximity_players_by_sport.php?user_id=%ld&long=%f&lat=%f&range=%ld&sport_id=%ld&order_by=%ld",userID, longitude, latitude, range, sportID, orderBy];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *resultArray = (NSMutableArray *)result;
            if (complete) complete(resultArray);
        }
    }];
    
}

+(void)fetchBrowsePlayers:(NSInteger)userID
             onCompletion:(RequestArrayCompletionHandler)complete
{
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/players/get_browse_players.php?user_id=%ld",userID];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *resultArray = (NSMutableArray *)result;
            if (complete) complete(resultArray);
        }
    }];
    
}

+ (void)fetchFeaturedPlayerForSport:(NSInteger)sportID
                       onCompletion:(RequestDictionaryCompletionHandler)complete
{
    NSString *detailPath = [NSString stringWithFormat:@"sc/players/get_featured_player.php?sport_id=%ld",sportID];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *resultArray = (NSDictionary *)result;
            if (complete) complete(resultArray);
        }
    }];
}

+(void)fetchLeaderboardforSport:(NSInteger)sportID
                       withUser:(NSInteger)userID
                   onCompletion:(RequestArrayCompletionHandler)complete
{
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/players/get_sport_leaderboard.php?&sport_id=%ld&user_id=%ld",sportID, userID];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *resultArray = (NSMutableArray *)result;
            if (complete) complete(resultArray);
        }
    }];
    
}


+(void)fetchPlayerCoinsSummary:(NSInteger)userID
                  onCompletion:(RequestDictionaryCompletionHandler)complete
{
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/players/get_player_coins_summary.php?user_id=%ld", userID];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
    
}

// Can be used to figure out who can challenge session user or any other user in Vaiden App
+(void)whoCanChallengeUser:(NSInteger)sessionUserID
              onCompletion:(RequestDictionaryCompletionHandler)complete
{
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/players/who_can_challenge_user.php?user_id=%ld", sessionUserID];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableDictionary *resultArray = (NSMutableDictionary *)result;
            if (complete) complete(resultArray);
        }
    }];
    
}

// "playersAre" is either "following" or "anyone" - these are the players that can challenge the session user
+(void)setWhoCanChallengeUser:(NSInteger)sessionUserID
                   playersAre:(NSString *)playersAre
                 onCompletion:(RequestDictionaryCompletionHandler)complete
{
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/players/set_who_can_challenge_user.php?user_id=%ld&players_are=%@", sessionUserID, playersAre];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableDictionary *resultArray = (NSMutableDictionary *)result;
            if (complete) complete(resultArray);
        }
    }];
    
}
+(void)checkOKToChallengeUser:(NSInteger)userID
                bySessionUser:(NSInteger)sessionUserID
                 onCompletion:(RequestDictionaryCompletionHandler)complete
{
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/players/check_ok_to_challenge.php?user_id=%ld&session_user_id=%ld", userID, sessionUserID];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableDictionary *resultArray = (NSMutableDictionary *)result;
            if (complete) complete(resultArray);
        }
    }];
    
}

+(void)fetchParticipatingUniversityList:(NSInteger)userID
                           onCompletion:(RequestArrayCompletionHandler)complete
{
    // Encode ARGS for URL
    NSString *userIDStringConversion = [NSString stringWithFormat:@"%ld", userID];
    userIDStringConversion = [userIDStringConversion URLEncode];
    
    NSString *baseURL = [NSString stringWithFormat:@"%@/", BASE_URL];
    NSString *detailPath = [NSString stringWithFormat:@"%@sc/promotions/fetch_university_list.php/", baseURL];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:userIDStringConversion forKey:@"user_id"];
    
    [IOSRequest requestPathAF2:baseURL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath withParameters:parameters onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *results = (NSMutableArray *)result;
            if (complete) complete(results);
        }
    }];
    
}

+(void)resetPasswordWithEntry:(NSString *)entry
                 onCompletion:(RequestDictionaryCompletionHandler)complete
{
    // Encode ARGS for URL
    NSString *entryConversion = [entry URLEncode];
    
    NSString *baseURL = [NSString stringWithFormat:@"%@/", BASE_URL];
    NSString *detailPath = [NSString stringWithFormat:@"%@sc/players/reset_password.php/", baseURL];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:entryConversion forKey:@"reset_entry"];
    
    [IOSRequest requestPathAF2:baseURL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath withParameters:parameters onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
    
}

+(void)setNewPasswordWithOldPassword:(NSString *)oldPassword
                      andNewPassword:(NSString *)newPassword
                             forUser:(NSInteger)userID
                        onCompletion:(RequestDictionaryCompletionHandler)complete
{
    // Encode ARGS for URL
    NSString *oldConvertedPassword = @"";
    if ([oldPassword length] > 0)
        oldConvertedPassword = [[oldPassword MD5] URLEncode];
    else
        oldConvertedPassword = @"";
    
    NSString *newConvertedPassword = [[newPassword MD5] URLEncode];
    
    
    NSString *baseURL = [NSString stringWithFormat:@"%@/", BASE_URL];
    NSString *detailPath = [NSString stringWithFormat:@"%@sc/players/set_new_password.php/", baseURL];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:oldConvertedPassword forKey:@"old_password"];
    [parameters setValue:newConvertedPassword forKey:@"new_password"];
    [parameters setValue:[NSString stringWithFormat:@"%ld", (long)userID] forKey:@"user"];
    
    [IOSRequest requestPathAF2:baseURL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath withParameters:parameters onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
    
}


+(void)checkPasswordExistsForUser:(NSInteger)userID
                     onCompletion:(RequestDictionaryCompletionHandler)complete
{
    // Encode ARGS for URL
    
    
    NSString *baseURL = [NSString stringWithFormat:@"%@/", BASE_URL];
    NSString *detailPath = [NSString stringWithFormat:@"%@sc/players/does_have_password.php/", baseURL];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    [parameters setValue:[NSString stringWithFormat:@"%ld", userID] forKey:@"user"];
    
    [IOSRequest requestPathAF2:baseURL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath withParameters:parameters onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
    
}
/******************************************************************************************/
/***************************** SPORTS *****************************************************/

// Sets the sports associated with a given player.  This is called in MySportsTableViewController

+(void)fetchPlayerSports:(NSInteger)userID
            onCompletion:(RequestArrayCompletionHandler)complete
{
    // Encode ARGS for URL
    NSString *userIDStringConversion = [NSString stringWithFormat:@"%ld", userID];
    userIDStringConversion = [userIDStringConversion URLEncode];
    
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/sports/getplayersports.php?user_id=%@",userIDStringConversion];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *resultArray = (NSMutableArray *)result;
            if (complete) complete(resultArray);
        }
    }];
    
}

// fetches the sports history for a given user and for a given sport
+(void)fetchPlayerSportHistory:(NSInteger)userID
                      forSport:(NSString *)sportName
                  onCompletion:(RequestArrayCompletionHandler)complete
{
    NSString *sportName_v2 = [sportName URLEncode];
    
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/sports/get_player_sport_history.php?user_id=%ld&sport_name=%@",userID, sportName_v2];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *resultArray = (NSMutableArray *)result;
            if (complete) complete(resultArray);
        }
    }];
    
}


// returns all the available sports.  It does not apply to a specific user.

+(void)fetchAllSports:(NSInteger)useID onCompletion:(RequestArrayCompletionHandler)complete
{
    
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/sports/getAvailSports.php?user_id=0"];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *resultArray = (NSMutableArray *)result;
            if (complete) complete(resultArray);
        }
    }];
    
}

+(void)fetchAllSportsV2:(NSInteger)userID onCompletion:(RequestDictionaryCompletionHandler)complete
{
    
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/sports/get_avail_sportsv2.php?user_id=%ld",userID];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
    
}

+(void)fetchAllLevelsonCompletion:(RequestArrayCompletionHandler)complete
{
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/sports/get_avail_levels.php"];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *resultArray = (NSMutableArray *)result;
            if (complete) complete(resultArray);
        }
    }];
}

+(void)fetchIndividualSportsList:(NSInteger)userID
                    onCompletion:(RequestArrayCompletionHandler)complete
{
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/sports/get_individual_sports.php?user_id=%ld",userID];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *resultArray = (NSMutableArray *)result;
            if (complete) complete(resultArray);
        }
    }];
    
}


+(void)fetchCrossReferencedSportsListBetweenSessionUser:(NSInteger)sessionUserID
                                          andCompetitor:(NSInteger)competitorUserID
                                           onCompletion:(RequestArrayCompletionHandler)complete
{
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/sports/get_cross_ref_individual_sports.php?session_user_id=%ld&competitor_user_id=%ld",sessionUserID, competitorUserID];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *resultArray = (NSMutableArray *)result;
            if (complete) complete(resultArray);
        }
    }];
    
}


+(void)fetchPickupSportsList:(NSInteger)userID
                onCompletion:(RequestArrayCompletionHandler)complete
{
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/sports/get_pickup_sports.php?user_id=%ld",userID];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *resultArray = (NSMutableArray *)result;
            if (complete) complete(resultArray);
        }
    }];
    
}

+(void)fetchMyIndividualSportLevels:(NSInteger)userID
                       onCompletion:(RequestArrayCompletionHandler)complete
{
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/sports/get_sport_levels.php?user_id=%ld",userID];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *resultArray = (NSMutableArray *)result;
            if (complete) complete(resultArray);
        }
    }];
    
}


+(void)createSportsForUser:(NSInteger)userID
                withSports:(NSString *)sportCodes
              onCompletion:(RequestDictionaryCompletionHandler)complete
{
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/sports/create_sports_for_user.php?user_id=%ld&sport_code=%@",userID, sportCodes];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_UPLOAD withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *resultArray = (NSDictionary *)result;
            if (complete) complete(resultArray);
        }
    }];
    
}
/***************************** END SPORTS *************************************************/
/******************************************************************************************/



/******************************* VENUE ****************************************************/
/******************************************************************************************/

+(void)saveVenueForUser:(NSInteger)userID
              venueName:(NSString *)venueName
                 street:(NSString *)street
                   city:(NSString *)city
                stateID:(NSString *)stateID
            description:(NSString *)description
              longitude:(float)longitude
               latitude:(float)latitude
             withSports:(NSString *)sportCodedString
              isPrivate:(BOOL)isPrivate
           onCompletion:(RequestDictionaryCompletionHandler)complete
{
    // Encode ARGS for URL
    NSString *userIDStringConversion = [NSString stringWithFormat:@"%ld", userID];
    userIDStringConversion = [userIDStringConversion URLEncode];
    
    
    venueName   = [venueName URLEncode];
    street      = [street URLEncode];
    city        = [city URLEncode];
    description = [description URLEncode];
    
    
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/venues/createvenue.php?user_id=%@&venue_name=%@&street=%@&city=%@&state_id=%@&description=%@&longitude=%f&latitude=%f&sports_code=%@&is_private=%d",userIDStringConversion, venueName, street, city, stateID, description, longitude, latitude, sportCodedString, isPrivate];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_UPLOAD withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *resultArray = (NSDictionary *)result;
            if (complete) complete(resultArray);
        }
    }];
    
    
}

+(void)saveVenueWithCoordinatesForUser:(NSInteger)userID
                             longitude:(float)longitude
                              latitude:(float)latitude
                             venueName:(NSString *)venueName
                           description:(NSString *)description
                            withSports:(NSString *)sportCodedString
                             isPrivate:(BOOL)isPrivate
                          onCompletion:(RequestDictionaryCompletionHandler)complete
{
    // Encode ARGS for URL
    NSString *userIDStringConversion = [NSString stringWithFormat:@"%ld", userID];
    userIDStringConversion = [userIDStringConversion URLEncode];
    
    
    venueName   = [venueName URLEncode];
    description = [description URLEncode];
    
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/venues/create_venue_with_coordinates.php?user_id=%@&venue_name=%@&description=%@&longitude=%f&latitude=%f&sports_code=%@&is_private=%d",userIDStringConversion, venueName, description, longitude, latitude, sportCodedString, isPrivate];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_UPLOAD withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *resultArray = (NSDictionary *)result;
            if (complete) complete(resultArray);
        }
    }];
    
}


+(void)saveTemporaryVenueForUser:(NSInteger)userID
                       venueName:(NSString *)venueName
                            city:(NSString *)city
                         stateID:(NSInteger)stateID
                     description:(NSString *)description
                      withSports:(NSString *)sportCodedString
                    onCompletion:(RequestDictionaryCompletionHandler)complete
{
    // Encode ARGS for URL
    NSString *userIDStringConversion = [NSString stringWithFormat:@"%ld", userID];
    userIDStringConversion = [userIDStringConversion URLEncode];
    
    
    venueName   = [venueName URLEncode];
    city        = [city URLEncode];
    description = [description URLEncode];
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/venues/create_temp_venue.php?user_id=%@&venue_name=%@&city=%@&state_id=%ld&description=%@&sports_code=%@",userIDStringConversion, venueName, city, stateID, description, sportCodedString];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_UPLOAD withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *resultArray = (NSDictionary *)result;
            if (complete) complete(resultArray);
        }
    }];
    
}


+(void)fetchUserVenuesFor:(NSInteger)userID onCompletion:(RequestArrayCompletionHandler)complete
{
    NSString *userIDStringConversion = [NSString stringWithFormat:@"%ld", userID];
    userIDStringConversion = [userIDStringConversion URLEncode];
    
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/venues/fetchuservenues.php?user_id=%@", userIDStringConversion];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *resultArray = (NSMutableArray *)result;
            if (complete) complete(resultArray);
        }
    }];
    
}

+(void)fetchVenueDetailForID:(NSInteger)venueID
             withSessionUser:(NSInteger)sessionUserID
                onCompletion:(RequestDictionaryCompletionHandler)complete
{
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/venues/fetch_venue_detail.php?venue_id=%ld&session_user_id=%ld", venueID, sessionUserID];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableDictionary *resultDictionary = (NSMutableDictionary *)result;
            if (complete) complete(resultDictionary);
        }
    }];
    
}


+ (void)fetchVenuesNearMe:(NSInteger)userID
                withRange:(NSInteger)range
            userLongitude:(float)longitude
             userLatitude:(float)latitude
             onCompletion:(RequestArrayCompletionHandler)complete


{
    NSString *detailPath = [NSString stringWithFormat:@"sc/venues/fetch_venues_near_me.php?user_id=%ld&range=%ld&longitude=%f&latitude=%f", userID, range, longitude, latitude];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *resultArray = (NSMutableArray *)result;
            if (complete) complete(resultArray);
        }
    }];
}

+ (void)changeFavoriteStatusForVenue:(NSInteger)venueID
                          withStatus:(BOOL)isFavorite
                            withUser:(NSInteger)userID
                        onCompletion:(RequestDictionaryCompletionHandler)complete


{
    NSString *detailPath = @"";
    
    if (isFavorite) {
        detailPath = [NSString stringWithFormat:@"sc/venues/make_venue_favorite.php?user_id=%ld&venue_id=%ld", userID, venueID];
    } else {
        detailPath = [NSString stringWithFormat:@"sc/venues/make_venue_unfavorite.php?user_id=%ld&venue_id=%ld", userID, venueID];
    }
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *resultArray = (NSDictionary *)result;
            if (complete) complete(resultArray);
        }
    }];
}


+ (void)submitVenueRating:(NSInteger)ratingValue
              withComment:(NSString *)comment
                   byUser:(NSInteger)userID
                 forVenue:(NSInteger)venueID
             onCompletion:(RequestDictionaryCompletionHandler)complete


{
    
    
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/venues/submit_venue_rating.php?user_id=%ld&venue_id=%ld&comment=%@&rating=%ld", userID, venueID, [comment URLEncode], ratingValue];
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_UPLOAD withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
    
}

+ (void)submitVenueFlag:(NSString *)flagType
            withComment:(NSString *)comment
                 byUser:(NSInteger)userID
               forVenue:(NSInteger)venueID
           onCompletion:(RequestDictionaryCompletionHandler)complete
{
    
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/venues/submit_venue_flag.php?user_id=%ld&venue_id=%ld&comment=%@&flag=%@", userID, venueID, [comment URLEncode], [flagType URLEncode]];
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_UPLOAD withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
    
}

+(void)fetchRatingsForVenue:(NSInteger)venueID
               onCompletion:(RequestDictionaryCompletionHandler)complete
{
    
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/venues/fetch_venue_ratings.php?venue_id=%ld", venueID];
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
    
}

+(void)fetchRatingForVenue:(NSInteger)venueID
                    byUser:(NSInteger)userID
              onCompletion:(RequestDictionaryCompletionHandler)complete
{
    
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/venues/fetch_venue_rating_by_user.php?venue_id=%ld&user_id=%ld", venueID, userID];
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
    
}

// Check Ins


+ (void)fetchAllContacts:(NSInteger)userID
            onCompletion:(RequestArrayCompletionHandler)complete
{
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/venues/check_in/get_all_contacts.php?user=%ld", userID];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *resultArray = (NSMutableArray *)result;
            if (complete) complete(resultArray);
        }
    }];
}

+ (void)checkInUser:(NSInteger)userID
        atLongitude:(CGFloat)longitude
         atLatitude:(CGFloat)latitude
            atVenue:(NSInteger)venueID
         atDateTime:(NSDate *)checkInDateTime
              isNow:(NSInteger)isNowInt
          withSport:(NSInteger)sportID
         andMessage:(NSString *)message
          andNotify:(NSMutableArray *)notifyArray
     postToNewsfeed:(NSInteger)shouldPostToNewsfeedInt
       onCompletion:(RequestDictionaryCompletionHandler)complete
{
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    
    NSString *dateTime = [[formatter stringFromDate: checkInDateTime] URLEncode];
    
    // set competitors to notify
    
    NSString *competitorCodeString = @"";
    int counter = 0;
    
    for (Player *object in notifyArray) {
        if (counter == 0)
            competitorCodeString = [NSString stringWithFormat:@"%ld", object.userID];
        else
            competitorCodeString = [competitorCodeString stringByAppendingFormat:@"-%ld", object.userID];
        
        counter ++;
    }
    
    
    
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/venues/check_in/check_in_at_venue.php?user=%ld&longitude=%f&latitude=%f&venue=%ld&datetime=%@&now_flag=%ld&message=%@&sport=%ld&should_newsfeed=%ld&comp_code=%@", userID, longitude, latitude, venueID, dateTime, isNowInt, [message URLEncode], sportID, shouldPostToNewsfeedInt, competitorCodeString];
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
}

+ (void)getVenueCheckIns:(NSInteger)venueID
         withSessionUser:(NSInteger)userID
   showFollowingListOnly:(BOOL)showFollowingListOnly
            onCompletion:(RequestArrayCompletionHandler)complete
{
    NSInteger showFollowingListOnlyIntValue = 0;
    
    if (showFollowingListOnly)
        showFollowingListOnlyIntValue = 1;
    else
        showFollowingListOnlyIntValue = 0;
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/venues/check_in/get_venue_checkins.php?user=%ld&venue=%ld&show_following=%ld", userID, venueID, showFollowingListOnlyIntValue];
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *results = (NSMutableArray *)result;
            if (complete) complete(results);
        }
    }];
    
}

/******************************* END VENUE ************************************************/
/******************************************************************************************/


/******************************* MATCHES **************************************************/
/******************************************************************************************/

+ (void)createPickupMatchWithInfo:(PickupMatch *)iMatch
                     onCompletion:(RequestDictionaryCompletionHandler)complete
{
    NSString *matchName     = [iMatch.matchName URLEncode];
    NSString *matchCreatorID = [[NSString stringWithFormat:@"%ld", iMatch.matchCreatorID] URLEncode];
    NSString *matchMessage  = [iMatch.message URLEncode];
    NSInteger isPrivate     = (iMatch.private) ? 1 : 0;
    NSString *sportType     = [iMatch.sport.sportType URLEncode];
    NSString *sportName     = [iMatch.sport.sportName URLEncode];
    NSInteger matchTypeID   = iMatch.matchType.matchTypeID;
    NSString *gender = [iMatch.gender URLEncode];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    
    NSString *dateTime = [[formatter stringFromDate: iMatch.dateTime] URLEncode];
    
    NSInteger venueID       = iMatch.venue.venueID;
    NSString *expCode       = iMatch.experienceCode;
    
    
    // need to find competitors
    // Rather than use JSON, I'm going to make a string of all competitor userID's where they are
    // marked by a "-" before the number.
    
    NSString *competitorCodeString = [NSString stringWithFormat:@"%@", matchCreatorID];  // start by making match creator a competitor
    
    for (Player *object in iMatch.competitorArray) {
        competitorCodeString = [competitorCodeString stringByAppendingFormat:@"-%ld", object.userID];
    }
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/matches/savepickupmatch.php?creator_id=%@&match_name=%@&message=%@&isprivate=%ld&sportname=%@&sporttype=%@&datetime=%@&venue_id=%ld&expcode=%@&compcode=%@&match_type_id=%ld&gender=%@", matchCreatorID, matchName, matchMessage, isPrivate, sportName, sportType, dateTime, venueID, expCode, competitorCodeString, matchTypeID, gender];
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_UPLOAD withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
    
}

+(void)fetchPickupMatchesForUser:(NSInteger)userID onCompletion:(RequestArrayCompletionHandler)complete
{
    NSString *userIDStringConversion = [NSString stringWithFormat:@"%ld", userID];
    userIDStringConversion = [userIDStringConversion URLEncode];
    
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/matches/getpickupmatches.php?user_id=%@", userIDStringConversion];
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *results = (NSMutableArray *)result;
            if (complete) complete(results);
        }
    }];
    
}

+ (void)rsvpForPickupMatch:(NSInteger)matchID
                  withRSVP:(NSInteger)rsvp
                andComment:(NSString *)comment
                   forUser:(NSInteger)userID
              onCompletion:(RequestDictionaryCompletionHandler)complete
{
    NSString *encodedComment = [comment URLEncode];
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/matches/rsvppickupmatch.php?match_id=%ld&rsvp=%ld&user_id=%ld&comment=%@", matchID, rsvp, userID, encodedComment];
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_UPLOAD withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
}

+(void)fetchPastPickupMatchesForUser:(NSInteger)userID onCompletion:(RequestArrayCompletionHandler)complete
{
    NSString *userIDStringConversion = [NSString stringWithFormat:@"%ld", userID];
    userIDStringConversion = [userIDStringConversion URLEncode];
    
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/matches/pastpickupmatches.php?user_id=%@", userIDStringConversion];
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *results = (NSMutableArray *)result;
            if (complete) complete(results);
        }
    }];
    
}


+ (void)createIndividualMatchWithInfo:(IndividualMatch *)iMatch
                         onCompletion:(RequestDictionaryCompletionHandler)complete
{
    NSString *matchName     = [iMatch.matchName URLEncode];
    NSString *matchCreatorID = [[NSString stringWithFormat:@"%ld", iMatch.matchCreatorID] URLEncode];
    NSString *matchMessage  = [iMatch.message URLEncode];
    NSInteger isPrivate     = (iMatch.private) ? 1 : 0;
    //    NSString *matchType     = [iMatch.sport.sportType URLEncode];
    
    //    NSString *sportName     = [iMatch.sport.sportName URLEncode];
    NSInteger sportID = iMatch.sport.sportID;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    
    NSString *dateTime = [[formatter stringFromDate: iMatch.dateTime] URLEncode];
    
    NSInteger venueID       = iMatch.venue.venueID;
    
    
    // need to find competitors
    // Rather than use JSON, I'm going to make a string of all competitor userID's where they are
    // marked by a "-" before the number.
    
    NSString *competitorCodeString = [NSString stringWithFormat:@"%@", matchCreatorID];  // start by making match creator a competitor
    
    competitorCodeString = [competitorCodeString stringByAppendingFormat:@"-%ld", iMatch.player2.userID];
    
    
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/matches/saveindividualmatch.php?creator_id=%@&match_name=%@&message=%@&isprivate=%ld&sport_id=%ld&datetime=%@&venue_id=%ld&compcode=%@", matchCreatorID, matchName, matchMessage, isPrivate, sportID, dateTime, venueID, competitorCodeString];
    
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_UPLOAD withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
    
}

+(void)fetchIndividualMatchesForUser:(NSInteger)userID onCompletion:(RequestArrayCompletionHandler)complete
{
    NSString *userIDStringConversion = [NSString stringWithFormat:@"%ld", userID];
    userIDStringConversion = [userIDStringConversion URLEncode];
    
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/matches/getindividualmatches.php?user_id=%@", userIDStringConversion];
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *results = (NSMutableArray *)result;
            if (complete) complete(results);
        }
    }];
    
}

+ (void)rsvpForIndividualMatch:(NSInteger)matchID
                      withRSVP:(NSInteger)rsvp
                    andComment:(NSString *)comment
                       forUser:(NSInteger)userID
                  onCompletion:(RequestDictionaryCompletionHandler)complete
{
    NSString *encodedComment = [comment URLEncode];
    
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/matches/rsvpindividualmatch.php?match_id=%ld&rsvp=%ld&comment=%@&user_id=%ld", matchID, rsvp, encodedComment, userID];
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_UPLOAD withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
    
}

+(void)fetchPastMatchesForUser:(NSInteger)userID onCompletion:(RequestDictionaryCompletionHandler)complete
{
    NSString *userIDStringConversion = [NSString stringWithFormat:@"%ld", userID];
    userIDStringConversion = [userIDStringConversion URLEncode];
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/matches/new/get_past_matches.php?user_id=%@", userIDStringConversion];
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
    
}

+(void)cancelMatchWithID:(NSInteger)matchID
                  byUser:(NSInteger)userID
                withType:(NSString *)matchType onCompletion:(RequestDictionaryCompletionHandler)complete
{
    matchType = [matchType capitalizedString];
    
    NSString *detailPath = @"";
    
    [NSString stringWithFormat:@"sc/matches/cancelindividualmatch.php?match_id=%ld&user_id=%ld", matchID, userID];
    
    if ([matchType isEqualToString:@"Individual"]) {
        detailPath = [NSString stringWithFormat:@"sc/matches/cancelindividualmatch.php?match_id=%ld&user_id=%ld", matchID, userID];
    } else if ([matchType isEqualToString:@"Pickup"]) {
        detailPath = [NSString stringWithFormat:@"sc/matches/cancelpickupmatch.php?match_id=%ld&user_id=%ld", matchID, userID];
    }
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
    
}

+(void)confirmMatchWithID:(NSInteger)matchID
                   byUser:(NSInteger)userID
                 withType:(NSString *)matchType
             onCompletion:(RequestDictionaryCompletionHandler)complete
{
    matchType = [matchType capitalizedString];
    
    NSString *detailPath = @"";
    
    if ([matchType isEqualToString:@"Individual"]) {
        detailPath = [NSString stringWithFormat:@"sc/matches/confirmindividualmatch.php?match_id=%ld&user_id=%ld", matchID, userID];
    } else if ([matchType isEqualToString:@"Pickup"]) {
        detailPath = [NSString stringWithFormat:@"sc/matches/confirm_pickup_match.php?match_id=%ld&user_id=%ld", matchID, userID];
    }
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
    
    
    
}

+ (void)setScoreForIndividualMatch:(NSInteger)matchID
                  withWinningScore:(NSInteger)winnerScore
                    forWinningUser:(NSInteger)winnerUserID
                   withLosingScore:(NSInteger)loserScore
                     forLosingUser:(NSInteger)loserUserID
                         recoredBy:(NSInteger)recordedByID
                      onCompletion:(RequestDictionaryCompletionHandler)complete
{
    NSString *detailPath = [NSString stringWithFormat:@"sc/matches/set_score_individualmatch_main.php?match_id=%ld&winner_score=%ld&winner_user_id=%ld&loser_score=%ld&loser_user_id=%ld&recorded_by=%ld", matchID, winnerScore, winnerUserID, loserScore, loserUserID, recordedByID];
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_UPLOAD withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
    
}

// new set score w/ multiple rounds (this should supercede previous function

+ (void)setScoreForIndividualMatchWithMultipleRounds:(IndividualMatch *)individualMatch
                                          withScores:(NSDictionary *)scoreDictionary
                                         confirmedBy:(NSInteger)confirmerUserID
                                        onCompletion:(RequestDictionaryCompletionHandler)complete
{
    
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/matches/new/set_score_individualmatch_with_rounds.php?player1score=%@&player2score=%@&player1_id=%ld&player2_id=%ld&match_id=%ld&larger_score_wins=%d&confirmer_id=%ld&version=%ld", [scoreDictionary objectForKey:@"player1_score"], [scoreDictionary objectForKey:@"player2_score"], individualMatch.player1.userID, individualMatch.player2.userID, individualMatch.matchID, individualMatch.sport.largerScoreWins, confirmerUserID, individualMatch.scoreIndMatch.version];
    
    
    //   NSLog(@"%@/%@", BASE_URL, detailPath);
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_UPLOAD withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *resultArray = (NSDictionary *)result;
            if (complete) complete(resultArray);
        }
    }];
    
}

+ (void)confirmScoreForIndividualMatchWithMultipleRounds:(NSInteger)matchID
                                              forVersion:(NSInteger)version
                                             confirmedBy:(NSInteger)confirmerUserID
                                            onCompletion:(RequestDictionaryCompletionHandler)complete
{
    
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/matches/new/confirm_score_individualmatch_with_rounds.php?match_id=%ld&version=%ld&confirmer_id=%ld", matchID, version, confirmerUserID];
    
    
    //   NSLog(@"%@/%@", BASE_URL, detailPath);
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableDictionary *resultArray = (NSMutableDictionary *)result;
            if (complete) complete(resultArray);
        }
    }];
    
}


+ (void)disputeScoreForIndividualMatchWithMultipleRounds:(NSInteger)matchID
                                              disputedBy:(NSInteger)userID
                                            onCompletion:(RequestDictionaryCompletionHandler)complete
{
    
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/matches/new/2nddispute_and_close_individual_match.php?match_id=%ld&user_id=%ld", matchID, userID];
    
    
    //   NSLog(@"%@/%@", BASE_URL, detailPath);
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableDictionary *resultArray = (NSMutableDictionary *)result;
            if (complete) complete(resultArray);
        }
    }];
    
}

+ (void)setMatchAsDisputed:(NSInteger)matchID
                disputedBy:(NSInteger)userID
              onCompletion:(RequestDictionaryCompletionHandler)complete
{
    NSString *detailPath = [NSString stringWithFormat:@"sc/matches/new/set_match_as_disputed.php?match_id=%ld&disputor_id=%ld", matchID, userID];
    
    
    //   NSLog(@"%@/%@", BASE_URL, detailPath);
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableDictionary *resultArray = (NSMutableDictionary *)result;
            if (complete) complete(resultArray);
        }
    }];
}

// start new matches ****

+ (void)fetchMatchNotifications:(NSInteger)userID
                   onCompletion:(RequestDictionaryCompletionHandler2)complete


{
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/matches/get_match_notifications.php?user_id=%ld", userID];
    
    
    //   NSLog(@"%@/%@", BASE_URL, detailPath);
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil, error);
        } else {
            NSMutableDictionary *resultArray = (NSMutableDictionary *)result;
            if (complete) complete(resultArray, error);
        }
    }];
    
}

+ (void)fetchIndividualMatchDetails:(NSInteger)matchID
                        sessionUser:(NSInteger)userID
                       onCompletion:(RequestDictionaryCompletionHandler)complete
{
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/matches/new/get_individual_match_info.php?match_id=%ld&user_id=%ld", matchID, userID];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *resultArray = (NSDictionary *)result;
            if (complete) complete(resultArray);
        }
    }];
}

+ (void)fetchPickupMatchDetails:(NSInteger)matchID
                   onCompletion:(RequestDictionaryCompletionHandler)complete
{
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/matches/new/get_pickup_match_info.php?match_id=%ld", matchID];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *resultArray = (NSDictionary *)result;
            if (complete) complete(resultArray);
        }
    }];
}


+ (void)fetchChallengeMatchesOpenToMe:(NSInteger)userID
                         onCompletion:(RequestArrayCompletionHandler)complete
{
    
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/matches/new/get_challenge_matches_open_to_me.php?user_id=%ld", userID];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *resultArray = (NSMutableArray *)result;
            if (complete) complete(resultArray);
        }
    }];
}

+ (void)fetchPickupMatchesOpenToMe:(NSInteger)userID
                      onCompletion:(RequestArrayCompletionHandler)complete
{
    
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/matches/new/get_pickup_matches_open_to_me.php?user_id=%ld", userID];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *resultArray = (NSMutableArray *)result;
            if (complete) complete(resultArray);
        }
    }];
}

+ (void)fetchMyUpcomingMatches:(NSInteger)userID
                  onCompletion:(RequestDictionaryCompletionHandler)complete
{
    NSString *detailPath = [NSString stringWithFormat:@"sc/matches/new/get_upcoming_matches2.php?user_id=%ld", userID];
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
}

+ (void)fetchTodaysgMatches:(NSInteger)userID
               onCompletion:(RequestDictionaryCompletionHandler)complete
{
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/matches/new/get_todays_matches.php?user_id=%ld", userID];
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
    
}

+ (void)fetchPendingMatchesIJoined:(NSInteger)userID
                      onCompletion:(RequestDictionaryCompletionHandler)complete
{
    NSString *detailPath = [NSString stringWithFormat:@"sc/matches/new/get_pending_matches_not_yet_confirmed.php?user_id=%ld", userID];
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
}

+ (void)fetchMatchesNeedingScoresOrConfirmations:(NSInteger)userID
                                    onCompletion:(RequestDictionaryCompletionHandler)complete
{
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/matches/new/get_matches_needing_actions.php?user_id=%ld", userID];
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
}

+ (void)fetchMatchesNearMe:(NSInteger)userID
                 withRange:(NSInteger)range
             userLongitude:(float)longitude
              userLatitude:(float)latitude
              onCompletion:(RequestArrayCompletionHandler)complete
{
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/matches/new/get_matches_near_me.php?user_id=%ld&range=%ld&longitude=%f&latitude=%f", userID, range, longitude, latitude];
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *results = (NSMutableArray *)result;
            if (complete) complete(results);
        }
    }];
}


+(void)getMatchTypesForSport:(NSInteger)sportID onCompletion:(RequestArrayCompletionHandler)complete
{
    NSString *detailPath = [NSString stringWithFormat:@"sc/matches/new/get_match_types.php?sport_id=%ld", sportID];
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *results = (NSMutableArray *)result;
            if (complete) complete(results);
        }
    }];
    
}


+(void)getIndividualMatchComments:(NSUInteger)matchID onCompletion:(RequestArrayCompletionHandler)complete
{
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/comments/get_individual_match_comments.php?match_id=%lu", matchID];
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *results = (NSMutableArray *)result;
            if (complete) complete(results);
        }
    }];
    
}

// recording comment specifically for a match
+(void)recordComment:(NSString *)comment
              byUser:(NSInteger)userID
            forMatch:(NSInteger)matchID
   withMatchCategory:(NSString *)matchCategory
        onCompletion:(RequestDictionaryCompletionHandler)complete
{
    NSString *revisedComment = [comment URLEncode];
    
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/matches/match_history/post_match_comment.php?comment=%@&user_id=%ld&match_id=%ld&match_category=%@", revisedComment, userID, matchID, matchCategory];
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_UPLOAD withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
}

+(void)setIndividualMatchAsNeverOccurred:(NSUInteger)matchID
                                  byUser:(NSUInteger)userID
                            onCompletion:(RequestDictionaryCompletionHandler)complete
{
    NSString *detailPath = [NSString stringWithFormat:@"sc/matches/new/set_ind_match_as_never_occurred.php?match_id=%lu&user_id=%lu", matchID, userID];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
}

+(void)setPickupMatchAsNeverOccurred:(NSUInteger)matchID
                              byUser:(NSUInteger)userID
                        onCompletion:(RequestDictionaryCompletionHandler)complete
{
    NSString *detailPath = [NSString stringWithFormat:@"sc/matches/new/set_pickup_match_as_never_occurred.php?match_id=%lu&user_id=%lu", matchID, userID];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
}

+(void)doesAlertsPageNeedRefreshForUser:(NSUInteger)userID
                           onCompletion:(RequestDictionaryCompletionHandler)complete
{
    NSString *detailPath = [NSString stringWithFormat:@"sc/matches/new/does_alerts_need_refresh.php?user_id=%lu", userID];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
}

+(void)getMatchUpdatesForUser:(NSUInteger)userID
                 onCompletion:(RequestArrayCompletionHandler)complete
{
    NSString *detailPath = [NSString stringWithFormat:@"sc/matches/new/get_match_updates.php?session_user_id=%lu", userID];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *results = (NSMutableArray *)result;
            if (complete) complete(results);
        }
    }];
}

+(void)getIndividualMatchHistory:(NSUInteger)matchID
                    onCompletion:(RequestArrayCompletionHandler)complete
{
    NSString *detailPath = [NSString stringWithFormat:@"sc/matches/match_history/get_history_for_individual_match.php?match_id=%lu", matchID];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *results = (NSMutableArray *)result;
            if (complete) complete(results);
        }
    }];
}

+(void)getPickupMatchHistory:(NSUInteger)matchID
                onCompletion:(RequestArrayCompletionHandler)complete
{
    NSString *detailPath = [NSString stringWithFormat:@"sc/matches/match_history/get_history_for_pickup_match.php?match_id=%lu", matchID];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *results = (NSMutableArray *)result;
            if (complete) complete(results);
        }
    }];
}

+(void)getUpdatedLevelsForUser:(NSUInteger)userID
                      forMatch:(NSUInteger)matchID
                  onCompletion:(RequestDictionaryCompletionHandler)complete
{
    NSString *detailPath = [NSString stringWithFormat:@"sc/matches/new/get_user_levels_after_match.php?user_id=%lu&match_id=%lu", userID, matchID];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
}



/******************************* END MATCHES **********************************************/
/******************************************************************************************/


/*******************************  RATE USER  **********************************************/
/******************************************************************************************/


+ (void)rateUser:(NSInteger)ratedUserID
          byUser:(NSInteger)raterUserID
      withRating:(NSString *)experienceRating
     withComment:(NSString *)comment
        forMatch:(NSInteger)matchID
    onCompletion:(RequestDictionaryCompletionHandler)complete
{
    NSString *experienceRatingConversion = [experienceRating URLEncode];
    NSString *commentConversion = [comment URLEncode];
    
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/rateuser.php?rater_user_id=%ld&rated_user_id=%ld&experience=%@&comment=%@&match_id=%ld", raterUserID, ratedUserID, experienceRatingConversion, commentConversion, matchID];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_UPLOAD withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
    
}


+(void)getSportRatingsForUser:(NSUInteger)userID
                     forSport:(NSUInteger)sportID
                 onCompletion:(RequestArrayCompletionHandler)complete
{
    NSString *detailPath = [NSString stringWithFormat:@"sc/players/ratings/get_sport_ratings.php?user_id=%lu&sport_id=%lu", userID, sportID];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *resultArray = (NSMutableArray *)result;
            if (complete) complete(resultArray);
        }
    }];
    
}


+(void)getSportRatingsListForUser:(NSUInteger)ratedPlayerID
                sportRatingTypeID:(NSUInteger)srTypeID
                     onCompletion:(RequestArrayCompletionHandler)complete
{
    NSString *detailPath = [NSString stringWithFormat:@"sc/players/ratings/get_sport_ratings_list.php?srtype_id=%lu&rated_player_id=%lu", srTypeID, ratedPlayerID];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *resultArray = (NSMutableArray *)result;
            if (complete) complete(resultArray);
        }
    }];
    
}

// When rating a player, this returns a list of all possible skill types for a sport such as "Crossover" or "Dribbling"
// It will also return any ratings the session user made have made for the target user

+(void)getSkillTypesForSport:(NSUInteger)sportID
             withSessionUser:(NSUInteger)sessionUserID
               andTargetUser:(NSUInteger)targetUserID
                onCompletion:(RequestArrayCompletionHandler)complete
{
    NSString *detailPath = [NSString stringWithFormat:@"sc/players/ratings/get_attributes_for_sport.php?sport_id=%lu&session_user_id=%lu&target_user_id=%lu", sportID, sessionUserID, targetUserID];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *resultArray = (NSMutableArray *)result;
            if (complete) complete(resultArray);
        }
    }];
    
}

+(void)saveSportRatingForSkillAttribute:(NSUInteger)srTypeID
                        withSessionUser:(NSUInteger)sessionUserID
                          andTargetUser:(NSUInteger)targetUserID
                             withRating:(float)ratingValue
                             andComment:(NSString *)comment
                           onCompletion:(RequestDictionaryCompletionHandler)complete
{
    NSString *detailPath = [NSString stringWithFormat:@"sc/players/ratings/save_sport_rating.php?srtype_id=%lu&session_user_id=%lu&target_user_id=%lu&rating_value=%f&comment=%@", srTypeID, sessionUserID, targetUserID, ratingValue, [comment URLEncode]];
    
    //   NSLog(@"%@/%@", BASE_URL, detailPath);
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_UPLOAD withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *resultArray = (NSDictionary *)result;
            if (complete) complete(resultArray);
        }
    }];
    
}




/*******************************  NEWSFEED   **********************************************/
/******************************************************************************************/

+(void)fetchPickupMatchesNewsfeeds:(NSInteger)userID onCompletion:(RequestArrayCompletionHandler)complete
{
    NSString *userIDStringConversion = [NSString stringWithFormat:@"%ld", userID];
    userIDStringConversion = [userIDStringConversion URLEncode];
    
    
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/newsfeed/get_pickup_matches.php?user_id=%@", userIDStringConversion];
    
    //  NSLog(@"%@/%@", BASE_URL, detailPath);
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *resultArray = (NSMutableArray *)result;
            if (complete) complete(resultArray);
        }
    }];
    
}

+(void)fetchIndividualMatchesNewsfeeds:(NSInteger)userID onCompletion:(RequestArrayCompletionHandler)complete
{
    NSString *userIDStringConversion = [NSString stringWithFormat:@"%ld", userID];
    userIDStringConversion = [userIDStringConversion URLEncode];
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/newsfeed/get_individual_matches.php?user_id=%@", userIDStringConversion];
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *resultArray = (NSMutableArray *)result;
            if (complete) complete(resultArray);
        }
    }];
    
}

+(void)recordComment:(NSString *)comment
              byUser:(NSInteger)userID
         forNewsPost:(NSInteger)newsID
        onCompletion:(RequestArrayCompletionHandler)complete
{
    NSString *revisedComment = [comment URLEncode];
    
    
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/comments/post_comment.php?comment=%@&user_id=%ld&ref_id=%ld&type=1", revisedComment, userID, newsID];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_UPLOAD withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *resultArray = (NSMutableArray *)result;
            if (complete) complete(resultArray);
        }
    }];
    
}
+(void)fetchCommentsforNewsPost:(NSInteger)newsID
                   onCompletion:(RequestArrayCompletionHandler)complete
{
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/comments/get_comments.php?ref_id=%ld&type=1", newsID];
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *resultArray = (NSMutableArray *)result;
            if (complete) complete(resultArray);
        }
    }];
    
}

+(void)saveProbabilityWithValue:(float)probabilityValue
             forIndividualMatch:(NSInteger)matchID
                   favoringUser:(NSInteger)favoredUserID
                         byUser:(NSInteger)ratingUserID
                   onCompletion:(RequestArrayCompletionHandler)complete
{
    
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/newsfeed/save_probability.php?probability=%f&match_id=%ld&favored_user_id=%ld&rating_user_id=%ld", probabilityValue, matchID, favoredUserID, ratingUserID];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_UPLOAD withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *resultArray = (NSMutableArray *)result;
            if (complete) complete(resultArray);
        }
    }];
    
}

+(void)getProbabilityforMatch:(NSInteger)matchID
                 onCompletion:(RequestArrayCompletionHandler)complete
{
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/newsfeed/get_probability.php?match_id=%ld", matchID];
    
    //   NSLog(@"%@/%@", BASE_URL, detailPath);
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *resultArray = (NSMutableArray *)result;
            if (complete) complete(resultArray);
        }
    }];
    
}

+(void)checkIfSessionUserVotedProbabiltyForMatch:(NSInteger)matchID
                                          byUser:(NSInteger)userID
                                    onCompletion:(RequestDictionaryCompletionHandler)complete
{
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/newsfeed/check_if_voted_probability.php?match_id=%ld&user_id=%ld", matchID, userID];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
    
    
}


+(void)fetchNewsfeeds:(NSInteger)userID
          withPageNum:(NSInteger)pageNum
              andSpan:(NSInteger)cellSpan
         onCompletion:(RequestArrayCompletionHandler)complete
{
    NSString *baseURL = [NSString stringWithFormat:@"%@/", BASE_URL];
    NSString *detailPath = [NSString stringWithFormat:@"%@sc/newsfeed/get_newsfeed2.php/", baseURL];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:[NSString stringWithFormat:@"%ld", userID] forKey:@"user_id"];
    [parameters setValue:[NSString stringWithFormat:@"%ld", pageNum] forKey:@"page_num"];
    [parameters setValue:[NSString stringWithFormat:@"%ld", cellSpan] forKey:@"span_cells"];
    
    [IOSRequest requestPathAF2:baseURL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath withParameters:parameters onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *resultArray = (NSMutableArray *)result;
            if (complete) complete(resultArray);
        }
    }];
    
}

+(void)fetchBrowseNewsfeeds:(NSInteger)userID
                withPageNum:(NSInteger)pageNum
                    andSpan:(NSInteger)cellSpan
               onCompletion:(RequestArrayCompletionHandler)complete
{
    NSString *baseURL = [NSString stringWithFormat:@"%@/", BASE_URL];
    NSString *detailPath = [NSString stringWithFormat:@"%@sc/newsfeed/get_browse_newsfeed.php/", baseURL];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:[NSString stringWithFormat:@"%ld", userID] forKey:@"user_id"];
    [parameters setValue:[NSString stringWithFormat:@"%ld", pageNum] forKey:@"page_num"];
    [parameters setValue:[NSString stringWithFormat:@"%ld", cellSpan] forKey:@"span_cells"];
    
    [IOSRequest requestPathAF2:baseURL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath withParameters:parameters onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *resultArray = (NSMutableArray *)result;
            if (complete) complete(resultArray);
        }
    }];
    
}

+(void)fetchNewsfeedDetail:(NSUInteger)newsfeedID
                    byUser:(NSInteger)userID onCompletion:(RequestArrayCompletionHandler)complete
{
    NSString *detailPath = [NSString stringWithFormat:@"sc/newsfeed/get_single_newsfeed_post.php?newsfeed_id=%lu&user_id=%ld", newsfeedID, userID];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_UPLOAD withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *results = (NSMutableArray *)result;
            if (complete) complete(results);
        }
    }];
    
}


+(void)fetchProfileNewsfeed:(NSInteger)userID withSessionUser:(NSInteger)sessionUserID withPageNum:(NSInteger)pageNum andSpan:(NSInteger)cellSpan onCompletion:(RequestArrayCompletionHandler)complete
{
    NSString *baseURL = [NSString stringWithFormat:@"%@/", BASE_URL];
    NSString *detailPath = [NSString stringWithFormat:@"%@sc/players/get_profile_newsfeed.php/", baseURL];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:[NSString stringWithFormat:@"%ld", userID] forKey:@"user_id"];
    [parameters setValue:[NSString stringWithFormat:@"%ld", sessionUserID] forKey:@"session_user_id"];
    [parameters setValue:[NSString stringWithFormat:@"%ld", pageNum] forKey:@"page_num"];
    [parameters setValue:[NSString stringWithFormat:@"%ld", cellSpan] forKey:@"span_cells"];
    
    [IOSRequest requestPathAF2:baseURL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath withParameters:parameters onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *resultArray = (NSMutableArray *)result;
            if (complete) complete(resultArray);
        }
    }];
    
}
+(void)makeRemixForNewsfeed:(NSUInteger)newsfeedID byUser:(NSInteger)userID onCompletion:(RequestDictionaryCompletionHandler)complete
{
    NSString *detailPath = [NSString stringWithFormat:@"sc/newsfeed/make_remix.php?newsfeed_id=%lu&user_id=%ld", newsfeedID, userID];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_UPLOAD withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
    
}


// for saving jpgs, videos from newsfeed post
+(void)saveNewsfeedTextPostWithFile:(NSString *)fileName
                            forUser:(NSInteger)userID
                       withFileType:(NSString *)fileType
                           withText:(NSString *)textPostString
                       onCompletion:(RequestDictionaryCompletionHandler)complete
{
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/s3image/save_newsfeed_file_post.php?filename=%@&user_id=%ld&filetype=%@&text_post=%@",[fileName URLEncode], userID, fileType, [textPostString URLEncode]];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_UPLOAD withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
    
}

// for saving jpgs, videos from newsfeed post
+(void)saveNewsfeedTextPost:(NSString *)textPostString
                    forUser:(NSInteger)userID
               onCompletion:(RequestDictionaryCompletionHandler)complete
{
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/newsfeed/save_text_post.php?user_id=%ld&text_post=%@", userID, [textPostString URLEncode]];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_UPLOAD withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
}


+ (void)likeNewsfeedPost:(NSInteger)newsfeedID
           sessionUserID:(NSInteger)sessionUserID
            onCompletion:(RequestDictionaryCompletionHandler)complete
{
    NSString *detailPath = [NSString stringWithFormat:@"sc/newsfeed/like_post.php?newsfeed_id=%ld&user_id=%ld", newsfeedID, sessionUserID];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
}

+ (void)UnLikeNewsfeedPost:(NSInteger)newsfeedID
             sessionUserID:(NSInteger)sessionUserID
              onCompletion:(RequestDictionaryCompletionHandler)complete
{
    NSString *detailPath = [NSString stringWithFormat:@"sc/newsfeed/unlike_post.php?newsfeed_id=%ld&user_id=%ld", newsfeedID, sessionUserID];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
}

+ (void)getPostLikes:(NSInteger)newsfeedID
        onCompletion:(RequestArrayCompletionHandler)complete
{
    NSString *detailPath = [NSString stringWithFormat:@"sc/newsfeed/get_post_likes.php?newsfeed_id=%ld", newsfeedID];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *results = (NSMutableArray *)result;
            if (complete) complete(results);
        }
    }];
}

+ (void)deletePost:(NSInteger)newsfeedID
            byUser:(NSInteger)userID
      onCompletion:(RequestDictionaryCompletionHandler)complete
{
    NSString *detailPath = [NSString stringWithFormat:@"sc/newsfeed/delete_post.php?newsfeed_id=%ld&user_id=%ld", newsfeedID, userID];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
}

+ (void)flagPost:(NSInteger)newsfeedID
          byUser:(NSInteger)userID
    withFeedback:(NSString *)feedbackString
    onCompletion:(RequestDictionaryCompletionHandler)complete
{
    NSString *detailPath = [NSString stringWithFormat:@"sc/newsfeed/flag_post.php?newsfeed_id=%ld&user_id=%ld&feedback=%@", newsfeedID, userID, [feedbackString URLEncode]];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
}



/******************************* END NEWSFEED *********************************************/
/******************************************************************************************/

/*********************************** SEARCH   *********************************************/
/******************************************************************************************/

+(void)searchForUser:(NSString *)searchString notAssociatedWith:(NSInteger)userID onCompletion:(RequestArrayCompletionHandler)complete
{
    NSString *userIDStringConversion = [NSString stringWithFormat:@"%ld", userID];
    userIDStringConversion = [userIDStringConversion URLEncode];
    
    NSString *convertedSearchString = [searchString URLEncode];
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/search/searchforuser.php?user_id=%@&search_string=%@", userIDStringConversion, convertedSearchString];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *results = (NSMutableArray *)result;
            if (complete) complete(results);
        }
    }];
    
}


/******************************* END SEARCH   *********************************************/
/******************************************************************************************/

+(void)checkIfAppNeedsUpdateForPhoneVersion:(NSString *)versionOnPhone
                               onCompletion:(RequestDictionaryCompletionHandler)complete
{
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/does_app_need_update.php?version_on_phone=%@", [versionOnPhone URLEncode]];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableDictionary *resultArray = (NSMutableDictionary *)result;
            if (complete) complete(resultArray);
        }
    }];
    
    
    
}

// Check FB Friends with Vaiden Database
+ (void)checkFBFriendsAgainstVaidenDBForUser:(NSInteger)userID
                                 withFriends:(NSDictionary *)friendsDictionary
                                onCompletion:(RequestArrayCompletionHandler)complete
{
    NSString *detailPage = [NSString stringWithFormat:@"sc/players/check_fb_friends.php"];
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:BASE_URL]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:detailPage
                                                      parameters:friendsDictionary];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [httpClient registerHTTPOperationClass:[AFHTTPRequestOperation class]];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        // Print the response body in text
        
        /* my code */
        NSString *result = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        
        NSLog(@"Response: %@", result);
        
        NSError *e = nil;
        NSMutableArray *jsonArray = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: &e];
        
        
        //    NSMutableArray *resultArray = (NSMutableArray *)result;
        if (complete) complete(jsonArray);
        
        
        /* end my code */
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    [operation start];
    
    
}

+(void)getNumAlertsForUser:(NSInteger)userID
              onCompletion:(RequestDictionaryCompletionHandler)complete
{
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/matches/new/get_num_alerts.php?user_id=%ld", userID];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableDictionary *resultArray = (NSMutableDictionary *)result;
            if (complete) complete(resultArray);
        }
    }];
    
    
    
}


+(void)getNewMessagesForUser:(NSInteger)userID
                onCompletion:(RequestArrayCompletionHandler)complete
{
    NSString *detailPath = [NSString stringWithFormat:@"sc/matches/match_history/get_new_messages.php?user_id=%ld", userID];
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *results = (NSMutableArray *)result;
            if (complete) complete(results);
        }
    }];
}

+(void)getOpenMatchesForUser:(NSInteger)userID
                onCompletion:(RequestArrayCompletionHandler)complete
{
    NSString *detailPath = [NSString stringWithFormat:@"sc/matches/match_history/get_open_matches.php?user_id=%ld", userID];
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *results = (NSMutableArray *)result;
            if (complete) complete(results);
        }
    }];
}

// same thing as getOpenMatchesForUser but filters by a user (only shows the matches that filter user and session user are both involved in)
+(void)getOpenMatchesForUser:(NSInteger)userID
                filterByUser:(NSInteger)filterUserID
                onCompletion:(RequestArrayCompletionHandler)complete
{
    NSString *detailPath = [NSString stringWithFormat:@"sc/matches/match_history/get_open_matches_filter_by_user.php?user_id=%ld&filter_user_id=%ld", userID, filterUserID];
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *results = (NSMutableArray *)result;
            if (complete) complete(results);
        }
    }];
}

/************************************ COMPETITIONS ************************************************************************************/

+(void)registerForCompetitionUser:(NSInteger)userID
                 withUniversityID:(NSInteger)universityID
                   andSchoolEmail:(NSString *)schoolEmail
            andSchoolOrganization:(NSString *)schoolOrganization
                     onCompletion:(RequestDictionaryCompletionHandler)complete
{
    NSString *baseURL = [NSString stringWithFormat:@"%@/", BASE_URL];
    NSString *detailPath = [NSString stringWithFormat:@"%@sc/competitions/register.php/", baseURL];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:[NSString stringWithFormat:@"%ld", userID] forKey:@"user_id"];
    [parameters setValue:[NSString stringWithFormat:@"%ld", universityID] forKey:@"university_id"];
    [parameters setValue:[schoolEmail URLEncode] forKey:@"school_email"];
    [parameters setValue:[schoolOrganization URLEncode] forKey:@"school_organization"];
    
    
    [IOSRequest requestPathAF2:baseURL numTimes:NUM_RETRIES_UPLOAD withDetailPath:detailPath withParameters:parameters onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableDictionary *results = (NSMutableDictionary *)result;
            if (complete) complete(results);
        }
    }];
    
}


+(void)checkIfRegisteredForCompetitionWithUser:(NSInteger)userID
                                  onCompletion:(RequestDictionaryCompletionHandler)complete
{
    NSString *baseURL = [NSString stringWithFormat:@"%@/", BASE_URL];
    NSString *detailPath = [NSString stringWithFormat:@"%@sc/competitions/check_if_registered.php/", baseURL];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:[NSString stringWithFormat:@"%ld", userID] forKey:@"user_id"];
    
    
    
    [IOSRequest requestPathAF2:baseURL numTimes:NUM_RETRIES_UPLOAD withDetailPath:detailPath withParameters:parameters onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableDictionary *results = (NSMutableDictionary *)result;
            if (complete) complete(results);
        }
    }];
    
}

+(void)getSchoolRankingsForUniversity:(NSInteger)universityID
                         onCompletion:(RequestArrayCompletionHandler)complete
{
    NSString *baseURL = [NSString stringWithFormat:@"%@/", BASE_URL];
    NSString *detailPath = [NSString stringWithFormat:@"%@sc/competitions/get_school_rankings.php/", baseURL];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:[NSString stringWithFormat:@"%ld", universityID] forKey:@"university_id"];
    
    
    
    [IOSRequest requestPathAF2:baseURL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath withParameters:parameters onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *results = (NSMutableArray *)result;
            if (complete) complete(results);
        }
    }];
    
}

// TEAMS

+(void)checkTeamName:(NSString *)teamName
        onCompletion:(RequestDictionaryCompletionHandler)complete
{
    // Encode ARGS for URL
    NSString *teamNameConversion = [teamName URLEncode];
    
    NSString *baseURL = [NSString stringWithFormat:@"%@/", BASE_URL];
    NSString *detailPath = [NSString stringWithFormat:@"%@sc/teams/check_team_name.php/", baseURL];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:teamNameConversion forKey:@"team_name"];
    
    [IOSRequest requestPathAF2:baseURL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath withParameters:parameters onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
    
}

+(void)createTeam:(Team *)nTeam
     onCompletion:(RequestDictionaryCompletionHandler)complete
{
    // Encode ARGS for URL
    NSString *teamNameConversion = [nTeam.teamName URLEncode];
    
    NSString *competitorCodeString = [NSString stringWithFormat:@"%ld", nTeam.captain.userID];  // start by making match creator a competitor
    
    for (Player *object in nTeam.players) {
        competitorCodeString = [competitorCodeString stringByAppendingFormat:@"-%ld", object.userID];
    }
    
    NSString *baseURL = [NSString stringWithFormat:@"%@/", BASE_URL];
    NSString *detailPath = [NSString stringWithFormat:@"%@sc/teams/create_team.php/", baseURL];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:teamNameConversion forKey:@"team_name"];
    [parameters setValue:competitorCodeString forKeyPath:@"members"];
    [parameters setValue:[NSString stringWithFormat:@"%ld", nTeam.captain.userID] forKeyPath:@"creator_id"];
    [parameters setValue:nTeam.picStringName forKeyPath:@"team_logo"];
    
    [IOSRequest requestPathAF2:baseURL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath withParameters:parameters onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
    
}


+(void)getSessionUserTeams:(NSInteger)userID
              onCompletion:(RequestArrayCompletionHandler)complete
{
    // Encode ARGS for URL
    
    
    NSString *baseURL = [NSString stringWithFormat:@"%@/", BASE_URL];
    NSString *detailPath = [NSString stringWithFormat:@"%@sc/teams/get_session_user_teams.php/", baseURL];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    [parameters setValue:[NSString stringWithFormat:@"%ld", userID] forKeyPath:@"user"];
    
    [IOSRequest requestPathAF2:baseURL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath withParameters:parameters onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *results = (NSMutableArray *)result;
            if (complete) complete(results);
        }
    }];
    
}

+(void)getTeamDetails:(NSInteger)teamID
         onCompletion:(RequestDictionaryCompletionHandler)complete
{
    NSString *baseURL = [NSString stringWithFormat:@"%@/", BASE_URL];
    NSString *detailPath = [NSString stringWithFormat:@"%@sc/teams/get_team_details.php/", baseURL];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    [parameters setValue:[NSString stringWithFormat:@"%ld", teamID] forKeyPath:@"team"];
    
    [IOSRequest requestPathAF2:baseURL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath withParameters:parameters onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
    
}

+(void)joinTeam:(NSInteger)teamID
       withUser:(NSInteger)userID
   onCompletion:(RequestDictionaryCompletionHandler)complete
{
    NSString *baseURL = [NSString stringWithFormat:@"%@/", BASE_URL];
    NSString *detailPath = [NSString stringWithFormat:@"%@sc/teams/join_team.php/", baseURL];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    [parameters setValue:[NSString stringWithFormat:@"%ld", teamID] forKeyPath:@"team"];
    [parameters setValue:[NSString stringWithFormat:@"%ld", userID] forKeyPath:@"user"];
    
    [IOSRequest requestPathAF2:baseURL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath withParameters:parameters onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
    
}

+(void)leaveTeam:(NSInteger)teamID
        withUser:(NSInteger)userID
    onCompletion:(RequestDictionaryCompletionHandler)complete
{
    NSString *baseURL = [NSString stringWithFormat:@"%@/", BASE_URL];
    NSString *detailPath = [NSString stringWithFormat:@"%@sc/teams/leave_team.php/", baseURL];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    [parameters setValue:[NSString stringWithFormat:@"%ld", teamID] forKeyPath:@"team"];
    [parameters setValue:[NSString stringWithFormat:@"%ld", userID] forKeyPath:@"user"];
    
    [IOSRequest requestPathAF2:baseURL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath withParameters:parameters onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
    
}
// must be captain to delete team
+(void)deleteTeam:(NSInteger)teamID
           byUser:(NSInteger)userID
     onCompletion:(RequestDictionaryCompletionHandler)complete
{
    NSString *baseURL = [NSString stringWithFormat:@"%@/", BASE_URL];
    NSString *detailPath = [NSString stringWithFormat:@"%@sc/teams/delete_team.php/", baseURL];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    [parameters setValue:[NSString stringWithFormat:@"%ld", teamID] forKeyPath:@"team"];
    [parameters setValue:[NSString stringWithFormat:@"%ld", userID] forKeyPath:@"user"];
    
    [IOSRequest requestPathAF2:baseURL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath withParameters:parameters onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
    
}

+(void)getUninvitedContacts:(NSInteger)userID
                    forTeam:(NSInteger)teamID
               onCompletion:(RequestArrayCompletionHandler)complete
{
    NSString *baseURL = [NSString stringWithFormat:@"%@/", BASE_URL];
    NSString *detailPath = [NSString stringWithFormat:@"%@sc/teams/get_uninvited_contacts.php/", baseURL];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    [parameters setValue:[NSString stringWithFormat:@"%ld", teamID] forKeyPath:@"team"];
    [parameters setValue:[NSString stringWithFormat:@"%ld", userID] forKeyPath:@"user"];
    
    [IOSRequest requestPathAF2:baseURL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath withParameters:parameters onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *results = (NSMutableArray *)result;
            if (complete) complete(results);
        }
    }];
    
}

+(void)setTeamMembersForTeam:(NSInteger)teamID
                 withMembers:(NSMutableArray *)memberArray
                  andCaptain:(NSInteger)captainID
                onCompletion:(RequestDictionaryCompletionHandler)complete
{
    NSString *competitorCodeString = [NSString stringWithFormat:@"%ld", captainID];  // start by making match creator a competitor
    
    for (TeamPlayer *object in memberArray) {
        competitorCodeString = [competitorCodeString stringByAppendingFormat:@"-%ld", object.userID];
    }
    
    NSString *baseURL = [NSString stringWithFormat:@"%@/", BASE_URL];
    NSString *detailPath = [NSString stringWithFormat:@"%@sc/teams/set_team_members.php/", baseURL];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    [parameters setValue:competitorCodeString forKeyPath:@"members"];
    [parameters setValue:[NSString stringWithFormat:@"%ld", captainID] forKeyPath:@"captain"];
    [parameters setValue:[NSString stringWithFormat:@"%ld", teamID] forKeyPath:@"team"];
    
    [IOSRequest requestPathAF2:baseURL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath withParameters:parameters onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
    
}

// returns the teams that contacts (who session user follows) participates in
+(void)getContactsTeamsForUser:(NSInteger)userID
                  onCompletion:(RequestArrayCompletionHandler)complete

{
    NSString *baseURL = [NSString stringWithFormat:@"%@/", BASE_URL];
    NSString *detailPath = [NSString stringWithFormat:@"%@sc/teams/get_contacts_teams.php/", baseURL];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    [parameters setValue:[NSString stringWithFormat:@"%ld", userID] forKeyPath:@"user"];
    
    [IOSRequest requestPathAF2:baseURL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath withParameters:parameters onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *results = (NSMutableArray *)result;
            if (complete) complete(results);
        }
    }];
    
}

// returns the teams list that session user is captain of
+(void)getSessionUserCaptainTeams:(NSInteger)userID
                     onCompletion:(RequestArrayCompletionHandler)complete
{
    // Encode ARGS for URL
    
    
    NSString *baseURL = [NSString stringWithFormat:@"%@/", BASE_URL];
    NSString *detailPath = [NSString stringWithFormat:@"%@sc/teams/get_session_user_captain_teams.php/", baseURL];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    [parameters setValue:[NSString stringWithFormat:@"%ld", userID] forKeyPath:@"user"];
    
    [IOSRequest requestPathAF2:baseURL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath withParameters:parameters onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *results = (NSMutableArray *)result;
            if (complete) complete(results);
        }
    }];
    
}

+(void)getAllTeamSportsonCompletion:(RequestArrayCompletionHandler)complete
{
    // Encode ARGS for URL
    
    NSString *baseURL = [NSString stringWithFormat:@"%@/", BASE_URL];
    NSString *detailPath = [NSString stringWithFormat:@"%@sc/teams/get_team_sports.php/", baseURL];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    [parameters setValue:[NSString stringWithFormat:@"%d", 0] forKeyPath:@"user"];
    
    
    [IOSRequest requestPathAF2:baseURL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath withParameters:parameters onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *results = (NSMutableArray *)result;
            if (complete) complete(results);
        }
    }];
    
}


+ (void)createTeamMatchWithInfo:(TeamMatch *)tMatch
                   onCompletion:(RequestDictionaryCompletionHandler)complete
{
    NSString *matchName     = [tMatch.matchName URLEncode];
    NSString *matchCreatorID = [NSString stringWithFormat:@"%ld", tMatch.matchCreatorID];
    NSString *matchMessage  = [tMatch.message URLEncode];
    NSInteger isPrivate     = (tMatch.private) ? 1 : 0;
    NSString *matchType     = [NSString stringWithFormat:@"%ld", tMatch.matchType.matchTypeID];
    
    //    NSString *sportName     = [iMatch.sport.sportName URLEncode];
    NSInteger sportID = tMatch.sport.sportID;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    
    NSString *dateTime = [[formatter stringFromDate: tMatch.dateTime] URLEncode];
    
    NSInteger venueID       = tMatch.venue.venueID;
    
    
    // Rather than use JSON, I'm going to make a string of all competitor teamID's where they are
    // marked by a "-" before the number.
    
    //  NSString *competitorCodeString = [NSString stringWithFormat:@"%@", matchCreatorID];  // start by making match creator a competitor
    
    //   competitorCodeString = [competitorCodeString stringByAppendingFormat:@"-%ld", tMatch.team2.teamID];
    
    
    NSString *baseURL = [NSString stringWithFormat:@"%@/", BASE_URL];
    NSString *detailPath = [NSString stringWithFormat:@"%@sc/teams/team_matches/save_team_match.php/", baseURL];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    [parameters setValue:matchCreatorID forKeyPath:@"creator_id"];
    [parameters setValue:matchName forKey:@"match_name"];
    [parameters setValue:matchMessage forKey:@"message"];
    [parameters setValue:[NSString stringWithFormat:@"%ld", isPrivate] forKey:@"isprivate"];
    [parameters setValue:[NSString stringWithFormat:@"%ld", sportID] forKey:@"sport_id"];
    [parameters setValue:dateTime forKey:@"datetime"];
    [parameters setValue:[NSString stringWithFormat:@"%ld", venueID] forKey:@"venue_id"];
    // [parameters setValue:competitorCodeString forKey:@"compcode"];
    [parameters setValue:matchType forKey:@"match_type"];
    [parameters setValue:[NSString stringWithFormat:@"%ld", tMatch.team1.teamID] forKey:@"team1"];
    [parameters setValue:[NSString stringWithFormat:@"%ld", tMatch.team2.teamID] forKey:@"team2"];
    
    
    [IOSRequest requestPathAF2:baseURL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath withParameters:parameters onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
    
}

+ (void)fetchTeamMatchDetails:(NSInteger)matchID
                 onCompletion:(RequestDictionaryCompletionHandler)complete
{
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/teams/team_matches/get_team_match_info.php?match_id=%ld", matchID];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *resultArray = (NSDictionary *)result;
            if (complete) complete(resultArray);
        }
    }];
}

// alerts

+ (void)fetchAlertsForUser:(NSInteger)userID
              onCompletion:(RequestDictionaryCompletionHandler)complete
{
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/alerts/get_player_alerts.php?user_id=%ld", userID];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *resultArray = (NSDictionary *)result;
            if (complete) complete(resultArray);
        }
    }];
}

+ (void)fetchNumUnreadAlertsForUser:(NSInteger)userID
                       onCompletion:(RequestDictionaryCompletionHandler)complete
{
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/alerts/get_num_unread_alerts.php?user_id=%ld", userID];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *resultArray = (NSDictionary *)result;
            if (complete) complete(resultArray);
        }
    }];
}

+ (void)setAlertsAsReadForUser:(NSInteger)userID
                  onCompletion:(RequestDictionaryCompletionHandler)complete
{
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/alerts/set_alerts_as_read_for_user.php?user_id=%ld", userID];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *resultArray = (NSDictionary *)result;
            if (complete) complete(resultArray);
        }
    }];
}

+ (void)approveFollowConnection:(NSInteger)sessionUserID
                     withLeader:(NSInteger)leaderID
                    andFollower:(NSInteger)followerID
                   onCompletion:(RequestDictionaryCompletionHandler)complete
{
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/players/approve_follow_connection.php?session_user_id=%ld&leader_id=%ld&follower_id=%ld", sessionUserID, leaderID, followerID];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *resultArray = (NSDictionary *)result;
            if (complete) complete(resultArray);
        }
    }];
}

// Privacy Settings

// If the approval flag is set to YES, then any user who tries to follow user with the userID will have to get approval first
+ (void)setPrivacySettingFollowRequestRequiresApprovalForUser:(NSInteger)userID
                                              andApprovalFlag:(BOOL)approvalFlag
                                                 onCompletion:(RequestDictionaryCompletionHandler)complete
{
    NSInteger appFl = 0;
    
    if (approvalFlag)
        appFl = 1;
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/settings/save_privacy_settings.php?user_id=%ld&follow_requires_approval_flag=%ld", userID, appFl];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *resultArray = (NSDictionary *)result;
            if (complete) complete(resultArray);
        }
    }];
}

+ (void)getPrivacySettingsForUser:(NSInteger)userID
                     onCompletion:(RequestDictionaryCompletionHandler)complete
{
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/settings/get_privacy_settings.php?user_id=%ld", userID];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *resultArray = (NSDictionary *)result;
            if (complete) complete(resultArray);
        }
    }];
}

// LOCKER

+ (void)getLockerTagsWithType:(NSInteger)tagType
                 onCompletion:(RequestArrayCompletionHandler)complete
{
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/locker/get_tags.php?tag_type=%ld", tagType];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *resultArray = (NSMutableArray *)result;
            if (complete) complete(resultArray);
        }
    }];
}

+ (void)getTrainingTagsForSport:(NSInteger)sportID
                   onCompletion:(RequestArrayCompletionHandler)complete
{
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/locker/get_training_tags.php?sport_id=%ld", sportID];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *resultArray = (NSMutableArray *)result;
            if (complete) complete(resultArray);
        }
    }];
}

+ (void)getStatTagsForSport:(NSInteger)sportID
               onCompletion:(RequestArrayCompletionHandler)complete
{
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/locker/get_stat_tags.php?sport_id=%ld", sportID];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *resultArray = (NSMutableArray *)result;
            if (complete) complete(resultArray);
        }
    }];
}



+ (void)getVenuesWithSimilarNamesTo:(NSString *)venueNameFragment
                       onCompletion:(RequestArrayCompletionHandler)complete
{
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/search/search_for_venues.php?fragment=%@", [venueNameFragment URLEncode]];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *resultArray = (NSMutableArray *)result;
            if (complete) complete(resultArray);
        }
    }];
}


+(void)saveLockerPostWithImage:(NSString *)imageName
                     createdBy:(NSInteger)creatorID
                    withSeason:(NSString *)seasonTag
                       andYear:(NSString *)yearTag
                      andSport:(NSInteger)sportID
                      andVenue:(NSInteger)venueID
                  andVenueName:(NSString *)venueName
                andTagContacts:(NSMutableArray *)tagContacts
              andShareContacts:(NSMutableArray *)shareContacts
                    shareToAll:(BOOL)shareToAll
                  andStatsTags:(NSMutableArray *)statsTags
               andTrainingTags:(NSMutableArray *)trainingTags
                andUserCaption:(NSString *)userCaption
                   andEventTag:(EventTag *)event
                  onCompletion:(RequestDictionaryCompletionHandler)complete
{
    // Encode ARGS for URL
    
    NSString *tagContactsString = @"";
    NSString *shareContactsString = @"";
    NSString *trainingTagsString = @"";
    NSString *statsTagsString  = @"";

    // make code for tag contacts
    int counter = 0;
    
    for (Player *object in tagContacts) {
        if (counter == 0)
            tagContactsString = [NSString stringWithFormat:@"%ld", object.userID];
        else
            tagContactsString = [tagContactsString stringByAppendingFormat:@"-%ld", object.userID];
        
        counter ++;
    }
    
    // make code for share contacts
    
    int counter2 = 0;
    
    for (Player *object2 in shareContacts) {
        if (counter2 == 0)
            shareContactsString = [NSString stringWithFormat:@"%ld", object2.userID];
        else
            shareContactsString = [shareContactsString stringByAppendingFormat:@"-%ld", object2.userID];
        
        counter2++;
    }
    
    NSInteger shareToAllInt = 0;
    
    if (shareToAll)
        shareToAllInt = 1;
    
    
    NSString *baseURL = [NSString stringWithFormat:@"%@/", BASE_URL];
    NSString *detailPath = [NSString stringWithFormat:@"%@sc/locker/save_locker_post.php/", baseURL];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:[imageName URLEncode] forKey:@"image_name"];
    [parameters setValue:[NSString stringWithFormat:@"%ld", creatorID] forKey:@"creator_id"];
    [parameters setValue:[seasonTag URLEncode] forKey:@"season"];
    [parameters setValue:[yearTag URLEncode] forKey:@"year"];
    [parameters setValue:[NSString stringWithFormat:@"%ld", sportID] forKey:@"sport_id"];
    [parameters setValue:[NSString stringWithFormat:@"%ld", venueID] forKey:@"venue_id"];
    [parameters setValue:[venueName URLEncode] forKey:@"venue_name"];
    [parameters setValue:[tagContactsString URLEncode] forKey:@"tag_contacts"];
    [parameters setValue:[shareContactsString URLEncode] forKey:@"share_contacts"];
    [parameters setValue:[NSString stringWithFormat:@"%ld", shareToAllInt] forKey:@"share_to_all"];
    [parameters setValue:[userCaption URLEncode] forKey:@"text_caption"];
    [parameters setValue:[NSString stringWithFormat:@"%ld", event.eventID] forKeyPath:@"event_id"];
    [parameters setValue:[event.eventName URLEncode] forKeyPath:@"event_name"];
    
    
    // make code for training tags
    int counter3 = 0;
    
    for (TrainingTag *object in trainingTags) {
        if (counter3 == 0)
            trainingTagsString = [NSString stringWithFormat:@"%ld", object.trainingTagID];
        else
            trainingTagsString = [trainingTagsString stringByAppendingFormat:@"-%ld", object.trainingTagID];
        
        counter3 ++;
    }
    [parameters setValue:trainingTagsString forKey:@"training_tags"];
    
    
    // make code for stats
    int counter4 = 0;
    
    for (StatTag *object in statsTags) {
        if (counter4 == 0) {
            statsTagsString = [NSString stringWithFormat:@"%ld^%@", object.statTagID, object.statTagValue];
        } else {
            statsTagsString = [statsTagsString stringByAppendingFormat:@"-%ld^%@", object.statTagID, object.statTagValue];
        }
        counter4 ++;
    }
    [parameters setValue:[statsTagsString URLEncode] forKey:@"stats_tags"];
    
    
    [IOSRequest requestPathAF2:baseURL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath withParameters:parameters onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
    
}

+ (void)fetchLockerAlbumCovers:(NSInteger)userID
              andSessionUserID:(NSInteger)sessionUserID
                   withPageNum:(NSInteger)pageNum
                       andSpan:(NSInteger)spanCells
                  onCompletion:(RequestDictionaryCompletionHandler)complete
{
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/players/get_locker_feed.php?user_id=%ld&page_num=%ld&span_cells=%ld&session_user_id=%ld", userID, pageNum, spanCells, sessionUserID];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *resultArray = (NSDictionary *)result;
            if (complete) complete(resultArray);
        }
    }];
}

+ (void)fetchLockerPicsForSport:(NSInteger)sportID
                        andYear:(NSInteger)picsYear
                      andSeason:(NSString *)picsSeason
                        forUser:(NSInteger)userID
                 andSessionUser:(NSInteger)sessionUserID
                   onCompletion:(RequestArrayCompletionHandler)complete
{
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/locker/get_locker_pics.php?sport_id=%ld&pics_year=%ld&pics_season=%@&user=%ld&session_user=%ld", sportID, picsYear, [picsSeason URLEncode], userID, sessionUserID];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *resultArray = (NSMutableArray *)result;
            if (complete) complete(resultArray);
        }
    }];
}

+ (void)fetchLockerPicDetail:(NSInteger)lockerID
               sessionUserID:(NSInteger)sessionUserID
                onCompletion:(RequestDictionaryCompletionHandler)complete
{
    NSString *detailPath = [NSString stringWithFormat:@"sc/locker/get_locker_pic_detail.php?locker_id=%ld&session_user_id=%ld", lockerID, sessionUserID];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
}

+ (void)makeLockerPostAlbumCover:(NSInteger)lockerID
                        forSport:(NSInteger)sportID
                       andSeason:(NSString *)season
                         andYear:(NSInteger)year
                forsessionUserID:(NSInteger)sessionUserID
                    onCompletion:(RequestDictionaryCompletionHandler)complete
{
    NSString *detailPath = [NSString stringWithFormat:@"sc/locker/make_sports_locker_cover.php?locker_id=%ld&user_id=%ld&sport_id=%ld&season=%@&year=%ld", lockerID, sessionUserID, sportID, [season URLEncode], year];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
}

+ (void)removeLockerUserTagForUser:(NSInteger)userID
                         andlocker:(NSInteger)lockerID
                      onCompletion:(RequestDictionaryCompletionHandler)complete
{
    NSString *baseURL = [NSString stringWithFormat:@"%@/", BASE_URL];
    NSString *detailPath = [NSString stringWithFormat:@"%@sc/locker/remove_user_tag.php/", baseURL];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:[NSString stringWithFormat:@"%ld", userID] forKey:@"user_id"];
    [parameters setValue:[NSString stringWithFormat:@"%ld", lockerID] forKey:@"locker_id"];
    
    [IOSRequest requestPathAF2:baseURL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath withParameters:parameters onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
    
}

// sport is not taken into account when fetching a user's events
+ (void)fetchUserEvents:(NSInteger)userID
           onCompletion:(RequestArrayCompletionHandler)complete
{
    NSString *detailPath = [NSString stringWithFormat:@"sc/locker/get_user_events.php?user_id=%ld", userID];
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *results = (NSMutableArray *)result;
            if (complete) complete(results);
        }
    }];
}

+ (void)fetchSimilarEvents:(NSString *)stringFragment
                  forSport:(NSInteger)sportID
           onCompletion:(RequestArrayCompletionHandler)complete
{
    NSString *detailPath = [NSString stringWithFormat:@"sc/search/search_for_events.php?sport_id=%ld&fragment=%@", sportID, [stringFragment URLEncode]];
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableArray *results = (NSMutableArray *)result;
            if (complete) complete(results);
        }
    }];
}

// Kickstarter

+ (void)getKickstarterInfo:(NSInteger)userID
              onCompletion:(RequestDictionaryCompletionHandler)complete
{
    NSString *detailPath = [NSString stringWithFormat:@"sc/kickstarter.php?user_id=%ld", userID];
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
}

/*
 *=================================New Function Added After Jun 26th==========================
 * @Add by Xin
 *============================================================================================
 */
// compare the user from address book with the user from vaiden database by their username
+ (void)checkAddressBookFriendsAgainstVaidenDBByName:(NSInteger)userID
                                         withFriends:(NSDictionary *)friendsDictionary
                                        onCompletion:(RequestArrayCompletionHandler)complete
{
    NSString *detailPage = [NSString stringWithFormat:@"sc/players/check_addressbook_friends.php"];
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:BASE_URL]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:detailPage
                                                      parameters:friendsDictionary];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [httpClient registerHTTPOperationClass:[AFHTTPRequestOperation class]];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {

        NSString *result = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSLog(@"Response: %@", result);
        
        NSError *e = nil;
        NSMutableArray *jsonArray = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: &e];
        
        if (complete)
            complete(jsonArray);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    [operation start];
}

// Send invite email to the contact saved in the contacts book
+ (void)sendInviteEmailToFriends:(NSMutableArray *)friends
                           from:(NSInteger)userID
                   onCompletion:(RequestDictionaryCompletionHandler)complete
{
    NSString *baseURL = [NSString stringWithFormat:@"%@/", BASE_URL];
    NSString *detailPath = [NSString stringWithFormat:@"%@sc/players/send_invite_email.php/", baseURL];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:friends forKey:@"friends"];
    [parameters setValue:[NSNumber numberWithInteger:userID] forKey:@"user_id"];
    
    [IOSRequest requestPathAF2:baseURL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath withParameters:parameters onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
    
}

// compare the user from address book with the user from vaiden database by their username
+ (void)checkTwitterFriendsAgainstVaidenDBByName:(NSInteger)userID
                                         withFriends:(NSDictionary *)friendsDictionary
                                        onCompletion:(RequestArrayCompletionHandler)complete
{
    NSString *detailPage = [NSString stringWithFormat:@"sc/players/check_twitter_friends.php"];
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:BASE_URL]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:detailPage
                                                      parameters:friendsDictionary];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [httpClient registerHTTPOperationClass:[AFHTTPRequestOperation class]];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSString *result = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSLog(@"Response: %@", result);
        
        NSError *e = nil;
        NSMutableArray *jsonArray = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: &e];
        
        if (complete)
            complete(jsonArray);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    [operation start];
}

+ (void)saveReferralCode:(NSString *)code
                  ofUser:(NSUInteger)userID
            onCompletion:(RequestDictionaryCompletionHandler)complete
{
    
    NSString *basePath = BASE_URL;
    NSString *detailPath = [NSString stringWithFormat:@"sc/players/save_referral_code.php?code=%@&user_id=%lu",code, userID];
    
    [IOSRequest requestPathAF:basePath numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];

}

+ (void)getReferralCodeForUSer:(NSUInteger)userID
                  onCompletion:(RequestDictionaryCompletionHandler)complete
{
    
    NSString *basePath = BASE_URL;
    NSString *detailPath = [NSString stringWithFormat:@"sc/players/get_referral_code.php?user_id=%lu",userID];
    
    [IOSRequest requestPathAF:basePath numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
    
}

+ (void)getTotalVoteForUser:(NSUInteger)userID
                  onCompletion:(RequestDictionaryCompletionHandler)complete
{
    
    NSString *basePath = BASE_URL;
    NSString *detailPath = [NSString stringWithFormat:@"sc/vote/get_user_vote_bank.php?user_id=%lu",userID];
    
    [IOSRequest requestPathAF:basePath numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
    
}

+ (void)voteFromUser:(NSUInteger)userID
              toUser:(NSUInteger)targetID
           withVotes:(NSInteger)votes
               onCompletion:(RequestDictionaryCompletionHandler)complete
{
    
    NSString *basePath = BASE_URL;
    NSString *detailPath = [NSString stringWithFormat:@"sc/vote/vote_for_friends.php?user_id=%lu&target_id=%lu&votes=%d", userID, targetID, votes];
    
    [IOSRequest requestPathAF:basePath numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
    
}

+ (void)thankFromUser:(NSUInteger)userID
               toUser:(NSUInteger)targetID
             forAlert:(NSUInteger)alertID
         onCompletion:(RequestDictionaryCompletionHandler)complete
{
    NSString *basePath = BASE_URL;
    NSString *detailPath = [NSString stringWithFormat:@"sc/vote/thanks_friends.php?user_id=%lu&target_id=%lu&alert_id=%lu", userID, targetID, alertID];
    
    [IOSRequest requestPathAF:basePath numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
    
}

+ (void)getVotesInfoForUser:(NSUInteger)userID
         onCompletion:(RequestDictionaryCompletionHandler)complete
{
    NSString *basePath = BASE_URL;
    NSString *detailPath = [NSString stringWithFormat:@"sc/vote/get_monthly_vote.php?user_id=%lu", userID];
    
    [IOSRequest requestPathAF:basePath numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
    
}

+ (void)getTimeFromServerWithCompletion:(RequestDictionaryCompletionHandler)complete
{
    NSString *basePath = BASE_URL;
    NSString *detailPath = @"sc/vote/timestamp.php";
    
    [IOSRequest requestPathAF:basePath numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
}

+ (void)getLocationForUser:(NSInteger)userID
              onCompletion:(RequestDictionaryCompletionHandler)complete
{
    NSString *basePath = BASE_URL;
    NSString *detailPath = [NSString stringWithFormat:@"sc/vote/getLocationForUser.php?user_id=%lu", userID];
    
    [IOSRequest requestPathAF:basePath numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
}

+ (void)getRankingsInRegion:(NSString *)region
                   withUser:(NSInteger)userID
                 onCompletion:(RequestDictionaryCompletionHandler)complete
{
    NSString *basePath = BASE_URL;
    NSString *detailPath = [NSString stringWithFormat:@"sc/vote/getRankingsInRegions.php?user_id=%lu&region=%@",userID,region];
    
    [IOSRequest requestPathAF:basePath numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
}

+ (void)loadReceivingVotesInRegionForUser:(NSInteger)userID
               onCompletion:(RequestDictionaryCompletionHandler)complete
{
    NSString *basePath = BASE_URL;
    NSString *detailPath = [NSString stringWithFormat:@"sc/vote/getReceivingVotesInRegion.php?user_id=%lu",userID];
    
    [IOSRequest requestPathAF:basePath numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
}

+ (void)loadSupportersForUser:(NSInteger)userID
                     inRegion:(NSString *)region
                       forAll:(BOOL)flag
                 onCompletion:(RequestDictionaryCompletionHandler)complete
{
    NSString *basePath = BASE_URL;
    NSString *detailPath = [NSString stringWithFormat:@"sc/vote/getSupportersInRegion.php?user_id=%lu&all=%d&region=%@",userID,flag?1:0,region];
    
    [IOSRequest requestPathAF:basePath numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
}

+ (void)fetchSupportsForUser:(NSInteger)userID
                      forAll:(BOOL)flag
                 onCompletion:(RequestDictionaryCompletionHandler)complete
{
    NSString *basePath = BASE_URL;
    NSString *detailPath = [NSString stringWithFormat:@"sc/vote/getUserSupports.php?user_id=%lu&all=%d",userID,flag?1:0];
    
    [IOSRequest requestPathAF:basePath numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
}

+ (void)getVoteDetailForUser:(NSInteger)userID
                onCompletion:(RequestDictionaryCompletionHandler)complete
{
    NSString *basePath = BASE_URL;
    NSString *detailPath = [NSString stringWithFormat:@"sc/vote/getUserVoteDetail.php?user_id=%lu",userID];
    
    [IOSRequest requestPathAF:basePath numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
}

+ (void)saveExperenceForUser:(NSInteger)userID
          withData:(NSDictionary *)data
                onCompletion:(RequestArrayCompletionHandler)complete
{
    NSString *detailPage = [NSString stringWithFormat:@"sc/players/save_experience.php"];
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:BASE_URL]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:detailPage
                                                      parameters:data];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [httpClient registerHTTPOperationClass:[AFHTTPRequestOperation class]];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSString *result = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSLog(@"Response: %@", result);
        
        NSError *e = nil;
        NSMutableArray *jsonArray = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: &e];
        
        if (complete)
            complete(jsonArray);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    [operation start];
}

+ (void)saveAwardForUser:(NSInteger)userID
                    withData:(NSDictionary *)data
                onCompletion:(RequestArrayCompletionHandler)complete
{
    NSString *detailPage = [NSString stringWithFormat:@"sc/players/save_award.php"];
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:BASE_URL]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:detailPage
                                                      parameters:data];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [httpClient registerHTTPOperationClass:[AFHTTPRequestOperation class]];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSString *result = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSLog(@"Response: %@", result);
        
        NSError *e = nil;
        NSMutableArray *jsonArray = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: &e];
        
        if (complete)
            complete(jsonArray);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    [operation start];
}

+ (void)fetchExperienceForUser:(NSInteger)userID
                onCompletion:(RequestDictionaryCompletionHandler)complete
{
    NSString *basePath = BASE_URL;
    NSString *detailPath = [NSString stringWithFormat:@"sc/players/getExperienceAndAwards.php?user_id=%lu&type=e",userID];
    
    [IOSRequest requestPathAF:basePath numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
}

+ (void)fetchAwardForUser:(NSInteger)userID
                          onCompletion:(RequestDictionaryCompletionHandler)complete
{
    NSString *basePath = BASE_URL;
    NSString *detailPath = [NSString stringWithFormat:@"sc/players/getExperienceAndAwards.php?user_id=%lu&type=a",userID];
    
    [IOSRequest requestPathAF:basePath numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
}

+ (void)saveVotesForUser:(NSInteger)userID
               withVotes:(NSInteger)votes
                andPrice:(CGFloat)price
            onCompletion:(RequestDictionaryCompletionHandler)complete
{
    NSString *basePath = BASE_URL;
    NSString *detailPath = [NSString stringWithFormat:@"sc/vote/saveVotesForUser.php?user_id=%lu&votes=%ld&price=%.2f",userID, (long)votes, price];
    
    [IOSRequest requestPathAF:basePath numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
}

+ (void)saveBoostForUser:(NSInteger)userID
               withType:(NSInteger)type
                andPrice:(CGFloat)price
            onCompletion:(RequestDictionaryCompletionHandler)complete
{
    NSString *basePath = BASE_URL;
    NSString *detailPath = [NSString stringWithFormat:@"sc/vote/saveBoostForUser.php?user_id=%lu&boost=%d&price=%.2f",userID, type, price];
    
    [IOSRequest requestPathAF:basePath numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
}

+ (void)getBoostStatusForUser:(NSInteger)userID
            onCompletion:(RequestDictionaryCompletionHandler)complete
{
    NSString *basePath = BASE_URL;
    NSString *detailPath = [NSString stringWithFormat:@"sc/vote/getBoostStatusForUser.php?user_id=%lu",userID];
    
    [IOSRequest requestPathAF:basePath numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
}

+ (void)checkBoostedUserViewed:(NSInteger)userID
                 onCompletion:(RequestDictionaryCompletionHandler)complete
{
    NSString *basePath = BASE_URL;
    NSString *detailPath = [NSString stringWithFormat:@"sc/vote/checkBoostedUserViewed.php?user_id=%lu",userID];
    
    [IOSRequest requestPathAF:basePath numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *results = (NSDictionary *)result;
            if (complete) complete(results);
        }
    }];
}

/*
 *=================================New Function Added After July 24th==========================
 * @Add by Fangzhou
 *============================================================================================
 */
// Can be used to fetch charity info in Vaiden App


+(void)displayCharity:(NSInteger)sessionUserID
         onCompletion:(RequestDictionaryCompletionHandler)complete
{
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/get_user_charity.php?user_id=%lu",sessionUserID];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableDictionary *resultArray = (NSMutableDictionary *)result;
            if (complete) complete(resultArray);
        }
    }];
    
}

+(void)setCharity:(NSInteger)sessionUserID
                   charity:(NSString *)charity
                 onCompletion:(RequestDictionaryCompletionHandler)complete
{

    NSString *str = [NSString stringWithFormat:@"sc/set_charity.php?user_id=%ld&charity=%@", sessionUserID, charity];
    
    NSString *detailPath = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSMutableDictionary *resultArray = (NSMutableDictionary *)result;
            if (complete) complete(resultArray);
        }
    }];
    
}

+ (void)setOptSettingsForUser:(NSInteger)userID
                                              andApprovalFlag:(BOOL)approvalFlag
                                                 onCompletion:(RequestDictionaryCompletionHandler)complete
{
    NSInteger appFl = 0;
    
    if (approvalFlag)
        appFl = 1;
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/settings/set_opt_settings.php?user_id=%ld&approval_flag=%ld", userID, appFl];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *resultArray = (NSDictionary *)result;
            if (complete) complete(resultArray);
        }
    }];
}

+ (void)getOptSettingsForUser:(NSInteger)userID
                     onCompletion:(RequestDictionaryCompletionHandler)complete
{
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/settings/get_opt_settings.php?user_id=%ld", userID];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *resultArray = (NSDictionary *)result;
            if (complete) complete(resultArray);
        }
    }];
}
+ (void)getLoginNums:(NSInteger)userID
        onCompletion:(RequestDictionaryCompletionHandler)complete;
{
    
    NSString *detailPath = [NSString stringWithFormat:@"sc/settings/get_login.php?user_id=%ld", userID];
    
    
    [IOSRequest requestPathAF:BASE_URL numTimes:NUM_RETRIES_COMMUNICATE withDetailPath:detailPath onCompletion:^(NSString *result, NSError *error) {
        if (error) {
            if (complete) complete(nil);
        } else {
            NSDictionary *resultArray = (NSDictionary *)result;
            if (complete) complete(resultArray);
        }
    }];
}

@end
