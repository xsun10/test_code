//
//  Team.m
//  Phenom
//
//  Created by James Chung on 4/3/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "Team.h"

@implementation Team

- (Team *)initWithTeamDetails:(NSInteger)teamID
                  andTeamName:(NSString *)teamName
                   andCaptain:(TeamPlayer *)teamCaptain
                 andPicString:(NSString *)picString


{
    self = [super init];
    
    if (self) {
        _teamID = teamID;
        _teamName = teamName;
        _captain = teamCaptain;
        _picStringName = picString;
        _players = [[NSMutableArray alloc] init];
        
    }
    return self;
}

- (void)loadTeamMembersFromServerObject:(id)serverArray
{
    for (id object in serverArray) {
        TeamPlayer *p = [[TeamPlayer alloc] initWithTeamPlayerDetails:[object[@"competitor_id"] integerValue]
                                              andUserName:object[@"username"]
                                      andProfilePicString:object[@"profile_pic_string"]
                                                  andMemberStatus:object[@"member_status"]];
        [self.players addObject:p];
    }
}

- (void)setCaptainFromTeamMembersArray:(NSInteger)captainID
{
    for (TeamPlayer *object in self.players) {
        if (object.userID == captainID) {
            _captain = [[TeamPlayer alloc] initWithTeamPlayerDetails:object.userID
                                                         andUserName:object.userName
                                                 andProfilePicString:object.profileBaseString
                                                     andMemberStatus:object.memberStatus];
            break;
        }
    }
}

- (NSInteger) getNumConfirmedMembers
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"memberStatus = %@", @"CONFIRMED"];
    return [[self.players filteredArrayUsingPredicate:predicate] count];
}

- (BOOL) isSessionUserAlreadyConfirmedMember:(NSInteger)sessionUserID
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"memberStatus = %@ AND userID = %ld", @"CONFIRMED", sessionUserID];
    if ([[self.players filteredArrayUsingPredicate:predicate] count])
        return YES;
    
    return NO;
}

- (BOOL) isSessionUserUnconfirmedAndAllowedToJoinTeam:(NSInteger)sessionUserID
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"memberStatus = %@ AND userID = %ld", @"UNCONFIRMED", sessionUserID];
    if ([[self.players filteredArrayUsingPredicate:predicate] count])
        return YES;
    
    return NO;
}
@end
