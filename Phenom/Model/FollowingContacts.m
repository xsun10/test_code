//
//  FollowingContacts.m
//  Phenom
//
//  Created by James Chung on 3/26/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "FollowingContacts.h"
#import "IOSRequest.h"
#import "PlayerSport.h"

@implementation FollowingContacts


- (void)resetFollowingInfo:(NSInteger)userID 
{
 //   __weak FollowingContacts *weakSelf = self;
    /*
    [IOSRequest fetchUserFollowingList:userID  onCompletion:^(NSMutableArray *queryList) {
                
        for(id user in queryList) {
            
            if ([user isKindOfClass:[NSDictionary class]])
            {
                                               
                            Player *player = [[Player alloc] initWithPlayerDetails:[user[@"user_id"] integerValue]
                                                                     andProfileStr:user[@"profile_pic_string"]
                                                                           andUserName:user[@"username"]
                                                                       andFullName:user[@"fullname"]
                                                                           andLocation:user[@"location"]
                                                                          andAbout:user[@"about"]
                                                                          andCoins:[user[@"coins"] floatValue]];
                            if (user[@"sports"])
                            {
                             
                                for (id object in user[@"sports"]) {
                                    PlayerSport *sport = [[PlayerSport alloc] initWithSportDetails:object[@"sport_name"]
                                                                               andType:object[@"sport_type"]
                                                                              andLevel:object[@"level"]
                                                                                    andSportRecord:object[@"win_loss_record"]];
                                    
                                    
                                    if ([object[@"sport_type"] isEqualToString:@"individual"]) {
                                        [player addPlayerSport:sport];
                                    } else if ([object[@"sport_type"] isEqualToString:@"team"]) {
                                        [player addPlayerSport:sport];
                                    }
                                
                                }
                            }
                                [weakSelf.playerArray addObject:player]; //???
                            
                            if ([queryList count] == [weakSelf.playerArray count])
                                [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateContactTable"
                                                                                     object:self
                                                                                   userInfo:nil];
                
            }
        }
        if ([queryList count] == 0)
            [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateContactTable"
                                                                object:self
                                                              userInfo:nil];
    
    }];
*/
}

- (Player *)playerAtIndex:(NSInteger)index
{
    return self.playerArray[index];
}

@end
