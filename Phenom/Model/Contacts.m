//
//  Contacts.m
//  Phenom
//
//  Created by James Chung on 3/26/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "Contacts.h"
#import "Player.h"

@implementation Contacts

// lazy instantiation
- (NSMutableArray *)playerArray
{
    if (!_playerArray) _playerArray = [[NSMutableArray alloc] init];
    return _playerArray;
}

-(NSInteger)numberOfContacts
{
    return [self.playerArray count];
}
@end
