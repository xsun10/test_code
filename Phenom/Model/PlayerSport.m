//
//  PlayerSport.m
//  Phenom
//
//  Created by James Chung on 3/27/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "PlayerSport.h"

@implementation PlayerSport


- (PlayerSport *) initWithSportDetails:(NSInteger)sportID
                               andName:(NSString *)sportName
                               andType:(NSString *)sportType
                              andLevel:(NSString *)level
                        andSportRecord:(id)recordObject
{
    self = [super initWithSportDetails:sportID
                               andName:sportName
                               andType:sportType];
    
    
    if (self) {
        _level = level;
        _playerRecordForSport = [[PlayerRecord alloc] initWithPlayerRecordWins:[recordObject[@"wins"] integerValue]
                                                                     andLosses:[recordObject[@"losses"] integerValue]
                                                                       andTies:[recordObject[@"ties"] integerValue]];
        _avgSportRatingAllAttributes = 0;
    }
    return self;
}

// without sportID
- (PlayerSport *) initWithSportDetails:(NSString *)sportName
                         andType:(NSString *)sportType
                        andLevel:(NSString *)level
                        andSportRecord:(id)recordObject
{
    self = [super initWithSportDetails:sportName
                               andType:sportType];
    
    
    if (self) {
        _level = level;
        
        if ([recordObject isKindOfClass:[NSNull class]]) {
            _playerRecordForSport = [[PlayerRecord alloc] initWithPlayerRecordWins:0 andLosses:0 andTies:0];
        } else {
            _playerRecordForSport = [[PlayerRecord alloc] initWithPlayerRecordWins:[recordObject[@"wins"] integerValue]
                                                                         andLosses:[recordObject[@"losses"] integerValue]
                                                                           andTies:[recordObject[@"ties"] integerValue]];
        }
        _avgSportRatingAllAttributes = 0;
    }
    return self;
}

// have to make copy of object before return or alloc new return obj?

+ (PlayerSport *)getSportObjectForName:(NSString *)sportName inArray:(NSMutableArray *)sportArray
{
    for (PlayerSport *sport in sportArray) {
        if ([sport.sportName isEqualToString:sportName]) {
            return sport;
        }
    }
    return nil;
}



@end

