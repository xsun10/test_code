//
//  PlayerSport.h
//  Phenom
//
//  Created by James Chung on 3/27/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "Sport.h"
#import "PlayerRecord.h"
#import "AvgSportRatingAllAttributes.h"

@interface PlayerSport : Sport


@property (nonatomic, strong) NSString *level;
@property (nonatomic, strong) PlayerRecord *playerRecordForSport;
@property (nonatomic, strong) AvgSportRatingAllAttributes *avgSportRatingAllAttributes;

- (PlayerSport *) initWithSportDetails:(NSInteger)sportID
                               andName:(NSString *)sportName
                               andType:(NSString *)sportType
                              andLevel:(NSString *)level
                        andSportRecord:(id)recordObject;

- (PlayerSport *) initWithSportDetails:(NSString *)sportName
                         andType:(NSString *)sportType
                        andLevel:(NSString *)level
                        andSportRecord:(id)recordObject;

+ (PlayerSport *)getSportObjectForName:(NSString *)sportName inArray:(NSMutableArray *)sportArray;



@end
