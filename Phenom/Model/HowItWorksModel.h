//
//  HowItWorksModel.h
//  Phenom
//
//  Created by James Chung on 3/14/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HowItWorksModel : NSObject

@property (nonatomic) NSInteger currentSlide;

- (NSString *) getNextSlide:(NSString *)direction;
- (NSInteger) currentCount;

@end
