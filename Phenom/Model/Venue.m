//
//  Venue.m
//  Phenom
//
//  Created by James Chung on 4/3/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "Venue.h"



@implementation Venue

-(Venue *)initVenueWithName:(NSString *)venueName
                 andVenueID:(NSInteger)venueID
                 andAddress:(NSString *)streetAddress
                    andCity:(NSString *)city
               andStateName:(NSString *)stateName
                 andStateID:(NSInteger)stateID
                andLatitude:(float)latitude
               andLongitude:(float)longitude
             andDescription:(NSString *)description
                 andCreator:(NSInteger)creatorID
                  andSports:(NSMutableArray *)sportsArray
               andAvgRating:(AvgRating *)theRating
{
    self = [super init];
    
    if (self) {
        _venueName = venueName;
        _venueID    = venueID;
        _streetAddress = streetAddress;
        _city = city;
        _stateName = stateName;
        _stateID = stateID;
        _latitude = latitude;
        _longitude = longitude;
        _description = description;
        _creatorID = creatorID;
        _sportsArray = [[NSMutableArray alloc] initWithArray:sportsArray];
        _picsArray = [[NSMutableArray alloc] init];
        _avgRating = theRating;
        _venueCheckInsArray = [[NSMutableArray alloc] init];
    }
    
    return self;
}

-(Venue *)initVenueWithName:(NSString *)venueName
                 andVenueID:(NSInteger)venueID
            andAddress:(NSString *)streetAddress
               andCity:(NSString *)city
              andStateName:(NSString *)stateName
                 andStateID:(NSInteger)stateID
                andLatitude:(float)latitude
               andLongitude:(float)longitude
             andDescription:(NSString *)description
                 andCreator:(NSInteger)creatorID
{
     self = [super init];
    
    if (self) {
        _venueName = venueName;
        _venueID    = venueID;
        _streetAddress = streetAddress;
        _city = city;
        _stateName = stateName;
        _stateID = stateID;
        _latitude = latitude;
        _longitude = longitude;
        _description = description;
        _creatorID = creatorID;
        _sportsArray = [[NSMutableArray alloc] init];
        _picsArray = [[NSMutableArray alloc] init];
        _venueCheckInsArray = [[NSMutableArray alloc] init];
    }
    
    return self;
}

// A more general initializer
- (Venue *)initVenueWithName:(NSString *)venueName
                  andVenueID:(NSInteger)venueID
{
    self = [super init];
    
    if (self) {
        _venueName = venueName;
        _venueID = venueID;
    }
    
    return self;
}


@end
