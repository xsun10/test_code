//
//  HowItWorksModel.m
//  Phenom
//
//  Created by James Chung on 3/14/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "HowItWorksModel.h"

@interface HowItWorksModel ()
@property (strong, nonatomic) NSArray *slideShowComponents;

@end

@implementation HowItWorksModel

#define MAX_SLIDESHOW_LENGTH 5

- (id)init
{
    self = [super init];
    if (self) {
        if (!_slideShowComponents) _slideShowComponents = @[@"Intro2.jpg", @"Intro3.jpg", @"Intro4.jpg", @"Intro5.jpg", @"Intro6.jpg"];
        self.currentSlide = 0;
    }
    return self;
}



- (NSString *) getNextSlide:(NSString *)direction
{
    if ([direction isEqualToString:@"Right"] && self.currentSlide > 0 ) {
        self.currentSlide --;
    } else if  ([direction isEqualToString:@"Left"] && self.currentSlide < MAX_SLIDESHOW_LENGTH -1) {
        self.currentSlide ++;
    }

    return self.slideShowComponents[self.currentSlide];

}

- (NSInteger) currentCount
{
    return self.currentSlide;
}

@end
