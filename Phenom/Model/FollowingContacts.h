//
//  FollowingContacts.h
//  Phenom
//
//  Created by James Chung on 3/26/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "Contacts.h"
#import "Player.h"


@interface FollowingContacts : Contacts  // NOTE: subclass on Contacts

//- (id) initForUserID:(NSInteger)userID;
- (Player *)playerAtIndex:(NSInteger)index;
- (void)resetFollowingInfo:(NSInteger)userID;
@end
