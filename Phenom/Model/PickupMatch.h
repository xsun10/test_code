//
//  PickupMatch.h
//  Phenom
//
//  Created by James Chung on 4/3/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "Match.h"
#import "MatchType.h"
#import "MatchPlayer.h"

@interface PickupMatch : Match

@property (nonatomic) NSInteger minCompetitors;
@property (nonatomic, strong) MatchType *matchType;
@property (nonatomic, strong) NSString *gender;

@property (strong, nonatomic) NSMutableArray *experienceArray;
@property (strong, nonatomic) NSString *experienceCode;


@property (nonatomic, strong) NSMutableArray *competitorArray; // this is actually a set of Players + their status (see MatchNotificationsTVC)

- (PickupMatch *) initMatchWithDetails:(NSInteger)matchID
                               andMatchName:(NSString *)matchName
                                 isPrivate:(BOOL)isPrivate
                                 withSport:(Sport *)sport
                               withMessage:(NSString *)message
                                onDateTime:(NSDate *)dateTime
                                   atVenue:(Venue *)venue
                             createdByUser:(NSInteger)creatorID
                            minCompetitors:(NSInteger)minCompetitors
                           withCompetitors:(NSMutableArray *)competitorArray
                            withExperience:(NSMutableArray *)experience
                          andMatchType:(MatchType *)matchType
                                 andActive:(BOOL)active
                     andMatchStatus:(NSString *)matchStatus;

- (PickupMatch *) initMatchWithDetails:(NSInteger)matchID
                          andMatchName:(NSString *)matchName
                             isPrivate:(BOOL)isPrivate
                             withSport:(Sport *)sport
                           withMessage:(NSString *)message
                            onDateTime:(NSDate *)dateTime
                               atVenue:(Venue *)venue
                         createdByUser:(NSInteger)creatorID
                          withUsername:(NSString *)matchCreatorUsername
                         andProfileStr:(NSString *)matchCreatorProfileBaseString
                        minCompetitors:(NSInteger)minCompetitors
                       withCompetitors:(NSMutableArray *)competitorArray
                        withExperience:(NSMutableArray *)experience
                          andMatchType:(MatchType *)matchType
                             andActive:(BOOL)active
                     andMatchStatus:(NSString *)matchStatus;

+ (NSMutableArray *)makeCompetitorArray:(NSArray *)competitorResults forSportName:(NSString *)sportName andType:(NSString *)sportType;

//+ (NSString *)getStatus:(PickupMatch *)pMatch sessionUserID:(NSInteger)sessionUserID;

// if player rsvp exceeds the minimum rsvp count
//- (BOOL)isMatchRSVPConfirmed;

- (NSString *)getMatchStatus;

- (NSString *)getSessionUserStatus;

- (NSString *)getSessionUserCodedStatus;

- (NSInteger)getNumConfirmed;

- (BOOL)checkLoggedInUserConfirmedBOOL;


+ (NSString *)getExperienceString:(PickupMatch *)pickupMatch;

- (BOOL)canSessionUserRSVPForMatch;

- (Player *)getMatchCreatorObject;

- (void)loadCompetitorArrayFromObject:(NSArray *)competitorObject;

- (BOOL)wasPlayerInvited:(NSInteger)playerID;

- (MatchPlayer *)getPlayerOtherThanSessionUser:(NSInteger)sessionUserID;

@end
