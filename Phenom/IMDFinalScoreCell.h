//
//  IMDFinalScoreCell.h
//  Vaiden
//
//  Created by James Chung on 12/26/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IndividualMatch.h"

@interface IMDFinalScoreCell : UITableViewCell <UICollectionViewDataSource, UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *usernameLabel1;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel2;
@property (weak, nonatomic) IBOutlet UICollectionView *scoreCV;
@property (weak, nonatomic) IBOutlet UIView *iconBackground;
@property (weak, nonatomic) IBOutlet UIImageView *iconPic;
@property (weak, nonatomic) IBOutlet UIView *myBackgroundView;

@property (nonatomic, strong) IndividualMatch *individualMatch;

@property (nonatomic) NSUInteger player1ID;
@property (nonatomic) NSUInteger player2ID;

@end
