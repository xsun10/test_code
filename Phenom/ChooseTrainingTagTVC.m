//
//  ChooseTrainingTagTVC.m
//  Vaiden
//
//  Created by James Chung on 5/27/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "ChooseTrainingTagTVC.h"
#import "IOSRequest.h"
#import "TrainingTag.h"
#import "UIView+Manipulate.h"

@interface ChooseTrainingTagTVC ()

@property (nonatomic, strong) NSMutableArray *tagsArray;

@end

@implementation ChooseTrainingTagTVC

- (NSMutableArray *)tagsArray
{
    if (!_tagsArray) _tagsArray = [[NSMutableArray alloc] init];
    return _tagsArray;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    [self setDoneButton];
    [self loadTrainingTags];
}

/*- (void)setDoneButton
{
    UIButton *btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 50, 21);
    [btn makeRoundedBorderWithRadius:3];
    btn.layer.borderColor = [UIColor whiteColor].CGColor;
    btn.layer.borderWidth = 1.0;
    
    [btn addTarget:self action:@selector(doneAction:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundColor:[UIColor clearColor]];
    [btn setTitle:@"DONE" forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-MediumItalic" size:12.0]];
    
    
    // Initialize UIBarbuttonitem...
    UIBarButtonItem *proe = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = proe;
}*/

- (void)loadTrainingTags
{
    [self.tagsArray removeAllObjects];
    
    [IOSRequest getTrainingTagsForSport:self.sportID
                           onCompletion:^(NSMutableArray *results) {
                               
                               for (id object in results) {
                                   TrainingTag *tag = [[TrainingTag alloc] initWithTrainingTagInfo:[object[@"tt_id"] integerValue]
                                                                                andTrainingTagName:object[@"tt_name"]];
                                   
                                   [self.tagsArray addObject:tag];
                                   
                               }
                               dispatch_async(dispatch_get_main_queue(), ^{
                                   
                                   [self.tableView reloadData];
                               });
                           }];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.tagsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Tag Cell" forIndexPath:indexPath];
    
    TrainingTag *tag = [self.tagsArray objectAtIndex:indexPath.row];
    cell.textLabel.text = tag.trainingTagName;
    cell.textLabel.font = [UIFont systemFontOfSize:13];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TrainingTag *tag = [self.tagsArray objectAtIndex:indexPath.row];
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    if (tag.isSelected) {
        cell.accessoryType = UITableViewCellAccessoryNone;
        tag.isSelected = NO;
    } else {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        tag.isSelected = YES;
    }
    
    [self.tagsArray replaceObjectAtIndex:indexPath.row withObject:tag];
}
- (IBAction)doneAction2:(UIButton *)sender
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isSelected = %ld", 1];
    NSMutableArray *filteredArray = [NSMutableArray arrayWithArray:[self.tagsArray filteredArrayUsingPredicate:predicate]];
    
    if ([filteredArray count] <= 3) {
        [self.delegate setWithViewController:self withTrainingTags:filteredArray];
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Too Many Routines";
        alert.message = @"Please only choose up to 3 Routines.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
    }

}

@end
