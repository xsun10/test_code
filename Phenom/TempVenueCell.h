//
//  TempVenueCell.h
//  Vaiden
//
//  Created by James Chung on 1/4/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TempVenueCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *chooseButton;
@property (weak, nonatomic) IBOutlet UILabel *venueNameLabel    ;
@property (weak, nonatomic) IBOutlet UILabel *tempVenueFlagLabel;
@property (weak, nonatomic) IBOutlet UILabel *venueDescriptionLabel;

@end
