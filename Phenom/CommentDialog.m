//
//  CommentDialog.m
//  Vaiden
//
//  Created by Turbo on 7/11/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "CommentDialog.h"
#import "UIView+Manipulate.h"
#import "UIColor+VaidenColors.h"

@interface CommentDialog () <UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UIView *window;
@property (weak, nonatomic) IBOutlet UIView *innerWindow;
@property (weak, nonatomic) IBOutlet UILabel *CharacterLimit;
@property (weak, nonatomic) IBOutlet UIButton *postButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topSpace;
@end

@implementation CommentDialog

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.window makeRoundedBorderWithRadius:5];
    [self.innerWindow makeRoundedBorderWithRadius:5];
    [self.postButton makeRoundedBorderWithRadius:3];
    [self.cancelButton makeRoundedBorderWithRadius:3];
    
    [self.textView setDelegate:self];
    UITapGestureRecognizer *singleFingerTap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                      action:@selector(handleSingleTap:)];
    [self.view addGestureRecognizer:singleFingerTap];
    [self.CharacterLimit setText:@"255 characters remaining"];
    
    if ([[UIScreen mainScreen] bounds].size.height <= 480) {
        [self.topSpace setConstant:self.topSpace.constant - 20];
    }
}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
    // Dismiss the keyboard
    [self.textView resignFirstResponder];
}

- (IBAction)postClicked:(id)sender
{
    if ([self checkText:self.textView.text]) {
        [self.delegate startSumitComments:self.textView.text];
        [self dismissViewControllerAnimated:NO completion:nil];
    }
}

- (void)textViewDidChange:(UITextView *)textView
{
    NSInteger remain = 255 - textView.text.length;
    if (remain >= 0) { // character checking
        [self.CharacterLimit setTextColor:[UIColor colorWithRed:170/255.0 green:170/255.0 blue:170/255.0 alpha:1.0]];
        self.CharacterLimit.text = [NSString stringWithFormat:@"%d characters remaining", remain];
    } else { 
        [self.CharacterLimit setTextColor:[UIColor redColor]];
        self.CharacterLimit.text = [NSString stringWithFormat:@"%d characters over", -remain];
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range
 replacementText:(NSString *)text{
    if (range.length==0) {
        if ([text isEqualToString:@"\n"]) {
            [textView resignFirstResponder];
            return NO;
        }
    }
    return YES;
}

- (BOOL)checkText:(NSString *)text
{
    if (text.length == 0) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        if (self.isTextPost) {
            alert.title = @"Text Post Error";
            alert.message = @"Please enter a non-blank text post.";
        } else {
            alert.title = @"Comment Error";
            alert.message = @"Please enter a non-blank comment.";
        }
        [alert addButtonWithTitle:@"OK"];
        [alert show];
        return NO;
    } else if (self.textView.text.length > 255) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        if (self.isTextPost) {
            alert.title = @"Text Post Error";
            alert.message = @"Please enter a text post within 255 characters.";
        } else {
            alert.title = @"Comment Error";
            alert.message = @"Please enter a comment within 255 characters.";
        }
        [alert addButtonWithTitle:@"OK"];
        [alert show];
        return NO;
    }
    return YES;
}

- (IBAction)cancelClicked:(id)sender
{
    [self dismissViewControllerAnimated:NO completion:nil];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
