//
//  MatchNewsScoreUpdateCell.m
//  Vaiden
//
//  Created by James Chung on 10/30/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "MatchNewsScoreUpdateCell.h"
#import "Player.h"
#import "S3Tools.h"
#import "AFNetworking.h"
#import "NewsfeedMatchHeadingCVCell.h"
#import "MatchNewsScoreUpdateScoreCVCell.h"
#import "UIImage+ProportionalFill.h"
#import "IndividualMatchRoundScore.h"

@implementation MatchNewsScoreUpdateCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)layoutSubviews
{
    self.matchHeadingCV.delegate = self;
    self.matchHeadingCV.dataSource = self;
    
    self.scoresCV.delegate = self;
    self.scoresCV.dataSource = self;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    if ([self.matchCategory isEqualToString:@"individual"] && collectionView == self.matchHeadingCV) {
        return 3;
    } else if (collectionView == self.scoresCV) {
        return [self.iMatch.scoreIndMatch.roundScoresIndMatch count];
    }
    
    return [self.competitors count];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    if (collectionView == self.scoresCV)
        return 2;
    
    return 1;
}

- (void)goToProfile:(UIButton *)sender
{
    [self.delegate setMatchNewsScoreUpdateCellParentViewController:self withUserIDToSegue:sender.tag];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    Player *p = nil;
    
    if ([self.matchCategory isEqualToString:@"individual"] && collectionView == self.matchHeadingCV) {
        static NSString *cellIdentifier = @"Competitor Pics";
        
        NewsfeedMatchHeadingCVCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
        
        if (indexPath.row == 1) {
            // show "vs" not the pic
            cell.playerPic.hidden = YES;
            UILabel *vsLabel = [[UILabel alloc] initWithFrame:CGRectMake(8, 0, 40, 40)];
            vsLabel.text = @"VS";
            [vsLabel setFont:[UIFont systemFontOfSize:18]];
            
            [cell addSubview:vsLabel];
            
            
        } else {
            
            
            if (indexPath.row == 2)
                p  = [self.competitors objectAtIndex:1];
            else
                p = [self.competitors objectAtIndex:0];
            
            NSString *urlMain = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                        baseString:p.profileBaseString];
            
            [cell.playerPic setImageWithURL:[NSURL URLWithString:urlMain]
                           placeholderImage:[UIImage imageNamed:@"avatar round"]];
            [UIImage makeRoundedImage:cell.playerPic withRadius:20];
            
            // profile pic button
            UIButton *picButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
            [picButton addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
            picButton.tag = p.userID;
            [cell.playerPic addSubview:picButton];
            cell.playerPic.userInteractionEnabled = YES;
            // end profile pic button

        }
        
        return  cell;
    } else if (collectionView == self.matchHeadingCV){
        
        static NSString *cellIdentifier = @"Competitor Pics";
        
        NewsfeedMatchHeadingCVCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
        
        p = [self.competitors objectAtIndex:indexPath.row];
        NSString *urlMain = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                    baseString:p.profileBaseString];
        
        [cell.playerPic setImageWithURL:[NSURL URLWithString:urlMain]
                       placeholderImage:[UIImage imageNamed:@"avatar round"]];
        [UIImage makeRoundedImage:cell.playerPic withRadius:20];
        
        
        return  cell;
    } else if (collectionView == self.scoresCV) {
        static NSString *cellIdentifier = @"Score Cell";
        MatchNewsScoreUpdateScoreCVCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
        
        IndividualMatchRoundScore *matchScore = [self.iMatch.scoreIndMatch.roundScoresIndMatch objectAtIndex:indexPath.row];
        
        if (indexPath.section == 0) { // first section
            cell.scoreLabel.text = [NSString stringWithFormat:@"%ld", matchScore.scorePlayer1];
            self.player1ScoreLabel.text = [self.iMatch getPlayerForID:matchScore.userIDPlayer1].userName; // I know this is not efficient...doing this for time's sake for now.
        } else if (indexPath.section == 1) { // second section
            cell.scoreLabel.text = [NSString stringWithFormat:@"%ld", matchScore.scorePlayer2];
            self.player2ScoreLabel.text = [self.iMatch getPlayerForID:matchScore.userIDPlayer2].userName; // I know this is not efficient...doing this for time's sake for now.
        }
        
        return cell;
        
    }
    
    return nil;
}
@end
