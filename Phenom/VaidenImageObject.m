//
//  VaidenImageObject.m
//  Vaiden
//
//  Created by James Chung on 5/7/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "VaidenImageObject.h"

@implementation VaidenImageObject


- (VaidenImageObject *) initWithOrigImageData:(NSData *)origImageData
                      andRegularSizeImageData:(NSData *)regularSizeImageData
                andRegularSizeRetinaImageData:(NSData *)regularSizeRetinaImageData
                             andThumbnailData:(NSData *)thumbnailData
                       andRetinaThumbnailData:(NSData *)retinaThumbnailData
                             andFileExtension:(NSString *)fileExtension
                                     andImage:(UIImage *)imageHandle
{
    self = [super init];
    
    if (self) {
        _origImageData = origImageData;
        _regularSizeImageData = regularSizeImageData;
        _regularSizeRetinaImageData = regularSizeRetinaImageData;
        _thumbnailImageData = thumbnailData;
        _thumbnailRetinaImageData = retinaThumbnailData;
        _fileExtension = fileExtension;
        _imageHandle = imageHandle;
            
    }
    return self;
    
}

@end
