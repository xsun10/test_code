//
//  LockerAlbumCover.m
//  Vaiden
//
//  Created by James Chung on 5/10/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "LockerAlbumCover.h"

@implementation LockerAlbumCover

- (LockerAlbumCover *)initWithLockerAlbumCoverDetails:(NSInteger)lockerID
                                           andCreator:(NSInteger)creatorID
                                         andImageName:(NSString *)imageName
                                            andSeason:(NSString *)season
                                              andYear:(NSInteger)year
                                             andSport:(Sport *)sp
                                           andNumPics:(NSInteger)numPics
{
    self = [super init];
    
    if (self) {
        _lockerID = lockerID;
        _creatorID = creatorID;
        _imageName = imageName;
        _season = season;
        _year = year;
        _sp = sp;
        _numPics = numPics;
        
    }
    return self;
}

@end
