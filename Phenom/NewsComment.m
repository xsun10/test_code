//
//  NewsComment.m
//  Vaiden
//
//  Created by James Chung on 9/27/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "NewsComment.h"

@implementation NewsComment

- (NewsComment *)initWithCommentDetails:(NSInteger)commentID
                     andCommentText:(NSString *)commentText
                           byUserID:(NSInteger)userID
                        andUsername:(NSString *)username
                      andProfileStr:(NSString *)profileBaseString
                        forNewsPost:(NSInteger)newsID
                             onDate:(NSDate *)postDate
{
    self = [super initWithCommentDetails:commentID
                          andCommentText:commentText
                                byUserID:userID
                             andUsername:username
                           andProfileStr:profileBaseString
                                  onDate:postDate];
    
    if (self) {
        _newsID = newsID;
    }
    
    return self;
}

@end
