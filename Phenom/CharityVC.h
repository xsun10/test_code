//
//  CharityVC.h
//  Vaiden
//
//  Created by Turbo on 7/23/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "CustomBaseVC.h"
@protocol charityDelegate <NSObject>
-(void)holdUI;
@end
@interface CharityVC : CustomBaseVC <MBProgressHUDDelegate >
{
    MBProgressHUD *HUD;
}
@property (nonatomic, weak) id <charityDelegate>delegate;
@property (weak, nonatomic) IBOutlet UIView *popView;
@property (weak, nonatomic) IBOutlet UIButton *cancel;
@property (weak, nonatomic) IBOutlet UITextField *charityInput;
@property (weak, nonatomic) IBOutlet UIButton *submit;

@end
