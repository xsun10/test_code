//
//  LockerTagCollectionViewCell.h
//  Vaiden
//
//  Created by James Chung on 5/7/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LockerTagCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *tagNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *tagValueLabel;
@property (weak, nonatomic) IBOutlet UIImageView *tagEditIcon;
@property (weak, nonatomic) IBOutlet UIImageView *tagPlusIcon;
@property (weak, nonatomic) IBOutlet UIButton *tagButton;
@property (weak, nonatomic) IBOutlet UILabel *requiredMessageLabel;

@end
