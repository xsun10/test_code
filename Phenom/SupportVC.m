//
//  SupportVC.m
//  Vaiden
//
//  Created by Turbo on 7/22/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "SupportVC.h"
#import "UIView+Manipulate.h"
#import "IOSRequest.h"
#import "UserPreferences.h"
#import "SupportCVC.h"
#import "S3Tools.h"
#import "UIImageView+AFNetworking.h"
#import "UIImage+ProportionalFill.h"
#import "UIColor+VaidenColors.h"
#import "PlayerDetailTVC.h"
#import "SupportsTVC.h"

@interface SupportVC () <UICollectionViewDataSource, UICollectionViewDelegate, UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *pageScrollView;
@property (weak, nonatomic) IBOutlet UIView *northeast;
@property (weak, nonatomic) IBOutlet UIView *midwest;
@property (weak, nonatomic) IBOutlet UIView *south;
@property (weak, nonatomic) IBOutlet UIView *southeast;
@property (weak, nonatomic) IBOutlet UIView *west;
@property (weak, nonatomic) IBOutlet UILabel *westLabel;
@property (weak, nonatomic) IBOutlet UILabel *midwestLabel;
@property (weak, nonatomic) IBOutlet UILabel *northeastLabel;
@property (weak, nonatomic) IBOutlet UILabel *southLabel;
@property (weak, nonatomic) IBOutlet UILabel *southeastLabel;
@property (weak, nonatomic) IBOutlet UIImageView *map;
@property (weak, nonatomic) IBOutlet UILabel *regionLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIButton *viewAllButton;
@property (weak, nonatomic) IBOutlet UIButton *viewAllBtn;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *square;
@property (weak, nonatomic) IBOutlet UIView *point1;
@property (weak, nonatomic) IBOutlet UIView *point2;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeight;
@property (weak, nonatomic) IBOutlet UIImageView *icon1;
@property (weak, nonatomic) IBOutlet UIImageView *icon2;
@property (weak, nonatomic) IBOutlet UIView *bottomLine;
@property (weak, nonatomic) IBOutlet UIImageView *profilePic;
@property (weak, nonatomic) IBOutlet UILabel *levelLabel;
@property (weak, nonatomic) IBOutlet UILabel *voteRestLabel;
@property (weak, nonatomic) IBOutlet UILabel *title1;
@property (weak, nonatomic) IBOutlet UILabel *title2;

@property (nonatomic, strong) UserPreferences *preferences;
@property (assign) NSString *region;
@property (nonatomic, strong) NSMutableArray * data;
@property (assign) NSInteger targetID;

@property (nonatomic, strong) UIView *overlay;
@property (nonatomic, strong) UIView *overlay2;
@property (nonatomic) NSString *secPick;
@property (nonatomic) NSInteger days;
@end

@implementation SupportVC

#define NORTHEAST @"Northeast"
#define SOUTHEAST @"Southeast"
#define MIDWEST @"Midwest"
#define SOUTH @"South"
#define WEST @"West"

#define SUPPORTS @"supports"
#define SUPPORTERS @"supporters"

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (NSMutableArray *)data
{
    if (!_data) _data = [[NSMutableArray alloc] init];
    return _data;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
    self.modalPresentationStyle = UIModalPresentationCurrentContext;
    self.tabBarController.modalPresentationStyle = UIModalPresentationCurrentContext;
    [self.northeast makeRoundedBorderWithRadius:3];
    [self.midwest makeRoundedBorderWithRadius:3];
    [self.south makeRoundedBorderWithRadius:3];
    [self.southeast makeRoundedBorderWithRadius:3];
    [self.west makeRoundedBorderWithRadius:3];
    [self.viewAllButton makeRoundedBorderWithRadius:5];
    [self.viewAllButton.layer setBorderColor:[[UIColor colorWithRed:179/255.0 green:179/255.0 blue:179/255.0 alpha:1.0] CGColor]];
    [self.viewAllButton.layer setBorderWidth:1.0];
    [self.viewAllBtn makeRoundedBorderWithRadius:5];
    [self.viewAllBtn.layer setBorderColor:[[UIColor colorWithRed:179/255.0 green:179/255.0 blue:179/255.0 alpha:1.0] CGColor]];
    [self.viewAllBtn.layer setBorderWidth:1.0];
    
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
    // Init the overlay view 4 collection view
    self.overlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.collectionView.frame.size.width, self.collectionView.frame.size.height)];
    self.overlay.backgroundColor = [UIColor whiteColor];
    
    UIImage *img = [UIImage imageNamed:@"attention_grey"];
    UIImageView *icon = [[UIImageView alloc] initWithImage:img];
    [icon setFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width/2 - 10,15,20,20)];
    [icon setContentMode:UIViewContentModeCenter];
    
    UILabel *textLine = [[UILabel alloc] initWithFrame:CGRectMake(0, 35, self.collectionView.frame.size.width, 42)];
    textLine.textColor = [UIColor colorWithRed:170/255.0 green:170/255.0 blue:170/255.0 alpha:1.0];
    [textLine setNumberOfLines:0];
    [textLine setLineBreakMode:NSLineBreakByWordWrapping];
    textLine.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:11];
    textLine.text = @"You do not have any supporters yet. Invite friends and upload photos to build a following.";
    [textLine setTextAlignment:NSTextAlignmentCenter];
    [self.overlay addSubview:icon];
    [self.overlay addSubview:textLine];
    
    // Init the overlay view 4 tableview
    self.overlay2 = [[UIView alloc] initWithFrame:CGRectMake(0, 128, self.tableView.frame.size.width, self.tableView.frame.size.height-128)];
    self.overlay2.backgroundColor = [UIColor whiteColor];
    
    [icon setFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width/2 - 10,25,20,20)];
    
    [textLine setFrame:CGRectMake(10, 45, self.tableView.frame.size.width-20, 42)];
    
    self.secPick = SUPPORTS;
    [self.tableView setHidden:YES];
    [self.overlay2 addSubview:icon];
    [self.overlay2 addSubview:textLine];
    
    [self.square makeRoundedBorderWithRadius:5];
    [self.square.layer setBorderColor:[[UIColor colorWithRed:229/255.0 green:229/255.0 blue:229/255.0 alpha:1.0] CGColor]];
    [self.point1 makeRoundedBorderWithRadius:3];
    [self.point2 makeRoundedBorderWithRadius:3];
    
    [self.tableViewHeight setConstant:[[UIScreen mainScreen] bounds].size.height - self.tabBarController.tabBar.frame.size.height - self.navigationController.navigationBar.frame.size.height - 63];
    
    [self loadPage];
    [self firstlogin];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    self.pageScrollView.contentSize = CGSizeMake(320.0, 650.0);
    
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationItem.title = @"SUPPORT";
}

- (void)addOverlay
{
    if ([self.secPick isEqualToString: SUPPORTS]) {
        [self.overlay2 setFrame:CGRectMake(0, 0, self.collectionView.frame.size.width, self.collectionView.frame.size.height)];
        [self.collectionView addSubview:self.overlay2];
    } else {
        [self.overlay2 setFrame:CGRectMake(0, 128, self.tableView.frame.size.width, self.tableView.frame.size.height-128)];
        [self.tableView addSubview:self.overlay2];
        [self.tableView setUserInteractionEnabled:NO];
    }
    
}

- (void)removeOverlay
{
    if ([self.secPick isEqualToString:SUPPORTS]) {
        [self.overlay2 removeFromSuperview];
    } else {
        [self.overlay2 removeFromSuperview];
        [self.tableView setUserInteractionEnabled:YES];
    }
    
}

- (void) firstlogin
{
    [IOSRequest getLoginNums:[self.preferences getUserID]
                onCompletion:^(NSDictionary *results) {
                    NSLog(@"%@",results);
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if([results[@"num"] isEqualToString:@"1"]){
                            [self performSegueWithIdentifier:@"coach_segue" sender:self];
                            
                        }
                        
                    });
                }];
    
}

- (void)loadPage
{
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.labelText = @"Loading...";
    if ([self.secPick isEqualToString:SUPPORTS]) {
        [self checkRegionForUser];
    } else {
        [self getUserVoteDetail];
    }
}

- (void) getUserVoteDetail
{
    [IOSRequest getVoteDetailForUser:[self.preferences getUserID]
                        onCompletion:^(NSDictionary *result) {
                            if ([result[@"outcome"] isEqualToString:@"success"]) {
                                NSLog(@"%@", result);
                                NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                                        baseString:result[@"list"][@"profilePic"]];
                                [self.profilePic setImageWithURL:[NSURL URLWithString:url]
                                           placeholderImage:[UIImage imageNamed:@"avatar square"]];
                                [UIImage makeRoundedImageNoBorder:self.profilePic withRadius:3];
                                
                                self.levelLabel.text = [NSString stringWithFormat:@"Your level: %@", [result[@"list"][@"level"] uppercaseString]];
                                if ([result[@"list"][@"vote_to_next"] integerValue]  == 0) {
                                    self.voteRestLabel.text = [NSString stringWithFormat:@"Votes Until Next Level: --"];
                                } else {
                                    self.voteRestLabel.text = [NSString stringWithFormat:@"Votes Until Next Level: %d", [result[@"list"][@"vote_to_next"]  integerValue]];
                                }
                                [self fetchUserSupports:NO];
                            } else if ([result[@"outcome"] isEqualToString:@"failure"]) {
                                UIAlertView *alert = [[UIAlertView alloc] init];
                                alert.title = @"Submission Error";
                                alert.message = @"There was a problem submitting your request.  Please try again.";
                                [alert addButtonWithTitle:@"OK"];
                                [alert show];
                            }
                        }];
}

- (void) fetchUserSupports:(BOOL)flag
{
    [IOSRequest fetchSupportsForUser:[self.preferences getUserID]
                              forAll:flag
                        onCompletion:^(NSDictionary *result) {
                            NSLog(@"%@",result);
                            [self.data removeAllObjects];
                            if ([result[@"outcome"] isEqualToString:@"success"]) {
                                if (result[@"list"] != [NSNull null]) {
                                    [self removeOverlay];
                                    for (id object in result[@"list"]) {
                                        [self.data addObject:object];
                                    }
                                } else {
                                    [self addOverlay];
                                }
                                self.days = [result[@"days"] integerValue];
                                [self.tableView reloadData];
                                [MBProgressHUD hideHUDForView:self.view animated:YES];
                            } else if ([result[@"outcome"] isEqualToString:@"failure"]) {
                                UIAlertView *alert = [[UIAlertView alloc] init];
                                alert.title = @"Submission Error";
                                alert.message = @"There was a problem submitting your request.  Please try again.";
                                [alert addButtonWithTitle:@"OK"];
                                [alert show];
                            }
                        }];
    // Delay 10s to dismiss the overlay
    double delayInSeconds = 10.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        //code to be executed on the main queue after delay
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    });
}

- (void) checkRegionForUser
{
    [IOSRequest getLocationForUser:[self.preferences getUserID]
                      onCompletion:^(NSDictionary *result) {
                          NSLog(@"%@",result);
                          if ([result[@"outcome"] isEqualToString:@"success"]) {
                              NSArray* addresses = [(NSString *)result[@"location"] componentsSeparatedByString: @","];
                              NSString* state = [addresses objectAtIndex: 1];
                              [self getRegionForState:state]; // Translate the state code into region
                              [self loadReceivingVotesInRegion];
                              [self updatePage];
                          } else if ([result[@"outcome"] isEqualToString:@"failure"]) {
                              UIAlertView *alert = [[UIAlertView alloc] init];
                              alert.title = @"Submission Error";
                              alert.message = @"There was a problem submitting your request.  Please try again.";
                              [alert addButtonWithTitle:@"OK"];
                              [alert show];
                          }
                      }];
}

- (void) getRegionForState:(NSString *)state
{
    if ([state isEqualToString:@"CT"] || [state isEqualToString:@"DE"] || [state isEqualToString:@"ME"] || [state isEqualToString:@"MD"] || [state isEqualToString:@"MA"] || [state isEqualToString:@"NH"] || [state isEqualToString:@"NJ"] || [state isEqualToString:@"NY"] || [state isEqualToString:@"PA"] || [state isEqualToString:@"RI"] || [state isEqualToString:@"VT"]) {
        self.region = NORTHEAST;
    } else if ([state isEqualToString:@"AL"] || [state isEqualToString:@"AR"] || [state isEqualToString:@"FL"] || [state isEqualToString:@"GA"] || [state isEqualToString:@"KY"] || [state isEqualToString:@"LA"] || [state isEqualToString:@"MS"] || [state isEqualToString:@"NC"] || [state isEqualToString:@"SC"] || [state isEqualToString:@"TN"] || [state isEqualToString:@"VA"] || [state isEqualToString:@"WV"]) {
        self.region = SOUTHEAST;
    } else if ([state isEqualToString:@"IL"] || [state isEqualToString:@"IN"] || [state isEqualToString:@"IA"] || [state isEqualToString:@"KS"] || [state isEqualToString:@"NE"] || [state isEqualToString:@"ND"] || [state isEqualToString:@"MI"] || [state isEqualToString:@"MN"] || [state isEqualToString:@"MO"] || [state isEqualToString:@"OH"] || [state isEqualToString:@"SD"]) {
        self.region = MIDWEST;
    } else if ([state isEqualToString:@"AZ"] || [state isEqualToString:@"NM"] || [state isEqualToString:@"OK"] || [state isEqualToString:@"TX"]) {
        self.region = SOUTH;
    } else if ([state isEqualToString:@"AK"] || [state isEqualToString:@"CA"] || [state isEqualToString:@"CO"] || [state isEqualToString:@"HI"] || [state isEqualToString:@"ID"] || [state isEqualToString:@"MT"] || [state isEqualToString:@"NV"] || [state isEqualToString:@"UT"] || [state isEqualToString:@"WA"] || [state isEqualToString:@"WY"]) {
        self.region = WEST;
    }
}

- (void) updatePage
{
    if ([self.region isEqualToString:NORTHEAST]) {
        self.map.image = [UIImage imageNamed:@"northeast_selected"];
        self.regionLabel.text = [NORTHEAST uppercaseString];
        [self.northeast setBackgroundColor:[UIColor colorWithRed:137/255.0 green:110/255.0 blue:57/255.0 alpha:0.7]];
        [self.west setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.midwest setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.south setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.southeast setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
    } else if ([self.region isEqualToString:SOUTHEAST]) {
        self.map.image = [UIImage imageNamed:@"southeast_selected"];
        self.regionLabel.text = [SOUTHEAST uppercaseString];
        [self.southeast setBackgroundColor:[UIColor colorWithRed:137/255.0 green:110/255.0 blue:57/255.0 alpha:0.7]];
        [self.northeast setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.midwest setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.south setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.west setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
    } else if ([self.region isEqualToString:MIDWEST]) {
        self.map.image = [UIImage imageNamed:@"midwest_selected"];
        self.regionLabel.text = [MIDWEST uppercaseString];
        [self.midwest setBackgroundColor:[UIColor colorWithRed:137/255.0 green:110/255.0 blue:57/255.0 alpha:0.7]];
        [self.northeast setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.west setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.south setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.southeast setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
    } else if ([self.region isEqualToString:SOUTH]) {
        self.map.image = [UIImage imageNamed:@"south_selected"];
        self.regionLabel.text = [SOUTH uppercaseString];
        [self.south setBackgroundColor:[UIColor colorWithRed:137/255.0 green:110/255.0 blue:57/255.0 alpha:0.7]];
        [self.northeast setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.midwest setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.west setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.southeast setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
    } else if ([self.region isEqualToString:WEST]) {
        self.map.image = [UIImage imageNamed:@"west_selected"];
        self.regionLabel.text = [WEST uppercaseString];
        [self.west setBackgroundColor:[UIColor colorWithRed:137/255.0 green:110/255.0 blue:57/255.0 alpha:0.7]];
        [self.northeast setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.midwest setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.south setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.southeast setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
    }
}

- (void)loadReceivingVotesInRegion
{
    [IOSRequest loadReceivingVotesInRegionForUser:[self.preferences getUserID]
                                     onCompletion:^(NSDictionary *results) {
                                         NSLog(@"%@",results);
                                         if ([results[@"outcome"] isEqualToString:@"success"]) {
                                             [self updateVotesMapWithNortheast:results[@"list"][NORTHEAST]
                                                                     southeast:results[@"list"][SOUTHEAST]
                                                                       midwest:results[@"list"][MIDWEST]
                                                                         south:results[@"list"][SOUTH]
                                                                          west:results[@"list"][WEST]];
                                             [self loadSupportersForUserInRegion:self.region forAll:NO];
                                         } else if ([results[@"outcome"] isEqualToString:@"failure"]) {
                                             UIAlertView *alert = [[UIAlertView alloc] init];
                                             alert.title = @"Submission Error";
                                             alert.message = @"There was a problem submitting your request. Please try again later or contact us.";
                                             [alert addButtonWithTitle:@"OK"];
                                             [alert show];
                                         }
                                     }];
}

- (void)loadDataForPage
{
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.labelText = @"Loading...";
    [self loadSupportersForUserInRegion:self.region forAll:NO];
}

- (void)updateVotesMapWithNortheast:(NSString *)northeast
                          southeast:(NSString *)southeast
                            midwest:(NSString *)midwest
                              south:(NSString *)south
                               west:(NSString *)west
{
    self.westLabel.text = [NSString stringWithFormat:@"%d", [west integerValue]];
    self.northeastLabel.text = [NSString stringWithFormat:@"%d", [northeast integerValue]];
    self.southeastLabel.text = [NSString stringWithFormat:@"%d", [southeast integerValue]];
    self.midwestLabel.text = [NSString stringWithFormat:@"%d", [midwest integerValue]];
    self.southLabel.text = [NSString stringWithFormat:@"%d", [south integerValue]];
}

- (void)loadSupportersForUserInRegion:(NSString *)region forAll:(BOOL)flag
{
    [IOSRequest loadSupportersForUser:[self.preferences getUserID]
                             inRegion:self.region
                               forAll:flag
                         onCompletion:^(NSDictionary *result) {
                             [self.data removeAllObjects];
                             NSLog(@"%@", result);
                             if ([result[@"outcome"] isEqualToString:@"success"]) {
                                 if (result[@"list"] != [NSNull null]) {
                                     [self removeOverlay];
                                     for (id object in result[@"list"]) {
                                         [self.data addObject:object];
                                     }
                                 } else {
                                     [self addOverlay];
                                 }
                                 [self.collectionView reloadData];
                                 [MBProgressHUD hideHUDForView:self.view animated:YES];
                             } else if ([result[@"outcome"] isEqualToString:@"failure"]) {
                                 UIAlertView *alert = [[UIAlertView alloc] init];
                                 alert.title = @"Submission Error";
                                 alert.message = @"There was a problem submitting your request. Please try again later or contact us.";
                                 [alert addButtonWithTitle:@"OK"];
                                 [alert show];
                             }
                         }];
    // Delay 10s to dismiss the overlay
    double delayInSeconds = 10.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        //code to be executed on the main queue after delay
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    });
    
}

#pragma - mark ACTIONS
- (IBAction)northeastPicked:(id)sender {
    if (![self.region isEqualToString: NORTHEAST]) {
        self.region = NORTHEAST;
        self.regionLabel.text = [NORTHEAST uppercaseString];
        self.map.image = [UIImage imageNamed:@"northeast_selected"];
        [self.northeast setBackgroundColor:[UIColor colorWithRed:137/255.0 green:110/255.0 blue:57/255.0 alpha:0.7]];
        [self.west setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.midwest setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.south setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.southeast setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self loadDataForPage];
    }
}

- (IBAction)midwestPicked:(id)sender {
    if (![self.region isEqualToString: MIDWEST]) {
        self.region = MIDWEST;
        self.map.image = [UIImage imageNamed:@"midwest_selected"];
        self.regionLabel.text = [MIDWEST uppercaseString];
        [self.midwest setBackgroundColor:[UIColor colorWithRed:137/255.0 green:110/255.0 blue:57/255.0 alpha:0.7]];
        [self.northeast setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.west setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.south setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.southeast setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self loadDataForPage];
    }
}

- (IBAction)southPicked:(id)sender {
    if (![self.region isEqualToString: SOUTH]) {
        self.region = SOUTH;
        self.regionLabel.text = [SOUTH uppercaseString];
        self.map.image = [UIImage imageNamed:@"south_selected"];
        [self.south setBackgroundColor:[UIColor colorWithRed:137/255.0 green:110/255.0 blue:57/255.0 alpha:0.7]];
        [self.northeast setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.midwest setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.west setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.southeast setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self loadDataForPage];
    }
}

- (IBAction)southeastPicked:(id)sender {
    if (![self.region isEqualToString: SOUTHEAST]) {
        self.region = SOUTHEAST;
        self.regionLabel.text = [SOUTHEAST uppercaseString];
        self.map.image = [UIImage imageNamed:@"southeast_selected"];
        [self.southeast setBackgroundColor:[UIColor colorWithRed:137/255.0 green:110/255.0 blue:57/255.0 alpha:0.7]];
        [self.northeast setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.midwest setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.south setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.west setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self loadDataForPage];
    }
}

- (IBAction)westPicked:(id)sender {
    if (![self.region isEqualToString: WEST]) {
        self.region = WEST;
        self.regionLabel.text = [WEST uppercaseString];
        self.map.image = [UIImage imageNamed:@"west_selected"];
        [self.west setBackgroundColor:[UIColor colorWithRed:137/255.0 green:110/255.0 blue:57/255.0 alpha:0.7]];
        [self.northeast setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.midwest setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.south setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.southeast setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self loadDataForPage];
    }
}

- (IBAction)picButtonClicked:(id)sender {
    [self performSegueWithIdentifier:@"detail_from_support" sender:sender];
}

- (IBAction)viewAllClicked:(id)sender {
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.labelText = @"Loading...";
    if ([self.secPick isEqualToString:SUPPORTS]) {
        [self.viewAllBtn setEnabled:NO];
        self.regionLabel.text = @"ALL REGION";
        self.region = @"";
        self.map.image = [UIImage imageNamed:@"map_basic"];
        [self.west setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.northeast setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.midwest setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.south setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self.southeast setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:0.7]];
        [self loadSupportersForUserInRegion:self.region forAll:YES];
    } else {
        [self.viewAllButton setEnabled:NO];
        [self fetchUserSupports:YES];
    }
}

- (IBAction)supports:(id)sender {
    self.secPick = SUPPORTS;
    
    [self.viewAllBtn setEnabled:YES];
    self.icon1.image = [UIImage imageNamed:@"in_select"];
    self.icon2.image = [UIImage imageNamed:@"out"];
    CGRect orginFrame = self.bottomLine.frame;
    if (orginFrame.origin.x > 26) { // back to the first one
        [UIView animateWithDuration:0.3
                         animations:^{
                             self.bottomLine.frame = CGRectMake(26, orginFrame.origin.y, orginFrame.size.width, orginFrame.size.height);
                         }
                         completion:nil];
    }
    self.title1.textColor = [UIColor goldColor];
    self.title2.textColor = [UIColor colorWithRed:170/255.0 green:170/255.0 blue:170/255.0 alpha:1.0];
    
    [self.pageScrollView setHidden:NO];
    [self.tableView setHidden:YES];
    [self loadPage];
}

- (IBAction)supporter:(id)sender {
    self.secPick = SUPPORTERS;
    
    [self.viewAllButton setEnabled:YES];
    self.icon1.image = [UIImage imageNamed:@"in"];
    self.icon2.image = [UIImage imageNamed:@"out_select"];
    CGRect orginFrame = self.bottomLine.frame;
    if (orginFrame.origin.x < 186) { // push to second one
        [UIView animateWithDuration:0.3
                         animations:^{
                             self.bottomLine.frame = CGRectMake(186, orginFrame.origin.y, orginFrame.size.width, orginFrame.size.height);
                         }
                         completion:nil];
    }
    self.title1.textColor = [UIColor colorWithRed:170/255.0 green:170/255.0 blue:170/255.0 alpha:1.0];
    self.title2.textColor = [UIColor goldColor];
    
    [self.pageScrollView setHidden:YES];
    [self.tableView setHidden:NO];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self loadPage];
}

- (IBAction)myProfilePicButton:(id)sender {
    // Nothing...
}

- (IBAction)supportProfilePicButton:(id)sender {
    [self performSegueWithIdentifier:@"detail_from_support" sender:sender];
}

#pragma - mark collection view datasource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    NSInteger sections = self.data.count / 4;
    if (self.data.count % 4 > 0) {
        return sections+1;
    } else {
        return sections;
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSInteger sections = self.data.count / 4;
    if (self.data.count % 4 > 0) {
        if (section == sections) {
            return self.data.count % 4;
        } else {
            return 4;
        }
    } else {
        return 4;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    SupportCVC *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"support_cell" forIndexPath:indexPath];
    
    cell.name.text = [self.data objectAtIndex:indexPath.section*4+indexPath.row][@"username"];
    
    NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%d votes",[[self.data objectAtIndex:indexPath.section*4+indexPath.row][@"vote_num"] integerValue]]];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor goldColor] range:NSMakeRange(0,[NSString stringWithFormat:@"%d",[[self.data objectAtIndex:indexPath.section*4+indexPath.row][@"vote_num"] integerValue]].length)];
    cell.votes.attributedText = string;
    //cell.votes.text = [NSString stringWithFormat:@"%d votes",[[self.data objectAtIndex:indexPath.section*4+indexPath.row][@"vote_num"] integerValue]] ;
    
    cell.picBtn.tag = [[self.data objectAtIndex:indexPath.section*4+indexPath.row][@"user_id"] integerValue];
    
    NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                            baseString:[self.data objectAtIndex:indexPath.section*4+indexPath.row][@"profilePic"]];
    
    [cell.image setImageWithURL:[NSURL URLWithString:url]
                    placeholderImage:[UIImage imageNamed:@"avatar square"]];
    
    [UIImage makeRoundedImageNoBorder:cell.image withRadius:3];
    
    return cell;
}

#pragma - mark table view datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.data.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SupportsTVC *cell = [tableView dequeueReusableCellWithIdentifier:@"support_user_cell"];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.name.text = [self.data objectAtIndex:indexPath.row][@"username"];
    cell.yourVotes.text = [NSString stringWithFormat:@"Your Votes: %d",[[self.data objectAtIndex:indexPath.row][@"vote_give"] integerValue]];
    cell.rank.text = [NSString stringWithFormat:@"Rank: %d", [[self.data objectAtIndex:indexPath.row][@"rank"] integerValue]];
    cell.region.text = [NSString stringWithFormat:@"Region: %@", [self.data objectAtIndex:indexPath.row][@"region"]];
    
    NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                            baseString:[self.data objectAtIndex:indexPath.row][@"profilePic"]];
    [cell.profilePic setImageWithURL:[NSURL URLWithString:url]
               placeholderImage:[UIImage imageNamed:@"avatar square"]];
    [UIImage makeRoundedImageNoBorder:cell.profilePic withRadius:3];
    
    NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%d votes",[[self.data objectAtIndex:indexPath.row][@"votes"] integerValue]]];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor goldColor] range:NSMakeRange(0,[NSString stringWithFormat:@"%d",[[self.data objectAtIndex:indexPath.row][@"votes"] integerValue]].length)];
    cell.votes.attributedText = string;
    
    [cell.bar makeRoundedBorderWithRadius:3];
    // Draw the red bar
    CGFloat atio = self.days/100.0;
    [cell.redBarWidth setConstant:atio * cell.bar.frame.size.width];
    [cell.redbar setBackgroundColor:[UIColor colorWithRed:192/255.0 green:57/255.0 blue:43/255.0 alpha:1.0]];
    [cell.redbar makeRoundedBorderWithRadius:4];
    
    // Draw the green bar
    atio = [[self.data objectAtIndex:indexPath.row][@"vote_give"] integerValue]/100.0;
    [cell.greenBarWidth setConstant:atio * cell.bar.frame.size.width];
    [cell.greenBar setBackgroundColor:[UIColor colorWithRed:39/255.0 green:174/255.0 blue:96/255.0 alpha:1.0]];
    [cell.greenBar makeRoundedBorderWithRadius:4];
    
    cell.profileBtn.tag = [[self.data objectAtIndex:indexPath.row][@"user_id"] integerValue];
    
    return cell;
}

#pragma - mark segue
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(UIButton *)sender
{
    if ([segue.identifier isEqualToString:@"detail_from_support"]) {
        PlayerDetailTVC *controller = segue.destinationViewController;
        controller.playerUserID = sender.tag;
        controller.fromComment = YES;
    }
    if([segue.identifier isEqualToString:@"coach_segue"])  {
        id theSegue = segue.destinationViewController;
        [theSegue setValue:@"coach_support" forKey:@"title"];
    }
}

@end
