//
//  ComposeNewMessageIntroTVC.m
//  Vaiden
//
//  Created by James Chung on 12/25/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "ComposeNewMessageIntroTVC.h"
#import "IOSRequest.h"
#import "UserPreferences.h"
#import "NSDate+Utilities.h"
#import "ComposeNewMessageIntroCell.h"
#import "S3Tools.h"
#import "AFNetworking.h"
#import "UIImage+ProportionalFill.h"
#import "MatchHistoryTVC.h"
#import "UIColor+VaidenColors.h"

@interface ComposeNewMessageIntroTVC ()

@property (nonatomic, strong) UserPreferences *preferences;
@property (nonatomic, strong) NSMutableArray *openMatches;
@property (nonatomic) NSInteger selectedRow;

@property (nonatomic) BOOL noResultsToDisplay;
@property (nonatomic, strong) UIView *tempView;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;

@property (weak, nonatomic) IBOutlet UIView *topMessageView;
@property (weak, nonatomic) IBOutlet UILabel *topMessageLabel;
@property (nonatomic, strong) UIView *overlay;

@end

@implementation ComposeNewMessageIntroTVC

- (NSMutableArray *)openMatches
{
    if (!_openMatches) _openMatches = [[NSMutableArray alloc] init];
    return _openMatches;
}

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self showTempSpinner];
//    self.noResultsToDisplay = YES;
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];

    
    self.selectedRow = 0;
    
    if (self.filterUserID == 0)
        [self loadOpenMatches];
    else
        [self loadOpenMatchesFilterByUser];
}

- (void)showTempSpinner
{
    self.tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    self.tempView.backgroundColor = [UIColor lightGrayColor];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] init];
    [self.activityIndicator setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width/2.0, [UIScreen mainScreen].bounds.size.height/2.0 - 50)];
    [self.activityIndicator startAnimating];
    [self.tempView addSubview:self.activityIndicator];
    
    [self.view addSubview:self.tempView];
    
}


- (void)loadOpenMatches
{
    [IOSRequest getOpenMatchesForUser:[self.preferences getUserID] onCompletion:^(NSMutableArray *results) {
        
        for (id object in results) {
            
            if ([object[@"match_category"] isEqualToString:@"individual"]) {
                IndividualMatch *iMatch = [[IndividualMatch alloc] init];
                [iMatch setMatchID:[object[@"match_id"] integerValue]];
                [iMatch setMatchName:object[@"match_name"]];
            
          
            
                Sport *matchSport = [[Sport alloc] initWithSportDetails:[object[@"sport_id"] integerValue]
                                                                andName:object[@"sport_name"]
                                                                andType:@"individual"];
                NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
                [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                NSDate *dateTime = [[formatter dateFromString:object[@"ts"]] toLocalTime];
            
                [iMatch setDateTime:dateTime];
                [iMatch setSport:matchSport];
            
                MatchPlayer *player2 = [MatchPlayer makeMatchCompetitorObject:object[@"competitors"]
                                                                    forPlayer:2
                                                                 forSportName:object[@"sport_name"]
                                                                      andType:@"individual"];
            
                MatchPlayer *player1 = [MatchPlayer makeMatchCompetitorObject:object[@"competitors"]
                                                                    forPlayer:1
                                                                 forSportName:object[@"sport_name"]
                                                                      andType:@"individual"];
            
                [iMatch setPlayer1:player1];
                [iMatch setPlayer2:player2];
            
                [self.openMatches addObject:iMatch];
            } else {
                PickupMatch *pMatch = [[PickupMatch alloc] init];
                [pMatch setMatchID:[object[@"match_id"] integerValue]];
                [pMatch setMatchName:object[@"match_name"]];
                
                Sport *matchSport = [[Sport alloc] initWithSportDetails:[object[@"sport_id"] integerValue]
                                                                andName:object[@"sport_name"]
                                                                andType:@"pickup"];
                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                NSDate *dateTime = [[formatter dateFromString:object[@"ts"]] toLocalTime];
                
                [pMatch setDateTime:dateTime];
                [pMatch setSport:matchSport];
                
                NSMutableArray *competitorArray = [PickupMatch makeCompetitorArray:object[@"competitors"] forSportName:object[@"sport_name"] andType:object[@"sport_type"]];
                [pMatch setCompetitorArray:competitorArray];
                
                [self.openMatches addObject:pMatch];
            }
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([results count] == 0) {
                self.topMessageLabel.hidden = YES;
                self.tableView.separatorColor = [UIColor clearColor];
                self.noResultsToDisplay = YES;
                self.topMessageView.backgroundColor = [UIColor lightLightGray];
                self.tableView.backgroundColor = [UIColor lightLightGray];
                [self displayErrorOverlay];
            } else {
                self.noResultsToDisplay = NO;
            }
            
            [self.activityIndicator stopAnimating];
            self.tempView.hidden = YES;

            [self.tableView reloadData];
        });
        
    }];
  
   }


- (void)loadOpenMatchesFilterByUser
{
    [IOSRequest getOpenMatchesForUser:[self.preferences getUserID]
                         filterByUser:self.filterUserID
                         onCompletion:^(NSMutableArray *results) {
        
        for (id object in results) {
            
            if ([object[@"match_category"] isEqualToString:@"individual"]) {
                IndividualMatch *iMatch = [[IndividualMatch alloc] init];
                [iMatch setMatchID:[object[@"match_id"] integerValue]];
                [iMatch setMatchName:object[@"match_name"]];
            
            
            
                Sport *matchSport = [[Sport alloc] initWithSportDetails:[object[@"sport_id"] integerValue]
                                                                andName:object[@"sport_name"]
                                                                andType:@"individual"];
                NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
                [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                NSDate *dateTime = [[formatter dateFromString:object[@"ts"]] toLocalTime];
            
                [iMatch setDateTime:dateTime];
                [iMatch setSport:matchSport];
            
                MatchPlayer *player2 = [MatchPlayer makeMatchCompetitorObject:object[@"competitors"]
                                                                    forPlayer:2
                                                                forSportName:object[@"sport_name"]
                                                                      andType:@"individual"];
            
                MatchPlayer *player1 = [MatchPlayer makeMatchCompetitorObject:object[@"competitors"]
                                                                    forPlayer:1
                                                                 forSportName:object[@"sport_name"]
                                                                      andType:@"individual"];
            
                [iMatch setPlayer1:player1];
                [iMatch setPlayer2:player2];
            
                [self.openMatches addObject:iMatch];
            } else {
                PickupMatch *pMatch = [[PickupMatch alloc] init];
                [pMatch setMatchID:[object[@"match_id"] integerValue]];
                [pMatch setMatchName:object[@"match_name"]];
                
                Sport *matchSport = [[Sport alloc] initWithSportDetails:[object[@"sport_id"] integerValue]
                                                                andName:object[@"sport_name"]
                                                                andType:@"pickup"];
                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                NSDate *dateTime = [[formatter dateFromString:object[@"ts"]] toLocalTime];
                
                [pMatch setDateTime:dateTime];
                [pMatch setSport:matchSport];
                
                NSMutableArray *competitorArray = [PickupMatch makeCompetitorArray:object[@"competitors"] forSportName:object[@"sport_name"] andType:object[@"sport_type"]];
                [pMatch setCompetitorArray:competitorArray];
                
                [self.openMatches addObject:pMatch];
            }
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([results count] == 0) {
                self.topMessageLabel.hidden = YES;
                self.tableView.separatorColor = [UIColor clearColor];
                self.noResultsToDisplay = YES;
                self.topMessageView.backgroundColor = [UIColor lightLightGray];
                self.tableView.backgroundColor = [UIColor lightLightGray];
                [self displayErrorOverlay];
                
            } else {
                self.noResultsToDisplay = NO;
            }
            
            [self.activityIndicator stopAnimating];
            self.tempView.hidden = YES;
            
            [self.tableView reloadData];
        });
        
    }];
    
}

- (void)displayErrorOverlay
{
    self.overlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, [UIScreen mainScreen].bounds.size.height)];
    self.overlay.backgroundColor = [UIColor lightLightGray];
    
    UILabel *firstLine = [[UILabel alloc] initWithFrame:CGRectMake(30, ([UIScreen mainScreen].bounds.size.height / 2) - 100, 260, 30)];
    firstLine.textColor = [UIColor darkGrayColor];
    firstLine.font = [UIFont systemFontOfSize:23];
    firstLine.text = @"No Open Matches";
    
    UILabel *secondLine = [[UILabel alloc] initWithFrame:CGRectMake(30, ([UIScreen mainScreen].bounds.size.height / 2) - 70, 260, 80)];
    secondLine.textColor = [UIColor lightGrayColor];
    secondLine.font = [UIFont systemFontOfSize:17];
    secondLine.text = @"In Vaiden, you can only message other players if you have open matches with them";
    secondLine.numberOfLines = 0;
    
    [self.overlay addSubview:firstLine];
    [self.overlay addSubview:secondLine];
    
    [self.tableView addSubview:self.overlay];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.noResultsToDisplay)
        return 1;
    
    return [self.openMatches count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Match Cell";
    ComposeNewMessageIntroCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if (self.noResultsToDisplay) {
        cell.matchNameLabel.hidden = YES;
        cell.profilePic.hidden = YES;
        cell.dateTimeLabel.hidden = YES;
        cell.vsLabel.hidden = YES;
        cell.sportLabel.hidden = YES;
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.textLabel.text = @"You do not have any open matches";
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.textLabel.textColor = [UIColor lightGrayColor];
        cell.textLabel.hidden = NO;
        cell.backgroundColor = [UIColor lightLightGray];
        return cell;
    }
    
    
    Match *theMatch = [self.openMatches objectAtIndex:indexPath.row];
    
    if ([theMatch isKindOfClass:[IndividualMatch class]]) {
        IndividualMatch *iMatch = (IndividualMatch *) theMatch;
        
        NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                baseString:[[iMatch getOtherPlayer:[self.preferences getUserID]] profileBaseString]];
        
        [cell.profilePic setImageWithURL:[NSURL URLWithString:url]
                        placeholderImage:[UIImage imageNamed:@"avatar round"]];
        
        [UIImage makeRoundedImage:cell.profilePic withRadius:20];
        
        
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"MMMM d, YYYY"];
        cell.dateTimeLabel.text = [dateFormatter stringFromDate:iMatch.dateTime];
        
        cell.matchNameLabel.text = iMatch.matchName;
        cell.vsLabel.text = [NSString stringWithFormat:@"%@ vs %@", iMatch.player1.userName, iMatch.player2.userName];
        cell.sportLabel.text = [NSString stringWithFormat:@"Challenge %@", [iMatch.sport.sportName capitalizedString]];
        

    } else {
        PickupMatch *pMatch = (PickupMatch *)theMatch;
        
        MatchPlayer *otherPlayer = [pMatch getPlayerOtherThanSessionUser:[self.preferences getUserID]];
        
        NSString *profilePicString;
        
        if (otherPlayer == nil)
            profilePicString = [self.preferences getProfilePicString];
        else
            profilePicString = otherPlayer.profileBaseString;
        
        NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                baseString:profilePicString];
        [cell.profilePic setImageWithURL:[NSURL URLWithString:url]
                        placeholderImage:[UIImage imageNamed:@"avatar round"]];
        
        [UIImage makeRoundedImage:cell.profilePic withRadius:20];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"MMMM d, YYYY"];
        cell.dateTimeLabel.text = [dateFormatter stringFromDate:pMatch.dateTime];
        
        cell.matchNameLabel.text = pMatch.matchName;
        cell.vsLabel.text = [NSString stringWithFormat:@"%ld confirmed players", [pMatch getNumConfirmed]];
        cell.sportLabel.text = [NSString stringWithFormat:@"Pickup %@", [pMatch.sport.sportName capitalizedString]];
        
    }
    
    
   
    
    // Configure the cell...
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.noResultsToDisplay) {
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        return screenRect.size.height - 300;
    }
    
    return 96.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedRow = indexPath.row;
    [self performSegueWithIdentifier:@"Match History Segue" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Match History Segue"]) {
        MatchHistoryTVC *controller = segue.destinationViewController;
        controller.playerMatch = [self.openMatches objectAtIndex:self.selectedRow];
        controller.shouldShowKeyboard = YES;
    }
}
@end
