//
//  UIColor+VaidenColors.h
//  Vaiden
//
//  Created by James Chung on 10/3/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (VaidenColors)

+ (UIColor *)salmonColor;
+ (UIColor *)deepSkyBlue;
+ (UIColor *)peacock;
+ (UIColor *)peacockDark;
+ (UIColor *)sienna;
+ (UIColor *)lightGreenColor;
+ (UIColor *)burgundyColor;
+ (UIColor *)smokeBackground;
+ (UIColor *)midGray;
+ (UIColor *)midGray1;
+ (UIColor *)midGray2;
+ (UIColor *)lightLightGray;
+ (UIColor *)midLightGray;
+ (UIColor *)azureColor;
+ (UIColor *)skyBlue;
+ (UIColor *)iRed;
+ (UIColor *)emeraldGreen;
+ (UIColor *)almostBlack;
+ (UIColor *)springGreen;
+ (UIColor *)newBlueLight;
+ (UIColor *)newBlueDark;
+ (UIColor *)grayBorder1;
+ (UIColor *)newCharcoal;
+ (UIColor *)goldColor;
+ (UIColor *)lightGrayColor2;
+ (UIColor *)darkGrayColor2;
+ (UIColor *)darkGrayColor3;
@end
