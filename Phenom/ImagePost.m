//
//  ImagePost.m
//  Phenom
//
//  Created by James Chung on 5/28/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "ImagePost.h"

@implementation ImagePost



- (ImagePost *)initWithNewsDetails:(NSInteger)newsMakerUserID
                       andUserName:(NSString *)newsMakerUserName
                     andProfileStr:(NSString *)profileBaseString
                          withDate:(NSDate *)newsDate
                       andPostType:(NSInteger)postType
                         andNewsID:(NSInteger)newsID
                       andTextPost:(NSString *)textPost
                andImageFileString:(NSString *)imageBaseFileString
                    andNumComments:(NSUInteger)numComments
                     andNumRemixes:(NSUInteger)numRemixes

{
    self = [super initWithNewsDetails:newsMakerUserID
                          andUserName:newsMakerUserName
                        andProfileStr:profileBaseString
                             withDate:newsDate
                          andPostType:postType
                       andNumComments:numComments
                        andNumRemixes:numRemixes
                            andNewsID:newsID
                          andTextPost:textPost];
    
    if (self) {
        _imageBaseFileString = imageBaseFileString;
    }
    return self;
}



@end
