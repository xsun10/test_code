//
//  TeamMatchCreationNews.h
//  Vaiden
//
//  Created by James Chung on 4/16/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "News.h"
#import "TeamMatch.h"

@interface TeamMatchCreationNews : News

@property (nonatomic, strong) TeamMatch *tMatch;

- (TeamMatchCreationNews *) initWithIndividualMatchCreationNewsDetails:(TeamMatch *)tMatch
                                                        matchCreatorUserName:(NSString *)matchCreatorUserName
                                                               andProfileStr:(NSString *)profileBaseString
                                                                newsPostDate:(NSDate *)newsPostDate
                                                                 andPostType:(NSInteger)postType
                                                                   andNewsID:(NSInteger)newsID
                                                              andNumComments:(NSUInteger)numComments
                                                               andNumRemixes:(NSUInteger)numRemixes;


@end
