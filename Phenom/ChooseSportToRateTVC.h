//
//  ChooseSportToRateTVC.h
//  Vaiden
//
//  Created by James Chung on 12/27/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Player.h"
#import "CustomBaseTVC.h"

@interface ChooseSportToRateTVC : CustomBaseTVC

@property (nonatomic, strong) Player* playerToRate;
@end
