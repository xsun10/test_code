//
//  VoteAlert.h
//  Vaiden
//
//  Created by Turbo on 7/17/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Contact.h"

@interface VoteAlert : NSObject

@property (nonatomic, strong) Contact *refPlayer;
@property (nonatomic, strong) NSString *message;
@property (nonatomic) NSInteger alertType;
@property (nonatomic, strong) NSDate *alertDateTime;
@property (nonatomic) BOOL wasViewed;
@property (nonatomic) NSInteger votes;
@property (nonatomic) NSInteger alertID;
@property (nonatomic) NSInteger sent;


- (VoteAlert *) initWithVoteAlertDetails:(NSInteger)alertID
                                fromUser:(NSInteger)userID
                             andUserName:(NSString *)userName
                     andProfilePicString:(NSString *)profilePicString
                              andMessage:(NSString *)message
                            andAlertType:(NSInteger)alertType
                            andAlertDate:(NSDate *)alertDate
                            andWasViewed:(BOOL)wasViewed
                           andVoteNumber:(NSInteger)votes
                                 andSent:(NSInteger)sent;

- (VoteAlert *) initWithVoteAlertThanksDetails:(NSInteger)userID
                                   andUserName:(NSString *)userName
                           andProfilePicString:(NSString *)profilePicString
                                    andMessage:(NSString *)message
                                  andAlertType:(NSInteger)alertType
                                  andAlertDate:(NSDate *)alertDate
                                  andWasViewed:(BOOL)wasViewed;
@end
