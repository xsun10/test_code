//
//  ShowPhotosInLockerAlbumCVC.h
//  Vaiden
//
//  Created by James Chung on 5/12/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "CustomBaseCVC.h"
#import "Sport.h"


@interface ShowPhotosInLockerAlbumCVC : CustomBaseCVC

@property (nonatomic, strong) Sport *sport;
@property (nonatomic, strong) NSString *picsSeason;
@property (nonatomic) NSInteger picsYear;
@property (nonatomic) NSInteger albumHolderUserID;

@end
