//
//  ProfileTabVC.m
//  Phenom
//
//  Created by James Chung on 3/18/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "ProfileTabVC.h"
#import "IOSRequest.h"

@interface ProfileTabVC ()


@end

@implementation ProfileTabVC

-(void)fetchAddress:(NSString *)address
{
    NSLog(@"Loading Address: %@",address);
    [IOSRequest requestPath:address onCompletion:^(NSString *result, NSError *error){
        dispatch_async(dispatch_get_main_queue(), ^{
            if (!error) {
                // No Error
            } else {
                // Handle Error
                NSLog(@"ERROR: %@",error);
            }
        });
    }];
}



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
