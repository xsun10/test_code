//
//  ConfirmIndMatchScoreVC.h
//  Vaiden
//
//  Created by James Chung on 11/25/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "CustomBaseVC.h"

@interface ConfirmIndMatchScoreVC : CustomBaseVC <UICollectionViewDelegate, UICollectionViewDataSource, MBProgressHUDDelegate> {
    MBProgressHUD *HUD;
}

@property (nonatomic) NSUInteger matchID;

@end
