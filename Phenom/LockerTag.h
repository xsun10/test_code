//
//  LockerTag.h
//  Vaiden
//
//  Created by James Chung on 5/7/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LockerTag : NSObject

#define TAG_TYPE_SPORT 1
#define TAG_TYPE_SEASON 2
#define TAG_TYPE_YEAR 3
#define TAG_TYPE_VENUE 4
#define TAG_TYPE_PEOPLE 5
#define TAG_TYPE_STATS 6
#define TAG_TYPE_TRAINING 7
#define TAG_TYPE_EVENT 8


@property (nonatomic) NSInteger tagID;
@property (nonatomic, strong) NSString *tagName;
@property (nonatomic) NSInteger tagType;


- (LockerTag *) initWithTagDetails:(NSInteger)lockerTagID
                        andTagType:(NSInteger)lockerTagType
                        andTagName:(NSString *)lockerTagName;

@end
