//
//  TrainingTag.m
//  Vaiden
//
//  Created by James Chung on 5/27/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "TrainingTag.h"

@implementation TrainingTag

- (TrainingTag *)initWithTrainingTagInfo:(NSInteger)trainingTagID
                      andTrainingTagName:(NSString *)trainingTagName
{
    self = [super init];
    
    if (self) {
        _trainingTagID      = trainingTagID;
        _trainingTagName    = trainingTagName;
        _isSelected = NO;
    }
    
    return self;
}
@end
