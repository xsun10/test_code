//
//  QuickInfo3TVC.m
//  Vaiden
//
//  Created by James Chung on 9/30/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "QuickInfo3TVC.h"
#import "UIImage+ImageEffects.h"

@interface QuickInfo3TVC ()

@property (nonatomic, strong) UIImage *backImage;

@end

@implementation QuickInfo3TVC


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];

    self.backImage = [UIImage imageNamed:@"Jordan Background"];
    
    [self setBackgroundImage:1];
}


- (void)setBackgroundImage:(NSUInteger)imageIndex
{
    NSString *effectText = @"";
    UIImage *effectImage = nil;
    
    effectImage = self.backImage;
    
    switch (imageIndex)
    {
        case 0:
            effectImage = self.backImage;
            break;
        case 1:
            effectImage = [self.backImage applyLightEffect];
            effectText = NSLocalizedString(@"Light", @"");
            //           self.effectLabel.textColor = [UIColor whiteColor];
            break;
        case 2:
            effectImage = [self.backImage applyExtraLightEffect];
            effectText = NSLocalizedString(@"Extra light", @"");
            //            self.effectLabel.textColor = [UIColor lightGrayColor];
            break;
        case 3:
            effectImage = [self.backImage applyDarkEffect];
            effectText = NSLocalizedString(@"Dark", @"");
            //           self.effectLabel.textColor = [UIColor darkGrayColor];
            break;
        case 4:
            effectImage = [self.backImage applyTintEffectWithColor:[UIColor blueColor]];
            effectText = NSLocalizedString(@"Color tint", @"");
            //           self.effectLabel.textColor = [UIColor darkGrayColor];
            break;
    }
    
    //    self.infoTableview.backgroundColor = [UIColor colorWithPatternImage:effectImage];
    // self.backImageView.image = effectImage;
    
    UIImageView *tempImageView = [[UIImageView alloc] initWithImage:effectImage];
    [tempImageView setFrame:self.tableView.frame];
    
    self.tableView.backgroundView = tempImageView;
    
}

@end
