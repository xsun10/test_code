//
//  SportRatingsSummaryHeaderCell.h
//  Vaiden
//
//  Created by James Chung on 1/1/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SportRatingsSummaryHeaderCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *sportIcon;
@property (weak, nonatomic) IBOutlet UILabel *sportName;
@property (weak, nonatomic) IBOutlet UILabel *sportLevel;
@property (weak, nonatomic) IBOutlet UILabel *sportRecord;
@property (weak, nonatomic) IBOutlet UILabel *username;
@property (weak, nonatomic) IBOutlet UIButton *ratePlayerButton;
@property (weak, nonatomic) IBOutlet UIImageView *bulbIcon;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UIImageView *profilePic;

@end
