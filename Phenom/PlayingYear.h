//
//  PlayingYear.h
//  Vaiden
//
//  Created by James Chung on 5/8/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PlayingYear : NSObject

- (PlayingYear *)initWithYear:(NSString *)year;
@property (nonatomic, strong) NSString *year;

// initializes object to current year
- (void)setWithCurrentYear;
@end
