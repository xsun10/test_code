//
//  AddVenueIntroVC.m
//  Vaiden
//
//  Created by James Chung on 12/5/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "AddVenueIntroVC.h"
#import "UIColor+VaidenColors.h"
#import "UIView+Manipulate.h"
#import "AddVenueWithoutAddressTVC.h"
#import "UIImage+ImageEffects.h"

@interface AddVenueIntroVC () <UIActionSheetDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *pageScrollView;
@property (weak, nonatomic) IBOutlet UIView *viewForButton1;
@property (weak, nonatomic) IBOutlet UIView *viewForButton2;
@property (weak, nonatomic) IBOutlet UILabel *headlineLabel;
@property (weak, nonatomic) IBOutlet UIView *iconBackground;
@property (weak, nonatomic) IBOutlet UIImageView *iconPic;
@property (weak, nonatomic) IBOutlet UIImageView *backImage;
@property (weak, nonatomic) IBOutlet UIView *viewForButton3;
@property (weak, nonatomic) IBOutlet UILabel *extraTextForButton3;

@property (nonatomic) NSUInteger choiceNum;

@end

@implementation AddVenueIntroVC

// For the action sheet
#define CANCEL_BUTTON_ACTIONSHEET @"Cancel "

#define TITLE_OF_ACTIONSHEET_FOR_CREATE_VENUE @"WARNING: Other users rely on data you enter.  False entries can lead to the suspension of your account."
#define CREATE_VENUE @"Ok, I understand"

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.choiceNum = 0;
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
    
    if (!self.isCreatingMatch) {
        self.viewForButton3.hidden = YES;
        self.extraTextForButton3.hidden = YES;
    }
    
    //[self setPageBlurredBackgroundImage:1];
    
    [self setUpPageValues];

}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self.pageScrollView setContentInset:UIEdgeInsetsMake(0, 0, 130, 0)];

    self.pageScrollView.contentSize = CGSizeMake(320.0, 600.0);
}

- (void)setUpPageValues
{
    [self.viewForButton1 makeRoundedBorderWithRadius:3];
    [self.viewForButton2 makeRoundedBorderWithRadius:3];
    [self.viewForButton3 makeRoundedBorderWithRadius:3];
    
    [self.iconBackground makeCircleWithColorAndBackground:[UIColor colorWithRed:199/255.0 green:160/255.0 blue:74/255.0 alpha:1.0] andRadius:45];
    
    //self.iconPic.image = [[UIImage imageNamed:@"Location Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    //[self.iconPic setTintColor:[UIColor whiteColor]];
    
}

#define MAKE_VENUE_WITH_ADDRESS 1
#define MAKE_VENUE_WITH_COORDINATES 2
#define MAKE_TEMP_VENUE 3

- (IBAction)AddVenueWithAddress:(id)sender
{
    self.choiceNum = MAKE_VENUE_WITH_ADDRESS;
    [self showActionsheet];
}

- (IBAction)addVenueWithoutAddress:(id)sender
{
    self.choiceNum = MAKE_VENUE_WITH_COORDINATES;
    [self showActionsheet];
}
- (IBAction)addTemporaryVenue:(id)sender
{
    self.choiceNum = MAKE_TEMP_VENUE;
    [self showActionsheet];
}


- (void)setPageBlurredBackgroundImage:(NSUInteger)imageIndex
{
    NSString *effectText = @"";
    UIImage *effectImage = nil;
    UIImage *backImage = [UIImage imageNamed:@"add_venue_bg"];
    
    effectImage = backImage;
    
    switch (imageIndex)
    {
        case 0:
            effectImage = backImage;
            break;
        case 1:
            effectImage = [backImage applyMidLightEffectLessBlur];
            effectText = NSLocalizedString(@"Light", @"");
            break;
        case 2:
            effectImage = [backImage applyExtraLightEffect];
            effectText = NSLocalizedString(@"Extra light", @"");
            break;
        case 3:
            effectImage = [backImage applyDarkEffect];
            effectText = NSLocalizedString(@"Dark", @"");
            break;
        case 4:
            effectImage = [backImage applyTintEffectWithColor:[UIColor blueColor]];
            effectText = NSLocalizedString(@"Color tint", @"");
            break;
    }
    
    self.backImage.image = effectImage;
    
    
}


- (void)showActionsheet
{
        UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                      initWithTitle:TITLE_OF_ACTIONSHEET_FOR_CREATE_VENUE delegate:self
                                      cancelButtonTitle:CANCEL_BUTTON_ACTIONSHEET destructiveButtonTitle:nil
                                      otherButtonTitles:CREATE_VENUE,  nil];
        [actionSheet showFromTabBar:self.tabBarController.tabBar];
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == actionSheet.destructiveButtonIndex) {
        [self.view.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    } else {
        NSString *choice = [actionSheet buttonTitleAtIndex:buttonIndex];
        
        if ([choice isEqualToString:CREATE_VENUE]) {
            if (self.choiceNum == MAKE_VENUE_WITH_ADDRESS) {
                 [self performSegueWithIdentifier:@"Add Venue With Address Segue" sender:self];
            } else if (self.choiceNum == MAKE_VENUE_WITH_COORDINATES) {
                 [self performSegueWithIdentifier:@"Add Venue Without Address Segue" sender:self];
            } else if (self.choiceNum == MAKE_TEMP_VENUE) {
                [self performSegueWithIdentifier:@"Add Temporary Venue Segue" sender:self];
            }
      
            
        }
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Add Temporary Venue Segue"]) {
        AddTemporaryVenueTVC *controller = segue.destinationViewController;
        controller.delegate = self;
        
        
        if (self.isCreatingMatch)
            controller.numVCsToPop = 3;
        else
            controller.numVCsToPop = 1;
    } else if ([segue.identifier isEqualToString:@"Add Venue With Address Segue"]) {
        AddVenueTVC *controller = segue.destinationViewController;
        controller.delegate = self;
        controller.isCreatingMatch = self.isCreatingMatch;
    } else if ([segue.identifier isEqualToString:@"Add Venue Without Address Segue"]) {
        AddVenueWithoutAddressTVC *controller = segue.destinationViewController;
        controller.delegate = self;
        controller.isCreatingMatch = self.isCreatingMatch;
    }
}

// delegate method
- (void)setViewController:(AddTemporaryVenueTVC *)controller withVenueID:(NSInteger)venueID andVenueName:(NSString *)venueName
{
    [self.delegate setViewController:self withVenueID:venueID andVenueName:venueName];
}

- (void)setMyVenueViewController:(AddVenueTVC *)controller andVenueID:(NSInteger)venueID andVenueName:(NSString *)venueName
{
    [self.delegate setViewController:self withVenueID:venueID andVenueName:venueName];
}

- (void)setViewController:(AddVenueWithoutAddressTVC *)controller andVenueID:(NSInteger)venueID andVenueName:(NSString *)venueName
{
    [self.delegate setViewController:self withVenueID:venueID andVenueName:venueName];
}
@end
