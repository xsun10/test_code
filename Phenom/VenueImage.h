//
//  VenueImage.h
//  Vaiden
//
//  Created by James Chung on 10/8/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VenueImage : NSObject

@property (nonatomic, strong) NSString *venuePicString;
@property (nonatomic, strong) UIImage *venuePic;
@property (nonatomic) BOOL isDefaultImage;


-(VenueImage*)initWithVenueImageInfo:(NSString *)venuePicString
                            andImage:(UIImage *)venuePic
                           isDefault:(BOOL)isDefault;


@end
