//
//  IMDHeaderInfoButtonCVCell.h
//  Vaiden
//
//  Created by James Chung on 2/25/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IMDHeaderInfoButtonCVCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *iconPic;
@property (weak, nonatomic) IBOutlet UILabel *actionName;
@property (weak, nonatomic) IBOutlet UIButton *matchActionButton;

@end
