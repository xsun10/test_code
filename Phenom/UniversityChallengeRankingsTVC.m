//
//  UniversityChallengeRankingsTVC.m
//  Vaiden
//
//  Created by James Chung on 3/1/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "UniversityChallengeRankingsTVC.h"
#import "UserPreferences.h"
#import "UniversityChallengeRankingsCell.h"
#import "IOSRequest.h"
#import "S3Tools.h"
#import "UIView+Manipulate.h"
#import "AFNetworking.h"
#import "UIImage+ProportionalFill.h"
#import "PlayerDetailTVC.h"
#import "AddIndividualMatchIntroVC.h"

@interface UniversityChallengeRankingsTVC ()

@property (nonatomic, strong) NSMutableArray *players;
@property (nonatomic, strong) UserPreferences *preferences;
@property (nonatomic, strong) UIView *tempView;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;

@property (nonatomic) NSInteger selectedRow;

@end

@implementation UniversityChallengeRankingsTVC

#define SPORT_ID 1
#define SPORT_NAME @"basketball"

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (NSMutableArray *)players
{
    if (!_players) _players = [[NSMutableArray alloc] init];
    return  _players;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];


    [self showTempSpinner];
    
    [IOSRequest getSchoolRankingsForUniversity:self.universityID onCompletion:^(NSMutableArray *results) {
        for (id object in results) {
            Player *pl = [[Player alloc] initWithPlayerDetails:[object[@"user_id"] integerValue]
                                                            andUserName:object[@"username"]];
            [pl setFullName:object[@"fullname"]];
            [pl setProfileBaseString:object[@"profile_pic_string"]];
        
            PlayerSport *ps = [[PlayerSport alloc] initWithSportDetails:SPORT_ID
                                                                andName:SPORT_NAME
                                                                andType:@"individual"
                                                               andLevel:object[@"level"]
                                                         andSportRecord:nil];
            ps.playerRecordForSport = [[PlayerRecord alloc] initWithPlayerRecordWins:[object[@"wins"] integerValue]
                                                                           andLosses:[object[@"losses"] integerValue]
                                                                             andTies:[object[@"ties"] integerValue]];
            [pl addPlayerSport:ps];
            
            [self.players addObject:pl];
        
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
            [self.activityIndicator stopAnimating];
            self.tempView.hidden = YES;
            
            if ([self.players count] >= 50) {
                self.navigationItem.title = @"Top 50";
            } else if ([self.players count] >= 20) {
                self.navigationItem.title = @"Top 20";
            } else if ([self.players count] >= 10) {
                self.navigationItem.title = @"Top 10";
            } else {
                self.navigationItem.title = @"Class Ranks";
            }
        });
        
        
    }];
}


- (void)showTempSpinner
{
    self.tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, [UIScreen mainScreen].bounds.size.height)];
    self.tempView.backgroundColor = [UIColor lightGrayColor];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] init];
    [self.activityIndicator setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width/2.0, [UIScreen mainScreen].bounds.size.height/2.0 )];
    [self.activityIndicator startAnimating];
    [self.tempView addSubview:self.activityIndicator];
    
    [self.view addSubview:self.tempView];
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.players count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"My Cell";
    UniversityChallengeRankingsCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    Player *vContact = (self.players)[indexPath.row];
    
    PlayerSport *sport = [vContact.playerSports objectAtIndex:0]; // only one sport should be in array
    
    if ([vContact computeTotalNumMatchesPlayedForSport:sport.sportID] > 0)
        cell.sportLevelLabel.text = [NSString stringWithFormat:@"%ld",  [sport.level integerValue]];
    else
        cell.sportLevelLabel.text = @"0";
    
    cell.recordLabel.text = [NSString stringWithFormat:@"%ld %@ - %ld %@", sport.playerRecordForSport.wins, [[sport.playerRecordForSport getWinsText] capitalizedString], sport.playerRecordForSport.losses, [[sport.playerRecordForSport getLossesText] capitalizedString]];
    
    if ([vContact.fullName length] > 0 && ![vContact.fullName isKindOfClass:[NSNull class]] && vContact.fullName != nil)
        cell.usernameLabel.text = vContact.fullName;
    else
        cell.usernameLabel.text = vContact.userName;
    
    cell.numRankingLabel.text = [NSString stringWithFormat:@"%ld", indexPath.row + 1];
    cell.sportNameLabel.text = [sport.sportName capitalizedString];
    
    NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                            baseString:vContact.profileBaseString
                     ];
    
    [cell.profilePic setImageWithURL:[NSURL URLWithString:url]
                    placeholderImage:[UIImage imageNamed:@"avatar round"]];
    
    [UIImage makeRoundedImage:cell.profilePic withRadius:30];
    
    if ([self.preferences getUserID] == vContact.userID) {
        cell.challengeButton.hidden = YES;
    } else {
        cell.challengeButton.hidden = NO;
        [cell.challengeButton makeRoundedBorderWithRadius:3];
    }
    
    cell.challengeButton.tag = indexPath.row;
    

    
    return cell;
}
- (IBAction)challengeUserAction:(UIButton *)sender
{
    self.selectedRow = sender.tag;
    sender.enabled = NO;
    if (![self.preferences isVerifiedAccount]) {
        sender.enabled = YES;
        [self performSegueWithIdentifier:@"Should Verify Segue" sender:self];
    } else {
        Player *c = [self.players objectAtIndex:self.selectedRow];
        
        [IOSRequest checkOKToChallengeUser:c.userID
                             bySessionUser:[self.preferences getUserID
                                            ] onCompletion:^(NSDictionary *results) {
                                 dispatch_async(dispatch_get_main_queue(), ^{
                                     sender.enabled = YES;
                                     if ([results[@"ok"] isEqualToString:@"yes"]) {
                                         [self performSegueWithIdentifier:@"Challenge Segue" sender:self];
                                     } else {
                                         UIAlertView *alert = [[UIAlertView alloc] init];
                                         alert.title = @"Can't Challenge User";
                                         alert.message = @"This user can only be challenged by players he / she follows";
                                         [alert addButtonWithTitle:@"OK"];
                                         [alert show];
                                     }
                                 });
                                 
                             }];
        
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 84.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedRow = indexPath.row;
    
    [self performSegueWithIdentifier:@"Player Detail Segue" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    Player *selectedPlayer = [self.players objectAtIndex:self.selectedRow];

    if ([segue.identifier isEqualToString:@"Player Detail Segue"]) {
        PlayerDetailTVC *controller = segue.destinationViewController;
        controller.playerUserID = selectedPlayer.userID;
        controller.profilePicString = selectedPlayer.profileBaseString;
    } else if ([segue.identifier isEqualToString:@"Challenge Segue"]) {
        Player *player = [self.players objectAtIndex:self.selectedRow];
        AddIndividualMatchIntroVC *controller = segue.destinationViewController;
        controller.player = player;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return self.universityName;
 
}

@end
