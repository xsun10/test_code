//
//  Venue+MKAnnotation.h
//  Phenom
//
//  Created by James Chung on 5/7/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "Venue.h"
#import <MapKit/MapKit.h>

@interface Venue (MKAnnotation) <MKAnnotation>

// this blocks main thread
- (UIImage *)thumbnail;
- (CLLocationCoordinate2D) coordinate;

@end
