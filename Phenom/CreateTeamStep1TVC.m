//
//  CreateTeamStep1TVC.m
//  Vaiden
//
//  Created by James Chung on 4/6/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "CreateTeamStep1TVC.h"
#import "UITextField+FormValidations.h"
#import "CreateTeamStep2TVC.h"
#import "Team.h"
#import "IOSRequest.h"

@interface CreateTeamStep1TVC ()

@property (weak, nonatomic) IBOutlet UITextField *teamNameLabel;
@property (nonatomic, strong) UIBarButtonItem *doneButton;

@end

@implementation CreateTeamStep1TVC



- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
    [self setDoneButton];
}

- (void)setDoneButton
{
    UIButton *btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 50, 21);
    btn.layer.borderColor = [UIColor whiteColor].CGColor;
    btn.layer.borderWidth = 1.0;
    
    [btn addTarget:self action:@selector(done:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundColor:[UIColor clearColor]];
    [btn setTitle:@"NEXT" forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-MediumItalic" size:12.0]];
    
    // Initialize UIBarbuttonitem...
    self.doneButton = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = self.doneButton;
}

- (void)done:(UIButton *)sender
{
    [self.teamNameLabel resignFirstResponder];
    
    if (![self.teamNameLabel checkTeamName]) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Team Name Error";
        alert.message = @"Team Name is not valid.  Please enter a name between 3 and 20 letters with only alphanumeric characters.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
        
        
    } else {
        self.doneButton.enabled = NO;
        HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        HUD.labelText = @"Checking Name";
        
        [IOSRequest checkTeamName:self.teamNameLabel.text
                     onCompletion:^(NSDictionary *results) {
                         
                         dispatch_async(dispatch_get_main_queue(), ^{
                             self.doneButton.enabled = YES;
                             [MBProgressHUD hideHUDForView:self.view animated:YES];

                             if ([results[@"outcome"] isEqualToString:@"exists"]) {
                                 UIAlertView *alert = [[UIAlertView alloc] init];
                                 alert.title = @"Team Name Error";
                                 alert.message = @"That team name already exists.  Please try again with a new name.";
                                 [alert addButtonWithTitle:@"OK"];
                                 [alert show];
                             } else if ([results[@"outcome"] isEqualToString:@"not_exists"]) {
                                 [self performSegueWithIdentifier:@"Step 2 Create Team Segue" sender:self];
                             }
                         });
                         
                     }];
        
        double delayInSeconds = 10.0;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            //code to be executed on the main queue after delay
            self.doneButton.enabled = YES;
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        });
        
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Step 2 Create Team Segue"]) {
        CreateTeamStep2TVC *controller = segue.destinationViewController;
        controller.nTeam = [[Team alloc] init];
        controller.nTeam.teamName = self.teamNameLabel.text;
    }
}

@end
