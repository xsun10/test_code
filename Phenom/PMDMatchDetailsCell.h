//
//  PMDMatchDetailsCell.h
//  Vaiden
//
//  Created by James Chung on 1/16/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PMDMatchDetailsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *dateTimeIconBackground;
@property (weak, nonatomic) IBOutlet UIImageView *dateTimeIconImage;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@property (weak, nonatomic) IBOutlet UIView *locationIconBackground;
@property (weak, nonatomic) IBOutlet UIImageView *locationIconImage;
@property (weak, nonatomic) IBOutlet UIImageView *venueImage;
@property (weak, nonatomic) IBOutlet UILabel *venueNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *streetNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *cityStateLabel;
@property (weak, nonatomic) IBOutlet UIButton *detailsButton;
@property (weak, nonatomic) IBOutlet UIView *myBackgroundView;


@end
