//
//  OpenPickupMatchesCollectionCell.m
//  Phenom
//
//  Created by James Chung on 7/19/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "OpenPickupMatchesCollectionCell.h"

@implementation OpenPickupMatchesCollectionCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
