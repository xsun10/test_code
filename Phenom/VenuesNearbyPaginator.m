//
//  VenuesNearbyPaginator.m
//  Vaiden
//
//  Created by James Chung on 12/9/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "VenuesNearbyPaginator.h"


@interface VenuesNearbyPaginator() {
}

// protected properties

@property (nonatomic, strong) NSMutableArray *venuesNearby;

@end

@implementation VenuesNearbyPaginator

- (NSMutableArray *)venuesNearby
{
    if (!_venuesNearby) _venuesNearby = [[NSMutableArray alloc] init];
    return _venuesNearby;
}


@end
