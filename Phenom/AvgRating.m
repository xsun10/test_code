//
//  AvgRating.m
//  Phenom
//
//  Created by James Chung on 7/4/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "AvgRating.h"
#import "Rating.h"
#import "NSDate+Utilities.h"

@implementation AvgRating

-(AvgRating *)initWithAvgRating:(float)avgRating
                     andRatings:(NSArray *)ratingsArray
{
    self = [super init];
    
    if (self) {
        _avgRatingValue = avgRating;
        _ratings = [[NSMutableArray alloc] initWithArray:ratingsArray];
        
    }
    
    return self;
}

- (void)addARating:(NSInteger)ratingValue
       withComment:(NSString *)comment
            byUser:(NSInteger)userID
      withUserName:(NSString *)userName
withProfilePicString:(NSString *)profilePicString
            onDate:(NSDate *)ratingDate
{
    Rating *rating = [[Rating alloc] initWithRating:ratingValue
                                             byUser:userID
                                       withUserName:userName
                                andProfilePicString:profilePicString
                                        withComment:comment
                                             onDate:ratingDate];
    
    [_ratings addObject:rating];
}

- (NSInteger)getRatingCount
{
   return [_ratings count];
}

// arrayObj references objects from venues/fetch_venue_detail.php and venue_utilities.php on server
+ (NSMutableArray *)makeRatingsArray:(NSArray *)arrayObj
{
    NSMutableArray *rArray = [[NSMutableArray alloc] init];
    
    for (id object in rArray) {
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *date = [[formatter dateFromString:object[@"rating_date"]] toLocalTime];
        
        
        Rating *rate = [[Rating alloc] initWithRating:[object[@"rating"] integerValue]
                                               byUser:[object[@"user_id"] integerValue]
                                         withUserName:object[@"username"]
                                  andProfilePicString:object[@"profile_pic_string"]
                                          withComment:object[@"comment"]
                                               onDate:date];
        
        [rArray addObject:rate];
    }
    
    return rArray;
}

- (UIImage *)getRatingImage
{
    UIImage *ratingImage = nil;
    NSInteger rate = (int)floor(self.avgRatingValue);
    
    switch (rate) {
        case 0:
            ratingImage = [UIImage imageNamed:@"star zero"];
            break;
            
        case 1:
            ratingImage = [UIImage imageNamed:@"star one"];
            break;
            
        case 2:
            ratingImage = [UIImage imageNamed:@"star two"];
            break;
            
        case 3:
            ratingImage = [UIImage imageNamed:@"star three"];
            break;
            
        case 4:
            ratingImage = [UIImage imageNamed:@"star four"];
            break;
            
        case 5:
            ratingImage = [UIImage imageNamed:@"star five"];
            break;
            
        default:
            ratingImage = [UIImage imageNamed:@"star zero"];
            break;
    }
    return  ratingImage;
}

@end
