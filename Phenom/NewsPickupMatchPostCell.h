//
//  NewPickupMatchPostCell.h
//  Phenom
//
//  Created by James Chung on 4/23/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewsfeedMatchHeadingCV.h"

@class NewsPickupMatchPostCell;

@protocol NewsPickupMatchPostCell_delegate <NSObject>
-(void)setNewsPickupMatchPostCellViewController:(NewsPickupMatchPostCell *)controller withUserIDToSegue:(NSInteger)userID;
@end



@interface NewsPickupMatchPostCell : UITableViewCell <UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, weak) id <NewsPickupMatchPostCell_delegate> delegate;
@property (weak, nonatomic) IBOutlet UIImageView *newsfeedIcon;

@property (weak, nonatomic) IBOutlet UIImageView *profilePic;
@property (weak, nonatomic) IBOutlet UILabel *postTime;
@property (weak, nonatomic) IBOutlet UILabel *matchName;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@property (weak, nonatomic) IBOutlet UILabel *venueLabel;
@property (weak, nonatomic) IBOutlet UILabel *sportLabel;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;

@property (weak, nonatomic) IBOutlet UIButton *commentButton;
@property (weak, nonatomic) IBOutlet UILabel *commentNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *commentTextLabel;
@property (weak, nonatomic) IBOutlet UIImageView *commentButtonIcon;
@property (weak, nonatomic) IBOutlet UIView *commentButtonBackground;
@property (weak, nonatomic) IBOutlet UIView *shareButtonBackground;

@property (weak, nonatomic) IBOutlet UILabel *sharesTextLabel;
@property (weak, nonatomic) IBOutlet UIButton *sharesButton;
@property (weak, nonatomic) IBOutlet UILabel *sharesNumberLabel;
@property (weak, nonatomic) IBOutlet UIImageView *shareButtonIcon;
@property (weak, nonatomic) IBOutlet UIView *cellMainBackgroundView;
@property (weak, nonatomic) IBOutlet UIImageView *genderIcon;
@property (weak, nonatomic) IBOutlet UIView *sportIconBackground;
@property (weak, nonatomic) IBOutlet UIImageView *sportIconDetail;


@property (weak, nonatomic) IBOutlet UIImageView *sportIcon;
@property (weak, nonatomic) IBOutlet UIView *pickupLabelBackground;
@property (weak, nonatomic) IBOutlet UIView *backgroundViewArea;
@property (weak, nonatomic) IBOutlet UILabel *sharedByLabel;
@property (weak, nonatomic) IBOutlet UILabel *genderLabel;
@property (weak, nonatomic) IBOutlet NewsfeedMatchHeadingCV *matchHeadingCV;
@property (weak, nonatomic) IBOutlet UILabel *headlineLabel;
@property (weak, nonatomic) IBOutlet UIView *detailButtonView;
@property (weak, nonatomic) IBOutlet UIButton *detailButton;
@property (weak, nonatomic) IBOutlet UILabel *publicPrivateMessageLabel;
@property (weak, nonatomic) IBOutlet UIImageView *sportBackgroundImage;

@property (weak, nonatomic) IBOutlet UIButton *actionButtonHandle;
@property (nonatomic, strong) NSString *matchCategory;
@property (nonatomic, strong) NSMutableArray *competitors;
@property (weak, nonatomic) IBOutlet UIImageView *starIcon;

@property (weak, nonatomic) IBOutlet UIView *likeButtonBackground;
@property (weak, nonatomic) IBOutlet UIImageView *likeIcon;
@property (weak, nonatomic) IBOutlet UILabel *likeNumberLabel;
@property (weak, nonatomic) IBOutlet UIButton *likeButton;
@property (weak, nonatomic) IBOutlet UIButton *viewLikesButton;
@property (weak, nonatomic) IBOutlet UIView *smallCircleView;
@property (weak, nonatomic) IBOutlet UIButton *viewCommentsButton;

@end
