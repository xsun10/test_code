//
//  ConfirmIndMatchScoreIntroMessage.m
//  Phenom
//
//  Created by James Chung on 7/29/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "ConfirmIndMatchScoreIntroMessage.h"
#import "S3Tools.h"
#import "UIImage+ProportionalFill.h"
#import "AFNetworking.h"
#import "IOSRequest.h"
#import "ConfirmIndMatchScoreVC.h"
#import "UIView+Manipulate.h"
#import "UserPreferences.h"
#import "UIImage+ImageEffects.h"
#import "UIColor+VaidenColors.h"

@interface ConfirmIndMatchScoreIntroMessage () <UIActionSheetDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *pageScrollView;
@property (weak, nonatomic) IBOutlet UIView *pageView;

@property (weak, nonatomic) IBOutlet UIView *iconBackground;
@property (weak, nonatomic) IBOutlet UIImageView *confirmIcon;
@property (weak, nonatomic) IBOutlet UIButton *confirmButtonHandle;
@property (nonatomic, strong) IndividualMatch *individualMatch;
@property (nonatomic, strong) UserPreferences *preferences;
@property (weak, nonatomic) IBOutlet UIImageView *blurredImageBackground;
@property (weak, nonatomic) IBOutlet UIButton *voidMatchButtonHandle;


@end

@implementation ConfirmIndMatchScoreIntroMessage

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    [self.pageScrollView setContentInset:UIEdgeInsetsMake(64, 0, 44, 0)];
    
    [self setPageBlurredBackgroundImage:1];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
    //[self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back_button"] style:UIBarButtonItemStyleBordered target:self.navigationController action:@selector(popViewControllerAnimated:)]];
    
    [self setPageInfo];
    
}



- (void)setPageInfo
{
    [self.confirmButtonHandle makeRoundedBorderWithRadius:3];
    [self.voidMatchButtonHandle makeRoundedBorderWithRadius:3];
    
//    [self.iconBackground makeCircleWithColor:[UIColor peacock] andRadius:45];
//    self.iconBackground.backgroundColor = [UIColor peacock];
    
//    self.confirmIcon.image = [[UIImage imageNamed:@"Check Mark"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
//    [self.confirmIcon setTintColor:[UIColor whiteColor]];
//    [self.confirmButtonHandle makeCircleWithColor:[UIColor peacock] andRadius:5];
//    [self.confirmButtonHandle setBackgroundColor:[UIColor peacock]];
//    [self.voidMatchButtonHandle setBackgroundColor:[UIColor peacock]];
}


- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    self.pageScrollView.contentSize = CGSizeMake(320.0, 575.0);
    
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Confirm Segue"]) {
        ConfirmIndMatchScoreVC *controller = segue.destinationViewController;
        controller.matchID = self.matchID;
    }
}

- (void)setPageBlurredBackgroundImage:(NSUInteger)imageIndex
{
    NSString *effectText = @"";
    UIImage *effectImage = nil;
    UIImage *backImage = [UIImage imageNamed:@"confirm_bg"];
    
    effectImage = backImage;
    
    switch (imageIndex)
    {
        case 0:
            effectImage = backImage;
            break;
        case 1:
            effectImage = [backImage applyMidLightEffectLessBlur];
            effectText = NSLocalizedString(@"Light", @"");
            break;
        case 2:
            effectImage = [backImage applyExtraLightEffect];
            effectText = NSLocalizedString(@"Extra light", @"");
            break;
        case 3:
            effectImage = [backImage applyDarkEffect];
            effectText = NSLocalizedString(@"Dark", @"");
            break;
        case 4:
            effectImage = [backImage applyTintEffectWithColor:[UIColor blueColor]];
            effectText = NSLocalizedString(@"Color tint", @"");
            break;
    }
    
    self.blurredImageBackground.image = effectImage;
    
    
}

#define TITLE_OF_ACTIONSHEET @"Void the match if it never occurred. It will remove it from our records."
#define CANCEL_BUTTON_ACTIONSHEET @"Cancel "
#define VOID_MATCH @"Void Match"

- (IBAction)voidMatch:(id)sender
{
    
 //   if (!self.venuePicActionSheet) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                      initWithTitle:TITLE_OF_ACTIONSHEET delegate:self
                                      cancelButtonTitle:CANCEL_BUTTON_ACTIONSHEET destructiveButtonTitle:nil
                                      otherButtonTitles:VOID_MATCH,  nil];
        [actionSheet showFromTabBar:self.tabBarController.tabBar];
        
   //     self.venuePicActionSheet = actionSheet;
    //}

}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == actionSheet.destructiveButtonIndex) {
        [self.view.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    } else {
        NSString *choice = [actionSheet buttonTitleAtIndex:buttonIndex];
        
        if ([choice isEqualToString:VOID_MATCH]) {
            [self voidTheMatch];
        }
    }
}

- (void)voidTheMatch
{
    HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    HUD.labelText = @"Updating Match Status";

    [IOSRequest setIndividualMatchAsNeverOccurred:self.matchID
                                           byUser:[self.preferences getUserID]
                                     onCompletion:^(NSDictionary *results) {
                                         
                                         if ([results[@"outcome"] isEqualToString:@"success"]) {
                                             HUD.labelText = @"Success!";
                                         } else {
                                             HUD.labelText = @"Match Update Error";
                                         }
                                         [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
                                         
                                         if ([results[@"outcome"] isEqualToString:@"success"])
                                             [self.navigationController popViewControllerAnimated:YES];

                                     }];
}



@end
