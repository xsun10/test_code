//
//  CreateTeamStep3VC.h
//  Vaiden
//
//  Created by James Chung on 4/6/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "CustomBaseVC.h"
#import "Team.h"
#import "MBProgressHUD.h"

@interface CreateTeamStep3VC : CustomBaseVC <UICollectionViewDataSource, UICollectionViewDelegate, MBProgressHUDDelegate>
{
    MBProgressHUD *HUD;
}

@property (nonatomic, strong) Team *nTeam;


@end
