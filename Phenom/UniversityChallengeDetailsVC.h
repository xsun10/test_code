//
//  UniversityChallengeDetailsVC.h
//  Vaiden
//
//  Created by James Chung on 2/28/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UniversityChallengeDetailsVC : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic) BOOL canRegister;
@property (nonatomic) NSInteger competitionID;
@property (nonatomic) NSInteger universityID;
@property (nonatomic, strong) NSString *universityName;

@end
