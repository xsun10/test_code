//
//  InviteFBFriendsTVC.h
//  Vaiden
//
//  Created by James Chung on 9/18/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import "CustomBaseTVC.h"

@interface InviteFBFriendsTVC : CustomBaseTVC <FBFriendPickerDelegate>

@end
