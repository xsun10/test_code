//
//  AvgRating.h
//  Phenom
//
//  Created by James Chung on 7/4/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AvgRating : NSObject

@property (nonatomic) float avgRatingValue; // no nsfloat?
@property (nonatomic, strong) NSMutableArray *ratings;  // containts Rating objects

-(AvgRating *)initWithAvgRating:(float)avgRating
                     andRatings:(NSArray *)ratingsArray;

- (void)addARating:(NSInteger)ratingValue
       withComment:(NSString *)comment
            byUser:(NSInteger)userID
      withUserName:(NSString *)userName
withProfilePicString:(NSString *)profilePicString
            onDate:(NSDate *)ratingDate;

- (NSInteger)getRatingCount;

+ (NSMutableArray *)makeRatingsArray:(NSArray *)arrayObj;

- (UIImage *)getRatingImage;

@end
