//
//  MatchHistoryObj.m
//  Vaiden
//
//  Created by James Chung on 1/29/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "MatchHistoryObj.h"
#import "MyHelpers.h"

@implementation MatchHistoryObj


- (MatchHistoryObj *) initWithHistoryObject:(id)dataObject
{
    self = [super init];
    
    if (self) {
        
        _matchHistoryID = [dataObject[@"history_id"] integerValue];
        _participant = [[Player alloc] initWithPlayerDetails:[dataObject[@"player_id"] integerValue] andUserName:dataObject[@"username"]];
        _update = dataObject[@"general_message"];
        _occurrenceDate = [MyHelpers getDateTimeFromString:dataObject[@"ts"]];
        
    }
    return self;
    
}

@end
