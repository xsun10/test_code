//
//  CompetitionNews.h
//  Vaiden
//
//  Created by James Chung on 3/6/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "News.h"

@interface CompetitionNews : News

@property (nonatomic, strong) NSString *universityName;
@property (nonatomic, strong) NSString *universityOrganization;

- (CompetitionNews *)initWithNewsID:(NSInteger)newsMakerID
                        andUsername:(NSString *)newsMakerUsername
                      andProfileStr:(NSString *)newsMakerProfileString
                           withDate:(NSDate *)newsDate
                        andPostType:(NSInteger)postType
                     andNumComments:(NSInteger)numComments
                      andNumRemixes:(NSInteger)numRemixes
                          andNewsID:(NSInteger)newsID
                  andUniversityName:(NSString *)universityName
          andUniversityOrganization:(NSString *)universityOrganization;
@end
