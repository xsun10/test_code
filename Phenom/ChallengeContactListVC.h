//
//  ChallengeContactListVC.h
//  Phenom
//
//  Created by James Chung on 7/12/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChooseIndividualSportTVC.h"
#import "CustomBaseVC.h"

@interface ChallengeContactListVC : CustomBaseVC <UITableViewDataSource, UITableViewDelegate, ChooseIndividualSportTVC_Delegate>

@end
