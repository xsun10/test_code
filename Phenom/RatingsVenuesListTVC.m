//
//  RatingsVenuesListTVC.m
//  Phenom
//
//  Created by James Chung on 7/5/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "RatingsVenuesListTVC.h"
#import "AvgRating.h"
#import "Rating.h"
#import "RatingsVenuesListCell.h"
#import "IOSRequest.h"
#import "S3Tools.h"
#import "UIImage+ProportionalFill.h"
#import "AFNetworking.h"
#import "NSDate+Utilities.h"
#import "UIColor+VaidenColors.h"

@interface RatingsVenuesListTVC ()

@property (nonatomic, strong) AvgRating *ar;
@property (nonatomic, strong) NSMutableDictionary *cellHeightsDictionary;
@property (nonatomic, strong) RatingsVenuesListCell *prototypeCell;
@property (nonatomic, strong) UIView *tempView;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
@property (nonatomic, strong) UIView *overlay;
@end

@implementation RatingsVenuesListTVC

- (RatingsVenuesListCell *)prototypeCell
{
    if (!_prototypeCell) {
        _prototypeCell = [self.tableView dequeueReusableCellWithIdentifier:@"Rating Cell"];
    }
    return _prototypeCell;
}

- (NSMutableDictionary *)cellHeightsDictionary
{
    if (!_cellHeightsDictionary) _cellHeightsDictionary = [[NSMutableDictionary alloc] init];
    return _cellHeightsDictionary;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setRatingsData];
}

- (void)showTempSpinner
{
    self.tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    self.tempView.backgroundColor = [UIColor lightGrayColor];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] init];
    [self.activityIndicator setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width/2.0, [UIScreen mainScreen].bounds.size.height/2.0 - 50)];
    [self.activityIndicator startAnimating];
    [self.tempView addSubview:self.activityIndicator];
    
    [self.view addSubview:self.tempView];
    
}

- (void)displayErrorOverlay
{
    self.overlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, [UIScreen mainScreen].bounds.size.height)];
    self.overlay.backgroundColor = [UIColor colorWithRed:239/255.0 green:239/255.0 blue:244/255.0 alpha:1.0];
    
    UIImage *img = [UIImage imageNamed:@"no_rating"];
    UIImageView *icon = [[UIImageView alloc] initWithImage:img];
    [icon setFrame:CGRectMake(135,110,50,50)];
    [icon setContentMode:UIViewContentModeCenter];
    
    UILabel *firstLine = [[UILabel alloc] initWithFrame:CGRectMake(35, 190, 240, 21)];
    firstLine.textColor = [UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:1.0];
    firstLine.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:15];
    firstLine.text = @"No Ratings Yet";
    [firstLine setTextAlignment:NSTextAlignmentCenter];
    
    UILabel *secondLine = [[UILabel alloc] initWithFrame:CGRectMake(20, 210, 260, 42)];
    secondLine.textColor = [UIColor colorWithRed:170/255.0 green:170/255.0 blue:170/255.0 alpha:1.0];
    secondLine.font = [UIFont fontWithName:@"HelveticaNeue" size:13];
    secondLine.text = @"Sorry, but this venue does not have any ratings yet. Be the first to rate it!";
    secondLine.numberOfLines = 0;
    [secondLine setTextAlignment:NSTextAlignmentCenter];
    
    [self.overlay addSubview:icon];
    [self.overlay addSubview:firstLine];
    [self.overlay addSubview:secondLine];
    
    [self.tableView addSubview:self.overlay];
}

- (void)setRatingsData
{
    [IOSRequest fetchRatingsForVenue:self.venueID
                        onCompletion:^(NSDictionary *results) {
                            
                            NSMutableArray *ratingsArray = [[NSMutableArray alloc] init];
                            
                            for (id object in results[@"list"]) {
                                NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
                                [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                                NSDate *rDate = [[formatter dateFromString:object[@"rating_date"]] toLocalTime];
                                
                                Rating *rating = [[Rating alloc] initWithRating:[object[@"rating"] integerValue]
                                                                         byUser:[object[@"user_id"] integerValue]
                                                                   withUserName:object[@"username"]
                                                            andProfilePicString:object[@"profile_pic_string"]
                                                                    withComment:object[@"comment"]
                                                                         onDate:rDate];
                                
                                [ratingsArray addObject:rating];
                            }
                            
                            self.ar = [[AvgRating alloc] initWithAvgRating:[results[@"avg"] floatValue]
                                                                andRatings:ratingsArray];
                            
                            dispatch_async(dispatch_get_main_queue(), ^ {
                                
                                if ([results[@"list"] count] == 0) {
                                    [self displayErrorOverlay];
                                }
                                
                                [self.tableView reloadData];
                                [self.activityIndicator stopAnimating];
                                self.tempView.hidden = YES;
                                
                            });
                        }];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.ar.ratings count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Rating Cell";
    RatingsVenuesListCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    Rating *rating = [self.ar.ratings objectAtIndex:indexPath.row];

    NSInteger commentTag = 1;
    NSInteger dateTag = 2;
    NSString *timeDiffString = nil;
    
    if (cell == nil) {
        cell = [[RatingsVenuesListCell alloc] initWithStyle:UITableViewCellStyleDefault
                                  reuseIdentifier:@"CellIdentifier"];
    }
    
    /*
    if ([cell viewWithTag:commentTag] == nil) {
             cell = [[RatingsVenuesListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }*/
                
            
        
  /*
        cell.dateLabel.frame = CGRectMake(65.0f, dateYOrigin, 200.0f, 15.0f);
        UILabel *dateTitle = [[UILabel alloc] initWithFrame:CGRectMake(65.0f, dateYOrigin, 200.0f, 15.0f)];
        dateTitle.font = [UIFont systemFontOfSize:13.0];
        dateTitle.textColor = [UIColor grayColor];
        [cell addSubview:dateTitle];
   */     
        
        
        
    /*
    if ([cell viewWithTag:commentTag] == nil) {
  //      cell = [[RatingsVenuesListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        itemTitle = [[UILabel alloc] initWithFrame:CGRectMake(65.0f, 28.0f, 243.0f, 200.0f)];
        itemTitle.text = rating.comment;
        itemTitle.adjustsFontSizeToFitWidth = NO;
        itemTitle.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        itemTitle.font = [UIFont systemFontOfSize:13.0];
        itemTitle.lineBreakMode = NSLineBreakByWordWrapping;
        itemTitle.numberOfLines = 0;
        [itemTitle sizeToFit];
        

        [cell addSubview:itemTitle];
        
                
        float frameHeight = itemTitle.frame.size.height;
        
        float dateYOrigin = frameHeight + 28.0f;
        
        UILabel *dateTitle = [[UILabel alloc] initWithFrame:CGRectMake(65.0f, dateYOrigin, 200.0f, 15.0f)];
        dateTitle.font = [UIFont systemFontOfSize:13.0];
        dateTitle.textColor = [UIColor grayColor];
        [cell addSubview:dateTitle];
        
        
        float cellHeight = frameHeight + 55;
        
        [self.cellHeightsDictionary setValue:[NSNumber numberWithFloat:cellHeight] forKey:[NSString stringWithFormat:@"%d", indexPath.row]];
    }
    */
    NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                            baseString:rating.profilePicString
                     ];
    
    [cell.profilePic setImageWithURL:[NSURL URLWithString:url]
                      placeholderImage:[UIImage imageNamed:@"avatar round"]];
    
    [UIImage makeRoundedImage:cell.profilePic withRadius:25];
    
    cell.usernameLabel.text = rating.userName;
    cell.starImage.image = [rating getRatingImage];
    
    NSDate *currentDate = [NSDate date];
    NSTimeInterval timeDifference = [currentDate timeIntervalSinceDate:rating.ratingDate];
    
    int minutes = timeDifference / 60;
    int hours = minutes / 60;
    int days = minutes / 1440;
    
    
    if (minutes > 1440)
        timeDiffString = [NSString stringWithFormat:@"%dd ago", days];
    else if (minutes > 60)
        timeDiffString = [NSString stringWithFormat:@"%dh ago", hours];
    else
        timeDiffString = [NSString stringWithFormat:@"%dm ago", minutes];

    

    ((UILabel *)[cell viewWithTag:commentTag]).text = rating.comment;
    ((UILabel *)[cell viewWithTag:dateTag]).text = timeDiffString;
    
    cell.dateLabel.text = timeDiffString;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    RatingsVenuesListCell *rCell = (RatingsVenuesListCell *)cell;
    Rating *rating = [self.ar.ratings objectAtIndex:indexPath.row];
    
    rCell.commentLabel.frame = CGRectMake(65.0f, 49.0f, 243.0f, 400.0f);
    rCell.commentLabel.text = rating.comment;
    rCell.commentLabel.adjustsFontSizeToFitWidth = NO;
    rCell.commentLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    rCell.commentLabel.font = [UIFont systemFontOfSize:13.0];
    rCell.commentLabel.lineBreakMode = NSLineBreakByWordWrapping;
    rCell.commentLabel.numberOfLines = 0;
    [rCell.commentLabel sizeToFit];
    
    CGFloat offset = 0;
    
    if ([rating.comment length] == 0) {
        offset = rCell.commentLabel.frame.origin.y;
    } else {
        offset = rCell.commentLabel.frame.size.height + rCell.commentLabel.frame.origin.y;
    }
    
    rCell.dateLabel.frame = CGRectMake(65.0f, offset, 243.0f, 200.0f);
    [rCell.dateLabel sizeToFit];

}

#define DEFAULT_CELL_HEIGHT 93
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
Rating *rating = [self.ar.ratings objectAtIndex:indexPath.row];
    return [self.prototypeCell heightForData:rating.comment andDate:nil];
    
    /*
    NSNumber *cellHeight = [self.cellHeightsDictionary valueForKey:[NSString stringWithFormat:@"%d", indexPath.row]];
        
    if ([cellHeight floatValue] > 75)
        return [cellHeight floatValue];
    else
        return DEFAULT_CELL_HEIGHT;
     */
}


@end
