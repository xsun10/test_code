//
//  VenueDetailTVC.m
//  Phenom
//
//  Created by James Chung on 7/2/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "VenueDetailTVC.h"
#import "IOSRequest.h"
#import "Venue+MKAnnotation.h"
#import "MapOverlayVC.h"
#import "VenueDetailSportsCVC.h"
#import "AvgRating.h"
#import "Rating.h"
#import "RateVenueTVC.h"
#import "ReportVenueTVC.h"
#import "RatingsVenuesListTVC.h"
#import "S3Tools.h"
#import "AFNetworking.h"
#import "VenueDetailPicsCVC.h"
#import "VenuePicDetailVC.h"
#import "UIColor+VaidenColors.h"
#import "SetVenuePicsVC.h"
#import "UserPreferences.h"
#import "UIView+Manipulate.h"
#import "PlayerDetailTVC.h"
#import "CheckInTVC.h"
#import "VenueCheckIn.h"
#import "NSDate+Utilities.h"
#import "VDCheckInsCell.h"
#import "UIImage+ProportionalFill.h"
#import "SpecialUIImageView.h"
#import "SpecialUILabel.h"
#import "CheckInHistoryTVC.h"
#import "VenueCheckinStats.h"
#import "VDCheckInStatsCell.h"
#import "ImageSegue.h"

@interface VenueDetailTVC ()
@property (weak, nonatomic) IBOutlet UIImageView *mapImage;
@property (weak, nonatomic) IBOutlet UILabel *venueNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *streetLabel;
@property (weak, nonatomic) IBOutlet UILabel *cityStateLabel;
@property (nonatomic, strong) Venue *venue;
@property (weak, nonatomic) IBOutlet UICollectionView *sportsCV;
@property (weak, nonatomic) IBOutlet UITextView *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *ratingLabel;


@property (weak, nonatomic) IBOutlet UIImageView *starImage;
@property (weak, nonatomic) IBOutlet UIView *subView1;
@property (weak, nonatomic) IBOutlet UIButton *viewRatingsButton;
@property (weak, nonatomic) IBOutlet UILabel *viewRatingsLabel;



@property (weak, nonatomic) IBOutlet UIImageView *venueThumbPic;
@property (weak, nonatomic) IBOutlet UICollectionView *picsCV;
@property (nonatomic) NSInteger selectedPicRow;
@property (nonatomic, strong) UIView *tempView;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
@property (nonatomic, strong) UserPreferences *preferences;

// Rate Button
@property (weak, nonatomic) IBOutlet UIView *rateButtonBackground;
@property (weak, nonatomic) IBOutlet UIImageView *rateIcon;
@property (weak, nonatomic) IBOutlet UILabel *rateLabel;

// Flag Button
@property (weak, nonatomic) IBOutlet UIView *flagButtonBackground;
@property (weak, nonatomic) IBOutlet UIImageView *flagIcon;
@property (weak, nonatomic) IBOutlet UILabel *flagButtonLable;

// Heart button
@property (weak, nonatomic) IBOutlet UIView *heartBackgroundView;
@property (weak, nonatomic) IBOutlet UIImageView *heartIcon;
@property (weak, nonatomic) IBOutlet UILabel *heartButtonLabel;

// Camera Button
@property (weak, nonatomic) IBOutlet UIView *cameraButtonBackground;
@property (weak, nonatomic) IBOutlet UIImageView *cameraIcon;
@property (weak, nonatomic) IBOutlet UILabel *cameraButtonLabel;

@property (weak, nonatomic) IBOutlet UIButton *chooseButton;
@property (nonatomic, strong) NSMutableArray *theKingsOfVenue;

@property (nonatomic) BOOL doneLoading;

@property (nonatomic, strong) VenueCheckinStats *checkinStats;

@property (nonatomic) BOOL isFavorite;
@property (nonatomic) NSInteger selectedUser;
@property (nonatomic) BOOL showFollowingInCheckinHistory;

//@property (nonatomic, strong) UIImage *localMapImage; // used so we do not have to reload map image every time cell is dequeued
@property (nonatomic) CGPoint center;
@property (nonatomic) CGFloat offset;
@property (nonatomic) CGFloat totalY;
@property (nonatomic, strong) UIImage * showupImage;
@end

@implementation VenueDetailTVC

#define TAG_SPORTS_CV 5
#define TAG_PICS_CV 10

//#define SECTION_HEADER 0


#define SECTION_MAP 1
#define SECTION_PHOTOS 0
#define SECTION_SPORTS 3
#define SECTION_DESCRIPTION 2
#define SECTION_CHECKIN_STATS 4
#define SECTION_CHECKINS 5
#define SECTION_KING 6


- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (NSMutableArray *)theKingsOfVenue
{
    if (!_theKingsOfVenue) _theKingsOfVenue = [[NSMutableArray alloc] init];
    return  _theKingsOfVenue;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.doneLoading = NO;
    self.showFollowingInCheckinHistory = NO;
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
    
    [self.chooseButton makeRoundedBorderWithRadius:3];
    
    if (!self.showChooseButton)
        self.chooseButton.backgroundColor = [UIColor goldColor];
    else
        self.chooseButton.backgroundColor = [UIColor goldColor];
    
    if (self.showChooseButton) {
     //   self.chooseButton.hidden = NO;
     //   self.chooseButton.layer.borderWidth = 1.0;
     //   self.chooseButton.layer.borderColor = [UIColor whiteColor].CGColor;
        [self.chooseButton setTitle:@"Choose" forState:UIControlStateNormal];

    } else {
      //  self.chooseButton.hidden = YES;
        [self.chooseButton setTitle:@"Check-In" forState:UIControlStateNormal];
    }
    [self.chooseButton makeRoundedBorderWithRadius:3];

    /*
    self.sportsCV.delegate = self;
    self.sportsCV.dataSource = self;
    
    self.picsCV.delegate = self;
    self.picsCV.dataSource = self;
    
    self.sportsCV.tag = TAG_SPORTS_CV;
    self.picsCV.tag = TAG_PICS_CV;
    */
    [self showNoResultsOverlay:YES];
    double delayInSeconds = 15.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        //code to be executed on the main queue after delay
        [self.tempView removeFromSuperview];
        self.tempView.hidden = YES;
        self.tableView.scrollEnabled = YES;
    });
    [self colorButtons];
//    [self setVenueInfo];
}

- (void)colorButtons
{
 //   [self.rateButtonBackground setBackgroundColor:[UIColor midGray2]];
    self.rateIcon.image = [[UIImage imageNamed:@"Rate Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.rateIcon setTintColor:[UIColor darkGrayColor2]];
     [self.rateLabel setTextColor:[UIColor darkGrayColor2]];
    
  //  [self.flagButtonBackground setBackgroundColor:[UIColor midGray2]];
    self.flagIcon.image = [[UIImage imageNamed:@"Flag Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.flagIcon setTintColor:[UIColor darkGrayColor2]];
    [self.flagButtonLable setTextColor:[UIColor darkGrayColor2]];
    
//    [self.heartBackgroundView setBackgroundColor:[UIColor midGray2]];
    self.heartIcon.image = [[UIImage imageNamed:@"Heart Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.heartIcon setTintColor:[UIColor darkGrayColor2]];
    [self.heartButtonLabel setTextColor:[UIColor darkGrayColor2]];
    
//    [self.cameraButtonBackground setBackgroundColor:[UIColor midGray2]];
    self.cameraIcon.image = [[UIImage imageNamed:@"Camera Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.cameraIcon setTintColor:[UIColor darkGrayColor2]];
    [self.cameraButtonLabel setTextColor:[UIColor darkGrayColor2]];
  
}

- (void)showNoResultsOverlay:(BOOL)shouldShow
{
    if (shouldShow) {
        self.tableView.scrollEnabled = NO;
        self.tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
        [self.tempView setBackgroundColor:[UIColor smokeBackground]];
        
        self.activityIndicator = [[UIActivityIndicatorView alloc] init];
        [self.activityIndicator setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width/2.0, [UIScreen mainScreen].bounds.size.height/2.0 - 50)];
        [self.activityIndicator startAnimating];
        [self.tempView addSubview:self.activityIndicator];
        
        [self.view addSubview:self.tempView];
        
    } else {
        self.tableView.scrollEnabled = YES;
        [self.activityIndicator removeFromSuperview];
        [self.tempView removeFromSuperview];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setVenueInfo];
    
  //  if ([self.preferences getVenueDetailPageRefreshState]) {
  //      [self.preferences setVenueDetailPageRefreshState:NO];
  //      [self setVenueInfo];
  //  }
    //[self assignMapImage];
}



- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
 //   [self zoomMapViewToFitAnnotations:self.mapView animated:NO];
 //   self.descriptionLabel = [self makeDynamicHeightLabel:self.venue.description  forLabel:self.descriptionLabel];
    
    
}
- (IBAction)rateVenueAction:(id)sender
{
    if (!self.venue.isTemporary)
        [self performSegueWithIdentifier:@"rate segue" sender:self];
    else {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Temporary Venue Error";
        alert.message = @"Sorry but you cannot rate a temporary venue";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
        
    }
}

- (void)setVenueInfo
{

    [self.theKingsOfVenue removeAllObjects];
    
    [IOSRequest fetchVenueDetailForID:self.venueID
                      withSessionUser:[self.preferences getUserID]
                         onCompletion:^(NSDictionary *results) {
                             
                             NSMutableArray *sportsArray = [[NSMutableArray alloc] init];
                             for (id sportObj in results[@"venue_sports"]) {
                                 Sport *sport = [[Sport alloc] initWithSportDetails:[sportObj[@"sport_id"] integerValue]
                                                                            andName:sportObj[@"sport_name"]
                                                                            andType:nil];
                                 [sportsArray addObject:sport];
                             }
                             
                             AvgRating *theRating = [[AvgRating alloc] initWithAvgRating:[results[@"venue_avg_rating"] floatValue]
                                                                              andRatings:[AvgRating makeRatingsArray:results[@"venue_ratings"]]];
                             
                             self.venue = [[Venue alloc] initVenueWithName:results[@"name"]
                                                                  andVenueID:self.venueID
                                                                  andAddress:results[@"street"]
                                                                     andCity:results[@"city"]
                                                                andStateName:results[@"state"]
                                                                  andStateID:[results[@"state_id"] integerValue]
                                                                 andLatitude:[results[@"latitude"] floatValue]
                                                                andLongitude:[results[@"longitude"] floatValue]
                                                              andDescription:results[@"description"]
                                                                  andCreator:[results[@"creator_id"] integerValue]
                                                                   andSports:sportsArray
                                                              andAvgRating:theRating];
                             
                             [self.venue setIsTemporary:[results[@"is_temporary"] boolValue]];
                             
                             id venuePicObj = results[@"venue_pic_info"];
                             VenueImage *venuePicInfo = [[VenueImage alloc] initWithVenueImageInfo:venuePicObj[@"filestring"]
                                                                                          andImage:nil
                                                                                         isDefault:YES];
                             [self.venue setVenueImageThumb:venuePicInfo];
                             
                             // Now load all venue pics besides just the thumbnail pic that has already been loaded
                             
                             for (id venuePicsArrayObj in results[@"venue_pics_array"]) {
                                 VenueImage *vI = [[VenueImage alloc] initWithVenueImageInfo:venuePicsArrayObj[@"filestring"]
                                                                                    andImage:nil
                                                                                   isDefault:NO];
                                 [self.venue.picsArray addObject:vI];
                             }
                             
                             for (id kingObject in results[@"kings"]) {
                                 Player *theKing = [[Player alloc] initWithPlayerDetails:[kingObject[@"user_id"] integerValue]
                                                                             andUserName:kingObject[@"username"]
                                                                     andProfilePicString:kingObject[@"profile_pic_string"]];
                                 // not setting a level or sport record for now
                                 PlayerSport *kingSport = [[PlayerSport alloc] initWithSportDetails:[kingObject[@"sport_id"] integerValue]
                                                                                            andName:kingObject[@"sport_name"]
                                                                                            andType:@"individual"
                                                                                           andLevel:@"0"
                                                                                     andSportRecord:nil];
                                 [theKing addPlayerSport:kingSport];
                                 [self.theKingsOfVenue addObject:theKing];
                             }
                             
                             // do checkins
                             
                             for (id object in results[@"check_ins"]) {
                                 NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
                                 [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                                 NSDate *dateTime = [[formatter dateFromString:object[@"check_in_ts"]] toLocalTime];
                                 
                                 Player *checkInPlayer = [[Player alloc] initWithPlayerDetails:[object[@"user_id"] integerValue]
                                                                                   andUserName:object[@"username"]
                                                                           andProfilePicString:object[@"profile_pic_string"]];
                                 
                                 VenueCheckIn *vci = [[VenueCheckIn alloc] initWithCheckInID:[object[@"checkin_id"] integerValue]
                                                                           checkedInDateTime:dateTime
                                                                            checkedInMessage:object[@"message"]
                                                                                    isPublic:[object[@"is_public"] boolValue]
                                                                                       isNow:[object[@"is_now"] boolValue]
                                                                               checkedInUser:checkInPlayer
                                                                              checkedInSport:[[Sport alloc] initWithSportDetails:[object[@"sport_id"] integerValue]
                                                                                                                         andName:object[@"sport_name"]
                                                                                                                         andType:nil]];
                                 [self.venue.venueCheckInsArray addObject:vci];
                                 
                             }
                             
                            
                             self.checkinStats = [[VenueCheckinStats alloc] initWithVenueCheckinStatsAllTime:[results[@"check_in_stats_all_time"] integerValue]
                                                                                                     andCheckinStatForHour:[results[@"check_in_stats_hour"] integerValue]
                                                                                         andCheckinStatForHourForFollowing:[results[@"check_in_stats_hour_for_following"] integerValue]];
                             
                             
                             dispatch_async(dispatch_get_main_queue(), ^{
                                 
                                 self.navigationItem.title = results[@"name"];
                                 if ([results count] != 0) {
                                     [self showNoResultsOverlay:NO];
                                 }
                                 self.venueNameLabel.text = results[@"name"];
                                 if([results[@"street"] length] == 0 && [results[@"city"] length] == 0 && [results[@"state"] length] == 0) {
                                     self.streetLabel.text = [NSString stringWithFormat:@"Longitude: %f", [results[@"longitude"] floatValue]];
                                     self.cityStateLabel.text = [NSString stringWithFormat:@"Latitude: %f", [results[@"latitude"] floatValue]];
                                 } else {
                                     if ([results[@"street"] length] > 0)
                                         self.streetLabel.text = results[@"street"];
                                     else {
                                         self.streetLabel.text = @"Street Not Specified";
                                         self.streetLabel.font = [UIFont fontWithName:@"HelveticaNeue-LightItalic" size:13];
                                     }
                                     self.cityStateLabel.text = [NSString stringWithFormat:@"%@, %@", results[@"city"], results[@"state"]];
                                 }
                                 
        //                         self.descriptionLabel.text = self.venue.description;
                                 self.ratingLabel.text = [NSString stringWithFormat:@"%.0f rating", theRating.avgRatingValue];
                                 self.starImage.image = [theRating getRatingImage];
                              
                                 
                                 self.venueThumbPic.contentMode = UIViewContentModeScaleAspectFill;
                                 self.venueThumbPic.clipsToBounds = YES;
                                 
                                 NSString *url = [S3Tools getFileNameStringWithType:@"venue_pic"
                                                                         baseString:self.venue.venueImageThumb.venuePicString];
                                 
                                 if ([self.venue.venueImageThumb.venuePicString length] == 0) {
                                     UILabel *noPicLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, 50, 260, 50)];
                                     noPicLabel.text = @"No Venue Pics";
                                     noPicLabel.textAlignment = NSTextAlignmentCenter;
                                     noPicLabel.font = [UIFont fontWithName:@"HelveticaNeue-LightItalic" size:25];
                                     noPicLabel.textColor = [UIColor lightGrayColor];
                                     [self.view addSubview:noPicLabel];
                                 }
                                 
                                 [self.venueThumbPic setImageWithURL:[NSURL URLWithString:url]
                                               placeholderImage:[UIImage imageNamed:@"Venue Default"]];
                                 
                                 
                                 if ([results[@"is_favorite"] boolValue]) {
                                     self.isFavorite = YES;
                                   //  [self.heartBackgroundView setBackgroundColor:[UIColor midGray]];
                                     self.heartIcon.image = [[UIImage imageNamed:@"Heart Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                                     [self.heartIcon setTintColor:[UIColor salmonColor]];
                                     [self.heartButtonLabel setTextColor:[UIColor salmonColor]];
                                 } else {
                                     self.isFavorite = NO;
                                 }
                                 
                                 if (self.venue.isTemporary) {
                                     CGRect tempLabelFrame = CGRectMake(self.starImage.frame.origin.x - 20, self.starImage.frame.origin.y +3, 180, 21);
                                     UILabel *tempLabel = [[UILabel alloc] initWithFrame:tempLabelFrame];
                                     tempLabel.text = @"Unofficial Venue";
                                     self.starImage.hidden = YES;
                                     tempLabel.textColor = [UIColor salmonColor];
                                     tempLabel.font = [UIFont fontWithName:@"HelveticaNeue-LightItalic" size:18];
                                     [self.subView1 addSubview:tempLabel];
                                     self.viewRatingsButton.enabled = NO;
                                     self.viewRatingsLabel.hidden = YES;
                                 }
                                 
                                 [self.sportsCV reloadData];
                                 self.doneLoading = YES;
                                 [self.tableView reloadData];
                                 
                                 
                                 
                             });
                             
                      
                             
                         }];
}


/*
#define MINIMUM_ZOOM_ARC 0.014 //approximately 1 miles (1 degree of arc ~= 69 miles)
#define ANNOTATION_REGION_PAD_FACTOR 1.15
#define MAX_DEGREES_ARC 360
//size the mapView region to fit its annotations
- (void)zoomMapViewToFitAnnotations:(MKMapView *)mapView animated:(BOOL)animated
{
    NSArray *annotations = mapView.annotations;
    int count = [mapView.annotations count];
    if ( count == 0) { return; } //bail if no annotations
    
    //convert NSArray of id <MKAnnotation> into an MKCoordinateRegion that can be used to set the map size
    //can't use NSArray with MKMapPoint because MKMapPoint is not an id
    MKMapPoint points[count]; //C array of MKMapPoint struct
    for( int i=0; i<count; i++ ) //load points C array by converting coordinates to points
    {
        CLLocationCoordinate2D coordinate = [(id <MKAnnotation>)annotations[i] coordinate];
        points[i] = MKMapPointForCoordinate(coordinate);
    }
    //create MKMapRect from array of MKMapPoint
    MKMapRect mapRect = [[MKPolygon polygonWithPoints:points count:count] boundingMapRect];
    //convert MKCoordinateRegion from MKMapRect
    MKCoordinateRegion region = MKCoordinateRegionForMapRect(mapRect);
    
    //add padding so pins aren't scrunched on the edges
    region.span.latitudeDelta  *= ANNOTATION_REGION_PAD_FACTOR;
    region.span.longitudeDelta *= ANNOTATION_REGION_PAD_FACTOR;
    //but padding can't be bigger than the world
    if( region.span.latitudeDelta > MAX_DEGREES_ARC ) { region.span.latitudeDelta  = MAX_DEGREES_ARC; }
    if( region.span.longitudeDelta > MAX_DEGREES_ARC ){ region.span.longitudeDelta = MAX_DEGREES_ARC; }
    
    //and don't zoom in stupid-close on small samples
    if( region.span.latitudeDelta  < MINIMUM_ZOOM_ARC ) { region.span.latitudeDelta  = MINIMUM_ZOOM_ARC; }
    if( region.span.longitudeDelta < MINIMUM_ZOOM_ARC ) { region.span.longitudeDelta = MINIMUM_ZOOM_ARC; }
    //and if there is a sample of 1 we want the max zoom-in instead of max zoom-out
    if( count == 1 )
    {
        region.span.latitudeDelta = MINIMUM_ZOOM_ARC;
        region.span.longitudeDelta = MINIMUM_ZOOM_ARC;
    }
    [self.mapView setRegion:region animated:animated];
}

- (void)assignMapImage
{
    NSArray *annotations = self.mapView.annotations;
    CLLocationCoordinate2D coordinate = [(id <MKAnnotation>)annotations[0] coordinate];
    NSString *staticMapUrl = [NSString stringWithFormat:@"http://maps.google.com/maps/api/staticmap?markers=color:red|%f,%f&%@&sensor=true",coordinate.latitude, coordinate.longitude,@"zoom=16&size=320x185"];
    NSURL *mapUrl = [NSURL URLWithString:[staticMapUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    self.mapImage.image = [UIImage imageWithData: [NSData dataWithContentsOfURL:mapUrl]];
    self.mapImage.userInteractionEnabled = YES;
    UITapGestureRecognizer *pgr = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self action:@selector(handleTap:)];
    //    pgr.delegate = self.view;
    [self.mapImage addGestureRecognizer:pgr];
    
    
    
}
 
 */

/*
- (void)handleTap:(UITapGestureRecognizer *)tapGestureRecognizer
{
    [self performSegueWithIdentifier:@"large map view segue" sender:self];
}
*/

- (void)setWithTapAction:(VDMapCell *)controller shouldSegue:(BOOL)shouldSegue
{
    if (shouldSegue)
         [self performSegueWithIdentifier:@"large map view segue" sender:self];
}

/*
- (void)handleTapOnVenuePic:(UITapGestureRecognizer *)tapGestureRecognizer
{
    VenueDetailPicsCVC *cell = (VenueDetailPicsCVC *)tapGestureRecognizer.view.superview.superview;
    NSIndexPath *selectedIndexPath = [self.picsCV indexPathForCell:cell];
    self.selectedPicRow = selectedIndexPath.row;
    
    [self performSegueWithIdentifier:@"Image Detail Segue" sender:self];
}
*/
- (void)setWithPicToShowEnlarged:(VDPhotosCell *)controller withPicNum:(NSInteger)picNum Center:(CGPoint)center Offset:(CGFloat)offset
{
    self.selectedPicRow = picNum;
    self.center = center;
    self.offset = offset;
    [self performSegueWithIdentifier:@"Image Detail Segue" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"large map view segue"]) {
        MapOverlayVC *controller = (MapOverlayVC *)segue.destinationViewController;
        controller.venue = self.venue;
    } else if ([segue.identifier isEqualToString:@"rate segue"]) {
        RateVenueTVC *controller = (RateVenueTVC *)segue.destinationViewController;
        controller.venueID = self.venueID;
    } else if ([segue.identifier isEqualToString:@"flag segue"]) {
        ReportVenueTVC *controller = (ReportVenueTVC *)segue.destinationViewController;
        controller.venueID = self.venueID;
    } else if ([segue.identifier isEqualToString:@"ratings list segue"]) {
        RatingsVenuesListTVC *controller = (RatingsVenuesListTVC *)segue.destinationViewController;
        controller.venueID = self.venueID;
    } else if ([segue.identifier isEqualToString:@"Image Detail Segue"]) {
        VenuePicDetailVC *controller = segue.destinationViewController;
        controller.venueImage = [self.venue.picsArray objectAtIndex:self.selectedPicRow];
        NSLog(@"shit %f",self.center.x);
        CGPoint point = CGPointMake(self.center.x + 37.5, self.totalY + 72 + 37.5);
        controller.originX = self.center.x;
        controller.originY = self.center.y;
        ((ImageSegue *)segue).originatingPoint = point;
        ((ImageSegue *)segue).contentoffset = self.tableView.contentOffset.y;
        //controller.contentoffset = self.tableView.contentOffset.y;
        /*((ImageSegue *)segue).navigationBarHeight = self.navigationController.navigationBar.bounds.size.height;
        ((ImageSegue *)segue).tabBarHeight = self.tabBarController.tabBar.bounds.size.height;*/
        //controller.tabBarHeight = self.tabBarController.tabBar.bounds.size.height;
        NSString *url = [S3Tools getFileNameStringWithType:@"venue_pic"
                                                baseString:controller.venueImage.venuePicString];
        UIImageView *tmp = [[UIImageView alloc] init];
        [tmp setImageWithURL:[NSURL URLWithString:url]
                      placeholderImage:[UIImage imageNamed:@"Landscape Placeholder"]];
        self.showupImage = tmp.image;
        ((ImageSegue *)segue).image = self.showupImage;
        ((ImageSegue *)segue).scrollOffset = self.offset;
    } else if ([segue.identifier isEqualToString:@"Add Pics Segue"]) {
        SetVenuePicsVC *controller = segue.destinationViewController;
        controller.venueID = self.venueID;
        controller.numVCsToPopBack = 2;
    } else if ([segue.identifier isEqualToString:@"Player Detail Segue"]) {
        PlayerDetailTVC *controller = segue.destinationViewController;
        controller.playerUserID = self.selectedUser;
    } else if ([segue.identifier isEqualToString:@"Check In Segue"]) {
        CheckInTVC *controller = segue.destinationViewController;
        controller.venueID = self.venueID;
        controller.venueName = self.venue.venueName;
    } else if ([segue.identifier isEqualToString:@"CheckIn History Segue"]) {
        CheckInHistoryTVC *controller = segue.destinationViewController;
        controller.venueID = self.venueID;
        controller.showFollowingInCheckinHistory = self.showFollowingInCheckinHistory;
    }
}

/* It is necessary for unwind segue, but do nothing */
- (IBAction) unwindFromViewController:(UIStoryboardSegue *)segue
{
    CGPoint point = CGPointMake(self.center.x + 37.5, self.totalY + 72 + 37.5);
    ((ImageSegue *)segue).originatingPoint = point;
    ((ImageSegue *)segue).scrollOffset = self.offset;
    UIImageView *tmp = [[UIImageView alloc] init];
    VenuePicDetailVC *vc = (VenuePicDetailVC *)segue.sourceViewController;
    ((ImageSegue *)segue).showupView = vc.imageView;
    ((ImageSegue *)segue).originFrame = vc.frame;
}

- (void)goToProfile:(UIButton *)sender
{
    self.selectedUser = sender.tag;
    [self performSegueWithIdentifier:@"Player Detail Segue" sender:self];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
 /*   NSInteger numSections = 2; // default minimum number of sections
    
    if (self.doneLoading) {
        
        if (!self.venue.isTemporary)
            numSections++;
        
        
        if ([self.venue.description length] > 0)
            numSections++;
        
        if ([self.venue.picsArray count] > 0)
            numSections ++;
        
        return numSections;
    }
    
    
    return  0; */
    
    NSInteger numSections = 5;
    
    if ([self.theKingsOfVenue count] > 0)
        numSections += 1;
    
    if ([self.venue.venueCheckInsArray count] > 0)
        numSections += 1;
    
    return numSections;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == SECTION_KING) {
        if (!self.venue.isTemporary && [self.theKingsOfVenue count] > 0)
            return 190;
        else
            return 0;
    } else if (indexPath.section == SECTION_MAP)
        if (!self.venue.isTemporary)
            return 185.0;
        else
            return 0;
    else if (indexPath.section == SECTION_SPORTS)
        return 130.0;
    else if (indexPath.section == SECTION_DESCRIPTION) {
        if ([self.venue.description length] > 0)
            return [self calculateHeightForDescriptionSection]; // was 98
        else
            return 0;
    } else if (indexPath.section == SECTION_PHOTOS) {
        if ([self.venue.picsArray count] > 0)
            return 145.0;
        else
            return 0;
    } else if (indexPath.section == SECTION_CHECKINS && [self.venue.venueCheckInsArray count] > 0) {
       return [self.venue.venueCheckInsArray count] * 70 + 30 + 30;
        
    } else if (indexPath.section == SECTION_CHECKIN_STATS) {
        return 102;
    }
    
    
    return 0;
}
#define CELL_CONTENT_WIDTH 287
#define CELL_CONTENT_MARGIN 23
#define FONT_SIZE 13

- (CGFloat)calculateHeightForDescriptionSection
{
    NSString *text = self.venue.description;
    
    CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH, 1000.0f);
    
    /********** because sizeWithFont deprecated *************/
    
    NSMutableDictionary *attributes = [NSMutableDictionary new];
    [attributes setObject:[UIFont systemFontOfSize:FONT_SIZE] forKey:NSFontAttributeName];
    
    CGRect sizeRect = [text boundingRectWithSize:constraint options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
    
    CGSize size = sizeRect.size;
    //      CGSize size = [text sizeWithFont:[UIFont systemFontOfSize:FONT_SIZE] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    
    /********* end because sizeWithFont deprecated *********/
    
    return size.height + 80;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == SECTION_KING) {
        VDKingCell *cell = [tableView dequeueReusableCellWithIdentifier:@"King Cell" forIndexPath:indexPath];
        
        // clear subviews
        if ([cell.myBackgroundView subviews]){
            for (UIView *subview in [cell.myBackgroundView subviews]) {
                if (subview.tag == 1)
                    [subview removeFromSuperview];
                
            }
        }
    
        
        cell.delegate = self;
        
        cell.kingsArray = self.theKingsOfVenue;
        
        if (self.doneLoading && [self.theKingsOfVenue count] == 0) {
        /*    UILabel *kingMissingLabel = [[UILabel alloc] initWithFrame:CGRectMake(cell.kingsCollectionView.frame.origin.x, cell.kingsCollectionView.frame.origin.y + 30, cell.kingsCollectionView.frame.size.width, 30)];
            kingMissingLabel.textColor = [UIColor lightGrayColor];
            kingMissingLabel.font = [UIFont fontWithName:@"HelveticaNeue-LightItalic" size:15];
            kingMissingLabel.text = @"There is no king for this venue";
            kingMissingLabel.textAlignment = NSTextAlignmentCenter;
            kingMissingLabel.tag = 1;
            
            [cell.contentView addSubview:kingMissingLabel];
            
            cell.kingsPageControl.hidden = YES;
            cell.kingsCollectionView.hidden = YES;*/

        } else {
            [cell initializeKingsCell];
            
            [cell.kingsCollectionView reloadData];

        }
        
        return cell;
        
    } else if (indexPath.section == SECTION_MAP) {
        VDMapCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Map Cell" forIndexPath:indexPath];
        
        if (cell.mapImage.tag != 1) {
            [cell initializeVenueMap:self.venue];
            cell.delegate = self;
        }
        
        return cell;
    } else if (indexPath.section == SECTION_SPORTS) {
        VDSportsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Sports Cell" forIndexPath:indexPath];
        [cell initializeCollectionView];
        cell.sportsArray = self.venue.sportsArray;
        [cell.sportsCollectionView reloadData];
        
        return cell;
    } else if (indexPath.section == SECTION_DESCRIPTION) {
        VDDescriptionCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Description Cell" forIndexPath:indexPath];
      //  cell.descriptionTextView.text = self.venue.description;
        cell.descriptionLabel.text = self.venue.description;
        cell.descriptionIcon.image = [[UIImage imageNamed:@"Info Icon 2"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        cell.descriptionIcon.tintColor = [UIColor colorWithRed:34/255.0 green:34/255.0 blue:34/255.0 alpha:1.0];
        
        return cell;
    } else if (indexPath.section == SECTION_PHOTOS) {
        VDPhotosCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Photos Cell" forIndexPath:indexPath];
        [cell initializePicsCollectionView];
        cell.delegate = self;
        cell.picsArray = self.venue.picsArray;
        cell.cameraIcon.image = [[UIImage imageNamed:@"Camera Icon 2"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        cell.cameraIcon.tintColor = [UIColor darkGrayColor2];
        [cell.picsCollectionView reloadData];
        CGRect rectOfCellInTableView = [tableView rectForRowAtIndexPath:indexPath];
        //CGRect rectOfCellInSuperview = [tableView convertRect:rectOfCellInTableView toView:[tableView superview]];
        
        NSLog(@"Y of Cell is: %f", rectOfCellInTableView.origin.y);
        self.totalY = rectOfCellInTableView.origin.y;
        return cell;
    } else if (indexPath.section == SECTION_CHECKINS) {
        VDCheckInsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CheckIns Cell" forIndexPath:indexPath];
        
        // clear subviews
        if ([cell.myBackgroundView subviews]){
            for (UIView *subview in [cell.myBackgroundView subviews]) {
                if ([subview isKindOfClass:[SpecialUIImageView class]]) {
                    SpecialUIImageView * newSubview = (SpecialUIImageView *)subview;
                    if (newSubview.shouldRemoveFromMySuperview)  // only items I created programmatically
                        [subview removeFromSuperview];
                    
                } else if ([subview isKindOfClass:[SpecialUILabel class]]) {
                    SpecialUILabel *newSubview = (SpecialUILabel *)subview;
                    if (newSubview.shouldRemoveFromMySuperview)
                        [subview removeFromSuperview];
                }
                
            }
        }
        
       
        NSInteger startingY = 20 + 30;
        
        for (VenueCheckIn *vci in self.venue.venueCheckInsArray) {
            
            // pic
            SpecialUIImageView *pic = [[SpecialUIImageView alloc] initWithFrame:CGRectMake(15, startingY, 40, 40)];
            pic.shouldRemoveFromMySuperview = YES;
            NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                    baseString:vci.checkedInUser.profileBaseString];
            
            [pic setImageWithURL:[NSURL URLWithString:url]
                      placeholderImage:[UIImage imageNamed:@"avatar square"]];
            
            [UIImage makeRoundedImage:pic withRadius:10];
            
            [cell.myBackgroundView addSubview:pic];
            
            // profile pic button
            UIButton *picButton1 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
            picButton1.tag = vci.checkedInUser.userID;
            [picButton1 addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
            [pic addSubview:picButton1];
            pic.userInteractionEnabled = YES;
            // end profile pic button

            
            
            // label for username
            
            SpecialUILabel *usernameLabel = [[SpecialUILabel alloc] initWithFrame:CGRectMake(65, startingY-15, 220, 63)];
            usernameLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:13];
         //   usernameLabel.text = vci.checkedInUser.userName;
            usernameLabel.shouldRemoveFromMySuperview = YES;
            usernameLabel.numberOfLines = 0;
            
            NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ %@", vci.checkedInUser.userName, [vci getCheckedInStatusMessage]]];
            
            [string addAttribute:NSForegroundColorAttributeName value:[UIColor goldColor] range:NSMakeRange(0, [vci.checkedInUser.userName length])];
            usernameLabel.attributedText = string;

            
            [cell.myBackgroundView addSubview:usernameLabel];
            
            // label for date
            
            // check in detail label
            
        //    SpecialUILabel *checkinDetailLabel = [[SpecialUILabel alloc] initWithFrame:CGRectMake(65, startingY + 18, 220, 36)];
        //    checkinDetailLabel.shouldRemoveFromMySuperview = YES;
            /*
            if (vci.isNow)
                checkinDetailLabel.text = [NSString stringWithFormat:@"checked into this location on %@ to play %@", dateText, [vci.checkedInSport.sportName capitalizedString]];
            else
                checkinDetailLabel.text = [NSString stringWithFormat:@"will be at this venue on %@ to play %@", dateText, [vci.checkedInSport.sportName capitalizedString]];
            */
          /*  checkinDetailLabel.text = [vci getCheckedInStatusMessage];
            
            checkinDetailLabel.font = [UIFont systemFontOfSize:12];
            checkinDetailLabel.textColor = [UIColor lightGrayColor];
            [cell.contentView addSubview:checkinDetailLabel];
            checkinDetailLabel.numberOfLines = 2;
            checkinDetailLabel.lineBreakMode = NSLineBreakByWordWrapping;
           */
            /*
            // label for message
            UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(65, startingY + 18, 200, 18)];
            messageLabel.font = [UIFont systemFontOfSize:12];
            messageLabel.textColor = [UIColor lightGrayColor];
            messageLabel.numberOfLines = 0;
            messageLabel.text = [NSString stringWithFormat:@"\"%@\"", vci.message];
            [cell.contentView addSubview:messageLabel];
            */
            startingY += 65;
        }
        
        
        return cell;
    } else if (indexPath.section == SECTION_CHECKIN_STATS) {
       
        VDCheckInStatsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Check In Stats Cell" forIndexPath:indexPath];
       
        cell.statCheckinsForHourLabel.text = [NSString stringWithFormat:@"%ld", self.checkinStats.checkInStatsHour];
        cell.statCheckinsForHourForFollowingLabel.text = [NSString stringWithFormat:@"%ld that you follow", self.checkinStats.checkInStatsHourForFollowing];
        cell.statCheckinsAllTimeLabel.text = [NSString stringWithFormat:@"%ld all time", self.checkinStats.checkInStatsAllTime];
        
        return cell;
    }
    
    return nil;
}
/*
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView.tag == TAG_SPORTS_CV)
        return [self.venue.sportsArray count];
    else if (collectionView.tag == TAG_PICS_CV)
        return [self.venue.picsArray count];
    
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView.tag == TAG_SPORTS_CV) {
        static NSString *cellIdentifier = @"Sport Cell";
    
        VenueDetailSportsCVC *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
        Sport *sport = [self.venue.sportsArray objectAtIndex:indexPath.row];
    
        cell.sportIcon.image = [sport getSportIcon];
        cell.sportName.text = [sport.sportName capitalizedString];
    
        return cell;
    } else if (collectionView.tag == TAG_PICS_CV) {
        
        static NSString *cellIdentifier = @"Pic Cell";
        
        VenueDetailPicsCVC *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
        
        cell.venuePic.contentMode = UIViewContentModeScaleAspectFill;
        cell.venuePic.clipsToBounds = YES;
        
        VenueImage *vI = [self.venue.picsArray objectAtIndex:indexPath.row];
        
        NSString *url = [S3Tools getFileNameStringWithType:@"venue_pic"
                                                baseString:vI.venuePicString];
        
        [cell.venuePic setImageWithURL:[NSURL URLWithString:url]
                      placeholderImage:[UIImage imageNamed:@"Landscape Placeholder"]];

        UITapGestureRecognizer *pgr = [[UITapGestureRecognizer alloc]
                                       initWithTarget:self action:@selector(handleTapOnVenuePic:)];
        cell.venuePic.userInteractionEnabled = YES;
        [cell.venuePic addGestureRecognizer:pgr];
        
        return  cell;
    }
    
    return nil;
}
*/
- (IBAction)addPics:(id)sender
{
    [self performSegueWithIdentifier:@"Add Pics Segue" sender:self];
}

- (IBAction)favoriteAction:(id)sender
{
    [IOSRequest changeFavoriteStatusForVenue:self.venueID
                                  withStatus:!self.isFavorite // will change to opposite favorite status
                                    withUser:[self.preferences getUserID]
                                onCompletion:^(NSDictionary *results) {
                                    
                                    if ([results[@"is_favorite"] boolValue]) {
                                        self.isFavorite = YES;
                                   //     [self.heartBackgroundView setBackgroundColor:[UIColor midGray]];
                                        self.heartIcon.image = [[UIImage imageNamed:@"Heart Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                                        [self.heartIcon setTintColor:[UIColor salmonColor]];
                                        [self.heartButtonLabel setTextColor:[UIColor salmonColor]];
                                    } else {
                                        self.isFavorite = NO;
                                    //    [self.heartBackgroundView setBackgroundColor:[UIColor midGray]];
                                        self.heartIcon.image = [[UIImage imageNamed:@"Heart Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                                        [self.heartIcon setTintColor:[UIColor darkGrayColor]];
                                        [self.heartButtonLabel setTextColor:[UIColor darkGrayColor]];

                                    }
                                }];
}
// choose button is only used when creating a match.  otherwise, it will be a "check-in" button
- (IBAction)chooseVenue:(id)sender
{
    if (self.showChooseButton) {
        [self.delegate setVenueDetailTViewController:self withVenue:self.venue];
        NSInteger currentIndex = [self.navigationController.viewControllers indexOfObject:self];

        [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:currentIndex-2] animated:YES];
    } else {
        [self performSegueWithIdentifier:@"Check In Segue" sender:self];
    }
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = nil;

    if (section == SECTION_KING && !self.venue.isTemporary) {
   //     view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 130)];
   //     /* Create custom view to display section header... */
   //     UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, 40, tableView.frame.size.width - 10, 30)];
   //     [label setFont:[UIFont boldSystemFontOfSize:25]];
   //     NSString *string = @"King Of The Venue";
        /* Section header is in 0th index... */
   //     [label setText:string];
   //     [label setTextColor:[UIColor lightGrayColor]];
   //     [view addSubview:label];
        
   //     UILabel *subLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, label.frame.size.height + label.frame.origin.y - 5, tableView.frame.size.width - 40, 60)];
   //     subLabel.text = @"This title is reserved for the player who has defeated the most unique opponents at this location.";
   //     [subLabel setFont:[UIFont systemFontOfSize:14]];
   //     subLabel.numberOfLines = 3;
   //     subLabel.contentMode = NSLineBreakByWordWrapping;
   //     subLabel.textColor = [UIColor lightGrayColor];
        
   //     [view addSubview:subLabel];
        
   //     return view;

    } else if (section == SECTION_MAP && !self.venue.isTemporary) {
    //    view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 60)];
    //    /* Create custom view to display section header... */
    //    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, 25, tableView.frame.size.width, 30)];
    //    [label setFont:[UIFont boldSystemFontOfSize:18]];
    //    NSString *string = @"Venue Map";
        /* Section header is in 0th index... */
    //    [label setText:string];
    //    [label setTextColor:[UIColor lightGrayColor]];
     //   [label setTextAlignment:NSTextAlignmentCenter];
     //   [view addSubview:label];
        
        return view;
    } else if (section == SECTION_SPORTS) {
    //    view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 60)];

        /* Create custom view to display section header... */
    //    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, 25, tableView.frame.size.width, 30)];
    //    [label setFont:[UIFont boldSystemFontOfSize:18]];
    //    NSString *string = @"Sports Played Here";
        /* Section header is in 0th index... */
    //    [label setText:string];
     //   [label setTextColor:[UIColor lightGrayColor]];
     //   [label setTextAlignment:NSTextAlignmentCenter];
     //   [view addSubview:label];
        
     //   return view;
    } else if (section == SECTION_DESCRIPTION && [self.venue.description length] > 0) {
     //   view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 60)];

        /* Create custom view to display section header... */
    //    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, 40, tableView.frame.size.width - 10, 30)];
    //    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, 25, tableView.frame.size.width, 30)];

      //  [label setFont:[UIFont boldSystemFontOfSize:18]];
     //   NSString *string = @"Venue Description";
        /* Section header is in 0th index... */
     //   [label setText:string];
     //   [label setTextColor:[UIColor lightGrayColor]];
      //  [label setTextAlignment:NSTextAlignmentCenter];
    //    [view addSubview:label];
        
        return view;
    } else if (section == SECTION_PHOTOS && [self.venue.picsArray count] > 0) {
//        view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 60)];

        /* Create custom view to display section header... */
//        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, 25, tableView.frame.size.width, 30)];
//        [label setFont:[UIFont boldSystemFontOfSize:18]];
//        NSString *string = @"Venue Photos";
        /* Section header is in 0th index... */
//        [label setText:string];
//        [label setTextColor:[UIColor lightGrayColor]];
    //    [label setTextAlignment:NSTextAlignmentCenter];
//        [view addSubview:label];
        
//        return view;
    }  else if (section == SECTION_CHECKINS && [self.venue.venueCheckInsArray count] > 0) {
   //     view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 60)];
        
        /* Create custom view to display section header... */
   //     UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, 25, tableView.frame.size.width, 30)];
   //     [label setFont:[UIFont boldSystemFontOfSize:18]];
   //     NSString *string = @"Check-Ins";
        /* Section header is in 0th index... */
   //     [label setText:string];
   //     [label setTextColor:[UIColor lightGrayColor]];
        //    [label setTextAlignment:NSTextAlignmentCenter];
    //    [view addSubview:label];
        
    //    return view;
    } else if (section == SECTION_CHECKIN_STATS) {
      //  view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 60)];
        
        /* Create custom view to display section header... */
      //  UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, 25, tableView.frame.size.width, 30)];
       // [label setFont:[UIFont boldSystemFontOfSize:18]];
     //   NSString *string = @"Venue Stats";
        /* Section header is in 0th index... */
   //     [label setText:string];
 //       [label setTextColor:[UIColor lightGrayColor]];
      //  [view addSubview:label];
        
        return view;
    }
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == SECTION_KING && !self.venue.isTemporary) {
        return 5;
    } else if (section == SECTION_MAP && !self.venue.isTemporary) {
        return 5;
    } else if (section == SECTION_SPORTS) {
        return 5;
    } else if (section == SECTION_DESCRIPTION && [self.venue.description length] > 0) {
        return 5;
    } else if (section == SECTION_PHOTOS && [self.venue.picsArray count] > 0) {
        return 5;
    } else if (section == SECTION_CHECKINS && [self.venue.venueCheckInsArray count] > 0) {
        return 5;
    } else if (section == SECTION_CHECKIN_STATS) {
        return 5;
    }
    return  0;
}
/*
- (UILabel *)makeDynamicHeightLabel:(NSString *)myText forLabel:(UILabel *)myLabel
{
    CGSize maxLabelSize = CGSizeMake(280,300);
    
    NSMutableDictionary *attributes = [NSMutableDictionary new];
    [attributes setObject:[UIFont systemFontOfSize:myLabel.font.pointSize] forKey:NSFontAttributeName];
    
    CGRect sizeRect = [myText boundingRectWithSize:maxLabelSize options:NSLineBreakByWordWrapping | NSStringDrawingUsesLineFragmentOrigin  attributes:attributes context:nil];
    
    CGSize expectedLabelSize = sizeRect.size;
    
    //adjust the label the new height.
    CGRect newFrame = myLabel.frame;
    newFrame.size.width = expectedLabelSize.width;
    newFrame.size.height = expectedLabelSize.height + 20;
    myLabel.frame = newFrame;
    
    return myLabel;
}


*/

- (void)setWithUserFromVC:(VDKingCell *)controller withUser:(NSInteger)userID
{
    self.selectedUser = userID;
    [self performSegueWithIdentifier:@"Player Detail Segue" sender:self];
}

- (IBAction)viewAllCheckinsAction:(id)sender
{
    self.showFollowingInCheckinHistory = NO;
    [self performSegueWithIdentifier:@"CheckIn History Segue" sender:self];
}
- (IBAction)followingCheckInHistoryForHour:(id)sender
{
    self.showFollowingInCheckinHistory = YES;
    [self performSegueWithIdentifier:@"CheckIn History Segue" sender:self];
}

@end
