//
//  ScoresRecordedInfoVC.m
//  Vaiden
//
//  Created by James Chung on 8/26/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "ScoresRecordedInfoVC.h"

@interface ScoresRecordedInfoVC ()

@end

@implementation ScoresRecordedInfoVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)closeAction:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
