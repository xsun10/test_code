//
//  RecordIndMatchScoreSuccess.h
//  Phenom
//
//  Created by James Chung on 7/23/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IndividualMatch.h"
#import "CustomBaseVC.h"

@interface RecordIndMatchScoreSuccess : CustomBaseVC <UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, strong) IndividualMatch *individualMatch;

@end
