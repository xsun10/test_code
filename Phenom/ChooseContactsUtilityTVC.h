//
//  CheckInContactsTVC.h
//  Vaiden
//
//  Created by James Chung on 4/22/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "CustomBaseTVC.h"

@class ChooseContactsUtilityTVC;

@protocol ChooseContactsUtilityTVC_Delegate <NSObject>
- (void)setWithAllContactsController:(ChooseContactsUtilityTVC *)controller
                   withContactsArray:(NSMutableArray *)contactsArray;
@end

@interface ChooseContactsUtilityTVC : CustomBaseTVC


@property (nonatomic, weak) id <ChooseContactsUtilityTVC_Delegate> delegate;

@property (nonatomic) NSInteger maxContactsSelected;
@property (nonatomic, strong) NSString *errorOverlayLongMessage;
@property (nonatomic) BOOL pageIsForLockerPeopleTag;


@end
