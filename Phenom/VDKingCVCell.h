//
//  VDKingCVCell.h
//  Vaiden
//
//  Created by James Chung on 3/4/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VDKingCVCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *profilePic;
@property (weak, nonatomic) IBOutlet UILabel *additionalTextLabel;
@end
