//
//  RSVPMatchResultCVCell.h
//  Vaiden
//
//  Created by James Chung on 11/14/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RSVPMatchResultCVCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *stepTextLabel;
@property (weak, nonatomic) IBOutlet UIView *numBackground;
@property (weak, nonatomic) IBOutlet UILabel *numLabel;
@property (weak, nonatomic) IBOutlet UIImageView *headlineImage;
@property (weak, nonatomic) IBOutlet UILabel *headingLabel;
@end
