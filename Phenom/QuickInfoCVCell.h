//
//  QuickInfoCVCell.h
//  Vaiden
//
//  Created by James Chung on 11/12/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuickInfoCVCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UIImageView *iconPic;
@property (weak, nonatomic) IBOutlet UIView *iconPicBackground;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end
