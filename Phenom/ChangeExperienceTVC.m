//
//  ChangeExperienceTVC.m
//  Vaiden
//
//  Created by Turbo on 7/28/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "ChangeExperienceTVC.h"
#import "ExperienceTileCell.h"
#import "ExperiencePickerCell.h"
#import "ChooseOneSportTVC.h"
#import "IOSRequest.h"
#import "UserPreferences.h"

@interface ChangeExperienceTVC () <UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate, ChooseOneSportTVC_Delegate>
@property (nonatomic) UIPickerView *datePicker;
@property (nonatomic) NSInteger datePickerHeight;
@property (nonatomic) BOOL datePickerIsShowing;
@property (nonatomic, strong) UIView *closeDatePickerView;
@property (nonatomic, strong) UIButton *closeDatePickerButton;
@property (nonatomic) NSIndexPath *path;
@property (nonatomic) NSInteger selectedRow;
@property (nonatomic) NSInteger year;
@property (nonatomic) NSInteger month;

@property (nonatomic, strong) NSMutableArray *years;
@property (nonatomic) NSString *date;
@property (nonatomic) NSInteger year_sel1;
@property (nonatomic) NSInteger month_sel1;
@property (nonatomic) NSInteger year_sel2;
@property (nonatomic) NSInteger month_sel2;
@property (nonatomic) Sport *sport;

@property (nonatomic, strong) UITextField *text;
@property (nonatomic) BOOL first;
@property (nonatomic, strong) UserPreferences *preferences;
@end

@implementation ChangeExperienceTVC
#define EXPERIENCE @"exp"
#define AWARD @"awd"

- (UIPickerView *)datePicker
{
    if (!_datePicker) _datePicker = [[UIPickerView alloc] init];
    return _datePicker;
}

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (NSMutableArray *)years
{
    if (!_years) _years = [[NSMutableArray alloc] init];
    return _years;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    [self.navigationItem setBackBarButtonItem: backButton];
    
    if (self.isChange && [self.type isEqualToString:EXPERIENCE]) {
        self.navigationItem.title = @"Edit Experience";
    } else if (self.isChange && [self.type isEqualToString:AWARD]) {
        self.navigationItem.title = @"Edit Award";
    } else if (!self.isChange && [self.type isEqualToString:EXPERIENCE]) {
        self.navigationItem.title = @"Enter Experience";
    } else {
        self.navigationItem.title = @"Enter Award";
    }
    
    
    [self setExtraCellLineHidden:self.tableView];
    
    // set back button
    self.navigationController.navigationBar.topItem.title = @"";
    self.datePicker.delegate = self;
    self.datePicker.dataSource = self;
    // prepare for year
    [self getAllyears];
    self.year_sel1 = -1;
    self.month_sel1 = -1;
    self.year_sel2 = -1;
    self.month_sel2 = -1;
    self.sport = nil;
    if (self.isChange) {
        self.selectedRow = -1; // was 44
        NSArray *stringArray = [self.time componentsSeparatedByString:@" to "];
        for (int i=0; i<stringArray.count; i++) {
            NSString *str = [stringArray objectAtIndex:i];
            NSArray *subStrArray = [str componentsSeparatedByString:@", "];
            NSString *mon = [subStrArray objectAtIndex:0];
            NSString *year = [subStrArray objectAtIndex:1];
            if (i == 0) {
                self.month_sel1 = [mon isEqualToString:@"Spring"]?0:[mon isEqualToString:@"Summer"]?1:[mon isEqualToString:@"Autumn"]?2:3;
                self.year_sel1 = [year integerValue] - 1960;
            } else {
                self.month_sel2 = [mon isEqualToString:@"Spring"]?0:[mon isEqualToString:@"Summer"]?1:[mon isEqualToString:@"Autumn"]?2:3;
                self.year_sel2 = [year integerValue] - 1960;
            }
        }
        [self.tableView reloadData];
    }
    
    self.datePickerHeight = 44;
    self.datePickerIsShowing = NO;

    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] init];
    [tapGesture addTarget:self action:@selector(handleTap:)];
    [tapGesture setCancelsTouchesInView:NO];
    [self.tableView addGestureRecognizer:tapGesture];
}

- (void) handleTap:(UITapGestureRecognizer *)gesture
{
    [self.text resignFirstResponder];
}

- (void)getAllyears
{
    //Get Current Year into year
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy"];
    self.year = [[formatter stringFromDate:[NSDate date]] intValue];
    [formatter setDateFormat:@"MM"];
    self.month = [[formatter stringFromDate:[NSDate date]] intValue];
    
    //Create Years Array from 1960 to This year
    for (int i=1960; i<=self.year; i++) {
        [self.years addObject:[NSString stringWithFormat:@"%d",i]];
    }
}

#pragma mark - UIPickerView data source
- (NSInteger)numberOfComponentsInPickerView: (UIPickerView*)thePickerView {
    return 2;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component
{
    if (component == 0) {
        return 4;
    } else {
        return [self.years count];
    }
    
}
- (NSString *)pickerView:(UIPickerView *)thePickerView
             titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (component == 0) {
        return row==0?@"Spring":row==1?@"Summer":row==2?@"Autumn":@"Winter";
    } else {
        return [self.years objectAtIndex:row];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([self.type isEqualToString:EXPERIENCE]) {
        return 4;
    } else {
        return 3;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.type isEqualToString:EXPERIENCE]) {
        if (indexPath.row == 0) {
            ExperienceTileCell *cell = [tableView dequeueReusableCellWithIdentifier:@"name_cell"];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            cell.titleName.text = @"Team Name";
            
            if (self.name.length == 0 || [self.name isKindOfClass:[NSNull class]]) {
                [cell.titleInput setPlaceholder:@"Enter Team Name"];
            } else {
                [cell.titleInput setText:self.name];
            }
            
            [cell.titleInput setDelegate:self];
            self.text = cell.titleInput;
            
            return cell;
        } else {
            ExperiencePickerCell *cell = [tableView dequeueReusableCellWithIdentifier:@"picker_cell"];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            cell.title.text = indexPath.row==1?@"Start":indexPath.row==2?@"End":@"Sport";
            if (indexPath.row == 3) {
                if (self.isChange && self.sportname.length >0) {
                    cell.content.text = self.sportname;
                } else if (self.sport == nil) {
                    [cell.content setHidden:YES];
                } else {
                    [cell.content setHidden:NO];
                    cell.content.text = self.sport.sportName;
                }
            } else {
                if ((indexPath.row == 1 && (self.month_sel1 == -1 || self.year_sel1 == -1))
                    || (indexPath.row == 2 && (self.month_sel2 == -1 || self.year_sel2 == -1)) ) {
                    [cell.content setHidden:YES];
                } else {
                    [cell.content setHidden:NO];
                    if (indexPath.row == 1) {
                        cell.content.text = [NSString stringWithFormat:@"%@, %@", self.month_sel1==0?@"Spring":self.month_sel1==1?@"Summer":self.month_sel1==2?@"Autumn":@"Winter", [self.years objectAtIndex:self.year_sel1]];
                    } else {
                        cell.content.text = [NSString stringWithFormat:@"%@, %@", self.month_sel2==0?@"Spring":self.month_sel2==1?@"Summer":self.month_sel2==2?@"Autumn":@"Winter", [self.years objectAtIndex:self.year_sel2]];
                    }
                }
            }
            
            return cell;
        }
    } else {
        if (indexPath.row == 0) {
            ExperienceTileCell *cell = [tableView dequeueReusableCellWithIdentifier:@"name_cell"];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            cell.titleName.text = @"Award Name";
            
            if (self.name.length == 0 || [self.name isKindOfClass:[NSNull class]]) {
                [cell.titleInput setPlaceholder:@"Enter Award Name"];
            } else {
                [cell.titleInput setText:self.name];
            }
            [cell.titleInput setDelegate:self];
            self.text = cell.titleInput;

            return cell;
        } else {
            ExperiencePickerCell *cell = [tableView dequeueReusableCellWithIdentifier:@"picker_cell"];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            cell.title.text = indexPath.row==1?@"Date Received":@"Sport";
            if (indexPath.row == 2) {
                if (self.isChange && self.sportname.length >0) {
                    cell.content.text = self.sportname;
                } else if (self.sport == nil) {
                    [cell.content setHidden:YES];
                } else {
                    [cell.content setHidden:NO];
                    cell.content.text = self.sport.sportName;
                }
            } else {
                if ((indexPath.row == 1 && (self.month_sel1 == -1 || self.year_sel1 == -1))) {
                    [cell.content setHidden:YES];
                } else {
                    [cell.content setHidden:NO];
                    if (indexPath.row == 1) {
                        cell.content.text = [NSString stringWithFormat:@"%@, %@", self.month_sel1==0?@"Spring":self.month_sel1==1?@"Summer":self.month_sel1==2?@"Autumn":@"Winter", [self.years objectAtIndex:self.year_sel1]];
                    }
                }
            }
            
            return cell;
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.datePickerIsShowing) {
        return;
    }
    if ([self.type isEqualToString:EXPERIENCE]) {
        self.path = indexPath;
        if ((indexPath.row == 1 || indexPath.row == 2)) {
            self.selectedRow = indexPath.row;
            // Can check in up to 24 hours in advance
            ExperiencePickerCell* cell = (ExperiencePickerCell *)[self.tableView cellForRowAtIndexPath:indexPath];
            
            [tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section] atScrollPosition:UITableViewScrollPositionTop animated:YES];
            
            self.datePicker.alpha = 0;
            [self.tableView beginUpdates];
            self.datePickerHeight = 44 + 190;
            [self.tableView endUpdates];
            [cell.title setHidden:YES];
            [cell.icon setHidden:YES];
            [cell.content setHidden:YES];
            [cell addSubview:self.datePicker];
            [cell bringSubviewToFront:self.datePicker];
            
            [UIView animateWithDuration:0.2 animations:^{
                if (indexPath.row == 1) {
                    self.first = YES;
                    if (self.month_sel1 == -1) {
                        // default pick the current time
                        if (self.month >=3 && self.month <=6) {
                            [self.datePicker selectRow:0 inComponent:0 animated:YES];
                            self.month_sel1 = 0;
                        } else if (self.month >6 && self.month <=9) {
                            [self.datePicker selectRow:1 inComponent:0 animated:YES];
                            self.month_sel1 = 1;
                        } else if (self.month >9 && self.month <=11) {
                            [self.datePicker selectRow:2 inComponent:0 animated:YES];
                            self.month_sel1 = 2;
                        } else if (self.month >11 && self.month <3) {
                            [self.datePicker selectRow:3 inComponent:0 animated:YES];
                            self.month_sel1 = 3;
                        }
                    } else {
                        [self.datePicker selectRow:self.month_sel1 inComponent:0 animated:YES];
                    }
                    
                    if (self.year_sel1 == -1) {
                        [self.datePicker selectRow:self.year - 1960 inComponent:1 animated:YES];
                        self.year_sel1 = self.year - 1960;
                    } else {
                        [self.datePicker selectRow:self.year_sel1 inComponent:1 animated:YES];
                    }
                } else {
                    self.first = NO;
                    if (self.month_sel2 == -1) {
                        // default pick the current time
                        if (self.month >=3 && self.month <=6) {
                            [self.datePicker selectRow:0 inComponent:0 animated:YES];
                            self.month_sel2 = 0;
                        } else if (self.month >6 && self.month <=9) {
                            [self.datePicker selectRow:1 inComponent:0 animated:YES];
                            self.month_sel2 = 1;
                        } else if (self.month >9 && self.month <=11) {
                            [self.datePicker selectRow:2 inComponent:0 animated:YES];
                            self.month_sel2 = 2;
                        } else if (self.month >11 && self.month <3) {
                            [self.datePicker selectRow:3 inComponent:0 animated:YES];
                            self.month_sel2 = 3;
                        }
                    } else {
                        [self.datePicker selectRow:self.month_sel2 inComponent:0 animated:YES];
                    }
                    
                    if (self.year_sel2 == -1) {
                        [self.datePicker selectRow:self.year - 1960 inComponent:1 animated:YES];
                        self.year_sel2 = self.year - 1960;
                    } else {
                        [self.datePicker selectRow:self.year_sel2 inComponent:1 animated:YES];
                    }
                }
                
                self.datePicker.alpha = 1;
                self.datePickerIsShowing = YES;
                self.closeDatePickerView = [[UIView alloc] initWithFrame:CGRectMake(0, cell.frame.origin.y, 320, 40)];
                self.closeDatePickerView.backgroundColor = [UIColor darkGrayColor];
                self.closeDatePickerView.alpha = 0.5;
                
                self.closeDatePickerButton = [[UIButton alloc] initWithFrame:CGRectMake(260,  7, 50, 25)];
                [self.closeDatePickerButton setTitle:@"CLOSE" forState:UIControlStateNormal];
                [self.closeDatePickerButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-MediumItalic" size:11]];
                self.closeDatePickerButton.layer.borderColor = [UIColor lightGrayColor].CGColor;
                self.closeDatePickerButton.layer.borderWidth = 1.0;
                
                [self.closeDatePickerButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
                [self.closeDatePickerButton addTarget:self action:@selector(tapDate:) forControlEvents:UIControlEventTouchUpInside];
                
                [self.closeDatePickerView addSubview:self.closeDatePickerButton];
                [self.tableView addSubview:self.closeDatePickerView];
                
            } completion:nil];
        } else {
            [self performSegueWithIdentifier:@"choose_sport_segue" sender:self];
        }
    } else {
        self.path = indexPath;
        if (indexPath.row == 1) {
            self.selectedRow = indexPath.row;
            // Can check in up to 24 hours in advance
            ExperiencePickerCell* cell = (ExperiencePickerCell *)[self.tableView cellForRowAtIndexPath:indexPath];
            
            [tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section] atScrollPosition:UITableViewScrollPositionTop animated:YES];
            
            self.datePicker.alpha = 0;
            [self.tableView beginUpdates];
            self.datePickerHeight = 44 + 190;
            [self.tableView endUpdates];
            [cell.title setHidden:YES];
            [cell.icon setHidden:YES];
            [cell.content setHidden:YES];
            [cell addSubview:self.datePicker];
            [cell bringSubviewToFront:self.datePicker];
            
            [UIView animateWithDuration:0.2 animations:^{
                if (indexPath.row == 1) {
                    self.first = YES;
                    if (self.month_sel1 == -1) {
                        // default pick the current time
                        if (self.month >=3 && self.month <=6) {
                            [self.datePicker selectRow:0 inComponent:0 animated:YES];
                            self.month_sel1 = 0;
                        } else if (self.month >6 && self.month <=9) {
                            [self.datePicker selectRow:1 inComponent:0 animated:YES];
                            self.month_sel1 = 1;
                        } else if (self.month >9 && self.month <=11) {
                            [self.datePicker selectRow:2 inComponent:0 animated:YES];
                            self.month_sel1 = 2;
                        } else if (self.month >11 && self.month <3) {
                            [self.datePicker selectRow:3 inComponent:0 animated:YES];
                            self.month_sel1 = 3;
                        }
                    } else {
                        [self.datePicker selectRow:self.month_sel1 inComponent:0 animated:YES];
                    }
                    
                    if (self.year_sel1 == -1) {
                        [self.datePicker selectRow:self.year - 1960 inComponent:1 animated:YES];
                        self.year_sel1 = self.year - 1960;
                    } else {
                        [self.datePicker selectRow:self.year_sel1 inComponent:1 animated:YES];
                    }
                }
                self.datePicker.alpha = 1;
                self.datePickerIsShowing = YES;
                self.closeDatePickerView = [[UIView alloc] initWithFrame:CGRectMake(0, cell.frame.origin.y, 320, 40)];
                self.closeDatePickerView.backgroundColor = [UIColor darkGrayColor];
                self.closeDatePickerView.alpha = 0.5;
                
                self.closeDatePickerButton = [[UIButton alloc] initWithFrame:CGRectMake(260,  7, 50, 25)];
                [self.closeDatePickerButton setTitle:@"CLOSE" forState:UIControlStateNormal];
                [self.closeDatePickerButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-MediumItalic" size:11]];
                self.closeDatePickerButton.layer.borderColor = [UIColor lightGrayColor].CGColor;
                self.closeDatePickerButton.layer.borderWidth = 1.0;
                
                [self.closeDatePickerButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
                [self.closeDatePickerButton addTarget:self action:@selector(tapDate:) forControlEvents:UIControlEventTouchUpInside];
                
                [self.closeDatePickerView addSubview:self.closeDatePickerButton];
                [self.tableView addSubview:self.closeDatePickerView];
                
            } completion:nil];
        } else {
            [self performSegueWithIdentifier:@"choose_sport_segue" sender:self];
        }

    }
}

- (void)tapDate:(UIButton *)sender
{
    [UIView animateWithDuration:0.5 animations:^(void) {
        self.closeDatePickerView.alpha = 0.0;
        if (self.datePickerIsShowing) {
            [UIView animateWithDuration:0.2
                             animations:^{
                                 self.datePicker.alpha = 0;
                             }
                             completion:^(BOOL finished){
                                 [self.datePicker removeFromSuperview];
                                 [self.closeDatePickerView removeFromSuperview];
                             }];
            
            ExperiencePickerCell* cell = (ExperiencePickerCell *)[self.tableView cellForRowAtIndexPath:self.path];
            [cell.title setHidden:NO];
            [cell.icon setHidden:NO];
            [cell.content setHidden:NO];
            
            [self.tableView beginUpdates];
            self.selectedRow = -1; // was 44
            [self.tableView endUpdates];
            
            self.datePickerIsShowing = NO;
            
            // prevent hiding the separator on the top
            [self.tableView reloadRowsAtIndexPaths:@[self.path] withRowAnimation:UITableViewRowAnimationAutomatic];
            
        }
    }];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == self.selectedRow) {
        return self.datePickerHeight;
    }
    
    return 44;
}

- (void)setExtraCellLineHidden: (UITableView *)tableView{
    UIView *view =[[UIView alloc]init];
    view.backgroundColor = [UIColor clearColor];
    [tableView setTableFooterView:view];
    [tableView setTableHeaderView:view];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (component == 1) {
        if (self.first) {
            self.year_sel1 = row;
        } else {
            self.year_sel2 = row;
        }
    } else {
        if (self.first) {
            self.month_sel1 = row;
        } else {
            self.month_sel2 = row;
        }
        
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - choose sport segue
- (void)setViewController:(ChooseOneSportTVC *)controller withSport:(Sport *)sp
{
    self.sport = sp;
    self.sportname = @"";
    [self.tableView reloadRowsAtIndexPaths:@[self.path] withRowAnimation:UITableViewRowAnimationAutomatic];
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"choose_sport_segue"]) {
        ChooseOneSportTVC *controller = segue.destinationViewController;
        controller.delegate = self;
    }
}

- (bool)checkInput {
    if ([self.type isEqualToString:EXPERIENCE] && (self.text.text.length > 0 && self.month_sel1 != -1 && self.month_sel2 != -1 && (![self.sport isKindOfClass:[NSNull class]] || self.sportname.length > 0))) {
        return YES;
    } else if ([self.type isEqualToString:AWARD] && (self.text.text.length > 0 && self.month_sel1 != -1 && (![self.sport isKindOfClass:[NSNull class]] || self.sportname.length > 0))) {
        return YES;
    } else {
        return NO;
    }
}

- (IBAction)doneClicked:(id)sender {
    if (![self checkInput]) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Invalid Input";
        alert.message = @"please check your input informatipn and try again.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
        return;
    }
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.labelText = @"Submitting...";
    if ([self.type isEqualToString:EXPERIENCE]) {
        NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
        [data setObject:[NSString stringWithFormat:@"%ld",(long)[self.preferences getUserID]] forKey:@"user_id"];
        [data setObject:[NSString stringWithFormat:@"%ld",(long)self.changeID] forKey:@"id"];
        [data setObject:self.text.text forKey:@"team_name"];
        [data setObject:[NSString stringWithFormat:@"%@, %@", self.month_sel1==0?@"Spring":self.month_sel1==1?@"Summer":self.month_sel1==2?@"Autumn":@"Winter", [self.years objectAtIndex:self.year_sel1]] forKey:@"start"];
        [data setObject:[NSString stringWithFormat:@"%@, %@", self.month_sel2==0?@"Spring":self.month_sel2==1?@"Summer":self.month_sel2==2?@"Autumn":@"Winter", [self.years objectAtIndex:self.year_sel2]] forKey:@"end"];
        [data setObject:[NSString stringWithFormat:@"%ld",(long)self.sport.sportID ] forKey:@"sport_id"];
        if (self.sportname != nil) {
            [data setObject:self.sportname forKey:@"sport_name"];
        }
        [IOSRequest saveExperenceForUser:[self.preferences getUserID]
                                withData:data
                            onCompletion:^(NSMutableArray *result) {
                                [MBProgressHUD hideHUDForView:self.view animated:YES];
                                [self.delegate reloadTable:EXPERIENCE];
                                [self.navigationController popViewControllerAnimated:YES];
                            }];
    } else {
        NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
        [data setObject:[NSString stringWithFormat:@"%d",[self.preferences getUserID]] forKey:@"user_id"];
        [data setObject:self.text.text forKey:@"award_name"];
        [data setObject:[NSString stringWithFormat:@"%d",self.changeID] forKey:@"id"];
        [data setObject:[NSString stringWithFormat:@"%@, %@", self.month_sel1==0?@"Spring":self.month_sel1==1?@"Summer":self.month_sel1==2?@"Autumn":@"Winter", [self.years objectAtIndex:self.year_sel1]] forKey:@"date"];
        [data setObject:[NSString stringWithFormat:@"%d",self.sport.sportID ] forKey:@"sport_id"];
        if (self.sportname != nil) {
            [data setObject:self.sportname forKey:@"sport_name"];
        }
        [IOSRequest saveAwardForUser:[self.preferences getUserID]
                                withData:data
                            onCompletion:^(NSMutableArray *result) {
                                [MBProgressHUD hideHUDForView:self.view animated:YES];
                                [self.delegate reloadTable:AWARD];
                                [self.navigationController popViewControllerAnimated:YES];
                            }];
    }
    double delayInSeconds = 10.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        //code to be executed on the main queue after delay
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    });
}

@end
