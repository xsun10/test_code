//
//  WhatAreVCoinsVC.m
//  Vaiden
//
//  Created by James Chung on 8/26/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "WhatAreVCoinsVC.h"
#import "UIImage+ImageEffects.h"
#import "UIImage+ProportionalFill.h"

@interface WhatAreVCoinsVC ()
@property (weak, nonatomic) IBOutlet UIImageView *blurredImage;
@property (nonatomic, strong) UIImage *backImage;
@property (weak, nonatomic) IBOutlet UIImageView *topBanner;
@property (weak, nonatomic) IBOutlet UIImageView *circleImageBackground;
@property (weak, nonatomic) IBOutlet UIImageView *coinIcon;
@property (weak, nonatomic) IBOutlet UIView *whiteStrip;
@property (weak, nonatomic) IBOutlet UIImageView *vaidenLogo;


@end

@implementation WhatAreVCoinsVC


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.coinIcon.image = [[UIImage imageNamed:@"Coins"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.coinIcon setTintColor:[UIColor whiteColor]];
    
    self.vaidenLogo.image = [[UIImage imageNamed:@"Vaderen Tab"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.vaidenLogo setTintColor:[UIColor whiteColor]];
    
	// Do any additional setup after loading the view.
    
    [self.whiteStrip setAlpha:0.2];
    self.topBanner.contentMode = UIViewContentModeScaleAspectFill;
    self.topBanner.clipsToBounds = YES;
    self.topBanner.image = [UIImage imageNamed:@"Coin Background"];
    
    self.backImage = [UIImage imageNamed:@"Coin Background"];
    [self setBackgroundImage:1];
}


- (void)setBackgroundImage:(NSUInteger)imageIndex
{
    NSString *effectText = @"";
    UIImage *effectImage = nil;
    
    effectImage = self.backImage;
    
    switch (imageIndex)
    {
        case 0:
            effectImage = self.backImage;
            break;
        case 1:
            effectImage = [self.backImage applyLightEffect];
            effectText = NSLocalizedString(@"Light", @"");
 //           self.effectLabel.textColor = [UIColor whiteColor];
            break;
        case 2:
            effectImage = [self.backImage applyExtraLightEffect];
            effectText = NSLocalizedString(@"Extra light", @"");
//            self.effectLabel.textColor = [UIColor lightGrayColor];
            break;
        case 3:
            effectImage = [self.backImage applyDarkEffect];
            effectText = NSLocalizedString(@"Dark", @"");
 //           self.effectLabel.textColor = [UIColor darkGrayColor];
            break;
        case 4:
            effectImage = [self.backImage applyTintEffectWithColor:[UIColor blueColor]];
            effectText = NSLocalizedString(@"Color tint", @"");
 //           self.effectLabel.textColor = [UIColor darkGrayColor];
            break;
    }
    
    self.blurredImage.image = effectImage;
    self.circleImageBackground.image = effectImage;
    [UIImage makeRoundedImageNoBorder:self.circleImageBackground withRadius:60];

 //   self.effectLabel.text = effectText;
    
 //   self.imageIndex++;
}

- (IBAction)closeAction:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
