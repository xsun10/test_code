//
//  sendThanksDialog.h
//  Vaiden
//
//  Created by Turbo on 7/17/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface sendThanksDialog : UIViewController <MBProgressHUDDelegate> {
    MBProgressHUD *HUD;
}

@property (nonatomic) NSInteger targetID;
@property (nonatomic) NSInteger alertID;
@end
