//
//  AddVenueWithoutAddressTVC.h
//  Vaiden
//
//  Created by James Chung on 12/5/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "AddVenueSportsTVC.h"
#import "MBProgressHUD.h"
#import "CustomBaseTVC.h"


@class AddVenueWithoutAddressTVC;

@protocol AddVenueWithoutAddressTVC_Delegate <NSObject>

- (void)setViewController:(AddVenueWithoutAddressTVC *)controller andVenueID:(NSInteger)venueID andVenueName:(NSString *)venueName;

@end

@interface AddVenueWithoutAddressTVC : CustomBaseTVC <CLLocationManagerDelegate, AddVenueSportsTVC_Delegate, MBProgressHUDDelegate>
{
    CLLocationManager *locationManager;
    MBProgressHUD *HUD;
}

@property (nonatomic, weak) id <AddVenueWithoutAddressTVC_Delegate> delegate;

@property (nonatomic) BOOL isCreatingMatch;

@end


