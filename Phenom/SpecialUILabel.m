//
//  SpecialUILabel.m
//  Vaiden
//
//  Created by James Chung on 1/30/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "SpecialUILabel.h"

@implementation SpecialUILabel

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (SpecialUILabel *)init
{
    if (self = [super init]) {
        _shouldRemoveFromMySuperview = NO;
    }
    return self;
}
@end
