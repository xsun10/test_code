//
//  VenueCheckinStats.h
//  Vaiden
//
//  Created by James Chung on 5/3/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VenueCheckinStats : NSObject

@property (nonatomic) NSInteger checkInStatsAllTime;
@property (nonatomic) NSInteger checkInStatsHour;
@property (nonatomic) NSInteger checkInStatsHourForFollowing;

-(VenueCheckinStats *)initWithVenueCheckinStatsAllTime:(NSInteger)allTimeStat
                                 andCheckinStatForHour:(NSInteger)hourCheckinStat
                     andCheckinStatForHourForFollowing:(NSInteger)hourCheckinFollowingStat;


@end
