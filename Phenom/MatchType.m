//
//  MatchType.m
//  Vaiden
//
//  Created by James Chung on 8/6/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "MatchType.h"

@implementation MatchType

- (MatchType *) initWithMatchType:(NSInteger)matchTypeID
                  withDescription:(NSString *)description
{
    self = [super init];
    
    if (self) {
        _description = description;
        _matchTypeID = matchTypeID;
    }
    
    return self;
}


@end
