//
//  AddressBookFriendsPickerTVC.h
//  Vaiden
//
//  Created by Turbo on 6/27/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

#define TWITTER_TAB 1
#define ADDRESS_BOOK_TAB 2

@protocol addressBookFriendsPickerDelegate <NSObject>

- (void) addressBookFriendsPickerDoneClicked:(UIBarButtonItem *)sender withData:(NSMutableDictionary *)data withType:(NSInteger)type;

- (void) addressBookFriendsPickerCancelWasClicked:(id)sender;

@end

@interface AddressBookFriendsPickerTVC : UITableViewController

// define delegate property
@property (nonatomic, weak) id<addressBookFriendsPickerDelegate> delegate;

@property (nonatomic, strong) NSMutableArray * contacts;
@property (nonatomic, strong) NSDictionary * profilePics;
@property (nonatomic, strong) NSDictionary * emails;
@property (nonatomic, strong) NSDictionary * friendList;

@property (strong, nonatomic) NSMutableDictionary * picker;
@property (nonatomic) NSInteger currentTab;

@end
