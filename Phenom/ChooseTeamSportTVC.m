//
//  ChooseTeamSportTVC.m
//  Vaiden
//
//  Created by James Chung on 4/10/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "ChooseTeamSportTVC.h"
#import "IOSRequest.h"
#import "ChooseTeamSportCell.h"
#import "UIColor+VaidenColors.h"

@interface ChooseTeamSportTVC ()

@property (nonatomic, strong) NSMutableArray *sportsArray;
@property (nonatomic, strong) UIView *tempView;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;

@end

@implementation ChooseTeamSportTVC


- (NSMutableArray *)sportsArray
{
    if (!_sportsArray) _sportsArray = [[NSMutableArray alloc] init];
    return _sportsArray;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self loadSports];
}

- (void)loadSports
{
    [self showTempSpinner];
    
    [IOSRequest getAllTeamSportsonCompletion:^(NSMutableArray *results) {
        
        for (id object in results) {
            Sport *s = [[Sport alloc] initWithSportDetails:[object[@"sport_id"] integerValue]
                                                   andName:object[@"sport_name"]
                                                   andType:@"team"];
            [self.sportsArray addObject:s];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.activityIndicator stopAnimating];
            self.tempView.hidden = YES;
            [self.tableView reloadData];
            
        });
        
    }];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60.0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.sportsArray count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ChooseTeamSportCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Sport Cell" forIndexPath:indexPath];
    
    Sport *sport = [self.sportsArray objectAtIndex:indexPath.row];
    
    cell.sportIcon.image = [[sport getSportIcon] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [cell.sportIcon setTintColor:[UIColor newBlueDark]];
    cell.sportName.text = [sport.sportName capitalizedString];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Sport *sport = [self.sportsArray objectAtIndex:indexPath.row];
    
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
    
    [self.delegate setWithSportFromVC:self withSport:sport];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)showTempSpinner
{
    self.tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    self.tempView.backgroundColor = [UIColor lightGrayColor];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] init];
    [self.activityIndicator setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width/2.0, [UIScreen mainScreen].bounds.size.height/2.0 - 50)];
    [self.activityIndicator startAnimating];
    [self.tempView addSubview:self.activityIndicator];
    
    [self.view addSubview:self.tempView];
    
}


@end
