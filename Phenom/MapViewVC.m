//
//  MapViewVC.m
//  Phenom
//
//  Created by James Chung on 5/16/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "MapViewVC.h"

@interface MapViewVC ()

@end

@implementation MapViewVC

- (MKMapView *)mapView
{
    if (!_mapView) _mapView = [[MKMapView alloc] init];
    return _mapView;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.mapView.delegate = self;
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    static NSString *resuseID = @"MapViewTVC";
    
    MKAnnotationView *view = [mapView dequeueReusableAnnotationViewWithIdentifier:resuseID];
    if (!view) {
        view = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:resuseID];
        view.canShowCallout = YES;
        
        if ([mapView.delegate respondsToSelector:@selector(mapView:annotationView:calloutAccessoryControlTapped:)]) {
            view.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        }
        view.leftCalloutAccessoryView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,30,30)];
    }
    
    if ([view.leftCalloutAccessoryView isKindOfClass:[UIImageView class]]) {
        UIImageView *imageView = (UIImageView *)view.leftCalloutAccessoryView;
        imageView.image = nil;
    }
    
    return view;
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    if ([view.leftCalloutAccessoryView isKindOfClass:[UIImageView class]]) {
        UIImageView *imageView = (UIImageView *)(view.leftCalloutAccessoryView);
        
        if ([view.annotation respondsToSelector:@selector(thumbnail)]) {
            imageView.image = [view.annotation performSelector:@selector(thumbnail)];
        }
    }
}

@end
