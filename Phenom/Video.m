//
//  Video.m
//  Phenom
//
//  Created by James Chung on 6/4/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "Video.h"

@implementation Video



- (Video *)initWithVideoDetails:(NSString *)videoBaseFileString
             withThumbnailImage:(UIImage *)thumbnailImage
                   andVideoType:(NSString *)videoType
{
    self = [super init];
    
    if (self) {
        _videoBaseFileString = videoBaseFileString;
        _thumbnailImage = thumbnailImage;
        _videoType = videoType;
    }
    
    return self;
}


@end
