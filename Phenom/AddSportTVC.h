//
//  QuickInfo2TVC.h
//  Vaiden
//
//  Created by James Chung on 9/30/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomBaseTVC.h"
#import "MBProgressHUD.h"

@interface AddSportTVC : CustomBaseTVC <MBProgressHUDDelegate>
{
    MBProgressHUD *HUD;
}

@end
