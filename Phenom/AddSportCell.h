//
//  AddSportCell.h
//  Vaiden
//
//  Created by James Chung on 11/12/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddSportCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *sportIcon;
@property (weak, nonatomic) IBOutlet UILabel *sportName;

@end
