//
//  IndividualMatchHistoryCell.h
//  Vaiden
//
//  Created by James Chung on 10/21/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IndividualMatchHistoryCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *picView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *subheadlineLabel;

@end
