//
//  CompetitionNews.m
//  Vaiden
//
//  Created by James Chung on 3/6/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "CompetitionNews.h"

@implementation CompetitionNews

- (CompetitionNews *)initWithNewsID:(NSInteger)newsMakerID
                        andUsername:(NSString *)newsMakerUsername
                      andProfileStr:(NSString *)newsMakerProfileString
                           withDate:(NSDate *)newsDate
                        andPostType:(NSInteger)postType
                     andNumComments:(NSInteger)numComments
                      andNumRemixes:(NSInteger)numRemixes
                          andNewsID:(NSInteger)newsID
                  andUniversityName:(NSString *)universityName
          andUniversityOrganization:(NSString *)universityOrganization

{
    self = [super initWithNewsDetails:newsMakerID
                          andUserName:newsMakerUsername
                        andProfileStr:newsMakerProfileString
                             withDate:newsDate
                          andPostType:postType
                       andNumComments:numComments
                        andNumRemixes:numRemixes
                            andNewsID:newsID];
    
    if (self) {
        _universityName = universityName;
        _universityOrganization = universityOrganization;
       
    }
    return self;
}

@end
