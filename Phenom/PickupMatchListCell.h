//
//  UpcomingPickupMatchesCell.h
//  Phenom
//
//  Created by James Chung on 5/11/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PickupMatchListCell;

@protocol PickupMatchListCell_delegate <NSObject>
-(void)setPickupMatchListCellViewController:(PickupMatchListCell *)controller withUserIDToSegue:(NSInteger)userID;
@end





@interface PickupMatchListCell : UITableViewCell <UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, weak) id <PickupMatchListCell_delegate> delegate;


/*
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *profilePic;
@property (weak, nonatomic) IBOutlet UILabel *headlineLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *venueLabel;
@property (weak, nonatomic) IBOutlet UILabel *streetLabel;
@property (weak, nonatomic) IBOutlet UILabel *cityStateLabel;
*/
@property (weak, nonatomic) IBOutlet UIView *mainBackgroundViewArea;
@property (weak, nonatomic) IBOutlet UIImageView *sportIcon;
@property (weak, nonatomic) IBOutlet UIView *sportIconBackground;
@property (weak, nonatomic) IBOutlet UILabel *matchHeading;
@property (weak, nonatomic) IBOutlet UILabel *sessionUserStatus;
@property (weak, nonatomic) IBOutlet UILabel *matchStatus;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *playerCV;
@property (weak, nonatomic) IBOutlet UILabel *experienceLabel;
@property (weak, nonatomic) IBOutlet UILabel *genderLabel;
@property (weak, nonatomic) IBOutlet UILabel *sportLabel;
@property (weak, nonatomic) IBOutlet UIImageView *triangleIcon;
@property (weak, nonatomic) IBOutlet UIView *iconBackgroundView;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UIView *headlineBackground;
@property (weak, nonatomic) IBOutlet UILabel *matchTypeLabel;
//@property (weak, nonatomic) IBOutlet UIImageView *sportIcon;
@property (weak, nonatomic) IBOutlet UIImageView *starIcon;
@property (weak, nonatomic) IBOutlet UIImageView *sportPictureBackground;
@property (weak, nonatomic) IBOutlet UILabel *matchNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *newsfeedIcon;

@property (nonatomic, strong) NSMutableArray *competitors;

@property (weak, nonatomic) IBOutlet UILabel *additionalPlayersLabel;
@property (weak, nonatomic) IBOutlet UIButton *detailsButton;

@end
