//
//  VoteShopVC.m
//  Vaiden
//
//  Created by Turbo on 8/4/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "VoteShopVC.h"
#import "BuyVoteCVCell.h"
#import "UIView+Manipulate.h"
#import "UIColor+VaidenColors.h"
#import "VotePuchaseTier1Helper.h"
#import <StoreKit/StoreKit.h>
#import "IOSRequest.h"
#import "UserPreferences.h"

@interface VoteShopVC () <UICollectionViewDataSource, UICollectionViewDelegate>
@property (weak, nonatomic) IBOutlet UIView *buttonLine;
@property (weak, nonatomic) IBOutlet UIButton *home;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageController;
@property (weak, nonatomic) IBOutlet UIScrollView *pageScrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonLinePadding;
@property (weak, nonatomic) IBOutlet UILabel *leftTitle;
@property (weak, nonatomic) IBOutlet UILabel *leftContext;
@property (weak, nonatomic) IBOutlet UILabel *rightTitle;
@property (weak, nonatomic) IBOutlet UILabel *rightContext;
@property (weak, nonatomic) IBOutlet UIImageView *home_page;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *screenHeight;

@property (weak, nonatomic) NSString *selected;
@property (strong, nonatomic) UserPreferences *preferences;
@property (strong, nonatomic) NSMutableDictionary *products;
@end

@implementation VoteShopVC
#define BOOST @"boost"
#define BUY @"buy"

- (NSMutableDictionary *)products
{
    if (!_products) _products = [[NSMutableDictionary alloc] init];
    return _products;
}

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //[[NSUserDefaults standardUserDefaults] removeObjectForKey:@"unupload_purchased_votes"];
    //[[NSUserDefaults standardUserDefaults] removeObjectForKey:@"unupload_purchased_votes_price"];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    [self.collectionView setPagingEnabled:YES];
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [flowLayout setMinimumInteritemSpacing:0.0f];
    [flowLayout setMinimumLineSpacing:0.0f];
    [flowLayout setItemSize:CGSizeMake(320, 356)];
    [self.collectionView setCollectionViewLayout:flowLayout];
    
    if (self.fromProfile) { // BOOST
        self.selected = BOOST;
        [self.buttonLinePadding setConstant:184.0f];
        [self setInstruction:BOOST];
        if ([[UIScreen mainScreen]bounds].size.height > 480) {
            [self.screenHeight setConstant:560];
        }
    } else {
        self.selected = BUY;
    }
    
    [self.home_page setHidden:YES];
    
    [self loadPage];
}

- (void)viewWillAppear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productPurchased:) name:IAPHelperProductPurchasedNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    if ([[UIScreen mainScreen]bounds].size.height <= 480) {
        if (!self.fromProfile) {
            self.pageScrollView.contentSize = CGSizeMake(320.0, 550.0);
        } else {
            self.pageScrollView.contentSize = CGSizeMake(320.0, 485.0);
        }
    }
    
    
}

- (void)productPurchased:(NSNotification *)notification
{
    NSString * productIdentifier = notification.object;
    NSDictionary *userInfo = notification.userInfo;
    NSArray * products = (NSArray *)self.products[userInfo[@"type"]];
    [products enumerateObjectsUsingBlock:^(SKProduct * product, NSUInteger idx, BOOL *stop) {
        if ([product.productIdentifier isEqualToString:productIdentifier]) {
            [self uploadDatabase:NO withType:userInfo[@"type"]];
            *stop = YES;
        }
    }];
}

- (void)loadPage
{
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.labelText = @"Loading...";
    
    // Retrieve the boost status for user
    [IOSRequest getBoostStatusForUser:[self.preferences getUserID]
                         onCompletion:^(NSDictionary *results) {
                             if ([results[@"outcome"] isEqualToString:@"success"]) {
                                 [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"boosted"];
                             } else {
                                 [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"boosted"];
                             }
                         }];
    
    [[VotePuchaseTier1Helper sharedInstance] requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
        if (success && products.count > 0) {
            NSMutableArray *boost = [[NSMutableArray alloc] init];
            NSMutableArray *buy = [[NSMutableArray alloc] init];
            for (SKProduct *product in products) {
                if([product.localizedTitle isEqualToString:BOOST]) {
                    [boost addObject:product];
                } else if([product.localizedTitle isEqualToString:BUY]){
                    [buy addObject:product];
                }
            }
            [self.products setObject:boost forKey:BOOST];
            [self.products setObject:buy forKey:BUY];
            [self.collectionView reloadData];
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        } else { // error view
            UIAlertView *alert = [[UIAlertView alloc] init];
            alert.title = @"Purchase Error";
            alert.message = @"Please make sure you start your purchase service.";
            [alert addButtonWithTitle:@"OK"];
            [alert show];
        }
    }];
    double delayInSeconds = 10.0; // 10seconds to dismiss the loading view
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        //code to be executed on the main queue after delay
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    });
}

- (void) uploadDatabase:(BOOL)before withType:(NSString *)type
{
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.labelText = @"Sumitting...";
    if ([type isEqualToString:BUY]) {
        [IOSRequest saveVotesForUser:[self.preferences getUserID]
                           withVotes:[[NSUserDefaults standardUserDefaults] integerForKey:@"unupload_purchased_votes"]
                            andPrice:[[NSUserDefaults standardUserDefaults] floatForKey:@"unupload_purchased_votes_price"]
                        onCompletion:^(NSDictionary *results) {
                            if ([results[@"outcome"] isEqualToString:@"success"]) {
                                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"unupload_purchased_votes"];
                                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"unupload_purchased_votes_price"];
                                if (before) {
                                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                                } else {
                                    HUD.labelText = @"  Submited!  ";
                                    double delayInSeconds = 2.0; // 2 seconds to dismiss the loading view
                                    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
                                    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                                        //code to be executed on the main queue after delay
                                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                                    });
                                }
                            } else {
                                UIAlertView *alert = [[UIAlertView alloc] init];
                                alert.title = @"Submission Error";
                                alert.message = @"There was a problem submitting your request.  Please try again.";
                                [alert addButtonWithTitle:@"OK"];
                                [alert show];
                            }
                        }];
        double delayInSeconds = 10.0; // 10seconds to dismiss the loading view
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            //code to be executed on the main queue after delay
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        });
    } else {
        NSLog(@"%@",type);
        NSLog(@"%d-%f",[[NSUserDefaults standardUserDefaults] integerForKey:@"unupload_purchased_votes"],[[NSUserDefaults standardUserDefaults] floatForKey:@"unupload_purchased_votes_price"]);
        [IOSRequest saveBoostForUser:[self.preferences getUserID]
                           withType:[[NSUserDefaults standardUserDefaults] integerForKey:@"unupload_boost_votes"]
                            andPrice:[[NSUserDefaults standardUserDefaults] floatForKey:@"unupload_purchased_votes_price"]
                        onCompletion:^(NSDictionary *results) {
                            if ([results[@"outcome"] isEqualToString:@"success"]) {
                                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"unupload_boost_votes"];
                                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"unupload_purchased_votes_price"];
                                [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"boosted"];
                                [self.collectionView reloadData];
                                HUD.labelText = @"  Submited!  ";
                                double delayInSeconds = 2.0; // 2 seconds to dismiss the loading view
                                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
                                dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                                    //code to be executed on the main queue after delay
                                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                                });
                            } else {
                                UIAlertView *alert = [[UIAlertView alloc] init];
                                alert.title = @"Submission Error";
                                alert.message = @"There was a problem submitting your request.  Please try again.";
                                [alert addButtonWithTitle:@"OK"];
                                [alert show];
                            }
                        }];
        double delayInSeconds = 10.0; // 10seconds to dismiss the loading view
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            //code to be executed on the main queue after delay
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        });

    }
}

- (void) setInstruction: (NSString *)type
{
    if ([type isEqualToString:BOOST]) {
        self.leftTitle.text = @"Lasts for 24 Hours";
        self.leftContext.text = @"For every vote transaction you receive over the next 24 hours, you’ll receive an added boost.";
        self.rightTitle.text = @"Per Unique User";
        self.rightContext.text = @"You will receive each extra vote per transaction limited to one unique user at a time. You will not receive a boost if the same user votes multiple times. ";
    } else {
        self.leftTitle.text = @"Non-Expiring";
        self.leftContext.text = @"These votes can be used at any time and , unlike your daily vote, they do not expire at the end of the day";
        self.rightTitle.text = @"Do Not Reset";
        self.rightContext.text = @"If you give thse votes to a user and he/she does not win the monthly endorsement contest, they will carry over to next month’s tally.";
    }
}

#pragma mark - uicollection view data source
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 3;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    BuyVoteCVCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"buy_vote_cell" forIndexPath:indexPath];
    
    [cell.numberView makeRoundedBorderWithRadius:35]; // round
    [cell.numberView.layer setBorderWidth:7];
    [cell.numberView.layer setBorderColor:[[UIColor goldColor] CGColor]];
    
    [cell.priceView makeRoundedBorderWithRadius:5];
    [cell.priceView.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    [cell.priceView.layer setBorderWidth:1];
    
    [cell.button makeRoundedBorderWithRadius:3];
    cell.button.tag = indexPath.row; // which one is clicked
    if ([self.selected isEqualToString:BOOST] && [[NSUserDefaults standardUserDefaults] integerForKey:@"boosted"] == 1) {
        [cell.button setBackgroundColor:[UIColor lightGrayColor]];
        [cell.button setUserInteractionEnabled:NO];
    } else {
        [cell.button setBackgroundColor:[UIColor goldColor]];
        [cell.button setUserInteractionEnabled:YES];
    }
    
    if ([self.selected isEqualToString:BOOST]) {
        NSArray * boost = (NSArray *)self.products[BOOST];
        if (indexPath.row == 0) {
            cell.number.text = @"+1";
            cell.price.text = [NSString stringWithFormat:@"$ %.2f",((SKProduct *)boost[indexPath.row]).price.floatValue];
            cell.title.text = @"+1 BOOST";
            cell.context.text = @"RECEIVE 1 EXTRA VOTE FOR EVERY VOTE YOU’RE GIVEN OVER THE NEXT 24 HOURS";
        } else if (indexPath.row == 1) {
            cell.number.text = @"+3";
            cell.price.text = [NSString stringWithFormat:@"$ %.2f",((SKProduct *)boost[indexPath.row]).price.floatValue];
            cell.title.text = @"+3 BOOST";
            cell.context.text = @"RECEIVE 3 EXTRA VOTE FOR EVERY VOTE YOU’RE GIVEN OVER THE NEXT 24 HOURS";
        } else if (indexPath.row == 2) {
            cell.number.text = @"+5";
            cell.price.text = [NSString stringWithFormat:@"$ %.2f",((SKProduct *)boost[indexPath.row]).price.floatValue];
            cell.title.text = @"+5 BOOST";
            cell.context.text = @"RECEIVE 5 EXTRA VOTE FOR EVERY VOTE YOU’RE GIVEN OVER THE NEXT 24 HOURS";
        }
    } else if ([self.selected isEqualToString:BUY]) {
        NSArray * buy = (NSArray *)self.products[BUY];
        if (indexPath.row == 0) {
            cell.number.text = @"10";
            cell.price.text = [NSString stringWithFormat:@"$ %.2f",((SKProduct *)buy[indexPath.row]).price.floatValue];
            cell.title.text = @"GET 10 VOTES";
            cell.context.text = @"10 NON-EXPIRING VOTES TO GIVE TO OTHER ATHLETES";
        } else if (indexPath.row == 1) {
            cell.number.text = @"35";
            cell.price.text = [NSString stringWithFormat:@"$ %.2f",((SKProduct *)buy[indexPath.row]).price.floatValue];
            cell.title.text = @"GET 35 VOTES";
            cell.context.text = @"35 NON-EXPIRING VOTES TO GIVE TO OTHER ATHLETES";
        } else if (indexPath.row == 2) {
            cell.number.text = @"55";
            cell.price.text = [NSString stringWithFormat:@"$ %.2f",((SKProduct *)buy[indexPath.row]).price.floatValue];
            cell.title.text = @"GET 55 VOTES";
            cell.context.text = @"55 NON-EXPIRING VOTES TO GIVE TO OTHER ATHLETES";
        }
    }
    
    
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView
  didEndDisplayingCell:(UICollectionViewCell *)cell
    forItemAtIndexPath:(NSIndexPath *)indexPath
{
    BuyVoteCVCell* currentCell = ([[collectionView visibleCells]count] > 0) ? [collectionView visibleCells][0] : nil;
    
    if(cell != nil){
        NSInteger rowIndex = [collectionView indexPathForCell:currentCell].row;
        [self.pageController setCurrentPage:rowIndex];
        
    }
}


- (IBAction)getVoteClicked:(id)sender {
    [self.home_page setHidden:YES];
    self.selected = BOOST;
    [self.home setImage:[UIImage imageNamed:@"home_unselect"] forState:UIControlStateNormal];
    [self.buttonLine setHidden:NO];
    CGRect orginFrame = self.buttonLine.frame;
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.buttonLine.frame = CGRectMake(184, orginFrame.origin.y, orginFrame.size.width, orginFrame.size.height);
                     } completion:nil];
    [self setInstruction:BOOST];
    [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0] atScrollPosition:UICollectionViewScrollPositionNone animated:YES];
    [self.collectionView reloadData];
}

- (IBAction)buyVoteClicked:(id)sender {
    [self.home_page setHidden:YES];
    self.selected = BUY;
    [self.home setImage:[UIImage imageNamed:@"home_unselect"] forState:UIControlStateNormal];
    [self.buttonLine setHidden:NO];
    CGRect orginFrame = self.buttonLine.frame;
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.buttonLine.frame = CGRectMake(0, orginFrame.origin.y, orginFrame.size.width, orginFrame.size.height);
                     } completion:nil];
    [self setInstruction:BUY];
    [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0] atScrollPosition:UICollectionViewScrollPositionNone animated:YES];
    [self.collectionView reloadData];
}

- (IBAction)homeClicked:(id)sender {
    [self.home_page setHidden:NO];
    [self.home setImage:[UIImage imageNamed:@"home"] forState:UIControlStateNormal];
    [self.buttonLine setHidden:YES];
}

- (IBAction)buyButtonClicked:(id)sender {
    UIButton * btn = (UIButton *)sender;
    if ([self.selected isEqualToString:BUY]) {
        NSArray * buy = (NSArray *)self.products[BUY];
        switch (btn.tag) {
            case 0:
                // 10 votes
                NSLog(@"buy 10 votes");
                if ([[NSUserDefaults standardUserDefaults] integerForKey:@"unupload_purchased_votes"] > 0) {
                    [self uploadDatabase:YES withType:BUY]; // clear the un-upload data
                }
                [[VotePuchaseTier1Helper sharedInstance] buyProduct:(SKProduct *)buy[btn.tag]];
                break;
            case 1:
                // 35 votes
                NSLog(@"buy 35 votes");
                if ([[NSUserDefaults standardUserDefaults] integerForKey:@"unupload_purchased_votes"] > 0) {
                    [self uploadDatabase:YES withType:BUY]; // clear the un-upload data
                }
                [[VotePuchaseTier1Helper sharedInstance] buyProduct:(SKProduct *)buy[btn.tag]];
                break;
            case 2:
                // 55 votes
                NSLog(@"buy 55 votes");
                if ([[NSUserDefaults standardUserDefaults] integerForKey:@"unupload_purchased_votes"] > 0) {
                    [self uploadDatabase:YES withType:BUY]; // clear the un-upload data
                }
                [[VotePuchaseTier1Helper sharedInstance] buyProduct:(SKProduct *)buy[btn.tag]];
                break;
            default:
                break;
        }
    } else if ([self.selected isEqualToString:BOOST]) {
        NSArray * boost = (NSArray *)self.products[BOOST];
        switch (btn.tag) {
            case 0:
                // 1 votes
                NSLog(@"boost 1 votes");
                [[VotePuchaseTier1Helper sharedInstance] buyProduct:(SKProduct *)boost[btn.tag]];
                break;
            case 1:
                // 3 votes
                NSLog(@"boost 3 votes");
                [[VotePuchaseTier1Helper sharedInstance] buyProduct:(SKProduct *)boost[btn.tag]];
                break;
            case 2:
                // 5 votes
                NSLog(@"boost 5 votes");
                [[VotePuchaseTier1Helper sharedInstance] buyProduct:(SKProduct *)boost[btn.tag]];
                break;
            default:
                break;
        }

    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
