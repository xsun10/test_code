//
//  SupportsTVC.h
//  Vaiden
//
//  Created by Turbo on 7/23/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SupportsTVC : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *profilePic;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *votes;
@property (weak, nonatomic) IBOutlet UILabel *yourVotes;
@property (weak, nonatomic) IBOutlet UILabel *region;
@property (weak, nonatomic) IBOutlet UILabel *rank;
@property (weak, nonatomic) IBOutlet UIView *bar;
@property (weak, nonatomic) IBOutlet UIView *redbar;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *redBarWidth;
@property (weak, nonatomic) IBOutlet UIView *greenBar;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *greenBarWidth;
@property (weak, nonatomic) IBOutlet UIButton *profileBtn;
@end
