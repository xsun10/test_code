//
//  UpcomingMatchesTVC.m
//  Phenom
//
//  Created by James Chung on 5/11/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "MatchListTVC.h"
#import "IndividualMatchListCell.h"
#import "UserPreferences.h"
#import "IOSRequest.h"
#import "PickupMatchDetailTVC.h"
#import "IndividualMatchDetailTVC.h"
#import "S3Tools.h"
#import "AFNetworking.h"
#import "UIImage+ProportionalFill.h"
#import "UIView+Manipulate.h"
#import "UIColor+VaidenColors.h"
#import "NSDate+Utilities.h"
#import "PlayerDetailTVC.h"

@interface MatchListTVC ()

@property (nonatomic, strong) UserPreferences *preferences;
@property (nonatomic, strong) NSMutableArray *upcomingMatches;
@property (nonatomic) NSInteger selectedSection;
@property (nonatomic, strong) UIView *tempView;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
@property (nonatomic) NSInteger selectedPlayerToShowDetailPage;


@end

@implementation MatchListTVC



- (NSMutableArray *)upcomingMatches
{
    if (!_upcomingMatches) _upcomingMatches = [[NSMutableArray alloc] init];
    return _upcomingMatches;
}

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
    
    [self showTempSpinner];


    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refreshView:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshControl];
    
    [self refreshView:refreshControl];
}

- (void)showTempSpinner
{
    self.tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    self.tempView.backgroundColor = [UIColor lightGrayColor];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] init];
    [self.activityIndicator setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width/2.0, [UIScreen mainScreen].bounds.size.height/2.0 - 50)];
    [self.activityIndicator startAnimating];
    [self.tempView addSubview:self.activityIndicator];
    
    [self.view addSubview:self.tempView];
    
}


-(void)refreshView:(UIRefreshControl *)refresh
{
    [self.upcomingMatches removeAllObjects];
    
    refresh.attributedTitle = [[NSAttributedString alloc] initWithString:@"Refreshing data..."];
      
    if (self.loadMatchType == LOAD_UPCOMING_MATCHES)
        [self loadUpcomingMatchData];
    else if (self.loadMatchType == LOAD_TODAYS_MATCHES)
        [self loadTodaysMatchData];
    else if (self.loadMatchType == LOAD_PAST_MATCHES)
        [self loadPastMatchData];
    
    [refresh endRefreshing];
}

- (void)loadUpcomingMatchData
{
    UINavigationController *navCon  = (UINavigationController*) [self.navigationController.viewControllers objectAtIndex:1];
    navCon.navigationItem.title = @"Upcoming Matches";
    [IOSRequest fetchMyUpcomingMatches:[self.preferences getUserID]
                          onCompletion:^(NSDictionary *results) {
                                  [self loadIndividualMatches:results[@"upcoming_individual_matches"] ];
                                  [self loadPickupMatches:results[@"upcoming_pickup_matches"]];
                              
                              dispatch_async(dispatch_get_main_queue(), ^ {
                                  
                                  [self.tableView reloadData];
                                  [self.activityIndicator stopAnimating];
                                  self.tempView.hidden = YES;

                              });
                              
    }];
  
}

- (void)loadTodaysMatchData
{
    UINavigationController *navCon  = (UINavigationController*) [self.navigationController.viewControllers objectAtIndex:1];
    navCon.navigationItem.title = @"Today's Matches";
    [IOSRequest fetchTodaysgMatches:[self.preferences getUserID]
                       onCompletion:^(NSDictionary *results) {
                           [self loadIndividualMatches:results[@"todays_individual_matches"] ];
                           [self loadPickupMatches:results[@"todays_pickup_matches"] ];
                           
                           dispatch_async(dispatch_get_main_queue(), ^ {
                               [self.tableView reloadData];
                               [self.activityIndicator stopAnimating];
                               self.tempView.hidden = YES;

                           });
                           
                       }];
}

- (void)loadPastMatchData
{
    UINavigationController *navCon = (UINavigationController *)[self.navigationController.viewControllers objectAtIndex:1];
    navCon.navigationItem.title = @"Past Matches";
    [IOSRequest fetchPastMatchesForUser:[self.preferences getUserID]
                                     onCompletion:^(NSDictionary *results) {
                                         
                                         for (id object in results[@"past_matches"]) {
                                             if ([object[@"match_category"] isEqualToString:@"individual"]) {
                                                 [self loadSingleIndividualMatch:object];
                                             } else if ([object[@"match_category"] isEqualToString:@"pickup"]) {
                                                 [self loadSinglePickupMatch:object];
                                             }
                                         }
                                         
                                         dispatch_async(dispatch_get_main_queue(), ^{
                                             [self.tableView reloadData];
                                             [self.activityIndicator stopAnimating];
                                             self.tempView.hidden = YES;

                                         });
                                         
                                     }];
}




- (void)loadSingleIndividualMatch:(NSDictionary *)object
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *dateTime = [[formatter dateFromString:object[@"date_time"]] toLocalTime];
    
    Venue *venue = [[Venue alloc] initVenueWithName:object[@"venue_name"]
                                         andVenueID:[object[@"venue_id"] integerValue]
                                         andAddress:object[@"venue_street"]
                                            andCity:object[@"venue_city"]
                                       andStateName:object[@"venue_state"]
                                         andStateID:[object[@"venue_state_id"] integerValue]
                                        andLatitude:[object[@"latitude"] floatValue]
                                       andLongitude:[object[@"longitude"] floatValue]
                                     andDescription:object[@"description"]
                                         andCreator:[object[@"venue_creator_id"] integerValue]];
    
    Sport *sport = [[Sport alloc] initWithSportDetails:object[@"sport_name"]
                                               andType:object[@"sport_type"]];
    
    
    MatchPlayer *player2 = [MatchPlayer makeMatchCompetitorObject:object[@"competitors"]
                                                        forPlayer:2
                                                     forSportName:object[@"sport_name"]
                                                          andType:object[@"sport_type"]];
    
    MatchPlayer *player1 = [MatchPlayer makeMatchCompetitorObject:object[@"competitors"]
                                                        forPlayer:1
                                                     forSportName:object[@"sport_name"]
                                                          andType:object[@"sport_type"]];
    
    MatchPlayer *sessionPlayer = nil;
    MatchPlayer *otherPlayer = nil;
    
    if (player1.userID == [self.preferences getUserID]) {
        sessionPlayer = player1;
        otherPlayer = player2;
    } else {
        sessionPlayer = player2;
        otherPlayer = player1;
    }
    
    NSDictionary *prob = object[@"probabilities"];
    
    float prob_session_player;
    float prob_other_player;
    
    if ([prob[@"player1_id"] integerValue] == sessionPlayer.userID) {
        prob_session_player = [prob[@"prob_player1"] floatValue];
        prob_other_player = [prob[@"prob_player2"] floatValue];
    } else {
        prob_session_player = [prob[@"prob_player2"] floatValue];
        prob_other_player = [prob[@"prob_player1"] floatValue];
    }
    
    IndividualMatch *iMatch = [[IndividualMatch alloc]
                               initMatchWithDetails:[object[@"match_id"] integerValue]
                               andMatchName:object[@"match_name"]
                               isPrivate:[object[@"is_private"] boolValue]
                               withSport:sport
                               withMessage:object[@"message"]
                               onDateTime:dateTime
                               atVenue:venue
                               createdByUser:[object[@"creator_id"] integerValue]
                               withMatchPlayer1:sessionPlayer
                               withMatchPlayer2:otherPlayer
                               andActive:[object[@"active"] boolValue]
                               andProbabilityPlayer1Wins:prob_session_player
                               andProbabilityPlayer2Wins:prob_other_player];
    [iMatch setMatchStatus:object[@"match_status"]];
    
    
    if (self.loadMatchType == LOAD_PAST_MATCHES) { // only need to know scores for past matches
        /* For Scores */
        id scoreSummaryObject = object[@"score_summary"];
        
        for (id roundScoreObj in scoreSummaryObject[@"match_scores"]) {
            
            // Hack to make sure that player1 user id is same for individualmatch.scoreIndMatch values and individualmatch.player1
            // Same thing goes for player2
            
            if ([roundScoreObj[@"player1_id"] integerValue] == iMatch.player1.userID) {
                [iMatch.scoreIndMatch addRoundScore:[roundScoreObj[@"round"] integerValue]
                                         forPlayer1:[roundScoreObj[@"player1_id"] integerValue]
                                   withScorePlayer1:[roundScoreObj[@"score_player1"] integerValue]
                                         forPlayer2:[roundScoreObj[@"player2_id"] integerValue]
                                   withScorePlayer2:[roundScoreObj[@"score_player2"] integerValue]];
            } else {
                [iMatch.scoreIndMatch addRoundScore:[roundScoreObj[@"round"] integerValue]
                                         forPlayer1:[roundScoreObj[@"player2_id"] integerValue]
                                   withScorePlayer1:[roundScoreObj[@"score_player2"] integerValue]
                                         forPlayer2:[roundScoreObj[@"player1_id"] integerValue]
                                   withScorePlayer2:[roundScoreObj[@"score_player1"] integerValue]];
            }
            
            
            [iMatch.scoreIndMatch setVersion:[scoreSummaryObject[@"version"] integerValue]];
            [iMatch.scoreIndMatch setScoreRecordedBy:[scoreSummaryObject[@"recorder_user_id"] integerValue]];
            [iMatch.scoreIndMatch setScoreConfirmedBy:[scoreSummaryObject[@"confirmer_user_id"] integerValue]];
            [iMatch.scoreIndMatch setScoreDisputedBy:[scoreSummaryObject[@"disputor_user_id"] integerValue]];
            [iMatch.scoreIndMatch setWasFirstVersionIndividualScoreRecordedBySessionUserAndDisputedByOtherUser:[scoreSummaryObject[@"was_session_user_score_overruled"] boolValue]];
        }
        /* End For Scores */
    }
    [self.upcomingMatches addObject:iMatch];
    //         if ([self.upcomingMatches count] == ([individualMatches count] + pCount)) {
    
    //        }

}

- (void)loadSinglePickupMatch:(NSDictionary *)object
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *dateTime = [[formatter dateFromString:object[@"date_time"]] toLocalTime];
    
    Venue *venue = [[Venue alloc] initVenueWithName:object[@"venue_name"]
                                         andVenueID:[object[@"venue_id"] integerValue]
                                         andAddress:object[@"venue_street"]
                                            andCity:object[@"venue_city"]
                                       andStateName:object[@"venue_state"]
                                         andStateID:[object[@"venue_state_id"] integerValue]
                                        andLatitude:[object[@"latitude"] floatValue]
                                       andLongitude:[object[@"longitude"] floatValue]
                                     andDescription:object[@"description"]
                                         andCreator:[object[@"venue_creator_id"] integerValue]];
    
    Sport *sport = [[Sport alloc] initWithSportDetails:object[@"sport_name"]
                                               andType:object[@"sport_type"]];
    [sport setSportID:[object[@"sport_id"] integerValue]];
    
    NSMutableArray *competitorArray = [PickupMatch makeCompetitorArray:object[@"competitors"] forSportName:object[@"sport_name"] andType:object[@"sport_type"]];
    
    PickupMatch *iMatch = [[PickupMatch alloc]
                           initMatchWithDetails:[object[@"match_id"] integerValue]
                           andMatchName:object[@"match_name"]
                           isPrivate:[object[@"is_private"] boolValue]
                           withSport:sport
                           withMessage:object[@"message"]
                           onDateTime:dateTime
                           atVenue:venue
                           createdByUser:[object[@"creator_id"] integerValue]
                           withUsername:[Match getUsernameForID:[object[@"creator_id"] integerValue] inArray:competitorArray]
                           andProfileStr:object[@"creator_profile_pic_string"]
                           minCompetitors:[object[@"min_competitors"] integerValue]
                           withCompetitors:competitorArray
                           withExperience:[NSMutableArray arrayWithArray:object[@"experience"]]
                           andMatchType:[[MatchType alloc] initWithMatchType:[object[@"match_type_id"] integerValue] withDescription:object[@"match_type_description"]]
                           andActive:[object[@"active"] boolValue]
                           andMatchStatus:object[@"match_status"]];
    
    [iMatch setGender:object[@"gender"]];
    [self.upcomingMatches addObject:iMatch];
    
    //     if ([self.upcomingMatches count] == ([pickupMatches count] + iCount)) {
    //         dispatch_async(dispatch_get_main_queue(), ^{
    //             [self.tableView reloadData];
    //         });
    //     }

}

- (void)loadIndividualMatches:(NSArray *)individualMatches
{
    for (id object in individualMatches ) {
        [self loadSingleIndividualMatch:object];
        
    }
    
}

- (void)loadPickupMatches:(NSArray *)pickupMatches
{
    for (id object in pickupMatches) {
        [self loadSinglePickupMatch:object];
        
    }
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    return [self.upcomingMatches count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    id object = (self.upcomingMatches)[indexPath.section];
    
    
    if ([object isKindOfClass:[IndividualMatch class]]) {
        /***************************************** INDIVIDUAL MATCH **************************************************/
        static NSString *CellIdentifier = @"Upcoming Individual Matches Cell"; // or upcoming pickup matches cell
        IndividualMatchListCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        
        IndividualMatch *iMatch = (IndividualMatch *)object;
        
        /* pics */
        
        NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                baseString:iMatch.player1.profileBaseString];
        
        [cell.player1ProfilePic setImageWithURL:[NSURL URLWithString:url]
                          placeholderImage:[UIImage imageNamed:@"avatar square"]];
        
        cell.player1ProfilePic.layer.borderColor = [UIColor whiteColor].CGColor;
        cell.player1ProfilePic.layer.borderWidth = 1;
        
        NSString *url2 = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                baseString:iMatch.player2.profileBaseString];
        
        [cell.player2ProfilePic setImageWithURL:[NSURL URLWithString:url2]
                               placeholderImage:[UIImage imageNamed:@"avatar square"]];
        
        cell.player2ProfilePic.layer.borderColor = [UIColor whiteColor].CGColor;
        cell.player2ProfilePic.layer.borderWidth = 1;
        
        
        
        // profile pic button
        UIButton *opponentPicButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 125, 125)];
        [opponentPicButton addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
        opponentPicButton.tag = iMatch.player1.userID;
        [cell.player1ProfilePic addSubview:opponentPicButton];
        cell.player1ProfilePic.userInteractionEnabled = YES;
        cell.player1Username.text = iMatch.player1.userName;

        [cell.vsCircle makeRoundedBorderWithRadius:30];
        
        UIButton *opponentPicButton2 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 125, 125)];
        [opponentPicButton2 addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
        opponentPicButton2.tag = iMatch.player2.userID;
        [cell.player2ProfilePic addSubview:opponentPicButton2];
        cell.player2ProfilePic.userInteractionEnabled = YES;
        cell.player2Username.text = iMatch.player2.userName;
       /*
        NSString *sessionUserUrl = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                           baseString:[self.preferences getProfilePicString]];
        
        
        [cell.sessionUserProfilePic setImageWithURL:[NSURL URLWithString:sessionUserUrl]
                                   placeholderImage:[UIImage imageNamed:@"avatar round"]];
        
        [UIImage makeRoundedImage:cell.sessionUserProfilePic withRadius:25];
        
        cell.sessionUserProfilePic.layer.borderColor = [UIColor whiteColor].CGColor;
        cell.sessionUserProfilePic.layer.borderWidth = 1;
        
        UIButton *sessionUserPicButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
        [sessionUserPicButton addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
        sessionUserPicButton.tag = [self.preferences getUserID];
        [cell.sessionUserProfilePic addSubview:sessionUserPicButton];
        cell.sessionUserProfilePic.userInteractionEnabled = YES;
     */
        

        
        /* end pics */
        
        cell.matchTypeLabel.text = @"Challenge Match";
        if (self.loadMatchType == LOAD_PAST_MATCHES) {
            cell.messageLabel.text = [iMatch getMatchStatusMessage:[self.preferences getUserID]];
        } else {
            cell.messageLabel.text = [NSString stringWithFormat:@"%@ is scheduled to play you in %@", iMatch.player2.userName,iMatch.sport.sportName];
        }
        cell.matchNameLabel.text = iMatch.matchName;
        cell.sportIcon.image = [[iMatch.sport getSportIcon2] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [cell.sportIcon setTintColor:[UIColor darkGrayColor2]];
        [cell.sportIconBackground makeRoundedBorderWithRadius:10];
        
        cell.sportPictureBackground.contentMode = UIViewContentModeScaleAspectFill;
        cell.sportPictureBackground.clipsToBounds = YES;
        cell.sportPictureBackground.image = [iMatch getMatchDetailPageBackground];
        
        
        cell.matchHeading.text = [NSString stringWithFormat:@"%@ CHALLENGE MATCH", [iMatch.sport.sportName uppercaseString]];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"MMMM d, YYYY"];
        cell.dateLabel.text = [dateFormatter stringFromDate:iMatch.dateTime];
        
        cell.locationLabel.text = iMatch.venue.venueName;
        cell.detailsButton.tag = indexPath.section;
        
        [cell.detailsButton addTarget:self action:@selector(detailsActionIndividualMatch:) forControlEvents:UIControlEventTouchUpInside];
        
        UIButton *newsfeedIconButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
        [newsfeedIconButton addTarget:self action:@selector(detailsActionIndividualMatch:) forControlEvents:UIControlEventTouchUpInside];
        newsfeedIconButton.tag = indexPath.section;
        [cell.newsfeedIcon addSubview:newsfeedIconButton];
        cell.newsfeedIcon.userInteractionEnabled = YES;

        
        
        [cell.mainBackgroundViewArea makeRoundedBorderWithRadius:3];
        cell.mainBackgroundViewArea.layer.borderColor = [UIColor midGray2].CGColor;
        cell.mainBackgroundViewArea.layer.borderWidth = 1.0;
        
        return cell;
    
    } else if ([object isKindOfClass:[PickupMatch class]]) {
        /******************************************* PICKUP MATCH ****************************************************/
        static NSString *CellIdentifier = @"Upcoming Pickup Matches Cell";
        PickupMatchListCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        
        PickupMatch *pMatch = (PickupMatch *)object;
        
        cell.matchNameLabel.text = pMatch.matchName;
        cell.matchHeading.text = [NSString stringWithFormat:@"%@ PICKUP MATCH", [pMatch.sport.sportName uppercaseString]];
     //   cell.matchHeading.textColor = [UIColor peacock];
        cell.locationLabel.text = pMatch.venue.venueName;
        cell.sessionUserStatus.text = [NSString stringWithFormat:@"You're scheduled to play in this match"];

        cell.sportIcon.image = [[pMatch.sport getSportIcon2] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [cell.sportIcon setTintColor:[UIColor darkGrayColor2]];
        [cell.sportIconBackground makeRoundedBorderWithRadius:10];
        cell.sportLabel.text = [pMatch.sport.sportName capitalizedString];
        
        cell.sportPictureBackground.contentMode = UIViewContentModeScaleAspectFill;
        cell.sportPictureBackground.clipsToBounds = YES;
        cell.sportPictureBackground.image = [pMatch getMatchDetailPageBackground];
  
        cell.genderLabel.text = [pMatch.gender capitalizedString];
        
        cell.matchStatus.text = [pMatch getMatchStatus];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"MMM d"];
        cell.dateLabel.text = [dateFormatter stringFromDate:pMatch.dateTime];
        
        [cell.mainBackgroundViewArea makeRoundedBorderWithRadius:3];
        cell.mainBackgroundViewArea.layer.borderColor = [UIColor midGray2].CGColor;
        cell.mainBackgroundViewArea.layer.borderWidth = 1.0;
        NSInteger numDiff = [pMatch getNumConfirmed] - 2;
        
        if (numDiff > 0)
            cell.additionalPlayersLabel.text = [NSString stringWithFormat:@"and %ld others", numDiff];
        else
            cell.additionalPlayersLabel.hidden = YES;
        
        cell.delegate = self;
        
        cell.competitors = pMatch.competitorArray;
        cell.detailsButton.tag = indexPath.section;
        [cell.detailsButton addTarget:self action:@selector(detailsActionPickupMatch:) forControlEvents:UIControlEventTouchUpInside];

        UIButton *newsfeedIconButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
        [newsfeedIconButton addTarget:self action:@selector(detailsActionPickupMatch:) forControlEvents:UIControlEventTouchUpInside];
        newsfeedIconButton.tag = indexPath.section;
        [cell.newsfeedIcon addSubview:newsfeedIconButton];
        cell.newsfeedIcon.userInteractionEnabled = YES;
        
   //     [cell.detailsButton makeCircleWithColor:[UIColor midGray2] andRadius:5.0];
        
        return cell;
    }
    
    
    // Configure the cell...
    
    return nil;
}


- (CGFloat)tableView:(UITableView *)t heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([(self.upcomingMatches)[indexPath.section] isKindOfClass:[IndividualMatch class]]) {
        return 222;
    } else if ([(self.upcomingMatches)[indexPath.section] isKindOfClass:[PickupMatch class]]) {
        return 294;
    }
    
    return 0;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"Pickup Match Detail Segue"]){
        
        PickupMatchDetailTVC *controller = (PickupMatchDetailTVC *)segue.destinationViewController;
        
       PickupMatch *pickupMatch = (self.upcomingMatches)[self.selectedSection];
        controller.matchID = pickupMatch.matchID;
        
    } else if([segue.identifier isEqualToString:@"Individual Match Detail Segue"]){
        
        IndividualMatchDetailTVC *controller = (IndividualMatchDetailTVC *)segue.destinationViewController;
        
        IndividualMatch *match = (self.upcomingMatches)[self.selectedSection];
        controller.matchID = match.matchID;
        
    } /*else if ([segue.identifier isEqualToString:@"Decline Upcoming Individual Match"]) {
        RSVPForMatchTVC *controller = (RSVPForMatchTVC *)segue.destinationViewController;
        controller.matchObject = (self.upcomingMatches)[self.selectedSection];
        controller.acceptDecline = @"Decline";
    } else if ([segue.identifier isEqualToString:@"Decline Upcoming Pickup Match"]) {
        RSVPForMatchTVC *controller = (RSVPForMatchTVC *)segue.destinationViewController;
        controller.matchObject = (self.upcomingMatches)[self.selectedSection];
        controller.acceptDecline = @"Decline";
        
    } */ else if ([segue.identifier isEqualToString:@"Player Detail Segue"]) {
        PlayerDetailTVC *controller = (PlayerDetailTVC *)segue.destinationViewController;
        controller.playerUserID = self.selectedPlayerToShowDetailPage;
    }
    
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
/*    self.selectedSection = indexPath.section;
    if ([(self.upcomingMatches)[indexPath.section] isKindOfClass:[PickupMatch class]]) {
        [self performSegueWithIdentifier:@"Pickup Match Detail Segue" sender:self];
    } else if ([(self.upcomingMatches)[indexPath.section] isKindOfClass:[IndividualMatch class]]) {
        [self performSegueWithIdentifier:@"Individual Match Detail Segue" sender:self];
    }*/
}

- (NSString *)getPastMatchStatusMessage:(IndividualMatch *)iMatch
{
    NSString *message = @"";
    
    if ([iMatch.matchStatus isEqualToString:@"SCORE_RECORDED"]) {
        
    } else if ([iMatch.matchStatus isEqualToString:@"SCORE_CONFIRMED"]) {
        // show the score
    } else if ([iMatch.matchStatus isEqualToString:@"CANCELLED"]) {
        
    } else if ([iMatch.matchStatus isEqualToString:@"NEVER_OCCURRED"]) {
        
    }
    
    return message;
}

- (IBAction)detailsActionPickupMatch:(UIButton *)sender
{
    self.selectedSection = sender.tag;
 //   if ([(self.upcomingMatches)[self.selectedSection] isKindOfClass:[PickupMatch class]]) {
        [self performSegueWithIdentifier:@"Pickup Match Detail Segue" sender:self];
 //   } else if ([(self.upcomingMatches)[self.selectedSection] isKindOfClass:[IndividualMatch class]]) {
 //       [self performSegueWithIdentifier:@"Individual Match Detail Segue" sender:self];
 //   }
}
- (IBAction)detailsActionIndividualMatch:(UIButton *)sender
{
    self.selectedSection = sender.tag;
    [self performSegueWithIdentifier:@"Individual Match Detail Segue" sender:self];
    
}

// for individual matches
- (void)goToProfile:(UIButton *)sender
{
    self.selectedPlayerToShowDetailPage = sender.tag;
    [self performSegueWithIdentifier:@"Player Detail Segue" sender:self];
}

// for pickup matches
- (void)setPickupMatchListCellViewController:(PickupMatchListCell *)controller withUserIDToSegue:(NSInteger)userID
{
    self.selectedPlayerToShowDetailPage = userID;
    [self performSegueWithIdentifier:@"Player Detail Segue" sender:self];
}

@end
