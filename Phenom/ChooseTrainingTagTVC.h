//
//  ChooseTrainingTagTVC.h
//  Vaiden
//
//  Created by James Chung on 5/27/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "CustomBaseTVC.h"

@class ChooseTrainingTagTVC;

@protocol ChooseTrainingTagTVC_Delegate <NSObject>

- (void)setWithViewController:(ChooseTrainingTagTVC *)controller withTrainingTags:(NSMutableArray *)trainingTags;

@end

@interface ChooseTrainingTagTVC : CustomBaseTVC
@property (nonatomic, weak) id <ChooseTrainingTagTVC_Delegate> delegate;


@property (nonatomic) NSInteger sportID;

@end
