//
//  UITextField+FormValidations.m
//  Phenom
//
//  Created by James Chung on 3/20/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "UITextField+FormValidations.h"

@implementation UITextField (FormValidations)


- (BOOL)checkEmail
{
    if ([self.text length] >= 50) return NO;
    
    NSString *regexString = @"^[+\\w\\.\\-']+@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*(\\.[a-zA-Z]{2,})+$";
    
    if (nil == self.text)
        self.text = [NSString string];
    
    return [self checkWithRegex:regexString];
}

- (BOOL)checkWithRegex:(NSString *)regexString
{
    
    BOOL success = YES;
    
    if(!self.text)
    {
        success = NO;
    }
    else if(regexString)
    {
        NSError *error = NULL;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:regexString options:NSRegularExpressionCaseInsensitive error:&error];
        if(!error)
        {
            NSUInteger numberOfMatches = [regex numberOfMatchesInString:self.text options:0 range:NSMakeRange(0, self.text.length)];
            success = numberOfMatches == 1;
        }
    }
    
    return success;
}


- (BOOL)checkAlphanumeric
{
    if ([self.text length] >= 32) return NO;
    
    NSString *regexString = @"^[a-z0-9_-]{3,15}$";
    
    if (nil == self.text)
        self.text = [NSString string];
    
    return [self checkWithRegex:regexString];
}

- (BOOL)checkAlphanumericNoLengthLimit
{
    NSString *regexString = @"^[a-z0-9_-]{3,15}$";
    
    if (nil == self.text)
        self.text = [NSString string];
    
    return [self checkWithRegex:regexString];
}

- (BOOL)checkStreetAddress
{
    if ([self.text length] >= 100) return NO;
    
  //  NSString *regexString = @"^[a-zA-Z]+(?:[\\s-][a-zA-Z]+)*$";
    NSString *regexString = @"^[a-zA-Z0-9\\s.\\-\\'\\!\\?\\,\\:\\&\\$\\@]+$";
    if (nil == self.text)
        self.text = [NSString string];
    
    return [self checkWithRegex:regexString];
    
}

- (BOOL)checkCityName
{
    if ([self.text length] >= 50 && [self.text length]<=0) return NO;
    
    NSString *regexString = @"^[a-zA-Z]+(?:[\\s-][a-zA-Z]+)*$";
    
    if (nil == self.text)
        self.text = [NSString string];
    
    return [self checkWithRegex:regexString];
    
}

- (BOOL)checkLocationName
{
    if ([self.text length] >= 50 || [self.text length] <=0) return NO;
    return YES;
}
- (BOOL)checkPassword
{
    if ([self.text length] >= 32) return NO;
    
    NSString *regexString = @"^[A-Za-z0-9!@#$%^&*()_]{6,20}$";
    
    if (nil == self.text)
        self.text = [NSString string];
    
    return [self checkWithRegex:regexString];
}

- (BOOL)checkMatchName
{
    if ([self.text length] >= 50 || [self.text length] < 3) return NO;
    
    NSString *regexString = @"^[a-zA-Z0-9\\s.\\-\\'\\!\\?\\,\\:\\&\\$\\@]+$";
    
    if (nil == self.text)
        self.text = [NSString string];
    
    return [self checkWithRegex:regexString];
}

- (BOOL)checkTeamName
{
    if ([self.text length] >= 20 || [self.text length] < 3) return NO;
    
    NSString *regexString = @"^[a-zA-Z]+(?:[\\s-][a-zA-Z]+)*$";
    
    if (nil == self.text)
        self.text = [NSString string];
    
    return [self checkWithRegex:regexString];
    
}


@end
