//
//  ContactListTVCell.h
//  Vaiden
//
//  Created by Turbo on 6/27/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactListTVCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *profilePic;
@property (weak, nonatomic) IBOutlet UILabel *fullname;

@end
