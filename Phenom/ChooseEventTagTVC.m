//
//  ChooseEventTagTVC.m
//  Vaiden
//
//  Created by James Chung on 5/28/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "ChooseEventTagTVC.h"
#import "UserPreferences.h"
#import "IOSRequest.h"
#import "NSDate+Utilities.h"
#import "ChooseEventTagCell.h"
#import "UIView+Manipulate.h"

@interface ChooseEventTagTVC () <UISearchBarDelegate>

@property (nonatomic, strong) NSMutableArray *eventTags;
@property (weak, nonatomic) IBOutlet UISearchBar *eventSearchBar;
@property (nonatomic, strong) UserPreferences *preferences;

@end

@implementation ChooseEventTagTVC

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (NSMutableArray *)eventTags
{
    if (!_eventTags) _eventTags = [[NSMutableArray alloc] init];
    return _eventTags;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
//    [self setDoneButton];
    self.eventSearchBar.delegate = self;
    
    [self loadEvents];
}

/*- (void)setDoneButton
{
    UIButton *btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 50, 21);
    [btn makeRoundedBorderWithRadius:3];
    btn.layer.borderColor = [UIColor whiteColor].CGColor;
    btn.layer.borderWidth = 1.0;
    
    [btn addTarget:self action:@selector(doneAction:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundColor:[UIColor clearColor]];
    [btn setTitle:@"DONE" forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-MediumItalic" size:12.0]];
    
    
    // Initialize UIBarbuttonitem...
    UIBarButtonItem *proe = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = proe;
}*/


// onload display users events if there are any
- (void)loadEvents
{
    [self.eventTags removeAllObjects];
    [IOSRequest fetchUserEvents:[self.preferences getUserID]
                   onCompletion:^(NSMutableArray *results) {
                       
                       
                       
                       for (id object in results) {
                           NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
                           [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                           NSDate *dateTime = [[formatter dateFromString:object[@"event_date"]] toLocalTime];
                    
                           Venue *eventVenue = [[Venue alloc] initVenueWithName:object[@"venue_name"]
                                                                     andVenueID:[object[@"venue_id"] integerValue]];
                           [eventVenue setCity:object[@"venue_city"]];
                           [eventVenue setStreetAddress:object[@"venue_street"]];
                           [eventVenue setStateName:object[@"venue_state"]];
                           
                           EventTag *event = [[EventTag alloc] initWithEvent:[object[@"event_id"] integerValue]
                                                                andEventName:object[@"event_name"]
                                                                andEventDate:dateTime
                                                                    andVenue:eventVenue
                                                               andWebAddress:object[@"event_web_address"]
                                                              andPhoneNumber:object[@"event_phone"]
                                                             andEmailAddress:object[@"event_email"]];
                           
                           [self.eventTags addObject:event];
                       }
                       
                       dispatch_async(dispatch_get_main_queue(), ^{
                          
                           [self.tableView reloadData];
                       });
                   }];
}

// as user types, update the tableview with search results.. if do not choose anything from
// tableviewcells, then event will be made from the text input in the search bar
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [self.eventTags removeAllObjects];
    
    if ([searchText length] > 1) {
        [IOSRequest fetchSimilarEvents:searchText
                              forSport:self.sportID
                          onCompletion:^(NSMutableArray *results) {
                          
                          for (id object in results) {
                              NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
                              [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                              NSDate *dateTime = [[formatter dateFromString:object[@"event_date"]] toLocalTime];
                              
                              Venue *eventVenue = [[Venue alloc] initVenueWithName:object[@"venue_name"]
                                                                        andVenueID:[object[@"venue_id"] integerValue]];
                              [eventVenue setCity:object[@"venue_city"]];
                              [eventVenue setStreetAddress:object[@"venue_street"]];
                              [eventVenue setStateName:object[@"venue_state"]];
                              
                              EventTag *event = [[EventTag alloc] initWithEvent:[object[@"event_id"] integerValue]
                                                                   andEventName:object[@"event_name"]
                                                                   andEventDate:dateTime
                                                                       andVenue:eventVenue
                                                                  andWebAddress:object[@"event_web_address"]
                                                                 andPhoneNumber:object[@"event_phone"]
                                                                andEmailAddress:object[@"event_email"]];
                              
                              [self.eventTags addObject:event];
                          }
                          
                          dispatch_async(dispatch_get_main_queue(), ^{
                             
                              [self.tableView reloadData];
                          });
                      }];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  [self.eventTags count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    EventTag *event = [self.eventTags objectAtIndex:indexPath.row];
    
    if (event.eventVenue != nil) {
        return 70;
    }
    
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ChooseEventTagCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Event Cell" forIndexPath:indexPath];
    
    EventTag *event = [self.eventTags objectAtIndex:indexPath.row];
    
    cell.eventNameLabel.text = event.eventName;
    if ([event.eventVenue.streetAddress length] > 0 && [event.eventVenue.city length] > 0 && [event.eventVenue.stateName length] > 0)
        cell.eventAddressLabel.text = [NSString stringWithFormat:@"%@, %@, %@", event.eventVenue.streetAddress, event.eventVenue.city, event.eventVenue.stateName];
    else if ([event.eventVenue.city length] > 0 && [event.eventVenue.stateName length] > 0)
        cell.eventAddressLabel.text = [NSString stringWithFormat:@"%@, %@", event.eventVenue.city, event.eventVenue.stateName];
    else if ([event.eventVenue.stateName length] > 0)
        cell.eventAddressLabel.text = [NSString stringWithFormat:@"%@" ,event.eventVenue.stateName];
    else
        cell.eventAddressLabel.hidden = YES;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM/dd/yyyy at hh:mm a"];
    
    cell.eventDateTimeLabel.text = [formatter stringFromDate: event.eventDate];
    
    return cell;
}

// this page completes with ether didselectrowatindexpath or doneAction
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    EventTag *event = [self.eventTags objectAtIndex:indexPath.row];
    [self.delegate setWithViewController:self withEvent:event];
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)doneAction2:(id)sender
{
    // need validations for the text input!!
    
    if ([self.eventSearchBar.text length] > 3) {
        EventTag *event = [[EventTag alloc] initWithEvent:0
                                             andEventName:self.eventSearchBar.text
                                             andEventDate:nil];
        [self.delegate setWithViewController:self withEvent:event];
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Event Name Error";
        alert.message = @"Please enter an event name greater than 3 characters in length.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
    }
}


@end
