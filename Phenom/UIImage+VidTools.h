//
//  UIImage+VidTools.h
//  Phenom
//
//  Created by James Chung on 5/29/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (VidTools)

/*
+ (UIImage *)imageFromMovie:(NSURL *)movieURL atTime:(NSTimeInterval)time;
 */

+(UIImage*) drawImage:(UIImage*) fgImage
              inImage:(UIImage*) bgImage
              atPoint:(CGPoint)  point;

+ (UIImage *)imageFromMovie2:(NSURL *)contentURL;
@end
