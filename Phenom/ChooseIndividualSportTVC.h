//
//  ChooseIndividualSportTVC.h
//  Phenom
//
//  Created by James Chung on 7/11/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Sport.h"
#import "CustomBaseTVC.h"

@class ChooseIndividualSportTVC;

@protocol ChooseIndividualSportTVC_Delegate <NSObject>
- (void)setWithSportFromVC:(ChooseIndividualSportTVC *)controller withSport:(Sport *)sport;

@end

@interface ChooseIndividualSportTVC : CustomBaseTVC

@property (nonatomic, weak) id <ChooseIndividualSportTVC_Delegate> delegate;
@property (nonatomic) NSInteger competitorUserID; // not the session user id

@end
