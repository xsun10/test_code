//
//  PDVenueKingCell.h
//  Vaiden
//
//  Created by James Chung on 3/7/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDVenueKingCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *profilePic;
@property (weak, nonatomic) IBOutlet UILabel *venueNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *updatePostLabel;
@property (weak, nonatomic) IBOutlet UILabel *sharedByLabel;
@property (weak, nonatomic) IBOutlet UILabel *postDateTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *numCommentLabel;
@property (weak, nonatomic) IBOutlet UILabel *commentTextLabel;
@property (weak, nonatomic) IBOutlet UIButton *commentButton;

@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *venuePic;
@property (weak, nonatomic) IBOutlet UIImageView *commentButtonIcon;

@property (weak, nonatomic) IBOutlet UIView *mainCellBackgroundView;
@property (weak, nonatomic) IBOutlet UIButton *venueDetailButton;
@property (weak, nonatomic) IBOutlet UIButton *actionButtonHandle;
@property (weak, nonatomic) IBOutlet UIImageView *likeIcon;
@property (weak, nonatomic) IBOutlet UILabel *likeNumberLabel;
@property (weak, nonatomic) IBOutlet UIButton *likeButton;
@property (weak, nonatomic) IBOutlet UIImageView *newsfeedIcon;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UIView *smallCircleView;
@property (weak, nonatomic) IBOutlet UIButton *viewLikesButton;
@property (weak, nonatomic) IBOutlet UIButton *viewCommentsButton;
@property (weak, nonatomic) IBOutlet UIButton *venueButton;

@end
