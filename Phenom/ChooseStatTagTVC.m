//
//  ChooseStatTagTVC.m
//  Vaiden
//
//  Created by James Chung on 5/27/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "ChooseStatTagTVC.h"
#import "IOSRequest.h"
#import "StatTag.h"
#import "ChooseStatTagCell.h"
#import "UIView+Manipulate.h"
#import "CombinTagTVCell.h"
#import "TagDetailTVC.h"

@interface ChooseStatTagTVC () <TagDetailDelegate>
@property (nonatomic, strong) NSMutableArray *tagsArray;
@property (nonatomic, strong) NSMutableArray *subCategoriesArray;
@property (nonatomic, strong) NSMutableArray *displayList;
@end

@implementation ChooseStatTagTVC

- (NSMutableArray *)tagsArray
{
    if (!_tagsArray) _tagsArray = [[NSMutableArray alloc] init];
    return _tagsArray;
}

- (NSMutableArray *)displayList
{
    if (!_displayList) _displayList = [[NSMutableArray alloc] init];
    return _displayList;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title = [self.sportObj.sportName uppercaseString];
//    [self setDoneButton];
    [self loadStatTags];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.navigationItem.title = [self.sportObj.sportName uppercaseString];
}

- (void)loadStatTags
{
    [self.tagsArray removeAllObjects];
    
    [IOSRequest getStatTagsForSport:self.sportObj.sportID onCompletion:^(NSMutableArray *results) {
        
        for (id object in results) {
            StatTag *tag = [[StatTag alloc] initWithStatTag:[object[@"st_id"] integerValue]
                                             andStatTagName:object[@"st_name"]
                                            andStatTagValue:0
                                                andStatLink:object[@"st_link"]
                                            andStatLinkType:object[@"st_link_type"]
                                             andSubCategory:object[@"sub_category"]
                                         andShowSubcategory:[object[@"show_subcategory"] boolValue]];
            [self.tagsArray addObject:tag];
            if (self.displayList.count == 0) {
                [self.displayList addObject:tag];
            } else {
                int flag = 0;
                for (int i=0; i<self.displayList.count; i++) {
                    StatTag *t = [self.displayList objectAtIndex:i];
                    if ([t.statLink isEqualToString:tag.statLink] && t.statLink.length >0 && ([tag.statLinkType isEqualToString:@"numerator"] || [tag.statLinkType isEqualToString:@"denominator"])) {
                        break;
                    }
                    flag ++;
                }
                if (flag == self.displayList.count) {
                    [self.displayList addObject:tag];
                }
            }
        }
        
        self.subCategoriesArray = [[NSMutableArray alloc] initWithArray:[self.tagsArray valueForKeyPath:@"@distinctUnionOfObjects.subCategory"]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self.tableView reloadData];
        });
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSString *subCategory = [self.subCategoriesArray objectAtIndex:section];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"subCategory = %@", subCategory];
    NSArray *filteredArray = [NSArray arrayWithArray:[self.displayList filteredArrayUsingPredicate:predicate]];
    
    return [filteredArray count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.subCategoriesArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *subCategory = [self.subCategoriesArray objectAtIndex:indexPath.section];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"subCategory = %@", subCategory];
    NSArray *filteredArray = [NSArray arrayWithArray:[self.displayList filteredArrayUsingPredicate:predicate]];
    
    StatTag *tag = [filteredArray objectAtIndex:indexPath.row];
    if (tag.statLink.length >0 && ([tag.statLinkType isEqualToString:@"numerator"] || [tag.statLinkType isEqualToString:@"denominator"])) {
        CombinTagTVCell *cell = [tableView dequeueReusableCellWithIdentifier:@"combin_tag_cell"];
        cell.statName.text = tag.statLink;
        
        cell.data.tag = tag.statTagID;
        
        NSPredicate *pre = [NSPredicate predicateWithFormat:@"statLink = %@", tag.statLink];
        NSArray *result = [NSArray arrayWithArray:[self.tagsArray filteredArrayUsingPredicate:pre]];
        StatTag * numerator = (StatTag *)[result objectAtIndex:0];
        StatTag * denominator = (StatTag *)[result objectAtIndex:1];
        NSString *str = @"";
        if (numerator.statTagValue > 0 && denominator.statTagValue > 0) {
            str = [NSString stringWithFormat:@"%@/%@", denominator.statTagValue, numerator.statTagValue];
        }
        cell.data.text = str;
        return cell;
    } else {
        ChooseStatTagCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Tag Cell" forIndexPath:indexPath];
        
        if (tag.showSubCategory)
            cell.statNameLabel.text = [NSString stringWithFormat:@"%@ %@", tag.statLink, tag.statTagName];
        else
            cell.statNameLabel.text = tag.statTagName;
        
        cell.statTextField.tag = tag.statTagID;
        cell.statTextField.text = tag.statTagValue;
        return cell;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [[self.subCategoriesArray objectAtIndex:section] uppercaseString];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}
- (IBAction)editingDidEndStatField:(UITextField *)sender
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"statTagID = %ld", sender.tag];
    NSArray *filteredArray = [NSArray arrayWithArray:[self.tagsArray filteredArrayUsingPredicate:predicate]];
    
    StatTag *tag = [filteredArray objectAtIndex:0]; // should only be one
    NSInteger myindex = [self.tagsArray indexOfObject:tag];

    tag.statTagValue = sender.text;
    
    if ([sender.text length] > 0)
        tag.isSet = YES;
    else
        tag.isSet = NO;

    
    [self.tagsArray replaceObjectAtIndex:myindex withObject:tag];
}

/*- (void)setDoneButton
{
    UIButton *btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 50, 21);
    [btn makeRoundedBorderWithRadius:3];
    btn.layer.borderColor = [UIColor whiteColor].CGColor;
    btn.layer.borderWidth = 1.0;
    
    [btn addTarget:self action:@selector(doneAction:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundColor:[UIColor clearColor]];
    [btn setTitle:@"DONE" forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-MediumItalic" size:12.0]];
    
    
    // Initialize UIBarbuttonitem...
    UIBarButtonItem *proe = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = proe;
}*/

- (IBAction)doneAction:(id)sender
{
  /*  for (int n = 0; n < [self.subCategoriesArray count]; n++) {
        for (int i = 0; i < [self.tagsArray count]; i++) {
            ChooseStatTagCell *cell = (ChooseStatTagCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:n]];
            [cell.statTextField resignFirstResponder];
        }
    }*/
    [self.view endEditing:YES];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isSet = YES"];
    NSMutableArray *filteredArray = [NSMutableArray arrayWithArray:[self.displayList filteredArrayUsingPredicate:predicate]];
    NSMutableArray *resultArray = [NSMutableArray arrayWithArray:[self.tagsArray filteredArrayUsingPredicate:predicate]];
    NSLog(@"%d",filteredArray.count);
    if ([filteredArray count] <= 3) {
        [self.delegate setWithViewController:self withStatTags:resultArray withTotal:filteredArray.count];
        NSLog(@"%@",resultArray);
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Too Many Stats";
        alert.message = @"Please only enter up to 3 stats.  The rest of the fields should be blank.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
    }

}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"combine_segue"]) {
        TagDetailTVC *controller = (TagDetailTVC *)segue.destinationViewController;
        controller.tagsArray = self.tagsArray;
        CombinTagTVCell * cell = (CombinTagTVCell *)sender;
        controller.statLink = cell.statName.text;
        controller.delegate = self;
    }
}

#pragma mark - stats detail delegate
- (void)GetGroupedStats:(NSMutableArray *)tagsArray fromView:(UIViewController *)controller
{
    self.tagsArray = tagsArray;
    [self.tableView reloadData];
}

@end
