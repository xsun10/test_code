//
//  VDPhotosCell.h
//  Vaiden
//
//  Created by James Chung on 3/3/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@class VDPhotosCell;

@protocol VDPhotosCell_Delegate <NSObject>
- (void)setWithPicToShowEnlarged:(VDPhotosCell *)controller withPicNum:(NSInteger)picNum Center:(CGPoint)center Offset:(CGFloat)offset;

@end


@interface VDPhotosCell : UITableViewCell <UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic, strong) NSMutableArray *picsArray;
@property (weak, nonatomic) IBOutlet UICollectionView *picsCollectionView;
@property (nonatomic, weak) id <VDPhotosCell_Delegate> delegate;
@property (weak, nonatomic) IBOutlet UIImageView *cameraIcon;
@property (weak, nonatomic) IBOutlet UIView *myBackgroundView;

- (void)initializePicsCollectionView;
@end
