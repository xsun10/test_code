//
//  OtherMatchNotificationCell.h
//  Phenom
//
//  Created by James Chung on 6/17/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UpdatesMatchNotificationCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *cellIcon;
@property (nonatomic, weak) IBOutlet UILabel *headlineMessage;
@property (nonatomic, weak) IBOutlet UILabel *description;
@property (weak, nonatomic) IBOutlet UILabel *description2;
@property (weak, nonatomic) IBOutlet UIImageView *timerIcon;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@end
