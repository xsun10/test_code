//
//  EventTag.m
//  Vaiden
//
//  Created by James Chung on 5/28/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "EventTag.h"

@implementation EventTag

- (EventTag *)initWithEvent:(NSInteger)eventID
               andEventName:(NSString *)eventName
               andEventDate:(NSDate *)eventDate
{
    self = [super init];
    
    if (self) {
        _eventID = eventID;
        _eventName = eventName;
        _eventDate = eventDate;
        _eventVenue = nil;
        _webAddress = @"";
        _phoneNumber = @"";
        _emailAddress = @"";
    }
    return self;
}

- (EventTag *)initWithEvent:(NSInteger)eventID
               andEventName:(NSString *)eventName
               andEventDate:(NSDate *)eventDate
                   andVenue:(Venue *)eventVenue
              andWebAddress:(NSString *)webAddress
             andPhoneNumber:(NSString *)phoneNumber
            andEmailAddress:(NSString *)emailAddress
{
    self = [self initWithEvent:eventID
                  andEventName:eventName
                  andEventDate:eventDate];
    
    if (self) {
        _eventVenue = eventVenue;
        _webAddress = webAddress;
        _phoneNumber = phoneNumber;
        _emailAddress = emailAddress;
    }
    return self;
}
@end
