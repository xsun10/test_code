//
//  WhoCanChallengeUserTVC.h
//  Vaiden
//
//  Created by James Chung on 12/1/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "CustomBaseTVC.h"

@interface WhoCanChallengeUserTVC : CustomBaseTVC <MBProgressHUDDelegate>
{
    MBProgressHUD *HUD;
}

@end
