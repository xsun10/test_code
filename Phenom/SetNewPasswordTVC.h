//
//  SetNewPasswordTVC.h
//  Vaiden
//
//  Created by James Chung on 4/3/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface SetNewPasswordTVC : UITableViewController <MBProgressHUDDelegate>
{
    MBProgressHUD *HUD;
}

@end
