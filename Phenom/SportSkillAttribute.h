//
//  SportSkillType.h
//  Vaiden
//
//  Created by James Chung on 10/7/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SportSkillAttribute : NSObject

@property (nonatomic) NSUInteger skillTypeID;
@property (nonatomic, strong) NSString *skillTypeDescription;

-(SportSkillAttribute *)initWillSportSkillType:(NSUInteger)sportSkillID
                 andSportSkillDescription:(NSString *)sportSkillDescription;

@end
