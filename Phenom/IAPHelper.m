//
//  IAPHelper.m
//  Vaiden
//
//  Containing all the basic function for in-app purchase
//
//  Created by Turbo on 8/4/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "IAPHelper.h"

NSString *const IAPHelperProductPurchasedNotification = @"IAPHelperProductPurchasedNotification";

@interface IAPHelper () <SKProductsRequestDelegate, SKPaymentTransactionObserver>
@end

@implementation IAPHelper

SKProductsRequest * _productsRequest;

RequestProductsCompletionHandler _completionHandler;
NSSet * _productIdentifiers;
NSMutableSet * _purchasedProductIdentifiers;

- (id)initWithProductIdentifiers:(NSSet *)productIdentifiers {
    if ((self = [super init])) {
        
        // Store product identifiers
        _productIdentifiers = productIdentifiers;
        
        // Vote can buy many times, so it's not necessary to check the item purchase status.
    }
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    return self;
}

// Ger product information from iTunes Connect
- (void)requestProductsWithCompletionHandler:(RequestProductsCompletionHandler)completionHandler {
    
    _completionHandler = [completionHandler copy];
    
    _productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:_productIdentifiers];
    _productsRequest.delegate = self;
    [_productsRequest start];
    
}

- (void)buyProduct:(SKProduct *)product {
    
    NSLog(@"Buying %@...", product.productIdentifier);
    
    SKPayment * payment = [SKPayment paymentWithProduct:product];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
    
}

#pragma mark - SKProductsRequestDelegate
- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response {
    
    NSLog(@"Loaded list of products...");
    _productsRequest = nil;
    
    NSLog(@"%d",[response.products count]);
    NSArray * skProducts = response.products;
    for (SKProduct * skProduct in skProducts) {
        NSLog(@"Found product: %@ %@ %0.2f",
              skProduct.productIdentifier,
              skProduct.localizedTitle,
              skProduct.price.floatValue);
    }
    
    _completionHandler(YES, skProducts);
    _completionHandler = nil;
    
}

- (void)request:(SKRequest *)request didFailWithError:(NSError *)error {
    
    NSLog(@"Failed to load list of products.");
    _productsRequest = nil;
    
    _completionHandler(NO, nil);
    _completionHandler = nil;
    
}

#pragma mark - SKPaymentTransactionObserver
- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
    for (SKPaymentTransaction * transaction in transactions) {
        switch (transaction.transactionState)
        {
            case SKPaymentTransactionStatePurchased: // success
                [self completeTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed: // failed
                [self failedTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored: // restore
                [self restoreTransaction:transaction];
            default:
                break;
        }
    };
}

- (void)completeTransaction:(SKPaymentTransaction *)transaction {
    NSLog(@"completeTransaction...");
    // Save the unupload data into the NSUserDefaults
    [self provideContentForProductIdentifier:transaction.payment.productIdentifier];
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
}

- (void)restoreTransaction:(SKPaymentTransaction *)transaction {
    NSLog(@"restoreTransaction...");
    // Save the unupload data into the NSUserDefaults
    [self provideContentForProductIdentifier:transaction.payment.productIdentifier];
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
}

- (void)failedTransaction:(SKPaymentTransaction *)transaction {
    
    NSLog(@"failedTransaction...");
    if (transaction.error.code != SKErrorPaymentCancelled)
    {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Purchase Error";
        alert.message = [NSString stringWithFormat:@"Transaction error: %@", transaction.error.localizedDescription];
        [alert addButtonWithTitle:@"OK"];
        [alert show];
    }
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
}

- (void)provideContentForProductIdentifier:(NSString *)productIdentifier {
    NSMutableDictionary * userInfo = [[NSMutableDictionary alloc] init];
    if ([productIdentifier isEqualToString:@"com.vaiden.vaidenapp.purchase"]) { // 10 votes
        int currentValue = [[NSUserDefaults standardUserDefaults] integerForKey:@"unupload_purchased_votes"];
        currentValue += 10;
        [[NSUserDefaults standardUserDefaults] setInteger:currentValue forKey:@"unupload_purchased_votes"];
        [[NSUserDefaults standardUserDefaults] setFloat:0.99 forKey:@"unupload_purchased_votes_price"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [userInfo setObject:@"buy" forKey:@"type"];
    } else if ([productIdentifier isEqualToString:@"com.vaiden.vaidenapp.purchase2"]) { // 35 votes
        int currentValue = [[NSUserDefaults standardUserDefaults] integerForKey:@"unupload_purchased_votes"];
        currentValue += 35;
        [[NSUserDefaults standardUserDefaults] setInteger:currentValue forKey:@"unupload_purchased_votes"];
        [[NSUserDefaults standardUserDefaults] setFloat:2.99 forKey:@"unupload_purchased_votes_price"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [userInfo setObject:@"buy" forKey:@"type"];
    }  else if ([productIdentifier isEqualToString:@"com.vaiden.vaidenapp.purchase3"]) { // 55 votes
        int currentValue = [[NSUserDefaults standardUserDefaults] integerForKey:@"unupload_purchased_votes"];
        currentValue += 55;
        [[NSUserDefaults standardUserDefaults] setInteger:currentValue forKey:@"unupload_purchased_votes"];
        [[NSUserDefaults standardUserDefaults] setFloat:4.99 forKey:@"unupload_purchased_votes_price"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [userInfo setObject:@"buy" forKey:@"type"];
    } else if ([productIdentifier isEqualToString:@"com.vaiden.vaidenapp.boost"]) {
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"unupload_boost_votes"];
        [[NSUserDefaults standardUserDefaults] setFloat:0.99 forKey:@"unupload_purchased_votes_price"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [userInfo setObject:@"boost" forKey:@"type"];
    } else if ([productIdentifier isEqualToString:@"com.vaiden.vaidenapp.boost2"]) {
        [[NSUserDefaults standardUserDefaults] setInteger:3 forKey:@"unupload_boost_votes"];
        [[NSUserDefaults standardUserDefaults] setFloat:2.99 forKey:@"unupload_purchased_votes_price"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [userInfo setObject:@"boost" forKey:@"type"];
    } else if ([productIdentifier isEqualToString:@"com.vaiden.vaidenapp.boost3"]) {
        [[NSUserDefaults standardUserDefaults] setInteger:5 forKey:@"unupload_boost_votes"];
        [[NSUserDefaults standardUserDefaults] setFloat:4.99 forKey:@"unupload_purchased_votes_price"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [userInfo setObject:@"boost" forKey:@"type"];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:IAPHelperProductPurchasedNotification object:productIdentifier userInfo:userInfo];
}

@end
