//
//  ChooseSportUtilityTVC.m
//  Vaiden
//
//  Created by James Chung on 5/8/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

// INT THE PAST, I HAVE MADE MUTLIPLE VERSIONS OF CHOOSING SPORT TVC.  THIS ONE WILL ALLOW OPTIONS TO SELECT SPORTS UNDER
// DIFFERENT CRITERIA.  FOR EXAMPLE, BEING ABLE TO EITHER SELECT 1 SPORT OR MULTIPLE SPORTS...OR BEING ABLE TO SELECT
// ALL SPORTS, INDIVIDUAL SPORTS, PICKUP SPORTS, OR TEAM SPORTS.

#import "ChooseSportUtilityTVC.h"
#import "ChooseSportUtilityCell.h"
#import "IOSRequest.h"
#import "UIColor+VaidenColors.h"

@interface ChooseSportUtilityTVC ()

@property (nonatomic, strong) NSMutableArray *sportsArray;

@end

@implementation ChooseSportUtilityTVC

- (NSMutableArray *)sportsArray
{
    if (!_sportsArray) _sportsArray = [[NSMutableArray alloc] init];
    return _sportsArray;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    if (self.sportTypeInt == SPORT_TYPE_ALL) {
        [self loadAllSports];
    }

}

- (void)loadAllSports
{
    [IOSRequest fetchAllSports:0
                  onCompletion:^(NSMutableArray *results) {
                      
                      for (id object in results) {
                          if (self.hideFitnessSport && [object[@"sport_name"] isEqualToString:@"fitness"]) // we don't show fitness for stat tags
                              continue;
                          
                          Sport *sp = [[Sport alloc] initWithSportDetails:[object[@"sport_id"] integerValue]
                                                                 andName:object[@"sport_name"]
                                                                 andType:nil];
                          
                          
                          [self.sportsArray addObject:sp];
                      }
                      
                      dispatch_async(dispatch_get_main_queue(), ^{
                         
                          [self.tableView reloadData];
                      });
                  }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.sportsArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ChooseSportUtilityCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Sport Cell" forIndexPath:indexPath];

    Sport *sp = [self.sportsArray objectAtIndex:indexPath.row];
    
    if (sp.isSelected || sp.sportID == self.chosenSport.sportID)
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    else
        cell.accessoryType = UITableViewCellAccessoryNone;
    
    cell.sportNameLabel.text = [sp.sportName capitalizedString];
    cell.sportIcon.image = [[sp getSportIcon] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [cell.sportIcon setTintColor:[UIColor newBlueLight]];

    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Sport *sp = [self.sportsArray objectAtIndex:indexPath.row];
    self.chosenSport = sp;
    sp.isSelected = YES;
    NSMutableArray *selectedSports = [[NSMutableArray alloc] init];
    
    if (self.selectMultiple) {
        
    } else {
        [selectedSports addObject:sp];
        
        [self.delegate setWithViewController:self withSports:selectedSports isMultiple:NO];
        [self.navigationController popViewControllerAnimated:YES];
    }
}
@end
