//
//  ChooseTrickShotVC.h
//  Phenom
//
//  Created by James Chung on 6/4/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ChooseTrickShotVC;

@protocol ChooseTrickShotVC_Delegate <NSObject>

- (void)setWithTrickShotVideoVC:(ChooseTrickShotVC *)controller
                  withVideoData:(NSData *)vidData
                   andThumbnail:(UIImage *)vidThumbnail
                     andFileExt:(NSString *)fileExt;
@end

@interface ChooseTrickShotVC : UIViewController

@property (nonatomic, weak) id <ChooseTrickShotVC_Delegate> delegate;
@end





