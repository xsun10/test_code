//
//  IMDHeaderInfoCell.m
//  Vaiden
//
//  Created by James Chung on 12/26/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "IMDHeaderInfoCell.h"
#import "UIView+Manipulate.h"
#import "IMDHeaderInfoButtonCVCell.h"
#import "UIColor+VaidenColors.h"

@interface IMDHeaderInfoCell ()
@property (nonatomic) NSInteger buttonConfig;

@end
@implementation IMDHeaderInfoCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)matchButtonAction:(UIButton *)sender
{
    IMDHeaderInfoButtonCVCell *cell = (IMDHeaderInfoButtonCVCell *)[self.buttonCollectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:sender.tag inSection:0]];
    [self.delegate setWithButtonMessage:self withMessage:cell.actionName.text];
}


- (void)configureButtonDisplay:(NSInteger)matchButtonConfig
{
    self.buttonConfig = matchButtonConfig;
    self.buttonCollectionView.delegate = self;
    self.buttonCollectionView.dataSource = self;
    
    switch (matchButtonConfig) {
        case BUTTON_CONFIG_EMPTY_SCORE:
            [self.button1 setTitle:@"Messages" forState:UIControlStateNormal];
            [self.button2 setTitle:@"Share" forState:UIControlStateNormal];
            [self.button3 setTitle:@"Rankings" forState:UIControlStateNormal];
            
            [self.button1 makeCircleWithColorAndBackground:[UIColor whiteColor] andRadius:5];
            [self.button2 makeCircleWithColorAndBackground:[UIColor whiteColor] andRadius:5];
            [self.button3 makeCircleWithColorAndBackground:[UIColor whiteColor] andRadius:5];

            self.button4.hidden = YES;
            
            break;
            
        case BUTTON_CONFIG_SHOW_SCORE:
            [self.button1 setTitle:@"Messages" forState:UIControlStateNormal];
            [self.button2 setTitle:@"Share" forState:UIControlStateNormal];
            [self.button3 setTitle:@"Rankings" forState:UIControlStateNormal];
            
            [self.button1 makeCircleWithColorAndBackground:[UIColor whiteColor] andRadius:5];
            [self.button2 makeCircleWithColorAndBackground:[UIColor whiteColor] andRadius:5];
            [self.button3 makeCircleWithColorAndBackground:[UIColor whiteColor] andRadius:5];
            
            self.button4.hidden = YES;
            break;
            
        case BUTTON_CONFIG_CONFIRM_BUTTONS:
            [self.button1 setTitle:@"Confirm Score" forState:UIControlStateNormal];
            [self.button2 setTitle:@"Dispute Score" forState:UIControlStateNormal];
            [self.button3 setTitle:@"Messages" forState:UIControlStateNormal];
            [self.button4 setTitle:@"Share" forState:UIControlStateNormal];
            
            [self.button1 makeCircleWithColorAndBackground:[UIColor whiteColor] andRadius:5];
            [self.button2 makeCircleWithColorAndBackground:[UIColor whiteColor] andRadius:5];
            [self.button3 makeCircleWithColorAndBackground:[UIColor whiteColor] andRadius:5];
            [self.button4 makeCircleWithColorAndBackground:[UIColor whiteColor] andRadius:5];
            
            break;
            
        case BUTTON_CONFIG_RECORD_BUTTONS:
            [self.button1 setTitle:@"Record Score" forState:UIControlStateNormal];
            [self.button2 setTitle:@"Void Match" forState:UIControlStateNormal];
            [self.button3 setTitle:@"Messages" forState:UIControlStateNormal];
            [self.button4 setTitle:@"Share" forState:UIControlStateNormal];
            
            [self.button1 makeCircleWithColorAndBackground:[UIColor whiteColor] andRadius:5];
            [self.button2 makeCircleWithColorAndBackground:[UIColor whiteColor] andRadius:5];
            [self.button3 makeCircleWithColorAndBackground:[UIColor whiteColor] andRadius:5];
            [self.button4 makeCircleWithColorAndBackground:[UIColor whiteColor] andRadius:5];
            
            break;
            
        case BUTTON_CONFIG_EMPTY_MATCH_OUTCOME:
            [self.button1 setTitle:@"Messages" forState:UIControlStateNormal];
            [self.button2 setTitle:@"Share" forState:UIControlStateNormal];
            [self.button3 setTitle:@"Rankings" forState:UIControlStateNormal];
            
            [self.button1 makeCircleWithColorAndBackground:[UIColor whiteColor] andRadius:5];
            [self.button2 makeCircleWithColorAndBackground:[UIColor whiteColor] andRadius:5];
            [self.button3 makeCircleWithColorAndBackground:[UIColor whiteColor] andRadius:5];
            
            self.button4.hidden = YES;
            
            break;
            
        case BUTTON_CONFIG_CANCELLED_MATCH:
            
            [self.button1 setTitle:@"Messages" forState:UIControlStateNormal];
            [self.button2 setTitle:@"Share" forState:UIControlStateNormal];
            [self.button3 setTitle:@"Rankings" forState:UIControlStateNormal];
            
            [self.button1 makeCircleWithColorAndBackground:[UIColor whiteColor] andRadius:5];
            [self.button2 makeCircleWithColorAndBackground:[UIColor whiteColor] andRadius:5];
            [self.button3 makeCircleWithColorAndBackground:[UIColor whiteColor] andRadius:5];
            
            self.button4.hidden = YES;
            break;
            
        case BUTTON_CONFIG_MATCH_CREATOR:
            [self.button1 setTitle:@"Cancel Match" forState:UIControlStateNormal];
            [self.button2 setTitle:@"Messages" forState:UIControlStateNormal];
            [self.button3 setTitle:@"Share" forState:UIControlStateNormal];
            
            [self.button1 makeCircleWithColorAndBackground:[UIColor whiteColor] andRadius:5];
            [self.button2 makeCircleWithColorAndBackground:[UIColor whiteColor] andRadius:5];
            [self.button3 makeCircleWithColorAndBackground:[UIColor whiteColor] andRadius:5];

            self.button4.hidden = YES;
            break;
            
        case BUTTON_CONFIG_CONFIRMED_MATCH_PARTICIPANT:
            [self.button1 setTitle:@"Withdraw RSVP" forState:UIControlStateNormal];
            [self.button2 setTitle:@"Messages" forState:UIControlStateNormal];
            [self.button3 setTitle:@"Share" forState:UIControlStateNormal];
            
            [self.button1 makeCircleWithColorAndBackground:[UIColor whiteColor] andRadius:5];
            [self.button2 makeCircleWithColorAndBackground:[UIColor whiteColor] andRadius:5];
            [self.button3 makeCircleWithColorAndBackground:[UIColor whiteColor] andRadius:5];
            
            self.button4.hidden = YES;
            
            break;
            
        case BUTTON_CONFIG_UNCONFIRMED_MATCH_PARTICIPANT:
            [self.button1 setTitle:@"Join Match" forState:UIControlStateNormal];
            [self.button2 setTitle:@"Decline Match" forState:UIControlStateNormal];
            [self.button3 setTitle:@"Messages" forState:UIControlStateNormal];
            [self.button4 setTitle:@"Share" forState:UIControlStateNormal];
            
            [self.button1 makeCircleWithColorAndBackground:[UIColor whiteColor] andRadius:5];
            [self.button2 makeCircleWithColorAndBackground:[UIColor whiteColor] andRadius:5];
            [self.button3 makeCircleWithColorAndBackground:[UIColor whiteColor] andRadius:5];
            [self.button4 makeCircleWithColorAndBackground:[UIColor whiteColor] andRadius:5];
            
            break;
            
        case BUTTON_CONFIG_MINIMAL_DISPLAY:
            self.button1.hidden = YES;
            self.button2.hidden = YES;
            self.button3.hidden = YES;
            self.button4.hidden = YES;
        default:
            break;
        
    }
    
}

// configureButtonDisplay must be called first

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    switch (self.buttonConfig) {
        case BUTTON_CONFIG_EMPTY_SCORE:
            return 3;
            
            break;
            
        case BUTTON_CONFIG_SHOW_SCORE:
            return 3;
            break;
            
        case BUTTON_CONFIG_CONFIRM_BUTTONS:
            return 4;
            
            break;
            
        case BUTTON_CONFIG_RECORD_BUTTONS:
            return 4;
            
            break;
            
        case BUTTON_CONFIG_EMPTY_MATCH_OUTCOME:
            return 3;
            
            break;
            
        case BUTTON_CONFIG_CANCELLED_MATCH:
            
            return 3;
            break;
            
        case BUTTON_CONFIG_MATCH_CREATOR:
            return 3;
            break;
            
        case BUTTON_CONFIG_CONFIRMED_MATCH_PARTICIPANT:
            return 3;
            
            break;
            
        case BUTTON_CONFIG_UNCONFIRMED_MATCH_PARTICIPANT:
            return 4;
            
            break;
            
        case BUTTON_CONFIG_MINIMAL_DISPLAY:
            return 0;
            break;
        default:
            break;
    }
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"My Cell";
    
    IMDHeaderInfoButtonCVCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.matchActionButton.tag = indexPath.row;
    
    switch (self.buttonConfig) {
        case BUTTON_CONFIG_EMPTY_SCORE:
            if (indexPath.row == 0) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Message Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor darkGrayColor3]];
                
                cell.actionName.text = @"Messages";
                
            } else if (indexPath.row == 1) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Share Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor darkGrayColor3]];
                cell.actionName.text = @"Share";
                
            } else if (indexPath.row == 2) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Leaderboard Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor darkGrayColor3]];
                cell.actionName.text = @"Rankings";
            } else if (indexPath.row == 3) {
                
            }
            
            break;
            
        case BUTTON_CONFIG_SHOW_SCORE:
            if (indexPath.row == 0) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Message Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor darkGrayColor3]];
                cell.actionName.text = @"Messages";
            } else if (indexPath.row == 1) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Share Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor darkGrayColor3]];
                cell.actionName.text = @"Share";
            } else if (indexPath.row == 2) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Leaderboard Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor darkGrayColor3]];
                cell.actionName.text = @"Rankings";
            } else if (indexPath.row == 3) {
                
            }
            
            break;
            
        case BUTTON_CONFIG_CONFIRM_BUTTONS:
            if (indexPath.row == 0) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Confirm Score Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor darkGrayColor3]];
                cell.actionName.text = @"Confirm Score";
                
            } else if (indexPath.row == 1) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Dispute Score Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor darkGrayColor3]];
                cell.actionName.text = @"Dispute Score";
            } else if (indexPath.row == 2) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Message Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor darkGrayColor3]];
                cell.actionName.text = @"Messages";
            } else if (indexPath.row == 3) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Share Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor darkGrayColor3]];
                cell.actionName.text = @"Share";
            }
            break;
            
        case BUTTON_CONFIG_RECORD_BUTTONS:
            if (indexPath.row == 0) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Record Score Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor darkGrayColor3]];
                cell.actionName.text = @"Record Score";
            } else if (indexPath.row == 1) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Void Match Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor darkGrayColor3]];
                cell.actionName.text = @"Void Match";
            } else if (indexPath.row == 2) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Message Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor darkGrayColor3]];
                cell.actionName.text = @"Messages";
            } else if (indexPath.row == 3) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Share Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor darkGrayColor3]];
                cell.actionName.text = @"Share";
            }
            
            break;
            
        case BUTTON_CONFIG_EMPTY_MATCH_OUTCOME:
            if (indexPath.row == 0) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Message Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor darkGrayColor3]];
                cell.actionName.text = @"Messages";
            } else if (indexPath.row == 1) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Share Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor darkGrayColor3]];
                cell.actionName.text = @"Share";
            } else if (indexPath.row == 2) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Leaderboard Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor darkGrayColor3]];
                cell.actionName.text = @"Rankings";
            } else if (indexPath.row == 3) {
                
            }
            
            break;
            
        case BUTTON_CONFIG_CANCELLED_MATCH:
            
            if (indexPath.row == 0) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Message Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor darkGrayColor3]];
                cell.actionName.text = @"Messages";
            } else if (indexPath.row == 1) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Share Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor darkGrayColor3]];
                cell.actionName.text = @"Share";
            } else if (indexPath.row == 2) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Leaderboard Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor darkGrayColor3]];
                cell.actionName.text = @"Rankings";
            } else if (indexPath.row == 3) {
                
            }
            
            break;
            
        case BUTTON_CONFIG_MATCH_CREATOR:
            if (indexPath.row == 0) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Cancel Match Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor darkGrayColor3]];
                cell.actionName.text = @"Cancel Match";
            } else if (indexPath.row == 1) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Message Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor darkGrayColor3]];
                cell.actionName.text = @"Messages";
            } else if (indexPath.row == 2) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Share Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor darkGrayColor3]];
                cell.actionName.text = @"Share";
            } else if (indexPath.row == 3) {
                
            }
            
            break;
            
        case BUTTON_CONFIG_CONFIRMED_MATCH_PARTICIPANT:
          
            if (indexPath.row == 0) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Decline Match Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor darkGrayColor3]];
                cell.actionName.text = @"Withdraw RSVP";
            } else if (indexPath.row == 1) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Message Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor darkGrayColor3]];
                cell.actionName.text = @"Messages";
            } else if (indexPath.row == 2) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Share Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor darkGrayColor3]];
                cell.actionName.text = @"Share";
            } else if (indexPath.row == 3) {
                
            }
            break;
            
        case BUTTON_CONFIG_UNCONFIRMED_MATCH_PARTICIPANT:
            
            if (indexPath.row == 0) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Join Match Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor darkGrayColor3]];
                cell.actionName.text = @"Join Match";
            } else if (indexPath.row == 1) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Decline Match Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor darkGrayColor3]];
                cell.actionName.text = @"Decline Match";
            } else if (indexPath.row == 2) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Message Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor darkGrayColor3]];
                cell.actionName.text = @"Messages";
            } else if (indexPath.row == 3) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Share Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor darkGrayColor3]];
                cell.actionName.text = @"Share";
            }
            
            break;
            
        case BUTTON_CONFIG_MINIMAL_DISPLAY:
            cell.iconPic = nil;
            cell.actionName.text = @"";
            
            break;
        default:
            break;
    }
    return cell;
}

#define INSET_3_BUTTONS 30
#define INSET_4_BUTTONS 0
#define INSET_0_BUTTONS 0

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    
    
    switch (self.buttonConfig) {
        case BUTTON_CONFIG_EMPTY_SCORE:
            return UIEdgeInsetsMake(0, INSET_3_BUTTONS, 0, 0);
            
            break;
            
        case BUTTON_CONFIG_SHOW_SCORE:
            return UIEdgeInsetsMake(0, INSET_3_BUTTONS, 0, 0);
            break;
            
        case BUTTON_CONFIG_CONFIRM_BUTTONS:
            return UIEdgeInsetsMake(0, INSET_4_BUTTONS, 0, 0);
            
            break;
            
        case BUTTON_CONFIG_RECORD_BUTTONS:
            return UIEdgeInsetsMake(0, INSET_4_BUTTONS, 0, 0);
            
            break;
            
        case BUTTON_CONFIG_EMPTY_MATCH_OUTCOME:
            return UIEdgeInsetsMake(0, INSET_3_BUTTONS, 0, 0);
            
            break;
            
        case BUTTON_CONFIG_CANCELLED_MATCH:
            
            return UIEdgeInsetsMake(0, INSET_3_BUTTONS, 0, 0);
            break;
            
        case BUTTON_CONFIG_MATCH_CREATOR:
            return UIEdgeInsetsMake(0, INSET_3_BUTTONS, 0, 0);
            break;
            
        case BUTTON_CONFIG_CONFIRMED_MATCH_PARTICIPANT:
            return UIEdgeInsetsMake(0, INSET_3_BUTTONS, 0, 0);
            
            break;
            
        case BUTTON_CONFIG_UNCONFIRMED_MATCH_PARTICIPANT:
            return UIEdgeInsetsMake(0, INSET_4_BUTTONS, 0, 0);
            
            break;
            
        case BUTTON_CONFIG_MINIMAL_DISPLAY:
            return UIEdgeInsetsMake(0, INSET_0_BUTTONS, 0, 0);
            break;
        default:
            break;
    }
    return UIEdgeInsetsMake(0, INSET_0_BUTTONS, 0, 0);
    
}

@end
