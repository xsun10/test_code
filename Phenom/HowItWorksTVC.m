//
//  HowItWorksTVC.m
//  Vaiden
//
//  Created by Turbo on 8/4/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "HowItWorksTVC.h"
#import "HowItWorksCell.h"
@interface HowItWorksTVC ()


@end

@implementation HowItWorksTVC


- (void)viewDidLoad
{
    [super viewDidLoad];
   
    
}
-(void)viewWillAppear:(BOOL)animated{
self.navigationController.navigationBar.topItem.title=@"How It Works";

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HowItWorksCell *cell = (HowItWorksCell *)[tableView dequeueReusableCellWithIdentifier:@"HowItWorksCell"];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    if (indexPath.row == 0) {
        cell.left_icon.image = [UIImage imageNamed:@"attention_gold"];
        [cell.left_icon setContentMode:UIViewContentModeCenter];
        cell.right_icon.image = [UIImage imageNamed:@"right_arrow"];
        [cell.right_icon setContentMode:UIViewContentModeCenter];
        cell.label.text = @"STEPS";
    } else if (indexPath.row == 1) {
        cell.left_icon.image = [UIImage imageNamed:@"hammer_gold"];
        [cell.left_icon setContentMode:UIViewContentModeCenter];
        cell.right_icon.image = [UIImage imageNamed:@"right_arrow"];
        [cell.right_icon setContentMode:UIViewContentModeCenter];
        cell.label.text = @"TIPS AND STRATEGIES";
    } else if (indexPath.row == 2){
        cell.left_icon.image = [UIImage imageNamed:@"info_gold"];
        [cell.left_icon setContentMode:UIViewContentModeCenter];
        cell.right_icon.image = [UIImage imageNamed:@"right_arrow"];
        [cell.right_icon setContentMode:UIViewContentModeCenter];
        cell.label.text = @"F.A.Q.";
    }     return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        [self performSegueWithIdentifier:@"HowItWorks_Segue" sender:self];
    } else if (indexPath.row == 1) {
        [self performSegueWithIdentifier:@"Tips_Segue" sender:self];
    } else  {
       // [self performSegueWithIdentifier:@"discover_segue" sender:self];
    }
}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
