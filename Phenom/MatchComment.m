//
//  MatchComment.m
//  Vaiden
//
//  Created by James Chung on 9/27/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "MatchComment.h"

@implementation MatchComment

- (MatchComment *)initWithCommentDetails:(NSInteger)commentID
                         andCommentText:(NSString *)commentText
                               byUserID:(NSInteger)userID
                            andUsername:(NSString *)username
                          andProfileStr:(NSString *)profileBaseString
                                forMatch:(NSInteger)matchID
                           matchCategory:(NSString *)matchCategory
                                 onDate:(NSDate *)postDate
{
    self = [super initWithCommentDetails:commentID
                          andCommentText:commentText
                                byUserID:userID
                             andUsername:username
                           andProfileStr:profileBaseString
                                  onDate:postDate];
    
    if (self) {
        _matchID = matchID;
        _matchCategory = matchCategory;
    }
    
    return self;
}

@end
