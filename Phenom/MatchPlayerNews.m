//
//  MatchPlayerNews.m
//  Vaiden
//
//  Created by James Chung on 10/18/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "MatchPlayerNews.h"

@implementation MatchPlayerNews


- (MatchPlayerNews *)initWithDetails:(Match *)matchInfo
                      postDate:(NSDate *)newsDate
               headLineMessage:(NSString *)headlineMessage
                generalMessage:(NSString *)generalMessage
                           forPlayer:(Player *)player
                         withMessage:(NSString *)playerMessage

{
    self = [super initWithDetails:matchInfo
                         postDate:newsDate
                  headLineMessage:headlineMessage
                   generalMessage:generalMessage];
    
    if (self) {
        _player = player;
        _playerMessage = playerMessage;
    }
    return self;
}

@end
