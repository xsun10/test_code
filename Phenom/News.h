//
//  News.h
//  Phenom
//
//  Created by James Chung on 4/23/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Player.h"

@interface News : NSObject

@property (strong, nonatomic) NSDate *newsDate;
@property (nonatomic) NSInteger newsMakerUserID;
@property (nonatomic, strong) NSString *newsMakerUserName;
@property (nonatomic, strong) NSString *profileBaseString;
@property (nonatomic, strong) NSString *textPost;
@property (nonatomic) NSUInteger numComments;
@property (nonatomic) NSUInteger numRemixes;
@property (nonatomic) BOOL didSessionUserRemixPost;
@property (nonatomic, strong) Player *sharedByPlayer;

@property (nonatomic) BOOL doesSessionUserLikePost;
@property (nonatomic) NSUInteger numLikes;

@property (nonatomic) NSInteger postType;
@property (nonatomic) NSInteger newsID;

- (News *)initWithNewsDetails:(NSInteger)newsMakerUserID
                  andUserName:(NSString *)newsMakerUserName
                andProfileStr:(NSString *)profileBaseString
                     withDate:(NSDate *)newsDate
                  andPostType:(NSInteger)postType
               andNumComments:(NSUInteger)numComments
                andNumRemixes:(NSUInteger)numRemixes
                    andNewsID:(NSInteger)newsID;

// initializer with text post
- (News *)initWithNewsDetails:(NSInteger)newsMakerUserID
                  andUserName:(NSString *)newsMakerUserName
                andProfileStr:(NSString *)profileBaseString
                     withDate:(NSDate *)newsDate
                  andPostType:(NSInteger)postType
               andNumComments:(NSUInteger)numComments
                andNumRemixes:(NSUInteger)numRemixes
                    andNewsID:(NSInteger)newsID
                  andTextPost:(NSString *)textPost;

// impletmented as a temporary hack
- (News *)initWithMinDetails:(NSDate *)newsDate;

- (NSString *)canPostBeSharedBySessionUser:(NSInteger)sessionUserID;
- (UIImage *)getNewsIcon;

@end
