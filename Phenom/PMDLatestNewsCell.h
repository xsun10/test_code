//
//  PMDLatestNewsCell.h
//  Vaiden
//
//  Created by James Chung on 1/17/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PMDLatestNewsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *newsIconBackground;
@property (weak, nonatomic) IBOutlet UIImageView *newsIconPic;

@property (weak, nonatomic) IBOutlet UIImageView *picImage;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *subheadlineLabel;
@property (weak, nonatomic) IBOutlet UILabel *datetimeLabel;
@property (weak, nonatomic) IBOutlet UIButton *detailsButton;

@end
