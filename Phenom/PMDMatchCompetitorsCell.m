//
//  PMDMatchCompetitorsCell.m
//  Vaiden
//
//  Created by James Chung on 1/17/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "PMDMatchCompetitorsCell.h"
#import "PickupMatchPlayersCVCell.h"
#import "MatchPlayer.h"
#import "S3Tools.h"
#import "IOSRequest.h"
#import "UIImage+ProportionalFill.h"
#import "AFNetworking.h"

@implementation PMDMatchCompetitorsCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)initPlayers
{
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.pageControl.currentPage = 0;
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"status = %@", @"ACCEPTED"];
    NSArray *matchParticipants = [self.pickupMatch.competitorArray filteredArrayUsingPredicate:predicate];
    
    self.statsLabel.text = [NSString stringWithFormat:@"%ld Joined %ld More Needed", [matchParticipants count], self.pickupMatch.minCompetitors - [matchParticipants count]];
    self.pageControl.numberOfPages = [matchParticipants count];


}


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"status = %@", @"ACCEPTED"];
    NSArray *matchParticipants = [self.pickupMatch.competitorArray filteredArrayUsingPredicate:predicate];
    
    return matchParticipants.count;
    
}

- (void)goToProfile:(UIButton *)sender
{
     [self.delegate setPMDMatchCompetitorsCellViewController:self withUserIDToSegue:sender.tag];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Player Pic Cell";
    
    PickupMatchPlayersCVCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"status = %@", @"ACCEPTED"];
    NSArray *matchParticipants = [self.pickupMatch.competitorArray filteredArrayUsingPredicate:predicate];
    
    if (indexPath.item < [matchParticipants count]) {
        MatchPlayer *player = matchParticipants[indexPath.item];
        
        
        NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                baseString:player.profileBaseString];
        
        [cell.profilePic setImageWithURL:[NSURL URLWithString:url]
                        placeholderImage:[UIImage imageNamed:@"avatar round"]];
        [UIImage makeRoundedImage:cell.profilePic withRadius:30];
        [cell.profilePic.layer setBorderWidth:2.0f];
        [cell.profilePic.layer setBorderColor:[UIColor whiteColor].CGColor];
        
        UIButton *playerPicButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];
        [playerPicButton addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
        playerPicButton.tag = player.userID;
        [cell.profilePic addSubview:playerPicButton];
        cell.profilePic.userInteractionEnabled = YES;
        
        cell.usernameLabel.text = player.userName;
        cell.usernameLabel.textColor = [UIColor darkGrayColor];
        
        PlayerSport *sport = [PlayerSport getSportObjectForName:self.pickupMatch.sport.sportName inArray:player.playerSports];
        cell.levelLabel.text = [NSString stringWithFormat:@"Level %ld", [sport.level integerValue]];
        cell.levelLabel.textColor = [UIColor darkGrayColor];
        if (player.userID == self.pickupMatch.matchCreatorID)
            cell.matchCreatorLabel.hidden = NO;
        else
            cell.matchCreatorLabel.hidden = YES;
        
    } else {
        [cell.profilePic setImage:[UIImage imageNamed:@"avatar round slot"]];
        cell.usernameLabel.text = @"";
        cell.levelLabel.text = @"Slot Open";
        cell.levelLabel.textColor = [UIColor darkGrayColor];
    }
    return cell;
}



@end
