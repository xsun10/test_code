//
//  CheckInHistoryTVC.h
//  Vaiden
//
//  Created by James Chung on 4/24/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "CustomBaseTVC.h"

@interface CheckInHistoryTVC : CustomBaseTVC

@property (nonatomic) NSInteger venueID;
@property (nonatomic) BOOL showFollowingInCheckinHistory;

@end
