//
//  AddVenueSportTVC.m
//  Phenom
//
//  Created by James Chung on 7/8/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "AddVenueSportsTVC.h"
#import "IOSRequest.h"
#import "UserPreferences.h"
#import "AddVenueSportsCell.h"
#import "UIColor+VaidenColors.h"

@interface AddVenueSportsTVC ()

@property (nonatomic, strong) NSMutableArray *sportsArray;
@property (nonatomic, strong) UserPreferences *preferences;
@property (nonatomic, strong) UIView *tempView;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
@property (nonatomic, strong) UIBarButtonItem *doneButton;

@end

@implementation AddVenueSportsTVC

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (NSMutableArray *)sportsArray
{
    if (!_sportsArray) _sportsArray = [[NSMutableArray alloc] init];
    return _sportsArray;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    self.tempView.backgroundColor = [UIColor midGray];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] init];
    [self.activityIndicator setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width/2.0, [UIScreen mainScreen].bounds.size.height/2.0 - 50)];
    [self.activityIndicator startAnimating];
    [self.tempView addSubview:self.activityIndicator];
    
    [self.view addSubview:self.tempView];

    //[self setDoneButton];

    [self setSports];
}


/*- (void)setDoneButton
{
    UIButton *btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 50, 21);
    btn.layer.borderColor = [UIColor whiteColor].CGColor;
    btn.layer.borderWidth = 1.0;
    
    [btn addTarget:self action:@selector(doneAction:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundColor:[UIColor clearColor]];
    [btn setTitle:@"DONE" forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-MediumItalic" size:12.0]];
    
    // Initialize UIBarbuttonitem...
    self.doneButton = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = self.doneButton;
}*/

- (void)setSports
{
    [self.sportsArray removeAllObjects];
    
    [IOSRequest fetchAllSports:[self.preferences getUserID] onCompletion:^(NSMutableArray *results) {
        for (id object in results) {
            Sport *sport = [[Sport alloc] initWithSportDetails:[object[@"sport_id"] integerValue]
                                                       andName:object[@"sport_name"]
                                                       andType:@"individual"]; // dummy data....
            
            [self.sportsArray addObject:sport];
            
            if ([self.sportsArray count] == [results count]) {
                dispatch_async(dispatch_get_main_queue(), ^ {
                    if ([self.sportsArray count] > 0) {
                        [self.activityIndicator stopAnimating];
                        self.tempView.hidden = YES;
                    }
                    
                    [self.tableView reloadData];
                });
            }
        }
    }];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.sportsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Sport Cell";
    AddVenueSportsCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    Sport *sport = [self.sportsArray objectAtIndex:indexPath.row];
    cell.sportName.text = [sport.sportName capitalizedString];
    cell.sportIcon.image = [[sport getSportIcon] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [cell.sportIcon setTintColor:[UIColor newBlueDark]];
    
    if (sport.isSelected)
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    else
        cell.accessoryType = UITableViewCellAccessoryNone;

    
    return cell;
}

- (IBAction)doneAction:(id)sender
{
    NSMutableArray *chosenArray = [[NSMutableArray alloc] init];

    for (Sport *sp in self.sportsArray) {
        if (sp.isSelected) {
            [chosenArray addObject:sp];
        }
    }
 /*   for (int i = 0; i < [self.sportsArray count]; i++) {
      //  UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
        
        if (cell.accessoryType == UITableViewCellAccessoryCheckmark)
            [chosenArray addObject:[self.sportsArray objectAtIndex:i]];
    }*/
    
    [self.delegate setViewController:self withSports:chosenArray];
    [self.navigationController popViewControllerAnimated:YES];

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    
    Sport *sp = [self.sportsArray objectAtIndex:indexPath.row];
    
    if (cell.accessoryType == UITableViewCellAccessoryNone) {
        sp.isSelected = YES;
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }  else {
        sp.isSelected = NO;
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60.0;
}

@end
