//
//  FeaturedUsersCell.h
//  Vaiden
//
//  Created by James Chung on 9/15/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StartByAddingFBContactsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *profilePic;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UILabel *numFollowingLabel;
@property (weak, nonatomic) IBOutlet UILabel *numFollowersLabel;
@property (weak, nonatomic) IBOutlet UIButton *followButton;
@property (weak, nonatomic) IBOutlet UILabel *fullnameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *cornerIcon;

@end
