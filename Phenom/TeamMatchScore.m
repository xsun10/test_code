//
//  TeamMatchScore.m
//  Vaiden
//
//  Created by James Chung on 4/16/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "TeamMatchScore.h"
#import "TeamMatchRoundScore.h"

@implementation TeamMatchScore


- (TeamMatchScore *) initWithMatchScore
{
    self = [super init];
    
    if (self) {
        _roundScoresTeamMatch = [[NSMutableArray alloc] init];
        
    }
    
    return self;
}

- (void)addRoundScore:(NSInteger)numRound
             forTeam1:(NSInteger)team1ID
    withScoreForTeam1:(NSInteger)scoreTeam1
             forTeam2:(NSInteger)team2ID
    withScoreForTeam2:(NSInteger)scoreTeam2
{
    TeamMatchRoundScore *roundScore = [[TeamMatchRoundScore alloc] initWithRoundScore:numRound
                                                                           scoreTeam1:scoreTeam1
                                                                          withTeam1ID:team1ID
                                                                           scoreTeam2:scoreTeam1
                                                                          withTeam2ID:team2ID];
    
    [self.roundScoresTeamMatch addObject:roundScore];
}

// Certain sports have multiple rounds (ex tennis has multiple "sets" each of which we consider a round.
// A player in tennis can enter scores for each set.
// Sports like basketball only have one round.
//
// This function will return the net score.  So, if the sport has 1 round, then the score for that round will
// be returned.  If the sport has multiple rounds (ex sets in Tennis), then the number of rounds won / lost by
// each user will be returned.

- (NSMutableDictionary *)getNetScore
{
    NSMutableDictionary *results = [[NSMutableDictionary alloc] init];
    
    NSInteger team1TotalScore = 0;
    NSInteger team2TotalScore = 0;
    NSInteger team1ID = 0;
    NSInteger team2ID = 0;
    
    
    if ([self.roundScoresTeamMatch count] > 1) {
        
        for (TeamMatchRoundScore *score in self.roundScoresTeamMatch) {
            team1ID = score.team1ID;
            team2ID = score.team2ID;
            
            if (score.scoreTeam1 > score.scoreTeam2) {
                team1TotalScore ++;
            } else if (score.scoreTeam2 > score.scoreTeam1) {
                team2TotalScore ++;
            } else if (score.scoreTeam1 == score.scoreTeam2) {
                team1TotalScore ++;
                team2TotalScore ++;
            }
            
        }
    } else {
        // only 1 round / set
        TeamMatchRoundScore *imrs = [self.roundScoresTeamMatch objectAtIndex:0];
        team1TotalScore = imrs.scoreTeam1;
        team2TotalScore = imrs.scoreTeam2;
    }
    
    [results setValue:[NSNumber numberWithInt:team1TotalScore] forKey:@"team1_score"];
    [results setValue:[NSNumber numberWithInt:team2TotalScore] forKey:@"team2_score"];
    [results setValue:[NSNumber numberWithInt:team1ID] forKey:@"team1_id"];
    [results setValue:[NSNumber numberWithInt:team2ID] forKey:@"team2_id"];
    
    
    
    return  results;
}


@end
