//
//  RecordIndMatchScoreSuccessCVCell.h
//  Vaiden
//
//  Created by James Chung on 11/25/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecordIndMatchScoreSuccessCVCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;

@end
