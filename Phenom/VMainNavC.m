//
//  VMainNavC.m
//  Vaiden
//
//  Created by Turbo on 6/12/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "VMainNavC.h"
#import "ImageSegue.h"

@interface VMainNavC ()

@end

@implementation VMainNavC

- (UIStoryboardSegue *)segueForUnwindingToViewController:(UIViewController *)toViewController
                                      fromViewController:(UIViewController *)fromViewController
                                              identifier:(NSString *)identifier
{
    NSLog(@"where?");
    // Instantiate a new CustomUnwindSegue
    ImageSegue *segue = [[ImageSegue alloc] initWithIdentifier:identifier
                                                        source:fromViewController
                                                   destination:toViewController];
    // Set the target point for the animation to the center of the button in this VC
    //CGPoint point = CGPointMake(self.center.x + 37.5, self.totalY + 72 + 37.5);
    //((ImageSegue *)segue).originatingPoint = point;
    segue.unwind = YES;
    return segue;
}

@end
