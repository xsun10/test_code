//
//  SpecialUIButton.m
//  Vaiden
//
//  Created by James Chung on 1/30/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "SpecialUIButton.h"

@implementation SpecialUIButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (SpecialUIButton *)init
{
    if (self = [super init]) {
        _shouldRemoveFromMySuperview = NO;
    }
    return  self;
}
@end
