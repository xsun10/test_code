//
//  PickupMatchesNeedingActionsCollectionCell.h
//  Vaiden
//
//  Created by James Chung on 10/14/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PickupMatchesNeedingActionsCollectionCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *profilePic;


@end
