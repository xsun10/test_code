//
//  UpcomingIndividualMatchNotificationCell.h
//  Vaiden
//
//  Created by James Chung on 3/11/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UpcomingIndividualMatchNotificationCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *headlineMessage;
@property (weak, nonatomic) IBOutlet UILabel *pendingConfirmedTag;
@property (weak, nonatomic) IBOutlet UILabel *montLabel;
@property (weak, nonatomic) IBOutlet UILabel *todayTomorrowLabel;
@property (weak, nonatomic) IBOutlet UIImageView *sportIcon;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *venueLabel;
@property (weak, nonatomic) IBOutlet UIImageView *userImage1;
@property (weak, nonatomic) IBOutlet UIImageView *userImage2;
@property (weak, nonatomic) IBOutlet UILabel *matchNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *sportPicBackground;
@property (weak, nonatomic) IBOutlet UIView *mainBackgroundViewCell;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@property (weak, nonatomic) IBOutlet UIButton *detailButton;
@end
