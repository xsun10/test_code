//
//  UrgentMatchNotificationCell.h
//  Phenom
//
//  Created by James Chung on 6/17/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UrgentMatchNotificationCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *matchCount;
@property (nonatomic, weak) IBOutlet UILabel *headlineMessage;
@property (nonatomic, weak) IBOutlet UILabel *description;
@property (weak, nonatomic) IBOutlet UIImageView *dotBackground;
@property (weak, nonatomic) IBOutlet UIImageView *notePic;
@property (weak, nonatomic) IBOutlet UILabel *sportLabel;
@property (weak, nonatomic) IBOutlet UIImageView *bulbIcon;
@end
