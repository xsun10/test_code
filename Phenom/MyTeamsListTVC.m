//
//  MyTeamsListTVC.m
//  Vaiden
//
//  Created by James Chung on 4/7/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "MyTeamsListTVC.h"
#import "IOSRequest.h"
#import "UserPreferences.h"
#import "MyTeamsListCell.h"
#import "UIColor+VaidenColors.h"
#import "UIView+Manipulate.h"
#import "TeamDetailTVC.h"

@interface MyTeamsListTVC ()

@property (nonatomic, strong) UserPreferences *preferences;
@property (nonatomic, strong) NSMutableArray *teamsList;
@property (nonatomic, strong) UIView *tempView;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
@property (nonatomic) NSInteger selectedRow;
@property (nonatomic, strong) UIView *overlay;


@end

@implementation MyTeamsListTVC

- (NSMutableArray *)teamsList
{
    if (!_teamsList) _teamsList = [[NSMutableArray alloc] init];
    return _teamsList;
}

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];

    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self loadTeamsList];
}
- (void)loadTeamsList
{
    [self.teamsList removeAllObjects];
    
    [self showTempSpinner];
    [IOSRequest getSessionUserTeams:[self.preferences getUserID] onCompletion:^(NSMutableArray *results) {
        
        for (id object in results) {
            Team *aTeam = [[Team alloc] initWithTeamDetails:[object[@"team_id"] integerValue]
                                                andTeamName:object[@"team_name"]
                                                 andCaptain:[[TeamPlayer alloc] initWithPlayerDetails:[object[@"captain_id"] integerValue]
                                                                                          andUserName:object[@"captain_username"]]
                                               andPicString:object[@"logo_string"]];
            [aTeam setNumMembers:[object[@"num_members"] integerValue]];
            
            [self.teamsList addObject:aTeam];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self.activityIndicator stopAnimating];
            self.tempView.hidden = YES;
            
            if ([self.teamsList count] == 0) {
                [self displayErrorOverlay];
            } else {
                self.overlay.hidden = YES;
                [self.tableView reloadData];
            }
        });
        
    }];
}

- (void)showTempSpinner
{
    self.tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    self.tempView.backgroundColor = [UIColor lightGrayColor];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] init];
    [self.activityIndicator setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width/2.0, [UIScreen mainScreen].bounds.size.height/2.0 - 50)];
    [self.activityIndicator startAnimating];
    [self.tempView addSubview:self.activityIndicator];
    
    [self.view addSubview:self.tempView];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.teamsList count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString static *CellIdentifier = @"Team Cell";
    
    MyTeamsListCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    Team *aTeam = [self.teamsList objectAtIndex:indexPath.row];
    cell.teamPic.image = [UIImage imageNamed:aTeam.picStringName];
    cell.teamNameLabel.text = aTeam.teamName;
    cell.numParticipantsLabel.text = [NSString stringWithFormat:@"%ld members", aTeam.numMembers];
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedRow = indexPath.row;
    [self performSegueWithIdentifier:@"Team Detail Segue" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Team Detail Segue"]) {
        Team *aTeam = [self.teamsList objectAtIndex:self.selectedRow];
        TeamDetailTVC *controller = segue.destinationViewController;
        controller.teamID = aTeam.teamID;
        
    }
}

- (void)displayErrorOverlay
{
    if (self.overlay == nil) {
        self.overlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, [UIScreen mainScreen].bounds.size.height)];
        self.overlay.backgroundColor = [UIColor lightLightGray];
        
        UILabel *firstLine = [[UILabel alloc] initWithFrame:CGRectMake(30, ([UIScreen mainScreen].bounds.size.height / 2) - 115, 260, 30)];
        firstLine.textColor = [UIColor darkGrayColor];
        firstLine.font = [UIFont systemFontOfSize:23];
        firstLine.text = @"No Teams";
        
        UILabel *secondLine = [[UILabel alloc] initWithFrame:CGRectMake(30, ([UIScreen mainScreen].bounds.size.height / 2) - 90, 260, 100)];
        secondLine.textColor = [UIColor lightGrayColor];
        secondLine.font = [UIFont systemFontOfSize:17];
        secondLine.text = @"You are not a part of any teams yet.  Get started by creating one and invite your friends to join it.";
        secondLine.numberOfLines = 0;
        
        UIButton *createButton = [[UIButton alloc] initWithFrame:CGRectMake(27, ([UIScreen mainScreen].bounds.size.height / 2) + 20  , 210, 40)];
        [createButton setTitle:@"Create Team" forState:UIControlStateNormal];
        [createButton setTitleColor:[UIColor peacock] forState:UIControlStateNormal];
        createButton.titleLabel.font = [UIFont systemFontOfSize:16.0];
        [createButton addTarget:self action:@selector(createNewTeam) forControlEvents:UIControlEventTouchUpInside];
        
        
        [createButton makeRoundedBorderWithRadius:3];
        [createButton.layer setBorderWidth:1.0];
        [createButton.layer setBorderColor:[[UIColor peacock] CGColor]];
        
        
        
        
        [self.overlay addSubview:firstLine];
        [self.overlay addSubview:secondLine];
        [self.overlay addSubview:createButton];
        
        [self.tableView addSubview:self.overlay];

    } else
        self.overlay.hidden = NO;
    
   }

- (void)createNewTeam
{
    if (![self.preferences isVerifiedAccount]) {
        [self performSegueWithIdentifier:@"Check User Verified Segue" sender:self];
    } else {
        [self performSegueWithIdentifier:@"Create Team Segue" sender:self];
    }
}

@end
