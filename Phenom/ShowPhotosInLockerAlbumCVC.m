//
//  ShowPhotosInLockerAlbumCVC.m
//  Vaiden
//
//  Created by James Chung on 5/12/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "ShowPhotosInLockerAlbumCVC.h"
#import "ShowPhotosInLockerAlbumCell.h"
#import "UserPreferences.h"
#import "IOSRequest.h"
#import "LockerPic.h"
#import "S3Tools2.h"
#import "UIImageView+AFNetworking.h"
#import "LockerPhotoDetailTVC.h"
#import "NSDate+Utilities.h"

@interface ShowPhotosInLockerAlbumCVC ()

@property (nonatomic, strong) UserPreferences *preferences;
@property (nonatomic, strong) NSMutableArray *picsArray;
@property (nonatomic) NSInteger selectedRow;
@property (nonatomic, strong) UIRefreshControl *refreshControl;

@end

@implementation ShowPhotosInLockerAlbumCVC

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (NSMutableArray *)picsArray
{
    if (!_picsArray) _picsArray = [[NSMutableArray alloc] init];
    return _picsArray;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
    
    
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(refreshView) forControlEvents:UIControlEventValueChanged];
    [self.collectionView addSubview:self.refreshControl];

    [self refreshView];

    // Do any additional setup after loading the view.
}
- (void)refreshView
{
    [self.picsArray removeAllObjects];
    
    [self.refreshControl endRefreshing];
    
    [self loadLockerPics];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if ([self.preferences getProfilePageRefreshState] == YES) {
        [self.picsArray removeAllObjects];
        
        [self loadLockerPics];
    }
}
- (void)loadLockerPics
{
    [IOSRequest fetchLockerPicsForSport:self.sport.sportID
                                andYear:self.picsYear
                              andSeason:self.picsSeason
                                forUser:self.albumHolderUserID
                         andSessionUser:[self.preferences getUserID]
                           onCompletion:^(NSMutableArray *results) {
                               
                               for (id object in results) {
                                
                                   
                                   NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                                   dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
                                   NSDate *postDate = [[dateFormatter dateFromString:object[@"ts"]] toLocalTime];
                                   
                                   LockerPic *lp = [[LockerPic alloc] initWithDetails:[object[@"locker_id"] integerValue]
                                                                          andFileName:object[@"image_name"]
                                                                           andCreator:[[Player alloc] initWithPlayerDetails:[object[@"creator_id"] integerValue]
                                                                                                                andUserName:object[@"creator_username"]]
                                                                            andSeason:self.picsSeason
                                                                              andYear:self.picsYear
                                                                          andNumComments:[object[@"num_comments"] integerValue]
                                                                          andNumLikes:[object[@"num_likes"] integerValue]
                                                                          andPostDate:postDate];
                                   [self.picsArray addObject:lp];
                               }
                               
                               dispatch_async(dispatch_get_main_queue(), ^{
                                   self.navigationItem.title = [NSString stringWithFormat:@"%@ %@ %ld", [self.sport.sportName uppercaseString], [self.picsSeason uppercaseString], self.picsYear];
                                   [self.collectionView reloadData];
                               });
                               
                           }];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.picsArray count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ShowPhotosInLockerAlbumCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Pic Cell" forIndexPath:indexPath];
    
    if ([self.picsArray count] == 0) return cell;
    
    LockerPic *lp = [self.picsArray objectAtIndex:indexPath.row];
    
    NSString *urlPostPic = [S3Tools2 getFileNameStringWithType:@"photo_thumbnail" baseString:[NSString stringWithFormat:@"%ld/photo/%@", lp.creator.userID, lp.imageFileName]];
    
    cell.lockerPic.contentMode = UIViewContentModeScaleAspectFill;
    cell.lockerPic.clipsToBounds = YES;
    cell.imageButtonHandle.tag = indexPath.row;
    
    [cell.lockerPic setImageWithURL:[NSURL URLWithString:urlPostPic] placeholderImage:[UIImage imageNamed:@"Landscape Placeholder"]];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
}
- (IBAction)detailAction:(UIButton *)sender
{
    self.selectedRow = sender.tag;
    [self performSegueWithIdentifier:@"Detail Segue" sender:self];

}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(UIButton *)sender
{
    if ([segue.identifier isEqualToString:@"Detail Segue"]) {
        LockerPhotoDetailTVC *controller = segue.destinationViewController;
        LockerPic *lp = [self.picsArray objectAtIndex:self.selectedRow];
        controller.lockerID = lp.lockerID;
        controller.picCreatorID = lp.creator.userID;
        controller.imageName = lp.imageFileName;
        controller.picSeason = lp.picSeason;
        controller.picYear = lp.picYear;
        controller.picSport = self.sport;
        
    }
}
@end
