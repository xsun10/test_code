//
//  ChooseOneSportTVC.h
//  Vaiden
//
//  Created by James Chung on 4/23/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "CustomBaseTVC.h"
#import "Sport.h"

@class ChooseOneSportTVC;

@protocol ChooseOneSportTVC_Delegate <NSObject>

-(void)setViewController:(ChooseOneSportTVC *)controller withSport:(Sport *)sp;

@end




@interface ChooseOneSportTVC : CustomBaseTVC

@property (nonatomic, weak) id <ChooseOneSportTVC_Delegate> delegate;

@end
