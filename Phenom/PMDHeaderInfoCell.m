//
//  PMDHeaderInfoCell.m
//  Vaiden
//
//  Created by James Chung on 1/16/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "PMDHeaderInfoCell.h"
#import "UIView+Manipulate.h"
#import "PickupMatchPlayersCVCell.h"
#import "S3Tools.h"
#import "UIView+Manipulate.h"
#import "UIImage+ProportionalFill.h"
#import "AfNetworking.h"
#import "UIColor+VaidenColors.h"

@interface PMDHeaderInfoCell ()
@property (nonatomic) NSInteger buttonConfig;

@end

@implementation PMDHeaderInfoCell

// View states (As defined Above and in excel sheet)
#define VIEW_STATE_1 0
#define VIEW_STATE_2 1
#define VIEW_STATE_3 2
#define VIEW_STATE_4 3
#define VIEW_STATE_5 4
#define VIEW_STATE_6 5

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)initPlayers
{
    self.playerCollectionView.delegate = self;
    self.playerCollectionView.dataSource = self;
  //  self.pageControl.currentPage = 0;
    
 //   NSPredicate *predicate = [NSPredicate predicateWithFormat:@"status = %@", @"ACCEPTED"];
 //   NSArray *matchParticipants = [self.pickupMatch.competitorArray filteredArrayUsingPredicate:predicate];
    
  //  self.numJoinedOrNeededLabel.text = [NSString stringWithFormat:@"%ld Joined %ld More Needed", [matchParticipants count], self.pickupMatch.minCompetitors - [matchParticipants count]];
 //   self.pageControl.numberOfPages = [matchParticipants count];
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (IBAction)matchButtonAction:(UIButton *)sender
{
    PMDHeaderInfoButtonCVCell *cell = (PMDHeaderInfoButtonCVCell *)[self.buttonCollectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:sender.tag inSection:0]];
    [self.delegate setWithButtonMessage:self withMessage:cell.actionName.text];
}




- (void)configureButtonDisplay:(NSInteger)matchButtonConfig
{
    self.buttonConfig = matchButtonConfig;
    self.buttonCollectionView.delegate = self;
    self.buttonCollectionView.dataSource = self;
        
}

// configureButtonDisplay must be called first

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView == self.buttonCollectionView) {
        switch (self.buttonConfig) {
            case VIEW_STATE_1:
                return 4;
            
                break;
            
            case VIEW_STATE_2:
            
                if (!self.isSessionUserConfirmed) {
                    return 0;
                } else {
                    // eventually might not show if over 10 days ago
                    return 2;
                }
                break;
            
            case VIEW_STATE_3:
            
                if (!self.isSessionUserConfirmed) {
                    return 2;
                } else {
                    return 3;
                }
            
                break;
            
            case VIEW_STATE_4:
                return 0;
            
                break;
            
            case VIEW_STATE_5:
                if (!self.isSessionUserConfirmed) {
                    return 2;
                } else {
                    return 3;
                }
                break;
            
            case VIEW_STATE_6:
            
                return 3;
                break;
            
            default:
                break;
        }
    } else if (collectionView == self.playerCollectionView) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"status = %@", @"ACCEPTED"];
        NSArray *matchParticipants = [self.pickupMatch.competitorArray filteredArrayUsingPredicate:predicate];
        
        return matchParticipants.count;
    }
    return 0;
}

- (void)goToProfile:(UIButton *)sender
{
//    [self.delegate setPMDMatchCompetitorsCellViewController:self withUserIDToSegue:sender.tag];
    [self.delegate setWithProfileIDForVC:self withUserIDToSegue:sender.tag];
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == self.buttonCollectionView) {
        static NSString *cellIdentifier = @"My Cell";
    
        PMDHeaderInfoButtonCVCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
        cell.matchActionButton.tag = indexPath.row;
    
        switch (self.buttonConfig) {
            case VIEW_STATE_1:
                if (indexPath.row == 0) {
                    cell.iconPic.image = [[UIImage imageNamed:@"BTN Set To Ready Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                    [cell.iconPic setTintColor:[UIColor darkGrayColor2]];
                    cell.actionName.text = @"Ready To Play";
                } else if (indexPath.row == 1) {
                    cell.iconPic.image = [[UIImage imageNamed:@"BTN Cancel Match Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                    [cell.iconPic setTintColor:[UIColor darkGrayColor2]];
                    cell.actionName.text = @"Cancel Match";
                } else if (indexPath.row == 2) {
                    cell.iconPic.image = [[UIImage imageNamed:@"BTN Message Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                    [cell.iconPic setTintColor:[UIColor darkGrayColor2]];
                    cell.actionName.text = @"Message";
                } else if (indexPath.row == 3) {
                    cell.iconPic.image = [[UIImage imageNamed:@"BTN Share Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                    [cell.iconPic setTintColor:[UIColor darkGrayColor2]];
                    cell.actionName.text = @"Share";
                }
            
                break;
            
            case VIEW_STATE_2:
                if (!self.isSessionUserConfirmed) {
                
                } else {
                
                    if (indexPath.row == 0) {
                        cell.iconPic.image = [[UIImage imageNamed:@"BTN Message Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                        [cell.iconPic setTintColor:[UIColor darkGrayColor2]];
                        cell.actionName.text = @"Message";
                    } else if (indexPath.row == 1) {
                        cell.iconPic.image = [[UIImage imageNamed:@"BTN Share Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                        [cell.iconPic setTintColor:[UIColor darkGrayColor2]];
                        cell.actionName.text = @"Share";
                    } else if (indexPath.row == 2) {
               
                    } else if (indexPath.row == 3) {
                
                    }
                    
                }

                break;
            
            case VIEW_STATE_3:
                if (!self.isSessionUserConfirmed) {
                    if (indexPath.row == 0) {
                        cell.iconPic.image = [[UIImage imageNamed:@"BTN Join Match Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                        [cell.iconPic setTintColor:[UIColor darkGrayColor2]];
                        cell.actionName.text = @"Join";
                    } else if (indexPath.row == 1) {
                        cell.iconPic.image = [[UIImage imageNamed:@"BTN Decline Match Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                        [cell.iconPic setTintColor:[UIColor darkGrayColor2]];
                        cell.actionName.text = @"Decline";
                    } else if (indexPath.row == 2) {
                    
                    } else if (indexPath.row == 3) {
                    
                    }
                } else {
                    if (indexPath.row == 0) {
                        cell.iconPic.image = [[UIImage imageNamed:@"BTN Decline Match Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                        [cell.iconPic setTintColor:[UIColor darkGrayColor2]];
                        cell.actionName.text = @"Withdraw";
                    } else if (indexPath.row == 1) {
                        cell.iconPic.image = [[UIImage imageNamed:@"BTN Message Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                        [cell.iconPic setTintColor:[UIColor darkGrayColor2]];
                        cell.actionName.text = @"Message";
                    } else if (indexPath.row == 2) {
                        cell.iconPic.image = [[UIImage imageNamed:@"BTN Share Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                        [cell.iconPic setTintColor:[UIColor darkGrayColor2]];
                        cell.actionName.text = @"Share";
                    } else if (indexPath.row == 3) {
                    
                    }
                }
     
                break;
            
            case VIEW_STATE_4:
        
            
            break;
            
            case VIEW_STATE_5:
                if (!self.isSessionUserConfirmed) {
                    if (indexPath.row == 0) {
                        cell.iconPic.image = [[UIImage imageNamed:@"BTN Join Match Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                        [cell.iconPic setTintColor:[UIColor darkGrayColor2]];
                        cell.actionName.text = @"Join";
                    } else if (indexPath.row == 1) {
                        cell.iconPic.image = [[UIImage imageNamed:@"BTN Share Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                        [cell.iconPic setTintColor:[UIColor darkGrayColor2]];
                        cell.actionName.text = @"Share";
                    } else if (indexPath.row == 2) {
                    
                    } else if (indexPath.row == 3) {
                    
                    }
                } else {
                    if (indexPath.row == 0) {
                        cell.iconPic.image = [[UIImage imageNamed:@"BTN Decline Match Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                            [cell.iconPic setTintColor:[UIColor darkGrayColor2]];
                        cell.actionName.text = @"Withdraw";
                    } else if (indexPath.row == 1) {
                        cell.iconPic.image = [[UIImage imageNamed:@"BTN Message Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                        [cell.iconPic setTintColor:[UIColor darkGrayColor2]];
                        cell.actionName.text = @"Message";
                    } else if (indexPath.row == 2) {
                        cell.iconPic.image = [[UIImage imageNamed:@"BTN Share Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                        [cell.iconPic setTintColor:[UIColor darkGrayColor2]];
                        cell.actionName.text = @"Share";
                    } else if (indexPath.row == 3) {
                    
                    }
                }
            
            
                break;
            
            
            case VIEW_STATE_6:
            
                if (indexPath.row == 0) {
                    cell.iconPic.image = [[UIImage imageNamed:@"BTN Cancel Match Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                    [cell.iconPic setTintColor:[UIColor darkGrayColor2]];
                    cell.actionName.text = @"Cancel Match";
                } else if (indexPath.row == 1) {
                    cell.iconPic.image = [[UIImage imageNamed:@"BTN Message Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                    [cell.iconPic setTintColor:[UIColor darkGrayColor2]];
                    cell.actionName.text = @"Message";
                } else if (indexPath.row == 2) {
                    cell.iconPic.image = [[UIImage imageNamed:@"BTN Share Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                    [cell.iconPic setTintColor:[UIColor darkGrayColor2]];
                    cell.actionName.text = @"Share";
                } else if (indexPath.row == 3) {
                
                }
            
                break;
            
               default:
                    break;
        }
    
        return cell;
        
        
    } else if (collectionView == self.playerCollectionView) {
        static NSString *cellIdentifier = @"Player Pic Cell";
        
        PickupMatchPlayersCVCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"status = %@", @"ACCEPTED"];
        NSArray *matchParticipants = [self.pickupMatch.competitorArray filteredArrayUsingPredicate:predicate];
        
        if (indexPath.item < [matchParticipants count]) {
            MatchPlayer *player = matchParticipants[indexPath.item];
            
            
            NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                    baseString:player.profileBaseString];
            
            [cell.profilePic setImageWithURL:[NSURL URLWithString:url]
                            placeholderImage:[UIImage imageNamed:@"avatar square"]];
            [UIImage makeRoundedImage:cell.profilePic withRadius:10];
            
        //    [cell.profilePic.layer setBorderWidth:2.0f];
        //    [cell.profilePic.layer setBorderColor:[UIColor whiteColor].CGColor];
            
            UIButton *playerPicButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];
            [playerPicButton addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
            playerPicButton.tag = player.userID;
            [cell.profilePic addSubview:playerPicButton];
            cell.profilePic.userInteractionEnabled = YES;
            
            cell.usernameLabel.text = player.userName;
       //     cell.usernameLabel.textColor = [UIColor darkGrayColor];
            
            PlayerSport *sport = [PlayerSport getSportObjectForName:self.pickupMatch.sport.sportName inArray:player.playerSports];
            cell.levelLabel.text = [NSString stringWithFormat:@"Level %ld", [sport.level integerValue]];
            cell.levelLabel.textColor = [UIColor darkGrayColor];
            if (player.userID == self.pickupMatch.matchCreatorID)
                cell.matchCreatorLabel.hidden = NO;
            else
                cell.matchCreatorLabel.hidden = YES;
            
        } else {
            [cell.profilePic setImage:[UIImage imageNamed:@"avatar round slot"]];
            cell.usernameLabel.text = @"";
            cell.levelLabel.text = @"Slot Open";
            cell.levelLabel.textColor = [UIColor darkGrayColor];
        }
        return cell;
    }
    return nil;
}

#define INSET_3_BUTTONS 30
#define INSET_2_BUTTONS 70
#define INSET_4_BUTTONS 0
#define INSET_0_BUTTONS 0

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    
    if (collectionView == self.buttonCollectionView) {
        switch (self.buttonConfig) {
            case VIEW_STATE_1:
                return UIEdgeInsetsMake(0, INSET_4_BUTTONS, 0, 0);
            
                break;
            
            case VIEW_STATE_2:
                if (!self.isSessionUserConfirmed) {
                    return UIEdgeInsetsMake(0, INSET_0_BUTTONS, 0, 0);
                } else {
                    // eventually might not show if over 10 days ago
                    return UIEdgeInsetsMake(0, INSET_2_BUTTONS, 0, 0);

                }
                break;
            
            case VIEW_STATE_3:
                if (!self.isSessionUserConfirmed) {
                    return UIEdgeInsetsMake(0, INSET_2_BUTTONS, 0, 0);
                } else {
                    return UIEdgeInsetsMake(0, INSET_3_BUTTONS, 0, 0);
                }
            
            
                break;
            
            case VIEW_STATE_4:
                return UIEdgeInsetsMake(0, INSET_0_BUTTONS, 0, 0);
            
                break;
            
            case VIEW_STATE_5:
                if (!self.isSessionUserConfirmed) {
                    return UIEdgeInsetsMake(0, INSET_2_BUTTONS, 0, 0);
                } else {
                    return UIEdgeInsetsMake(0, INSET_3_BUTTONS, 0, 0);
                }
            
                break;
            
            case VIEW_STATE_6:
            
                return UIEdgeInsetsMake(0, INSET_3_BUTTONS, 0, 0);
                break;
        
            default:
                break;
        }
        return UIEdgeInsetsMake(0, INSET_0_BUTTONS, 0, 0);
    }
    
    return UIEdgeInsetsMake(0,0,0,0);
    
}



@end
