//
//  PickupMatchCreationNews.m
//  Phenom
//
//  Created by James Chung on 4/23/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "PickupMatchCreationNews.h"

@implementation PickupMatchCreationNews

- (PickupMatchCreationNews *) initWithPickupMatchCreationNewsDetails:(NSInteger)matchID
                                                        andMatchName:(NSString *)matchName
                                                      matchCreatorID:(NSInteger)matchCreatorID
                                                matchCreatorUserName:(NSString *)matchCreatorUserName
                                                     andProfileStr:(NSString *)profileBaseString
                                                           matchDate:(NSDate *)matchDate
                                                        newsPostDate:(NSDate *)newsPostDate
                                                         andPostType:(NSInteger)postType
                                                           atVenueID:(NSInteger)venueID
                                                         atVenueName:(NSString *)venueName
                                                         withSportID:(NSInteger)sportID
                                                        andSportName:(NSString *)sportName
                                                        andSportType:(NSString *)sportType
                                                           isPrivate:(BOOL)isPrivate
                                                      andMatchStatus:(NSString *)matchStatus
                                                      minCompetitors:(NSUInteger)minCompetitors
                                                  requiredExperience:(NSMutableArray *)requiredExperience
                                                      andMatchTypeID:(NSUInteger)matchTypeID
                                                    andMatchTypeDesc:(NSString *)matchTypeDesc
                                                           andNewsID:(NSInteger)newsID
                                                      andNumComments:(NSUInteger)numComments
                                                       andNumRemixes:(NSUInteger)numRemixes

{
    self = [super initWithNewsDetails:matchCreatorID
                          andUserName:matchCreatorUserName
                      andProfileStr:profileBaseString
                             withDate:newsPostDate
                          andPostType:postType
                       andNumComments:numComments
                        andNumRemixes:numRemixes
                            andNewsID:newsID];
    
    if (self) {
        Sport *matchSport = [[Sport alloc] initWithSportDetails:sportID andName:sportName andType:sportType];
        Venue *matchVenue = [[Venue alloc] initVenueWithName:venueName andVenueID:venueID];
        MatchType *matchType = [[MatchType alloc] initWithMatchType:matchTypeID withDescription:matchTypeDesc];
        
        _pickupMatch = [[PickupMatch alloc] initMatchWithDetails:matchID
                                                    andMatchName:matchName
                                                       isPrivate:isPrivate
                                                       withSport:matchSport
                                                     withMessage:nil
                                                      onDateTime:matchDate
                                                         atVenue:matchVenue
                                                   createdByUser:matchCreatorID
                                                  minCompetitors:minCompetitors
                                                 withCompetitors:nil
                                                  withExperience:requiredExperience
                                                    andMatchType:matchType
                                                       andActive:YES
                                                  andMatchStatus:matchStatus];
        [self.pickupMatch setMatchCreatorUsername:matchCreatorUserName];
        
    }
    return self;
}


@end
