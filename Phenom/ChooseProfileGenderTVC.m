//
//  ChooseProfileGenderTVC.m
//  Vaiden
//
//  Created by James Chung on 12/21/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "ChooseProfileGenderTVC.h"

@interface ChooseProfileGenderTVC ()

@end

@implementation ChooseProfileGenderTVC


- (void)viewDidLoad
{
    [super viewDidLoad];

    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
    
   

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
 
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
    [self.delegate setProfileGenderWithController:self withGenderString:cell.textLabel.text];
    [self.navigationController popViewControllerAnimated:YES];
}

@end
