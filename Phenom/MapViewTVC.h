//
//  MapViewTVC.h
//  Phenom
//
//  Created by James Chung on 5/7/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//
// will disply a thumbnail in leftCalloutAccessoryview if the
// annotation implements the method "thumbnail" (return UIImage)

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "CustomBaseTVC.h"

@interface MapViewTVC : CustomBaseTVC <MKMapViewDelegate>

@property (nonatomic, strong) IBOutlet MKMapView *mapView;
@end
