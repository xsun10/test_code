//
//  MatchNews.h
//  Vaiden
//
//  Created by James Chung on 10/18/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Match.h"
#import "News.h"

@interface MatchNews : News

@property (nonatomic, strong) Match *match;
@property (nonatomic, strong) NSString *headlineMessageLabel;
@property (nonatomic, strong) NSString *matchGeneralMessage;

- (MatchNews *)initWithDetails:(Match *)matchInfo
                      postDate:(NSDate *)newsDate
               headLineMessage:(NSString *)headlineMessage
                generalMessage:(NSString *)generalMessage;

- (NSString *)canPostBeSharedBySessionUser:(NSInteger)sessionUserID;


@end
