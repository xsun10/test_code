//
//  CreateUsernameVC.h
//  Vaiden
//
//  Created by James Chung on 9/18/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomBaseVC.h"
#import "MBProgressHUD.h"

@interface CreateUsernameVC : CustomBaseVC <MBProgressHUDDelegate> {
    MBProgressHUD *HUD;
    NSOperationQueue         *operationQueue;
}

@end
