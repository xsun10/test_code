//
//  MatchComment.h
//  Vaiden
//
//  Created by James Chung on 9/27/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "Comment.h"

@interface MatchComment : Comment


@property (nonatomic) NSUInteger matchID;
@property (nonatomic, strong) NSString *matchCategory;

- (MatchComment *)initWithCommentDetails:(NSInteger)commentID
                          andCommentText:(NSString *)commentText
                                byUserID:(NSInteger)userID
                             andUsername:(NSString *)username
                           andProfileStr:(NSString *)profileBaseString
                                forMatch:(NSInteger)matchID
                           matchCategory:(NSString *)matchCategory
                                  onDate:(NSDate *)postDate;

@end
