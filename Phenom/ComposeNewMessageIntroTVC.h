//
//  ComposeNewMessageIntroTVC.h
//  Vaiden
//
//  Created by James Chung on 12/25/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomBaseTVC.h"

@interface ComposeNewMessageIntroTVC : CustomBaseTVC

@property (nonatomic) NSUInteger filterUserID;

@end
