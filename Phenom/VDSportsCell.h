//
//  VDSportsCell.h
//  Vaiden
//
//  Created by James Chung on 3/3/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VDSportsCell : UITableViewCell <UICollectionViewDataSource, UICollectionViewDelegate>

- (void)initializeCollectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *sportsCollectionView;
@property (nonatomic, strong) NSMutableArray *sportsArray;
@property (weak, nonatomic) IBOutlet UIView *myBackgroundView;
@end
