//
//  S3Tools2.h
//  Vaiden
//
//  Created by James Chung on 5/9/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <AWSS3/AWSS3.h>
#import <AWSRuntime/AWSRuntime.h>


@interface S3Tools2 : NSOperation <AmazonServiceRequestDelegate>

@property (nonatomic, strong) NSData *fileData;
@property (nonatomic, strong) NSString *dataType;
@property (nonatomic, strong ) NSString *fileNameBase;
@property (nonatomic, strong) NSString *fileExtension;
@property (nonatomic) NSInteger userID;
@property (nonatomic, strong) NSString *keyName;

-(id)initWithData:(NSData *)theData
          forUser:(NSInteger)userID
         dataType:(NSString *)dataType
andBaseFileNameString:(NSString *)fileNameBaseString
       andFileExt:(NSString *)fileExtension;

+ (NSString *)getFileNameStringWithType:(NSString *)dataType
                             baseString:(NSString *)baseFileStringIn;
+ (NSMutableDictionary *)getFileNameBaseStringWithType:(NSString *)dataType forUser:(NSInteger)userID;
+ (NSString *)getFileNameBaseStringWithType:(NSString *)dataType forVenue:(NSInteger)venueID;

@end
