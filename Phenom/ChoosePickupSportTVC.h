//
//  ChoosePickupSportTVC.h
//  Phenom
//
//  Created by James Chung on 7/12/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Sport.h"
#import "CustomBaseTVC.h"

@class ChoosePickupSportTVC;

@protocol ChoosePickupSportTVC_Delegate <NSObject>
- (void)setWithSportFromVC:(ChoosePickupSportTVC *)controller withSport:(Sport *)sport;

@end

@interface ChoosePickupSportTVC : CustomBaseTVC

@property (nonatomic, weak) id <ChoosePickupSportTVC_Delegate> delegate;

@end
