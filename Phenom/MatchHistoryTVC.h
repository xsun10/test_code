//
//  MatchHistoryTVC.h
//  Vaiden
//
//  Created by James Chung on 10/21/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Match.h"
#import "MBProgressHUD.h"
#import "CustomBaseTVC.h"

@interface MatchHistoryTVC : CustomBaseTVC <MBProgressHUDDelegate>
{
    MBProgressHUD *HUD;
}

@property (nonatomic) Match *playerMatch;
// if this is set, we know we came from match notifications page and should pop to root vc when done
@property (nonatomic) BOOL shouldShowKeyboard;

@end
