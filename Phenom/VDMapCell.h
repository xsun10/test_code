//
//  VDMapCell.h
//  Vaiden
//
//  Created by James Chung on 3/3/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Venue.h"
#import <MapKit/MapKit.h>

@class VDMapCell;

@protocol VDMapCell_Delegate <NSObject>
- (void)setWithTapAction:(VDMapCell *)controller shouldSegue:(BOOL)shouldSegue;

@end


@interface VDMapCell : UITableViewCell <MKMapViewDelegate>

@property (nonatomic, strong) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UIImageView *mapImage;

@property (nonatomic, weak) id <VDMapCell_Delegate> delegate;


- (void)initializeVenueMap:(Venue *)myVenue;

@end
