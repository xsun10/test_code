//
//  PMDMatchStatusCell.h
//  Vaiden
//
//  Created by James Chung on 1/16/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PMDMatchStatusCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *circleIconView;
@property (weak, nonatomic) IBOutlet UILabel *matchStatusLabel;

@end
