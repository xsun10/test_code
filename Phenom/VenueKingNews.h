//
//  VenueKingNews.h
//  Vaiden
//
//  Created by James Chung on 3/7/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "News.h"

@interface VenueKingNews : News

@property (nonatomic, strong) Venue *venue;
@property (nonatomic, strong) Sport *sport;

- (VenueKingNews *)initWithNewsID:(NSInteger)newsMakerID
                      andUsername:(NSString *)newsMakerUsername
                    andProfileStr:(NSString *)newsMakerProfileString
                         withDate:(NSDate *)newsDate
                      andPostType:(NSInteger)postType
                   andNumComments:(NSInteger)numComments
                    andNumRemixes:(NSInteger)numRemixes
                        andNewsID:(NSInteger)newsID
                         andVenue:(Venue *)venueObj
                         andSport:(Sport *)sportObj;
@end
