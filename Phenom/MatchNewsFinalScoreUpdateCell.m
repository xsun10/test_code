//
//  MatchNewsFinalScoreUpdateCell.m
//  Vaiden
//
//  Created by James Chung on 11/1/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "MatchNewsFinalScoreUpdateCell.h"
#import "Player.h"
#import "NewsfeedMatchHeadingCVCell.h"
#import "S3Tools.h"
#import "IOSRequest.h"
#import "AFNetworking.h"
#import "UIImage+ProportionalFill.h"
#import "MatchNewsFinalScoreUpdateScoreCVCell.h"
#import "IndividualMatchRoundScore.h"

@implementation MatchNewsFinalScoreUpdateCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews
{
    self.matchHeadingCV.delegate = self;
    self.matchHeadingCV.dataSource = self;
    
    self.scoreCV.delegate = self;
    self.scoreCV.dataSource = self;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    if ([self.matchCategory isEqualToString:@"individual"] && collectionView == self.matchHeadingCV) {
        return 3;
    } else if (collectionView == self.scoreCV) {
        return [self.iMatch.scoreIndMatch.roundScoresIndMatch count];
    }
    
    return [self.competitors count];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    if (collectionView == self.scoreCV)
        return 2;
    
    return 1;
}

- (void)goToProfile:(UIButton *)sender
{
    [self.delegate setMatchNewsFinalScoreUpdateCellParentViewController:self withUserIDToSegue:sender.tag];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
   
   
    Player *p = nil;
    
    if ([self.matchCategory isEqualToString:@"individual"]  && collectionView == self.matchHeadingCV) {
        static NSString *cellIdentifier = @"Competitor Pics";
        
        NewsfeedMatchHeadingCVCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
        
        if (indexPath.row == 1) {
            // show "vs" not the pic
            cell.playerPic.hidden = YES;
            UILabel *vsLabel = [[UILabel alloc] initWithFrame:CGRectMake(8, 0, 40, 50)];
            vsLabel.text = @"VS";
            vsLabel.textColor = [UIColor whiteColor];
            vsLabel.textAlignment = NSTextAlignmentCenter;
            [vsLabel setFont:[UIFont fontWithName:@"HelveticaNeue-BoldItalic" size:22]];
            
            [cell addSubview:vsLabel];
            
            
        } else {
            
            
            if (indexPath.row == 2)
                p  = [self.competitors objectAtIndex:1];
            else
                p = [self.competitors objectAtIndex:0];
        
            NSString *urlMain = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                        baseString:p.profileBaseString];
            
            [cell.playerPic setImageWithURL:[NSURL URLWithString:urlMain]
                           placeholderImage:[UIImage imageNamed:@"avatar round"]];
            [UIImage makeRoundedImage:cell.playerPic withRadius:25];
            
            cell.playerPic.layer.borderColor = [UIColor whiteColor].CGColor;
            cell.playerPic.layer.borderWidth = 1;
            
            // profile pic button
            UIButton *picButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
            [picButton addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
            picButton.tag = p.userID;
            [cell.playerPic addSubview:picButton];
            cell.playerPic.userInteractionEnabled = YES;
            // end profile pic button
        }
        
        return  cell;
    } else if (collectionView == self.matchHeadingCV){
        static NSString *cellIdentifier = @"Competitor Pics";
        
        NewsfeedMatchHeadingCVCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];

        p = [self.competitors objectAtIndex:indexPath.row];
        NSString *urlMain = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                    baseString:p.profileBaseString];
        
        [cell.playerPic setImageWithURL:[NSURL URLWithString:urlMain]
                       placeholderImage:[UIImage imageNamed:@"avatar round"]];
        [UIImage makeRoundedImage:cell.playerPic withRadius:25];

        
        return  cell;
    } else if (collectionView == self.scoreCV) {
        static NSString *cellIdentifier = @"Score Cell";
        MatchNewsFinalScoreUpdateScoreCVCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
        
        IndividualMatchRoundScore *matchScore = [self.iMatch.scoreIndMatch.roundScoresIndMatch objectAtIndex:indexPath.row];
        
        if (indexPath.section == 0) { // first section
            cell.scoreLabel.text = [NSString stringWithFormat:@"%d", matchScore.scorePlayer1];
            self.player1ScoreLabel.text = [self.iMatch getPlayerForID:matchScore.userIDPlayer1].userName; // I know this is not efficient...doing this for time's sake for now.
        } else if (indexPath.section == 1) { // second section
            cell.scoreLabel.text = [NSString stringWithFormat:@"%d", matchScore.scorePlayer2];
            self.player2ScoreLabel.text = [self.iMatch getPlayerForID:matchScore.userIDPlayer2].userName; // I know this is not efficient...doing this for time's sake for now.
        }
        
        return cell;
    }
   
    return nil;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    if (collectionView != self.scoreCV)
        return UIEdgeInsetsMake(0, 40, 0, 0);
 
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

@end
