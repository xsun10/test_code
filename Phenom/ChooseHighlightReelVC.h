//
//  ChooseHighlightReelVC.h
//  Phenom
//
//  Created by James Chung on 5/28/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ChooseHighlightReelVC;

@protocol ChooseHighlightReelVC_Delegate <NSObject>
-(void)setWithHighlighVC:(ChooseHighlightReelVC *)controller
           withVideoData:(NSData *)vidData
            andThumbnail:(UIImage *)vidThumbnail
              andFileExt:(NSString *)fileExt;
@end

@interface ChooseHighlightReelVC : UIViewController

@property (nonatomic, weak) id <ChooseHighlightReelVC_Delegate> delegate;
@end


