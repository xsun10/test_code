//
//  NTeamLandingTVC.m
//  Vaiden
//
//  Created by James Chung on 4/6/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "NTeamLandingTVC.h"
#import "UserPreferences.h"

@interface NTeamLandingTVC ()

@property (nonatomic, strong) UserPreferences *preferences;

@end

@implementation NTeamLandingTVC

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        if (![self.preferences isVerifiedAccount]) {
            [self performSegueWithIdentifier:@"Check Verify Status" sender:self];
        } else {
            [self performSegueWithIdentifier:@"Challenge Team Segue" sender:self];
        }
        
    } else if (indexPath.row == 1) { // create team cell
        //Check Verify Status
        if (![self.preferences isVerifiedAccount]) {
            [self performSegueWithIdentifier:@"Check Verify Status" sender:self];
        } else {
            [self performSegueWithIdentifier:@"Create Team Segue" sender:self];
        }
    }
}
@end
