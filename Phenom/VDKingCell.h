//
//  VDKingCell.h
//  Vaiden
//
//  Created by James Chung on 3/3/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@class VDKingCell;

@protocol VDKingCell_Delegate <NSObject>
- (void)setWithUserFromVC:(VDKingCell *)controller withUser:(NSInteger)userID;

@end



@interface VDKingCell : UITableViewCell <UICollectionViewDataSource, UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *kingsCollectionView;
@property (weak, nonatomic) IBOutlet UIPageControl *kingsPageControl;
- (void)initializeKingsCell;
@property (nonatomic, strong) NSMutableArray *kingsArray;
@property (weak, nonatomic) IBOutlet UIView *myBackgroundView;

@property (nonatomic, weak) id <VDKingCell_Delegate> delegate;


@end
