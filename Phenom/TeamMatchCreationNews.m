//
//  TeamMatchCreationNews.m
//  Vaiden
//
//  Created by James Chung on 4/16/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "TeamMatchCreationNews.h"

@implementation TeamMatchCreationNews

- (TeamMatchCreationNews *) initWithIndividualMatchCreationNewsDetails:(TeamMatch *)tMatch
                                                  matchCreatorUserName:(NSString *)matchCreatorUserName
                                                         andProfileStr:(NSString *)profileBaseString
                                                          newsPostDate:(NSDate *)newsPostDate
                                                           andPostType:(NSInteger)postType
                                                             andNewsID:(NSInteger)newsID
                                                        andNumComments:(NSUInteger)numComments
                                                         andNumRemixes:(NSUInteger)numRemixes



{
    self = [super initWithNewsDetails:tMatch.matchCreatorID
                          andUserName:matchCreatorUserName
                        andProfileStr:profileBaseString
                             withDate:newsPostDate
                          andPostType:(NSInteger)postType
                       andNumComments:numComments
                        andNumRemixes:numRemixes
                            andNewsID:newsID];
    
    if (self) {
        
        _tMatch = tMatch; 
        
        
    }
    return self;
}

@end
