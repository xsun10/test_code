//
//  TeamDetailTVC.h
//  Vaiden
//
//  Created by James Chung on 4/7/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "CustomBaseTVC.h"
#import "Team.h"
#import "TDButtonsCell.h"
#import "MBProgressHUD.h"


@interface TeamDetailTVC : CustomBaseTVC <TDButtonsCell_Delegate, UIActionSheetDelegate, MBProgressHUDDelegate>
{
    MBProgressHUD *HUD;
}

@property (nonatomic) NSInteger teamID;

@end
