//
//  VenueCheckinStats.m
//  Vaiden
//
//  Created by James Chung on 5/3/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "VenueCheckinStats.h"

@interface VenueCheckinStats ()

@end

@implementation VenueCheckinStats


-(VenueCheckinStats *)initWithVenueCheckinStatsAllTime:(NSInteger)allTimeStat
                                 andCheckinStatForHour:(NSInteger)hourCheckinStat
                     andCheckinStatForHourForFollowing:(NSInteger)hourCheckinFollowingStat
{
    self = [super init];
    
    if (self) {
        _checkInStatsAllTime = allTimeStat;
        _checkInStatsHour = hourCheckinStat;
        _checkInStatsHourForFollowing  = hourCheckinFollowingStat;
    }
    
    return self;
}

@end
