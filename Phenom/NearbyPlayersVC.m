//
//  NearbyPlayersVC.m
//  Phenom
//
//  Created by James Chung on 7/14/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "NearbyPlayersVC.h"
#import "UserPreferences.h"
#import "IOSRequest.h"
#import "S3Tools.h"
#import "UIImage+ProportionalFill.h"
#import "Contact.h"
#import "NearbyPlayersCell.h"
#import "AFNetworking.h"
#import "PlayerDetailTVC.h"
#import "UIColor+VaidenColors.h"
#import "UIButton+RoundBorder.h"
#import "UIView+Manipulate.h"

@interface NearbyPlayersVC ()

@property (nonatomic, strong) NSMutableArray *nearbyPlayers;
@property (nonatomic, strong) UserPreferences *preferences;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentControl;
@property (nonatomic) NSInteger filterSportID;
@property (nonatomic) NSInteger proximityRange;
@property (strong, nonatomic)  UILabel *sportLabel;
@property (weak, nonatomic) IBOutlet UITableView *playersTableView;
@property (strong, nonatomic) UIView *fixedMenu;
@property (nonatomic) NSInteger orderByType;
//@property (nonatomic) NSInteger selectedUserID;
@property (nonatomic) NSUInteger selectedRow;

@property (nonatomic) BOOL noResultsToDisplay;
@property (nonatomic, strong) UIView *tempView;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) UIButton *sportButtonInHiddenFixedMenu;

@property (nonatomic, strong) NSMutableArray *nearbyPlayersWithZeroPlayedMatches;
@property (nonatomic, strong) UIView *overlay;

@end

@implementation NearbyPlayersVC

#define ORDER_BY_LEVEL 0
#define ORDER_BY_COINS 1

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (NSMutableArray *)nearbyPlayers
{
    if (!_nearbyPlayers) _nearbyPlayers = [[NSMutableArray alloc] init];
    return _nearbyPlayers;
}

- (NSMutableArray *)nearbyPlayersWithZeroPlayedMatches
{
    if (!_nearbyPlayersWithZeroPlayedMatches) _nearbyPlayersWithZeroPlayedMatches = [[NSMutableArray alloc] init];
    return _nearbyPlayersWithZeroPlayedMatches;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.sportButtonInHiddenFixedMenu makeCircleWithColorAndBackground:[UIColor almostBlack] andRadius:5];
    
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Location Services Are Off";
        alert.message = @"Please turn on Location Services in your Phone Settings to use this feature";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
        self.noResultsToDisplay = YES;
        self.sportLabel.hidden = YES;
    } else {
        [self showTempSpinner];
        self.noResultsToDisplay = YES;
        
        self.filterSportID = 0;
        self.orderByType = ORDER_BY_LEVEL;
        self.selectedRow = 0;
        
 //       self.fixedMenu.layer.borderColor = [UIColor colorWithRed:68.0/255.0 green:74.0/255.0 blue:80.0/255.0 alpha:1].CGColor;
 //       self.fixedMenu.layer.borderWidth = .5f;
        
        self.playersTableView.delegate = self;
        self.playersTableView.dataSource = self;
        
 //       self.fixedMenu.center = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height-10/2);
        
        /*
        self.fixedMenu = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 35)];
        self.fixedMenu.backgroundColor = [UIColor almostBlack];
        
        self.fixedMenu.layer.borderColor = [UIColor almostBlack].CGColor;
        self.fixedMenu.layer.borderWidth = .5f;
        
        [self.view addSubview:self.fixedMenu];
        
        self.sportLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, 180, 35)];
   //     self.sportLabel.text = @"Showing: Basketball";
        self.sportLabel.textColor = [UIColor whiteColor];
        self.sportLabel.font = [UIFont systemFontOfSize:12];
        
        [self.fixedMenu addSubview:self.sportLabel];
        
        self.sportButtonInHiddenFixedMenu = [[UIButton alloc] initWithFrame:CGRectMake(270, 8, 45, 20)];
        [self.sportButtonInHiddenFixedMenu setTitle:@"Sport" forState:UIControlStateNormal];
        self.sportButtonInHiddenFixedMenu.titleLabel.textColor = [UIColor whiteColor];
        [self.sportButtonInHiddenFixedMenu.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-LightItalic" size:12]];
        self.sportButtonInHiddenFixedMenu.layer.borderColor = [UIColor whiteColor].CGColor;
        self.sportButtonInHiddenFixedMenu.layer.borderWidth = 0.5;
        [self.sportButtonInHiddenFixedMenu addTarget:self action:@selector(changeSport:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.fixedMenu addSubview:self.sportButtonInHiddenFixedMenu];
        */
        
        self.fixedMenu = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 40)]; // was 35 height
        self.fixedMenu.backgroundColor = [UIColor midGray1];
        
        self.fixedMenu.layer.borderColor = [UIColor midGray].CGColor;
        self.fixedMenu.layer.borderWidth = .5f;
        
        [self.view addSubview:self.fixedMenu];
        
        self.sportLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, 180, 40)]; // was 35 height
        self.sportLabel.textColor = [UIColor darkGrayColor];
        self.sportLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:15];
        
        [self.fixedMenu addSubview:self.sportLabel];
        
        self.sportButtonInHiddenFixedMenu = [[UIButton alloc] initWithFrame:CGRectMake(255, 7, 60, 25)]; // was 20 height & 8 y location
        [self.sportButtonInHiddenFixedMenu setTitle:@"FILTER" forState:UIControlStateNormal];
        [self.sportButtonInHiddenFixedMenu setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        //    self.sportButtonInHiddenFixedMenu.titleLabel.textColor = [UIColor darkGrayColor];
        [self.sportButtonInHiddenFixedMenu.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-MediumItalic" size:13]];
        self.sportButtonInHiddenFixedMenu.layer.borderColor = [UIColor darkGrayColor].CGColor;
        self.sportButtonInHiddenFixedMenu.layer.borderWidth = 0.5;
        [self.sportButtonInHiddenFixedMenu addTarget:self action:@selector(changeSport:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.fixedMenu addSubview:self.sportButtonInHiddenFixedMenu];
        
        
        [self.view addSubview:self.fixedMenu];
  
        self.sportLabel.hidden = NO;
     //   self.sportLabel.text = @"Showing: Basketball";
        
        [self.playersTableView setContentInset:UIEdgeInsetsMake(35,0,150,0)];
        
        [self initLocation];
        [locationManager startUpdatingLocation];
        
        

    }
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
    
    
}


- (void)showTempSpinner
{
    self.tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    self.tempView.backgroundColor = [UIColor lightGrayColor];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] init];
    [self.activityIndicator setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width/2.0, [UIScreen mainScreen].bounds.size.height/2.0 )];
    [self.activityIndicator startAnimating];
    [self.tempView addSubview:self.activityIndicator];
    
    [self.view addSubview:self.tempView];
    
}

- (void)displayErrorOverlay
{
    [self.overlay removeFromSuperview];
    
    self.overlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, [UIScreen mainScreen].bounds.size.height)];
    self.overlay.backgroundColor = [UIColor lightLightGray];
    
    UILabel *firstLine = [[UILabel alloc] initWithFrame:CGRectMake(30, ([UIScreen mainScreen].bounds.size.height / 2) - 115, 260, 30)];
    firstLine.textColor = [UIColor darkGrayColor];
    firstLine.font = [UIFont systemFontOfSize:23];
    firstLine.text = @"No Players Found";
    
    UILabel *secondLine = [[UILabel alloc] initWithFrame:CGRectMake(30, ([UIScreen mainScreen].bounds.size.height / 2) - 95, 260, 110)];
    secondLine.textColor = [UIColor lightGrayColor];
    secondLine.font = [UIFont systemFontOfSize:17];
    secondLine.text = @"We weren't able to find players in your area.  Invite your friends to join to get a start playing the Vaiden app";
    secondLine.numberOfLines = 0;
    
    [self.overlay addSubview:firstLine];
    [self.overlay addSubview:secondLine];
    
    [self.playersTableView addSubview:self.overlay];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
 //   if (self.noResultsToDisplay)
 //       return 1;
    
    return [self.nearbyPlayers count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Player Cell";
    
    NearbyPlayersCell *cell = (NearbyPlayersCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    /*
    if (self.noResultsToDisplay) {
        cell.usernameLabel.hidden = YES;
        cell.profilePic.hidden = YES;
        cell.followButton.hidden = YES;
        cell.levelLabel.hidden = YES;
        cell.probabilityLabel.hidden = YES;
        cell.sportLabel.hidden = YES;
        cell.textLabel.text = @"No Players Nearby";
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.textLabel.textColor = [UIColor lightGrayColor];
        cell.textLabel.hidden = NO;
        return cell;
    } else {
        cell.usernameLabel.hidden = NO;
        cell.profilePic.hidden = NO;
        cell.followButton.hidden = NO;
        cell.levelLabel.hidden = NO;
        cell.probabilityLabel.hidden = NO;
        cell.sportLabel.hidden = NO;
        cell.textLabel.hidden = YES;
    }
*/
    
    
    Contact *vContact = (self.nearbyPlayers)[indexPath.row];
    
    if (vContact.userID == [self.preferences getUserID]) // dont' show follow button if session user has same user id
        cell.followButton.hidden = YES;
    
    PlayerSport *sport = [vContact.playerSports objectAtIndex:0]; // only one sport should be in array
        
    if ([vContact computeTotalNumMatchesPlayedForSport:sport.sportID] > 0)
        cell.levelLabel.text = [NSString stringWithFormat:@"%ld",  [sport.level integerValue]];
    else
        cell.levelLabel.text = @"0";
    
    
    cell.sportLabel.text = [NSString stringWithFormat:@"%@ Level", [sport.sportName capitalizedString]];
//    cell.recordLabel.text = [NSString stringWithFormat:@"%d Wins - %d Losses", sport.playerRecordForSport.wins, sport.playerRecordForSport.losses];
    cell.coinsLabel.text = [NSString stringWithFormat:@"$%.0f", vContact.coins];
    
    if (vContact.userID != [self.preferences getUserID]) { // only show probability label if not session user
        cell.probabilityLabel.hidden = NO;
        float val = vContact.percentProbabilitySessionUserCanBeatPlayer * 100;
        cell.probabilityLabel.text = [NSString stringWithFormat:@"%.0f%@ chance you win", val, @"%"];
    } else {
        cell.probabilityLabel.hidden = YES;
    }
    
    [cell.followButton makeRoundedBorderWithRadius:3];
    cell.followButton.tag = indexPath.row;
    
    if (vContact.followingStatus == NOT_FOLLOWING_STATUS && (vContact.userID != [self.preferences getUserID])) {
        [cell.followButton setTitle:@"Follow" forState:UIControlStateNormal];
        cell.followButton.enabled = YES;
        cell.followButton.backgroundColor = [UIColor newBlueLight];
    } else if (vContact.followingStatus == IS_FOLLOWING_STATUS) {
        [cell.followButton setTitle:@"UnFollow" forState:UIControlStateNormal];
        cell.followButton.enabled = YES;
        cell.followButton.backgroundColor = [UIColor newBlueLight];
    } else if (vContact.followingStatus == PENDING_FOLLOWING_STATUS) {
        [cell.followButton setTitle:@"Pending" forState:UIControlStateNormal];
        cell.followButton.enabled = NO;
        cell.followButton.backgroundColor = [UIColor lightGrayColor];
    }
    
    cell.usernameLabel.text = vContact.userName;

    
    NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                            baseString:vContact.profileBaseString
                     ];
    
    [cell.profilePic setImageWithURL:[NSURL URLWithString:url]
                    placeholderImage:[UIImage imageNamed:@"avatar round"]];
    
    [UIImage makeRoundedImage:cell.profilePic withRadius:30];
    
    cell.pigIcon.image = [UIImage imageNamed:@"Coins"];
    
    
    return cell;
}


- (void)initLocation
{
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
    self.proximityRange = 10;
    
    
}

- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations {
    CLLocation *location = locations[0];
    
    
    [locationManager stopUpdatingLocation];
    //    [self.playersTableView setUserInteractionEnabled:YES];
    
    
    [IOSRequest fetchProximityPlayers:[self.preferences getUserID]
                              andLong:location.coordinate.longitude
                               andLat:location.coordinate.latitude
                            withRange:self.proximityRange
                        filterBySport:self.filterSportID
                              orderBy:self.orderByType
     
                         onCompletion:^(NSMutableArray *resultArray) {
                             [self.nearbyPlayers removeAllObjects];
                             [self.nearbyPlayersWithZeroPlayedMatches removeAllObjects];
                             for (id object in resultArray) {
                                 Contact *vContact = [[Contact alloc]
                                                      initWithContactDetails:[object[@"user_id"] integerValue]
                                                      andProfileStr:object[@"profile_pic_string"]
                                                      andUserName:object[@"username"]
                                                      andFullName:object[@"fullname"]
                                                      andLocation:object[@"location"]
                                                      andAbout:object[@"about"]
                                                      andCoins:[object[@"coins"] floatValue]
                                                      contactType:CONTACT_TYPE_NEARBY
                                                      followingStatus:[object[@"follow_status"] integerValue]];
                                 
                                 [vContact setPercentProbabilitySessionUserCanBeatPlayer:[object[@"probability_session_user_wins"] floatValue]];
                                 
                                 NSDictionary *sportObj = object[@"sports"];
                                 
                                 PlayerSport *sport = [[PlayerSport alloc] initWithSportDetails:sportObj[@"sport_name"]
                                                                                        andType:sportObj[@"type"]
                                                                                       andLevel:sportObj[@"level"]
                                                                                 andSportRecord:sportObj[@"win_loss_record"]];
                                 
                                
                                 [vContact addPlayerSport:sport];
                                 
                                 
                                 
                                 if ([vContact computeTotalNumMatchesPlayedForSport:sport.sportID] > 0) {
                                     
                                     [self.nearbyPlayers addObject:vContact]; // at least one match played, so add to nearbyPlayersArray
                                 } else {
                                    [self.nearbyPlayersWithZeroPlayedMatches addObject:vContact]; // only one match played so add to this array
                                 }
                             }
                             
                             // Now append the nearbyPlayersWithZeroPlayedMatches to end of nearbyPlayers
                             // Later when there is pagination, will append the nearbyPlayersWithZeroPlayedMatches after last paginated
                             // page is shown.
                             
                             [self.nearbyPlayers addObjectsFromArray:self.nearbyPlayersWithZeroPlayedMatches];
                             
                             dispatch_async(dispatch_get_main_queue(), ^ {
                                 
                                 [self.activityIndicator stopAnimating];
                                 self.tempView.hidden = YES;
                                 if ([self.nearbyPlayers count] == 0) {
                                     self.noResultsToDisplay = YES;
                                     self.sportLabel.text = @"";
                                     [self displayErrorOverlay];
                                 } else {
                                     self.noResultsToDisplay = NO;
                                     Contact *tempContact = [self.nearbyPlayers objectAtIndex:0];
                                     
                                     if ([tempContact.playerSports count] > 0) {
                                         PlayerSport *tempSport = [tempContact.playerSports objectAtIndex:0];
                                         self.sportLabel.text = [tempSport.sportName capitalizedString];
                                     }
                                 }
                                 
                                 [self.playersTableView reloadData];
                                 //                          [MBProgressHUD hideHUDForView:self.view animated:YES];
                             });
                             
                             
                             
                         }];
    
    
}

- (void)setWithSportFromVC:(ChooseIndividualSportTVC *)controller withSport:(Sport *)sport
{
    [self.overlay removeFromSuperview];
 //   self.overlay = nil;
    
 //   self.sportLabel.text = [sport.sportName capitalizedString];
    self.filterSportID = sport.sportID;
 //   [self initLocation];
    [locationManager startUpdatingLocation];
    
}

- (IBAction)changeSport:(id)sender
{
    [self performSegueWithIdentifier:@"Choose Sport Segue" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [self.overlay removeFromSuperview];
 //   self.overlay = nil;
    
    if ([segue.identifier isEqualToString:@"Choose Sport Segue"]) {
        ChooseIndividualSportTVC *controller = segue.destinationViewController;
        controller.delegate = self;
    } else if ([segue.identifier isEqualToString:@"Player Detail Segue"]) {
        PlayerDetailTVC *controller = segue.destinationViewController;
        Contact *vContact = [self.nearbyPlayers objectAtIndex:self.selectedRow];
        controller.playerUserID = vContact.userID;
        controller.profilePicString = vContact.profileBaseString;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.noResultsToDisplay) {
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        return screenRect.size.height - 150;
    }
    
    return 84.0;
}
/*
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    // CGFloat stillViewDesiredOriginY; declared ivar
    CGRect newFrame = self.fixedMenu.frame;
    newFrame.origin.x = 0;
    newFrame.origin.y = self.playersTableView.contentOffset.y+(self.playersTableView.frame.size.height-35);
    self.fixedMenu.frame = newFrame;
}
*/
- (IBAction)changeSegment:(id)sender
{
    if (self.segmentControl.selectedSegmentIndex == ORDER_BY_LEVEL) {
        self.orderByType = ORDER_BY_LEVEL;
    } else if (self.segmentControl.selectedSegmentIndex == ORDER_BY_COINS) {
        self.orderByType = ORDER_BY_COINS;
    }
    
    [locationManager startUpdatingLocation];
}

- (void)showSubmissionError
{
    UIAlertView *alert = [[UIAlertView alloc] init];
    alert.title = @"Submission Error";
    alert.message = @"There was a problem submitting your request.  Please try again.";
    [alert addButtonWithTitle:@"OK"];
    [alert show];
}


- (IBAction)followAction:(UIButton *)sender
{
    Contact *vContact = [self.nearbyPlayers objectAtIndex:sender.tag];
    sender.enabled = NO;
    if (vContact.followingStatus == IS_FOLLOWING_STATUS && vContact.userID != [self.preferences getUserID]) {
        [IOSRequest removeFollowConnectionWithLeader:vContact.userID
                                         andFollower:[self.preferences getUserID]
                                        onCompletion:^(NSDictionary *results) {
                                            
                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                sender.enabled = YES;
                                                
                                                if (![results[@"outcome"] isEqualToString:@"failure"]) {
                                                    [self.preferences setNewsfeedRefreshState:YES];
                                                    [self.preferences setChallengePlayersListRefreshState:YES];
                                                    vContact.followingStatus = NOT_FOLLOWING_STATUS;
                                                    [self.nearbyPlayers replaceObjectAtIndex:sender.tag withObject:vContact];
                                                    [self.playersTableView reloadData];
                                                } else {
                                                    [self showSubmissionError];
                                                }
                                                
                                            });
                                        }];
    } else if (vContact.followingStatus == NOT_FOLLOWING_STATUS && vContact.userID != [self.preferences getUserID]) {
        [IOSRequest makeFollowConnectionWithLeader:vContact.userID
                                       andFollower:[self.preferences getUserID]
                                      onCompletion:^(NSDictionary *results) {
                                          
                                          dispatch_async(dispatch_get_main_queue(), ^ {
                                              sender.enabled = YES;
                                              
                                              if (![results[@"outcome"] isEqualToString:@"failure"]) {
                                                  [self.preferences setNewsfeedRefreshState: YES];
                                                  [self.preferences setChallengePlayersListRefreshState:YES];
                                                  vContact.followingStatus = [results[@"follow_status"] integerValue];
                                                  [self.nearbyPlayers replaceObjectAtIndex:sender.tag withObject:vContact];
                                                  [self.playersTableView reloadData];
                                              } else {
                                                  [self showSubmissionError];
                                              }
                                          });
                                      }];
        
        
    }

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!self.noResultsToDisplay) {
        self.selectedRow = indexPath.row;
        [self performSegueWithIdentifier:@"Player Detail Segue" sender:self];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
