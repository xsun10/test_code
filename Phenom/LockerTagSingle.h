//
//  LockerTagSingle.h
//  Vaiden
//
//  Created by James Chung on 5/7/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "LockerTag.h"

@interface LockerTagSingle : LockerTag

@property (nonatomic, strong) NSString *tagSelection;
- (LockerTagSingle *) initMatchWithDetails:(NSInteger)lockerTagID
                                andTagType:(NSInteger)lockerTagType
                                andTagName:(NSString *)lockerTagName
                           andTagSelection:(NSString *)lockerTagSelection;

@end
