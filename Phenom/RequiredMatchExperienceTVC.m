//
//  RequiredMatchExperienceTVC.m
//  Phenom
//
//  Created by James Chung on 4/4/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "RequiredMatchExperienceTVC.h"

@interface RequiredMatchExperienceTVC ()
@property (nonatomic, strong) NSMutableArray *experiecesAllowed;
@property (nonatomic) NSMutableString *experienceCode;
@property (nonatomic) NSInteger checkMarkCount;

@end

#define NUM_CELLS 5

@implementation RequiredMatchExperienceTVC

-(NSMutableArray *)experiecesAllowed
{
    if (!_experiecesAllowed) _experiecesAllowed = [[NSMutableArray alloc] init];
    return _experiecesAllowed;
}

-(NSMutableString *)experienceCode
{
    if (!_experienceCode) _experienceCode = [[NSMutableString alloc] init];
    return _experienceCode;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}



- (IBAction)done:(id)sender
{
    // need to iterate through the cells and add to experiencesAllowed array
    NSInteger counter = 0;
    for (int i = 0; i < NUM_CELLS; i++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
        
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        
        // first check if all is selected
        if (cell.accessoryType == UITableViewCellAccessoryCheckmark) {
            if (counter == 0)
                [self.experienceCode appendFormat:@"%d", i];
            else
                [self.experienceCode appendFormat:@"-%d", i];
           
            [self.experiecesAllowed addObject:cell.textLabel.text];
            
            NSLog (@"exp code: %d", i);
            NSLog (@"exp string: %@", cell.textLabel.text);
            
            if (i == 0) // all is selected so no need to add more into array or experienceCode
                break;
            counter ++;
            
        }
    }
    
    [self.delegate setMatchExperienceViewController:self withExperience:self.experiecesAllowed andCode:self.experienceCode];
    [self.navigationController popViewControllerAnimated:YES];
    

    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *expCell = [self.tableView cellForRowAtIndexPath:indexPath];
    expCell.accessoryType = UITableViewCellAccessoryCheckmark;
    /*
    NSInteger counter = 0;
    for (int i = 0; i < NUM_CELLS; i++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
        
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        
        // first check if all is selected
        if (cell.accessoryType == UITableViewCellAccessoryCheckmark) {
            if (counter == 0)
                [self.experienceCode appendFormat:@"%d", i];
            else
                [self.experienceCode appendFormat:@"-%d", i];
            
            [self.experiecesAllowed addObject:cell.textLabel.text];
            
            NSLog (@"exp code: %d", i);
            NSLog (@"exp string: %@", cell.textLabel.text);
            
            if (i == 0) // all is selected so no need to add more into array or experienceCode
                break;
            counter ++;
            
        }
    }
    */
    self.experienceCode =  [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@"%ld", indexPath.row]];
    [self.experiecesAllowed addObject:expCell.textLabel.text];
    
    [self.delegate setMatchExperienceViewController:self withExperience:self.experiecesAllowed andCode:self.experienceCode];
    [self.navigationController popViewControllerAnimated:YES];
    
    /*
    
    if (expCell.accessoryType != UITableViewCellAccessoryCheckmark) {
        if (indexPath.row == 0) { // the All choice
            [self applyMarkToAllCells:UITableViewCellAccessoryCheckmark];
            self.checkMarkCount = 4;
        } else {
            expCell.accessoryType = UITableViewCellAccessoryCheckmark;
            self.checkMarkCount ++;
            if (self.checkMarkCount == 4)
                [self selectAllRow:UITableViewCellAccessoryCheckmark];
        }
    } else {
        if (indexPath.row == 0) { // the All choice
            [self applyMarkToAllCells:UITableViewCellAccessoryNone];
            self.checkMarkCount = 0;
        } else {
            expCell.accessoryType = UITableViewCellAccessoryNone;
            self.checkMarkCount --;
            if (self.checkMarkCount == 3)
                [self selectAllRow:UITableViewCellAccessoryNone];
        }
    }
    
    */
    
    
    
}

- (void)applyMarkToAllCells:(NSInteger)mark
{
    
    for (int i = 0; i < NUM_CELLS; i++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
        
      
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        cell.accessoryType = mark;
    }
    
    
}

- (void) selectAllRow:(NSInteger)mark
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryType = mark;
    
}

@end
