//
//  ChooseYearTVC.h
//  Vaiden
//
//  Created by James Chung on 5/8/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "CustomBaseTVC.h"

@class ChooseYearTVC;

@protocol ChooseYearTVC_Delegate <NSObject>
- (void)setWithViewController:(ChooseYearTVC *)controller withYear:(NSString *)year;

@end

@interface ChooseYearTVC : CustomBaseTVC

@property (nonatomic, weak) id <ChooseYearTVC_Delegate> delegate;
@property (nonatomic, strong) NSString *chosenYear;



@end
