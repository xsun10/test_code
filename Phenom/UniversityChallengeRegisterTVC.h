//
//  UniversityChallengeRegisterTVC.h
//  Vaiden
//
//  Created by James Chung on 2/28/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface UniversityChallengeRegisterTVC : UITableViewController <MBProgressHUDDelegate>
{
    MBProgressHUD *HUD;
}
@end
