//
//  ExploreUsersCVC.m
//  Vaiden
//
//  Created by Turbo on 7/25/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "ExploreUsersCVC.h"
#import "NewsfeedPaginator.h"
#import "ExploreUserCVCell.h"
#import "LockerPost.h"
#import "S3Tools2.h"
#import "UIImageView+AFNetworking.h"
#import "LockerPhotoDetailTVC.h"
#import "IOSRequest.h"
#import "UserPreferences.h"

@interface ExploreUsersCVC () <NMPaginatorDelegate, UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, strong) NewsfeedPaginator *newsfeedPaginator;
@property (nonatomic, strong) NSArray *result;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewHeight;
@property (nonatomic, strong) UIRefreshControl *refreshControl;
@property (nonatomic) NSInteger selectedLockerID;
@property (nonatomic, strong) UserPreferences *preferences;
@end

@implementation ExploreUsersCVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
    self.modalPresentationStyle = UIModalPresentationCurrentContext;
    self.tabBarController.modalPresentationStyle = UIModalPresentationCurrentContext;
    // set up the paginator
    self.newsfeedPaginator = [[NewsfeedPaginator alloc] initWithPageSize:12 delegate:self];
    [self refreshView:nil];
    //[self.newsfeedPaginator fetchFirstPageWithTab:NEWSFEED_BROWSE_SEGMENT andNewsfeedDetailID:0];
    
    // set delegate for collection view
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.scrollEnabled = YES;
    self.collectionView.alwaysBounceVertical = YES;
    self.collectionView.bounces = YES;
    [self.collectionViewHeight setConstant:[[UIScreen mainScreen] bounds].size.height - 143];
    
    // set pull down refresh
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(refreshView:) forControlEvents:UIControlEventValueChanged];
    [self.collectionView addSubview:self.refreshControl];
    [self firstlogin];
}

/* Override 3 functions to detect the shake gesture */
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self becomeFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self resignFirstResponder];
    [super viewWillDisappear:animated];
    self.navigationItem.title = @"DISCOVER";
}

- (BOOL)canBecomeFirstResponder {
    return YES;
}

/* Method to catch the shake gesture and bind function */
- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    if (motion == UIEventSubtypeMotionShake)
    {
        [self refreshView:nil];
    }
}

- (void)fetchNextPage
{
    [self.newsfeedPaginator fetchNextPage];
    if (self.newsfeedPaginator.page > 0) {
        HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        HUD.labelText = @"Loading...";
    }
}

- (void)paginator:(id)paginator didReceiveResults:(NSArray *)results
{
    [self.collectionView reloadData];
    [self.refreshControl endRefreshing];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    // Delay 10s to dismiss the overlay
    double delayInSeconds = 10.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        //code to be executed on the main queue after delay
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    });
}

-(void)refreshView:(UIRefreshControl *)refresh
{
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.labelText = @"Loading...";
    [self.newsfeedPaginator.results removeAllObjects];
    [self.newsfeedPaginator fetchFirstPageWithTab:NEWSFEED_BROWSE_SEGMENT andNewsfeedDetailID:0];
}

#pragma - mark collection veiw data source
- (NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    // Ignore the AdUnit
    NSInteger total = [self.newsfeedPaginator.results count] - 1;
    NSInteger sections = total / 3;
    if (total % 3 > 0) {
        return sections+1;
    } else {
        return sections;
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSInteger total = [self.newsfeedPaginator.results count] - 1;
    NSInteger sections = total / 3;
    if (total % 3 > 0) {
        if (section == sections) {
            return total % 3;
        } else {
            return 3;
        }
    } else {
        return 3;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ExploreUserCVCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"exploreCell" forIndexPath:indexPath];
    
    [cell.image setContentMode:UIViewContentModeScaleAspectFill];
    // Ignore AdUnit
    LockerPost *iPost = (LockerPost *)self.newsfeedPaginator.results[indexPath.section*3 + indexPath.row+1];
    NSString *urlPostPic = [S3Tools2 getFileNameStringWithType:@"photo_regsize" baseString:[NSString stringWithFormat:@"%ld/photo/%@", (long)iPost.newsMakerUserID, iPost.lockPic.imageFileName]];
    [cell.image setImageWithURL:[NSURL URLWithString:urlPostPic] placeholderImage:[UIImage imageNamed:@"Landscape Placeholder"]];
    
    return cell;
}

#pragma -mark collection view delegate
- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGSize size = CGSizeMake(104, ([[UIScreen mainScreen] bounds].size.height - 125)/4-4);
    return size;
}
- (void) firstlogin
{
    [IOSRequest getLoginNums:[self.preferences getUserID]
                onCompletion:^(NSDictionary *results) {
                    NSLog(@"%@",results);
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if([results[@"num"] isEqualToString:@"1"]){
                            [self performSegueWithIdentifier:@"coach_segue_discover" sender:self];
                            
                        }
                        
                    });
                }];
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    LockerPost *iPost = (LockerPost *)self.newsfeedPaginator.results[indexPath.section*3 + indexPath.row+1];
    self.selectedLockerID = iPost.lockPic.lockerID;
    [self performSegueWithIdentifier:@"locker_detail_from_discover" sender:self];
}

#pragma - mark scroll view
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGPoint offset = scrollView.contentOffset;
    CGRect bounds = scrollView.bounds;
    CGSize size = scrollView.contentSize;
    UIEdgeInsets inset = scrollView.contentInset;
    float y = offset.y + bounds.size.height - inset.bottom;
    float h = size.height;
    NSLog(@"%f - %f",y,h);
    if((int)y >= (int)h) {
        [self fetchNextPage];
    }
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"locker_detail_from_discover"]) {
        LockerPhotoDetailTVC *controller = segue.destinationViewController;
        controller.lockerID = self.selectedLockerID;
        controller.fromExplore = YES;
    }
    if([segue.identifier isEqualToString:@"coach_segue_discover"])  {
        id theSegue = segue.destinationViewController;
        [theSegue setValue:@"coach_discover" forKey:@"title"];
    }

}

- (IBAction)coach_mark:(id)sender {
    [self performSegueWithIdentifier:@"coach_segue_discover" sender:sender];
}
@end
