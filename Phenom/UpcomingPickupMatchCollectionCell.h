//
//  UpcomingPickupMatchCollectionCell.h
//  Phenom
//
//  Created by James Chung on 7/18/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UpcomingPickupMatchCollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *profilePic;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;

@end
