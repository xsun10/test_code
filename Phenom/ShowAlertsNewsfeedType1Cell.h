//
//  ShowAlertsNewsfeedMatch.h
//  Vaiden
//
//  Created by James Chung on 6/1/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShowAlertsNewsfeedType1Cell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *profilePic;
@property (weak, nonatomic) IBOutlet UILabel *alertLabel;

@end
