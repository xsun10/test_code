//
//  AddDirectIndividualMatchTVC.m
//  Phenom
//
//  Created by James Chung on 5/14/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "AddDirectIndividualMatchTVC.h"
#import "UserPreferences.h"
#import "IndividualMatch.h"
#import "IOSRequest.h"
#import "S3Tools.h"
#import "AFNetworking.h"
#import "AddIndividualMatchConfirmationVC.h"
#import "UIImage+ProportionalFill.h"
#import "UITextField+FormValidations.h"
#import "UIColor+VaidenColors.h"

@interface AddDirectIndividualMatchTVC ()
@property (weak, nonatomic) IBOutlet UIImageView *profilePic;
@property (weak, nonatomic) IBOutlet UILabel *challengeUserMessage;
@property (weak, nonatomic) IBOutlet UITextField *matchNameTextField;
@property (weak, nonatomic) IBOutlet UITableViewCell *sportCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *dateTimeCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *venueCell;
@property (weak, nonatomic) IBOutlet UISwitch *privatePublicSwitch;
@property (weak, nonatomic) IBOutlet UITextView *messageTextView;
@property (nonatomic, strong) UIButton *closeDatePickerButton;
@property (nonatomic, strong) UIView *closeDatePickerView;

@property (nonatomic, strong) UserPreferences *preferences;
@property (nonatomic, strong) IndividualMatch *individualMatch;

@property (nonatomic) UIDatePicker *datePicker;
@property (nonatomic) NSInteger datePickerHeight;
@property (nonatomic) BOOL datePickerIsShowing;
@property (weak, nonatomic) IBOutlet UILabel *sessionUserName;
@property (weak, nonatomic) IBOutlet UIImageView *sessionUserProfilePic;


@end

@implementation AddDirectIndividualMatchTVC

#define DATE_ROW 2

- (UIDatePicker *)datePicker
{
    if (!_datePicker) _datePicker = [[UIDatePicker alloc] init];
    return _datePicker;
}


- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (IndividualMatch *)individualMatch
{
    if (!_individualMatch) _individualMatch = [[IndividualMatch alloc] init];
    return  _individualMatch;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setTopSection];
    
    self.datePicker.datePickerMode = UIDatePickerModeDateAndTime;
    self.datePicker.maximumDate = [NSDate dateWithTimeIntervalSinceNow:7776000];
    self.datePicker.minimumDate = [NSDate date];
    self.datePickerHeight = 44;
    self.datePickerIsShowing = NO;
    
    // single tap gesture recognizer
//    UITapGestureRecognizer *tapGestureRecognize = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissDate)];
//    tapGestureRecognize.numberOfTapsRequired = 1;
//    [self.datePicker addGestureRecognizer:tapGestureRecognize];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
    //[self setDoneButton];
    
}

/*- (void)setDoneButton
{
    UIButton *btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 50, 21);
    btn.layer.borderColor = [UIColor whiteColor].CGColor;
    btn.layer.borderWidth = 1.0;
    
    [btn addTarget:self action:@selector(doneAction:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundColor:[UIColor clearColor]];
    [btn setTitle:@"DONE" forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-MediumItalic" size:12.0]];
    
    // Initialize UIBarbuttonitem...
    UIBarButtonItem *proe = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = proe;
}*/
/*
- (void)dismissDate
{
    [self.matchNameTextField resignFirstResponder];
    [self.messageTextView resignFirstResponder];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:2 inSection:0];
    UITableViewCell* cell = (UITableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    
    
   if (self.datePickerIsShowing) {
        [self performCloseCell:indexPath];
        self.datePickerIsShowing = NO;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
}
*/
- (void)setTopSection
{
    self.challengeUserMessage.text = self.usernameToChallenge;
    self.sessionUserName.text = [self.preferences getUserName];
    
    NSString *urlSessionUser = [S3Tools getFileNameStringWithType:@"thumbnail"
                                            baseString:[self.preferences getProfilePicString]];
    
    [self.sessionUserProfilePic setImageWithURL:[NSURL URLWithString:urlSessionUser]
                    placeholderImage:[UIImage imageNamed:@"avatar round"]];
    
    [UIImage makeRoundedImage:self.profilePic withRadius:35];
    
    
    NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                            baseString:self.challengeUserProfileBaseString];
        
    [self.profilePic setImageWithURL:[NSURL URLWithString:url]
                        placeholderImage:[UIImage imageNamed:@"avatar round"]];
    
    [UIImage makeRoundedImage:self.sessionUserProfilePic withRadius:35];

    
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"My Venue List"]) {
        VenuesTVC *controller = (VenuesTVC *)segue.destinationViewController;
        controller.delegate = self;
        controller.showChooseButton = YES;
        controller.showMakeTempVenueButtonOnSubsequentVC = YES;
        
    }  else if ([segue.identifier isEqualToString:@"Choose Common Sport Segue"]) {
        ChooseIndividualSportTVC *controller = (ChooseIndividualSportTVC *)segue.destinationViewController;
        controller.delegate = self;
        controller.competitorUserID = self.userIDToChallenge;
    } else if ([segue.identifier isEqualToString:@"Confirmation Segue"]) {
        AddIndividualMatchConfirmationVC *controller = segue.destinationViewController;
        controller.individualMatch = self.individualMatch;
    }
}



- (IBAction)clearMatchNameAction:(id)sender
{
    self.matchNameTextField.text = @"";
    [self.matchNameTextField resignFirstResponder];
}


- (IBAction)toggleSwitch:(id)sender
{
    self.individualMatch.private = self.privatePublicSwitch.on;
}

/*
// delegate method
- (void)setWithViewController:(DateTimePickerViewController *)controller withDate:(NSDate *)date
{
    self.individualMatch.dateTime = date;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM/dd/yyyy hh:mm a"];
    
    self.dateTimeCell.detailTextLabel.text = [formatter stringFromDate: date];
    
    
}*/

// venue delegate method
-(void)setVenueViewController:(VenuesTVC *)controller withVenue:(Venue *)venue
{
    self.individualMatch.venue = venue;
    self.venueCell.detailTextLabel.text = venue.venueName;
}


- (void)setWithSportFromVC:(ChooseIndividualSportTVC *)controller withSport:(Sport *)sport
{
    self.individualMatch.sport = sport;
    self.sportCell.detailTextLabel.text = [sport.sportName capitalizedString];
}

- (IBAction)doneAction:(id)sender
{
    [self.matchNameTextField resignFirstResponder];
    [self.messageTextView resignFirstResponder];
 
    if ([self fieldsAreOK]) {
        self.individualMatch.matchCreatorID = [self.preferences getUserID];
        self.individualMatch.matchName = self.matchNameTextField.text;
        self.individualMatch.message = self.messageTextView.text;
        
        MatchPlayer *player2 = [[MatchPlayer alloc] initWithPlayerDetails:self.userIDToChallenge andUserName:self.usernameToChallenge];
    
        self.individualMatch.player2 = player2;
    
        [self performSegueWithIdentifier:@"Confirmation Segue" sender:self];
    }
    
}

- (BOOL) fieldsAreOK
{
    if (![self.matchNameTextField checkMatchName]) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Match Name Error";
        alert.message = @"Please enter a match name between 3 - 50 letters in length.  Only limited punctuation marks are allowed.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
        
        return NO;
    }
    
    if ([self.messageTextView.text length] > 250) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Message Error";
        alert.message = @"Please enter a message that is less than 250 characters in length.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
        
        return NO;
    }
    
    UITableViewCell *sportCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    
    if ([sportCell.detailTextLabel.text isEqualToString:@"Detail"]) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Sport Selection Missing";
        alert.message = @"Please choose a sport for your challenge match.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
        
        return NO;
    }
    
    UITableViewCell *dateCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
    
    if ([dateCell.detailTextLabel.text isEqualToString:@"Detail"]) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Date Selection Missing";
        alert.message = @"Please choose a date for your challenge match.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
        
        return NO;
    }
    
    UITableViewCell *venueCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]];
    
    if ([venueCell.detailTextLabel.text isEqualToString:@"Detail"]) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Venue Selection Missing";
        alert.message = @"Please choose a venue for your challenge match.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
        
        return NO;
    }
    
    return YES;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [self.matchNameTextField resignFirstResponder];
    [self.messageTextView resignFirstResponder];
    UITableViewCell* cell = (UITableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    
    
    if (indexPath.row == DATE_ROW && !self.datePickerIsShowing) {
        
        cell.textLabel.text = @"";
        cell.detailTextLabel.text = @"";
        cell.accessoryType = UITableViewCellAccessoryNone;
        
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:DATE_ROW inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];

        self.datePicker.alpha = 0;
        [self.tableView beginUpdates];
        self.datePickerHeight = 44 + 190;
        
    
        [self.tableView endUpdates];
        
        
        [cell addSubview:self.datePicker];
        
        
        [UIView animateWithDuration:0.2 animations:^{
            self.datePicker.alpha = 1;
            self.datePickerIsShowing = YES;
            
            self.closeDatePickerView = [[UIView alloc] initWithFrame:CGRectMake(0, cell.frame.origin.y, 320, 40)];
            self.closeDatePickerView.backgroundColor = [UIColor darkGrayColor];
            self.closeDatePickerView.alpha = 0.5;
            
            self.closeDatePickerButton = [[UIButton alloc] initWithFrame:CGRectMake(260,  7, 50, 25)];
            [self.closeDatePickerButton setTitle:@"CLOSE" forState:UIControlStateNormal];
            [self.closeDatePickerButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-MediumItalic" size:11]];
            self.closeDatePickerButton.layer.borderColor = [UIColor lightGrayColor].CGColor;
            self.closeDatePickerButton.layer.borderWidth = 1.0;
            
            [self.closeDatePickerButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
            [self.closeDatePickerButton addTarget:self action:@selector(tapDate:) forControlEvents:UIControlEventTouchUpInside];
            
            [self.closeDatePickerView addSubview:self.closeDatePickerButton];
            [self.tableView addSubview:self.closeDatePickerView];

        } completion:nil];
        
        
    } else if (indexPath.row == DATE_ROW && self.datePickerIsShowing) {
   
    }
}

- (void)tapDate:(UIButton *)sender
{
    [self.matchNameTextField resignFirstResponder];
    [self.messageTextView resignFirstResponder];
    
    [UIView animateWithDuration:0.5 animations:^(void) {
        self.closeDatePickerView.alpha = 0.0;
    //    self.closeDatePickerButton.hidden = YES;
    //    self.closeDatePickerView.hidden = YES;
    //    self.closeDatePickerView = nil;
    //    self.closeDatePickerButton = nil;
        
        
        if (self.datePickerIsShowing) {
            [self performCloseCell:[NSIndexPath indexPathForRow:2 inSection:0]];
            self.datePickerIsShowing = NO;
        }
    }];
   
    
    
}

- (NSIndexPath *)tableView:(UITableView *)tableView willDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.closeDatePickerButton.hidden = YES;
    self.closeDatePickerView.hidden = YES;
    self.closeDatePickerView = nil;
    self.closeDatePickerButton = nil;
    
    
    [self performCloseCell:indexPath];
    self.datePickerIsShowing = NO;
    
    return indexPath;
}

- (void)performCloseCell:(NSIndexPath *)indexPath
{
    
    if (indexPath.row == DATE_ROW && self.datePickerIsShowing) {
        
        [UIView animateWithDuration:0.2 animations:^{self.datePicker.alpha = 0;} completion:^(BOOL finished){[self.datePicker removeFromSuperview];}];
        UITableViewCell *cell = (UITableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
        NSDate *myDate = self.datePicker.date;
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        //    [dateFormat setDateFormat:@"MMM d yyyy"];
        [dateFormat setDateFormat:@"MM/dd/yyyy hh:mm a"];
        cell.detailTextLabel.text = [dateFormat stringFromDate:myDate];
        cell.textLabel.text = @"Date";
        self.individualMatch.dateTime = myDate;
        
        
        [self.tableView beginUpdates];
        
        if (indexPath.row == DATE_ROW)
            self.datePickerHeight = 44;
        
        [self.tableView endUpdates];
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row == DATE_ROW && indexPath.section == 0) {
        return self.datePickerHeight;
    } else if (indexPath.row == 1 && indexPath.section == 1) {
        return 170;
    } else if (indexPath.row == 0 && indexPath.section == 1) {
        // Private Switch cell
        return 56;
    }
    
    return 44;
}

@end
