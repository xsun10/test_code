//
//  NewsTeamMatchPostCell.h
//  Vaiden
//
//  Created by James Chung on 4/16/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewsfeedMatchHeadingCV.h"

@class NewsTeamMatchPostCell;

@protocol NewsTeamMatchPostCell_delegate <NSObject>
-(void)setViewController:(NewsTeamMatchPostCell *)controller withUserIDToSegue:(NSInteger)userID;
@end


@interface NewsTeamMatchPostCell : UITableViewCell

@property (nonatomic, weak) id <NewsTeamMatchPostCell_delegate> delegate;


@property (weak, nonatomic) IBOutlet UIView *commentButtonBackground;
@property (weak, nonatomic) IBOutlet UIView *shareButtonBackground;
@property (weak, nonatomic) IBOutlet UIButton *commentButton;
@property (weak, nonatomic) IBOutlet UILabel *commentNumberLabel;
@property (weak, nonatomic) IBOutlet UIImageView *commentBubbleImage;
@property (weak, nonatomic) IBOutlet UIImageView *shareButtonImage;
@property (weak, nonatomic) IBOutlet UILabel *sharesNumberLabel;

@property (weak, nonatomic) IBOutlet UIView *vsCircle;
@property (weak, nonatomic) IBOutlet UIView *section2View;
@property (weak, nonatomic) IBOutlet UIView *section3View;

@property (weak, nonatomic) IBOutlet UIView *myBackgroundViewCell;


@property (weak, nonatomic) IBOutlet UILabel *timeFrameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *teamLogo1;
@property (weak, nonatomic) IBOutlet UIImageView *teamLogo2;
@property (weak, nonatomic) IBOutlet UILabel *teamNameLabel1;
@property (weak, nonatomic) IBOutlet UILabel *teamNameLabel2;

@property (weak, nonatomic) IBOutlet UIButton *sharesButton;
@property (weak, nonatomic) IBOutlet UILabel *sharedByLabel;
@property (weak, nonatomic) IBOutlet UILabel *matchNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *detailButton;
@property (weak, nonatomic) IBOutlet UILabel *headlineSportLabel;
@property (weak, nonatomic) IBOutlet UIImageView *headlineSportPic;
@property (weak, nonatomic) IBOutlet UILabel *matchStatus;
@property (weak, nonatomic) IBOutlet UIButton *actionButtonHandle;

@property (weak, nonatomic) IBOutlet UIView *likeButtonBackground;
@property (weak, nonatomic) IBOutlet UIImageView *likeIcon;
@property (weak, nonatomic) IBOutlet UILabel *likeNumberLabel;
@property (weak, nonatomic) IBOutlet UIButton *likeButton;
- (void)clearMyCV;

@end
