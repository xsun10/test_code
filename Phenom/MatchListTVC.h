//
//  UpcomingMatchesTVC.h
//  Phenom
//
//  Created by James Chung on 5/11/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomBaseTVC.h"
#import "PickupMatchListCell.h"

@interface MatchListTVC : CustomBaseTVC <PickupMatchListCell_delegate>


#define LOAD_UPCOMING_MATCHES 0
#define LOAD_TODAYS_MATCHES 1
#define LOAD_PAST_MATCHES 2
@property (nonatomic) NSUInteger loadMatchType;

@end
