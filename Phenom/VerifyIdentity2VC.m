//
//  VerifyIdentity2VC.m
//  Vaiden
//
//  Created by James Chung on 9/14/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "VerifyIdentity2VC.h"
#import "IOSRequest.h"
#import "UIButton+RoundBorder.h"
#import "UserPreferences.h"
#import "UIColor+VaidenColors.h"
#import "UIView+Manipulate.h"


@interface VerifyIdentity2VC ()
@property (weak, nonatomic) IBOutlet UIScrollView *pageScrollView;
@property (weak, nonatomic) IBOutlet UITextField *pinCodeEntry;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (nonatomic, strong) UserPreferences *preferences;
@property (weak, nonatomic) IBOutlet UIButton *resendCodeButton;
@property (weak, nonatomic) IBOutlet UIImageView *phoneIcon;
@property (weak, nonatomic) IBOutlet UIView *textBoxBackground;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UILabel *buttomLabel;

@end

@implementation VerifyIdentity2VC

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    // Add space between sentences for content label
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    [style setLineSpacing:5];
    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithString:@"Step 2: Enter the PIN that received via text message. If you did not receive one, click ‘Resends'."
                                                                   attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],
                                                                                NSParagraphStyleAttributeName:style}];
    self.contentLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:13];
    [self.contentLabel setAttributedText:attrStr];
    self.contentLabel.textAlignment = NSTextAlignmentCenter;
    
    // Add space between lines for the detail content
    [style setLineSpacing:4];
    attrStr = [[NSAttributedString alloc] initWithString:@"The PIN number should be a four digit code. If you did not receive a message, press back and check that the phone number you have entered is correct."
                                              attributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:119/255 green:119/255 blue:119/255 alpha:1.0],
                                                           NSParagraphStyleAttributeName:style}];
    self.buttomLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:13];
    [self.buttomLabel setAttributedText:attrStr];
    
    [self.submitButton makeRoundedBorderWithRadius:3];
    [self.resendCodeButton makeRoundedBorderWithRadius:3];
    self.phoneIcon.image = [[UIImage imageNamed:@"iPhone Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.phoneIcon setTintColor:[UIColor whiteColor]];
 //   [self.textBoxBackground makeRoundedBorderWithColor:[UIColor lightLightGray]];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self.pinCodeEntry addTarget:self action:@selector(textViewDidBeginEditing) forControlEvents:UIControlEventEditingDidBegin];
    self.pageScrollView.contentSize = CGSizeMake(320.0, 570.0);
}

- (void)textViewDidBeginEditing
{
    [self.pageScrollView setContentOffset:CGPointMake(0, 120 ) animated:YES];
}

- (IBAction)submit:(id)sender
{
    HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    HUD.labelText = @"Verifying";
    
    self.submitButton.enabled = NO;
    [self.pinCodeEntry resignFirstResponder];
    [IOSRequest verifyPin:[self.pinCodeEntry.text integerValue]
                   ofUser:[self.preferences getUserID]
             onCompletion:^(NSDictionary *results) {
                 dispatch_async(dispatch_get_main_queue(), ^ {
                     if ([results[@"outcome"] isEqualToString:@"Success"]) {
                    //     UIAlertView *alert = [[UIAlertView alloc] init];
                    //     alert.title = @"Account Verified";
                    //     alert.message = @"You may now participate in Challenge Matches.";
                    //     [alert addButtonWithTitle:@"OK"];
                    //     [alert show];
                      
                         HUD.labelText = @"Account Verified";
                         [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
                         [self.preferences setVerifiedState:YES];
                         [self dismissViewControllerAnimated:YES completion:nil];
                         self.submitButton.enabled = YES;
                         
                     } else {
                         [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
                         UIAlertView *alert = [[UIAlertView alloc] init];
                         alert.title = @"Pin Code Error";
                         alert.message = @"The pin code you entered was not valid.  Please try again.";
                         [alert addButtonWithTitle:@"OK"];
                         [alert show];
                         self.submitButton.enabled = YES;
                     }
                 });
                 
                 
             }];
    double delayInSeconds = 10.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        //code to be executed on the main queue after delay
        [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
    });
}
- (IBAction)exit:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)resendCode:(id)sender
{
    HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    HUD.labelText = @"Resending";
    [IOSRequest verifyPhoneNumber:self.userPhoneNumber
                           ofUser:[self.preferences getUserID]
                     onCompletion:^(NSDictionary *results) {
                         
                         dispatch_async(dispatch_get_main_queue(), ^{
                            [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
                         });
                         
                     }];
}
- (IBAction)dismissKeyboard:(id)sender
{
    [self.pinCodeEntry resignFirstResponder];
}

@end
