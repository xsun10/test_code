//
//  UniversityRegistrationSuccessVC.m
//  Vaiden
//
//  Created by James Chung on 3/1/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "UniversityRegistrationSuccessVC.h"

@interface UniversityRegistrationSuccessVC ()
@property (nonatomic, strong) UIBarButtonItem *doneButton;
@property (weak, nonatomic) IBOutlet UIImageView *headlinePic;
@property (weak, nonatomic) IBOutlet UIScrollView *pageScrollView;

@end

@implementation UniversityRegistrationSuccessVC



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    [self.navigationItem setHidesBackButton:YES animated:YES];

    //[self setDoneButton];
    
    self.headlinePic.contentMode = UIViewContentModeScaleAspectFill;
    self.headlinePic.clipsToBounds = YES;
    
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    self.pageScrollView.contentSize = CGSizeMake(320.0, 570.0);
}


/*- (void)setDoneButton
{
    UIButton *btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 50, 21);
    btn.layer.borderColor = [UIColor whiteColor].CGColor;
    btn.layer.borderWidth = 1.0;
    
    [btn addTarget:self action:@selector(doneAction) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundColor:[UIColor clearColor]];
    [btn setTitle:@"DONE" forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-MediumItalic" size:12.0]];
    
    // Initialize UIBarbuttonitem...
    self.doneButton = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = self.doneButton;
}*/

- (IBAction)doneAction:(id)sender
{
    
//    NSInteger currentIndex = [self.navigationController.viewControllers indexOfObject:self];
    
 //   [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:currentIndex-2] animated:YES];
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}

@end
