//
//  PickupMatchesNeedingActionsCell.h
//  Vaiden
//
//  Created by James Chung on 10/14/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PickupMatchesNeedingActionsCell : UITableViewCell <UICollectionViewDataSource, UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *sportIcon;
@property (weak, nonatomic) IBOutlet UILabel *matchHeading;
@property (weak, nonatomic) IBOutlet UILabel *sessionUserStatus;
@property (weak, nonatomic) IBOutlet UILabel *matchStatus;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *playerCV;
@property (weak, nonatomic) IBOutlet UILabel *experienceLabel;
@property (weak, nonatomic) IBOutlet UILabel *genderLabel;
@property (weak, nonatomic) IBOutlet UILabel *sportLabel;
@property (weak, nonatomic) IBOutlet UIImageView *triangleIcon;
@property (weak, nonatomic) IBOutlet UIView *barBackground;
@property (weak, nonatomic) IBOutlet UIView *iconBackgroundView;
@property (weak, nonatomic) IBOutlet UILabel *matchCategoryLabel;
@property (weak, nonatomic) IBOutlet UIImageView *starIcon;

@property (nonatomic, strong) NSMutableArray *competitors;

@end
