//
//  AddVenueIntroVC.h
//  Vaiden
//
//  Created by James Chung on 12/5/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddTemporaryVenueTVC.h"
#import "AddVenueTVC.h"
#import "AddVenueWithoutAddressTVC.h"
#import "CustomBaseVC.h"

@class AddVenueIntroVC;

@protocol AddVenueIntroVC_Delegate <NSObject>

- (void)setViewController:(AddVenueIntroVC *)controller withVenueID:(NSInteger)venueID andVenueName:(NSString *)venueName;

@end

@interface AddVenueIntroVC : CustomBaseVC <AddTemporaryVenueTVC_Delegate, AddVenueTVC_Delegate, AddVenueWithoutAddressTVC_Delegate>

@property (nonatomic, weak) id <AddVenueIntroVC_Delegate>delegate;
@property (nonatomic) BOOL isCreatingMatch;

@end


