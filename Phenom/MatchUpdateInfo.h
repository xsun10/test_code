//
//  MatchUpdateInfo.h
//  Vaiden
//
//  Created by James Chung on 10/10/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Player.h"

@interface MatchUpdateInfo : NSObject

@property (nonatomic) NSUInteger alerts_id;
@property (nonatomic) NSDate *postDate;
@property (nonatomic, strong) Player *postingPlayer;
@property (nonatomic, strong) NSString *matchName;
@property (nonatomic) NSUInteger matchID;
@property (nonatomic, strong) NSString *matchCategory;
@property (nonatomic, strong) NSString *alertText;


- (MatchUpdateInfo *) initWithMatchUpdateInfo:(NSInteger)alertsID
                                withAlertText:(NSString *)alertText
                                       onDate:(NSDate *)postDate
                               postedByPlayer:(Player *)postingPlayer
                                     forMatch:(NSInteger)matchID
                                withMatchName:(NSString *)matchName
                             forMatchCategory:(NSString *)matchCategory;


@end
