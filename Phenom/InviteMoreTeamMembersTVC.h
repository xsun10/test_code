//
//  InviteMoreTeamMembersTVC.h
//  Vaiden
//
//  Created by James Chung on 4/9/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "CustomBaseTVC.h"
#import "MBProgressHUD.h"

@interface InviteMoreTeamMembersTVC : CustomBaseTVC <MBProgressHUDDelegate>
{
    MBProgressHUD *HUD;
}

@property (nonatomic) NSInteger teamID;

@end
