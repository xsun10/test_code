//
//  DisputeIndMatchScoreIntroMessageVC.m
//  Phenom
//
//  Created by James Chung on 8/1/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "DisputeIndMatchScoreIntroMessageVC.h"
//#import "RecordIndividualMatchScoreTVC.h"
#import "UserPreferences.h"
#import "UIView+Manipulate.h"
#import "UIColor+VaidenColors.h"
#import "UIImage+ImageEffects.h"

@interface DisputeIndMatchScoreIntroMessageVC () 
@property (weak, nonatomic) IBOutlet UIScrollView *pageScrollView;
@property (nonatomic, strong) UserPreferences *preferences;
@property (weak, nonatomic) IBOutlet UIImageView *disputeIcon;
@property (weak, nonatomic) IBOutlet UIView *subBackground1;
@property (weak, nonatomic) IBOutlet UIButton *inputScoreButtonHandle;
@property (weak, nonatomic) IBOutlet UIImageView *blurredBackgroundImage;

@end

@implementation DisputeIndMatchScoreIntroMessageVC

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.pageScrollView setContentInset:UIEdgeInsetsMake(44, 0, 74, 0)];
    
    [self setPageBlurredBackgroundImage:1];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
    
    self.disputeIcon.image = [[UIImage imageNamed:@"Fist Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.disputeIcon setTintColor:[UIColor whiteColor]];
    
    [self.subBackground1 makeRoundedBorderWithColor:[UIColor whiteColor]];
    [self.inputScoreButtonHandle makeRoundedBorderWithColor:[UIColor sienna]];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.navigationItem setHidesBackButton:YES animated:YES];

}
- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    self.pageScrollView.contentSize = CGSizeMake(320.0, 620.0);
    
}

// might have to make a copy of individual match...otehrwise, if pop back up to parent view controller (don't actually want to
// dispute score and want to go back), the individual match object score data will be incorrect

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
/*    if ([segue.identifier isEqualToString:@"Enter New Score Segue"]) {
        self.individualMatch.scoreIndMatch.version = 2; // new version
        self.individualMatch.scoreIndMatch.scoreRecordedBy = [self.preferences getUserID];
        self.individualMatch.scoreIndMatch.scoreConfirmedBy = 0;
        self.individualMatch.scoreIndMatch.scoreDisputedBy = 0;
        [self.individualMatch.scoreIndMatch.roundScoresIndMatch removeAllObjects];
        
 //       RecordIndMatchScoreTVC *controller = segue.destinationViewController;
     //   controller.individualMatch = self.individualMatch;
   //     controller.matchID = self.individualMatch.matchID;
        
    }*/
}

- (IBAction)inputScoreAction:(id)sender
{
    [self performSegueWithIdentifier:@"Enter New Score Segue" sender:self];
}

- (void)setPageBlurredBackgroundImage:(NSUInteger)imageIndex
{
    NSString *effectText = @"";
    UIImage *effectImage = nil;
    UIImage *backImage = [UIImage imageNamed:@"BBall Player Background"];
    
    effectImage = backImage;
    
    switch (imageIndex)
    {
        case 0:
            effectImage = backImage;
            break;
        case 1:
            effectImage = [backImage applyLightEffect];
            effectText = NSLocalizedString(@"Light", @"");
            break;
        case 2:
            effectImage = [backImage applyExtraLightEffect];
            effectText = NSLocalizedString(@"Extra light", @"");
            break;
        case 3:
            effectImage = [backImage applyDarkEffect];
            effectText = NSLocalizedString(@"Dark", @"");
            break;
        case 4:
            effectImage = [backImage applyTintEffectWithColor:[UIColor blueColor]];
            effectText = NSLocalizedString(@"Color tint", @"");
            break;
    }
    
    self.blurredBackgroundImage.image = effectImage;
    
    
}

@end
