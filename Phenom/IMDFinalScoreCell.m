//
//  IMDFinalScoreCell.m
//  Vaiden
//
//  Created by James Chung on 12/26/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "IMDFinalScoreCell.h"
#import "IndividualMatchRoundScore.h"
#import "IMDFinalScoreCVCell.h"

@implementation IMDFinalScoreCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews
{
    self.scoreCV.delegate = self;
    self.scoreCV.dataSource = self;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.individualMatch.scoreIndMatch.roundScoresIndMatch count];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 2;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"My Cell";
    
    IMDFinalScoreCVCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    IndividualMatchRoundScore *matchScore = [self.individualMatch.scoreIndMatch.roundScoresIndMatch objectAtIndex:indexPath.row];
    
    if (indexPath.section == 0) { // first section
        if (self.player1ID == matchScore.userIDPlayer1)
            cell.scoreLabel.text = [NSString stringWithFormat:@"%ld", matchScore.scorePlayer1];
        else
            cell.scoreLabel.text = [NSString stringWithFormat:@"%ld", matchScore.scorePlayer2];
        
    } else if (indexPath.section == 1) { // second section
        if (self.player2ID == matchScore.userIDPlayer2)
            cell.scoreLabel.text = [NSString stringWithFormat:@"%ld", matchScore.scorePlayer2];
        else
            cell.scoreLabel.text = [NSString stringWithFormat:@"%ld", matchScore.scorePlayer1];
    }
    
    return cell;
}


@end
