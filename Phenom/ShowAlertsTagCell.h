//
//  ShowAlertsTagCell.h
//  Vaiden
//
//  Created by James Chung on 5/16/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlayerAlertTag.h"


@class ShowAlertsTagCell;

@protocol ShowAlertsTagCell_Delegate <NSObject>
- (void)setWithLockerIDFrom:(ShowAlertsTagCell *)cell withLockerPic:(LockerPic *)lp;

@end

@interface ShowAlertsTagCell : UITableViewCell <UICollectionViewDataSource, UICollectionViewDelegate>


@property (nonatomic, weak) id <ShowAlertsTagCell_Delegate> delegate;


@property (weak, nonatomic) IBOutlet UILabel *alertLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *imageCollectionView;
@property (weak, nonatomic) IBOutlet UIImageView *profilePic;
@property (nonatomic, strong) PlayerAlertTag *pa;
@property (nonatomic) NSInteger creatorID;
- (void)myInit;
@end
