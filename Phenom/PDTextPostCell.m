//
//  PDTextPostCell.m
//  Vaiden
//
//  Created by James Chung on 2/23/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "PDTextPostCell.h"

@implementation PDTextPostCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
