//
//  TheLatestNewsCell.h
//  Vaiden
//
//  Created by James Chung on 12/26/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IMDTheLatestNewsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *newsIconBackground;
@property (weak, nonatomic) IBOutlet UIImageView *newsIconPic;

@property (weak, nonatomic) IBOutlet UIImageView *picImage;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *subheadlineLabel;
@property (weak, nonatomic) IBOutlet UILabel *datetimeLabel;
@end
