//
//  FBShareRequestDialog.m
//  Vaiden
//
//  Created by Turbo on 7/11/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "FBShareRequestDialog.h"
#import "UIView+Manipulate.h"

@interface FBShareRequestDialog ()
@property (weak, nonatomic) IBOutlet UIImageView *imagePreview;
@property (weak, nonatomic) IBOutlet UIButton *postButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;

@property (weak, nonatomic) IBOutlet UIView *window;
@end

@implementation FBShareRequestDialog

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.window makeRoundedBorderWithRadius:5];
    [self.imagePreview makeRoundedBorderWithRadius:5];
    [self.postButton makeRoundedBorderWithRadius:5];
    [self.cancelButton makeRoundedBorderWithRadius:5];
    
    [self.imagePreview setImage:self.image];
    [self.imagePreview setContentMode:UIViewContentModeScaleAspectFit];
}

- (IBAction)postClicked:(UIButton *)sender {
    [self.delegate startShareImage];
    [self dismissViewControllerAnimated:NO completion:nil];
    
}

- (IBAction)cancelClicked:(UIButton *)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
