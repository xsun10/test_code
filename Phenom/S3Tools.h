//
//  S3Tools.h
//  Phenom
//
//  Created by James Chung on 5/24/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <AWSS3/AWSS3.h>
#import <AWSRuntime/AWSRuntime.h>

@interface S3Tools : NSOperation <AmazonServiceRequestDelegate>

@property (nonatomic, strong) NSData *fileData;
@property (nonatomic) NSInteger userID;
@property (nonatomic, strong) NSString *dataType;
@property (nonatomic, strong ) NSString *fileNameBase;
@property (nonatomic, strong) NSString *textPostString;
@property (nonatomic, strong) NSString *fileExtension;
@property (nonatomic) BOOL isNewsfeedPost;
@property (nonatomic) NSUInteger venueID;

@property (nonatomic, retain) AmazonS3Client *s3;

// for profile pic uploads
-(id)initWithData:(NSData *)theData
          forUser:(NSInteger)userID
         dataType:(NSString *)dataType
andBaseFileNameString:(NSString *)fileNameBaseString
andFileExt:(NSString *)fileExtension;

// for newsfeed posts
-(id)initWithData:(NSData *)theData
          forUser:(NSInteger)userID
         dataType:(NSString *)dataType
andBaseFileNameString:(NSString *)fileNameBaseString
andTextPostString:(NSString *)textPostString
andFileExt:(NSString *)fileExtension;


// for Venue pic uploads
-(id)initWithData:(NSData *)theData
         forVenue:(NSInteger)venueID
         dataType:(NSString *)dataType
andBaseFileNameString:(NSString *)fileNameBaseString
       andFileExt:(NSString *)fileExtension;



+ (NSString *)getBaseBucketURLForDataType:(NSString *)dataType;

+ (NSString *)getFileNameBaseStringWithType:(NSString *)dataType forUser:(NSInteger)userID ;

+ (NSString *)getFileNameBaseStringWithType:(NSString *)dataType forVenue:(NSInteger)venueID;

+ (NSString *)getFileNameStringWithType:(NSString *)dataType
                             baseString:(NSString *)baseFileStringIn;

@end
