//
//  MyHelpers.m
//  Phenom
//
//  Created by James Chung on 5/1/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "MyHelpers.h"
#import "NSDate+Utilities.h"

@implementation MyHelpers

+ (NSDate *)getDateTimeFromString:(NSString *)dateTimeString
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *dateTime = [[formatter dateFromString:dateTimeString] toLocalTime];
    
    return dateTime;
}


// for individual matches
+ (NSMutableArray *)makeCompetitorArray:(NSArray *)competitorObjList forSportName:(NSString *)sportName andType:(NSString *)sportType
{
    // competitorResults should be an array of arrays
    NSMutableArray *competitorInfoSet = [[NSMutableArray alloc] init];
    
    for (id object in competitorObjList) {
        NSMutableDictionary *innerInfo = [[NSMutableDictionary alloc] init];
        
        Player *player = [[Player alloc] initWithPlayerDetails:[object[@"competitor_id"] integerValue]
                                                 andProfileStr:object[@"profile_pic_string"]
                                                   andUserName:object[@"username"]
                                                   andFullName:object[@"fullname"]
                                                       andLocation:object[@"location"]
                                                      andAbout:object[@"about"]
                                                      andCoins:[object[@"coins"] floatValue]];
    
        // Rather than add all sports of the competitor, we are going to add just the sport that is
        // relevant to this match.  In this case, it is an individual sport
        
        PlayerSport *sport = [[PlayerSport alloc] initWithSportDetails:sportName
                                                               andType:sportType
                                                              andLevel:object[@"sport_level"]
                                                        andSportRecord:object[@"win_loss_record"]];
        [player.playerSports addObject:sport];
        
        
        /***********************************************/
        [innerInfo setValue:player forKey:@"player"];
        [innerInfo setValue:object[@"status"] forKey:@"status"];
        /**********************************************/
        
        [competitorInfoSet addObject:innerInfo];
    }
    return competitorInfoSet;
}

+ (NSMutableArray *)getConfirmedList:(id)confirmedObjectList
{
    NSMutableArray *resultConfirmedList = [[NSMutableArray alloc] init];
    
    for (id object in confirmedObjectList) {
        [resultConfirmedList addObject:object];
    }
    return resultConfirmedList;
}

+ (NSMutableArray *)makeExperienceArray:(NSArray *)experienceObjectList
{
    NSMutableArray *experienceLevels = [[NSMutableArray alloc] init];
    
    for (id object in experienceObjectList) {
        [experienceLevels addObject:object];
    }
    
    return  experienceLevels;
}

+ (NSString *)getParsedWinRecordForString:(NSString *)winLossRecord
{
    if ([winLossRecord length] == 0) return @"";
    
    NSMutableArray *substrings = [[NSMutableArray alloc] init];
    NSScanner *scanner = [NSScanner scannerWithString:winLossRecord];
    
//    while(![scanner isAtEnd]) {
        NSString *substring = nil;
        if([scanner scanUpToString:@"W" intoString:&substring]) {
            [substrings addObject:substring];
        }
 //   }
    if ([substrings count] > 0) {
        return substrings[0];
    }
    return @"";
}

+ (NSString *)getParsedLossRecordForString:(NSString *)winLossRecord
{
    if ([winLossRecord length] == 0) return @"";
    
    NSMutableArray *substrings = [[NSMutableArray alloc] init];
    NSScanner *scanner = [NSScanner scannerWithString:winLossRecord];
    
    [scanner scanUpToString:@"-" intoString:nil]; // Scan all characters before -
   // while(![scanner isAtEnd]) {
        NSString *substring = nil;
        [scanner scanString:@"-" intoString:nil]; // Scan the - character
        [scanner scanString:@" " intoString:nil]; // Scan the " " character
        if([scanner scanUpToString:@"L" intoString:&substring]) {
            [substrings addObject:substring];
        }
  //  }
    if ([substrings count] > 0) {
        return substrings[0];
    }
    return @"";
}

+ (NSString *)getNewPicURLIfFacebookSquareSize:(NSString *)originalURL
{

    if ([originalURL rangeOfString:@"facebook"].location != NSNotFound) {
        // must be facebook string
        if ([originalURL rangeOfString:@"square"].location != NSNotFound) {
            return [originalURL stringByReplacingOccurrencesOfString:@"square"
                                                          withString:@"large"];
        }
    }

    return originalURL;
}

@end
