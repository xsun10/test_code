//
//  ChooseSportToRateCell.h
//  Vaiden
//
//  Created by James Chung on 12/27/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChooseSportToRateCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *sportIcon;
@property (weak, nonatomic) IBOutlet UILabel *sportLabel;

@end
