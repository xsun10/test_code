//
//  PMDMatchCompetitorsCell.h
//  Vaiden
//
//  Created by James Chung on 1/17/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PickupMatch.h"


@class PMDMatchCompetitorsCell;

@protocol PMDMatchCompetitorsCell_delegate <NSObject>
-(void)setPMDMatchCompetitorsCellViewController:(PMDMatchCompetitorsCell *)controller withUserIDToSegue:(NSInteger)userID;
@end


@interface PMDMatchCompetitorsCell : UITableViewCell <UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, weak) id <PMDMatchCompetitorsCell_delegate> delegate;


@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (nonatomic, strong) PickupMatch *pickupMatch;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UILabel *statsLabel;
@property (nonatomic) NSInteger sessionUserID;
- (void)initPlayers;
@property (weak, nonatomic) IBOutlet UIView *myBackgroundView;

@end