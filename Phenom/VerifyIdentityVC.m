//
//  VerifyIdentityVC.m
//  Vaiden
//
//  Created by James Chung on 9/14/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "VerifyIdentityVC.h"
#import "IOSRequest.h"
#import "UserPreferences.h"
#import "UIButton+RoundBorder.h"
#import "UIColor+VaidenColors.h"
#import "VerifyIdentity2VC.h"
#import "UIView+Manipulate.h"

@interface VerifyIdentityVC ()
@property (weak, nonatomic) IBOutlet UITextField *phoneInput;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet UIButton *skipButton;
@property (weak, nonatomic) IBOutlet UIScrollView *pageScrollview;
@property (weak, nonatomic) IBOutlet UIView *pageView;
@property (nonatomic, strong) UserPreferences *preferences;
@property (weak, nonatomic) IBOutlet UIImageView *phoneIcon;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;

@end

@implementation VerifyIdentityVC


- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    // Add space between sentences for content label
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    [style setLineSpacing:5];
    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithString:@"Step 1: We’ll send a pin code via text to your phone. Enter the code on the next page. That’s it!"
                                                                   attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],
                                                                                NSParagraphStyleAttributeName:style}];
    self.contentLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:13];
    [self.contentLabel setAttributedText:attrStr];
    self.contentLabel.textAlignment = NSTextAlignmentCenter;
    
    [self.submitButton makeRoundedBorderWithRadius:3];
    self.phoneIcon.image = [[UIImage imageNamed:@"iPhone Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.phoneIcon setTintColor:[UIColor whiteColor]];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
     [self.phoneInput addTarget:self action:@selector(textViewDidBeginEditing) forControlEvents:UIControlEventEditingDidBegin];
    self.pageScrollview.contentSize = CGSizeMake(320.0, 650.0);
}

- (IBAction)skip:(id)sender
{
  //  [self.navigationController popToRootViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)submit:(id)sender
{
    if ([self isPhoneNumberOK]) {
        self.submitButton.enabled = NO;
        [self.phoneInput resignFirstResponder];
        HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        HUD.labelText = @"Submitting";
        [IOSRequest verifyPhoneNumber:self.phoneInput.text
                               ofUser:[self.preferences getUserID]
                         onCompletion:^(NSDictionary *results) {
                         
                         if ([results[@"outcome"] isEqualToString:@"Number Exists Already"]) {
                             
                             dispatch_async(dispatch_get_main_queue(), ^ {
                                 [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
                                 UIAlertView *alert = [[UIAlertView alloc] init];
                                 alert.title = @"Duplicate Phone Number";
                                 alert.message = @"That phone number already exists in our system.  Please try again with a different number.";
                                 [alert addButtonWithTitle:@"OK"];
                                 [alert show];
                                 self.submitButton.enabled = YES;
                                 
                             });
                             
                             
                         } else {
                             dispatch_async(dispatch_get_main_queue(), ^ {
                                 [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
                                 [self performSegueWithIdentifier:@"Pin Segue" sender:self];
                                 self.submitButton.enabled = YES;
                                 
                             });
                         }
                     }];
        double delayInSeconds = 10.0;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            //code to be executed on the main queue after delay
            [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
        });
    }

}

// for now just allowing for usa
- (BOOL)isPhoneNumberOK
{
    if ([self.phoneInput.text length] != 10) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Phone Number Error";
        alert.message = @"Please enter a valid USA phone number including the area code.  Please note, it is not necessary to input a 1 before the area code.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
        return NO;
    }
    return YES;
}

- (IBAction)onTouch:(id)sender
{
    [self textViewDidBeginEditing];
}

- (void)textViewDidBeginEditing
{
    [self.pageScrollview setContentOffset:CGPointMake(0, 120 ) animated:YES];
}

- (void)textViewDidEndEditing
{
    [self.pageScrollview setContentOffset:CGPointMake(0, 0 ) animated:YES];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Pin Segue"]) {
        VerifyIdentity2VC *controller = (VerifyIdentity2VC *)segue.destinationViewController;
        controller.userPhoneNumber = self.phoneInput.text;
    }
}
- (IBAction)dismissKeyboard:(id)sender
{
    [self.phoneInput resignFirstResponder];
}
@end
