
//
//  NewsFeedTVC.m
//  Phenom
//
//  Created by James Chung on 4/23/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "NewsFeedTVC.h"
#import "UserPreferences.h"
#import "IOSRequest.h"
#import "PickupMatchCreationNews.h"
#import "IndividualMatchCreationNews.h"
#import "TeamMatchCreationNews.h"
#import "PickupMatchDetailTVC.h"
#import "MyHelpers.h"
#import "S3Tools.h"
#import "AFNetworking.h"
#import "ImagePost.h"
#import "ImagePostCell.h"
//#import "VideoPost.h"
//#import "VideoPostCell.h"
#import "ShowVideoVC.h"
#import "UIImage+VidTools.h"
#import "UIImage+ProportionalFill.h"
#import "TextPostCell.h"
#import "TextPost.h"
#import "UIButton+RoundBorder.h"
#import "UIView+Manipulate.h"
#import "NSDate+Utilities.h"
#import "UIColor+VaidenColors.h"
#import "MatchNews.h"
#import "IndividualMatchDetailTVC.h"
#import "IndividualMatchRoundScore.h"
#import "MatchPlayerNewsOfficialFinishedIndividualMatch.h"
#import <FacebookSDK/FacebookSDK.h>
//#import <Social/Social.h>
#import "PlayerDetailTVC.h"
#import "SpecialUIButton.h"
#import "SpecialUILabel.h"
#import "MKNumberBadgeView.h"
#import "AdUnit.h"
#import "NewsfeedAdvertisementCell.h"
#import "CompetitionNews.h"
#import "CompetitionNewsCell.h"
#import "VenueKingCell.h"
#import "VenueKingNews.h"
#import "VenueDetailTVC.h"
#import "NewsTeamMatchPostCell.h"
#import "TeamDetailTVC.h"
#import "VenueCheckinNews.h"
#import "VenueCheckIn.h"
#import "VenueCheckinCell.h"
#import "S3Tools2.h"
#import "LockerPost.h"
#import "StatTag.h"
#import "TrainingTag.h"
#import "LockerPhotoDetailTVC.h"
#import "ShowLikesTVC.h"
#import "StatsGroup.h"
#import "StatTagCombination.h"
#import "FBShareRequestDialog.h"
#import "VaidenImageObject.h"
#import "AmazonClientManager.h"
#import "AddLockerPicturePreStepTVC.h"
#import "CommentDialog.h"
#import "VoteReceiving.h"
#import "VoteCell.h"
#import "VoteWinnerCell.h"

@interface NewsFeedTVC () <UIActionSheetDelegate, FBShareRequestDialogDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, CommentDialogDelegate>
@property (nonatomic, strong) UserPreferences *preferences;
//@property (nonatomic, strong) NSMutableArray *newsPosts;

@property (nonatomic) NSInteger selectedSection; // only used for Individual Match News right now.

@property (nonatomic, strong) UILabel *errorLabel;

@property (nonatomic) BOOL displayTableOK;
@property (nonatomic, strong) UIRefreshControl *refreshControl;
@property (nonatomic) BOOL noResultsToDisplay;

@property (nonatomic) NSInteger remixedNewsID;
@property (nonatomic, strong) NSIndexPath *remixedIndexPath;

@property (nonatomic, strong) NSString *fbMessage;
@property (nonatomic, strong) NSURL *fbImage;
@property (nonatomic, strong) NSString *fbTitle;
@property (nonatomic, strong) UIView *tempView;
@property (nonatomic, strong) UIActivityIndicatorView *onLoadActivityIndicator;

@property (nonatomic) NSInteger selectedPlayerToShowDetailPage;
@property (nonatomic) NSInteger selectedVenue;
@property (nonatomic, strong) UILabel *tempLabel;
@property (nonatomic, strong) UILabel *tempLabel2;
@property (nonatomic, strong) UIImageView *tempBrowseArrow;
@property (nonatomic, strong) UIImageView *noNewsfeed;

@property (nonatomic, strong) NSString *fbImageString;
@property (weak, nonatomic) IBOutlet UISegmentedControl *newsSegmentControl;
@property (nonatomic) NSInteger selectedLockerID;
@property (nonatomic) BOOL showKeyboardOnCommentsPage;
@property (assign) NSInteger currentSegment;
@property (nonatomic, strong) NSArray *stats;
@property (nonatomic, strong) NSArray *drills;
@property (nonatomic) BOOL isLockerShare;

@property (nonatomic, strong) UIImage * currentPickedImage;

@property (strong, nonatomic)UIPopoverController *imagePickerPopover;
@property (weak, nonatomic) UIActionSheet *choosePicActionSheet;
@property (nonatomic, strong) VaidenImageObject *viObject;

@property (nonatomic, strong) NSString *imageFileName;
@property (nonatomic, strong) NSString *imageFileNameBaseString;
@property (nonatomic, strong) S3Tools2 *regImageSizeUploader;
@property (nonatomic, strong) S3Tools2 *regImageRetinaSizeUploader;
@property (nonatomic, strong) S3Tools2 *thumbnailImageSizeUploader;
@property (nonatomic, strong) S3Tools2 *thumbnailImageRetinaSizeUploader;
@end


@implementation NewsFeedTVC

#define PICKUP_MATCH_CREATION 1
#define INDIVIDUAL_MATCH_CREATION 2
#define INDIVIDUAL_MATCH_CONFIRMATION 3
#define INDIVIDUAL_MATCH_RESULT 4
#define USER_TEXT_POST 5
#define USER_TEXT_POST_WITH_IMAGE 6

#define CELL_CONTENT_WIDTH 275
#define CELL_CONTENT_MARGIN 22.5
#define FONT_SIZE 13 // is actually 13
#define HEIGHT_OF_REST_OF_CELL 370
#define HEIGHT_OF_REST_OF_CELL_NO_IMAGE 174 // new height of text post, was 208

#define TITLE_OF_ACTIONSHEET @"Add A Pic To Your Locker"
#define CANCEL_BUTTON_ACTIONSHEET @"Cancel "
#define FROM_CAMERA @"Take A Picture"
#define FROM_LIBRARY @"Choose From Photo Library"
/*
#define SEGMENT_MY_NEWS 0
#define SEGMENT_BROWSE 1
*/
/*
 bundle id com.vaiden.vaidenapp
 
 jc.Turbo.${PRODUCT_NAME:rfc1034identifier}
 */



- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
    self.modalPresentationStyle = UIModalPresentationCurrentContext;
    self.tabBarController.modalPresentationStyle = UIModalPresentationCurrentContext;
    
    if (![self.preferences getHasMatchNotificationsPageBeenLoadedAtLeastOnce]) {
        [self.tabBarController setSelectedIndex:1];
    }
    
    UIImage *image = [UIImage imageNamed:@"no_newsfeed"];
    self.noNewsfeed = [[UIImageView alloc] initWithFrame:CGRectMake(130,110,50,50)];
    self.noNewsfeed.image = image;
    self.noNewsfeed.contentMode = UIViewContentModeCenter;
    
    self.tempLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, 180, 240, 21)];
    self.tempLabel.text = @"No News Feed Posts Yet";
    self.tempLabel.textColor = [UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:1.0];
    self.tempLabel.textAlignment = NSTextAlignmentCenter;
    self.tempLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:15];

    self.tempLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(20, 200, 260, 42)];
    self.tempLabel2.text = @"Until then, click ‘Browse’ to view featured news feed posts.";
    self.tempLabel2.textColor = [UIColor colorWithRed:170/255.0 green:170/255.0 blue:170/255.0 alpha:1.0];
    self.tempLabel2.textAlignment = NSTextAlignmentCenter;
    self.tempLabel2.font = [UIFont fontWithName:@"HelveticaNeue" size:13];
    [self.tempLabel2 setNumberOfLines:0];
    
    self.tempBrowseArrow = [[UIImageView alloc] initWithFrame:CGRectMake(190, 5, 50, 50)];
    self.tempBrowseArrow.image = [UIImage imageNamed:@"Browse Arrow"];
    
    [self.view addSubview:self.noNewsfeed];
    [self.view addSubview:self.tempLabel];
    [self.view addSubview:self.tempLabel2];
    [self.view addSubview:self.tempBrowseArrow];

    self.noNewsfeed.hidden = YES;
    self.tempLabel.hidden = YES;
    self.tempLabel2.hidden = YES;
    self.tempBrowseArrow.hidden = YES;
    

    [self.preferences setNewsfeedRefreshState:NO];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];

    //self.newsSegmentControl.selectedSegmentIndex = NEWSFEED_MYNEWS_SEGMENT;
   
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];

    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(refreshView:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshControl];
    
    [self setupTableViewFooter];
    
    [self showTempSpinner];

    // set up the paginator
    self.newsfeedPaginator = [[NewsfeedPaginator alloc] initWithPageSize:10 delegate:self];
//    [self.newsfeedPaginator fetchFirstPage];
    if (self.newsfeedDetailID > 0) {
        //self.newsSegmentControl.hidden = YES;
        [self.newsfeedPaginator fetchFirstPageWithTab:NEWSFEED_DETAIL_POST andNewsfeedDetailID:self.newsfeedDetailID];
    } else {
        //self.newsSegmentControl.hidden = NO;
        [self.newsfeedPaginator fetchFirstPageWithTab:NEWSFEED_MYNEWS_SEGMENT andNewsfeedDetailID:self.newsfeedDetailID];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if ([self.preferences getNewsfeedRefreshState] == YES) {
        [self.newsfeedPaginator.results removeAllObjects];
//        [self.newsfeedPaginator fetchFirstPage];
        
        if (self.newsfeedDetailID > 0) {
            //self.newsSegmentControl.hidden = YES;
            [self.newsfeedPaginator fetchFirstPageWithTab:NEWSFEED_DETAIL_POST andNewsfeedDetailID:self.newsfeedDetailID];
        } else {
            //self.newsSegmentControl.hidden = NO;
             [self.newsfeedPaginator fetchFirstPageWithTab:NEWSFEED_MYNEWS_SEGMENT andNewsfeedDetailID:self.newsfeedDetailID];
            [self.preferences setNewsfeedRefreshState:NO];
        }
        
    }
 }


- (void)fetchNextPage
{
    if (self.newsfeedDetailID == 0) {
        [self.newsfeedPaginator fetchNextPage];

        if (self.newsfeedPaginator.page > 0) {
            [self.activityIndicator startAnimating];
        }
    }
}

- (void)showTempSpinner
{
    self.tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    self.tempView.backgroundColor = [UIColor clearColor];
    self.tableView.scrollEnabled = NO;
    
    self.onLoadActivityIndicator = [[UIActivityIndicatorView alloc] init];
    [self.onLoadActivityIndicator setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width/2.0, [UIScreen mainScreen].bounds.size.height/2.0 - 50)];
    [self.onLoadActivityIndicator startAnimating];
    [self.onLoadActivityIndicator setColor:[UIColor darkGrayColor]];
    [self.tempView addSubview:self.onLoadActivityIndicator];
    
    [self.tableView addSubview:self.tempView];
    
    
       double delayInSeconds = 15.0;
       dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
       dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
    //code to be executed on the main queue after delay
           [self.tempView removeFromSuperview];
           self.tempView.hidden = YES;
           self.tableView.scrollEnabled = YES;
      });
}


- (void)setupTableViewFooter
{
    // set up label
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    footerView.backgroundColor = [UIColor clearColor];
    
    // set up activity indicator
    UIActivityIndicatorView *activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityIndicatorView.center = CGPointMake(160, 22);
    activityIndicatorView.hidesWhenStopped = YES;
    
    self.activityIndicator = activityIndicatorView;
    [footerView addSubview:activityIndicatorView];
    
    self.tableView.tableFooterView = footerView;
}



- (void)paginator:(id)paginator didReceiveResults:(NSArray *)results
{
    // update tableview footer
    [self.activityIndicator stopAnimating];

    
    /****ADDED FOR TEMP MESSAGE***/
    if ([self.newsfeedPaginator.results count] == 1) { // it's 1 because there is an ad unit
        self.noNewsfeed.hidden = NO;
        self.tempLabel.hidden = NO;
        self.tempLabel2.hidden = NO;
        self.tempBrowseArrow.hidden = NO;
    } else {
        self.noNewsfeed.hidden = YES;
        self.tempLabel.hidden = YES;
        self.tempLabel2.hidden = YES;
        self.tempBrowseArrow.hidden = YES;
    }
    /*******************************/
    
    
    // update tableview content
    // easy way : call [tableView reloadData];
    // nicer way : use insertRowsAtIndexPaths:withAnimation:
    NSMutableArray *indexPaths = [[NSMutableArray alloc] init];
    NSInteger i = [self.newsfeedPaginator.results count] - [results count];
    
    for(NSDictionary *result in results)
    {
        [indexPaths addObject:[NSIndexPath indexPathForRow:0 inSection:i]];
        i++;
    }

    
    if ([self.newsfeedPaginator.results count] > 0)
        self.noResultsToDisplay = NO;
    else
        self.noResultsToDisplay = YES;
    
    [self.tableView reloadData];
    [self.onLoadActivityIndicator stopAnimating];
    self.tableView.scrollEnabled = YES;
    
    self.tempView.hidden = YES;
    //self.newsSegmentControl.userInteractionEnabled = YES;
 //   if (self.scrollToSection > 0) {
 //       [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:self.scrollToSection]
 //                             atScrollPosition:UITableViewScrollPositionTop animated:YES];
 //   }
 //   self.scrollToSection = 0;

}

- (void)paginatorDidReset:(id)paginator
{
    [self.tableView reloadData];
    
}

- (void)paginatorDidFailToRespond:(id)paginator
{
    // Todo
}



-(void)refreshView:(UIRefreshControl *)refresh
{
    self.errorLabel.hidden = YES;
    self.displayTableOK = NO;
    [self.newsfeedPaginator.results removeAllObjects];
//    [self.newsfeedPaginator fetchFirstPage];
      //  [self.newsfeedPaginator fetchFirstPageWithTab:self.newsSegmentControl.selectedSegmentIndex andNewsfeedDetailID:self.newsfeedDetailID];
    if (self.currentSegment == NEWSFEED_MYNEWS_SEGMENT) {
        if (self.newsfeedDetailID > 0) {
            self.newsSegmentControl.hidden = YES;
            [self.newsfeedPaginator fetchFirstPageWithTab:NEWSFEED_DETAIL_POST andNewsfeedDetailID:self.newsfeedDetailID];
        } else {
            self.newsSegmentControl.hidden = NO;
            [self.newsfeedPaginator fetchFirstPageWithTab:NEWSFEED_MYNEWS_SEGMENT andNewsfeedDetailID:self.newsfeedDetailID];
        }
    } /*else if (self.currentSegment == NEWSFEED_BROWSE_SEGMENT) {
        [self.newsfeedPaginator fetchFirstPageWithTab:NEWSFEED_BROWSE_SEGMENT andNewsfeedDetailID:self.newsfeedDetailID];
    }*/
    
    
    [self showTempSpinner];
     //   self.newsfeedDetailID = 0;
    [refresh endRefreshing];
    
}


- (void)endRefreshWhenSynced
{
        dispatch_async(dispatch_get_main_queue(), ^{
 //           [self.newsPosts sortUsingDescriptors:
 //            @[[NSSortDescriptor sortDescriptorWithKey:@"newsDate" ascending:NO]]];
            [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
            self.displayTableOK = YES;
            
            [self.tableView reloadData];
            
        });
    
}

- (IBAction)changeNewsSegment:(UISegmentedControl *)sender
{
    self.newsSegmentControl.userInteractionEnabled = NO;
    [self.newsfeedPaginator.results removeAllObjects];
    if (sender.selectedSegmentIndex == NEWSFEED_MYNEWS_SEGMENT) {
        //[self.tableView reloadData];
        [self.newsfeedPaginator fetchFirstPageWithTab:NEWSFEED_MYNEWS_SEGMENT andNewsfeedDetailID:self.newsfeedDetailID];
        //[self.tableView reloadData];
        self.newsfeedDetailID = 0;
        [self.preferences setNewsfeedRefreshState:NO];
        self.currentSegment = NEWSFEED_MYNEWS_SEGMENT;
    }/* else if (sender.selectedSegmentIndex == NEWSFEED_BROWSE_SEGMENT) {
        //[self.newsfeedPaginator.results removeAllObjects];
        //[self.tableView reloadData];
        [self.newsfeedPaginator fetchFirstPageWithTab:NEWSFEED_BROWSE_SEGMENT andNewsfeedDetailID:self.newsfeedDetailID];
        //[self.tableView reloadData];
        [self.preferences setNewsfeedRefreshState:NO];
        self.newsfeedDetailID = 0;
        self.currentSegment = NEWSFEED_BROWSE_SEGMENT;
    }*/
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (self.noResultsToDisplay)
        return 1;
    else
        return [self.newsfeedPaginator.results count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
 //   if ([self.newsfeedPaginator.results count] == 0 || self.displayTableOK == NO)
 //       return 0;
 //   else
        return 1;
}

- (void)goToComments:(UIButton *)sender
{
    
}

- (void)goToProfile:(UIButton *)sender
{
    self.selectedPlayerToShowDetailPage = sender.tag;
    [self performSegueWithIdentifier:@"Player Detail Segue" sender:self];
}

- (void)goToVenue:(UIButton *)sender
{
    self.selectedVenue = sender.tag;
    [self performSegueWithIdentifier:@"Venue Detail Segue" sender:self];
}

- (void)setViewController:(NewsIndividualMatchPostCell *)controller withUserIDToSegue:(NSInteger)userID
{
    self.selectedPlayerToShowDetailPage = userID;
    [self performSegueWithIdentifier:@"Player Detail Segue" sender:self];
}

- (void)setMatchNewsFinalScoreUpdateCellParentViewController:(MatchNewsFinalScoreUpdateCell *)controller withUserIDToSegue:(NSInteger)userID
{
    self.selectedPlayerToShowDetailPage = userID;
    [self performSegueWithIdentifier:@"Player Detail Segue" sender:self];
}

- (void)setMatchNewsScoreUpdateCellParentViewController:(MatchNewsScoreUpdateCell *)controller withUserIDToSegue:(NSInteger)userID
{
    self.selectedPlayerToShowDetailPage = userID;
    [self performSegueWithIdentifier:@"Player Detail Segue" sender:self];
}

- (void)setMatchNewsPlayerUpdateCellParentViewController:(MatchNewsPlayerUpdateCell *)controller withUserIDToSegue:(NSInteger)userID
{
    self.selectedPlayerToShowDetailPage = userID;
    [self performSegueWithIdentifier:@"Player Detail Segue" sender:self];
}

- (void)setMatchNewsMatchUpdateCellParentViewController:(MatchNewsMatchUpdateCell *)controller withUserIDToSegue:(NSInteger)userID
{
    self.selectedPlayerToShowDetailPage = userID;
    [self performSegueWithIdentifier:@"Player Detail Segue" sender:self];
}

- (void)setNewsPickupMatchPostCellViewController:(NewsPickupMatchPostCell *)controller withUserIDToSegue:(NSInteger)userID
{
    self.selectedPlayerToShowDetailPage = userID;
    [self performSegueWithIdentifier:@"Player Detail Segue" sender:self];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0 || section == 1)
        return 8;
    return 15;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
  if (section == 0)
      return 1;
    
    return 15;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //NSLog(@"This is a %@ cell", [self.newsfeedPaginator.results objectAtIndex:self.newsfeedPaginator.results.count - 1]);
    if (self.noResultsToDisplay) {
        NSString *CellIdentifier = @"Default Cell";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        cell.textLabel.font = [UIFont systemFontOfSize:14];
        cell.textLabel.text = @"You do not have any newsfeed posts yet";
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.textLabel.textColor = [UIColor lightGrayColor];
        cell.textLabel.hidden = YES;
        
        return cell;
    }
    if ([(self.newsfeedPaginator.results)[indexPath.section] isKindOfClass:[AdUnit class]]) {
        NewsfeedAdvertisementCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Ad Cell" forIndexPath:indexPath];
        cell.adImage.contentMode = UIViewContentModeScaleAspectFill;
        cell.adImage.clipsToBounds = YES;

        return cell;
        
    } else if ([(self.newsfeedPaginator.results)[indexPath.section] isKindOfClass:[PickupMatchCreationNews class]]) {
        static NSString *CellIdentifier = @"Pickup Match News Cell";
        PickupMatchCreationNews *pickupMatchNews = (PickupMatchCreationNews *)(self.newsfeedPaginator.results)[indexPath.section];

        NewsPickupMatchPostCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        
        // clear subviews
        if ([cell.contentView subviews]){
            for (UIView *subview in [cell.contentView subviews]) {
                if ([subview isKindOfClass:[SpecialUIButton class]]) {
                    SpecialUIButton * newSubview = (SpecialUIButton *)subview;
                    if (newSubview.shouldRemoveFromMySuperview)  // only items I created programmatically
                        [subview removeFromSuperview];
                    
                } else if ([subview isKindOfClass:[SpecialUILabel class]]) {
                    SpecialUILabel *newSubview = (SpecialUILabel *)subview;
                    if (newSubview.shouldRemoveFromMySuperview)
                        [subview removeFromSuperview];
                }
                
            }
        }
        
    //    [cell.detailButtonView makeRoundedBorderWithColor:[UIColor midGray2]];

        [cell.actionButtonHandle addTarget:self action:@selector(postExtraActions:) forControlEvents:UIControlEventTouchUpInside];
        cell.actionButtonHandle.tag = indexPath.section;
        
        cell.sportIcon.image = [[pickupMatchNews.pickupMatch.sport getSportIcon2] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [cell.sportIcon setTintColor:[UIColor darkGrayColor2]];
        cell.sportLabel.text = [[NSString stringWithFormat:@"Pickup %@", pickupMatchNews.pickupMatch.sport.sportName] uppercaseString];
        [cell.sportIconBackground makeRoundedBorderWithRadius:10];
        
        cell.sportIconDetail.image = [[pickupMatchNews.pickupMatch.sport getSportIcon] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [cell.sportIconDetail setTintColor:[UIColor lightGrayColor]];
        cell.sportLabel.text = [pickupMatchNews.pickupMatch.sport.sportName capitalizedString];
        
        cell.matchName.text = pickupMatchNews.pickupMatch.matchName;
        
        cell.usernameLabel.text = pickupMatchNews.newsMakerUserName;
        
        if ([[pickupMatchNews.pickupMatch getMatchStatus] length] > 0) {
            cell.starIcon.hidden = NO;
            cell.messageLabel.text = [pickupMatchNews.pickupMatch getMatchStatus];
        } else {
            cell.messageLabel.text = @"";
            cell.starIcon.hidden = YES;
        }
        
        NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                baseString:pickupMatchNews.profileBaseString];
        
        [cell.profilePic setImageWithURL:[NSURL URLWithString:url]
                          placeholderImage:[UIImage imageNamed:@"avatar square"]];
        [UIImage makeRoundedImage:cell.profilePic withRadius:23];
        [cell.profilePic.layer setBorderWidth:2.0f];
        [cell.profilePic.layer setBorderColor:[UIColor whiteColor].CGColor];
        
        cell.delegate = self;
        
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"MMM d"];
        
        NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
        [timeFormatter setDateFormat:@"h:mm a"];
  
        cell.dateLabel.text = [NSString stringWithFormat:@"%@", [dateFormatter stringFromDate:pickupMatchNews.pickupMatch.dateTime]];
        cell.timeLabel.text = [NSString stringWithFormat:@"%@", [timeFormatter stringFromDate:pickupMatchNews.pickupMatch.dateTime]];
        
        cell.postTime.text = [NSDate timeDiffCalc:pickupMatchNews.newsDate];
        
        cell.venueLabel.text = pickupMatchNews.pickupMatch.venue.venueName;

        cell.detailButton.tag = indexPath.section;
        [cell.detailButton addTarget:self action:@selector(matchDetailsForPickupMatchCreation:) forControlEvents:UIControlEventTouchUpInside];
       
        UIButton *newsfeedIconButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
        [newsfeedIconButton addTarget:self action:@selector(matchDetailsForPickupMatchCreation:) forControlEvents:UIControlEventTouchUpInside];
        newsfeedIconButton.tag = indexPath.section;
        [cell.newsfeedIcon addSubview:newsfeedIconButton];
        cell.newsfeedIcon.userInteractionEnabled = YES;
        
        
        
        if ([pickupMatchNews.sharedByPlayer.userName length] > 0) {
            cell.sharedByLabel.text = [NSString stringWithFormat:@"Shared by %@", pickupMatchNews.sharedByPlayer.userName];
        } else {
            cell.sharedByLabel.text = [NSString stringWithFormat:@"Created by %@", pickupMatchNews.pickupMatch.matchCreatorUsername];
        }
        
   /*     if ([pickupMatchNews.pickupMatch.experienceArray count] > 0)
            cell.experienceLabel.text = [NSString stringWithFormat:@"%@ Experience", [[pickupMatchNews.pickupMatch.experienceArray componentsJoinedByString:@", "] capitalizedString]];
        else
            cell.experienceLabel.text = @"Any Experience";
    */
        cell.competitors = [[NSMutableArray alloc] initWithArray:pickupMatchNews.pickupMatch.competitorArray];
        cell.matchCategory = @"pickup";
        [cell.matchHeadingCV reloadData];
        
        cell.genderLabel.text = [pickupMatchNews.pickupMatch.gender capitalizedString];
        cell.genderIcon.image = [[Player getIconForGender:pickupMatchNews.pickupMatch.gender] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [cell.genderIcon setTintColor:[UIColor midGray]];
        
        if (pickupMatchNews.pickupMatch.private == YES) {
            cell.publicPrivateMessageLabel.text = @"This match is private.  Only invited players can join.";
        } else {
            cell.publicPrivateMessageLabel.text = @"This match is open for others to join";
        }
        
        /*************** SHOW MATCH HISTORY ********************/
  /*      NSInteger maxVisibleMatchHistory;
        
        if ([pickupMatchNews.pickupMatch.matchHistoryArray count] > 3)
            maxVisibleMatchHistory = 3;
        else
            maxVisibleMatchHistory = [pickupMatchNews.pickupMatch.matchHistoryArray count];
        
        NSInteger startPoint = 325;
        
        NSInteger myCounter = 0;
        for (MatchHistoryObj *matchHist in pickupMatchNews.pickupMatch.matchHistoryArray) {
            SpecialUILabel *updateLine = [[SpecialUILabel alloc] initWithFrame:CGRectMake(20, startPoint, 250, 15)];
            
            if ([matchHist.participant.userName isKindOfClass:[NSNull class]]) {
                NSString *firstCapChar = [[matchHist.update substringToIndex:1] capitalizedString];
                NSString *cappedString = [matchHist.update stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:firstCapChar];
                updateLine.text = [NSString stringWithFormat:@"%@", cappedString];
            }   else
                updateLine.text = [NSString stringWithFormat:@"%@ %@", matchHist.participant.userName, matchHist.update];
            
            updateLine.font = [UIFont systemFontOfSize:12];
            updateLine.textColor = [UIColor lightGrayColor];
            updateLine.shouldRemoveFromMySuperview = YES;
            [cell.contentView sendSubviewToBack:updateLine];
            [updateLine sizeToFit];
            
            [cell.contentView addSubview:updateLine];
            
            SpecialUILabel *timeLabel = [[SpecialUILabel alloc] initWithFrame:CGRectMake(updateLine.frame.size.width + 30, startPoint, 20, 15)];
            timeLabel.text = [NSDate timeDiffCalc:matchHist.occurrenceDate];
            timeLabel.font = [UIFont fontWithName:@"HelveticaNeue-LightItalic" size:12];
            timeLabel.textColor = [UIColor lightGrayColor];
            [timeLabel setTextAlignment:NSTextAlignmentLeft];
            [timeLabel sizeToFit];
            timeLabel.shouldRemoveFromMySuperview = YES;
            [cell.contentView sendSubviewToBack:timeLabel];
            [cell.contentView addSubview:timeLabel];
            
            startPoint += 20;
            myCounter ++;
            
            if (myCounter == maxVisibleMatchHistory)
                break;
            
            
        }*/
        
        /*************** END SHOW MATCH HISTORY ********************/
        
    //    cell.contentView.frame = CGRectMake(0, 0, self.tableView.frame.size.width, [self tableView:tableView heightForRowAtIndexPath:indexPath]);

        
    
        // Show comment button
        
        [cell.commentButton addTarget:self action:@selector(commentPost:) forControlEvents:UIControlEventTouchUpInside];
        cell.commentButton.tag = indexPath.section;
        cell.commentNumberLabel.text = [NSString stringWithFormat:@"%lu comments", pickupMatchNews.numComments];
        if (pickupMatchNews.numComments == 1)
            cell.commentTextLabel.text = @"Comment";
        else
            cell.commentTextLabel.text = @"Comments";
        
        [cell.likeButton addTarget:self action:@selector(likePost:) forControlEvents:UIControlEventTouchUpInside];
        cell.likeButton.tag = indexPath.section;
        cell.likeNumberLabel.text = [NSString stringWithFormat:@"%lu props", pickupMatchNews.numLikes];
        
        cell.commentButtonIcon.image = [[UIImage imageNamed:@"Comment Small Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [cell.commentButtonIcon setTintColor:[UIColor darkGrayColor2]];
   
        
        cell.likeIcon.image = [[UIImage imageNamed:@"Like Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        
        if (pickupMatchNews.doesSessionUserLikePost)
            [cell.likeIcon setTintColor:[UIColor goldColor]];
        else
            [cell.likeIcon setTintColor:[UIColor darkGrayColor2]];
        
        
        [cell.cellMainBackgroundView makeRoundedBorderWithRadius:3];
        cell.cellMainBackgroundView.layer.borderColor = [UIColor midGray2].CGColor;
        cell.cellMainBackgroundView.layer.borderWidth = 1.0;

        [cell.smallCircleView makeRoundedBorderWithRadius:2];
        cell.smallCircleView.backgroundColor = [UIColor darkGrayColor2];
        cell.commentNumberLabel.textColor = [UIColor darkGrayColor2];
        cell.likeNumberLabel.textColor = [UIColor darkGrayColor2];
        
        cell.viewLikesButton.tag = indexPath.section;
        cell.viewCommentsButton.tag = indexPath.section;
        
        [cell.viewLikesButton addTarget:self action:@selector(viewLikes:) forControlEvents:UIControlEventTouchUpInside];
        [cell.viewCommentsButton addTarget:self action:@selector(viewComments:) forControlEvents:UIControlEventTouchUpInside];

        
        return cell;
        
    }  else if ([(self.newsfeedPaginator.results)[indexPath.section] isKindOfClass:[IndividualMatchCreationNews class]]) {
        
        IndividualMatchCreationNews *individualMatchNews = (IndividualMatchCreationNews *)(self.newsfeedPaginator.results)[indexPath.section];
        NSLog(@"%@",(self.newsfeedPaginator.results)[indexPath.section]);
        NSString *CellIdentifier = @"";
        
        if (individualMatchNews.postType == INDIVIDUAL_MATCH_CREATION || individualMatchNews.postType == INDIVIDUAL_MATCH_CONFIRMATION) {
            CellIdentifier = @"Individual Match Creation News Cell";
        
            NewsIndividualMatchPostCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];

            // clear subviews
            if ([cell.contentView subviews]){
                for (UIView *subview in [cell.contentView subviews]) {
                    if ([subview isKindOfClass:[SpecialUIButton class]]) {
                       SpecialUIButton * newSubview = (SpecialUIButton *)subview;
                        if (newSubview.shouldRemoveFromMySuperview)  // only items I created programmatically
                            [subview removeFromSuperview];

                    } else if ([subview isKindOfClass:[SpecialUILabel class]]) {
                        SpecialUILabel *newSubview = (SpecialUILabel *)subview;
                        if (newSubview.shouldRemoveFromMySuperview)
                            [subview removeFromSuperview];
                    }
                   
                }
            }
            cell.matchNameLabel.text = individualMatchNews.individualMatch.matchName;
            
            [cell.actionButtonHandle addTarget:self action:@selector(postExtraActions:) forControlEvents:UIControlEventTouchUpInside];
            cell.actionButtonHandle.tag = indexPath.section;
            
            cell.headlineSportPic.image = [[individualMatchNews.individualMatch.sport getSportIcon2] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [cell.headlineSportPic setTintColor:[UIColor darkGrayColor2]];
            cell.headlineSportLabel.text = [[NSString stringWithFormat:@"%@ Challenge", individualMatchNews.individualMatch.sport.sportName] uppercaseString];
            
            
            // Need to get the other player (not the match creator
            MatchPlayer *player1 = nil; // will set as match creator
            MatchPlayer *player2 = nil; // the other player
        
            if (individualMatchNews.individualMatch.player1.userID == individualMatchNews.individualMatch.matchCreatorID) {
                player1 = individualMatchNews.individualMatch.player1;
                player2 = individualMatchNews.individualMatch.player2;
                
            } else {
                player2 = individualMatchNews.individualMatch.player1;
                player1 = individualMatchNews.individualMatch.player2;
            }
            
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            // player 1 pic
            NSString *urlMain1 = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                         baseString:[MyHelpers getNewPicURLIfFacebookSquareSize: player1.profileBaseString]];
            
             [cell.userImage1 setImageWithURL:[NSURL URLWithString:urlMain1]
                             placeholderImage:[UIImage imageNamed:@"avatar square"]];
            
            cell.userImage1.contentMode = UIViewContentModeScaleAspectFill;
            cell.userImage1.clipsToBounds = YES;
            
        
            // profile pic button
            UIButton *picButton1 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 153, 125)];
            [picButton1 addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
            picButton1.tag = player1.userID;
            [cell.userImage1 addSubview:picButton1];
            cell.userImage1.userInteractionEnabled = YES;
            // end profile pic button


            // player2 pic
            NSString *urlMain2 = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                         baseString:[MyHelpers getNewPicURLIfFacebookSquareSize:player2.profileBaseString]];
            
            [cell.userImage2 setImageWithURL:[NSURL URLWithString:urlMain2]
                            placeholderImage:[UIImage imageNamed:@"avatar square"]];
            
            cell.userImage2.contentMode = UIViewContentModeScaleAspectFill;
            cell.userImage2.clipsToBounds = YES;
            
            // profile pic button
            UIButton *picButton2 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 153, 125)];
            [picButton2 addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
            picButton2.tag = player2.userID;
            [cell.userImage2 addSubview:picButton2];
            cell.userImage2.userInteractionEnabled = YES;
            // end profile pic button
            
            [cell.vsCircle makeCircleWithColor:[UIColor newBlueLight] andRadius:30];
            
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
            
            cell.commentBubbleImage.image = [[UIImage imageNamed:@"Comment Small Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [cell.commentBubbleImage setTintColor:[UIColor darkGrayColor2]];
            
            [cell.headlineSportPicBackground makeRoundedBorderWithRadius:10];
            
            [cell.myBackgroundViewCell makeRoundedBorderWithRadius:3];
            cell.myBackgroundViewCell.layer.borderColor = [UIColor midGray2].CGColor;
            cell.myBackgroundViewCell.layer.borderWidth = 1.0;
            
            if ([[individualMatchNews.individualMatch getMatchStatusMessage:[self.preferences getUserID]] length] > 0) {
                cell.matchStatus.text = [individualMatchNews.individualMatch getMatchStatusMessage:[self.preferences getUserID]];
                cell.matchStatusIcon.hidden = NO;
            } else {
                cell.matchStatus.text = @"";
                cell.matchStatusIcon.hidden = YES;
            }
         //   cell.matchStatus.textColor = [UIColor whiteColor];
            
            cell.userNameLabel1.text = player1.userName;
            cell.userNameLabel2.text = player2.userName;
            
            // username1  button
            UIButton *username1Button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 21)];
            [username1Button addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
            username1Button.tag = player1.userID;
            [cell.userNameLabel1 addSubview:username1Button];
            cell.userNameLabel1.userInteractionEnabled = YES;
            // end username1 button

            // username2  button
            UIButton *username2Button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 21)];
            [username2Button addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
            username2Button.tag = player2.userID;
            [cell.userNameLabel2 addSubview:username2Button];
            cell.userNameLabel2.userInteractionEnabled = YES;
            // end username2 button
            
            
            cell.levelUser1.text = [NSString stringWithFormat:@"%ld", [[self getLevelForPlayer:player1 forSport:individualMatchNews.individualMatch.sport.sportName] integerValue]];
            cell.levelUser2.text = [NSString stringWithFormat:@"%ld", [[self getLevelForPlayer:player2 forSport:individualMatchNews.individualMatch.sport.sportName] integerValue]];
        
            cell.timeFrameLabel.text = [NSDate timeDiffCalc:individualMatchNews.newsDate];
            
            cell.detailButton.tag = indexPath.section;
            UIButton *newsfeedIconButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
            [newsfeedIconButton addTarget:self action:@selector(matchDetailsIndividualMatchCreation:) forControlEvents:UIControlEventTouchUpInside];
            newsfeedIconButton.tag = indexPath.section;
            [cell.newsfeedIcon addSubview:newsfeedIconButton];
            cell.newsfeedIcon.userInteractionEnabled = YES;
            
            
            if ([individualMatchNews.sharedByPlayer.userName length] > 0) {
    
                cell.sharedByLabel.text = [NSString stringWithFormat:@"Shared by %@", individualMatchNews.sharedByPlayer.userName];
                
            } else {
             
                cell.sharedByLabel.text = [NSString stringWithFormat:@"Created by %@", individualMatchNews.individualMatch.matchCreatorUsername];
            }
            
                    /*************** SHOW MATCH HISTORY ********************/
          /*  NSInteger maxVisibleMatchHistory;
            
            if ([individualMatchNews.individualMatch.matchHistoryArray count] > 3)
                maxVisibleMatchHistory = 3;
            else
                maxVisibleMatchHistory = [individualMatchNews.individualMatch.matchHistoryArray count];
            
            NSInteger startPoint = 233;
            
            NSInteger myCounter = 0;
            for (MatchHistoryObj *matchHist in individualMatchNews.individualMatch.matchHistoryArray) {
                SpecialUILabel *updateLine = [[SpecialUILabel alloc] initWithFrame:CGRectMake(20, startPoint, 250, 15)];
                
                if ([matchHist.participant.userName isKindOfClass:[NSNull class]]) {
                    NSString *firstCapChar = [[matchHist.update substringToIndex:1] capitalizedString];
                    NSString *cappedString = [matchHist.update stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:firstCapChar];
                    updateLine.text = [NSString stringWithFormat:@"%@", cappedString];
                }   else
                    updateLine.text = [NSString stringWithFormat:@"%@ %@", matchHist.participant.userName, matchHist.update];
                
                updateLine.font = [UIFont systemFontOfSize:12];
                updateLine.textColor = [UIColor lightGrayColor];
                updateLine.shouldRemoveFromMySuperview = YES;
                [updateLine sizeToFit];
                
                [cell.contentView addSubview:updateLine];
              
                SpecialUILabel *timeLabel = [[SpecialUILabel alloc] initWithFrame:CGRectMake(updateLine.frame.size.width + 30, startPoint, 20, 15)];
                timeLabel.text = [NSDate timeDiffCalc:matchHist.occurrenceDate];
                timeLabel.font = [UIFont fontWithName:@"HelveticaNeue-LightItalic" size:12];
                timeLabel.textColor = [UIColor lightGrayColor];
                [timeLabel setTextAlignment:NSTextAlignmentLeft];
                [timeLabel sizeToFit];
                timeLabel.shouldRemoveFromMySuperview = YES;
                [cell.contentView addSubview:timeLabel];
                
                startPoint += 20;
                myCounter ++;
                
                if (myCounter == maxVisibleMatchHistory)
                    break;

                
            }*/
            
            /*************** END SHOW MATCH HISTORY ********************/
            
        //    cell.contentView.frame = CGRectMake(0, 0, self.tableView.frame.size.width, [self tableView:tableView heightForRowAtIndexPath:indexPath]);
            
            // Show comment button
            
            [cell.commentButton addTarget:self action:@selector(commentPost:) forControlEvents:UIControlEventTouchUpInside];
            cell.commentButton.tag = indexPath.section;
            cell.commentNumberLabel.text = [NSString stringWithFormat:@"%lu comments", individualMatchNews.numComments];
            
            
            cell.commentBubbleImage.image = [[UIImage imageNamed:@"Comment Small Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [cell.commentBubbleImage setTintColor:[UIColor darkGrayColor2]];
            
            
            [cell.likeButton addTarget:self action:@selector(likePost:) forControlEvents:UIControlEventTouchUpInside];
            cell.likeButton.tag = indexPath.section;
            cell.likeNumberLabel.text = [NSString stringWithFormat:@"%lu props", individualMatchNews.numLikes];
            
            cell.likeIcon.image = [[UIImage imageNamed:@"Like Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];

            if (individualMatchNews.doesSessionUserLikePost)
                [cell.likeIcon setTintColor:[UIColor goldColor]];
            else
                [cell.likeIcon setTintColor:[UIColor darkGrayColor2]];
            
            [cell.smallCircleView makeRoundedBorderWithRadius:2];
            cell.smallCircleView.backgroundColor = [UIColor darkGrayColor2];
            cell.commentNumberLabel.textColor = [UIColor darkGrayColor2];
            cell.likeNumberLabel.textColor = [UIColor darkGrayColor2];
            
            cell.viewLikesButton.tag = indexPath.section;
            cell.viewCommentsButton.tag = indexPath.section;
            
            [cell.viewLikesButton addTarget:self action:@selector(viewLikes:) forControlEvents:UIControlEventTouchUpInside];
            [cell.viewCommentsButton addTarget:self action:@selector(viewComments:) forControlEvents:UIControlEventTouchUpInside];
            
            return cell;
            
        }
        
    }  else if ([(self.newsfeedPaginator.results)[indexPath.section] isKindOfClass:[TeamMatchCreationNews class]]) {
        
        TeamMatchCreationNews *teamMatchNews = (TeamMatchCreationNews *)(self.newsfeedPaginator.results)[indexPath.section];
        NSString *CellIdentifier = @"";
        
     //   if (teamMatchNews.postType == INDIVIDUAL_MATCH_CREATION) {
            CellIdentifier = @"Team Match Creation News Cell";
            
            NewsTeamMatchPostCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
            
            // clear subviews
            if ([cell.contentView subviews]){
                for (UIView *subview in [cell.contentView subviews]) {
                    if ([subview isKindOfClass:[SpecialUIButton class]]) {
                        SpecialUIButton * newSubview = (SpecialUIButton *)subview;
                        if (newSubview.shouldRemoveFromMySuperview)  // only items I created programmatically
                            [subview removeFromSuperview];
                        
                    } else if ([subview isKindOfClass:[SpecialUILabel class]]) {
                        SpecialUILabel *newSubview = (SpecialUILabel *)subview;
                        if (newSubview.shouldRemoveFromMySuperview)
                            [subview removeFromSuperview];
                    }
                    
                }
            }
//        }

        [cell.actionButtonHandle addTarget:self action:@selector(postExtraActions:) forControlEvents:UIControlEventTouchUpInside];
        cell.actionButtonHandle.tag = indexPath.section;
        
        cell.matchNameLabel.text = teamMatchNews.tMatch.matchName;
        
        cell.headlineSportPic.image = [[teamMatchNews.tMatch.sport getSportIcon] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [cell.headlineSportPic setTintColor:[UIColor whiteColor]];
        cell.headlineSportLabel.text = [[NSString stringWithFormat:@"%@ Team Challenge", teamMatchNews.tMatch.sport.sportName] uppercaseString];

        cell.teamLogo1.image = [UIImage imageNamed:teamMatchNews.tMatch.team1.picStringName];
        cell.teamLogo2.image = [UIImage imageNamed:teamMatchNews.tMatch.team2.picStringName];
        
        // team logo button
        UIButton *picButton1 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 153, 125)];
        [picButton1 addTarget:self action:@selector(goToTeamPage:) forControlEvents:UIControlEventTouchUpInside];
        picButton1.tag = teamMatchNews.tMatch.team1.teamID;
        [cell.teamLogo1 addSubview:picButton1];
        cell.teamLogo1.userInteractionEnabled = YES;

        // team pic button
        UIButton *picButton2 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 153, 125)];
        [picButton2 addTarget:self action:@selector(goToTeamPage:) forControlEvents:UIControlEventTouchUpInside];
        picButton2.tag = teamMatchNews.tMatch.team2.teamID;
        [cell.teamLogo2 addSubview:picButton2];
        cell.teamLogo2.userInteractionEnabled = YES;
        
        [cell.vsCircle makeCircleWithColor:[UIColor newBlueLight] andRadius:30];
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        [cell.commentButtonBackground makeRoundedBorderWithRadius:3];
        [cell.shareButtonBackground makeRoundedBorderWithRadius:3];
        cell.commentButtonBackground.layer.borderColor = [UIColor grayBorder1].CGColor;
        cell.commentButtonBackground.layer.borderWidth = 1.0;
        cell.shareButtonBackground.layer.borderColor = [UIColor grayBorder1].CGColor;
        cell.shareButtonBackground.layer.borderWidth = 1.0;
        
        
        cell.commentBubbleImage.image = [[UIImage imageNamed:@"Comment Small Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [cell.commentBubbleImage setTintColor:[UIColor lightGrayColor]];
        
        cell.shareButtonImage.image = [[UIImage imageNamed:@"Remix Share Small Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [cell.shareButtonImage setTintColor:[UIColor lightGrayColor]];
        
        cell.section2View.layer.borderColor = [UIColor midGray2].CGColor;
        cell.section2View.layer.borderWidth = 1.0;
        
        [cell.myBackgroundViewCell makeRoundedBorderWithRadius:3];
        cell.myBackgroundViewCell.layer.borderColor = [UIColor midGray2].CGColor;
        cell.myBackgroundViewCell.layer.borderWidth = 1.0;
        
 
        cell.teamNameLabel1.text = teamMatchNews.tMatch.team1.teamName;
        cell.teamNameLabel2.text = teamMatchNews.tMatch.team2.teamName;
        
        /*
        // username1  button
        UIButton *username1Button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 21)];
        [username1Button addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
        username1Button.tag = player1.userID;
        [cell.userNameLabel1 addSubview:username1Button];
        cell.userNameLabel1.userInteractionEnabled = YES;
        // end username1 button
        
        // username2  button
        UIButton *username2Button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 21)];
        [username2Button addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
        username2Button.tag = player2.userID;
        [cell.userNameLabel2 addSubview:username2Button];
        cell.userNameLabel2.userInteractionEnabled = YES;
        // end username2 button
        
        */
        
        
        cell.timeFrameLabel.text = [NSDate timeDiffCalc:teamMatchNews.newsDate];
        
        cell.detailButton.tag = indexPath.section;
        
        if ([teamMatchNews.sharedByPlayer.userName length] > 0) {
            
            cell.sharedByLabel.text = [NSString stringWithFormat:@"Shared by %@", teamMatchNews.sharedByPlayer.userName];
            
        } else {
            
            cell.sharedByLabel.text = [NSString stringWithFormat:@"Created by %@", teamMatchNews.tMatch.matchCreatorUsername];
        }
        
        
        
        /*************** SHOW MATCH HISTORY ********************/
        /*
        NSInteger maxVisibleMatchHistory;
        
        if ([individualMatchNews.individualMatch.matchHistoryArray count] > 3)
            maxVisibleMatchHistory = 3;
        else
            maxVisibleMatchHistory = [individualMatchNews.individualMatch.matchHistoryArray count];
        
        NSInteger startPoint = 233;
        
        NSInteger myCounter = 0;
        for (MatchHistoryObj *matchHist in individualMatchNews.individualMatch.matchHistoryArray) {
            SpecialUILabel *updateLine = [[SpecialUILabel alloc] initWithFrame:CGRectMake(20, startPoint, 250, 15)];
            
            if ([matchHist.participant.userName isKindOfClass:[NSNull class]]) {
                NSString *firstCapChar = [[matchHist.update substringToIndex:1] capitalizedString];
                NSString *cappedString = [matchHist.update stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:firstCapChar];
                updateLine.text = [NSString stringWithFormat:@"%@", cappedString];
            }   else
                updateLine.text = [NSString stringWithFormat:@"%@ %@", matchHist.participant.userName, matchHist.update];
            
            updateLine.font = [UIFont systemFontOfSize:12];
            updateLine.textColor = [UIColor lightGrayColor];
            updateLine.shouldRemoveFromMySuperview = YES;
            [updateLine sizeToFit];
            
            [cell.contentView addSubview:updateLine];
            
            SpecialUILabel *timeLabel = [[SpecialUILabel alloc] initWithFrame:CGRectMake(updateLine.frame.size.width + 30, startPoint, 20, 15)];
            timeLabel.text = [NSDate timeDiffCalc:matchHist.occurrenceDate];
            timeLabel.font = [UIFont fontWithName:@"HelveticaNeue-LightItalic" size:12];
            timeLabel.textColor = [UIColor lightGrayColor];
            [timeLabel setTextAlignment:NSTextAlignmentLeft];
            [timeLabel sizeToFit];
            timeLabel.shouldRemoveFromMySuperview = YES;
            [cell.contentView addSubview:timeLabel];
            
            startPoint += 20;
            myCounter ++;
            
            if (myCounter == maxVisibleMatchHistory)
                break;
            
            
        }
        */
        
        /*************** END SHOW MATCH HISTORY ********************/
        
        cell.contentView.frame = CGRectMake(0, 0, self.tableView.frame.size.width, [self tableView:tableView heightForRowAtIndexPath:indexPath]);
        
        // Show comment button
        /*
        
        [cell.commentButton addTarget:self action:@selector(commentPost:) forControlEvents:UIControlEventTouchUpInside];
        cell.commentButton.tag = indexPath.section;
        cell.commentNumberLabel.text = [NSString stringWithFormat:@"%lu", teamMatchNews.numComments];
        if (teamMatchNews.numComments == 1)
            cell.commentLabel.text = @"Comment";
        else
            cell.commentLabel.text = @"Comments";
        
        if (individualMatchNews.numRemixes == 1)
            cell.sharesLabel.text = @"Share";
        else
            cell.sharesLabel.text = @"Shares";
        
        
        cell.commentBubbleImage.image = [[UIImage imageNamed:@"Comment Small Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [cell.commentBubbleImage setTintColor:[UIColor lightGrayColor]];
        
        cell.shareIcon.image = [[UIImage imageNamed:@"Remix Share Small Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [cell.shareIcon setTintColor:[UIColor lightGrayColor]];
        
        
        [cell.sharesButton addTarget:self action:@selector(remixIndividualMatchPost:) forControlEvents:UIControlEventTouchUpInside];
        cell.sharesButton.tag = indexPath.section;
        cell.sharesNumberLabel.text = [NSString stringWithFormat:@"%lu", individualMatchNews.numRemixes];
        
        */

        
        return cell;

    
    } else if ([(self.newsfeedPaginator.results)[indexPath.section] isKindOfClass:[ImagePost class]]) {
        
        NSString *CellIdentifier = @"Text Image Cell";
        
        ImagePostCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        
        // clear subviews
        if ([cell.contentView subviews]){
            for (UIView *subview in [cell.contentView subviews]) {
                if ([subview isKindOfClass:[SpecialUIButton class]]) {
                    SpecialUIButton * newSubview = (SpecialUIButton *)subview;
                    if (newSubview.shouldRemoveFromMySuperview)  // only items I created programmatically
                        [subview removeFromSuperview];
                    
                } else if ([subview isKindOfClass:[SpecialUILabel class]]) {
                    SpecialUILabel *newSubview = (SpecialUILabel *)subview;
                    if (newSubview.shouldRemoveFromMySuperview)
                        [subview removeFromSuperview];
                }
                
            }
        }

        
        ImagePost *iPost = (ImagePost *)(self.newsfeedPaginator.results)[indexPath.section];

        [cell.actionButtonHandle addTarget:self action:@selector(postExtraActions:) forControlEvents:UIControlEventTouchUpInside];
        cell.actionButtonHandle.tag = indexPath.section;
        
        [cell.mainBackgroundViewArea makeRoundedBorderWithRadius:3];
        cell.mainBackgroundViewArea.layer.borderColor = [UIColor midGray2].CGColor;
        cell.mainBackgroundViewArea.layer.borderWidth = 1.0;
        
        
        cell.usernameLabel.text = iPost.newsMakerUserName;
        cell.messageLabel.text = [NSString stringWithFormat:@"posted a photo"];
       
        NSString *urlMain = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                    baseString:iPost.profileBaseString];
        
        cell.profilePic.hidden = YES;
        [cell.profilePic setImageWithURL:[NSURL URLWithString:urlMain]
                        placeholderImage:[UIImage imageNamed:@"avatar square"] ];
        [UIImage makeRoundedImage:cell.profilePic withRadius:30];
        
        cell.profilePic.layer.borderColor = [UIColor whiteColor].CGColor;
        cell.profilePic.layer.borderWidth = 1.0;
        
        // profile pic button
        UIButton *picButton1 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];
        [picButton1 addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
        picButton1.tag = iPost.newsMakerUserID;
        [cell.profilePic addSubview:picButton1];
        cell.profilePic.userInteractionEnabled = YES;
        // end profile pic button
        
        cell.profilePic.hidden = NO;
        
        NSString *urlPostPic = [S3Tools getFileNameStringWithType:@"photo"
                                                       baseString:iPost.imageBaseFileString];
        
        cell.imagePost.contentMode = UIViewContentModeScaleAspectFill;
        cell.imagePost.clipsToBounds = YES;
        
        [cell.imagePost setImageWithURL:[NSURL URLWithString:urlPostPic]
                        placeholderImage:[UIImage imageNamed:@"Landscape Placeholder"]];
        
        
        cell.dateLabel.text = [NSDate timeDiffCalc:iPost.newsDate];
  
        cell.userTextPostLabel = [self makeDynamicHeighLabel:iPost.textPost forLabel:cell.userTextPostLabel];
        cell.userTextPostLabel.text = iPost.textPost;

       
        // adjust image starting point
        CGRect newFrame = cell.imagePost.frame;
        newFrame.origin.y = cell.userTextPostLabel.frame.origin.y + cell.userTextPostLabel.frame.size.height + 10;
        cell.imagePost.frame = newFrame;
       
        if ([iPost.sharedByPlayer.userName length] > 0) {
            cell.sharedByLabel.hidden = NO;
            cell.sharedByLabel.text = [NSString stringWithFormat:@"shared by %@", iPost.sharedByPlayer.userName];
            
        } else {
            cell.sharedByLabel.hidden = YES;
        }
        
    //    CGFloat cellHeight = [self calculateHeightForImagePost:indexPath.section];
        
    //    cell.contentView.frame = CGRectMake(0, 0, self.tableView.frame.size.width, [self tableView:tableView heightForRowAtIndexPath:indexPath]);

        
        // Show comment button
       
        
        cell.commentIcon.image = [[UIImage imageNamed:@"Comment Small Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [cell.commentIcon setTintColor:[UIColor lightGrayColor]];
        
        
        [cell.commentButton addTarget:self action:@selector(commentPost:) forControlEvents:UIControlEventTouchUpInside];
        cell.commentButton.tag = indexPath.section;
        cell.commentNumberLabel.text = [NSString stringWithFormat:@"%lu", iPost.numComments];
        if (iPost.numComments == 1)
            cell.commentTextLabel.text = @"Comment";
        else
            cell.commentTextLabel.text = @"Comments";
        
        if (iPost.numRemixes == 1)
            cell.shareTextLabel.text = @"Share";
        else
            cell.shareTextLabel.text = @"Shares";
        
        
        [cell.likeButton addTarget:self action:@selector(likePost:) forControlEvents:UIControlEventTouchUpInside];
        cell.likeButton.tag = indexPath.section;
        cell.likeNumberLabel.text = [NSString stringWithFormat:@"%lu", iPost.numLikes];

        cell.likeIcon.image = [[UIImage imageNamed:@"Like Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];

        if (iPost.doesSessionUserLikePost)
            [cell.likeIcon setTintColor:[UIColor goldColor]];
        else
            [cell.likeIcon setTintColor:[UIColor lightGrayColor]];
        
        
        
      
        return cell;

    } else if ([(self.newsfeedPaginator.results)[indexPath.section] isKindOfClass:[LockerPost class]]) {
        
        NSString *CellIdentifier = @"Text Image Cell";
        
        ImagePostCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        
        // clear subviews
        if ([cell.contentView subviews]){
            for (UIView *subview in [cell.contentView subviews]) {
                if ([subview isKindOfClass:[SpecialUIButton class]]) {
                    SpecialUIButton * newSubview = (SpecialUIButton *)subview;
                    if (newSubview.shouldRemoveFromMySuperview)  // only items I created programmatically
                        [subview removeFromSuperview];
                    
                } else if ([subview isKindOfClass:[SpecialUILabel class]]) {
                    SpecialUILabel *newSubview = (SpecialUILabel *)subview;
                    if (newSubview.shouldRemoveFromMySuperview)
                        [subview removeFromSuperview];
                }
                
            }
        }

        LockerPost *iPost = (LockerPost *)(self.newsfeedPaginator.results)[indexPath.section];
        
        [cell.actionButtonHandle addTarget:self action:@selector(postExtraActions:) forControlEvents:UIControlEventTouchUpInside];
        cell.actionButtonHandle.tag = indexPath.section;
        
        [cell.mainBackgroundViewArea makeRoundedBorderWithRadius:6];
        
        cell.usernameLabel.text = iPost.newsMakerUserName;
        cell.messageLabel.text = [NSString stringWithFormat:@"posted a photo"];
        
        NSString *urlMain = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                    baseString:iPost.profileBaseString];
        
        cell.profilePic.hidden = YES;
        [cell.profilePic setImageWithURL:[NSURL URLWithString:urlMain]
                        placeholderImage:[UIImage imageNamed:@"avatar square"] ];
        [UIImage makeRoundedImageNoBorder:cell.profilePic withRadius:5];
        
        // profile pic button
        UIButton *picButton1 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];
        [picButton1 addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
        picButton1.tag = iPost.newsMakerUserID;
        [cell.profilePic addSubview:picButton1];
        cell.profilePic.userInteractionEnabled = YES;
        // end profile pic button
        
        cell.profilePic.hidden = NO;
        
        
        NSString *urlPostPic = [S3Tools2 getFileNameStringWithType:@"photo_regsize" baseString:[NSString stringWithFormat:@"%ld/photo/%@", iPost.newsMakerUserID, iPost.lockPic.imageFileName]];
        
        cell.imagePost.contentMode = UIViewContentModeScaleAspectFill;
        cell.imagePost.clipsToBounds = YES;
        
        [cell.imagePost setImageWithURL:[NSURL URLWithString:urlPostPic] placeholderImage:[UIImage imageNamed:@"Landscape Placeholder"]];
        
        cell.dateLabel.text = [NSDate timeDiffCalc:iPost.newsDate];
        
        cell.userTextPostLabel = [self makeDynamicHeighLabel:iPost.textPost forLabel:cell.userTextPostLabel];
        cell.userTextPostLabel.text = iPost.textPost;
        
        cell.contentView.frame = CGRectMake(0, 0, self.tableView.frame.size.width, [self tableView:tableView heightForRowAtIndexPath:indexPath]);
        
        
        // Show comment button
        cell.commentIcon.image = [[UIImage imageNamed:@"Comment Small Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [cell.commentIcon setTintColor:[UIColor darkGrayColor2]];
        
        cell.likeIcon.image = [[UIImage imageNamed:@"Like Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    
        if (iPost.doesSessionUserLikePost)
            [cell.likeIcon setTintColor:[UIColor goldColor]];
        else
            [cell.likeIcon setTintColor:[UIColor darkGrayColor2]];
        
        [cell.commentButton addTarget:self action:@selector(commentPost:) forControlEvents:UIControlEventTouchUpInside];
        cell.commentButton.tag = indexPath.section;
        cell.commentNumberLabel.text = [NSString stringWithFormat:@"%d comments", iPost.numComments];
        cell.commentNumberLabel2.text = [NSString stringWithFormat:@"%d", iPost.numComments];
        if (iPost.numComments == 1)
            cell.commentTextLabel.text = @"Comment";
        else
            cell.commentTextLabel.text = @"Comments";
        
        if (iPost.numRemixes == 1)
            cell.shareTextLabel.text = @"Share";
        else
            cell.shareTextLabel.text = @"Shares";
        
        [cell.smallCircle makeRoundedBorderWithRadius:2];
        cell.smallCircle.backgroundColor = [UIColor darkGrayColor2];
        cell.commentNumberLabel.textColor = [UIColor darkGrayColor2];
        cell.likeNumberLabel.textColor = [UIColor darkGrayColor2];
        
        [cell.likeButton addTarget:self action:@selector(likePost:) forControlEvents:UIControlEventTouchUpInside];
        cell.likeButton.tag = indexPath.section;
        cell.likeNumberLabel.text = [NSString stringWithFormat:@"%d props", iPost.numLikes];
        cell.likeNumberLabel2.text = [NSString stringWithFormat:@"%d", iPost.numLikes];
        
        if ([iPost.lockPic.statsGrp.statsArray count] > 0) {
            cell.statBackgroundView.hidden = NO;
            [cell.value1 setHidden:NO];
            [cell.desc1 setHidden:NO];
            if ([iPost.lockPic.statsGrp.statsArray count] == 1) {
                [cell.value2 setHidden:YES];
                [cell.value3 setHidden:YES];
                [cell.desc2 setHidden:YES];
                [cell.desc3 setHidden:YES];
                [cell.sep1 setHidden:YES];
                [cell.sep2 setHidden:YES];
                [cell.statIcon setHidden:YES];
                [cell.line1 setHidden:NO];
                [cell.line2 setHidden:NO];
                [cell.drills setHidden:YES];
                [cell.titleconst1 setConstant: 12 + 92];
                [cell.descconst1 setConstant: 10 + 90];
            } else if ([iPost.lockPic.statsGrp.statsArray count] == 2) {
                [cell.value2 setHidden:NO];
                [cell.value3 setHidden:YES];
                [cell.desc2 setHidden:NO];
                [cell.desc3 setHidden:YES];
                [cell.sep1 setHidden:NO];
                [cell.sep2 setHidden:YES];
                [cell.statIcon setHidden:YES];
                [cell.line1 setHidden:YES];
                [cell.line2 setHidden:YES];
                [cell.drills setHidden:YES];
                [cell.titleconst1 setConstant: 12 + 20];
                [cell.descconst1 setConstant: 10 + 20];
                [cell.descconst2 setConstant: 92 + 70];
                [cell.titleconst2 setConstant: 92 + 70];
                [cell.sepconst1 setConstant: 89 + 45];
            } else if ([iPost.lockPic.statsGrp.statsArray count] == 3) {
                [cell.value2 setHidden:NO];
                [cell.value3 setHidden:NO];
                [cell.desc2 setHidden:NO];
                [cell.desc3 setHidden:NO];
                [cell.sep1 setHidden:NO];
                [cell.sep2 setHidden:NO];
                [cell.statIcon setHidden:YES];
                [cell.line1 setHidden:YES];
                [cell.line2 setHidden:YES];
                [cell.drills setHidden:YES];
                [cell.titleconst1 setConstant: 8];
                [cell.descconst1 setConstant: 8];
                [cell.descconst2 setConstant: 92];
                [cell.titleconst2 setConstant: 92];
                [cell.sepconst1 setConstant: 89];
            }
            
            int counter = 0;
            
            for (id obj in iPost.lockPic.statsGrp.statsArray) {
                
                if ([obj isKindOfClass:[StatTag class]]) {
                    
                    StatTag *stat = (StatTag *)obj;
                    
                    switch (counter) {
                        case 0:
                            cell.value1.text = [NSString stringWithFormat:@"%@", stat.statTagValue];
                            if (stat.showSubCategory) {
                                cell.desc1.text = [NSString stringWithFormat:@"%@ %@", stat.statLink, stat.statTagName];
                            } else {
                                cell.desc1.text = stat.statTagName;
                            }
                            break;
                            
                        case 1:
                            cell.value2.text = [NSString stringWithFormat:@"%@", stat.statTagValue];
                            if (stat.showSubCategory) {
                                cell.desc2.text = [NSString stringWithFormat:@"%@ %@", stat.statLink, stat.statTagName];
                            } else {
                                cell.desc2.text = stat.statTagName;
                            }
                            break;
                        case 2:
                            cell.value3.text = [NSString stringWithFormat:@"%@", stat.statTagValue];
                            if (stat.showSubCategory) {
                                cell.desc3.text = [NSString stringWithFormat:@"%@ %@", stat.statLink, stat.statTagName];
                            } else
                                cell.desc3.text = stat.statTagName;
                            break;
                        default:
                            break;
                    }
                } else if ([obj isKindOfClass:[StatTagCombination class]]) {
                    StatTagCombination *sta = (StatTagCombination *)obj;
                    
                    switch (counter) {
                        case 0:
                            cell.value1.text = [NSString stringWithFormat:@"%@ / %@", sta.numerator.statTagValue, sta.denominator.statTagValue];
                            cell.desc1.text = [NSString stringWithFormat:@"%@", sta.numerator.statLink];
                            break;
                        case 1:
                            cell.value2.text = [NSString stringWithFormat:@"%@ / %@", sta.numerator.statTagValue, sta.denominator.statTagValue];
                            cell.desc2.text = [NSString stringWithFormat:@"%@", sta.numerator.statLink];
                            break;
                        case 2:
                            cell.value3.text = [NSString stringWithFormat:@"%@ / %@", sta.numerator.statTagValue, sta.denominator.statTagValue];
                            cell.desc3.text = [NSString stringWithFormat:@"%@", sta.numerator.statLink];
                            break;
                        default:
                            break;
                    }
                    
                }
                counter ++;
            }
        }
        /*if ([[iPost.lockPic.statsGrp.statsArray objectAtIndex:0] isKindOfClass:[StatTag class]]) {
    
                StatTag *stat = [iPost.lockPic.statsGrp.statsArray objectAtIndex:0]; // get the first one
            
                
                NSMutableAttributedString * string = nil;
                
                if (stat.showSubCategory)
                    string = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@  -  %@ %@", stat.statTagValue, stat.statLink, stat.statTagName]];
                else
                    string = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@  -  %@", stat.statTagValue, stat.statTagName]];*/
                /*
                if (stat.showSubCategory)
                    cell.statLabel.text = [[NSString stringWithFormat:@"%@ -- %@ %@", stat.statTagValue, stat.statLink, stat.statTagName] capitalizedString];
                else
                    cell.statLabel.text = [[NSString stringWithFormat:@"%@ -- %@", stat.statTagValue, stat.statTagName] capitalizedString];
                */
                /*[string addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, [stat.statTagValue length])];
                cell.statLabel.attributedText = string;
                [cell.statLabel setTextColor:[UIColor whiteColor]];
                
            } else if ([[iPost.lockPic.statsGrp.statsArray objectAtIndex:0] isKindOfClass:[StatTagCombination class]]) {
                StatTagCombination *sta = [iPost.lockPic.statsGrp.statsArray objectAtIndex:0];*/
                
               /*
                cell.statLabel.text = [[NSString stringWithFormat:@"%@ / %@ -- %@", sta.numerator.statTagValue, sta.denominator.statTagValue, sta.numerator.statLink] capitalizedString];
         
                */
                /*NSMutableAttributedString * string = nil;
                string = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ / %@  -  %@", sta.numerator.statTagValue, sta.denominator.statTagValue, sta.numerator.statLink]];
                [string addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, [sta.numerator.statTagValue length] + [sta.denominator.statTagValue length] + 3)];
                cell.statLabel.attributedText = string;
                [cell.statLabel setTextColor:[UIColor whiteColor]];
            }
            
            
            
        }*/
        else if ([iPost.lockPic.trainingTagsArray count] > 0) {
            cell.statBackgroundView.hidden = NO;
            
            NSString *otherTrainingText = @"";
            
            for (TrainingTag *tag in iPost.lockPic.trainingTagsArray) {
                if ([otherTrainingText length] > 0)
                    otherTrainingText = [otherTrainingText stringByAppendingString:@", "];
                
                otherTrainingText = [otherTrainingText stringByAppendingString:tag.trainingTagName];
            }
            
            if ([otherTrainingText length] > 0){
                cell.drills.text = [NSString stringWithFormat:@"%@", otherTrainingText];
            } else
                cell.drills.hidden = YES;
            
            [cell.statIcon setHidden:NO];
            cell.statIcon.image = [UIImage imageNamed:@"white_drill"];
            [cell.statBackgroundView bringSubviewToFront:cell.statIcon];
            //[cell.statIcon setTintColor:[UIColor whiteColor]];
            
            [cell.value1 setHidden:YES];
            [cell.value2 setHidden:YES];
            [cell.value3 setHidden:YES];
            [cell.desc1 setHidden:YES];
            [cell.desc2 setHidden:YES];
            [cell.desc3 setHidden:YES];
            [cell.sep1 setHidden:YES];
            [cell.sep2 setHidden:YES];
            [cell.line1 setHidden:YES];
            [cell.line2 setHidden:YES];
            [cell.drills setHidden:NO];
        } else { // no drills, neither stats
            cell.statBackgroundView.hidden = YES;
            [cell.statIcon setHidden:YES];
            [cell.value1 setHidden:YES];
            [cell.value2 setHidden:YES];
            [cell.value3 setHidden:YES];
            [cell.desc1 setHidden:YES];
            [cell.desc2 setHidden:YES];
            [cell.desc3 setHidden:YES];
            [cell.sep1 setHidden:YES];
            [cell.sep2 setHidden:YES];
            [cell.line1 setHidden:YES];
            [cell.line2 setHidden:YES];
            [cell.drills setHidden:YES];
        }
        
        cell.viewPropsButton.tag = indexPath.section;
        cell.viewCommentsButton.tag = indexPath.section;
        //cell.viewLockerImageButton.tag = iPost.lockPic.lockerID;
        
        [cell.viewPropsButton addTarget:self action:@selector(viewLikes:) forControlEvents:UIControlEventTouchUpInside];
        [cell.viewCommentsButton addTarget:self action:@selector(viewComments:) forControlEvents:UIControlEventTouchUpInside];
        
        
        UIButton *newsfeedIconButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
        [newsfeedIconButton addTarget:self action:@selector(viewLockerImage:) forControlEvents:UIControlEventTouchUpInside];
        newsfeedIconButton.tag = iPost.lockPic.lockerID;
        [cell.newsfeedIcon addSubview:newsfeedIconButton];
        cell.newsfeedIcon.userInteractionEnabled = YES;
        
        UIButton *tmpBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, cell.imagePost.frame.size.width, cell.contentView.frame.size.height)];
        NSLog(@"%f",tmpBtn.frame.size.height);
        tmpBtn.tag = iPost.lockPic.lockerID;
        [tmpBtn addTarget:self action:@selector(viewLockerImage:) forControlEvents:UIControlEventTouchUpInside];
        [cell.mainBackgroundViewArea addSubview:tmpBtn];
        //[cell.imagePost bringSubviewToFront:tmpBtn];
        [cell.mainBackgroundViewArea bringSubviewToFront:cell.buttomBar];
        [cell.mainBackgroundViewArea bringSubviewToFront:cell.profileView];
        return cell;
    }
    /* else if ([(self.newsfeedPaginator.results)[indexPath.section] isKindOfClass:[VideoPost class]]) {
         NSString *CellIdentifier  = @"Video Post Cell";
        VideoPostCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        VideoPost *vPost = (VideoPost *)(self.newsfeedPaginator.results)[indexPath.section];
        
        [cell.commentButton makeRoundBorderWithColor:[UIColor colorWithRed:220.0/255.0 green:220.0/255.0 blue:220.0/255.0 alpha:1.0] ];
        cell.commentIcon.image = [[UIImage imageNamed:@"Speech Bubble"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [cell.commentIcon setTintColor:[UIColor lightGrayColor]];
        cell.commentLabel.textColor = [UIColor lightGrayColor];
        cell.commentLabel.text = [NSString stringWithFormat:@"%d", vPost.numComments];
        
        [cell.remixButton makeRoundedBorderWithColor:[UIColor colorWithRed:220.0/255.0 green:220.0/255.0 blue:220.0/255.0 alpha:1.0] ];
        cell.remixLabel.textColor = [UIColor lightGrayColor];
        cell.remixLabel.text = [NSString stringWithFormat:@"%d", vPost.numRemixes];
        
        cell.remixIcon.image = [[UIImage imageNamed:@"Remix Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        
        if (vPost.didSessionUserRemixPost)
            [cell.remixIcon setTintColor:[UIColor colorWithRed:255.0/255.0 green:153.0/255.0 blue:153.0/255.0 alpha:1.0]];
        else
            [cell.remixIcon setTintColor:[UIColor lightGrayColor]];
        
        cell.timerIcon.image = [[UIImage imageNamed:@"Timer Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [cell.timerIcon setTintColor:[UIColor lightGrayColor]];
        
        
        
        
        
        cell.usernameLabel.text = vPost.newsMakerUserName;
        cell.messageLabel.text = @"posted a highlight reel";
        
        
        CGPoint mpoint = CGPointMake(27, 27);
        cell.videoPostIImage.image = [UIImage drawImage:[UIImage imageNamed:@"play-button.png"] inImage:vPost.vid.thumbnailImage atPoint:mpoint];
        
        NSString *urlMain = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                    baseString:vPost.profileBaseString];
        
        [cell.profilePic setImageWithURL:[NSURL URLWithString:urlMain]
                        placeholderImage:[UIImage imageNamed:@"avatar round"]];
        [UIImage makeRoundedImage:cell.profilePic withRadius:20];

        
        NSDate *currentDate = [NSDate date];
        NSTimeInterval timeDifference = [currentDate timeIntervalSinceDate:vPost.newsDate];
        int minutes = timeDifference / 60;
        int hours = minutes / 60;
        int days = minutes / 1440;
        NSString *timeDiffString = nil;
        
        if (minutes > 1440)
            timeDiffString = [NSString stringWithFormat:@"%dd", days];
        else if (minutes > 60)
            timeDiffString = [NSString stringWithFormat:@"%dh", hours];
        else
            timeDiffString = [NSString stringWithFormat:@"%dm", minutes];
        
        cell.dateLabel.text = timeDiffString;
        
        cell.userTextPost = [self makeDynamicHeighLabel:vPost.textPost forLabel:cell.userTextPost];
        cell.userTextPost.text = vPost.textPost;
        
        
        // adjust image starting point
        CGRect newFrame = cell.videoPostIImage.frame;
        newFrame.origin.y = cell.userTextPost.frame.origin.y + cell.userTextPost.frame.size.height + 10;
        cell.videoPostIImage.frame = newFrame;

        cell.commentButton.tag = indexPath.section;
        cell.remixButton.tag = indexPath.section;
        
        return cell;
        
    }*/ else if ([(self.newsfeedPaginator.results)[indexPath.section] isKindOfClass:[TextPost class]]) {
        NSString *CellIdentifier  = @"User Text Post Cell";
        TextPostCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        
        // clear subviews
        if ([cell.contentView subviews]){
            for (UIView *subview in [cell.contentView subviews]) {
                if ([subview isKindOfClass:[SpecialUIButton class]]) {
                    SpecialUIButton * newSubview = (SpecialUIButton *)subview;
                    if (newSubview.shouldRemoveFromMySuperview)  // only items I created programmatically
                        [subview removeFromSuperview];
                    
                } else if ([subview isKindOfClass:[SpecialUILabel class]]) {
                    SpecialUILabel *newSubview = (SpecialUILabel *)subview;
                    if (newSubview.shouldRemoveFromMySuperview)
                        [subview removeFromSuperview];
                }
                
            }
        }
        
        TextPost *nPost = (TextPost *)(self.newsfeedPaginator.results)[indexPath.section];
 
        [cell.actionButtonHandle addTarget:self action:@selector(postExtraActions:) forControlEvents:UIControlEventTouchUpInside];
        cell.actionButtonHandle.tag = indexPath.section;
        
        cell.usernameLabel.text = nPost.newsMakerUserName;
        
        [cell.sectionMainView makeRoundedBorderWithRadius:6];
        
        /*if ([nPost.sharedByPlayer.userName length] > 0) {
            cell.sharedByLabel.hidden = NO;
            cell.sharedByLabel.text = [NSString stringWithFormat:@"shared by %@", nPost.sharedByPlayer.userName];
           
        } else {
            cell.sharedByLabel.hidden = YES;
        }*/
        [cell.actionSheetHandler addTarget:self action:@selector(postExtraActions:) forControlEvents:UIControlEventTouchUpInside];
        cell.actionSheetHandler.tag = indexPath.section;
        
        cell.propsButton.tag = indexPath.section;
        cell.commentButtonLeft.tag = indexPath.section;
        //cell.viewLockerImageButton.tag = iPost.lockPic.lockerID;
        
        [cell.propsButton addTarget:self action:@selector(viewLikes:) forControlEvents:UIControlEventTouchUpInside];
        [cell.commentButtonLeft addTarget:self action:@selector(viewComments:) forControlEvents:UIControlEventTouchUpInside];
        
        NSString *urlMain = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                    baseString:nPost.profileBaseString];
        
        [cell.profilePic setImageWithURL:[NSURL URLWithString:urlMain]
                        placeholderImage:[UIImage imageNamed:@"avatar square"] ];
        [UIImage makeRoundedImageNoBorder:cell.profilePic withRadius:5];
        
        // profile pic button
        UIButton *picButton1 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];
        [picButton1 addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
        picButton1.tag = nPost.newsMakerUserID;
        [cell.profilePic addSubview:picButton1];
        cell.profilePic.userInteractionEnabled = YES;
        // end profile pic button
        
        
        cell.dateTimeLabel.text = [NSDate timeDiffCalc:nPost.newsDate];
        
        cell.userTextPostLabel = [self makeDynamicHeighLabel:nPost.textPost forLabel:cell.userTextPostLabel];
        cell.userTextPostLabel.text = nPost.textPost;
        
        cell.commentButtonIcon.image = [[UIImage imageNamed:@"Comment Small Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [cell.commentButtonIcon setTintColor:[UIColor darkGrayColor2]];
        
        cell.likeIcon.image = [[UIImage imageNamed:@"Like Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        if (nPost.doesSessionUserLikePost)
            [cell.likeIcon setTintColor:[UIColor goldColor]];
        else
            [cell.likeIcon setTintColor:[UIColor darkGrayColor2]];
        
        [cell.commentButton addTarget:self action:@selector(commentPost:) forControlEvents:UIControlEventTouchUpInside];
        cell.commentButton.tag = indexPath.section;
        cell.commentNumberLabel.text = [NSString stringWithFormat:@"%d comments", nPost.numComments];
        cell.commentNumberLabel2.text = [NSString stringWithFormat:@"%d", nPost.numComments];
        if (nPost.numComments == 1)
            cell.commentTextLabel.text = @"Comment";
        else
            cell.commentTextLabel.text = @"Comments";
        
        /*if (nPost.numRemixes == 1)
            cell.shareTextLabel.text = @"Share";
        else
            cell.shareTextLabel.text = @"Shares";*/
        
        [cell.smallCircle makeRoundedBorderWithRadius:2];
        cell.smallCircle.backgroundColor = [UIColor darkGrayColor2];
        cell.commentNumberLabel.textColor = [UIColor darkGrayColor2];
        cell.likeNumberLabel.textColor = [UIColor darkGrayColor2];
        
        [cell.likeButton addTarget:self action:@selector(likePost:) forControlEvents:UIControlEventTouchUpInside];
        cell.likeButton.tag = indexPath.section;
        cell.likeNumberLabel.text = [NSString stringWithFormat:@"%d props", nPost.numLikes];
        cell.likeNumberLabel2.text = [NSString stringWithFormat:@"%d", nPost.numLikes];
    
        
        cell.contentView.frame = CGRectMake(0, 0, self.tableView.frame.size.width, [self tableView:tableView heightForRowAtIndexPath:indexPath]);
        
        return cell;

        
    } else if ([(self.newsfeedPaginator.results)[indexPath.section] isKindOfClass:[MatchPlayerNewsOfficialFinishedIndividualMatch class]]) {
        
        // Match is confirmed and can now display final results
        
        MatchPlayerNewsOfficialFinishedIndividualMatch *mpn = [self.newsfeedPaginator.results objectAtIndex:indexPath.section];
        IndividualMatch *iMatch = (IndividualMatch *)mpn.match;
        
        NSString *CellIdentifier = @"Match News Final Score Update Cell";
        MatchNewsFinalScoreUpdateCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        
        // clear subviews
        if ([cell.contentView subviews]){
            for (UIView *subview in [cell.contentView subviews]) {
                if ([subview isKindOfClass:[SpecialUIButton class]]) {
                    SpecialUIButton * newSubview = (SpecialUIButton *)subview;
                    if (newSubview.shouldRemoveFromMySuperview)  // only items I created programmatically
                        [subview removeFromSuperview];
                    
                } else if ([subview isKindOfClass:[SpecialUILabel class]]) {
                    SpecialUILabel *newSubview = (SpecialUILabel *)subview;
                    if (newSubview.shouldRemoveFromMySuperview)
                        [subview removeFromSuperview];
                }
                
            }
        }
        
        [cell.actionButtonHandle addTarget:self action:@selector(postExtraActions:) forControlEvents:UIControlEventTouchUpInside];
        cell.actionButtonHandle.tag = indexPath.section;
        cell.iMatch = iMatch;
        
     //   [cell.detailButtonView makeRoundedBorderWithColor:[UIColor midGray2]];
        [cell.vsCircle makeCircleWithColor:[UIColor newBlueLight] andRadius:30];

        cell.sportIcon.image = [[iMatch.sport getSportIcon2] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [cell.sportIcon setTintColor:[UIColor darkGrayColor2]];
        [cell.sportIconBackground makeRoundedBorderWithRadius:10];

    
        cell.postTimeLabel.text = [NSDate timeDiffCalc:mpn.newsDate];

        
        cell.matchNameLabel.text = iMatch.matchName;
      
 
        
        cell.headlineMessage.text = [[NSString stringWithFormat:@"%@ Challenge", iMatch.sport.sportName] uppercaseString];
        
        cell.player1Username.text = iMatch.player1.userName;
        cell.player2Username.text = iMatch.player2.userName;
        
        // username1  button
        UIButton *username1Button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 21)];
        [username1Button addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
        username1Button.tag = iMatch.player1.userID;
        [cell.player1Username addSubview:username1Button];
        cell.player1Username.userInteractionEnabled = YES;
        // end username1 button
        
        // username2  button
        UIButton *username2Button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 21)];
        [username2Button addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
        username2Button.tag = iMatch.player2.userID;
        [cell.player2Username addSubview:username2Button];
        cell.player2Username.userInteractionEnabled = YES;
        // end username2 button

        cell.player1UsernameV2.text = iMatch.player1.userName;
        cell.player2UsernameV2.text = iMatch.player2.userName;
        
        // username1  button
        UIButton *username1ButtonV2 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 21)];
        [username1ButtonV2 addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
        username1ButtonV2.tag = iMatch.player1.userID;
        [cell.player1UsernameV2 addSubview:username1ButtonV2];
        cell.player1UsernameV2.userInteractionEnabled = YES;
        // end username1 button
        
        // username2  button
        UIButton *username2ButtonV2 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 21)];
        [username2ButtonV2 addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
        username2ButtonV2.tag = iMatch.player2.userID;
        [cell.player2UsernameV2 addSubview:username2ButtonV2];
        cell.player2UsernameV2.userInteractionEnabled = YES;
        // end username2 button
        
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        // player 1 pic
        NSString *urlMain1 = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                     baseString:[MyHelpers getNewPicURLIfFacebookSquareSize:iMatch.player1.profileBaseString]];
        
        [cell.userImage1 setImageWithURL:[NSURL URLWithString:urlMain1]
                        placeholderImage:[UIImage imageNamed:@"avatar square"]];
        
        cell.userImage1.contentMode = UIViewContentModeScaleAspectFill;
        cell.userImage1.clipsToBounds = YES;
        
   //     [UIImage makeRoundedImage:cell.userImage1 withRadius:25];
   //     [cell.userImage1.layer setBorderColor:[UIColor whiteColor].CGColor];
   //     [cell.userImage1.layer setBorderWidth:2.0f];
        
        
        // profile pic button
        UIButton *picButton1 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 153, 125)];
        [picButton1 addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
        picButton1.tag = iMatch.player1.userID;
        [cell.userImage1 addSubview:picButton1];
        cell.userImage1.userInteractionEnabled = YES;
        // end profile pic button
        
        
        // player2 pic
        NSString *urlMain2 = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                     baseString:[MyHelpers getNewPicURLIfFacebookSquareSize:iMatch.player2.profileBaseString]];
        
        [cell.userImage2 setImageWithURL:[NSURL URLWithString:urlMain2]
                        placeholderImage:[UIImage imageNamed:@"avatar square"]];
        
        cell.userImage2.contentMode = UIViewContentModeScaleAspectFill;
        cell.userImage2.clipsToBounds = YES;
        
  //      [UIImage makeRoundedImage:cell.userImage2 withRadius:25];
  //      [cell.userImage2.layer setBorderColor:[UIColor whiteColor].CGColor];
  //      [cell.userImage2.layer setBorderWidth:2.0f];
        
        
        // profile pic button
        UIButton *picButton2 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 153, 125)];
        [picButton2 addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
        picButton2.tag = iMatch.player2.userID;
        [cell.userImage2 addSubview:picButton2];
        cell.userImage2.userInteractionEnabled = YES;
        // end profile pic button
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        
        cell.player1Level.text = [NSString stringWithFormat:@"%ld", [[iMatch.player1 getLevelForSport:iMatch.sport.sportID] integerValue]];
        cell.player2Level.text = [NSString stringWithFormat:@"%ld", [[iMatch.player2 getLevelForSport:iMatch.sport.sportID] integerValue]];
        
        cell.sportLevelsHeader.text = [NSString stringWithFormat:@"New %@ Levels", [iMatch.sport.sportName capitalizedString]];
        cell.headlineMessage.text = [iMatch getMatchStatusMessage:[self.preferences getUserID]];
        
        
        
        [cell.scoreCV reloadData];
     
        cell.detailsButton.tag =indexPath.section;
        [cell.detailsButton addTarget:self action:@selector(matchDetailsIndividualMatchCreation:) forControlEvents:UIControlEventTouchUpInside];

        UIButton *newsfeedIconButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
        [newsfeedIconButton addTarget:self action:@selector(matchDetailsIndividualMatchCreation:) forControlEvents:UIControlEventTouchUpInside];
        newsfeedIconButton.tag = indexPath.section;
        [cell.newsfeedIcon addSubview:newsfeedIconButton];
        cell.newsfeedIcon.userInteractionEnabled = YES;

        
    //    cell.commentButton.tag = indexPath.section;
     //   cell.remixButton.tag = indexPath.section;
     
        if ([mpn.sharedByPlayer.userName length] > 0) {
            cell.sharedByLabel.text = [NSString stringWithFormat:@"Shared by %@", mpn.sharedByPlayer.userName];
        } else {
            cell.sharedByLabel.text = [NSString stringWithFormat:@"Created by %@", mpn.match.matchCreatorUsername];
        }

        
        
        /*************** SHOW MATCH HISTORY ********************/
        /*
        NSInteger maxVisibleMatchHistory;
        
        if ([iMatch.matchHistoryArray count] > 3)
            maxVisibleMatchHistory = 3;
        else
            maxVisibleMatchHistory = [iMatch.matchHistoryArray count];
        
        NSInteger startPoint = 370;
        
        NSInteger myCounter = 0;
        for (MatchHistoryObj *matchHist in iMatch.matchHistoryArray) {
            SpecialUILabel *updateLine = [[SpecialUILabel alloc] initWithFrame:CGRectMake(20, startPoint, 250, 15)];
            
            if ([matchHist.participant.userName isKindOfClass:[NSNull class]]) {
                NSString *firstCapChar = [[matchHist.update substringToIndex:1] capitalizedString];
                NSString *cappedString = [matchHist.update stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:firstCapChar];
                updateLine.text = [NSString stringWithFormat:@"%@", cappedString];
            }   else
                updateLine.text = [NSString stringWithFormat:@"%@ %@", matchHist.participant.userName, matchHist.update];
            
            updateLine.font = [UIFont systemFontOfSize:12];
            updateLine.textColor = [UIColor darkGrayColor];
            updateLine.tag = 1;
            [updateLine sizeToFit];
            updateLine.shouldRemoveFromMySuperview = YES;
            [cell.contentView addSubview:updateLine];
            
            SpecialUILabel *timeLabel = [[SpecialUILabel alloc] initWithFrame:CGRectMake(updateLine.frame.size.width + 30, startPoint, 20, 15)];
            timeLabel.text = [NSDate timeDiffCalc:matchHist.occurrenceDate];
            timeLabel.font = [UIFont fontWithName:@"HelveticaNeue-LightItalic" size:12];
            timeLabel.textColor = [UIColor lightGrayColor];
            [timeLabel setTextAlignment:NSTextAlignmentLeft];
            [timeLabel sizeToFit];
            timeLabel.tag = 1;
            timeLabel.shouldRemoveFromMySuperview = YES;
            [cell.contentView addSubview:timeLabel];
            
            startPoint += 20;
            myCounter ++;
            
            if (myCounter == maxVisibleMatchHistory)
                break;
            
            
        }
         */
        
        /*************** END SHOW MATCH HISTORY ********************/
        
     //   cell.contentView.frame = CGRectMake(0, 0, self.tableView.frame.size.width, [self tableView:tableView heightForRowAtIndexPath:indexPath]);

        
        cell.commentButtonIcon.image = [[UIImage imageNamed:@"Comment Small Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [cell.commentButtonIcon setTintColor:[UIColor darkGrayColor2]];
        
        
        [cell.myBackgroundViewCell makeRoundedBorderWithRadius:3];
        cell.myBackgroundViewCell.layer.borderColor = [UIColor midGray2].CGColor;
        cell.myBackgroundViewCell.layer.borderWidth = 1.0;
        
        
        // Show comment button
        
        [cell.commentButton addTarget:self action:@selector(commentPost:) forControlEvents:UIControlEventTouchUpInside];
        cell.commentButton.tag = indexPath.section;
        cell.numCommentLabel.text = [NSString stringWithFormat:@"%lu comments", mpn.numComments];
        
        
        
        [cell.likeButton addTarget:self action:@selector(likePost:) forControlEvents:UIControlEventTouchUpInside];
        cell.likeButton.tag = indexPath.section;
        cell.likeNumberLabel.text = [NSString stringWithFormat:@"%lu props", mpn.numLikes];
        
        cell.likeIcon.image = [[UIImage imageNamed:@"Like Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];

        if (mpn.doesSessionUserLikePost)
            [cell.likeIcon setTintColor:[UIColor goldColor]];
        else
            [cell.likeIcon setTintColor:[UIColor darkGrayColor2]];
        
        
        
        [cell.smallCircleView makeRoundedBorderWithRadius:2];
        cell.smallCircleView.backgroundColor = [UIColor darkGrayColor2];
        cell.commentNumberLabel.textColor = [UIColor darkGrayColor2];
        cell.likeNumberLabel.textColor = [UIColor darkGrayColor2];
        
        cell.viewLikesButton.tag = indexPath.section;
        cell.viewCommentsButton.tag = indexPath.section;
        
        [cell.viewLikesButton addTarget:self action:@selector(viewLikes:) forControlEvents:UIControlEventTouchUpInside];
        [cell.viewCommentsButton addTarget:self action:@selector(viewComments:) forControlEvents:UIControlEventTouchUpInside];

        
        
        
        return cell;
        
        
    } /*else if ([(self.newsfeedPaginator.results)[indexPath.section] isKindOfClass:[MatchPlayerNews class]]) {
        
        MatchPlayerNews *mpn = [self.newsfeedPaginator.results objectAtIndex:indexPath.section];
        
        ////////////////////// for scores ///////////////////////
        if ([mpn.match isKindOfClass:[IndividualMatch class]]) {
            IndividualMatch *iMatch = (IndividualMatch *)mpn.match;
            
            if (iMatch.scoreIndMatch.scoreRecordedBy != 0 || iMatch.scoreIndMatch.scoreConfirmedBy != 0 || iMatch.scoreIndMatch.scoreDisputedBy != 0) {
                // If we are here, then we know it's an individual match and a score was either recorded, confirmed, or disputed
                NSString *CellIdentifier = @"Match News Score Update Cell";
                MatchNewsScoreUpdateCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
           
                cell.iMatch = iMatch;
                [[cell.detailButtonView layer] setBorderWidth:1.0f];
                [[cell.detailButtonView layer] setBorderColor:[UIColor midGray2].CGColor];
                [[cell.commentButtonView layer] setBorderWidth:1.0f];
                [[cell.commentButtonView layer] setBorderColor:[UIColor midGray2].CGColor];
                [[cell.shareButtonView layer] setBorderWidth:1.0f];
                [[cell.shareButtonView layer] setBorderColor:[UIColor midGray2].CGColor];
                
                cell.numCommentLabel.text = [NSString stringWithFormat:@"%d comments ", mpn.numComments];
                cell.numRemixLabel.text = [NSString stringWithFormat:@"%d shares ", mpn.numRemixes];
                
                NSString *urlMain = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                            baseString:mpn.player.profileBaseString];
                
                [cell.picView setImageWithURL:[NSURL URLWithString:urlMain]
                                placeholderImage:[UIImage imageNamed:@"avatar round"]];
                [UIImage makeRoundedImage:cell.picView withRadius:23];
                [cell.picView.layer setBorderWidth:2.0f];
                [cell.picView.layer setBorderColor:[UIColor whiteColor].CGColor];
                
                // profile pic button
                UIButton *picButton1 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 46, 46)];
                [picButton1 addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
                picButton1.tag = mpn.player.userID;
                [cell.picView addSubview:picButton1];
                cell.picView.userInteractionEnabled = YES;
                // end profile pic button
                

                cell.headlineBackground.backgroundColor = [UIColor skyBlue];
            //    [cell.centerBackgroundView makeRoundedBorderwithColorAndDropshadow:[UIColor lightLightGray]];
                cell.matchNameLabel.text = iMatch.matchName;
            //    cell.picView.image = [[iMatch.sport getSportIcon] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            //    [cell.picView setTintColor:[UIColor whiteColor]];
                cell.headlineMessage.text = mpn.headlineMessageLabel;
                cell.dateTimeLabel.text = [NSDate timeDiffCalc:mpn.newsDate];
                
             //   if (iMatch.scoreIndMatch.scoreRecordedBy == mpn.player.userID) {
                if (iMatch.scoreIndMatch.scoreDisputedBy == mpn.player.userID) {
                    cell.scoreStatusLabel.text = [NSString stringWithFormat:@"%@ disputed the match score", mpn.player.userName];
                    cell.supplementaryMessageLabel.text = [NSString stringWithFormat:@"Both players cannot agree on the match score.  This match cannot be made official."];
                } else {
                    cell.scoreStatusLabel.text = [NSString stringWithFormat:@"%@ recorded the match score" , mpn.player.userName];
                    cell.supplementaryMessageLabel.text = [NSString stringWithFormat:@"This score must be confirmed by %@ to make this match official.", [iMatch usernameOfUserWhoDidNotRecordOrConfirmScore]];
                }
            //    }
                cell.player1Username.text = iMatch.player1.userName;
                cell.player2Username.text = iMatch.player2.userName;
                
                // username1  button
                UIButton *username1Button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 21)];
                [username1Button addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
                username1Button.tag = iMatch.player1.userID;
                [cell.player1Username addSubview:username1Button];
                cell.player1Username.userInteractionEnabled = YES;
                // end username1 button
                
                // username2  button
                UIButton *username2Button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 21)];
                [username2Button addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
                username2Button.tag = iMatch.player2.userID;
                [cell.player2Username addSubview:username2Button];
                cell.player2Username.userInteractionEnabled = YES;
                // end username2 button

                
                cell.delegate = self;
                
                NSMutableDictionary *netScores = [iMatch.scoreIndMatch getNetScore];
                
                NSString *scoreUnitNamePlayer1 = @"";
                NSString *scoreUnitNamePlayer2 = @"";
                
                if ([[netScores objectForKey:@"player1_score"] integerValue] > 1 || [[netScores objectForKey:@"player1_score"] integerValue] == 0) {
                    scoreUnitNamePlayer1 = [NSString stringWithFormat:@"%@s", [iMatch getScoreUnitValue]];
                } else {
                    scoreUnitNamePlayer1 = [iMatch getScoreUnitValue];
                }
                
                if ([[netScores objectForKey:@"player2_score"] integerValue] > 1 || [[netScores objectForKey:@"player2_score"] integerValue] == 0) {
                    scoreUnitNamePlayer2 = [NSString stringWithFormat:@"%@s", [iMatch getScoreUnitValue]];
                } else {
                    scoreUnitNamePlayer2 = [iMatch getScoreUnitValue];
                }
                
                cell.player1ScoreLabel.text = [NSString stringWithFormat:@"%d %@", [[netScores objectForKey:@"player1_score"] integerValue], scoreUnitNamePlayer1];
                cell.player2ScoreLabel.text = [NSString stringWithFormat:@"%d %@", [[netScores objectForKey:@"player2_score"] integerValue], scoreUnitNamePlayer2];
                cell.detailsButton.tag =indexPath.section;
        //        [cell.detailsButton makeCircleWithColorAndBackground:[UIColor peacock] andRadius:5.0];
                
                NSString *matchCategory = nil;
                
                matchCategory = @"Challenge";
           //     [cell.picViewBackground makeCircleWithColorAndBackground:[UIColor lightGreenColor] andRadius:15];
                    
                IndividualMatch *iMatch = (IndividualMatch *)mpn.match;
                cell.competitors = [[NSMutableArray alloc] initWithObjects:iMatch.player1, iMatch.player2, nil];
                cell.matchCategory = @"individual";
                [cell.matchHeadingCV reloadData];

                cell.headlineMessage.text = [NSString stringWithFormat:@"%@ %@ Match Update", [mpn.match.sport.sportName capitalizedString], matchCategory];
                cell.headlineBackground.backgroundColor = [UIColor skyBlue];
                
                cell.commentButton.tag = indexPath.section;
                cell.remixButton.tag = indexPath.section;
                cell.detailsButton.tag = indexPath.section;
                
                if ([mpn.sharedByPlayer.userName length] > 0) {
                    cell.shareDot.hidden = NO;
                    [cell.shareDot makeCircleWithColorAndBackground:[UIColor lightGrayColor] andRadius:3];
                    cell.sharedByLabel.hidden = NO;
                    cell.sharedByLabel.text = [NSString stringWithFormat:@"shared by %@", mpn.sharedByPlayer.userName];
                } else {
                    cell.shareDot.hidden = YES;
                    cell.sharedByLabel.hidden = YES;
                }
                return cell;
            }
        }
        ///////////////////// end for scores ///////////////////////
        
        NSString *CellIdentifier  = @"Match Player News Cell";
        MatchNewsPlayerUpdateCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        
    
        NSString *urlMain = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                    baseString:mpn.player.profileBaseString];
        
        [cell.profilePic setImageWithURL:[NSURL URLWithString:urlMain]
                        placeholderImage:[UIImage imageNamed:@"avatar round"]];
        [UIImage makeRoundedImage:cell.profilePic withRadius:23];
        [cell.profilePic.layer setBorderColor:[UIColor whiteColor].CGColor];
        [cell.profilePic.layer setBorderWidth:2.0f];
        
        // profile pic button
        UIButton *picButton1 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 46, 46)];
        [picButton1 addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
        picButton1.tag = mpn.player.userID;
        [cell.profilePic addSubview:picButton1];
        cell.profilePic.userInteractionEnabled = YES;
        // end profile pic button
        
        cell.detailsButton.tag =indexPath.section;
        
        [[cell.detailButtonView layer] setBorderWidth:1.0f];
        [[cell.detailButtonView layer] setBorderColor:[UIColor midGray2].CGColor];
        [[cell.commentButtonView layer] setBorderWidth:1.0f];
        [[cell.commentButtonView layer] setBorderColor:[UIColor midGray2].CGColor];
        [[cell.shareButtonView layer] setBorderWidth:1.0f];
        [[cell.shareButtonView layer] setBorderColor:[UIColor midGray2].CGColor];
        
        cell.numCommentLabel.text = [NSString stringWithFormat:@"%d comments ", mpn.numComments];
        cell.numRemixLabel.text = [NSString stringWithFormat:@"%d shares ", mpn.numRemixes];
        
        cell.timeLabel.text = [NSDate timeDiffCalc:mpn.newsDate];

        if ([mpn.match isKindOfClass:[IndividualMatch class]]) {
            cell.headlineBackground.backgroundColor = [UIColor skyBlue];
            cell.matchMessageLabel.text = [NSString stringWithFormat:@"%@ %@", mpn.player.userName, mpn.matchGeneralMessage];
        } else if ([mpn.match isKindOfClass:[PickupMatch class]]) {
            cell.headlineBackground.backgroundColor = [UIColor iRed];
            cell.matchMessageLabel.text = [NSString stringWithFormat:@"%@ %@", mpn.player.userName, mpn.matchGeneralMessage];
        }
  
        cell.matchNameLabel.text =mpn.match.matchName;
      //  [cell.centerViewBackground makeRoundedBorderwithColorAndDropshadow:[UIColor lightLightGray]];
      //  cell.playerActionMessage.text = [NSString stringWithFormat:@"Level %d (%@)",  [[mpn.player getLevelForSport:mpn.match.sport.sportID] integerValue], [mpn.match.sport.sportName capitalizedString]];
        
        NSString *matchCategory = nil;
        
        if ([mpn.match isKindOfClass:[IndividualMatch class]]) {
            matchCategory = @"Challenge";
            [cell.sportIconBackground makeCircleWithColorAndBackground:[UIColor lightGreenColor] andRadius:15];
            
            IndividualMatch *iMatch = (IndividualMatch *)mpn.match;
            cell.competitors = [[NSMutableArray alloc] initWithObjects:iMatch.player1, iMatch.player2, nil];
            cell.matchCategory = @"individual";
            [cell.matchHeadingCV reloadData];
        } else {
            matchCategory = @"Pickup";
            [cell.sportIconBackground makeCircleWithColorAndBackground:[UIColor sienna] andRadius:15];
            
            PickupMatch *pMatch = (PickupMatch *)mpn.match;
            cell.competitors = [[NSMutableArray alloc] initWithArray:pMatch.competitorArray];
            cell.matchCategory = @"pickup";
            [cell.matchHeadingCV reloadData];
            
            
        }
        
        cell.delegate = self;
        
        cell.commentButton.tag = indexPath.section;
        cell.remixButton.tag = indexPath.section;
        cell.detailsButton.tag = indexPath.section;
         cell.headlineLabel.text = [NSString stringWithFormat:@"%@ %@ Match", [mpn.match.sport.sportName capitalizedString], matchCategory];
        
        if ([mpn.sharedByPlayer.userName length] > 0) {
            cell.shareDot.hidden = NO;
            [cell.shareDot makeCircleWithColorAndBackground:[UIColor lightGrayColor] andRadius:3];
            cell.sharedByLabel.hidden = NO;
            cell.sharedByLabel.text = [NSString stringWithFormat:@"shared by %@", mpn.sharedByPlayer.userName];
        } else {
            cell.shareDot.hidden = YES;
            cell.sharedByLabel.hidden = YES;
        }
        
        return  cell;
    } else if ([(self.newsfeedPaginator.results)[indexPath.section] isKindOfClass:[MatchNews class]]) {

        NSString *CellIdentifier  = @"General Match News Cell";
        MatchNewsMatchUpdateCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        
        MatchNews *mn = [self.newsfeedPaginator.results objectAtIndex:indexPath.section];
      
        
        [[cell.detailButtonView layer] setBorderWidth:1.0f];
        [[cell.detailButtonView layer] setBorderColor:[UIColor midGray2].CGColor];
        [[cell.commentButtonView layer] setBorderWidth:1.0f];
        [[cell.commentButtonView layer] setBorderColor:[UIColor midGray2].CGColor];
        [[cell.shareButtonView layer] setBorderWidth:1.0f];
        [[cell.shareButtonView layer] setBorderColor:[UIColor midGray2].CGColor];
        
        cell.numCommentLabel.text = [NSString stringWithFormat:@"%d comments ", mn.numComments];
        cell.numRemixLabel.text = [NSString stringWithFormat:@"%d shares ", mn.numRemixes];
    
        cell.sportIcon.image = [[mn.match.sport getSportIcon] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [cell.sportIcon setTintColor:[UIColor whiteColor]];
        
        cell.matchName.text = mn.match.matchName;
        cell.generalMessageLabel.text = [NSString stringWithFormat:@"%@%@",[[mn.matchGeneralMessage substringToIndex:1] uppercaseString],[mn.matchGeneralMessage substringFromIndex:1] ];
  //      cell.generalMessageLabel.textColor = [UIColor salmonColor];
  //      [cell.messageBackgroundView makeRoundedBorderwithColorAndDropshadow:[UIColor lightLightGray]];
        
        NSString *matchCategory = nil;
        
        if ([mn.match isKindOfClass:[IndividualMatch class]]) {
            matchCategory = @"Challenge";
            [cell.sportIconBackground makeCircleWithColorAndBackground:[UIColor lightGreenColor] andRadius:15];
            
            IndividualMatch *iMatch = (IndividualMatch *)mn.match;
            cell.competitors = [[NSMutableArray alloc] initWithObjects:iMatch.player1, iMatch.player2, nil];
            cell.matchCategory = @"individual";
            [cell.matchHeadingCV reloadData];
            cell.headlineAbbreviation.text = @"C";
            cell.headlineMatchType.text = @"Challenge";
            cell.headlineBackground.backgroundColor = [UIColor skyBlue];
        } else {
            matchCategory = @"Pickup";
            [cell.sportIconBackground makeCircleWithColorAndBackground:[UIColor sienna] andRadius:15];
            
            PickupMatch *pMatch = (PickupMatch *)mn.match;
            cell.competitors = [[NSMutableArray alloc] initWithArray:pMatch.competitorArray];
            cell.matchCategory = @"pickup";
            [cell.matchHeadingCV reloadData];
            cell.headlineAbbreviation.text = @"P";
            cell.headlineMatchType.text = @"Pickup";
            cell.headlineBackground.backgroundColor = [UIColor iRed];
            
        }
        
        cell.headlineMessageLabel.text = [NSString stringWithFormat:@"%@ %@ Match Update", [mn.match.sport.sportName capitalizedString], matchCategory];
        
        cell.bulbIcon.image = [[UIImage imageNamed:@"Bulb Icon"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [cell.bulbIcon setTintColor:[UIColor whiteColor]];
        [cell.bulbBackground makeCircleWithColorAndBackground:[UIColor peacock] andRadius:17];
        
        cell.matchLocationLabel.text = mn.match.venue.venueName;
        cell.timeLabel.text = [NSDate timeDiffCalc:mn.newsDate];
        cell.detailButton.tag = indexPath.section;
        
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
        [dateFormatter1 setDateFormat:@"MMMM d"];
        
        NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
        [dateFormatter2 setDateFormat:@"h:mm a"];
        
        if ([mn.match isKindOfClass:[IndividualMatch class]]) {
            cell.matchCategoryLabel.text = @"Challenge Match";
        } else if ([mn.match isKindOfClass:[PickupMatch class]]){
            cell.matchCategoryLabel.text = @"Pickup Match";
        }
        
    //    [cell.detailButton makeCircleWithColorAndBackground:[UIColor peacock] andRadius:5.0];
     //   [cell.messageBackgroundView makeRoundedBorderWithColor:[UIColor lightLightGray]];
        
        cell.matchDateTimeLabel.text =  [NSString stringWithFormat:@"%@ at %@", [dateFormatter1 stringFromDate:mn.match.dateTime], [dateFormatter2 stringFromDate:mn.match.dateTime]];
        
        cell.delegate = self;
        
        cell.commentButton.tag = indexPath.section;
        cell.remixButton.tag = indexPath.section;
        cell.detailButton.tag = indexPath.section;
        
        if ([mn.sharedByPlayer.userName length] > 0) {
            cell.shareDot.hidden = NO;
            [cell.shareDot makeCircleWithColorAndBackground:[UIColor lightGrayColor] andRadius:3];
            cell.sharedByLabel.hidden = NO;
            cell.sharedByLabel.text = [NSString stringWithFormat:@"shared by %@", mn.sharedByPlayer.userName];
        } else {
            cell.shareDot.hidden = YES;
            cell.sharedByLabel.hidden = YES;
        }
        
        
        return cell;
        
    } */ else if ([(self.newsfeedPaginator.results)[indexPath.section] isKindOfClass:[CompetitionNews class]]) {
        
        CompetitionNewsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Competition Cell" forIndexPath:indexPath];
        
        // clear subviews
        if ([cell.contentView subviews]){
            for (UIView *subview in [cell.contentView subviews]) {
                if ([subview isKindOfClass:[SpecialUIButton class]]) {
                    SpecialUIButton * newSubview = (SpecialUIButton *)subview;
                    if (newSubview.shouldRemoveFromMySuperview)  // only items I created programmatically
                        [subview removeFromSuperview];
                    
                } else if ([subview isKindOfClass:[SpecialUILabel class]]) {
                    SpecialUILabel *newSubview = (SpecialUILabel *)subview;
                    if (newSubview.shouldRemoveFromMySuperview)
                        [subview removeFromSuperview];
                }
                
            }
        }
        
        
        CompetitionNews *cPost = (CompetitionNews *)(self.newsfeedPaginator.results)[indexPath.section];
        [cell.actionButtonHandle addTarget:self action:@selector(postExtraActions:) forControlEvents:UIControlEventTouchUpInside];
        cell.actionButtonHandle.tag = indexPath.section;
        
        cell.usernameLabel.text = cPost.newsMakerUserName;
      //  cell.messageLabel.text = [NSString stringWithFormat:@"posted a photo"];
        
        NSString *urlMain = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                    baseString:cPost.profileBaseString];
        
        [cell.profilePic setImageWithURL:[NSURL URLWithString:urlMain]
                        placeholderImage:[UIImage imageNamed:@"avatar square"]];

    //                    placeholderImage:[[UIImage imageNamed:@"avatar round"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]];
        [UIImage makeRoundedImage:cell.profilePic withRadius:30];
    
        cell.profilePic.layer.borderColor = [UIColor whiteColor].CGColor;
        cell.profilePic.layer.borderWidth = 1.0;
    
        cell.sportIcon.image = [[UIImage imageNamed:@"U Bar Button Logo"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [cell.sportIcon setTintColor:[UIColor whiteColor]];
   
        // profile pic button
        UIButton *picButton1 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];
        [picButton1 addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
        picButton1.tag = cPost.newsMakerUserID;
        [cell.profilePic addSubview:picButton1];
        cell.profilePic.userInteractionEnabled = YES;
        // end profile pic button
        
        
        cell.postDateLabel.text = [NSDate timeDiffCalc:cPost.newsDate];
        
        
        
        if ([cPost.sharedByPlayer.userName length] > 0) {
            cell.sharedByLabel.text = [NSString stringWithFormat:@"shared by %@", cPost.sharedByPlayer.userName];
            
        } else {
            cell.sharedByLabel.text = @"Basketball";
        }
        
        [cell.myCellBackgroundView makeRoundedBorderWithRadius:3];
        cell.myCellBackgroundView.layer.borderColor = [UIColor midGray2].CGColor;
        cell.myCellBackgroundView.layer.borderWidth = 1.0;
        
        if ([cPost.universityOrganization length] > 0) {
            cell.detailCompetitionTextLabel.text = [NSString stringWithFormat:@"@%@ is playing in the Vaiden Games and is representing the %@ group of %@", cPost.newsMakerUserName, cPost.universityOrganization, cPost.universityName];
        } else {
            cell.detailCompetitionTextLabel.text = [NSString stringWithFormat:@"@%@ is playing in the Vaiden Games and is representing %@",cPost.newsMakerUserName, cPost.universityName];
        }
        //    CGFloat cellHeight = [self calculateHeightForImagePost:indexPath.section];
        
        cell.contentView.frame = CGRectMake(0, 0, self.tableView.frame.size.width, [self tableView:tableView heightForRowAtIndexPath:indexPath]);
        
        
        // Show comment button
        
        [cell.commentButton addTarget:self action:@selector(commentPost:) forControlEvents:UIControlEventTouchUpInside];
        cell.commentButton.tag = indexPath.section;
        cell.numCommentLabel.text = [NSString stringWithFormat:@"%lu", cPost.numComments];
        if (cPost.numComments == 1)
            cell.commentTextLabel.text = @"Comment";
        else
            cell.commentTextLabel.text = @"Comments";
        
        if (cPost.numRemixes == 1)
            cell.shareTextLabel.text = @"Share";
        else
            cell.shareTextLabel.text = @"Shares";
        
        
        [cell.shareButton addTarget:self action:@selector(remixCompetitionPost:) forControlEvents:UIControlEventTouchUpInside];
        cell.shareButton.tag = indexPath.section;
        cell.numShareLabel.text = [NSString stringWithFormat:@"%lu", cPost.numRemixes];
        
        [cell.commentButtonBackground makeRoundedBorderWithRadius:3];
        [cell.shareButtonBackground makeRoundedBorderWithRadius:3];
        cell.commentButtonBackground.layer.borderColor = [UIColor grayBorder1].CGColor;
        cell.commentButtonBackground.layer.borderWidth = 1.0;
        cell.shareButtonBackground.layer.borderColor = [UIColor grayBorder1].CGColor;
        cell.shareButtonBackground.layer.borderWidth = 1.0;
        
        cell.commentIcon.image = [[UIImage imageNamed:@"Comment Small Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [cell.commentIcon setTintColor:[UIColor lightGrayColor]];
        
        cell.shareIcon.image = [[UIImage imageNamed:@"Remix Share Small Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [cell.shareIcon setTintColor:[UIColor lightGrayColor]];
        
        [cell.likeButton addTarget:self action:@selector(likePost:) forControlEvents:UIControlEventTouchUpInside];
        cell.likeButton.tag = indexPath.section;
        cell.likeNumberLabel.text = [NSString stringWithFormat:@"%lu", cPost.numLikes];
        
        cell.likeIcon.image = [[UIImage imageNamed:@"Like Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];

        
        if (cPost.doesSessionUserLikePost)
            [cell.likeIcon setTintColor:[UIColor salmonColor]];
        else
            [cell.likeIcon setTintColor:[UIColor lightGrayColor]];
        
        [cell.likeButtonBackground makeRoundedBorderWithRadius:3];
        cell.likeButtonBackground.layer.borderColor = [UIColor grayBorder1].CGColor;
        cell.likeButtonBackground.layer.borderWidth = 1.0;
        
   
        return cell;
        
        
    }  else if ([(self.newsfeedPaginator.results)[indexPath.section] isKindOfClass:[VenueKingNews class]]) {
        
        VenueKingCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Venue King Cell" forIndexPath:indexPath];
        
        // clear subviews
        if ([cell.contentView subviews]){
            for (UIView *subview in [cell.contentView subviews]) {
                if ([subview isKindOfClass:[SpecialUIButton class]]) {
                    SpecialUIButton * newSubview = (SpecialUIButton *)subview;
                    if (newSubview.shouldRemoveFromMySuperview)  // only items I created programmatically
                        [subview removeFromSuperview];
                    
                } else if ([subview isKindOfClass:[SpecialUILabel class]]) {
                    SpecialUILabel *newSubview = (SpecialUILabel *)subview;
                    if (newSubview.shouldRemoveFromMySuperview)
                        [subview removeFromSuperview];
                }
                
            }
        }
        
        
        VenueKingNews *vPost = (VenueKingNews *)(self.newsfeedPaginator.results)[indexPath.section];
        [cell.actionButtonHandle addTarget:self action:@selector(postExtraActions:) forControlEvents:UIControlEventTouchUpInside];
        cell.actionButtonHandle.tag = indexPath.section;
    //    cell.updatePostLabel.text = [NSString stringWithFormat:@"@%@ has become king of %@ for %@", vPost.newsMakerUserName, vPost.venue.venueName, [vPost.sport.sportName capitalizedString]];
    //    cell.venueNameLabel.text = vPost.venue.venueName;
   //     cell.usernameLabel.text = vPost.newsMakerUserName;
     
   //     cell.kingMessageLabel.text = [NSString stringWithFormat:@"Became King of the Venue for %@ at %@", [vPost.sport.sportName capitalizedString], vPost.venue.venueName];
        
        
        NSString *kingString = [NSString stringWithFormat:@"%@ became King of the Venue for %@ at", vPost.newsMakerUserName, [vPost.sport.sportName capitalizedString]];
        
        NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ %@", kingString, vPost.venue.venueName]];
        
        [string addAttribute:NSForegroundColorAttributeName value:[UIColor goldColor] range:NSMakeRange([kingString length] + 1, [vPost.venue.venueName length])];
        cell.kingMessageLabel.attributedText = string;
        
     /*
        UIButton *venueButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 280, 21)];
        [venueButton addTarget:self action:@selector(goToVenue:) forControlEvents:UIControlEventTouchUpInside];
        venueButton.tag = vPost.venue.venueID;
        [cell.kingMessageLabel addSubview:venueButton];
        cell.kingMessageLabel.userInteractionEnabled = YES;
    */
        NSString *urlMain = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                    baseString:vPost.profileBaseString];
        
        [cell.profilePic setImageWithURL:[NSURL URLWithString:urlMain]
                        placeholderImage:[UIImage imageNamed:@"avatar square"]];
        //                   placeholderImage:[[UIImage imageNamed:@"avatar round"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]];

//         [UIImage makeRoundedImage:cell.profilePic withRadius:30];
        [cell.profilePic makeRoundedBorderWithRadius:10];
        cell.profilePic.layer.borderColor = [UIColor clearColor].CGColor;
        
        // profile pic button
        UIButton *picButton1 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];
        [picButton1 addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
        picButton1.tag = vPost.newsMakerUserID;
        [cell.profilePic addSubview:picButton1];
        cell.profilePic.userInteractionEnabled = YES;
        // end profile pic button
        
        
        cell.postDateTimeLabel.text = [NSDate timeDiffCalc:vPost.newsDate];
        
        if ([vPost.sharedByPlayer.userName length] > 0) {
            cell.sharedByLabel.hidden = NO;
            cell.sharedByLabel.text = [NSString stringWithFormat:@"shared by %@", vPost.sharedByPlayer.userName];
            
        } else {
            cell.sharedByLabel.hidden = YES;
        }
        
        //    CGFloat cellHeight = [self calculateHeightForImagePost:indexPath.section];
        
    //    cell.contentView.frame = CGRectMake(0, 0, self.tableView.frame.size.width, [self tableView:tableView heightForRowAtIndexPath:indexPath]);
        
        [cell.mainCellBackgroundView makeRoundedBorderWithRadius:3];
        cell.mainCellBackgroundView.layer.borderColor = [UIColor midGray2].CGColor;
        cell.mainCellBackgroundView.layer.borderWidth = 1.0;
        
        // venue pic
        NSString *venueURLMain = [S3Tools getFileNameStringWithType:@"venue_pic"
                                                         baseString:vPost.venue.venueImageThumb.venuePicString];
        
        
        [cell.venueThumb setImageWithURL:[NSURL URLWithString:venueURLMain]
                        placeholderImage:[UIImage imageNamed:@"Venue Default"]];

        cell.venueThumb.contentMode = UIViewContentModeScaleAspectFill;
        cell.venueThumb.clipsToBounds = YES;
        
        // Show comment button
        
        [cell.commentButton addTarget:self action:@selector(commentPost:) forControlEvents:UIControlEventTouchUpInside];
        cell.commentButton.tag = indexPath.section;
        cell.numCommentLabel.text = [NSString stringWithFormat:@"%lu comments", vPost.numComments];

        
      cell.commentIcon.image = [[UIImage imageNamed:@"Comment Small Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [cell.commentIcon setTintColor:[UIColor darkGrayColor2]];
        
        
        cell.venueDetailsButton.tag = vPost.venue.venueID;
        [cell.venueDetailsButton addTarget:self action:@selector(goToVenue:) forControlEvents:UIControlEventTouchUpInside];

        
        UIButton *newsfeedIconButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
        [newsfeedIconButton addTarget:self action:@selector(goToVenue:) forControlEvents:UIControlEventTouchUpInside];
        newsfeedIconButton.tag = vPost.venue.venueID;
        [cell.newsfeedIcon addSubview:newsfeedIconButton];
        cell.newsfeedIcon.userInteractionEnabled = YES;

        
        [cell.likeButton addTarget:self action:@selector(likePost:) forControlEvents:UIControlEventTouchUpInside];
        cell.likeButton.tag = indexPath.section;
        cell.likeNumberLabel.text = [NSString stringWithFormat:@"%lu props", vPost.numLikes];
        
        cell.likeIcon.image = [[UIImage imageNamed:@"Like Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];

        if (vPost.doesSessionUserLikePost)
            [cell.likeIcon setTintColor:[UIColor goldColor]];
        else
            [cell.likeIcon setTintColor:[UIColor darkGrayColor2]];
  
        
        [cell.smallCircleView makeRoundedBorderWithRadius:2];
        cell.smallCircleView.backgroundColor = [UIColor darkGrayColor2];
        cell.numCommentLabel.textColor = [UIColor darkGrayColor2];
        cell.likeNumberLabel.textColor = [UIColor darkGrayColor2];
        
        cell.viewLikesButton.tag = indexPath.section;
        cell.viewCommentsButton.tag = indexPath.section;
        
        [cell.viewLikesButton addTarget:self action:@selector(viewLikes:) forControlEvents:UIControlEventTouchUpInside];
        [cell.viewCommentsButton addTarget:self action:@selector(viewComments:) forControlEvents:UIControlEventTouchUpInside];
        
        return cell;
    } else if ([(self.newsfeedPaginator.results)[indexPath.section] isKindOfClass:[VenueCheckinNews class]]) {
        
        VenueCheckinCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Checkin Cell" forIndexPath:indexPath];
        
        // clear subviews
        if ([cell.contentView subviews]){
            for (UIView *subview in [cell.contentView subviews]) {
                if ([subview isKindOfClass:[SpecialUIButton class]]) {
                    SpecialUIButton * newSubview = (SpecialUIButton *)subview;
                    if (newSubview.shouldRemoveFromMySuperview)  // only items I created programmatically
                        [subview removeFromSuperview];
                    
                } else if ([subview isKindOfClass:[SpecialUILabel class]]) {
                    SpecialUILabel *newSubview = (SpecialUILabel *)subview;
                    if (newSubview.shouldRemoveFromMySuperview)
                        [subview removeFromSuperview];
                }
                
            }
        }
        
        
        VenueCheckinNews *vPost = (VenueCheckinNews *)(self.newsfeedPaginator.results)[indexPath.section];
        
        [cell.actionButtonHandle addTarget:self action:@selector(postExtraActions:) forControlEvents:UIControlEventTouchUpInside];
        cell.actionButtonHandle.tag = indexPath.section;
   /*     if ([vPost.sharedByPlayer.userName length] > 0) {
            cell.sharedByLabel.hidden = NO;
            cell.sharedByLabel.text = [NSString stringWithFormat:@"shared by %@", vPost.sharedByPlayer.userName];
            
        } else {
            cell.sharedByLabel.hidden = YES;
        }
*/
        cell.contentView.frame = CGRectMake(0, 0, self.tableView.frame.size.width, [self tableView:tableView heightForRowAtIndexPath:indexPath]);
        
        [cell.mainCellBackgroundView makeRoundedBorderWithRadius:3];
        cell.mainCellBackgroundView.layer.borderColor = [UIColor midGray2].CGColor;
        cell.mainCellBackgroundView.layer.borderWidth = 1.0;
        cell.usernameLabel.text = vPost.vci.checkedInUser.userName;
        
        
        NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                baseString:vPost.vci.checkedInUser.profileBaseString];
        
        [cell.profilePic setImageWithURL:[NSURL URLWithString:url]
                        placeholderImage:[UIImage imageNamed:@"avatar square"]];
      //  [UIImage makeRoundedImage:cell.profilePic withRadius:30];
        [cell.profilePic makeRoundedBorderWithRadius:10];
        
        [cell.profilePic.layer setBorderWidth:2.0f];
        [cell.profilePic.layer setBorderColor:[UIColor whiteColor].CGColor];

        // profile pic button
        UIButton *picButton1 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];
        [picButton1 addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
        picButton1.tag = vPost.newsMakerUserID;
        [cell.profilePic addSubview:picButton1];
        cell.profilePic.userInteractionEnabled = YES;
        
        
        if ([vPost.vci.checkedInSport.sportName length] > 0) {
            cell.sportLabel.hidden = NO;
            cell.sportLabel.text = [NSString stringWithFormat:@"to play %@", [vPost.vci.checkedInSport.sportName capitalizedString]];
        } else
            cell.sportLabel.hidden = YES;
        
    //    cell.messageLabel.text = [vPost.vci getTimeStatusMessage:NO];
        cell.detail2Label.text = vPost.venue.venueName;
        
        
        if ([vPost.vci.message length] > 0) {
            NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ - Playing %@ at %@", vPost.vci.message, [vPost.vci.checkedInSport.sportName capitalizedString], vPost.venue.venueName]];
            
            [string addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, [vPost.vci.message length])];
            cell.messageLabel.attributedText = string;
        } else
            cell.messageLabel.text = [NSString stringWithFormat:@"Playing %@ at %@", [vPost.vci.checkedInSport.sportName capitalizedString], vPost.venue.venueName];
        
        
        
        
        cell.detail1Label.text = [vPost.vci getAbbrevTimeStatusMessage:NO];
  //      [cell.timeBackgroundView makeRoundedBorderWithRadius:3];
        
        UIButton *venueButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 280, 21)];
        [venueButton addTarget:self action:@selector(goToVenue:) forControlEvents:UIControlEventTouchUpInside];
        venueButton.tag = vPost.venue.venueID;
        [cell.detail2Label addSubview:venueButton];
        cell.detail2Label.userInteractionEnabled = YES;
        
        
        cell.postDateTimeLabel.text = [NSDate timeDiffCalc:vPost.newsDate];
        
        [cell.commentButton addTarget:self action:@selector(commentPost:) forControlEvents:UIControlEventTouchUpInside];
        cell.commentButton.tag = indexPath.section;
        cell.numCommentLabel.text = [NSString stringWithFormat:@"%lu comments", vPost.numComments];
        
        [cell.smallCircleView makeRoundedBorderWithRadius:2];
        cell.smallCircleView.backgroundColor = [UIColor darkGrayColor2];
        cell.numCommentLabel.textColor = [UIColor darkGrayColor2];
        cell.likeNumberLabel.textColor = [UIColor darkGrayColor2];
        
        
 //       [cell.shareButton addTarget:self action:@selector(remixCheckinPost:) forControlEvents:UIControlEventTouchUpInside];
 //       cell.shareButton.tag = indexPath.section;
 //       cell.numShareLabel.text = [NSString stringWithFormat:@"%lu", vPost.numRemixes];
        
 //       [cell.commentButtonBackgroundView makeRoundedBorderWithRadius:3];
 //       [cell.shareButtonBackgroundView makeRoundedBorderWithRadius:3];
 //       cell.commentButtonBackgroundView.layer.borderColor = [UIColor grayBorder1].CGColor;
 //       cell.commentButtonBackgroundView.layer.borderWidth = 1.0;
 //       cell.shareButtonBackgroundView.layer.borderColor = [UIColor grayBorder1].CGColor;
 //       cell.shareButtonBackgroundView.layer.borderWidth = 1.0;
        
        cell.commentIcon.image = [[UIImage imageNamed:@"Comment Small Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [cell.commentIcon setTintColor:[UIColor darkGrayColor2]];
        
 //       cell.shareIcon.image = [[UIImage imageNamed:@"Remix Share Small Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
 //       [cell.shareIcon setTintColor:[UIColor lightGrayColor]];

        [cell.likeButton addTarget:self action:@selector(likePost:) forControlEvents:UIControlEventTouchUpInside];
        cell.likeButton.tag = indexPath.section;
        cell.likeNumberLabel.text = [NSString stringWithFormat:@"%lu props", vPost.numLikes];
        
        cell.likeIcon.image = [[UIImage imageNamed:@"Like Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];

        if (vPost.doesSessionUserLikePost)
            [cell.likeIcon setTintColor:[UIColor goldColor]];
        else
            [cell.likeIcon setTintColor:[UIColor darkGrayColor2]];
        
        cell.viewLikesButton.tag = indexPath.section;
        cell.viewCommentsButton.tag = indexPath.section;
        cell.venueButton.tag = vPost.venue.venueID;
        
        [cell.viewLikesButton addTarget:self action:@selector(viewLikes:) forControlEvents:UIControlEventTouchUpInside];
        [cell.viewCommentsButton addTarget:self action:@selector(viewComments:) forControlEvents:UIControlEventTouchUpInside];
        [cell.venueButton addTarget:self action:@selector(goToVenue:) forControlEvents:UIControlEventTouchUpInside];
    
        UIButton *newsfeedIconButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
        [newsfeedIconButton addTarget:self action:@selector(goToVenue:) forControlEvents:UIControlEventTouchUpInside];
        newsfeedIconButton.tag = vPost.venue.venueID;
        [cell.newsfeedIcon addSubview:newsfeedIconButton];
        cell.newsfeedIcon.userInteractionEnabled = YES;
    
        
        return cell;
        

    } else if ([(self.newsfeedPaginator.results)[indexPath.section] isKindOfClass:[VoteReceiving class]]) {
        VoteReceiving *vPost = (VoteReceiving *)(self.newsfeedPaginator.results)[indexPath.section];
        if ([vPost.type isEqualToString:@"winner"]) {
            VoteWinnerCell *cell = [tableView dequeueReusableCellWithIdentifier:@"winner_cell" forIndexPath:indexPath];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            // clear subviews
            if ([cell.contentView subviews]){
                for (UIView *subview in [cell.contentView subviews]) {
                    if ([subview isKindOfClass:[SpecialUIButton class]]) {
                        SpecialUIButton * newSubview = (SpecialUIButton *)subview;
                        if (newSubview.shouldRemoveFromMySuperview)  // only items I created programmatically
                            [subview removeFromSuperview];
                        
                    } else if ([subview isKindOfClass:[SpecialUILabel class]]) {
                        SpecialUILabel *newSubview = (SpecialUILabel *)subview;
                        if (newSubview.shouldRemoveFromMySuperview)
                            [subview removeFromSuperview];
                    }
                    
                }
            }
            
            // Action sheet
            [cell.actionSheet addTarget:self action:@selector(postExtraActions:) forControlEvents:UIControlEventTouchUpInside];
            cell.actionSheet.tag = indexPath.section;
            
            // View
            cell.contentView.frame = CGRectMake(0, 0, self.tableView.frame.size.width, [self tableView:tableView heightForRowAtIndexPath:indexPath]);
            [cell.outerView makeRoundedBorderWithRadius:6];
            [cell.innerView makeRoundedBorderWithRadius:5];
            [cell.innerView.layer setBorderColor:[[UIColor colorWithRed:229/255.0 green:229/255.0 blue:229/255.0 alpha:1.0] CGColor]];
            
            // Feed cell
            NSArray * winners = vPost.winners;
            NSString *name1;
            NSString *name2;
            NSString *name3;
            NSString *name4;
            NSString *name5;
            BOOL northeast = NO;
            BOOL southeast = NO;
            BOOL midwest = NO;
            BOOL south = NO;
            BOOL west = NO;
            NSInteger count = 1;
            UIButton *picButton;
            for (int i = 0; i<winners.count; i++) {
                NSDictionary * obj = [winners objectAtIndex:i];
                
                // Set the image
                if ([vPost.region isEqualToString:obj[@"region"]]) { // Winner For All
                    if (([obj[@"region"] isEqualToString:@"Northeast"] && !northeast)||([obj[@"region"] isEqualToString:@"Southeast"] && !southeast)||([obj[@"region"] isEqualToString:@"South"] && !south)||([obj[@"region"] isEqualToString:@"Midwest"] && !midwest)||([obj[@"region"] isEqualToString:@"West"] && !west)) {
                        NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                                baseString:obj[@"profile_pic_string"]];
                        
                        [cell.pic1 setImageWithURL:[NSURL URLWithString:url]
                                  placeholderImage:[UIImage imageNamed:@"avatar round"]];
                        [cell.pic1 makeRoundedBorderWithRadius:24];
                        [cell.pic1.layer setBorderColor:[[UIColor goldColor] CGColor]];
                        [cell.pic1.layer setBorderWidth:3];
                        if ([obj[@"region"] isEqualToString:@"Northeast"]) {
                            northeast = YES;
                        } else if ([obj[@"region"] isEqualToString:@"Southeast"]) {
                            southeast = YES;
                        } else if ([obj[@"region"] isEqualToString:@"South"]) {
                            south = YES;
                        } else if ([obj[@"region"] isEqualToString:@"Midwest"]) {
                            midwest = YES;
                        } else if ([obj[@"region"] isEqualToString:@"West"]) {
                            west = YES;
                        }
                        // Set Pic button
                        NSLog(@"%f-%f-%f-%f",cell.pic1.frame.origin.x,cell.pic1.frame.origin.y,cell.pic1.frame.size.height,cell.pic1.frame.size.width);
                        picButton = [[UIButton alloc] initWithFrame:cell.pic1.frame];
                        [picButton addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
                        picButton.tag = [obj[@"user_id"] integerValue];
                        NSLog(@"%d",[obj[@"user_id"] integerValue]);
                        [cell.contentView addSubview:picButton];
                    }
                } else { // rest of the data
                    if ([obj[@"region"] isEqualToString:@"Northeast"] && !northeast) {
                        NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                                baseString:obj[@"profile_pic_string"]];
                        if (count == 1) {
                            [cell.smallPic1 setImageWithURL:[NSURL URLWithString:url]
                                           placeholderImage:[UIImage imageNamed:@"avatar round"]];
                            [cell.smallPic1 makeRoundedBorderWithRadius:17];
                            // Set Pic button
                            NSLog(@"%f-%f-%f-%f",cell.smallPic1.frame.origin.x,cell.smallPic1.frame.origin.y,cell.smallPic1.frame.size.height,cell.smallPic1.frame.size.width);
                            picButton = [[UIButton alloc] initWithFrame:cell.smallPic1.frame];
                            [picButton addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
                            picButton.tag = [obj[@"user_id"] integerValue];
                            [cell.contentView addSubview:picButton];
                        } else if (count == 2) {
                            [cell.smallPic2 setImageWithURL:[NSURL URLWithString:url]
                                           placeholderImage:[UIImage imageNamed:@"avatar round"]];
                            [cell.smallPic2 makeRoundedBorderWithRadius:17];
                            // Set Pic button
                            NSLog(@"%f-%f-%f-%f",cell.smallPic2.frame.origin.x,cell.smallPic2.frame.origin.y,cell.smallPic2.frame.size.height,cell.smallPic2.frame.size.width);
                            picButton = [[UIButton alloc] initWithFrame:cell.smallPic2.frame];
                            [picButton addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
                            picButton.tag = [obj[@"user_id"] integerValue];
                            [cell.contentView addSubview:picButton];
                        } else if (count == 3) {
                            [cell.smallPic3 setImageWithURL:[NSURL URLWithString:url]
                                           placeholderImage:[UIImage imageNamed:@"avatar round"]];
                            [cell.smallPic3 makeRoundedBorderWithRadius:17];
                            // Set Pic button
                            NSLog(@"%f-%f-%f-%f",cell.smallPic3.frame.origin.x,cell.smallPic3.frame.origin.y,cell.smallPic3.frame.size.height,cell.smallPic3.frame.size.width);
                            picButton = [[UIButton alloc] initWithFrame:cell.smallPic3.frame];
                            [picButton addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
                            picButton.tag = [obj[@"user_id"] integerValue];
                            [cell.contentView addSubview:picButton];
                        } else if (count == 4) {
                            [cell.smallPic4 setImageWithURL:[NSURL URLWithString:url]
                                           placeholderImage:[UIImage imageNamed:@"avatar round"]];
                            [cell.smallPic4 makeRoundedBorderWithRadius:17];
                            // Set Pic button
                            NSLog(@"%f-%f-%f-%f",cell.smallPic4.frame.origin.x,cell.smallPic4.frame.origin.y,cell.smallPic4.frame.size.height,cell.smallPic4.frame.size.width);
                            picButton = [[UIButton alloc] initWithFrame:cell.smallPic4.frame];
                            [picButton addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
                            picButton.tag = [obj[@"user_id"] integerValue];
                            [cell.contentView addSubview:picButton];
                        }
                        
                        count ++;
                        northeast = YES;
                    } else if ([obj[@"region"] isEqualToString:@"Southeast"] && !southeast) {
                        NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                                baseString:obj[@"profile_pic_string"]];
                        if (count == 1) {
                            [cell.smallPic1 setImageWithURL:[NSURL URLWithString:url]
                                           placeholderImage:[UIImage imageNamed:@"avatar round"]];
                            [cell.smallPic1 makeRoundedBorderWithRadius:17];
                            // Set Pic button
                            NSLog(@"%f-%f-%f-%f",cell.smallPic1.frame.origin.x,cell.smallPic1.frame.origin.y,cell.smallPic1.frame.size.height,cell.smallPic1.frame.size.width);
                            picButton = [[UIButton alloc] initWithFrame:cell.smallPic1.frame];
                            [picButton addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
                            picButton.tag = [obj[@"user_id"] integerValue];
                            [cell.contentView addSubview:picButton];
                        } else if (count == 2) {
                            [cell.smallPic2 setImageWithURL:[NSURL URLWithString:url]
                                           placeholderImage:[UIImage imageNamed:@"avatar round"]];
                            [cell.smallPic2 makeRoundedBorderWithRadius:17];
                            // Set Pic button
                            NSLog(@"%f-%f-%f-%f",cell.smallPic2.frame.origin.x,cell.smallPic2.frame.origin.y,cell.smallPic2.frame.size.height,cell.smallPic2.frame.size.width);
                            picButton = [[UIButton alloc] initWithFrame:cell.smallPic2.frame];
                            [picButton addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
                            picButton.tag = [obj[@"user_id"] integerValue];
                            [cell.contentView addSubview:picButton];
                        } else if (count == 3) {
                            [cell.smallPic3 setImageWithURL:[NSURL URLWithString:url]
                                           placeholderImage:[UIImage imageNamed:@"avatar round"]];
                            [cell.smallPic3 makeRoundedBorderWithRadius:17];
                            // Set Pic button
                            NSLog(@"%f-%f-%f-%f",cell.smallPic3.frame.origin.x,cell.smallPic3.frame.origin.y,cell.smallPic3.frame.size.height,cell.smallPic3.frame.size.width);
                            picButton = [[UIButton alloc] initWithFrame:cell.smallPic3.frame];
                            [picButton addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
                            picButton.tag = [obj[@"user_id"] integerValue];
                            [cell.contentView addSubview:picButton];
                        } else if (count == 4) {
                            [cell.smallPic4 setImageWithURL:[NSURL URLWithString:url]
                                           placeholderImage:[UIImage imageNamed:@"avatar round"]];
                            [cell.smallPic4 makeRoundedBorderWithRadius:17];
                            // Set Pic button
                            NSLog(@"%f-%f-%f-%f",cell.smallPic4.frame.origin.x,cell.smallPic4.frame.origin.y,cell.smallPic4.frame.size.height,cell.smallPic4.frame.size.width);
                            picButton = [[UIButton alloc] initWithFrame:cell.smallPic4.frame];
                            [picButton addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
                            picButton.tag = [obj[@"user_id"] integerValue];
                            [cell.contentView addSubview:picButton];
                        }
                        count ++;
                        southeast = YES;
                    } else if ([obj[@"region"] isEqualToString:@"South"] && !south) {
                        NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                                baseString:obj[@"profile_pic_string"]];
                        if (count == 1) {
                            [cell.smallPic1 setImageWithURL:[NSURL URLWithString:url]
                                           placeholderImage:[UIImage imageNamed:@"avatar round"]];
                            [cell.smallPic1 makeRoundedBorderWithRadius:17];
                            // Set Pic button
                            picButton = [[UIButton alloc] initWithFrame:cell.smallPic1.frame];
                            [picButton addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
                            picButton.tag = [obj[@"user_id"] integerValue];
                            [cell.contentView addSubview:picButton];
                        } else if (count == 2) {
                            [cell.smallPic2 setImageWithURL:[NSURL URLWithString:url]
                                           placeholderImage:[UIImage imageNamed:@"avatar round"]];
                            [cell.smallPic2 makeRoundedBorderWithRadius:17];
                            // Set Pic button
                            picButton = [[UIButton alloc] initWithFrame:cell.smallPic2.frame];
                            [picButton addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
                            picButton.tag = [obj[@"user_id"] integerValue];
                            [cell.contentView addSubview:picButton];
                        } else if (count == 3) {
                            [cell.smallPic3 setImageWithURL:[NSURL URLWithString:url]
                                           placeholderImage:[UIImage imageNamed:@"avatar round"]];
                            [cell.smallPic3 makeRoundedBorderWithRadius:17];
                            // Set Pic button
                            picButton = [[UIButton alloc] initWithFrame:cell.smallPic3.frame];
                            [picButton addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
                            picButton.tag = [obj[@"user_id"] integerValue];
                            [cell.contentView addSubview:picButton];
                        } else if (count == 4) {
                            [cell.smallPic4 setImageWithURL:[NSURL URLWithString:url]
                                           placeholderImage:[UIImage imageNamed:@"avatar round"]];
                            [cell.smallPic4 makeRoundedBorderWithRadius:17];
                            [cell.smallPic4 setImageWithURL:[NSURL URLWithString:url]
                                           placeholderImage:[UIImage imageNamed:@"avatar round"]];
                            [cell.smallPic4 makeRoundedBorderWithRadius:17];
                            // Set Pic button
                            picButton = [[UIButton alloc] initWithFrame:cell.smallPic4.frame];
                            [picButton addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
                            picButton.tag = [obj[@"user_id"] integerValue];
                            [cell.contentView addSubview:picButton];
                        }
                        count ++;
                        south = YES;
                    } else if ([obj[@"region"] isEqualToString:@"Midwest"] && !midwest) {
                        NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                                baseString:obj[@"profile_pic_string"]];
                        if (count == 1) {
                            [cell.smallPic1 setImageWithURL:[NSURL URLWithString:url]
                                           placeholderImage:[UIImage imageNamed:@"avatar round"]];
                            [cell.smallPic1 makeRoundedBorderWithRadius:17];
                            // Set Pic button
                            picButton = [[UIButton alloc] initWithFrame:cell.smallPic1.frame];
                            [picButton addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
                            picButton.tag = [obj[@"user_id"] integerValue];
                            [cell.contentView addSubview:picButton];
                        } else if (count == 2) {
                            [cell.smallPic2 setImageWithURL:[NSURL URLWithString:url]
                                           placeholderImage:[UIImage imageNamed:@"avatar round"]];
                            [cell.smallPic2 makeRoundedBorderWithRadius:17];
                            // Set Pic button
                            picButton = [[UIButton alloc] initWithFrame:cell.smallPic2.frame];
                            [picButton addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
                            picButton.tag = [obj[@"user_id"] integerValue];
                            [cell.contentView addSubview:picButton];
                        } else if (count == 3) {
                            [cell.smallPic3 setImageWithURL:[NSURL URLWithString:url]
                                           placeholderImage:[UIImage imageNamed:@"avatar round"]];
                            [cell.smallPic3 makeRoundedBorderWithRadius:17];
                            // Set Pic button
                            picButton = [[UIButton alloc] initWithFrame:cell.smallPic3.frame];
                            [picButton addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
                            picButton.tag = [obj[@"user_id"] integerValue];
                            [cell.contentView addSubview:picButton];
                        } else if (count == 4) {
                            [cell.smallPic4 setImageWithURL:[NSURL URLWithString:url]
                                           placeholderImage:[UIImage imageNamed:@"avatar round"]];
                            [cell.smallPic4 makeRoundedBorderWithRadius:17];
                            // Set Pic button
                            picButton = [[UIButton alloc] initWithFrame:cell.smallPic4.frame];
                            [picButton addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
                            picButton.tag = [obj[@"user_id"] integerValue];
                            [cell.contentView addSubview:picButton];
                        }
                        count ++;
                        midwest = YES;
                    } else if ([obj[@"region"] isEqualToString:@"West"] && !west) {
                        NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                                baseString:obj[@"profile_pic_string"]];
                        if (count == 1) {
                            [cell.smallPic1 setImageWithURL:[NSURL URLWithString:url]
                                           placeholderImage:[UIImage imageNamed:@"avatar round"]];
                            [cell.smallPic1 makeRoundedBorderWithRadius:17];
                            // Set Pic button
                            picButton = [[UIButton alloc] initWithFrame:cell.smallPic1.frame];
                            [picButton addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
                            picButton.tag = [obj[@"user_id"] integerValue];
                            [cell.contentView addSubview:picButton];
                        } else if (count == 2) {
                            [cell.smallPic2 setImageWithURL:[NSURL URLWithString:url]
                                           placeholderImage:[UIImage imageNamed:@"avatar round"]];
                            [cell.smallPic2 makeRoundedBorderWithRadius:17];
                            // Set Pic button
                            picButton = [[UIButton alloc] initWithFrame:cell.smallPic2.frame];
                            [picButton addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
                            picButton.tag = [obj[@"user_id"] integerValue];
                            [cell.contentView addSubview:picButton];
                        } else if (count == 3) {
                            [cell.smallPic3 setImageWithURL:[NSURL URLWithString:url]
                                           placeholderImage:[UIImage imageNamed:@"avatar round"]];
                            [cell.smallPic3 makeRoundedBorderWithRadius:17];
                            // Set Pic button
                            picButton = [[UIButton alloc] initWithFrame:cell.smallPic3.frame];
                            [picButton addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
                            picButton.tag = [obj[@"user_id"] integerValue];
                            [cell.contentView addSubview:picButton];
                        } else if (count == 4) {
                            [cell.smallPic4 setImageWithURL:[NSURL URLWithString:url]
                                           placeholderImage:[UIImage imageNamed:@"avatar round"]];
                            [cell.smallPic4 makeRoundedBorderWithRadius:17];
                            // Set Pic button
                            picButton = [[UIButton alloc] initWithFrame:cell.smallPic4.frame];
                            [picButton addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
                            picButton.tag = [obj[@"user_id"] integerValue];
                            [cell.contentView addSubview:picButton];
                        }
                        count ++;
                        west = YES;
                    }
                }
                
                // Build the context
                if ([obj[@"region"] isEqualToString:@"Northeast"]) {
                    if (name1.length == 0) {
                        name1 = obj[@"username"];
                    } else {
                        name1 = [NSString stringWithFormat:@"%@,%@",name1,obj[@"username"]];
                    }
                } else if ([obj[@"region"] isEqualToString:@"Southeast"]) {
                    if (name2.length == 0) {
                        name2 = obj[@"username"];
                    } else {
                        name2 = [NSString stringWithFormat:@"%@,%@",name2,obj[@"username"]];
                    }
                } else if ([obj[@"region"] isEqualToString:@"South"]) {
                    if (name3.length == 0) {
                        name3 = obj[@"username"];
                    } else {
                        name3 = [NSString stringWithFormat:@"%@,%@",name3,obj[@"username"]];
                    }
                } else if ([obj[@"region"] isEqualToString:@"Midwest"]) {
                    if (name4.length == 0) {
                        name4 = obj[@"username"];
                    } else {
                        name4 = [NSString stringWithFormat:@"%@,%@",name4,obj[@"username"]];
                    }
                } else if ([obj[@"region"] isEqualToString:@"West"]) {
                    if (name5.length == 0) {
                        name5 = obj[@"username"];
                    } else {
                        name5 = [NSString stringWithFormat:@"%@,%@",name5,obj[@"username"]];
                    }
                }
            }
            
            // Set the context
            cell.username1.text = name1;
            cell.username2.text = name2;
            cell.username3.text = name3;
            cell.username4.text = name4;
            cell.username5.text = name5;
            
            if ([vPost.region isEqualToString:@"Northeast"]) {
                vPost.string = name1;
            } else if ([vPost.region isEqualToString:@"Southeast"]) {
                vPost.string = name2;
            } else if ([vPost.region isEqualToString:@"South"]) {
                vPost.string = name3;
            } else if ([vPost.region isEqualToString:@"Midwest"]) {
                vPost.string = name4;
            } else if ([vPost.region isEqualToString:@"West"]) {
                vPost.string = name5;
            }
            
            [cell.commentButton addTarget:self action:@selector(commentPost:) forControlEvents:UIControlEventTouchUpInside];
            cell.commentButton.tag = indexPath.section;
            cell.commentNumberLabel.text = [NSString stringWithFormat:@"%d comments", vPost.numComments];
            [cell.smallCircle makeRoundedBorderWithRadius:2];
            cell.smallCircle.backgroundColor = [UIColor darkGrayColor2];
            cell.commentNumberLabel.textColor = [UIColor darkGrayColor2];
            cell.likeNumberLabel.textColor = [UIColor darkGrayColor2];
            cell.commentIcon.image = [[UIImage imageNamed:@"Comment Small Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [cell.commentIcon setTintColor:[UIColor darkGrayColor2]];
            [cell.likeButton addTarget:self action:@selector(likePost:) forControlEvents:UIControlEventTouchUpInside];
            cell.likeButton.tag = indexPath.section;
            cell.likeNumberLabel.text = [NSString stringWithFormat:@"%d props", vPost.numLikes];
            cell.likeIcon.image = [[UIImage imageNamed:@"Like Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            if (vPost.doesSessionUserLikePost)
                [cell.likeIcon setTintColor:[UIColor goldColor]];
            else
                [cell.likeIcon setTintColor:[UIColor darkGrayColor2]];
            
            cell.seeProps.tag = indexPath.section;
            cell.seeComments.tag = indexPath.section;
            [cell.seeProps addTarget:self action:@selector(viewLikes:) forControlEvents:UIControlEventTouchUpInside];
            [cell.seeComments addTarget:self action:@selector(viewComments:) forControlEvents:UIControlEventTouchUpInside];
            
            return cell;
            
        } else {
            VoteCell *cell = [tableView dequeueReusableCellWithIdentifier:@"vote_newsfeed_cell" forIndexPath:indexPath];
            
            // clear subviews
            if ([cell.contentView subviews]){
                for (UIView *subview in [cell.contentView subviews]) {
                    if ([subview isKindOfClass:[SpecialUIButton class]]) {
                        SpecialUIButton * newSubview = (SpecialUIButton *)subview;
                        if (newSubview.shouldRemoveFromMySuperview)  // only items I created programmatically
                            [subview removeFromSuperview];
                        
                    } else if ([subview isKindOfClass:[SpecialUILabel class]]) {
                        SpecialUILabel *newSubview = (SpecialUILabel *)subview;
                        if (newSubview.shouldRemoveFromMySuperview)
                            [subview removeFromSuperview];
                    }
                    
                }
            }
            
            // clear subviews
            if ([cell.innerView subviews]){
                for (UIView *subview in [cell.innerView subviews]) {
                    if ([subview isKindOfClass:[UIButton class]]) {
                            [subview removeFromSuperview];
                        
                    } 
                }
            }
            
            // Action sheet
            [cell.actionSheet addTarget:self action:@selector(postExtraActions:) forControlEvents:UIControlEventTouchUpInside];
            cell.actionSheet.tag = indexPath.section;
            
            // View
            cell.contentView.frame = CGRectMake(0, 0, self.tableView.frame.size.width, [self tableView:tableView heightForRowAtIndexPath:indexPath]);
            [cell.outterView makeRoundedBorderWithRadius:6];
            [cell.innerView makeRoundedBorderWithRadius:5];
            
            // Change the attribute of the context for newsfeed
            if ([vPost.type isEqualToString:@"level"]) {
                NSString * str = [NSString stringWithFormat:@"%@ reached a new level; %@", vPost.newsMakerUserName, [vPost.textPost uppercaseString]];
                vPost.string = str;
                NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:str];
                [string addAttribute:NSForegroundColorAttributeName value:[UIColor goldColor] range:NSMakeRange(0,vPost.newsMakerUserName.length)];
                [string addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:13] range:NSMakeRange(0,vPost.newsMakerUserName.length)];
                [string addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:13] range:NSMakeRange(string.length - vPost.textPost.length,vPost.textPost.length)];
                cell.textpost.attributedText = string;
                
                // Set Pic button
                UIButton *picButton = [[UIButton alloc] initWithFrame:CGRectMake(cell.textpost.frame.origin.x, cell.textpost.frame.origin.y, [vPost.newsMakerUserName sizeWithFont:[UIFont systemFontOfSize:13]].width, 21)];
                [picButton addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
                picButton.tag = vPost.newsMakerUserID;
                [cell.innerView addSubview:picButton];
            } else {
                NSString * str = [NSString stringWithFormat:@"%@ %@ %@", vPost.newsMakerUserName, vPost.textPost, vPost.targetName];
                vPost.string = str;
                NSArray *array = [str componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]; // separate by space
                NSString * number = [array objectAtIndex:2]; // Get the number
                
                NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ %@ %@", vPost.newsMakerUserName, vPost.textPost, vPost.targetName]];
                [string addAttribute:NSForegroundColorAttributeName value:[UIColor goldColor] range:NSMakeRange(0,vPost.newsMakerUserName.length)];
                [string addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:13] range:NSMakeRange(0,vPost.newsMakerUserName.length)];
                [string addAttribute:NSForegroundColorAttributeName value:[UIColor goldColor] range:NSMakeRange(string.length - vPost.targetName.length, vPost.targetName.length)];
                [string addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:13] range:NSMakeRange(string.length - vPost.targetName.length, vPost.targetName.length)];
                if ([vPost.type isEqualToString:@"receiving"]) {
                    [string addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:13] range:NSMakeRange(vPost.newsMakerUserName.length+10, number.length + 5)];
                } else if ([vPost.type isEqualToString:@"giving"]) {
                    [string addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:13] range:NSMakeRange(vPost.newsMakerUserName.length+6, number.length + 5)];
                }
                cell.textpost.attributedText = string;
                
                // Set Pic button
                UIButton *picButton = [[UIButton alloc] initWithFrame:CGRectMake(cell.textpost.frame.origin.x, cell.textpost.frame.origin.y, [vPost.newsMakerUserName sizeWithFont:[UIFont systemFontOfSize:13]].width, 21)];
                [picButton addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
                picButton.tag = vPost.newsMakerUserID;
                [cell.innerView addSubview:picButton];
                
                UIButton *picButton2 = [[UIButton alloc] initWithFrame:CGRectMake(cell.textpost.frame.origin.x+[str sizeWithFont:[UIFont systemFontOfSize:13]].width-[vPost.targetName sizeWithFont:[UIFont systemFontOfSize:13]].width, cell.textpost.frame.origin.y, [vPost.targetName sizeWithFont:[UIFont systemFontOfSize:13]].width, 21)];
                [picButton2 addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
                picButton2.tag = vPost.targetID;
                [cell.innerView addSubview:picButton2];
            }
            
            // icon for the vote newsfeed
            if ([vPost.type isEqualToString:@"receiving"]) {
                cell.image.image = [UIImage imageNamed:@"receive_vote"];
            } else if ([vPost.type isEqualToString:@"giving"]) {
                cell.image.image = [UIImage imageNamed:@"vote_gave"];
            } else if ([vPost.type isEqualToString:@"thanks"]) {
                cell.image.image = [UIImage imageNamed:@"sent_thanks"];
            } else if ([vPost.type isEqualToString:@"level"]) {
                cell.image.image = [UIImage imageNamed:@"level_up"];
            }
            
            cell.datetime.text = [NSDate timeDiffCalc:vPost.newsDate];
            
            [cell.commentsButton addTarget:self action:@selector(commentPost:) forControlEvents:UIControlEventTouchUpInside];
            cell.commentsButton.tag = indexPath.section;
            cell.commentNumberLabel.text = [NSString stringWithFormat:@"%d comments", vPost.numComments];
            [cell.smallCircle makeRoundedBorderWithRadius:2];
            cell.smallCircle.backgroundColor = [UIColor darkGrayColor2];
            cell.commentNumberLabel.textColor = [UIColor darkGrayColor2];
            cell.likeNumberLabel.textColor = [UIColor darkGrayColor2];
            cell.commentsIcon.image = [[UIImage imageNamed:@"Comment Small Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [cell.commentsIcon setTintColor:[UIColor darkGrayColor2]];
            [cell.likeButton addTarget:self action:@selector(likePost:) forControlEvents:UIControlEventTouchUpInside];
            cell.likeButton.tag = indexPath.section;
            cell.likeNumberLabel.text = [NSString stringWithFormat:@"%d props", vPost.numLikes];
            cell.likeIcon.image = [[UIImage imageNamed:@"Like Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            if (vPost.doesSessionUserLikePost)
                [cell.likeIcon setTintColor:[UIColor goldColor]];
            else
                [cell.likeIcon setTintColor:[UIColor darkGrayColor2]];
            
            cell.seeProps.tag = indexPath.section;
            cell.seeComments.tag = indexPath.section;
            [cell.seeProps addTarget:self action:@selector(viewLikes:) forControlEvents:UIControlEventTouchUpInside];
            [cell.seeComments addTarget:self action:@selector(viewComments:) forControlEvents:UIControlEventTouchUpInside];
            
            return cell;
        }
    } 
    return nil;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    // when reaching bottom, load a new page
 //   NSLog(@"content offset y: %f", scrollView.contentOffset.y);
 //   NSLog(@"contentsize height: %f", scrollView.contentSize.height);
 //   NSLog(@"bounds height: %f", scrollView.bounds.size.height);
 //   if (scrollView.contentOffset.y == scrollView.contentSize.height - scrollView.bounds.size.height)
 //   {
    
    CGPoint offset = scrollView.contentOffset;
    CGRect bounds = scrollView.bounds;
    CGSize size = scrollView.contentSize;
    UIEdgeInsets inset = scrollView.contentInset;
    float y = offset.y + bounds.size.height - inset.bottom;
    float h = size.height;
    
    //float reload_distance = 10;
    /*if (self.currentSegment == NEWSFEED_BROWSE_SEGMENT) {
        y += 0.009033; // Unknown offset for scrollview in browse segment, so set it as magic number
    }*/
    
    NSLog(@"%d - %d", (int)y, (int)h);
    
    if((int)y >= (int)h) {
        // ask next page only if we haven't reached last page
        if(![self.newsfeedPaginator reachedLastPage])
        {
            // fetch next page of results
            [self fetchNextPage];
        }
    }
}

- (NSString *)getWinLossRecordForUser:(MatchPlayer *) player forSport:(NSString *)sportName
{
    NSString *resultString = @"";
    
    for (PlayerSport *pSport in player.playerSports) {
        if ([pSport.sportName isEqualToString:sportName]) {
         //   resultString = pSport.sportWinLossRecord;
        }
    }
    
    return resultString;
}

- (NSString *)getLevelForPlayer:(MatchPlayer *)player forSport:(NSString *)sportName
{
    for (PlayerSport *sport in player.playerSports) {
        if ([sport.sportName isEqualToString:sportName])
            return sport.level;
    }
    return @"";
}



- (CGFloat)tableView:(UITableView *)t heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.noResultsToDisplay)
        return 81;
    
    if ([(self.newsfeedPaginator.results)[indexPath.section] isKindOfClass:[AdUnit class]]) {
      //  return 95;
        return 0;
    } else if ([(self.newsfeedPaginator.results)[indexPath.section] isKindOfClass:[PickupMatchCreationNews class]]) {
        
        PickupMatchCreationNews *pNews = (self.newsfeedPaginator.results)[indexPath.section];
        
        NSInteger numMatchHistoryToDisplay;
        
        if ([pNews.pickupMatch.matchHistoryArray count] > 3)
            numMatchHistoryToDisplay = 3;
        else
            numMatchHistoryToDisplay = [pNews.pickupMatch.matchHistoryArray count];
        
     //   return 345 + (numMatchHistoryToDisplay * 20) + 30 + 45;
        return 333;
        
        
        
    } else if ([(self.newsfeedPaginator.results)[indexPath.section] isKindOfClass:[IndividualMatchCreationNews class]]&& ([(self.newsfeedPaginator.results)[indexPath.section] postType] == INDIVIDUAL_MATCH_CREATION || [(self.newsfeedPaginator.results)[indexPath.section] postType] == INDIVIDUAL_MATCH_CONFIRMATION)) {
    /*    IndividualMatchCreationNews *iNews = (self.newsfeedPaginator.results)[indexPath.section];
 
        NSInteger numMatchHistoryToDisplay;
        
        if ([iNews.individualMatch.matchHistoryArray count] > 3)
            numMatchHistoryToDisplay = 3;
        else
            numMatchHistoryToDisplay = [iNews.individualMatch.matchHistoryArray count];
        
        return 350 + (numMatchHistoryToDisplay * 20) - 30;*/
        return 250;
        
    } else if ([(self.newsfeedPaginator.results)[indexPath.section] isKindOfClass:[TeamMatchCreationNews class]]) {
        TeamMatchCreationNews *tNews = (self.newsfeedPaginator.results)[indexPath.section];
        
        NSInteger numMatchHistoryToDisplay;
        
        if ([tNews.tMatch.matchHistoryArray count] > 3)
            numMatchHistoryToDisplay = 3;
        else
            numMatchHistoryToDisplay = [tNews.tMatch.matchHistoryArray count];
        
        return 350 + (numMatchHistoryToDisplay * 20) - 30;
        
    } else if ([(self.newsfeedPaginator.results)[indexPath.section] isKindOfClass:[IndividualMatchCreationNews class]]
               && [(self.newsfeedPaginator.results)[indexPath.section] postType] == INDIVIDUAL_MATCH_RESULT) {
        return 420;
    } else if ([(self.newsfeedPaginator.results)[indexPath.section] isKindOfClass:[ImagePost class]]) {
 
        return [self calculateHeightForImagePost:indexPath.section];
        
    } else if ([(self.newsfeedPaginator.results)[indexPath.section] isKindOfClass:[LockerPost class]]) {
        return [self calculateHeightForLockerImagePost:indexPath.section];
    }/* else if ([(self.newsfeedPaginator.results)[indexPath.section] isKindOfClass:[VideoPost class]]) {
        VideoPost *vPost = (self.newsfeedPaginator.results)[[indexPath section]];
        NSString *text = vPost.textPost;
        
        CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH, 1000.0f);
   
        // because sizeWithFont deprecated
        
        NSMutableDictionary *attributes = [NSMutableDictionary new];
        [attributes setObject:[UIFont systemFontOfSize:FONT_SIZE] forKey:NSFontAttributeName];
        
        CGRect sizeRect = [text boundingRectWithSize:constraint options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
        
        CGSize size = sizeRect.size;
        //      CGSize size = [text sizeWithFont:[UIFont systemFontOfSize:FONT_SIZE] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
        
        // end because sizeWithFont deprecated
        
        
        
        return  size.height + HEIGHT_OF_REST_OF_CELL + 10;
        
    } */ else if ([(self.newsfeedPaginator.results)[indexPath.section] isKindOfClass:[TextPost class]]) {

        return [self calculateHeightForTextPost:indexPath.section];
        
    } else if ([(self.newsfeedPaginator.results)[indexPath.section] isKindOfClass:[MatchPlayerNewsOfficialFinishedIndividualMatch class]]) {
        MatchPlayerNewsOfficialFinishedIndividualMatch *iNews = (self.newsfeedPaginator.results)[indexPath.section];
        
        NSInteger numMatchHistoryToDisplay;
        
        if ([iNews.match.matchHistoryArray count] > 3)
            numMatchHistoryToDisplay = 3;
        else
            numMatchHistoryToDisplay = [iNews.match.matchHistoryArray count];
        
    //    return 430 + (numMatchHistoryToDisplay * 20) + 30 - 19;
    //    return 420;
        return 365;
    } else if ([(self.newsfeedPaginator.results)[indexPath.section] isKindOfClass:[MatchPlayerNews class]]) {
        
        MatchPlayerNews *mpn = [self.newsfeedPaginator.results objectAtIndex:indexPath.section];
        
        ////////////////////// for scores ///////////////////////
        if ([mpn.match isKindOfClass:[IndividualMatch class]]) {
            IndividualMatch *iMatch = (IndividualMatch *)mpn.match;
            
        //    if (iMatch.scoreIndMatch.scoreRecordedBy == mpn.player.userID || iMatch.scoreIndMatch.scoreDisputedBy == mpn.player.userID) {
            if (iMatch.scoreIndMatch.scoreRecordedBy > 0 || iMatch.scoreIndMatch.scoreDisputedBy > 0) {
                return 410;
            } else {
                return 285;
            }
        } else {
            return 285;
        }
    } else if ([(self.newsfeedPaginator.results)[indexPath.section] isKindOfClass:[MatchNews class]]) {
        return 331;
    } else if ([(self.newsfeedPaginator.results)[indexPath.section] isKindOfClass:[CompetitionNews class]]) {
        return 327;
    } else if ([(self.newsfeedPaginator.results)[indexPath.section] isKindOfClass:[VenueKingNews class]]) {
        return 154;
    } else if ([(self.newsfeedPaginator.results)[indexPath.section] isKindOfClass:[VenueCheckinNews class]]) {
       return 250 + [self calculateHeightForCheckinTextPost:indexPath.section] - 15;
    } else if ([(self.newsfeedPaginator.results)[indexPath.section] isKindOfClass:[VoteReceiving class]]) {
        VoteReceiving * post = (self.newsfeedPaginator.results)[indexPath.section];
        if ([post.type isEqualToString:@"winner"]) {
            return 213;
        }
        return 104;
    }
    return 0;
}

- (CGFloat)calculateHeightForCheckinTextPost:(NSUInteger)section
{
    VenueCheckinNews *vPost = [self.newsfeedPaginator.results objectAtIndex:section];
    NSString *tempString = @"";
    
    if ([vPost.vci.message length] > 0) {
        tempString = [NSString stringWithFormat:@"%@ - Playing %@ at %@", vPost.vci.message, [vPost.vci.checkedInSport.sportName capitalizedString], vPost.venue.venueName];
    } else
        tempString = [NSString stringWithFormat:@"Playing %@ at %@", [vPost.vci.checkedInSport.sportName capitalizedString], vPost.venue.venueName];
    
    NSString *text = tempString;

    
    CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH, 1000.0f);
    
    /********** because sizeWithFont deprecated *************/
    
    NSMutableDictionary *attributes = [NSMutableDictionary new];
    [attributes setObject:[UIFont systemFontOfSize:FONT_SIZE] forKey:NSFontAttributeName];
    
    CGRect sizeRect = [text boundingRectWithSize:constraint options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
    
    CGSize size = sizeRect.size;
    //      CGSize size = [text sizeWithFont:[UIFont systemFontOfSize:FONT_SIZE] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    
    /********* end because sizeWithFont deprecated *********/
    
    return size.height;
}


- (CGFloat)calculateHeightForTextPost:(NSUInteger)section
{
    News *nPost = (self.newsfeedPaginator.results)[section];
    NSString *text = nPost.textPost;
    
    CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH, 1000.0f);
    
    /********** because sizeWithFont deprecated *************/
    
    NSMutableDictionary *attributes = [NSMutableDictionary new];
    [attributes setObject:[UIFont systemFontOfSize:FONT_SIZE] forKey:NSFontAttributeName];
    
    CGRect sizeRect = [text boundingRectWithSize:constraint options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
    
    CGSize size = sizeRect.size;
    //      CGSize size = [text sizeWithFont:[UIFont systemFontOfSize:FONT_SIZE] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    
    /********* end because sizeWithFont deprecated *********/
    
    NSInteger factorDueToShareString;
    
    if ([nPost.sharedByPlayer.userName length] > 0) {
        factorDueToShareString = 25;
    } else {
        factorDueToShareString = 30;
    }
    
    NSLog(@"%f",size.height + HEIGHT_OF_REST_OF_CELL_NO_IMAGE - factorDueToShareString);
    
    return  size.height + HEIGHT_OF_REST_OF_CELL_NO_IMAGE - factorDueToShareString;
}

- (CGFloat)calculateHeightForImagePost:(NSUInteger)section
{
    ImagePost *iPost = (self.newsfeedPaginator.results)[section];
    NSString *text = iPost.textPost;
    
    CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH, 1000.0f);
    
    /********** because sizeWithFont deprecated *************/
    
    NSMutableDictionary *attributes = [NSMutableDictionary new];
    [attributes setObject:[UIFont systemFontOfSize:FONT_SIZE] forKey:NSFontAttributeName];
    
    CGRect sizeRect = [text boundingRectWithSize:constraint options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
    
    CGSize size = sizeRect.size;
    //        CGSize size = [text sizeWithFont:[UIFont systemFontOfSize:FONT_SIZE] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    
    /********* end because sizeWithFont deprecated *********/
    
    NSInteger factorDueToShareString;
    
    if ([iPost.sharedByPlayer.userName length] > 0) {
        factorDueToShareString = 115;
    } else {
        factorDueToShareString = 100;
    }
    
    return size.height + HEIGHT_OF_REST_OF_CELL - 5 + factorDueToShareString - 60;
}

- (CGFloat)calculateHeightForLockerImagePost:(NSUInteger)section
{
    LockerPost *iPost = (self.newsfeedPaginator.results)[section];
    NSString *text = iPost.textPost;
    
    if ([text length] == 0) return 385.0;
    
    CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH, 1000.0f);
    
    /********** because sizeWithFont deprecated *************/
    
    NSMutableDictionary *attributes = [NSMutableDictionary new];
    [attributes setObject:[UIFont systemFontOfSize:13] forKey:NSFontAttributeName];
    
    CGRect sizeRect = [text boundingRectWithSize:constraint options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
    
    CGSize size = sizeRect.size;
    //        CGSize size = [text sizeWithFont:[UIFont systemFontOfSize:FONT_SIZE] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    
    /********* end because sizeWithFont deprecated *********/
    
    NSInteger factorDueToShareString;
    
    if ([iPost.sharedByPlayer.userName length] > 0) {
        factorDueToShareString = 115;
    } else {
        factorDueToShareString = 100;
    }
    
    return size.height + HEIGHT_OF_REST_OF_CELL - 5 + factorDueToShareString - 60;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    if([segue.identifier isEqualToString:@"Pickup Match Detail"]){
        PickupMatchDetailTVC *controller = segue.destinationViewController;
        
        if ([[self.newsfeedPaginator.results objectAtIndex:self.selectedSection] isKindOfClass:[MatchNews class]]) {
            
            MatchNews *iMatchNews = (self.newsfeedPaginator.results)[self.selectedSection];
            controller.matchID = iMatchNews.match.matchID;
            
        } else if ([[self.newsfeedPaginator.results objectAtIndex:self.selectedSection] isKindOfClass:[PickupMatchCreationNews class]]) {
            PickupMatchCreationNews *pCreationNews = (self.newsfeedPaginator.results)[self.selectedSection];
            controller.matchID = pCreationNews.pickupMatch.matchID;
        }

    } else if ([segue.identifier isEqualToString:@"Comment Segue"]) {
    
        CommentsTVC *controller = (CommentsTVC *)segue.destinationViewController;
        controller.delegate = self;
        controller.currentTableSection = self.selectedSection;
        controller.showKeyboardOnLoad = self.showKeyboardOnCommentsPage;
     
        
        if ([[self.newsfeedPaginator.results objectAtIndex:self.selectedSection] isKindOfClass:[News class]]) {
            News *n = (self.newsfeedPaginator.results)[self.selectedSection];
            controller.newsID = n.newsID;
        }
    
        

    } else if ([segue.identifier isEqualToString:@"Enter Probability Segue"]) {
        
  //      IndividualMatchCreationNews *individualMatchNews = (self.newsPosts)[self.selectedSection];

//        ProbabilityChooseCompetitorVC *controller = (ProbabilityChooseCompetitorVC *)segue.destinationViewController;
        
  ///      controller.individualMatch = individualMatchNews.individualMatch;

        
    } else if ([segue.identifier isEqualToString:@"Probability Summary Segue"]) {
        
  //      IndividualMatchCreationNews *individualMatchNews = (self.newsPosts)[self.selectedSection];
        
  //      ProbabilitySummaryTVC *controller = (ProbabilitySummaryTVC *)segue.destinationViewController;
  //      controller.individualMatch = individualMatchNews.individualMatch;
        
        
    } else if ([segue.identifier isEqualToString:@"show video segue"]) {
     /*   NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        VideoPost *vPost = (self.newsfeedPaginator.results)[indexPath.section];
        NSURL *videoURL = nil;
        
        if ([vPost.vid.videoType isEqualToString:@"trick_video"]) {
            videoURL = [NSURL URLWithString:[S3Tools getFileNameStringWithType:@"trick_video"
                                                                          baseString:vPost.vid.videoBaseFileString]];
        } else {
            videoURL = [NSURL URLWithString:[S3Tools getFileNameStringWithType:@"highlight_video"
                                                                    baseString:vPost.vid.videoBaseFileString]];
        }
        
        ShowVideoVC *controller = segue.destinationViewController;
        controller.showVideoFlag = YES;
        controller.webAddress = videoURL;
     */   
    } else if ([segue.identifier isEqualToString:@"Individual Match Segue"]) {
        IndividualMatchDetailTVC *controller = segue.destinationViewController;
        
        if ([[self.newsfeedPaginator.results objectAtIndex:self.selectedSection] isKindOfClass:[MatchNews class]]) {
            MatchNews *iMatchNews = (self.newsfeedPaginator.results)[self.selectedSection];
            controller.matchID = iMatchNews.match.matchID;

        } else if ([[self.newsfeedPaginator.results objectAtIndex:self.selectedSection] isKindOfClass:[IndividualMatchCreationNews class]]) {
            IndividualMatchCreationNews *iNews = (self.newsfeedPaginator.results)[self.selectedSection];
            controller.matchID = iNews.individualMatch.matchID;
        }
    } else if ([segue.identifier isEqualToString:@"Player Detail Segue"]) {
        PlayerDetailTVC *controller = segue.destinationViewController;
        controller.playerUserID = self.selectedPlayerToShowDetailPage;
        
    } else if ([segue.identifier isEqualToString:@"Venue Detail Segue"]) {
        VenueDetailTVC *controller = segue.destinationViewController;
        controller.venueID = self.selectedVenue;
    } else if ([segue.identifier isEqualToString:@"Team Detail Segue"]) {
        TeamDetailTVC *controller = segue.destinationViewController;
        controller.teamID = self.selectedPlayerToShowDetailPage;
    } else if ([segue.identifier isEqualToString:@"Locker Detail Segue"]) {
        LockerPhotoDetailTVC *controller = segue.destinationViewController;
        controller.lockerID = self.selectedLockerID;
    } else if ([segue.identifier isEqualToString:@"View Likes Segue"]) {
        ShowLikesTVC *controller = segue.destinationViewController;
        LockerPost *n = [self.newsfeedPaginator.results objectAtIndex:self.selectedSection];
        controller.newsfeedID = n.newsID;
    } else if ([segue.identifier isEqualToString:@"confirm_segue"]) {
        FBShareRequestDialog *controller = segue.destinationViewController;
        UIImageView *tmpView = [[UIImageView alloc] init];
        [tmpView setImageWithURL:[NSURL URLWithString:self.fbImageString]
                placeholderImage:[UIImage imageNamed:@"avatar square"]];
        self.currentPickedImage = tmpView.image;
        controller.image = [self addOverlayToPostImage:self.currentPickedImage];
        controller.delegate = self;
    } else if ([segue.identifier isEqualToString:@"Add Pic Segue"]) {
        AddLockerPicturePreStepVC *controller = segue.destinationViewController;
        controller.viObject = self.viObject;
        controller.regImageSizeUploader = self.regImageSizeUploader;
        controller.regImageRetinaSizeUploader = self.regImageRetinaSizeUploader;
        controller.thumbnailImageSizeUploader = self.thumbnailImageSizeUploader;
        controller.thumbnailImageRetinaSizeUploader = self.thumbnailImageRetinaSizeUploader;
        controller.imageFileNameBaseString = self.imageFileNameBaseString;
        controller.imageFileName = self.imageFileName;
    } else if ([segue.identifier isEqualToString:@"text_post_segue"]) {
        CommentDialog *controller = (CommentDialog *)segue.destinationViewController;
        controller.delegate = self;
        controller.isTextPost = YES;
    }

}



/*

- (MatchPlayer *)makeMatchCompetitorObject:(id)object forSportName:(NSString *)sportName andType:(NSString *)sportType
{
                
    MatchPlayer *player = [[MatchPlayer alloc] initWithPlayerDetails:[object[@"competitor_id"] integerValue]
                                                       andProfileStr:object[@"profile_pic_string"]
                                                         andUserName:object[@"username"]
                                                         andFullName:nil
                                                             andLocation:nil
                                                            andAbout:nil
                                                            andCoins:0
                                                           andStatus:nil];
    

    PlayerSport *sport = [[PlayerSport alloc] initWithSportDetails:sportName
                                                               andType:sportType
                                                              andLevel:object[@"level"]
                                             andSportWinLossRecord:object[@"win_loss_record"]];
    [player.playerSports addObject:sport];
    
            
    return player;
}
*/
/*
- (IBAction)probabilityButtonIndividualMatch:(id)sender
{
    UIView *contentView = (UIView *)[sender superview];
    UITableViewCell *cell = (UITableViewCell *)[contentView superview];
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    
    IndividualMatchCreationNews *individualMatchNews = (self.newsPosts)[indexPath.section];
    
    if ([Probability checkIfSessionUserVotedProbabilityInList1:individualMatchNews.individualMatch.probabilityListForPlayer1
                                                      andList2:individualMatchNews.individualMatch.probabilityListForPlayer2
                                                    withUserID:[self.preferences getUserID]]) {
//    if ([Probability checkIfSessionUserVotedProbabilityinMatch:individualMatchNews.individualMatch.matchID
//                                                        byUser:[self.preferences getUserID]]) {
        // segue here if session user voted probability already for this individual match
        self.selectedSection = indexPath.section;
        [self performSegueWithIdentifier:@"Probability Summary Segue" sender:self];
        
    } else {
        // should only be able to segue here if session user didn't vote probability already
        self.selectedSection = indexPath.section;
        [self performSegueWithIdentifier:@"Enter Probability Segue" sender:self];
    }
}

- (IBAction)probabilityButtonIndividualMatchResult:(id)sender
{
    UIView *contentView = (UIView *)[sender superview];
    UITableViewCell *cell = (UITableViewCell *)[contentView superview];
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];

    self.selectedSection = indexPath.section;
    [self performSegueWithIdentifier:@"Probability Summary Segue" sender:self];
}
*/
- (UILabel *)makeDynamicHeighLabel:(NSString *)myText forLabel:(UILabel *)myLabel
{
    CGSize maxLabelSize = CGSizeMake(300,500);
    
    
    /********** because sizeWithFont deprecated *************/
    
    NSMutableDictionary *attributes = [NSMutableDictionary new];
    [attributes setObject:[UIFont systemFontOfSize:myLabel.font.pointSize] forKey:NSFontAttributeName];
    
    CGRect sizeRect = [myText boundingRectWithSize:maxLabelSize options:NSLineBreakByWordWrapping | NSStringDrawingUsesLineFragmentOrigin  attributes:attributes context:nil];
    
    CGSize expectedLabelSize = sizeRect.size;
    //      CGSize expectedLabelSize = [myText sizeWithFont:myLabel.font
//constrainedToSize:maxLabelSize
//lineBreakMode:myLabel.lineBreakMode];
    
    /********* end because sizeWithFont deprecated *********/
    
    
    
    //adjust the label the new height.
    CGRect newFrame = myLabel.frame;
    newFrame.size.height = expectedLabelSize.height;
    myLabel.frame = newFrame;
    
    return myLabel;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
 /*   if ([(self.newsfeedPaginator.results)[indexPath.section] isKindOfClass:[VideoPost class]]) {
    
        [self performSegueWithIdentifier:@"show video segue" sender:self];
    }*/
    
   
}

- (void)commentPost:(UIButton *)sender
{
    self.selectedSection = sender.tag;
    self.showKeyboardOnCommentsPage = YES;
    [self performSegueWithIdentifier:@"Comment Segue" sender:self];
}

#define TITLE_OF_ACTIONSHEET_SHARE @"Share this post with your followers"
#define TITLE_OF_ACTIONSHEET_ALREADY_SHARED @"You've already shared this post with your followers"
#define CANCEL_BUTTON_ACTIONSHEET @"Cancel "
#define SHARE_POST @"Share in newsfeed"
#define UNSHARE_POST @"Unshare from newsfeed"
#define FACEBOOK_SHARE @"Facebook Share"
#define DELETE_POST @"Delete Post"
#define HIDE_POST @"Hide Post"
#define FLAG_POST @"Flag Post"


- (IBAction)remixTextPost:(UIButton *)sender
{
    /*
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:sender.tag];
    TextPost *tPost = [self.newsfeedPaginator.results objectAtIndex:sender.tag];
    
    self.remixedIndexPath = indexPath;
    self.remixedNewsID = tPost.newsID;
    
    NSString *checkIfPostCanBeShared = [tPost canPostBeSharedBySessionUser:[self.preferences getUserID]];
    self.fbMessage = tPost.textPost;
//    self.fbImage = [UIImage imageNamed:@"Vaderen Tab"];
//    self.fbTitle = [NSString stringWithFormat:@"%@ made a post in the Vaiden App", tPost.sharedByPlayer.userName];
    
    
    if ([checkIfPostCanBeShared isEqualToString:@"OK"]) {
        if (tPost.didSessionUserRemixPost) {
            
            [self showActionSheetForRemixNewsPost:UNSHARE_POST
                                    andSheetTitle:TITLE_OF_ACTIONSHEET_ALREADY_SHARED
                                     showFBButton:NO];
             
        } else {
            [self showActionSheetForRemixNewsPost:SHARE_POST
                                    andSheetTitle:TITLE_OF_ACTIONSHEET_SHARE
                                     showFBButton:NO];
             
        }
    } else {
       
        
    }*/
}

- (IBAction)remixImagePost:(UIButton *)sender
{
    /*
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:sender.tag];
    ImagePost *iPost = [self.newsfeedPaginator.results objectAtIndex:sender.tag];
    
    self.remixedIndexPath = indexPath;
    self.remixedNewsID = iPost.newsID;
    
 //   UIImageView *tempUIV = [[UIImageView alloc] init];
    
    NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                            baseString:iPost.imageBaseFileString];
    
    
    NSString *checkIfPostCanBeShared = [iPost canPostBeSharedBySessionUser:[self.preferences getUserID]];
    
    self.fbMessage = iPost.textPost;
    self.fbImage = [NSURL URLWithString:url];
    self.fbTitle = [NSString stringWithFormat:@"%@ shared an image from the Vaiden App", iPost.sharedByPlayer.userName];
    if ([checkIfPostCanBeShared isEqualToString:@"OK"]) {
        if (iPost.didSessionUserRemixPost) {
            [self showActionSheetForRemixNewsPost:UNSHARE_POST
                                    andSheetTitle:TITLE_OF_ACTIONSHEET_ALREADY_SHARED
                                     showFBButton:NO];
             
        } else {
            [self showActionSheetForRemixNewsPost:SHARE_POST
                                    andSheetTitle:TITLE_OF_ACTIONSHEET_SHARE
                                     showFBButton:NO];
        }
    } else {
   
    }*/
   /* [IOSRequest makeRemixForNewsfeed:iPost.newsID
                              byUser:[self.preferences getUserID]
                        onCompletion:^(NSDictionary *results) {
                            dispatch_async(dispatch_get_main_queue(), ^ {
                                [self.tableView beginUpdates];
                                
                                iPost.numRemixes = [results[@"num_remixes"] integerValue];
                                iPost.didSessionUserRemixPost = [results[@"did_session_user_remix_post"] boolValue];
                                
                                [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                                [self.tableView endUpdates];
                            });
                            
                        }];*/
}


- (IBAction)remixCompetitionPost:(UIButton *)sender
{
    /*
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:sender.tag];
    CompetitionNews *cPost = [self.newsfeedPaginator.results objectAtIndex:sender.tag];
    
    self.remixedIndexPath = indexPath;
    self.remixedNewsID = cPost.newsID;
    
    NSString *checkIfPostCanBeSharedInNewsfeed = [cPost canPostBeSharedBySessionUser:[self.preferences getUserID]];
    
    self.fbMessage = @"The 1v1 Intercollegiate Basketball Championships.  College students are welcome to register to compete!";
    self.fbTitle = [NSString stringWithFormat:@"%@ joined a competition", cPost.newsMakerUserName];
    if ([checkIfPostCanBeSharedInNewsfeed isEqualToString:@"OK"]) {
        if (cPost.didSessionUserRemixPost) {
            
            [self showActionSheetForRemixNewsPost:UNSHARE_POST
                                    andSheetTitle:TITLE_OF_ACTIONSHEET_ALREADY_SHARED
                                     showFBButton:YES];
        } else {
            [self showActionSheetForRemixNewsPost:SHARE_POST
                                    andSheetTitle:TITLE_OF_ACTIONSHEET_SHARE
                                     showFBButton:YES];
        }
    } else {
        [self showActionSheetForOnlyFBShare];
    }
     */
}

- (IBAction)remixVenueKingPost:(UIButton *)sender
{
    /*
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:sender.tag];
    VenueKingNews *vPost= [self.newsfeedPaginator.results objectAtIndex:sender.tag];
    
    self.remixedIndexPath = indexPath;
    self.remixedNewsID = vPost.newsID;
    
    NSString *checkIfPostCanBeSharedInNewsfeed = [vPost canPostBeSharedBySessionUser:[self.preferences getUserID]];
    
    self.fbMessage = [NSString stringWithFormat:@"%@ for %@", vPost.venue.venueName, [vPost.sport.sportName capitalizedString]];
    self.fbTitle = [NSString stringWithFormat:@"%@ became King Of a Venue", vPost.newsMakerUserName];
    if ([checkIfPostCanBeSharedInNewsfeed isEqualToString:@"OK"]) {
        if (vPost.didSessionUserRemixPost) {
            
            [self showActionSheetForRemixNewsPost:UNSHARE_POST
                                    andSheetTitle:TITLE_OF_ACTIONSHEET_ALREADY_SHARED
                                     showFBButton:YES];
        } else {
            [self showActionSheetForRemixNewsPost:SHARE_POST
                                    andSheetTitle:TITLE_OF_ACTIONSHEET_SHARE
                                     showFBButton:YES];
        }
    } else {
        [self showActionSheetForOnlyFBShare];
    }*/
}

- (IBAction)remixCheckinPost:(UIButton *)sender
{
    /*
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:sender.tag];
    VenueCheckinNews *vPost= [self.newsfeedPaginator.results objectAtIndex:sender.tag];
    
    self.remixedIndexPath = indexPath;
    self.remixedNewsID = vPost.newsID;
    
    NSString *checkIfPostCanBeSharedInNewsfeed = [vPost canPostBeSharedBySessionUser:[self.preferences getUserID]];
    
    NSString *sportName = [vPost.vci.checkedInSport.sportName capitalizedString];
    
    if (vPost.vci.isNow) {
        if ([sportName length] > 0)
            self.fbTitle = [NSString stringWithFormat:@"@%@ checked in at %@ to play %@", vPost.vci.checkedInUser.userName, vPost.venue.venueName, sportName];
        else
            self.fbTitle = [NSString stringWithFormat:@"@%@ checked in at %@", vPost.vci.checkedInUser.userName, vPost.venue.venueName];
    } else {
        if ([sportName length] > 0)
            self.fbTitle = [NSString stringWithFormat:@"@%@ will be at %@ at %@ to play %@", vPost.vci.checkedInUser.userName, vPost.venue.venueName, [vPost.vci.checkedInDateTime getDateFullDateString], sportName];
        else
            self.fbTitle = [NSString stringWithFormat:@"@%@ will be at %@ at %@", vPost.vci.checkedInUser.userName, vPost.venue.venueName, [vPost.vci.checkedInDateTime getDateFullDateString]];
    }
    
    
    if ([vPost.vci.message length] > 0)
        self.fbMessage = [NSString stringWithFormat:@"%@ says: \"%@\"", vPost.vci.checkedInUser.userName, vPost.vci.message];
    else
        self.fbMessage = @"";
    
    if ([checkIfPostCanBeSharedInNewsfeed isEqualToString:@"OK"]) {
        if (vPost.didSessionUserRemixPost) {
            
            [self showActionSheetForRemixNewsPost:UNSHARE_POST
                                    andSheetTitle:TITLE_OF_ACTIONSHEET_ALREADY_SHARED
                                     showFBButton:YES];
        } else {
            [self showActionSheetForRemixNewsPost:SHARE_POST
                                    andSheetTitle:TITLE_OF_ACTIONSHEET_SHARE
                                     showFBButton:YES];
        }
    } else {
        [self showActionSheetForOnlyFBShare];
    }*/

}

- (IBAction)remixIndividualMatchPost:(UIButton *)sender
{
    /*
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:sender.tag];
    IndividualMatchCreationNews *iPost = [self.newsfeedPaginator.results objectAtIndex:sender.tag];
    
    self.remixedIndexPath = indexPath;
    self.remixedNewsID = iPost.newsID;
    self.fbTitle = [NSString stringWithFormat:@"%@ challenged %@", iPost.individualMatch.matchCreatorUsername, [iPost.individualMatch getOtherPlayer:iPost.individualMatch.matchCreatorID].userName];
    
   NSString *checkIfPostCanBeSharedInNewsfeed = [iPost canPostBeSharedBySessionUser:[self.preferences getUserID]];
    
    self.fbMessage = [NSString stringWithFormat:@"Learn more about this %@ Challenge Match on the Vaiden iPhone App", [iPost.individualMatch.sport.sportName capitalizedString]];
  //  self.fbImage = [UIImage imageNamed:@"Vaderen Tab"];
    
    
    if ([checkIfPostCanBeSharedInNewsfeed isEqualToString:@"OK"]) {
        if (iPost.didSessionUserRemixPost) {
            
            [self showActionSheetForRemixNewsPost:UNSHARE_POST
                                    andSheetTitle:TITLE_OF_ACTIONSHEET_ALREADY_SHARED
                                     showFBButton:YES];
        } else {
            [self showActionSheetForRemixNewsPost:SHARE_POST
                                    andSheetTitle:TITLE_OF_ACTIONSHEET_SHARE
                                     showFBButton:YES];
        }
    } else {
        [self showActionSheetForOnlyFBShare];
    }*/
    /*
    [IOSRequest makeRemixForNewsfeed:iPost.newsID
                              byUser:[self.preferences getUserID]
                        onCompletion:^(NSDictionary *results) {
                            dispatch_async(dispatch_get_main_queue(), ^ {
                                [self.tableView beginUpdates];
                                
                                iPost.numRemixes = [results[@"num_remixes"] integerValue];
                                iPost.didSessionUserRemixPost = [results[@"did_session_user_remix_post"] boolValue];
                                
                                [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                                [self.tableView endUpdates];
                            });
                            
                        }];*/
}

- (IBAction)remixPickupMatchPost:(UIButton *)sender
{
    /*
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:sender.tag];
    PickupMatchCreationNews *pPost = [self.newsfeedPaginator.results objectAtIndex:sender.tag];
    
    self.remixedIndexPath = indexPath;
    self.remixedNewsID = pPost.newsID;
    
    NSString *checkIfPostCanBeSharedInNewsfeed = [pPost canPostBeSharedBySessionUser:[self.preferences getUserID]];
    if ([checkIfPostCanBeSharedInNewsfeed isEqualToString:@"OK"]) {
        if (pPost.didSessionUserRemixPost) {
            [self showActionSheetForRemixNewsPost:UNSHARE_POST andSheetTitle:TITLE_OF_ACTIONSHEET_ALREADY_SHARED andFBMessage:pPost. andFBImage:<#(UIImage *)#>];
        } else {
            [self showActionSheetForRemixNewsPost:SHARE_POST andSheetTitle:TITLE_OF_ACTIONSHEET_SHARE];
        }
    } else {
        [self showActionSheetForOnlyFBShare];
    }
    
    [IOSRequest makeRemixForNewsfeed:pPost.newsID
                              byUser:[self.preferences getUserID]
                        onCompletion:^(NSDictionary *results) {
                            dispatch_async(dispatch_get_main_queue(), ^ {
                                [self.tableView beginUpdates];
                                
                                pPost.numRemixes = [results[@"num_remixes"] integerValue];
                                pPost.didSessionUserRemixPost = [results[@"did_session_user_remix_post"] boolValue];
                                
                                [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                                [self.tableView endUpdates];
                            });
                            
                        }];*/
  /*
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:sender.tag];
    PickupMatchCreationNews *pPost = [self.newsfeedPaginator.results objectAtIndex:sender.tag];
    
    self.remixedIndexPath = indexPath;
    self.remixedNewsID = pPost.newsID;
    self.fbTitle = [NSString stringWithFormat:@"%@ created a pickup match", pPost.pickupMatch.matchCreatorUsername];
    
    NSString *checkIfPostCanBeSharedInNewsfeed = [pPost canPostBeSharedBySessionUser:[self.preferences getUserID]];
    
    self.fbMessage = [NSString stringWithFormat:@"Learn more about this %@ Pickup Match on the Vaiden iPhone App", [pPost.pickupMatch.sport.sportName capitalizedString]];
    //  self.fbImage = [UIImage imageNamed:@"Vaderen Tab"];
    
    
    if ([checkIfPostCanBeSharedInNewsfeed isEqualToString:@"OK"]) {
        if (pPost.didSessionUserRemixPost) {
            
            [self showActionSheetForRemixNewsPost:UNSHARE_POST
                                    andSheetTitle:TITLE_OF_ACTIONSHEET_ALREADY_SHARED
                                     showFBButton:YES];
        } else {
            [self showActionSheetForRemixNewsPost:SHARE_POST
                                    andSheetTitle:TITLE_OF_ACTIONSHEET_SHARE
                                     showFBButton:YES];
        }
    } else {
        [self showActionSheetForOnlyFBShare];
    }*/
}
/*
- (IBAction)remixIndividualMatchResultPost:(UIButton *)sender
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:sender.tag];
    MatchNews *iPost = [self.newsfeedPaginator.results objectAtIndex:sender.tag];
    
    self.remixedIndexPath = indexPath;
    self.remixedNewsID = iPost.newsID;
    
    [self showActionSheetForRemixNewsPost];
    
 
     [IOSRequest makeRemixForNewsfeed:iPost.newsID
     byUser:[self.preferences getUserID]
     onCompletion:^(NSDictionary *results) {
     dispatch_async(dispatch_get_main_queue(), ^ {
     [self.tableView beginUpdates];
     
     iPost.numRemixes = [results[@"num_remixes"] integerValue];
     iPost.didSessionUserRemixPost = [results[@"did_session_user_remix_post"] boolValue];
     
     [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
     [self.tableView endUpdates];
     });
     
     }];
}*/

- (IBAction)remixMatchPlayerNewsUpdatePost:(UIButton *)sender
{
    /*
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:sender.tag];
    MatchNews *iPost = [self.newsfeedPaginator.results objectAtIndex:sender.tag];
    
    self.remixedIndexPath = indexPath;
    self.remixedNewsID = iPost.newsID;
    
    IndividualMatch *iMatch = (IndividualMatch *)iPost.match;
    
    NSString *checkIfPostCanBeSharedInNewsfeed = [iPost canPostBeSharedBySessionUser:[self.preferences getUserID]];
    
    self.fbMessage  = [NSString stringWithFormat:@"Learn more about this %@ Challenge Match on the Vaiden iPhone App", [iMatch.sport.sportName capitalizedString]];
//    self.fbImage = [UIImage imageNamed:@"Vaderen Tab"];
    self.fbTitle = [NSString stringWithFormat:@"%@ vs %@", iMatch.player1.userName, iMatch.player2.userName];
    
    if ([checkIfPostCanBeSharedInNewsfeed isEqualToString:@"OK"]) {

        if (iPost.didSessionUserRemixPost) {
            [self showActionSheetForRemixNewsPost:UNSHARE_POST
                                    andSheetTitle:TITLE_OF_ACTIONSHEET_ALREADY_SHARED
                                     showFBButton:YES];
        } else {
            [self showActionSheetForRemixNewsPost:SHARE_POST
                                    andSheetTitle:TITLE_OF_ACTIONSHEET_SHARE
                                     showFBButton:YES];
        }
    } else {
        [self showActionSheetForOnlyFBShare];
    }
     */
}

- (IBAction)remixMatchNewsUpdate:(UIButton *)sender
{
    /*
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:sender.tag];
    MatchNews *iPost = [self.newsfeedPaginator.results objectAtIndex:sender.tag];
    
    self.remixedIndexPath = indexPath;
    self.remixedNewsID = iPost.newsID;
    
    IndividualMatch *iMatch = (IndividualMatch *)iPost.match;

    NSString *checkIfPostCanBeSharedInNewsfeed = [iPost canPostBeSharedBySessionUser:[self.preferences getUserID]];
    
    self.fbMessage  = [NSString stringWithFormat:@"Learn more about this %@ Challenge Match on the Vaiden iPhone App", [iMatch.sport.sportName capitalizedString]];
    //    self.fbImage = [UIImage imageNamed:@"Vaderen Tab"];
    self.fbTitle = [NSString stringWithFormat:@"%@ vs %@", iMatch.player1.userName, iMatch.player2.userName];
    
    if ([checkIfPostCanBeSharedInNewsfeed isEqualToString:@"OK"]) {
        if (iPost.didSessionUserRemixPost) {
            [self showActionSheetForRemixNewsPost:UNSHARE_POST
                                     andSheetTitle:TITLE_OF_ACTIONSHEET_ALREADY_SHARED
                                     showFBButton:YES
                                     ];
        } else {
            [self showActionSheetForRemixNewsPost:SHARE_POST
                                    andSheetTitle:TITLE_OF_ACTIONSHEET_SHARE
                                     showFBButton:YES
                                     ];
        }
    } else {
        [self showActionSheetForOnlyFBShare];
    }
     */
}

- (IBAction)remixMatchPlayerScoreRecordedNewsPost:(UIButton *)sender
{
/*    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:sender.tag];
    MatchNews *iPost = [self.newsfeedPaginator.results objectAtIndex:sender.tag];
    
    self.remixedIndexPath = indexPath;
    self.remixedNewsID = iPost.newsID;
    
    IndividualMatch *iMatch = (IndividualMatch *)iPost.match;
    
    NSString *checkIfPostCanBeSharedInNewsfeed = [iPost canPostBeSharedBySessionUser:[self.preferences getUserID]];
    
    self.fbMessage  = [NSString stringWithFormat:@"%@ Challenge Match", [iMatch.sport.sportName capitalizedString]];
    //    self.fbImage = [UIImage imageNamed:@"Vaderen Tab"];
    self.fbTitle = [NSString stringWithFormat:@"%@ vs %@", iMatch.player1.userName, iMatch.player2.userName];

    
    if ([checkIfPostCanBeSharedInNewsfeed isEqualToString:@"OK"]) {

        if (iPost.didSessionUserRemixPost) {
            [self showActionSheetForRemixNewsPost:UNSHARE_POST
                                    andSheetTitle:TITLE_OF_ACTIONSHEET_ALREADY_SHARED
                                     showFBButton:YES
                                     ];
        } else {
            [self showActionSheetForRemixNewsPost:SHARE_POST
                                    andSheetTitle:TITLE_OF_ACTIONSHEET_SHARE
                                     showFBButton:YES
                                     ];
        }
    } else {
        [self showActionSheetForOnlyFBShare];
    }*/
    
}

- (IBAction)remixMatchPlayerScoreConfirmedNewsPost:(UIButton *)sender
{
/*    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:sender.tag];
    MatchNews *iPost = [self.newsfeedPaginator.results objectAtIndex:sender.tag];
    
    self.remixedIndexPath = indexPath;
    self.remixedNewsID = iPost.newsID;
    
    IndividualMatch *iMatch = (IndividualMatch *)iPost.match;
    NSString *checkIfPostCanBeSharedInNewsfeed = [iPost canPostBeSharedBySessionUser:[self.preferences getUserID]];
    
    self.fbMessage  = [NSString stringWithFormat:@"Learn more about this %@ Challenge Match on the Vaiden iPhone App", [iMatch.sport.sportName capitalizedString]];
    //    self.fbImage = [UIImage imageNamed:@"Vaderen Tab"];
    self.fbTitle = [NSString stringWithFormat:@"%@ vs %@", iMatch.player1.userName, iMatch.player2.userName];

    
    if ([checkIfPostCanBeSharedInNewsfeed isEqualToString:@"OK"]) {
        if (iPost.didSessionUserRemixPost) {
            [self showActionSheetForRemixNewsPost:UNSHARE_POST
                                    andSheetTitle:TITLE_OF_ACTIONSHEET_ALREADY_SHARED
                                     showFBButton:YES
                                     ];

        } else {
            [self showActionSheetForRemixNewsPost:SHARE_POST
                                    andSheetTitle:TITLE_OF_ACTIONSHEET_SHARE
                                     showFBButton:YES
                                     ];

        }
    } else {
        [self showActionSheetForOnlyFBShare];
    }
 */
}




- (void)showActionSheetForRemixNewsPost:(NSString *)buttonTitle
                          andSheetTitle:(NSString *)sheetTitle
                           showFBButton:(BOOL)showFBButton
{
    
    if (showFBButton) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                      initWithTitle:sheetTitle delegate:self
                                      cancelButtonTitle:CANCEL_BUTTON_ACTIONSHEET destructiveButtonTitle:nil
                                      otherButtonTitles:FACEBOOK_SHARE, buttonTitle,  nil];
        [actionSheet showFromTabBar:self.tabBarController.tabBar];
    } else {
        UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                      initWithTitle:sheetTitle delegate:self
                                      cancelButtonTitle:CANCEL_BUTTON_ACTIONSHEET destructiveButtonTitle:nil
                                      otherButtonTitles:buttonTitle,  nil];
        [actionSheet showFromTabBar:self.tabBarController.tabBar];
    }
    
}

- (void)showActionSheetForRemixMatchNewsPost:(NSString *)buttonTitle
                               andSheetTitle:(NSString *)sheetTitle
                                showFBButton:(BOOL)showFBButton

{
    if (showFBButton) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:sheetTitle delegate:self
                                  cancelButtonTitle:CANCEL_BUTTON_ACTIONSHEET destructiveButtonTitle:nil
                                  otherButtonTitles:FACEBOOK_SHARE, buttonTitle,  nil];
        [actionSheet showFromTabBar:self.tabBarController.tabBar];
    } else {
        UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                      initWithTitle:sheetTitle delegate:self
                                      cancelButtonTitle:CANCEL_BUTTON_ACTIONSHEET destructiveButtonTitle:nil
                                      otherButtonTitles:buttonTitle,  nil];
        [actionSheet showFromTabBar:self.tabBarController.tabBar];
    }
}


- (void)showActionSheetForOnlyFBShare
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:TITLE_OF_ACTIONSHEET_SHARE delegate:self
                                  cancelButtonTitle:CANCEL_BUTTON_ACTIONSHEET destructiveButtonTitle:nil
                                  otherButtonTitles:FACEBOOK_SHARE,  nil];
    [actionSheet showFromTabBar:self.tabBarController.tabBar];
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == actionSheet.destructiveButtonIndex) {
        [self.view.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    } else {
        NSString *choice = [actionSheet buttonTitleAtIndex:buttonIndex];
        
        if ([choice isEqualToString:SHARE_POST] || [choice isEqualToString:UNSHARE_POST]) {
            [self submitRemixPost];
        } else if ([choice isEqualToString:FACEBOOK_SHARE]) {
            [self startFBShare];
        } else if ([choice isEqualToString:DELETE_POST]) {
            [self deletePost];
        } else if ([choice isEqualToString:FLAG_POST]) {
            [self flagPost];
        } else if ([choice isEqualToString:FROM_CAMERA]) {
            [self changePicFromCamera];
        } else if ([choice isEqualToString:FROM_LIBRARY]){
            [self changePicFromPhotoLibrary];
        }
    }
}

- (void)submitRemixPost
{
    [IOSRequest makeRemixForNewsfeed:self.remixedNewsID
                              byUser:[self.preferences getUserID]
                        onCompletion:^(NSDictionary *results) {
                            
                            dispatch_async(dispatch_get_main_queue(), ^ {
                                [self.tableView beginUpdates];
                                
                                id post = [self.newsfeedPaginator.results objectAtIndex:self.remixedIndexPath.section];
                                
                                if ([post isKindOfClass:[News class]]) {
                                    News *nPost = (News *)post;
                                    nPost.numRemixes = [results[@"num_remixes"] integerValue];
                                    nPost.didSessionUserRemixPost = [results[@"did_session_user_remix_post"] boolValue];

                                } 
                                
                                [self.tableView reloadRowsAtIndexPaths:@[self.remixedIndexPath] withRowAnimation:UITableViewRowAnimationNone];
                                [self.tableView endUpdates];
                                
                                self.remixedIndexPath = nil;
                                self.remixedNewsID = 0;
                            });
                            
                        }];

}


- (void)startFBShare
{
    if (FBSession.activeSession.isOpen) {
        if ([FBSession.activeSession.permissions indexOfObject:@"publish_actions"] == NSNotFound) {
            // Request publish_actions
            [FBSession.activeSession requestNewPublishPermissions:[NSArray arrayWithObject:@"publish_actions"]
                                                  defaultAudience:FBSessionDefaultAudienceFriends
                                                completionHandler:^(FBSession *session, NSError *error) {
                                                    __block NSString *alertText;
                                                    __block NSString *alertTitle;
                                                    if (!error) {
                                                        if ([FBSession.activeSession.permissions
                                                             indexOfObject:@"publish_actions"] == NSNotFound){
                                                            // Permission not granted, tell the user we will not publish
                                                            alertTitle = @"Permission not granted";
                                                            alertText = @"Your action will not be published to Facebook.";
                                                            [[[UIAlertView alloc] initWithTitle:@"title"
                                                                                        message:@"text"
                                                                                       delegate:self
                                                                              cancelButtonTitle:@"OK!"
                                                                              otherButtonTitles:nil] show];
                                                        } else {
                                                            // Permission granted, publish the OG story
                                                            [self presentFBDialog];
                                                        }
                                                        
                                                    } else {
                                                        // There was an error, handle it
                                                        // See https://developers.facebook.com/docs/ios/errors/
                                                    }
                                                }];
            
        } else {
            // permission exists
            [self presentFBDialog];
        }
    } else {
        [self openFBSession];
    }
}

- (void)openFBSession
{
    // if the session is closed, then we open it here, and establish a handler for state changes
    [FBSession openActiveSessionWithReadPermissions:nil
                                       allowLoginUI:YES
                                  completionHandler:^(FBSession *session,
                                                      FBSessionState state,
                                                      NSError *error) {
                                      if (error) {
                                          UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                                              message:error.localizedDescription
                                                                                             delegate:nil
                                                                                    cancelButtonTitle:@"OK"
                                                                                    otherButtonTitles:nil];
                                          [alertView show];
                                      } else if (session.isOpen) {
                                          [self presentFBDialog];
                                      }
                                  }];
}

- (void)presentFBDialog {
    // Present share dialog
    if (!self.isLockerShare) {
        // Put together the dialog parameters
        NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                       self.fbTitle, @"name",
                                       self.fbMessage, @"caption",
                                       @"Vaiden is a community for athletes to play their favorite sports using a social platform.", @"description",
                                       @"https://itunes.apple.com/us/app/vaiden/id796761406?ls=1&mt=8", @"link",
                                       self.fbImageString, @"picture",
                                       nil];
        
        // Show the feed dialog
        [FBWebDialogs presentFeedDialogModallyWithSession:nil
                                               parameters:params
                                                  handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
                                                      if (error) {
                                                          // An error occurred, we need to handle the error
                                                          // See: https://developers.facebook.com/docs/ios/errors
                                                          //                    NSLog([NSString stringWithFormat:@"Error publishing story: %@", error.description]);
                                                      } else {
                                                          if (result == FBWebDialogResultDialogNotCompleted) {
                                                              // User cancelled.
                                                              NSLog(@"User cancelled.");
                                                          } else {
                                                              // Handle the publish feed callback
                                                              NSDictionary *urlParams = [self parseURLParams:[resultURL query]];
                                                              
                                                              if (![urlParams valueForKey:@"post_id"]) {
                                                                  // User cancelled.
                                                                  NSLog(@"User cancelled.");
                                                                  
                                                              } else {
                                                                  // User clicked the Share button
                                                                  NSString *result = [NSString stringWithFormat: @"Posted story, id: %@", [urlParams valueForKey:@"post_id"]];
                                                                  NSLog(@"result %@", result);
                                                              }
                                                          }
                                                      }
                                                  }];
    } else {
        [self performSegueWithIdentifier:@"confirm_segue" sender:self];
    }
}

- (void)startShareImage
{
    // Loading overlay
    HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    HUD.labelText = @"Sharing...";
    
    NSMutableDictionary* params = [[NSMutableDictionary alloc] init];
    NSString *myMessage = self.fbMessage;
    [params setObject:myMessage forKey:@"message"]; // caption showed on the top of the sharing image
    
    /*UIImageView *tmpView = [[UIImageView alloc] init];
    [tmpView setImageWithURL:[NSURL URLWithString:self.fbImageString]
            placeholderImage:[UIImage imageNamed:@"avatar square"]];*/
    UIImage * image = [self addOverlayToPostImage:self.currentPickedImage];
    
    [params setObject:UIImagePNGRepresentation(image) forKey:@"picture"];
    
    [FBRequestConnection startWithGraphPath:@"me/photos"
                                 parameters:params
                                 HTTPMethod:@"POST"
                          completionHandler:^(FBRequestConnection *connection,
                                              id result,
                                              NSError *error)
     {
         [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
         if (error){
             UIAlertView *alert = [[UIAlertView alloc] init];
             alert.title = @"Submission Error!";
             alert.message = [NSString stringWithFormat:@"%@", error];
             [alert addButtonWithTitle:@"OK"];
             [alert show];
         } else {
             HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
             HUD.labelText = @"Shared!";
             //[HUD setMode:MBProgressHUDModeText];
             double delayInSeconds = 1.0; // delay one sec for the sucess message, and dismiss
             dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
             dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                 [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
             });
         }
     }];
}

- (UIImage *)addOverlayToPostImage:(UIImage *)sourcePic
{
    int total = self.stats.count;
    
    UIImage *rightCornerLogo = [UIImage imageNamed:@"vaiden_overlay_logo"];
    UIImage *bgShadow = [UIImage imageNamed:@"overlay_shadow"];
    
    if (total > 0) {
        UIImage *seperator = [UIImage imageNamed:@"seperator"];
        UIImage *lineImage;
        
        // draw the lines for single stat
        if (total == 1) {
            CGSize imageSize = CGSizeMake(60, 1);
            UIColor *fillColor = [UIColor whiteColor];
            UIGraphicsBeginImageContextWithOptions(imageSize, YES, 0);
            CGContextRef context = UIGraphicsGetCurrentContext();
            [fillColor setFill];
            CGContextFillRect(context, CGRectMake(0, 0, imageSize.width, imageSize.height));
            lineImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
        }
        
        CGSize size = CGSizeMake(sourcePic.size.width, sourcePic.size.height);
        
        UIGraphicsBeginImageContext(size);
        
        [sourcePic drawInRect:CGRectMake(0, 0, size.width, size.height)];
        [bgShadow drawInRect:CGRectMake(0, size.height-bgShadow.size.height, size.width, bgShadow.size.height)];
        [rightCornerLogo drawInRect:CGRectMake(size.width-rightCornerLogo.size.width-20, size.height-rightCornerLogo.size.height-10, rightCornerLogo.size.width, rightCornerLogo.size.height)];
        
        // Write the stats upon the image
        NSString *upperStr = @"";
        NSString *lowerStr = @"";
        
        for (int i=0; i<total; i++) {
            id test = [self.stats objectAtIndex:i];
            if ([test isKindOfClass:[StatTag class]]) {
                StatTag *stat = (StatTag *)test;
                if (stat.showSubCategory) {
                    upperStr = stat.statTagValue;
                    lowerStr = [NSString stringWithFormat:@"%@ %@",stat.statLink, stat.statTagName];
                } else {
                    upperStr = stat.statTagValue;
                    lowerStr = stat.statTagName;
                }
            } else if ([test isKindOfClass:[StatTagCombination class]]) {
                StatTagCombination *sta = (StatTagCombination *)test;
                upperStr = [NSString stringWithFormat:@"%@/%@", sta.numerator.statTagValue, sta.denominator.statTagValue];
                lowerStr = sta.numerator.statLink;
            }
            
            CGFloat width = size.width - rightCornerLogo.size.width - 30; //10 for space
            if (total == 1) {
                width = width/2;
            } else if (total == 2) {
                width = width/2;
            } else if (total == 3) {
                width = width/3;
            }
            
            UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:15];
            CGSize lineSize = [upperStr sizeWithAttributes:@{NSFontAttributeName:font}];
            CGRect upperRect;
            if (total == 1) {
                upperRect = CGRectMake(width - lineSize.width/2, size.height- (10 + 35), size.width, width-10);
            } else {
                upperRect = CGRectMake(width*i + width/2 - lineSize.width/2, size.height- (10 + 35), size.width, width-10);
            }
            [[UIColor whiteColor] set];
            [upperStr drawInRect:CGRectIntegral(upperRect) withFont:font];
            
            if (i < total - 1) { // Not the last item
                [seperator drawInRect:CGRectMake(width*(i+1), size.height- (5 + 23), seperator.size.width, seperator.size.height)];
            }
            
            UIFont *font2 = [UIFont fontWithName:@"HelveticaNeue" size:9];
            CGSize lineSize2 = [lowerStr sizeWithAttributes:@{NSFontAttributeName:font2}];
            if (lineSize2.width > width - 10) { // check if the string is out of bound, if so wrap the string by space
                NSArray * sepStr = [self wrapSentences:lowerStr withFont:font2 withinBounds:(width - 10)];
                for (int k=0; k<sepStr.count; k++) {
                    lineSize2 = [[sepStr objectAtIndex:k] sizeWithAttributes:@{NSFontAttributeName:font2}];
                    CGRect lowerRect = CGRectMake(width*i + width/2 - lineSize2.width/2, size.height- (17 + k*8), size.width, width-10);
                    [[sepStr objectAtIndex:k] drawInRect:CGRectIntegral(lowerRect) withFont:font2];
                }
            } else {
                CGRect lowerRect;
                if (total == 1) {
                    lowerRect = CGRectMake(width - lineSize2.width/2, size.height- (10 + 12), size.width, width-10);
                } else {
                    lowerRect = CGRectMake(width*i + width/2 - lineSize2.width/2, size.height- (10 + 12), size.width, width-10);
                }
                [lowerStr drawInRect:CGRectIntegral(lowerRect) withFont:font2];
            }
            
            if (total == 1) {
                [lineImage drawInRect:CGRectMake(width - lineSize2.width/2 - 10 - lineImage.size.width, size.height - (10 + 17), lineImage.size.width, lineImage.size.height)];
                [lineImage drawInRect:CGRectMake(width + lineSize2.width/2 + 10, size.height - (10 + 17), lineImage.size.width, lineImage.size.height)];
            }
        }
        
        UIImage *finalImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return finalImage;
    } else if (self.drills.count > 0) {
        UIImage *drillIcon = [UIImage imageNamed:@"white_drill"];
        
        CGSize size = CGSizeMake(sourcePic.size.width, sourcePic.size.height);
        UIGraphicsBeginImageContext(size);
        
        [sourcePic drawInRect:CGRectMake(0, 0, size.width, size.height)];
        [bgShadow drawInRect:CGRectMake(0, size.height-bgShadow.size.height, size.width, bgShadow.size.height)];
        [rightCornerLogo drawInRect:CGRectMake(size.width-rightCornerLogo.size.width-20, size.height-rightCornerLogo.size.height-10, rightCornerLogo.size.width, rightCornerLogo.size.height)];
        [drillIcon drawInRect:CGRectMake(10, size.height-drillIcon.size.height - 28, drillIcon.size.width, drillIcon.size.height)];
        
        // Write the drills upon the image
        NSString *drillText = @"";
        
        for (TrainingTag *tag in self.drills) {
            if ([drillText length] > 0)
                drillText = [drillText stringByAppendingString:@", "];
            drillText = [drillText stringByAppendingString:tag.trainingTagName];
        }
        
        CGFloat width = size.width - rightCornerLogo.size.width - 30; //10 for space
        
        UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:10];
        //CGSize lineSize = [drillText sizeWithAttributes:@{NSFontAttributeName:font}];
        CGRect textFrame = CGRectMake(10 + drillIcon.size.width + 5, size.height- (10 + 30), width-30, width-10);
        [[UIColor whiteColor] set];
        [drillText drawInRect:CGRectIntegral(textFrame) withFont:font];
        
        UIImage *finalImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return finalImage;
    } else {
        return sourcePic;
    }
}


/* Wrap the sentence without UILabelView by space character based on bounds and font */
- (NSMutableArray *) wrapSentences:(NSString *)source withFont:(UIFont *)font withinBounds:(CGFloat)bounds
{
    NSArray *sec = [source componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSMutableArray * sperateString = [[NSMutableArray alloc] init];
    NSString * part = @"";
    NSString * tmp = @"";
    CGSize tmpSize;
    for (int i=0; i<sec.count; i++) {
        if (part.length == 0) {
            part = (NSString *)[sec objectAtIndex:i];
        } else {
            tmp = [part stringByAppendingString:[NSString stringWithFormat:@" %@", (NSString *)[sec objectAtIndex:i]]];
            tmpSize = [tmp sizeWithAttributes:@{NSFontAttributeName:font}];
        }
        if (tmpSize.width > bounds) {
            [sperateString addObject:part];
            part = @""; // clear string
            tmpSize = CGSizeZero;
            i--; //back to last item
        } else if (i==sec.count-1) { //last item of the array
            [sperateString addObject:part];
        } else if (tmp.length > 0){ // otherwise
            part = tmp;
        }
    }
    NSLog(@"%@",sperateString);
    return sperateString;
}

// A function for parsing URL parameters returned by the Feed Dialog.
- (NSDictionary*)parseURLParams:(NSString *)query {
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *pair in pairs) {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        NSString *val =
        [kv[1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        params[kv[0]] = val;
    }
    return params;
}


- (void)setWithNumCommentsFromVC:(CommentsTVC *)controller withNumComments:(NSUInteger)numComments forSection:(NSUInteger)currentTableSection
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:currentTableSection];

    if ([[self.newsfeedPaginator.results objectAtIndex:indexPath.section] isKindOfClass:[IndividualMatchCreationNews class]]) {
         IndividualMatchCreationNews *iNews = [self.newsfeedPaginator.results objectAtIndex:currentTableSection];
        iNews.numComments = numComments;
        
    } else if ([[self.newsfeedPaginator.results objectAtIndex:indexPath.section] isKindOfClass:[PickupMatchCreationNews class]]) {
         PickupMatchCreationNews *pNews = [self.newsfeedPaginator.results objectAtIndex:currentTableSection];
        pNews.numComments = numComments;
        
    } else if ([[self.newsfeedPaginator.results objectAtIndex:indexPath.section] isKindOfClass:[ImagePost class]]) {
        ImagePost *iPost = [self.newsfeedPaginator.results objectAtIndex:currentTableSection];
        iPost.numComments = numComments;
        
    } else if ([[self.newsfeedPaginator.results objectAtIndex:indexPath.section] isKindOfClass:[TextPost class]]) {
        TextPost *tPost = [self.newsfeedPaginator.results objectAtIndex:currentTableSection];
        tPost.numComments = numComments;
    } else if ([[self.newsfeedPaginator.results objectAtIndex:indexPath.section] isKindOfClass:[MatchPlayerNewsOfficialFinishedIndividualMatch class]]) {
        MatchPlayerNewsOfficialFinishedIndividualMatch *mPost = [self.newsfeedPaginator.results objectAtIndex:currentTableSection];
        mPost.numComments = numComments;
    } else if ([[self.newsfeedPaginator.results objectAtIndex:indexPath.section] isKindOfClass:[CompetitionNews class]]) {
        CompetitionNews *cPost = [self.newsfeedPaginator.results objectAtIndex:currentTableSection];
        cPost.numComments = numComments;
    } else if ([[self.newsfeedPaginator.results objectAtIndex:indexPath.section] isKindOfClass:[VenueKingNews class]]) {
        VenueKingNews *vPost = [self.newsfeedPaginator.results objectAtIndex:currentTableSection];
        vPost.numComments = numComments;
    } else if ([[self.newsfeedPaginator.results objectAtIndex:indexPath.section] isKindOfClass:[VenueCheckinNews class]]) {
        VenueCheckinNews *vPost = [self.newsfeedPaginator.results objectAtIndex:currentTableSection];
        vPost.numComments = numComments;
    } else if ([[self.newsfeedPaginator.results objectAtIndex:indexPath.section] isKindOfClass:[LockerPost class]]) {
        LockerPost *iPost = [self.newsfeedPaginator.results objectAtIndex:currentTableSection];
        iPost.numComments = numComments;
        
    }
    
    [self.tableView beginUpdates];
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    [self.tableView endUpdates];
 //   self.scrollToSection = currentTableSection;
    
    

}

- (IBAction)matchDetailsForMatchPlayerNewsUpdate:(UIButton *)sender
{
    self.selectedSection = sender.tag;
     MatchPlayerNews *iNews = (self.newsfeedPaginator.results)[self.selectedSection];
    if ([iNews.match isKindOfClass:[IndividualMatch class]]) {
        [self performSegueWithIdentifier:@"Individual Match Segue" sender:self];
    } else if ([iNews.match isKindOfClass:[PickupMatch class]])  {
        [self performSegueWithIdentifier:@"Pickup Match Detail" sender:self];
    }

}

- (IBAction)matchDetailsMatchNewsUpdate:(UIButton *)sender
{
    self.selectedSection = sender.tag;
    
    MatchNews *iNews = (self.newsfeedPaginator.results)[self.selectedSection];
    if ([iNews.match isKindOfClass:[IndividualMatch class]]) {
        [self performSegueWithIdentifier:@"Individual Match Segue" sender:self];
    } else if ([iNews.match isKindOfClass:[PickupMatch class]])  {
        [self performSegueWithIdentifier:@"Pickup Match Detail" sender:self];
    }

}
- (IBAction)matchDetailsIndividualMatchCreation:(UIButton *)sender
{
    self.selectedSection = sender.tag;
    
    [self performSegueWithIdentifier:@"Individual Match Segue" sender:self];
}

- (IBAction)matchDetailsTeamMatchCreation:(UIButton *)sender
{
    self.selectedSection = sender.tag;
    
    [self performSegueWithIdentifier:@"Team Match Segue" sender:self];
}

- (IBAction)matchDetailsForPickupMatchCreation:(UIButton *)sender
{
    self.selectedSection = sender.tag;
    [self performSegueWithIdentifier:@"Pickup Match Detail" sender:self];
}

- (IBAction)clickAdAction:(id)sender
{
    [self performSegueWithIdentifier:@"ad segue" sender:self];
}
- (IBAction)competitionDetailsAction:(id)sender
{
    [self performSegueWithIdentifier:@"ad segue" sender:self];
}
- (IBAction)venueKingDetailsAction:(UIButton *)sender
{
    self.selectedVenue = sender.tag;
    [self performSegueWithIdentifier:@"Venue Detail Segue" sender:self];
}

- (void)goToTeamPage:(UIButton *)sender
{
    self.selectedPlayerToShowDetailPage = sender.tag;
    [self performSegueWithIdentifier:@"Team Detail Segue" sender:self];
}

- (void)postExtraActions:(UIButton *)sender
{
    NSInteger userIDToTest = 0;
    BOOL publicNews = NO;
    
    News *lp = (self.newsfeedPaginator.results)[sender.tag];
    self.selectedSection = sender.tag;
    
  /*  if ([lp isKindOfClass:[IndividualMatchCreationNews class]]) {
        IndividualMatchCreationNews *n = (IndividualMatchCreationNews *)lp;
        userIDToTest = n.individualMatch.matchCreatorID;
    } else if ([lp isKindOfClass:[MatchPlayerNewsOfficialFinishedIndividualMatch class]]) {
        MatchPlayerNewsOfficialFinishedIndividualMatch *n = (MatchPlayerNewsOfficialFinishedIndividualMatch *)lp;
        userIDToTest = n.match.matchCreatorID;
    } else if ([lp isKindOfClass:[PickupMatchCreationNews class]]) {
        PickupMatchCreationNews *n = (PickupMatchCreationNews *)lp;
        userIDToTest = n.pickupMatch.matchCreatorID;
    } else {
        userIDToTest = lp.newsMakerUserID;
    }
        */
    
    if ([lp isKindOfClass:[IndividualMatchCreationNews class]]) {
        IndividualMatchCreationNews *n = (IndividualMatchCreationNews *)lp;
        userIDToTest = n.individualMatch.matchCreatorID;
        
        self.fbTitle = [NSString stringWithFormat:@"%@ challenged %@", n.individualMatch.matchCreatorUsername, [n.individualMatch getOtherPlayer:n.individualMatch.matchCreatorID].userName];
        self.fbMessage = [NSString stringWithFormat:@"Learn more about this %@ Challenge Match on the Vaiden iPhone App", [n.individualMatch.sport.sportName capitalizedString]];
        self.fbImageString = @"https://s3.amazonaws.com/vaderen_pictures/vaiden_fb_icon.png";
        self.isLockerShare = NO;
    } else if ([lp isKindOfClass:[MatchPlayerNewsOfficialFinishedIndividualMatch class]]) {
        MatchPlayerNewsOfficialFinishedIndividualMatch *n = (MatchPlayerNewsOfficialFinishedIndividualMatch *)lp;
        IndividualMatch *m = (IndividualMatch *)n.match;
        userIDToTest = n.match.matchCreatorID;
        self.fbTitle = [NSString stringWithFormat:@"%@ challenged %@", n.match.matchCreatorUsername, [m getOtherPlayer:n.match.matchCreatorID].userName];
        self.fbMessage = [NSString stringWithFormat:@"Learn more about this %@ Challenge Match on the Vaiden iPhone App", [n.match.sport.sportName capitalizedString]];
        self.fbImageString = @"https://s3.amazonaws.com/vaderen_pictures/vaiden_fb_icon.png";
        self.isLockerShare = NO;
    } else if ([lp isKindOfClass:[PickupMatchCreationNews class]]) {
        PickupMatchCreationNews *n = (PickupMatchCreationNews *)lp;
        userIDToTest = n.pickupMatch.matchCreatorID;
        self.isLockerShare = NO;
        self.fbTitle = [NSString stringWithFormat:@"%@ created a pickup match", n.pickupMatch.matchCreatorUsername];
        self.fbMessage = [NSString stringWithFormat:@"Learn more about this %@ Pickup Match on the Vaiden iPhone App", [n.pickupMatch.sport.sportName capitalizedString]];
        self.fbImageString = @"https://s3.ama zonaws.com/vaderen_pictures/vaiden_fb_icon.png";
        
    } else {
        userIDToTest = lp.newsMakerUserID;
    }
    
    if ([lp isKindOfClass:[LockerPost class]]) {
        LockerPost *temp = (LockerPost *)lp;
        self.stats = temp.lockPic.statsGrp.statsArray;
        self.drills = temp.lockPic.trainingTagsArray;
        self.fbImageString = [S3Tools2 getFileNameStringWithType:@"photo_regsize" baseString:[NSString stringWithFormat:@"%ld/photo/%@", (long)temp.newsMakerUserID, temp.lockPic.imageFileName]];
        
        self.fbTitle = [NSString stringWithFormat:@"%@ shared an image via Vaiden", [self.preferences getUserName]];
        self.fbMessage = [NSString stringWithFormat:@"\"%@\"", temp.textPost];
        self.isLockerShare = YES;
    } else if ([lp isKindOfClass:[ImagePost class]]) {
        ImagePost *n = (ImagePost *)lp;
        self.isLockerShare = NO;
        NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                baseString:n.imageBaseFileString];
        self.fbMessage = n.textPost;
        self.fbImageString = url;
        self.fbTitle = [NSString stringWithFormat:@"%@ shared an image from the Vaiden App", n.sharedByPlayer.userName];
        
    } else if ([lp isKindOfClass:[TextPost class]]) {
        TextPost *n = (TextPost *)lp;
        self.fbTitle = [NSString stringWithFormat:@"%@ posts on the Vaiden App", n.newsMakerUserName];
        self.fbMessage = [NSString stringWithFormat:@"\"%@\"", n.textPost];
        self.fbImageString = @"https://s3.amazonaws.com/vaderen_pictures/vaiden_fb_icon.png";
        self.isLockerShare = NO;
    } else if ([lp isKindOfClass:[VoteReceiving class]]) {
        VoteReceiving *n = (VoteReceiving *)lp;
        if ([n.type isEqualToString:@"receiving"]) {
            self.fbTitle = [NSString stringWithFormat:@"%@ receives votes on the Vaiden App", n.newsMakerUserName];
        } else if ([n.type isEqualToString:@"giving"]) {
            self.fbTitle = [NSString stringWithFormat:@"%@ gave votes on the Vaiden App", n.newsMakerUserName];
        } else if ([n.type isEqualToString:@"thanks"]) {
            self.fbTitle = [NSString stringWithFormat:@"%@ sent thanks to %@ on the Vaiden App", n.newsMakerUserName, n.targetName];
        } else if ([n.type isEqualToString:@"level"]) {
            self.fbTitle = [NSString stringWithFormat:@"%@ reached a new vote level on the Vaiden App", n.newsMakerUserName];
        } else if ([n.type isEqualToString:@"winner"]) {
            self.fbTitle = [NSString stringWithFormat:@"%@ won a vaiden endorsement", n.string];
        }
        if ([n.type isEqualToString:@"winner"]) {
            publicNews = YES;
            self.fbMessage = [NSString stringWithFormat:@"\"%@ won this month's $500 Vaiden Endorsement\"", n.string];
        } else {
            self.fbMessage = [NSString stringWithFormat:@"\"%@\"", n.string];
        }
        self.fbImageString = @"https://s3.amazonaws.com/vaderen_pictures/vaiden_fb_icon.png";
        self.isLockerShare = NO;
    } else if ([lp isKindOfClass:[CompetitionNews class]]) {
        
        CompetitionNews *n = (CompetitionNews *)lp;
        self.isLockerShare = NO;
        self.fbMessage = @"The 1v1 Intercollegiate Basketball Championships.  College students are welcome to register to compete!";
        self.fbImageString = @"https://s3.amazonaws.com/vaderen_pictures/vaiden_fb_icon.png";
        self.fbTitle = [NSString stringWithFormat:@"%@ is playing in a competition", n.newsMakerUserName];
        
    } else if ([lp isKindOfClass:[VenueKingNews class]]) {
        VenueKingNews *n = (VenueKingNews *)lp;
        self.isLockerShare = NO;
        self.fbMessage = [NSString stringWithFormat:@"%@ for %@", n.venue.venueName, [n.sport.sportName capitalizedString]];
        self.fbTitle = [NSString stringWithFormat:@"%@ became King Of a Venue", n.newsMakerUserName];
        self.fbImageString = @"https://s3.amazonaws.com/vaderen_pictures/vaiden_fb_icon.png";
        
    } else if ([lp isKindOfClass:[VenueCheckinNews class]]) {
        VenueCheckinNews *vPost = (VenueCheckinNews *)lp;
        self.isLockerShare = NO;
        if (vPost.vci.isNow) {
            if ([vPost.vci.checkedInSport.sportName length] > 0)
                self.fbTitle = [NSString stringWithFormat:@"@%@ checked in at %@ to play %@", vPost.vci.checkedInUser.userName, vPost.venue.venueName, vPost.vci.checkedInSport.sportName];
            else
                self.fbTitle = [NSString stringWithFormat:@"@%@ checked in at %@", vPost.vci.checkedInUser.userName, vPost.venue.venueName];
        } else {
            if ([vPost.vci.checkedInSport.sportName length] > 0)
                self.fbTitle = [NSString stringWithFormat:@"@%@ will be at %@ at %@ to play %@", vPost.vci.checkedInUser.userName, vPost.venue.venueName, [vPost.vci.checkedInDateTime getDateFullDateString], vPost.vci.checkedInSport.sportName];
            else
                self.fbTitle = [NSString stringWithFormat:@"@%@ will be at %@ at %@", vPost.vci.checkedInUser.userName, vPost.venue.venueName, [vPost.vci.checkedInDateTime getDateFullDateString]];
        }
        
        
        if ([vPost.vci.message length] > 0)
            self.fbMessage = [NSString stringWithFormat:@"%@ says: \"%@\"", vPost.vci.checkedInUser.userName, vPost.vci.message];
        else
            self.fbMessage = @"";
        
    }

    
    if (userIDToTest == [self.preferences getUserID]) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                        initWithTitle:nil delegate:self
                                        cancelButtonTitle:CANCEL_BUTTON_ACTIONSHEET destructiveButtonTitle:nil
                                        otherButtonTitles:DELETE_POST, FACEBOOK_SHARE, nil];
        [actionSheet showFromTabBar:self.tabBarController.tabBar];
    } else if (publicNews) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                      initWithTitle:nil delegate:self
                                      cancelButtonTitle:CANCEL_BUTTON_ACTIONSHEET destructiveButtonTitle:nil
                                      otherButtonTitles:FACEBOOK_SHARE, nil];
        [actionSheet showFromTabBar:self.tabBarController.tabBar];
    } else {
        UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                        initWithTitle:nil delegate:self
                                        cancelButtonTitle:CANCEL_BUTTON_ACTIONSHEET destructiveButtonTitle:nil
                                        otherButtonTitles:FLAG_POST, nil];
        [actionSheet showFromTabBar:self.tabBarController.tabBar];
    }
}

- (void)deletePost
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Delete Confirmation"
                                                    message:@"Are you sure you would like to delete this post?"
                                                   delegate:self cancelButtonTitle:@"Cancel"
                                          otherButtonTitles:@"Delete", nil];
    
    [alert show];
    

        

}

- (void)flagPost
{
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Report Confirmation"
                                                    message:@"Are you sure you would like to report this post as inappropriate?"
                                                   delegate:self cancelButtonTitle:@"Cancel"
                                          otherButtonTitles:@"Report", nil];
    
    [alert show];
    
    

}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    News *n = (self.newsfeedPaginator.results)[self.selectedSection];

    if ([[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"Delete"]) {
        HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        HUD.labelText = @"Deleting";
        [IOSRequest deletePost:n.newsID
                        byUser:[self.preferences getUserID]
                  onCompletion:^(NSDictionary *results) {
                      
                      dispatch_async(dispatch_get_main_queue(), ^{
                          if ([results[@"outcome"] isEqualToString:@"success"]) {
                              HUD.labelText = @"Deleted!";
                              double delayInSeconds = 2.0;
                              dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
                              dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                                  //code to be executed on the main queue after delay
                                  [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
                                  [self.preferences setProfilePageRefreshState:YES];
                                  [self.newsfeedPaginator.results removeObjectAtIndex:self.selectedSection];
                                  [self.tableView reloadData];
                              });
                          } else {
                              [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
                              UIAlertView *alert = [[UIAlertView alloc] init];
                              alert.title = @"Deletion Error!";
                              alert.message = @"There was a problem completing your request.  Please try again.";
                              [alert addButtonWithTitle:@"OK"];
                              [alert show];
                          }
                      });
                  }];
        
        double delayInSeconds = 10.0;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            //code to be executed on the main queue after delay
            [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
        });
    } if ([[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"Report"]) {
        HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        HUD.labelText = @"Reporting Inappropriate";
        [IOSRequest flagPost:n.newsID
                      byUser:[self.preferences getUserID]
                withFeedback:@""
                onCompletion:^(NSDictionary *results) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if ([results[@"outcome"] isEqualToString:@"success"]) {
                            HUD.labelText = @"Post Reported!";
                            double delayInSeconds = 2.0;
                            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
                            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                                //code to be executed on the main queue after delay
                                [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
                                [self.preferences setProfilePageRefreshState:YES];
                            });
                        } else {
                            [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
                            UIAlertView *alert = [[UIAlertView alloc] init];
                            alert.title = @"Submission Error!";
                            alert.message = @"There was a problem completing your request.  Please try again.";
                            [alert addButtonWithTitle:@"OK"];
                            [alert show];
                        }
                    });
                    
                }];
    }
}

- (void)likePost:(UIButton *)sender
{
    News *n = (self.newsfeedPaginator.results)[sender.tag];
    sender.enabled = NO;

    if (n.doesSessionUserLikePost) {
        [IOSRequest UnLikeNewsfeedPost:n.newsID
                         sessionUserID:[self.preferences getUserID]
                          onCompletion:^(NSDictionary *results) {
                              dispatch_async(dispatch_get_main_queue(), ^{
                                  
                                  if ([results[@"outcome"] isEqualToString:@"failure"]) {
                                      UIAlertView *alert = [[UIAlertView alloc] init];
                                      alert.title = @"UnLike Error";
                                      alert.message = @"There was a problem completing your request.  Please try again later.";
                                      [alert addButtonWithTitle:@"OK"];
                                      [alert show];
                                  } else {
                                      n.doesSessionUserLikePost = NO;
                                      n.numLikes--;
                                      
                                      [self.newsfeedPaginator.results replaceObjectAtIndex:sender.tag withObject:n];
                                      
                                      // need to update in array
                                      [self.tableView reloadData];
                                      sender.enabled = YES;
                                  }
                              });
                              
                          }];
    } else {
        [IOSRequest likeNewsfeedPost:n.newsID
                       sessionUserID:[self.preferences getUserID]
                        onCompletion:^(NSDictionary *results) {
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                if ([results[@"outcome"] isEqualToString:@"failure"]) {
                                    UIAlertView *alert = [[UIAlertView alloc] init];
                                    alert.title = @"Like Error";
                                    alert.message = @"There was a problem completing your request.  Please try again later.";
                                    [alert addButtonWithTitle:@"OK"];
                                    [alert show];
                                } else {
                                    n.doesSessionUserLikePost = YES;
                                    n.numLikes++;
                                    
                                    [self.newsfeedPaginator.results replaceObjectAtIndex:sender.tag withObject:n];
                                    
                                    [self.tableView reloadData];
                                    sender.enabled = YES;
                                }
                            });
                        }];
    }

    
}

- (void)viewLikes:(UIButton *)sender
{
    self.selectedSection = sender.tag;
    [self performSegueWithIdentifier:@"View Likes Segue" sender:self];
}

- (void)viewComments:(UIButton *)sender
{
    self.selectedSection = sender.tag;
    self.showKeyboardOnCommentsPage = NO;
    [self performSegueWithIdentifier:@"Comment Segue" sender:self];
}

- (void)viewLockerImage:(UIButton *)sender
{
    self.selectedLockerID = sender.tag;
    [self performSegueWithIdentifier:@"Locker Detail Segue" sender:self];
}

- (IBAction)choosePic:(UIButton *)sender
{
    if (!self.choosePicActionSheet) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                      initWithTitle:TITLE_OF_ACTIONSHEET delegate:self
                                      cancelButtonTitle:CANCEL_BUTTON_ACTIONSHEET destructiveButtonTitle:nil
                                      otherButtonTitles:FROM_CAMERA, FROM_LIBRARY, nil];
        [actionSheet showFromTabBar:self.tabBarController.tabBar];
        
        self.choosePicActionSheet = actionSheet;
    }
    
}

- (void)changePicFromCamera
{
    [self presentImagePicker:UIImagePickerControllerSourceTypeCamera];
}


- (void) changePicFromPhotoLibrary
{
    [self presentImagePicker:UIImagePickerControllerSourceTypeSavedPhotosAlbum];
}

- (void) presentImagePicker:(UIImagePickerControllerSourceType)sourceType
{
    if ([UIImagePickerController isSourceTypeAvailable:sourceType]) {
        NSArray *availableMediaTypes = [UIImagePickerController availableMediaTypesForSourceType:sourceType];
        if ([availableMediaTypes containsObject:(NSString *)kUTTypeImage]) {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.sourceType = sourceType;
            picker.mediaTypes = @[(NSString *)kUTTypeImage];
            picker.allowsEditing = YES;
            picker.delegate = self;
            
            
            [self presentViewController:picker animated:YES completion:nil];
        }
    }
}

- (void) imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

#define MAX_IMAGE_WIDTH 70 // might want to change this to height....

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = info[UIImagePickerControllerEditedImage];
    
    if (!image) image = info[UIImagePickerControllerOriginalImage];
    if (image) {
        //  self.profilePicOriginal = image;
        UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
        CGRect frame = imageView.frame;
        if (frame.size.width > MAX_IMAGE_WIDTH) {
            frame.size.height = (frame.size.height / frame.size.width) * MAX_IMAGE_WIDTH;
            frame.size.width = MAX_IMAGE_WIDTH;
        }
        imageView.frame = frame;
        
        self.viObject = [[VaidenImageObject alloc] initWithOrigImageData:UIImageJPEGRepresentation([image imageScaledToFitSize:CGSizeMake(1000, 1000)], 1.0)
                                                 andRegularSizeImageData:UIImageJPEGRepresentation([image imageScaledToFitSize:CGSizeMake(320, 320)], 1.0)
                                           andRegularSizeRetinaImageData:UIImageJPEGRepresentation([image imageScaledToFitSize:CGSizeMake(640, 640)], 1.0)
                                                        andThumbnailData:UIImageJPEGRepresentation([image imageScaledToFitSize:CGSizeMake(70, 70)], 1.0)
                                                  andRetinaThumbnailData:UIImageJPEGRepresentation([image imageScaledToFitSize:CGSizeMake(140, 140)], 1.0)
                                                        andFileExtension:@"jpg"
                                                                andImage:image];
        
        
    }
    
    if (self.imagePickerPopover) {
        
        [self.imagePickerPopover dismissPopoverAnimated:YES];
        self.imagePickerPopover = nil;
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
    [self saveImageToAmazon];
    [self performSegueWithIdentifier:@"Add Pic Segue" sender:self];
}

- (void)saveImageToAmazon
{
    if (![AmazonClientManager hasCredentials]) {
        [[Constants credentialsAlert] show];
        NSLog(@"no credentials");
    }
    else {
        
        NSLog (@"has credentials");
        
        NSOperationQueue *operationQueue = [NSOperationQueue new];
        operationQueue.maxConcurrentOperationCount = 3;
        
        NSMutableDictionary *imageNameDictionary = [S3Tools2 getFileNameBaseStringWithType:@"photo" forUser:[self.preferences getUserID]];
        
        self.imageFileName = [imageNameDictionary objectForKey:@"image_name"];
        self.imageFileNameBaseString = [imageNameDictionary objectForKey:@"base_string"];
        
        self.regImageSizeUploader = [[S3Tools2 alloc] initWithData:self.viObject.regularSizeImageData
                                                           forUser:[self.preferences getUserID]
                                                          dataType:@"photo_regsize"
                                             andBaseFileNameString:self.imageFileNameBaseString
                                                        andFileExt:self.viObject.fileExtension];
        
        [operationQueue addOperation:self.regImageSizeUploader];
        
        self.regImageRetinaSizeUploader = [[S3Tools2 alloc] initWithData:self.viObject.regularSizeRetinaImageData
                                                                 forUser:[self.preferences getUserID]
                                                                dataType:@"photo_regsize_retina"
                                                   andBaseFileNameString:self.imageFileNameBaseString
                                                              andFileExt:self.viObject.fileExtension];
        
        [operationQueue addOperation:self.regImageRetinaSizeUploader];
        
        
        self.thumbnailImageSizeUploader = [[S3Tools2 alloc] initWithData:self.viObject.thumbnailImageData
                                                                 forUser:[self.preferences getUserID]
                                                                dataType:@"photo_thumbnail"
                                                   andBaseFileNameString:self.imageFileNameBaseString
                                                              andFileExt:self.viObject.fileExtension];
        
        [operationQueue addOperation:self.thumbnailImageSizeUploader];
        
        self.thumbnailImageRetinaSizeUploader = [[S3Tools2 alloc] initWithData:self.viObject.thumbnailRetinaImageData
                                                                       forUser:[self.preferences getUserID]
                                                                      dataType:@"photo_thumbnail_retina"
                                                         andBaseFileNameString:self.imageFileNameBaseString
                                                                    andFileExt:self.viObject.fileExtension];
        
        [operationQueue addOperation:self.thumbnailImageRetinaSizeUploader];
        
        
    }
}

- (IBAction)textPostClicked:(id)sender {
    [self performSegueWithIdentifier:@"text_post_segue" sender:sender];
}

#pragma mark - Coment dialog delegate
- (void) startSumitComments:(NSString *)comments
{
#warning not block UI yet.
    
    [IOSRequest saveNewsfeedTextPost:comments
                             forUser:[self.preferences getUserID]
                        onCompletion:^(NSDictionary *result) {
                            if ([result[@"result"] isEqualToString:@"failure"]) {
                                UIAlertView *alert = [[UIAlertView alloc] init];
                                alert.title = @"Submission Error!";
                                alert.message = @"There was a problem completing your request.  Please try again.";
                                [alert addButtonWithTitle:@"OK"];
                                [alert show];
                            } else if ([result[@"result"] isEqualToString:@"success"]) {
                                [self.newsfeedPaginator fetchFirstPageWithTab:NEWSFEED_MYNEWS_SEGMENT andNewsfeedDetailID:self.newsfeedDetailID];
                            }
                        }];
}

@end
