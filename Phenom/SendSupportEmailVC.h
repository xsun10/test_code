//
//  SendSupportEmailVC.h
//  Vaiden
//
//  Created by James Chung on 9/18/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "CustomBaseVC.h"

@interface SendSupportEmailVC : CustomBaseVC <MFMailComposeViewControllerDelegate> {

}

- (IBAction)actionEmailComposer;

@end
