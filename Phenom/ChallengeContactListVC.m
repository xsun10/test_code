//
//  ChallengeContactListVC.m
//  Phenom
//
//  Created by James Chung on 7/12/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "ChallengeContactListVC.h"
#import "UserPreferences.h"
#import "PlayerSport.h"
#import "IOSRequest.h"
#import "S3Tools.h"
#import "AFNetworking.h"
#import "Contact.h"
#import "ChallengeContactListCell.h"
#import "UIImage+ProportionalFill.h"
#import "PlayerDetailTVC.h"
#import "UIView+Manipulate.h"
#import <QuartzCore/QuartzCore.h>
//#import "AddIndividualMatchIntroVC.h"
#import "AddDirectIndividualMatchTVC.h"
#import "UIColor+VaidenColors.h"



@interface ChallengeContactListVC ()
@property (weak, nonatomic) IBOutlet UITableView *playersTableView;
//@property (weak, nonatomic) IBOutlet UIView *fixedMenu;
@property (nonatomic, strong) UIView *fixedMenu;
//@property (weak, nonatomic) IBOutlet UILabel *sportLabel;
@property (nonatomic, strong) UILabel *sportLabel;
@property (nonatomic) NSInteger filterSportID;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (nonatomic, strong) UserPreferences *preferences;
@property (nonatomic, strong) NSMutableArray *followingList;
@property (nonatomic, strong) NSMutableArray *searchList;
@property (nonatomic) BOOL searchFlag; // YES when we are performing a search
@property (nonatomic) NSInteger selectedRow;
@property (nonatomic) BOOL noResultsToDisplay;
@property (nonatomic, strong) UIView *tempView;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
//@property (weak, nonatomic) IBOutlet UIButton *sportButtonInHiddenFixedMenu;
@property (nonatomic, strong) UIView *overlay;
@property (nonatomic, strong)  UIButton *sportButtonInHiddenFixedMenu;
@property (nonatomic, strong) UIImageView *imageView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableHeight;
@end

@implementation ChallengeContactListVC


- (NSMutableArray *)searchList
{
    if (!_searchList) _searchList = [[NSMutableArray alloc] init];
    return _searchList;
}

- (NSMutableArray *)followingList
{
    if (!_followingList) _followingList = [[NSMutableArray alloc] init];
    return _followingList;
}

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self showTempSpinner];
    
    [self.preferences setChallengePlayersListRefreshState:NO];
    self.noResultsToDisplay = YES;
    self.filterSportID = 0;
    
   // [self.sportButtonInHiddenFixedMenu makeCircleWithColorAndBackground:[UIColor almostBlack] andRadius:5];

    self.fixedMenu = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 40)]; // was 35 height
    self.fixedMenu.backgroundColor = [UIColor colorWithRed:34/255.0 green:34/255.0 blue:34/255.0 alpha:1.0];
    
    self.fixedMenu.layer.borderColor = [UIColor colorWithRed:34/255.0 green:34/255.0 blue:34/255.0 alpha:1.0].CGColor;
    self.fixedMenu.layer.borderWidth = .5f;
    
    [self.view addSubview:self.fixedMenu];
    
    self.sportLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, 180, 40)]; // was 35 height
    self.sportLabel.textColor = [UIColor whiteColor];
    self.sportLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:15];
    
    [self.fixedMenu addSubview:self.sportLabel];

    self.sportButtonInHiddenFixedMenu = [[UIButton alloc] initWithFrame:CGRectMake(255, 7, 60, 25)]; // was 20 height & 8 y location
    [self.sportButtonInHiddenFixedMenu setTitle:@"Filter" forState:UIControlStateNormal];
    [self.sportButtonInHiddenFixedMenu setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    self.sportButtonInHiddenFixedMenu.titleLabel.textColor = [UIColor darkGrayColor];
    [self.sportButtonInHiddenFixedMenu.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:13]];
    //self.sportButtonInHiddenFixedMenu.layer.borderColor = [UIColor darkGrayColor].CGColor;
    //self.sportButtonInHiddenFixedMenu.layer.borderWidth = 0.5;
    [self.sportButtonInHiddenFixedMenu addTarget:self action:@selector(changeSport:) forControlEvents:UIControlEventTouchUpInside];

    [self.fixedMenu addSubview:self.sportButtonInHiddenFixedMenu];
    
    
    self.playersTableView.delegate = self;
    self.playersTableView.dataSource = self;
    
    self.playersTableView.separatorInset = UIEdgeInsetsZero;

    
    [self setFollowingInfo2];

    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
    
    [self.playersTableView setContentInset:UIEdgeInsetsMake(35,0,140,0)];
    
//    [self.playersTableView.superview addSubview:self.fixedMenu];
//    [self showOverlay];

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.overlay removeFromSuperview];
    self.overlay = nil;
    
    if ([self.preferences getChallengePlayersListRefreshState] == YES) {
        [self setFollowingInfo2];
        [self.preferences setChallengePlayersListRefreshState:NO];
    }
}

- (void)showTempSpinner
{
    self.tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    self.tempView.backgroundColor = [UIColor lightGrayColor];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] init];
    [self.activityIndicator setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width/2.0, [UIScreen mainScreen].bounds.size.height/2.0 )];
    [self.activityIndicator startAnimating];
    [self.tempView addSubview:self.activityIndicator];
    
    [self.view addSubview:self.tempView];
    
}

- (void)displayErrorOverlay
{
    self.overlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, [UIScreen mainScreen].bounds.size.height)];
    self.overlay.backgroundColor = [UIColor lightLightGray];
    
    UIImage *img = [UIImage imageNamed:@"no_contact"];
    UIImageView *icon = [[UIImageView alloc] initWithImage:img];
    [icon setFrame:CGRectMake(140, 100, 50, 50)];
    [icon setContentMode:UIViewContentModeCenter];
    
    UILabel *firstLine = [[UILabel alloc] initWithFrame:CGRectMake(30, 180, 260, 21)];
    firstLine.textColor = [UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:1.0];;
    firstLine.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:15];
    firstLine.text = @"No Contacts Yet";
    [firstLine setTextAlignment:NSTextAlignmentCenter];
    
    UILabel *secondLine = [[UILabel alloc] initWithFrame:CGRectMake(25, 200, 280, 42)];
    secondLine.textColor = [UIColor colorWithRed:170/255.0 green:170/255.0 blue:170/255.0 alpha:1.0];
    secondLine.font = [UIFont fontWithName:@"HelveticaNeue" size:13];
    secondLine.text = @"You are not following anyone yet.  To add contacts press the '+' button in the toolbar.";
    secondLine.numberOfLines = 0;
    [secondLine setTextAlignment:NSTextAlignmentCenter];
    
    [self.overlay addSubview:icon];
    [self.overlay addSubview:firstLine];
    [self.overlay addSubview:secondLine];
    
    [self.playersTableView addSubview:self.overlay];
}

- (void)showOverlay
{
    self.imageView = [[UIImageView alloc]
                              initWithImage:[UIImage imageNamed:@"Challenge Intro"]];
    
    
    UITapGestureRecognizer * recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];

    [self.imageView addGestureRecognizer:recognizer];
    self.imageView.userInteractionEnabled =  YES;
  
    [[[UIApplication sharedApplication] keyWindow] addSubview:self.imageView];

}

- (void) handleTap:(UITapGestureRecognizer *)recognize
{
  //  [self.imageView removeFromSuperView];
    self.imageView.hidden = YES;
}
- (void)setFollowingInfo
{
    
    [self.followingList removeAllObjects];
    [IOSRequest fetchUserFollowingList:[self.preferences getUserID]
                         sessionUserID:[self.preferences getUserID]
                          onCompletion:^(NSMutableArray *queryList) {
        
        for(id user in queryList) {
            
            if ([user isKindOfClass:[NSDictionary class]])
            {
                
                Contact *vContact = [[Contact alloc] initWithContactDetails:[user[@"user_id"] integerValue]
                                                              andProfileStr:user[@"profile_pic_string"]
                                                                andUserName:user[@"username"]
                                                                andFullName:user[@"fullname"]
                                                                andLocation:user[@"location"]
                                                                   andAbout:user[@"about"]
                                                                   andCoins:[user[@"coins"] floatValue]
                                                                contactType:CONTACT_TYPE_FOLLOWING
                                                            followingStatus:IS_FOLLOWING_STATUS];
                if (user[@"sports"])
                {
                    
                    for (id object in user[@"sports"]) {
                        PlayerSport *sport = [[PlayerSport alloc] initWithSportDetails:object[@"sport_name"]
                                                                               andType:object[@"sport_type"]
                                                                              andLevel:object[@"level"]
                                                                        andSportRecord:object[@"win_loss_record"]];
                        
                        
                        if ([object[@"sport_type"] isEqualToString:@"individual"]) {
                            [vContact addPlayerSport:sport];
                        } else if ([object[@"sport_type"] isEqualToString:@"team"]) {
                            [vContact addPlayerSport:sport];
                        }
                        
                    }
                }
                [self.followingList addObject:vContact];
                
            }
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.playersTableView reloadData];
            /*
             if ([self.followingList count] > 0) {
             
             [self.playersTableView reloadData];
             } else if ([queryList count] == 0) {
             //              self.noResultsLabel.hidden = NO;
             }
             //          [MBProgressHUD hideHUDForView:self.view animated:YES];
             */
            
        });
    }];
    
}

- (void)setFollowingInfo2
{
    
    [IOSRequest fetchUserFollowingList:[self.preferences getUserID]
                               bySport:self.filterSportID
                          onCompletion:^(NSMutableArray *queryList) {
                              [self.followingList removeAllObjects];
                              for(id user in queryList) {
                                  
                                  if ([user isKindOfClass:[NSDictionary class]])
                                  {
                                      
                                      Contact *vContact = [[Contact alloc] initWithContactDetails:[user[@"user_id"] integerValue]
                                                                                    andProfileStr:user[@"profile_pic_string"]
                                                                                      andUserName:user[@"username"]
                                                                                      andFullName:user[@"fullname"]
                                                                                      andLocation:user[@"location"]
                                                                                         andAbout:user[@"about"]
                                                                                         andCoins:[user[@"coins"] floatValue]
                                                                                      contactType:CONTACT_TYPE_FOLLOWING
                                                                                  followingStatus:IS_FOLLOWING_STATUS];
                                      [vContact setPercentProbabilitySessionUserCanBeatPlayer:[user[@"probability_session_user_wins"] floatValue]];
                                     
                                      NSDictionary *sportObj = user[@"sport"];
                                      
                                              PlayerSport *sport = [[PlayerSport alloc] initWithSportDetails:sportObj[@"sport_name"]
                                                                                                     andType:sportObj[@"type"]
                                                                                                    andLevel:sportObj[@"level"]
                                                                                              andSportRecord:sportObj[@"win_loss_record"]];
                                              
                                      [vContact addPlayerSport:sport];
                                      
                                      [self.followingList addObject:vContact];
                                      
                                                                       }
                              }
                              
                              
                 //             NSLog(@"Friend Count %lu", [queryList count]);
                              dispatch_async(dispatch_get_main_queue(), ^{
                                  
                                
                                  [self.activityIndicator stopAnimating];
                                  self.tempView.hidden = YES;
                                  if ([queryList count] == 0) {
                                      [self displayErrorOverlay];
                                      self.noResultsToDisplay = YES;
                                      self.sportLabel.text = @"";
                                  } else {
                                      self.noResultsToDisplay = NO;
                                      Contact *tempContact = [self.followingList objectAtIndex:0];
                                      
                                      if ([tempContact.playerSports count] > 0) {
                                          PlayerSport *tempSport = [tempContact.playerSports objectAtIndex:0];
                                          self.sportLabel.text = [tempSport.sportName capitalizedString];
                                      }
                                  }
                                  [self.playersTableView reloadData];
                                  
                              });
                              
                              if ([queryList count] == 0) {
                                  [self.activityIndicator stopAnimating];
                                  self.tempView.hidden = YES;
                              }
                              
                          }];

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.noResultsToDisplay)
        return 1;
    
    return [self.followingList count];
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Player Cell";
    ChallengeContactListCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
 //   if (cell == nil) {
 //       cell = [[ChallengeContactListCell alloc] initWithStyle:UITableViewCellStyleDefault
 //                                              reuseIdentifier:CellIdentifier];
 //   }
    
    if (self.noResultsToDisplay) {
        /*cell.usernameLabel.hidden = YES;
        cell.locationLabel.hidden = YES;
        cell.coinsLabel.hidden = YES;
        cell.recordLabel.hidden = YES;
        cell.profileThumb.hidden = YES;
        cell.challengeButton.hidden = YES;
        cell.textLabel.text = @"You are not following anyone yet";
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.textLabel.textColor = [UIColor lightGrayColor];
        cell.textLabel.hidden = NO;*/
        [self displayErrorOverlay];
        return cell;
    } else {
        [self.overlay removeFromSuperview];
        cell.usernameLabel.hidden = NO;
        cell.locationLabel.hidden = NO;
        cell.coinsLabel.hidden = NO;
        cell.recordLabel.hidden = NO;
        cell.profileThumb.hidden = NO;
        cell.challengeButton.hidden = NO;
        cell.textLabel.hidden = YES;
    }

    
    Contact *vContact = (self.followingList)[indexPath.row];
    
    PlayerSport *sport = [vContact.playerSports objectAtIndex:0]; // only one sport should be in array
    
    if ([vContact computeTotalNumMatchesPlayedForSport:sport.sportID] > 0)
        cell.levelLabel.text = [NSString stringWithFormat:@"%ld",  [sport.level integerValue]];
    else
        cell.levelLabel.text = @"0";
    
    cell.sportLabel.text = [NSString stringWithFormat:@"%@ Level", [sport.sportName capitalizedString]];
//    if (!vContact.followingFlag)
//        cell.followButton.hidden = NO;
//    else
//        cell.followButton.hidden = YES;
    
    
    cell.usernameLabel.text = vContact.userName;
    cell.locationLabel.text = vContact.location;
    cell.coinsLabel.text = [NSString stringWithFormat:@"%.0f", vContact.coins];
    cell.recordLabel.text = [NSString stringWithFormat:@"%lu Wins - %lu Losses", sport.playerRecordForSport.wins, sport.playerRecordForSport.losses];
    [cell.challengeButton makeRoundedBorderWithRadius:3];
    cell.challengeButton.tag = indexPath.row;
    
    NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                            baseString:vContact.profileBaseString
                     ];
    
    [cell.profileThumb setImageWithURL:[NSURL URLWithString:url]
                      placeholderImage:[UIImage imageNamed:@"avatar square"]];
    
    [UIImage makeRoundedImageNoBorder:cell.profileThumb withRadius:10];
    cell.pigIcon.image = [UIImage imageNamed:@"Coins"];
    
    float val = vContact.percentProbabilitySessionUserCanBeatPlayer * 100;
    cell.probabilityLabel.text = [NSString stringWithFormat:@"%.0f%@ chance you win", val, @"%"];
    
    
    return cell;
}

/*
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    //    self.playersTableView.hidden = NO;
    self.searchFlag = YES;
    
    [self.searchList removeAllObjects];
    [self.searchBar resignFirstResponder];
    //    [self.playersTableView setUserInteractionEnabled:YES];
    
 //   HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
 //   HUD.labelText = @"Loading";
    
    [IOSRequest searchForUser:searchBar.text
            notAssociatedWith:[self.preferences getUserID] onCompletion:^(NSMutableArray *resultArray) {
                
                for (id object in resultArray) {
                    
                    
                    Contact *contact = [[Contact alloc] initWithContactDetails:[object[@"user_id"] integerValue]
                                                                 andProfileStr:object[@"profile_pic_string"]
                                                                   andUserName:object[@"username"]
                                                                   andFullName:object[@"fullname"]
                                                                   andLocation:object[@"location"]
                                                                      andAbout:object[@"about"]
                                                                      andCoins:[object[@"coins"] floatValue]
                                                                   contactType:CONTACT_TYPE_SEARCH
                                                                 followingFlag:[object[@"follow_flag"] boolValue]];
                    
                    
                    [self.searchList addObject:contact];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if ([self.searchList count] == [resultArray count]) {
                            //                       self.noResultsLabel.hidden = YES;
                            [self.playersTableView reloadData];
                            [searchBar resignFirstResponder];
              //              [MBProgressHUD hideHUDForView:self.view animated:YES];
                            
                        }
                    });
                    
                    
                    
                }
                
                if ([resultArray count] == 0) {
                    //     [self.newContacts removeAllObjects];
                    //     [self.followingContacts.playerArray removeAllObjects];
                    [self.playersTableView reloadData];
                    //                 self.noResultsLabel.hidden = NO;
              //      [MBProgressHUD hideHUDForView:self.view animated:YES];
                }
                
                
            }];
    
    
}*/

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.noResultsToDisplay) {
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        return screenRect.size.height - 150;
    }
    
    return 84.0;
}
- (IBAction)challengeAction:(UIButton *)sender
{
    self.selectedRow = sender.tag;
    sender.enabled = NO;
    if (![self.preferences isVerifiedAccount]) {
        sender.enabled = YES;
        [self performSegueWithIdentifier:@"Should Verify Segue" sender:self];
    } else {
        Contact *c = [self.followingList objectAtIndex:self.selectedRow];
        
        [IOSRequest checkOKToChallengeUser:c.userID
                             bySessionUser:[self.preferences getUserID
                                            ] onCompletion:^(NSDictionary *results) {
                                 dispatch_async(dispatch_get_main_queue(), ^{
                                     sender.enabled = YES;
                                     if ([results[@"ok"] isEqualToString:@"yes"]) {
                                         [self performSegueWithIdentifier:@"Challenge Segue" sender:self];
                                     } else {
                                         UIAlertView *alert = [[UIAlertView alloc] init];
                                         alert.title = @"Can't Challenge User";
                                         alert.message = @"This user can only be challenged by players he / she follows";
                                         [alert addButtonWithTitle:@"OK"];
                                         [alert show];
                                     }
                                 });
                                 
                             }];

    }
}

- (IBAction)detailSegue:(id)sender
{
    CGPoint hitPoint = [sender convertPoint:CGPointZero toView:self.playersTableView];
    NSIndexPath *indexPath = [self.playersTableView indexPathForRowAtPoint:hitPoint];
    self.selectedRow = indexPath.row;
    
    [self performSegueWithIdentifier:@"Player Detail Segue" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    if ([segue.identifier isEqualToString:@"Player Detail Segue"]) {
        Player *player = [self.followingList objectAtIndex:self.selectedRow];
        PlayerDetailTVC *controller = segue.destinationViewController;
        controller.playerUserID = player.userID;
        controller.profilePicString = player.profileBaseString;

    } else if ([segue.identifier isEqualToString:@"Filter By Sport Segue"]) {
        ChooseIndividualSportTVC *controller = segue.destinationViewController;
        controller.delegate = self;
    } else if ([segue.identifier isEqualToString:@"Challenge Segue"]) {
        Player *player = [self.followingList objectAtIndex:self.selectedRow];
     //   AddIndividualMatchIntroVC *controller = segue.destinationViewController;
     //   controller.player = player;
        AddDirectIndividualMatchTVC *controller = segue.destinationViewController;
        controller.userIDToChallenge = player.userID;
        controller.usernameToChallenge = player.userName;
        controller.challengeUserProfileBaseString = player.profileBaseString;
    }
}

- (IBAction)changeSport:(id)sender
{
    [self performSegueWithIdentifier:@"Filter By Sport Segue" sender:self];
}

- (void)setWithSportFromVC:(ChooseIndividualSportTVC *)controller withSport:(Sport *)sport
{
  //  [self.overlay removeFromSuperview];
  //  self.overlay = nil;
    
//    self.sportLabel.text = sport.sportName;
    self.filterSportID = sport.sportID;
    [self setFollowingInfo2];
}
/*
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    // CGFloat stillViewDesiredOriginY; declared ivar
    CGRect newFrame = self.fixedMenu.frame;
    newFrame.origin.x = 0;
 //   newFrame.origin.y = self.playersTableView.contentOffset.y+(self.view.frame.size.height-35);
     newFrame.origin.y = self.fixedMenu.frame.origin.y - self.playersTableView.contentOffset.y;
    self.fixedMenu.frame = newFrame;
}
*/
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!self.noResultsToDisplay) {
        ChallengeContactListCell *cell = (ChallengeContactListCell *)[tableView cellForRowAtIndexPath:indexPath];
        cell.challengeButton.enabled = NO;
        
        self.selectedRow = indexPath.row;
        [self performSegueWithIdentifier:@"Player Detail Segue" sender:self];
    }
}



@end
