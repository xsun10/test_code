//
//  ChooseIndividualSportTVC.m
//  Phenom
//
//  Created by James Chung on 7/11/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "ChooseIndividualSportTVC.h"
#import "UserPreferences.h"
#import "IOSRequest.h"
#import "ChooseIndividualSportCell.h"
#import "UIColor+VaidenColors.h"

@interface ChooseIndividualSportTVC ()

@property (nonatomic, strong) NSMutableArray *sports;
@property (nonatomic, strong) UserPreferences *preferences;
@end

@implementation ChooseIndividualSportTVC

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (NSMutableArray *)sports
{
    if (!_sports) _sports = [[NSMutableArray alloc] init];
    return _sports;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadSports];
   
}

- (void)loadSports
{
    /*  // will do this one later
    [IOSRequest fetchCrossReferencedSportsListBetweenSessionUser:[self.preferences getUserID]
                                                   andCompetitor:self.competitorUserID
                                                    onCompletion:^(NSMutableArray *results) {
                                                        for (id object in results) {
                                                            Sport *sport = [[Sport alloc] initWithSportDetails:[object[@"sport_id"] integerValue]
                                                                                                       andName:object[@"sport_name"]
                                                                                                       andType:@"individual"];
                                                            [self.sports addObject:sport];
                                                            
                                                        }
                                                        
                                                        dispatch_async(dispatch_get_main_queue(), ^ {
                                                            [self.tableView reloadData];
                                                        });

                                                        
                                                    }];
     */
   
    [IOSRequest fetchIndividualSportsList:[self.preferences getUserID]
                             onCompletion:^(NSMutableArray *results) {
                                 for (id object in results) {
                                     Sport *sport = [[Sport alloc] initWithSportDetails:[object[@"sport_id"] integerValue]
                                                                                andName:object[@"sport_name"]
                                                                                andType:@"individual"];
                                     [self.sports addObject:sport];
                                     
                                 }
                                 
                                 dispatch_async(dispatch_get_main_queue(), ^ {
                                     [self.tableView reloadData];
                                 });
                             }];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.sports count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Sport Cell";
    ChooseIndividualSportCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    Sport *sport = [self.sports objectAtIndex:indexPath.row];
  
    cell.sportIcon.image = [[sport getSportIcon] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [cell.sportIcon setTintColor:[UIColor newBlueDark]];
    cell.sportName.text = [sport.sportName capitalizedString];
    
    // Configure the cell...
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Sport *sport = [self.sports objectAtIndex:indexPath.row];
    
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
    
    [self.delegate setWithSportFromVC:self withSport:sport];
    [self.navigationController popViewControllerAnimated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60.0;
}

@end
