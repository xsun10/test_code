//
//  MatchPlayerNewsOfficialFinishedIndividualMatch.m
//  Vaiden
//
//  Created by James Chung on 11/1/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "MatchPlayerNewsOfficialFinishedIndividualMatch.h"

@implementation MatchPlayerNewsOfficialFinishedIndividualMatch

- (MatchPlayerNewsOfficialFinishedIndividualMatch *)initWithDetails:(Match *)matchInfo
                                                           postDate:(NSDate *)newsDate
                                                    headLineMessage:(NSString *)headlineMessage
                                                     generalMessage:(NSString *)generalMessage
                                                          forPlayer:(Player *)player
                                                        withMessage:(NSString *)playerMessage
                                               andCoinTransferValue:(NSInteger)coinTransferValue
{
    self = [super initWithDetails:matchInfo
                         postDate:newsDate
                  headLineMessage:headlineMessage
                   generalMessage:generalMessage
                        forPlayer:player
                      withMessage:playerMessage];
    
    if (self) {
        _coinTransferValue = coinTransferValue;
    }
    
    return self;
}

@end
