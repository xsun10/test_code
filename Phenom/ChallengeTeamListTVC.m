//
//  ChallengeTeamListTVC.m
//  Vaiden
//
//  Created by James Chung on 4/10/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "ChallengeTeamListTVC.h"
#import "ChallengeTeamListCell.h"
#import "IOSRequest.h"
#import "UserPreferences.h"
#import "UIView+Manipulate.h"
#import "UIColor+VaidenColors.h"
#import "UIView+Manipulate.h"
#import "AddTeamMatchTVC.h"

@interface ChallengeTeamListTVC ()

@property (nonatomic, strong) NSMutableArray *teamsList;
@property (nonatomic, strong) UserPreferences *preferences;
@property (nonatomic, strong) UIView *tempView;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
@property (nonatomic, strong) UIView *overlay;
@property (nonatomic) NSInteger selectedRow;
@end

@implementation ChallengeTeamListTVC

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (NSMutableArray *)teamsList
{
    if (!_teamsList) _teamsList = [[NSMutableArray alloc] init];
    return _teamsList;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];

    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self loadTeams];
}

// this will show teams that your contacts (who you follow) are a part of
- (void)loadTeams
{
    [self.teamsList removeAllObjects];
    [self showTempSpinner];

    [IOSRequest getContactsTeamsForUser:[self.preferences getUserID] onCompletion:^(NSMutableArray *results) {
        
        for (id object in results) {
            if ([object[@"team_id"] integerValue] == self.sessionUserTeamToUse.teamID) continue; // can't challege a team you are on
            Team *t = [[Team alloc] initWithTeamDetails:[object[@"team_id"] integerValue]
                                            andTeamName:object[@"team_name"]
                                             andCaptain:nil
                                           andPicString:object[@"logo"]];
            [t setContactsWhoAreMembersShortDescription:object[@"contacts_description"]];
            [self.teamsList addObject:t];
            
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.activityIndicator stopAnimating];
            self.tempView.hidden = YES;
            
            if ([results count] == 0) {
                [self displayErrorOverlay];
            } else {
                self.overlay.hidden = YES;
            }
            
            
            [self.tableView reloadData];
        });
    }];
}

- (void)showTempSpinner
{
    self.tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    self.tempView.backgroundColor = [UIColor lightGrayColor];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] init];
    [self.activityIndicator setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width/2.0, [UIScreen mainScreen].bounds.size.height/2.0 - 50)];
    [self.activityIndicator startAnimating];
    [self.tempView addSubview:self.activityIndicator];
    
    [self.view addSubview:self.tempView];
    
}


- (void)displayErrorOverlay
{
    if (self.overlay == nil) {
        self.overlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, [UIScreen mainScreen].bounds.size.height)];
        self.overlay.backgroundColor = [UIColor lightLightGray];
        
        UILabel *firstLine = [[UILabel alloc] initWithFrame:CGRectMake(30, ([UIScreen mainScreen].bounds.size.height / 2) - 115, 260, 30)];
        firstLine.textColor = [UIColor darkGrayColor];
        firstLine.font = [UIFont systemFontOfSize:23];
        firstLine.text = @"No Teams To Challenge";
        
        UILabel *secondLine = [[UILabel alloc] initWithFrame:CGRectMake(30, ([UIScreen mainScreen].bounds.size.height / 2) - 90, 260, 100)];
        secondLine.textColor = [UIColor lightGrayColor];
        secondLine.font = [UIFont systemFontOfSize:17];
        secondLine.text = @"You do not have any contacts that are members of teams.  To begin, search for a team to challenge.";
        secondLine.numberOfLines = 0;
        
        UIButton *createButton = [[UIButton alloc] initWithFrame:CGRectMake(27, ([UIScreen mainScreen].bounds.size.height / 2) + 20  , 210, 40)];
        [createButton setTitle:@"Team Search" forState:UIControlStateNormal];
        [createButton setTitleColor:[UIColor peacock] forState:UIControlStateNormal];
        createButton.titleLabel.font = [UIFont systemFontOfSize:16.0];
        [createButton addTarget:self action:@selector(searchForTeam) forControlEvents:UIControlEventTouchUpInside];
        
        
        [createButton makeRoundedBorderWithRadius:3];
        [createButton.layer setBorderWidth:1.0];
        [createButton.layer setBorderColor:[[UIColor peacock] CGColor]];
        
        [self.overlay addSubview:firstLine];
        [self.overlay addSubview:secondLine];
        [self.overlay addSubview:createButton];
        
        [self.tableView addSubview:self.overlay];
    } else {
        self.overlay.hidden = NO;
    }
}

- (void)searchForTeam
{
    [self performSegueWithIdentifier:@"Team Search Segue" sender:self];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.teamsList count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70.0;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString static *CellIdentifier = @"Team Cell";
    ChallengeTeamListCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    Team *t = [self.teamsList objectAtIndex:indexPath.row];
    
    cell.teamLogo.image = [UIImage imageNamed:t.picStringName];
    cell.teamNameLabel.text = t.teamName;
    cell.contactsDescriptionLabel.text = t.contactsWhoAreMembersShortDescription;
    [cell.challengeButton makeRoundedBorderWithRadius:3];
    cell.challengeButton.tag = indexPath.row;
    
    return cell;
}
- (IBAction)challengeTeamAction:(UIButton *)sender
{
    self.selectedRow = sender.tag;
    [self performSegueWithIdentifier:@"Team Challenge Segue" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Team Challenge Segue"]) {
        AddTeamMatchTVC *controller = segue.destinationViewController;
        controller.team1 = self.sessionUserTeamToUse;
        
        Team *t2 = [self.teamsList objectAtIndex:self.selectedRow];
        controller.team2 = t2;
    }
}


@end
