//
//  SportRatingsListTVC.m
//  Vaiden
//
//  Created by James Chung on 10/7/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "SportRatingsListTVC.h"
#import "IOSRequest.h"
#import "S3Tools.h"
#import "SportRatingsListCell.h"
#import "SportRating.h"
#import "UIImage+ProportionalFill.h"
#import "AFNetworking.h"
#import "NSDate+Utilities.h"

@interface SportRatingsListTVC ()

@property (nonatomic, strong) NSMutableArray *sportRatingsList;

@end

@implementation SportRatingsListTVC


- (NSMutableArray *)sportRatingsList
{
    if (!_sportRatingsList) _sportRatingsList = [[NSMutableArray alloc] init];
    return _sportRatingsList;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];

    
    [self loadRatings];

}

- (void)loadRatings
{
    [self.sportRatingsList removeAllObjects];
    
    [IOSRequest getSportRatingsListForUser:self.ratedPlayerID
                         sportRatingTypeID:self.sportRatingsTypeID
                              onCompletion:^(NSMutableArray *results) {
                             
                                  for (id object in results) {
                                      
                                      NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
                                      [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                                      NSDate *ratingDateTime = [[formatter dateFromString:object[@"ts"]] toLocalTime];
                                      
                                      SportRating *sportRating = [[SportRating alloc] initWithSportRating:[object[@"sr_rating"] floatValue]
                                                                                     byRatingPlayerID:[object[@"rating_player_id"] integerValue]
                                                                               byRatingPlayerUsername:object[@"rating_player_username"]
                                                                              byRatingPlayerPicString:object[@"rating_player_pic_string"]
                                                                                     forRatedPlayerID:self.ratedPlayerID
                                                                                          withComment:object[@"comment"]
                                                                                               onDate:ratingDateTime
                                                                                forSportSkillAttribute:nil];
                                      
                                      [self.sportRatingsList addObject:sportRating];
                                      
                                  }
                                  dispatch_async(dispatch_get_main_queue(), ^{
                                      [self.tableView reloadData];
                                  });
                         }];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.sportRatingsList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Rating Cell";
    SportRatingsListCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    SportRating *sRating = [self.sportRatingsList objectAtIndex:indexPath.row];
    cell.usernameLabel.text = sRating.ratingPlayer.userName;
    
    NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                            baseString:sRating.ratingPlayer.profileBaseString];
    
    [cell.profilePic setImageWithURL:[NSURL URLWithString:url]
                    placeholderImage:[UIImage imageNamed:@"avatar round"]];
    [UIImage makeRoundedImage:cell.profilePic withRadius:25];
    
    cell.starIcons.image = [SportRating getRatingImageForRating:sRating.rating];
    cell.commentLabel.text = sRating.comment;
    
    NSDate *currentDate = [NSDate date];
    NSTimeInterval timeDifference = [currentDate timeIntervalSinceDate:sRating.ratingDate];
    int minutes = timeDifference / 60;
    int hours = minutes / 60;
    int days = minutes / 1440;
    NSString *timeDiffString = nil;
    
    if (minutes > 1440)
    timeDiffString = [NSString stringWithFormat:@"%dd", days];
    else if (minutes > 60)
        timeDiffString = [NSString stringWithFormat:@"%dh", hours];
    else
        timeDiffString = [NSString stringWithFormat:@"%dm", minutes];
    
    cell.dateLabel.text = timeDiffString;

    
    
    return cell;
}


#define FONT_SIZE 13.0f
#define CELL_CONTENT_WIDTH 200.0f
#define CELL_CONTENT_MARGIN 23.0f //was 13

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    NSString *text = nil;
    
    

    if ([self.sportRatingsList objectAtIndex:indexPath.row] != nil) {
        SportRating *sRating = [self.sportRatingsList objectAtIndex:indexPath.row];
        text = sRating.comment;
        NSLog (@"text is %@", text);
    } else {
        text = @"";
    }
    
    // Get a CGSize for the width and, effectively, unlimited height
    CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH - (CELL_CONTENT_MARGIN * 2), 20000.0f);
    // Get the size of the text given the CGSize we just made as a constraint
    CGSize size = [text sizeWithFont:[UIFont systemFontOfSize:FONT_SIZE] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    // Get the height of our measurement, with a minimum of 71 (standard cell size)
    CGFloat height = MAX(size.height, 65.0f);
    // return the height, with a bit of extra padding in
    return height + (CELL_CONTENT_MARGIN * 2) - 15;
    
}


@end
