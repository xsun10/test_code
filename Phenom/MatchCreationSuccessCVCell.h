//
//  MatchCreationSuccessCVCell.h
//  Vaiden
//
//  Created by James Chung on 11/15/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MatchCreationSuccessCVCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *iconPic;
@property (weak, nonatomic) IBOutlet UILabel *slideText;
@property (weak, nonatomic) IBOutlet UILabel *slideNum;
@property (weak, nonatomic) IBOutlet UIView *slideNumBackground;
@property (weak, nonatomic) IBOutlet UILabel *headingLabel;

@end
