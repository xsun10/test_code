//
//  AddPickupMatchTVC.h
//  Phenom
//
//  Created by James Chung on 4/3/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VenuesTVC.h"
#import "RequiredMatchExperienceTVC.h"
#import "ChoosePickupSportTVC.h"
#import "ChooseMatchTypeTVC.h"
#import "ChooseGenderTVC.h"
#import "CustomBaseTVC.h"

@interface AddPickupMatchTVC : CustomBaseTVC < VenueTVC_Delegate, RequiredMatchExperienceTVC_Delegate, ChoosePickupSportTVC_Delegate, ChooseMatchTypeTVC_Delegate, ChooseGenderTVC_Delegate>

@end
