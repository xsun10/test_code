//
//  VerifyIdentity2VC.h
//  Vaiden
//
//  Created by James Chung on 9/14/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "CustomBaseVC.h"

@interface VerifyIdentity2VC : CustomBaseVC <MBProgressHUDDelegate>
{
    MBProgressHUD *HUD;
}
@property (nonatomic, strong) NSString *userPhoneNumber;
@end
