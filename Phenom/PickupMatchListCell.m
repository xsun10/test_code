//
//  UpcomingPickupMatchesCell.m
//  Phenom
//
//  Created by James Chung on 5/11/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "PickupMatchListCell.h"
#import "UpcomingPickupMatchCollectionCell.h"
#import "MatchPlayer.h"
#import "S3Tools.h"
#import "IOSRequest.h"
#import "AFNetworking.h"
#import "UIImage+ProportionalFill.h"

@implementation PickupMatchListCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)awakeFromNib
{
    self.playerCV.delegate = self;
    self.playerCV.dataSource = self;
}


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
//    return [self.competitors count];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"status = %@", @"ACCEPTED"];
    NSArray *matchParticipants = [self.competitors filteredArrayUsingPredicate:predicate];
    
    return [matchParticipants count];
}

- (void)goToProfile:(UIButton *)sender
{
    [self.delegate setPickupMatchListCellViewController:self withUserIDToSegue:sender.tag];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Player Image Cell";
    
    UpcomingPickupMatchCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"status = %@", @"ACCEPTED"];
    NSArray *matchParticipants = [self.competitors filteredArrayUsingPredicate:predicate];
    
 //   if (indexPath.item < [matchParticipants count]) {
        MatchPlayer *player = matchParticipants[indexPath.row];
        
        //      NSString *urlString = [IOSRequest getPhotoURL:1 forID:player.userID];
        //      [cell.profilePic setImageWithURL:[NSURL URLWithString:urlString]
        //                      placeholderImage:[UIImage imageNamed:@"avatar.png"]];
        
        NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                baseString:player.profileBaseString];
        
        [cell.profilePic setImageWithURL:[NSURL URLWithString:url]
                        placeholderImage:[UIImage imageNamed:@"avatar square"]];

        
   //     [UIImage makeRoundedImage:cell.profilePic withRadius:20];
    
        // profile pic button
        UIButton *picButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
        [picButton addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
        picButton.tag = player.userID;
        [cell.profilePic addSubview:picButton];
        cell.profilePic.userInteractionEnabled = YES;
        cell.usernameLabel.text = player.userName;
    
 //   } else {
 //       [cell.profilePic setImage:[UIImage imageNamed:@"avatar round slot"]];
  //  }
    return cell;
}
@end
