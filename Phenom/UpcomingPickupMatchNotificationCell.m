//
//  UpcomingMatchNotificationCell.m
//  Phenom
//
//  Created by James Chung on 6/17/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "UpcomingPickupMatchNotificationCell.h"
#import "Player.h"
#import "UpcomingMatchNotificationCVCell.h"
#import "S3Tools.h"
#import "AFNetworking.h"
#import "IOSRequest.h"
#import "UIImage+ProportionalFill.h"



@implementation UpcomingPickupMatchNotificationCell

- (NSMutableArray *)competitors
{
    if (!_competitors) _competitors = [[NSMutableArray alloc] init];
    return _competitors;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setPlayerValues
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"status = %@", @"ACCEPTED"];
    NSArray *matchParticipants = [self.competitors filteredArrayUsingPredicate:predicate];
    
    self.confirmedParticipants = [NSMutableArray arrayWithArray:matchParticipants];
}
- (void)layoutSubviews
{
    self.playerCollectionView.delegate = self;
    self.playerCollectionView.dataSource = self;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    if ([self.matchCategory isEqualToString:@"individual"]) {
        return 3;
    }
   
    return [self.confirmedParticipants count];
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Player Image Cell";
    
    UpcomingMatchNotificationCVCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    
    Player *p = nil;
    
    if ([self.matchCategory isEqualToString:@"individual"]) {
        if (indexPath.row == 1) {
            // show "vs" not the pic
            cell.profilePic.hidden = YES;
            UILabel *vsLabel = [[UILabel alloc] initWithFrame:CGRectMake(4, 0, 34, 34)];
            vsLabel.text = @"VS";
         //   [vsLabel setFont:[UIFont systemFontOfSize:18]];
            
         //   vsLabel.textAlignment = NSTextAlignmentCenter;
            [vsLabel setFont:[UIFont fontWithName:@"HelveticaNeue-BoldItalic" size:20]];
            
            [cell addSubview:vsLabel];
            
            
        } else {
            
            
            if (indexPath.row == 2)
                p  = [self.competitors objectAtIndex:1];
            else
                p = [self.competitors objectAtIndex:0];
            
            NSString *urlMain = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                        baseString:p.profileBaseString];
            
            [cell.profilePic setImageWithURL:[NSURL URLWithString:urlMain]
                           placeholderImage:[UIImage imageNamed:@"avatar square"]];
        //    [UIImage makeRoundedImage:cell.profilePic withRadius:17];
            
            // profile pic button
            UIButton *picButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
            [picButton addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
            picButton.tag = p.userID;
            [cell.profilePic addSubview:picButton];
            cell.profilePic.userInteractionEnabled = YES;
            // end profile pic button
            
        }
        
        return  cell;
    } else {
       
        
        //   if (indexPath.item < [matchParticipants count]) {
        p = self.confirmedParticipants[indexPath.row];
        NSString *urlMain = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                    baseString:p.profileBaseString];
        
        [cell.profilePic setImageWithURL:[NSURL URLWithString:urlMain]
                       placeholderImage:[UIImage imageNamed:@"avatar round"]];
     //   [UIImage makeRoundedImage:cell.profilePic withRadius:17];
        
        
        return  cell;
    }
    
    return nil;
}

@end
