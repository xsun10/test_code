//
//  SpecialUILabel.h
//  Vaiden
//
//  Created by James Chung on 1/30/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SpecialUILabel : UILabel

@property (nonatomic) BOOL shouldRemoveFromMySuperview;

- (SpecialUILabel *)init;

@end
