//
//  ChallangeMatchHowItWorksVC.m
//  Vaiden
//
//  Created by Turbo on 6/5/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "ChallangeMatchHowItWorksVC.h"
#import "stepCVCell.h"

@interface ChallangeMatchHowItWorksVC () <UICollectionViewDelegate, UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (weak, nonatomic) IBOutlet UIScrollView *pageScrollView;
@end

@implementation ChallangeMatchHowItWorksVC

@synthesize segueName;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
    //[self.collectionView setPagingEnabled:YES];
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [flowLayout setMinimumInteritemSpacing:0.0f];
    [flowLayout setMinimumLineSpacing:0.0f];
    [flowLayout setItemSize:CGSizeMake(320, 382)];
    //[flowLayout setSectionInset:UIEdgeInsetsMake(10, 0, 0, 0)];
    [self.collectionView setPagingEnabled:YES];
    [self.collectionView setCollectionViewLayout:flowLayout];
    
    //self.collectionView.
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    self.pageScrollView.contentSize = CGSizeMake(320.0, 650.0);
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    // Six steps in total
    if ([segueName isEqualToString:@"challange"]) {
        [self.pageControl setNumberOfPages:6];
        return 6;
    } else if ([segueName isEqualToString:@"challange_success"]) {
        [self.pageControl setNumberOfPages:4];
        return 4;
    } else if ([segueName isEqualToString:@"pickup_success"]) {
        [self.pageControl setNumberOfPages:2];
        return 2;
    } else {
        [self.pageControl setNumberOfPages:4];
        return 4;
    }
}



- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"steps_cell";
    stepCVCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    if ([self.segueName isEqualToString:@"challange"]) {
        
        if (indexPath.row == 0) {
            [cell.stepsImage setImage:[UIImage imageNamed:@"step1"]];
            [cell.stepsImage setContentMode:UIViewContentModeCenter];
            
            cell.headlineLabel.text = @"Step 1:  Challenge An Opponent";
            
            cell.contentLabel.text = @"Choose a friend to challenge or find players nearby to compete with.";
        } else if (indexPath.row == 1) {
            [cell.stepsImage setImage:[UIImage imageNamed:@"step2"]];
            [cell.stepsImage setContentMode:UIViewContentModeCenter];
            
            cell.headlineLabel.text = @"Step 2:  Schedule Your Match";
            
            cell.contentLabel.text = @"Pick a date, time, and venue for your match. You should try and coordinate these details with your opponent when possible.";
        } else if (indexPath.row == 2) {
            [cell.stepsImage setImage:[UIImage imageNamed:@"step3"]];
            [cell.stepsImage setContentMode:UIViewContentModeCenter];
            
            cell.headlineLabel.text = @"Step 3:  Opponent Approval";
            
            cell.contentLabel.text = @"Wait for your opponent to accept your challenge request.  He / she will also have the option to decline your challenge.";
        } else if (indexPath.row == 3) {
            [cell.stepsImage setImage:[UIImage imageNamed:@"step4"]];
            [cell.stepsImage setContentMode:UIViewContentModeCenter];
            
            cell.headlineLabel.text = @"Step 4:  Play Your Match";
            
            cell.contentLabel.text = @"Meet at the specified venue at the scheduled date and time. Play your match and see who wins.";
        } else if (indexPath.row == 4) {
            [cell.stepsImage setImage:[UIImage imageNamed:@"step5"]];
            [cell.stepsImage setContentMode:UIViewContentModeCenter];
            
            cell.headlineLabel.text = @"Step 5:  Record/Review Score";
            
            cell.contentLabel.text = @"Once your match is completed, you’ll be asked to record the score. The other user must confirm the score of the match for it to be made official.";
        } else if (indexPath.row == 5) {
            [cell.stepsImage setImage:[UIImage imageNamed:@"step6"]];
            [cell.stepsImage setContentMode:UIViewContentModeCenter];
            
            cell.headlineLabel.text = @"Step 6:  Updated Sports Level";
            
            cell.contentLabel.text = @"Once the match is official, you and your opponent will receive updated sports levels based on the match results.";
        }
    } else if ([segueName isEqualToString:@"challange_success"]) {
        if (indexPath.row == 0) {
            [cell.stepsImage setImage:[UIImage imageNamed:@"step3"]];
            [cell.stepsImage setContentMode:UIViewContentModeCenter];
            
            cell.headlineLabel.text = @"Step 1:  Opponent Approval";
            
            cell.contentLabel.text = @"Wait for your opponent to accept your challenge request. He / she will also have the option to decline your challenge.";
        } else if (indexPath.row == 1) {
            [cell.stepsImage setImage:[UIImage imageNamed:@"step4"]];
            [cell.stepsImage setContentMode:UIViewContentModeCenter];
            
            cell.headlineLabel.text = @"Step 2:  Play Your Match";
            
            cell.contentLabel.text = @"Meet at the specified venue on the agreed time and date. Play your match and see who wins.";
        } else if (indexPath.row == 2) {
            [cell.stepsImage setImage:[UIImage imageNamed:@"step5"]];
            [cell.stepsImage setContentMode:UIViewContentModeCenter];
            
            cell.headlineLabel.text = @"Step 3:  Record/Review Score";
            
            cell.contentLabel.text = @"Once your match is completed, you’ll be asked to record the score. The other user must confirm the score of the match for it to be made official.";
        } else if (indexPath.row == 3) {
            [cell.stepsImage setImage:[UIImage imageNamed:@"step6"]];
            [cell.stepsImage setContentMode:UIViewContentModeCenter];
            
            cell.headlineLabel.text = @"Step 4:  Updated Sports Level";
            
            cell.contentLabel.text = @"Once the match is official, you and your opponent will receive updated sports levels based on the match results.";
        }
    } else if ([segueName isEqualToString:@"pickup_success"]) {
        if (indexPath.row == 0) {
            [cell.stepsImage setImage:[UIImage imageNamed:@"pickup_step3"]];
            [cell.stepsImage setContentMode:UIViewContentModeCenter];
            
            cell.headlineLabel.text = @"Step 1: Spread the Word";
            
            cell.contentLabel.text = @"Your match will appear in your newsfeed, and it will also be visible to players in your area (unless it is a private match).";
        } else if (indexPath.row == 1) {
            [cell.stepsImage setImage:[UIImage imageNamed:@"pickup_step4"]];
            [cell.stepsImage setContentMode:UIViewContentModeCenter];
            
            cell.headlineLabel.text = @"Step 2:  Meet Up and Play";
            
            cell.contentLabel.text = @"Play your match as scheduled. Scores are not recorded in pickup matches. They are just for fun.";
        }
    } else {
        if (indexPath.row == 0) {
            [cell.stepsImage setImage:[UIImage imageNamed:@"pickup_step1"]];
            [cell.stepsImage setContentMode:UIViewContentModeCenter];
            
            cell.headlineLabel.text = @"Step 1:  Schedule Match";
            
            cell.contentLabel.text = @"Pick the sport and set the date, time, and venue for your pickup match.";
        } else if (indexPath.row == 1) {
            [cell.stepsImage setImage:[UIImage imageNamed:@"pickup_step2"]];
            [cell.stepsImage setContentMode:UIViewContentModeCenter];
            
            cell.headlineLabel.text = @"Step 2: Invite Contacts";
            
            cell.contentLabel.text = @"Invite users from your contact list to attend. If you post a public match, other players in your area will be able to join.";
        } else if (indexPath.row == 2) {
            [cell.stepsImage setImage:[UIImage imageNamed:@"pickup_step3"]];
            [cell.stepsImage setContentMode:UIViewContentModeCenter];
            
            cell.headlineLabel.text = @"Step 3: Spread the Word";
            
            cell.contentLabel.text = @"Your match will appear in your newsfeed, and it will also be visible to players in your area (unless it is a private match).";
        } else if (indexPath.row == 3) {
            [cell.stepsImage setImage:[UIImage imageNamed:@"pickup_step4"]];
            [cell.stepsImage setContentMode:UIViewContentModeCenter];
            
            cell.headlineLabel.text = @"Step 4:  Meet Up and Play";
            
            cell.contentLabel.text = @"Play your match as scheduled. Scores are not recorded in pickup matches. They are just for fun.";
        }
    }

    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView
  didEndDisplayingCell:(UICollectionViewCell *)cell
    forItemAtIndexPath:(NSIndexPath *)indexPath
{
    stepCVCell* currentCell = ([[collectionView visibleCells]count] > 0) ? [collectionView visibleCells][0] : nil;
    
    if(cell != nil){
        NSInteger rowIndex = [collectionView indexPathForCell:currentCell].row;
        [self.pageControl setCurrentPage:rowIndex];
        
    }
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
