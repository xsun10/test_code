//
//  ChooseOneSportTVC.m
//  Vaiden
//
//  Created by James Chung on 4/23/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "ChooseOneSportTVC.h"
#import "UserPreferences.h"
#import "IOSRequest.h"
#import "UIColor+VaidenColors.h"
#import "ChooseOneSportTVCell.h"

@interface ChooseOneSportTVC ()

@property (nonatomic, strong) NSMutableArray *sportsArray;
@property (nonatomic, strong) UserPreferences *preferences;
@property (nonatomic, strong) UIView *tempView;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;

@end

@implementation ChooseOneSportTVC

- (NSMutableArray *)sportsArray
{
    if (!_sportsArray) _sportsArray = [[NSMutableArray alloc] init];
    return _sportsArray;
}

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self showTempView];
    [self setSports];
}

- (void)showTempView
{
    self.tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    self.tempView.backgroundColor = [UIColor midGray];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] init];
    [self.activityIndicator setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width/2.0, [UIScreen mainScreen].bounds.size.height/2.0 - 50)];
    [self.activityIndicator startAnimating];
    [self.tempView addSubview:self.activityIndicator];
    
    [self.view addSubview:self.tempView];

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Sport *s = [self.sportsArray objectAtIndex:indexPath.row];
    
    [self.delegate setViewController:self withSport:s];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setSports
{
    [self.sportsArray removeAllObjects];
    
    [IOSRequest fetchAllSports:[self.preferences getUserID] onCompletion:^(NSMutableArray *results) {
        for (id object in results) {
            if ([object[@"sport_name"] isEqualToString:@"fitness"])
                continue;
            
            Sport *sport = [[Sport alloc] initWithSportDetails:[object[@"sport_id"] integerValue]
                                                       andName:object[@"sport_name"]
                                                       andType:@"individual"]; // dummy data....
            
            [self.sportsArray addObject:sport];
            
           
        }
        
        dispatch_async(dispatch_get_main_queue(), ^ {
            if ([self.sportsArray count] > 0) {
                [self.activityIndicator stopAnimating];
                self.tempView.hidden = YES;
            }
            
            [self.tableView reloadData];
        });

    }];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.sportsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ChooseOneSportTVCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Sport Cell" forIndexPath:indexPath];
    
    Sport *sp = [self.sportsArray objectAtIndex:indexPath.row];
    
    cell.sportNameLabel.text = [sp.sportName capitalizedString];
    cell.sportPic.image = [[sp getSportIcon] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [cell.sportPic setTintColor:[UIColor newBlueLight]];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}
@end
