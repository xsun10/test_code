//
//  Contact.m
//  Phenom
//
//  Created by James Chung on 6/9/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "Contact.h"

@implementation Contact

- (Contact *) initWithContactDetails:(NSInteger)userID
                     andProfileStr:(NSString *)profileBaseString
                       andUserName:(NSString *)userName
                       andFullName:(NSString *)fullName
                       andLocation:(NSString *)location
                          andAbout:(NSString *)about
                            andCoins:(float)coins
                        contactType :(NSInteger)contactType
                     followingStatus:(NSInteger)followingStatus
{
    self = [super initWithPlayerDetails:userID
                          andProfileStr:profileBaseString
                            andUserName:userName
                            andFullName:fullName
                            andLocation:location
                               andAbout:about
                               andCoins:coins];

    
    if (self) {
        _contactType = contactType;
        _followingStatus = followingStatus;
        _approvalRequiredToFollow = NO;
    }
    
    return self;
}

@end
