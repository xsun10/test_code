//
//  LoginTableVC.h
//  Phenom
//
//  Created by James Chung on 3/18/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomBaseTVC.h"
#import "MBProgressHUD.h"

@interface LoginTableVC : CustomBaseTVC <MBProgressHUDDelegate>
{
    MBProgressHUD *HUD;
}

- (void)loginFailed;
@end
