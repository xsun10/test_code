//
//  PhenomAppDelegate.m
//  Phenom
//
//  Created by James Chung on 3/14/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "PhenomAppDelegate.h"
#import <FacebookSDK/FacebookSDK.h>
#import "LoginTableVC.h"
#import "LandingSlideVC.h"
#import "NewsFeedTVC.h"
#import "IOSRequest.h"
#import "UserPreferences.h"
//#import "VaidenTabBarController.h" // my test
//#import "MatchNotificationsVC.h"
#import "UIColor+VaidenColors.h"

// header file for in app purchase
#import "VotePuchaseTier1Helper.h"

@interface PhenomAppDelegate ()

@property (strong, nonatomic) UIViewController *navController;
@property (strong, nonatomic) UINavigationController *navController2;
@property (nonatomic, strong) UserPreferences *preferences;
@property (nonatomic, strong) UIStoryboard *sb;


@end

@implementation PhenomAppDelegate


@synthesize window;
@synthesize navigationController;

@synthesize navController = _navController;

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return  _preferences;
}

- (void)navigationController:(UINavigationController *)navController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    
    if ([viewController respondsToSelector:@selector(willAppearIn:)])
        [viewController performSelector:@selector(willAppearIn:) withObject:navController];
    
}


- (void)addMessageFromRemoteNotification:(NSDictionary*)userInfo updateUI:(BOOL)updateUI withApplication:(UIApplication *)application
{
   
	NSString *alertValue = [[userInfo valueForKey:@"aps"] valueForKey:@"alert"];
    
    
    if (application.applicationState == UIApplicationStateActive ) {
        
       
        
        dispatch_async(dispatch_get_main_queue(), ^ {
            [self.preferences setChallengePlayersListRefreshState:YES];
            [self.preferences setNewsfeedRefreshState:YES];
            [self.preferences setMatchNotificationRefreshState:YES];
            
            UIAlertView *alert = nil;
            if ([[userInfo valueForKey:@"act"] isEqualToString:@"IR"]) {
                [self.preferences setMatchNotificationsForceSegueWithAction:[userInfo valueForKey:@"act"] andMatchID:[[userInfo valueForKey:@"id"] integerValue]];
                alert = [[UIAlertView alloc] initWithTitle:@"Match Update" message:alertValue delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Record Score", nil];
            } else if ([[userInfo valueForKey:@"act"] isEqualToString:@"IC"]) {
                [self.preferences setMatchNotificationsForceSegueWithAction:[userInfo valueForKey:@"act"] andMatchID:[[userInfo valueForKey:@"id"] integerValue]];
                alert = [[UIAlertView alloc] initWithTitle:@"Match Update" message:alertValue delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Confirm Score", nil];
            } else if ([[userInfo valueForKey:@"act"] isEqualToString:@"ID"]) {
                [self.preferences setMatchNotificationsForceSegueWithAction:[userInfo valueForKey:@"act"] andMatchID:[[userInfo valueForKey:@"id"] integerValue]];
                alert = [[UIAlertView alloc] initWithTitle:@"Match Update" message:alertValue delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Challenge Details", nil];
            } else if ([[userInfo valueForKey:@"act"] isEqualToString:@"PD"]) {
                [self.preferences setMatchNotificationsForceSegueWithAction:[userInfo valueForKey:@"act"] andMatchID:[[userInfo valueForKey:@"id"] integerValue]];
                alert = [[UIAlertView alloc] initWithTitle:@"Match Update" message:alertValue delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Pickup Details", nil];
            } else if ([[userInfo valueForKey:@"act"] isEqualToString:@"IH"]) {
                [self.preferences setMatchNotificationsForceSegueWithAction:[userInfo valueForKey:@"act"] andMatchID:[[userInfo valueForKey:@"id"] integerValue]];
                alert = [[UIAlertView alloc] initWithTitle:@"New Message" message:alertValue delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"View Message", nil];
            } else if ([[userInfo valueForKey:@"act"] isEqualToString:@"PH"]) {
                [self.preferences setMatchNotificationsForceSegueWithAction:[userInfo valueForKey:@"act"] andMatchID:[[userInfo valueForKey:@"id"] integerValue]];
                alert = [[UIAlertView alloc] initWithTitle:@"New Message" message:alertValue delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"View Message", nil];
            } else if ([[userInfo valueForKey:@"act"] isEqualToString:@"IF"]) {
                [self.preferences setMatchNotificationsForceSegueWithAction:[userInfo valueForKey:@"act"] andMatchID:[[userInfo valueForKey:@"id"] integerValue]];
                alert = [[UIAlertView alloc] initWithTitle:@"Match Update" message:alertValue delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"New Level", nil];
            } else if ([[userInfo valueForKey:@"act"] isEqualToString:@"IN"]) {
                [self.preferences setMatchNotificationsForceSegueWithAction:[userInfo valueForKey:@"act"] andMatchID:[[userInfo valueForKey:@"id"] integerValue]];
                alert = [[UIAlertView alloc] initWithTitle:@"Match Update" message:alertValue delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"View Dispute", nil];
            } else if ([[userInfo valueForKey:@"act"] isEqualToString:@"TD"]) {
                // team detail
                [self.preferences setMatchNotificationsForceSegueWithAction:[userInfo valueForKey:@"act"] andTeamID:[[userInfo valueForKey:@"id"] integerValue]];
                alert = [[UIAlertView alloc] initWithTitle:@"Team Update" message:alertValue delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"View Team", nil];
            } else if ([[userInfo valueForKey:@"act"] isEqualToString:@"CD"]) {
                [self.preferences setMatchNotificationsForceSegueWithAction:[userInfo valueForKey:@"act"] andVenueID:[[userInfo valueForKey:@"id"]
                                                                                                                      integerValue]];
                alert = [[UIAlertView alloc] initWithTitle:@"Venue Checkin" message:alertValue delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"View Venue", nil];
                
            } else if ([[userInfo valueForKey:@"act"] isEqualToString:@"PA"]) {
                alert = [[UIAlertView alloc] initWithTitle:@"You Have A New Prop" message:alertValue delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"View", nil];
            } else if ([[userInfo valueForKey:@"act"] isEqualToString:@"CA"]) {
                alert = [[UIAlertView alloc] initWithTitle:@"You Have A New Comment" message:alertValue delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"View", nil];
            } else if ([[userInfo valueForKey:@"act"] isEqualToString:@"FA"]) {
                alert = [[UIAlertView alloc] initWithTitle:@"You Have A New Follower" message:alertValue delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"View", nil];
            } else if ([[userInfo valueForKey:@"act"] isEqualToString:@"RA"]) {
                alert = [[UIAlertView alloc] initWithTitle:@"You Have A New Request" message:alertValue delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"View", nil];
            } else if ([[userInfo valueForKey:@"act"] isEqualToString:@"TA"]) {
                alert = [[UIAlertView alloc] initWithTitle:@"You Have Been Tagged" message:alertValue delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"View", nil];
            } else if ([[userInfo valueForKey:@"act"] isEqualToString:@"VT"]) { // Alert view for vote notifications
                alert = [[UIAlertView alloc] initWithTitle:@"You Got New Votes" message:alertValue delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"View", nil];
            } else if ([[userInfo valueForKey:@"act"] isEqualToString:@"TFV"]) { // Alert view for vote notifications
                alert = [[UIAlertView alloc] initWithTitle:@"You Got Thanks" message:alertValue delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"View", nil];
            } else {
                //alert = [[UIAlertView alloc] initWithTitle:@"Match Update" message:alertValue delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            }
            
            // optional - add more buttons:
            //[alert addButtonWithTitle:@"Go to match"];
            [alert show];
            
            
       
            });
    } else if (application.applicationState == UIApplicationStateBackground) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.preferences setNewsfeedRefreshState:YES];
            [self.preferences setMatchNotificationRefreshState:YES];
            [self.myTabBarController setSelectedIndex:1];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshMatchNotificationsPageNotification"
                                                                object:self
                                                              userInfo:nil];
        });
    } else if (application.applicationState == UIApplicationStateInactive) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.preferences setNewsfeedRefreshState:YES];
            [self.preferences setMatchNotificationRefreshState:YES];
            [self.myTabBarController setSelectedIndex:1];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshMatchNotificationsPageNotification"
                                                                object:self
                                                              userInfo:nil];
        });
    }
    
    

}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"Record Score"] ||
        [[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"Confirm Score"] ||
        [[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"Challenge Details"] ||
        [[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"Pickup Details"] ||
        [[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"View Message"] ||
        [[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"New Level"] ||
        [[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"View Dispute"] ||
        [[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"View Team"] ||
        [[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"View Venue"] ||
        [[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"View"]) {
        
  
        [self.myTabBarController setSelectedIndex:1];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshMatchNotificationsPageNotification"
                                                                object:self
                                                              userInfo:nil];
        
    } else {
        [self.preferences setMatchNotificationsForceSegueWithAction:@"" andMatchID:0]; // reset so won't push record / confirm view controller when loading matchnotificationsvc
        
        // also check if on match notifications page...if so, reload it
        
      //  if ([self.myTabBarController selectedIndex] == 0) {
     //       [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshMatchNotificationsPageNotification"
     //                                                           object:self
   //                                                           userInfo:nil];
            
     //   }
    }
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
 /*   UIButton *bu = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 200, 10)];
    bu.backgroundColor = [UIColor blackColor];
    [self.navController.view addSubview:bu];
    
    navigationController.delegate = self;
    
    // Add the navigation controller's view to the window and display.
    [self.window addSubview:navigationController.view];
    [self.window makeKeyAndVisible];*/ // removed by jc 2/7/2014
    
     
    
    // Let the device know we want to receive push notifications
	[[UIApplication sharedApplication] registerForRemoteNotificationTypes:
     (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    
    [self customizeNavBarFont];
/*
    // Override point for customization after application launch.
    
    self.sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    self.navController = [self.sb instantiateViewControllerWithIdentifier:@"Landing"];
    
    self.navController2 = [[UINavigationController alloc]
                          initWithRootViewController:self.navController];
    
    self.window.rootViewController = self.navController2;
    [self.window makeKeyAndVisible];
    

    // See if the app has a valid token for the current state.
    if (FBSession.activeSession.state == FBSessionStateCreatedTokenLoaded) {
 
        [self openSession];
    } else {
    //    [self showLoginView];
    }
    */
    
    if (launchOptions != nil)
	{
		NSDictionary *dictionary = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
		if (dictionary != nil)
		{
			NSLog(@"Launched from push notification: %@", dictionary);
			[self addMessageFromRemoteNotification:dictionary updateUI:NO withApplication:(UIApplication *)application];
		}
	}
  
   
    // allow shake gesture
    application.applicationSupportsShakeToEdit = YES;
  
    // init instance for in-app purchase
    [VotePuchaseTier1Helper sharedInstance];
    
    // save un-uploaded boost purchase
    if ([[NSUserDefaults standardUserDefaults] integerForKey:@"unupload_boost_votes"] > 0) {
        [IOSRequest saveBoostForUser:[self.preferences getUserID]
                            withType:[[NSUserDefaults standardUserDefaults] integerForKey:@"unupload_boost_votes"]
                            andPrice:[[NSUserDefaults standardUserDefaults] floatForKey:@"unupload_purchased_votes_price"]
                        onCompletion:^(NSDictionary *results) {
                            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"unupload_boost_votes"];
                            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"unupload_purchased_votes_price"];
                            [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"boosted"];
                        }];
    }
    
    return YES;
}

/* Receive the push notification when the app is running foreground */
- (void)application:(UIApplication*)application didReceiveRemoteNotification:(NSDictionary*)userInfo
{
    [IOSRequest getNumAlertsForUser:[self.preferences getUserID] onCompletion:^(NSDictionary *results) {
        dispatch_async(dispatch_get_main_queue(), ^ {
            [UIApplication sharedApplication].applicationIconBadgeNumber = [results[@"num_alerts"] integerValue];
         //   [[self.myTabBarController tabBarItem] setBadgeValue:[NSString stringWithFormat:@"%d", [results[@"num_alerts"] integerValue]]];
      //      [self.myTabBarController setSelectedIndex:0];
            self.myTabBarController.tabBarItem.badgeValue = [NSString stringWithFormat:@"%ld", (long)[results[@"num_alerts"] integerValue]];
        });
    }];
    
    
	NSLog(@"Received notification: %@", userInfo);
	[self addMessageFromRemoteNotification:userInfo updateUI:YES withApplication:application];
}

/* Receive the push notification when the app is running background or never be launched */
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    // Download the data corresponding to the received notification
    [IOSRequest getNumAlertsForUser:[self.preferences getUserID] onCompletion:^(NSDictionary *results) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [UIApplication sharedApplication].applicationIconBadgeNumber = [results[@"num_alertws"] integerValue];
            [self.myTabBarController.tabBarItem setBadgeValue:[NSString stringWithFormat:@"%ld", [results[@"num_alerts"] longValue]]];
        });
    }];
    
    NSLog(@"Received notification: %@", userInfo);
    [self addMessageFromRemoteNotification:userInfo updateUI:YES withApplication:application];
    
    // Success
    completionHandler(UIBackgroundFetchResultNewData);
}

- (void)customizeNavBarFont
{
    // Customize the title text for *all* UINavigationBars
    [[UINavigationBar appearance] setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont fontWithName:@"HelveticaNeue-Light" size:20.0],
      NSFontAttributeName,
      nil]];
    
    [[UINavigationBar appearance] setBarTintColor:[UIColor almostBlack]];
}
/*
- (void)sessionStateChanged:(FBSession *)session
                      state:(FBSessionState) state
                      error:(NSError *)error
{
    switch (state) {
        case FBSessionStateOpen: {
                  UIViewController *topViewController =
            [self.navController2 topViewController];
        //    if ([[topViewController modalViewController]
        //         isKindOfClass:[SCLoginViewController class]]) {
                [topViewController dismissViewControllerAnimated:YES completion:nil];
          //  }
            
        }
            break;
        case FBSessionStateClosed:
        case FBSessionStateClosedLoginFailed:
            // Once the user has logged in, we want them to
            // be looking at the root view.
       //     [self.navController popToRootViewControllerAnimated:NO];
            
            [FBSession.activeSession closeAndClearTokenInformation];
            
            [self showLoginView];
            break;
        default:
            break;
    }
    
    if (error) {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Error"
                                  message:error.localizedDescription
                                  delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
}

- (void)openSession
{
    [FBSession openActiveSessionWithReadPermissions:@[@"basic_info", @"user_location",
                                                      @"user_birthday",
                                                      @"user_likes",
                                                      @"email"]
                                       allowLoginUI:YES
                                  completionHandler:
     ^(FBSession *session,
       FBSessionState state, NSError *error) {
         [self sessionStateChanged:session state:state error:error];
         
         
         [FBRequestConnection
          startForMeWithCompletionHandler:^(FBRequestConnection *connection,
                                            id<FBGraphUser> user,
                                            NSError *error) {
              if (!error) {
                  NSString *userInfo = @"";
                  
                  userInfo = [userInfo
                              stringByAppendingString:
                              [NSString stringWithFormat:@"ID: %@\n\n",
                               user.id]];
                  
                  userInfo = [userInfo
                              stringByAppendingString:
                              [NSString stringWithFormat:@"Email: %@\n\n",
                               user[@"email"]]];
                  
                  userInfo = [userInfo
                              stringByAppendingString:
                              [NSString stringWithFormat:@"Gender: %@\n\n",
                               user[@"gender"]]];
                  
         
                  // Example: typed access (name)
                  // - no special permissions required
                  userInfo = [userInfo
                              stringByAppendingString:
                              [NSString stringWithFormat:@"Name: %@\n\n",
                               user.name]];
                  
                  // Example: typed access, (birthday)
                  // - requires user_birthday permission
                  userInfo = [userInfo
                              stringByAppendingString:
                              [NSString stringWithFormat:@"Birthday: %@\n\n",
                               user.birthday]];
                  
                  // Example: partially typed access, to location field,
                  // name key (location)
                  // - requires user_location permission
                  userInfo = [userInfo
                              stringByAppendingString:
                              [NSString stringWithFormat:@"Location: %@\n\n",
                               user.location[@"name"]]];
                  
                  // Example: access via key (locale)
                  // - no special permissions required
                  userInfo = [userInfo
                              stringByAppendingString:
                              [NSString stringWithFormat:@"Locale: %@\n\n",
                               user[@"locale"]]];
                  
                  // Example: access via key for array (languages)
                  // - requires user_likes permission
                  if (user[@"languages"]) {
                      NSArray *languages = user[@"languages"];
                      NSMutableArray *languageNames = [[NSMutableArray alloc] init];
                      for (int i = 0; i < [languages count]; i++) {
                          languageNames[i] = languages[i][@"name"];
                      }
                      userInfo = [userInfo
                                  stringByAppendingString:
                                  [NSString stringWithFormat:@"Languages: %@\n\n",
                                   languageNames]];
                  }
                  
                  // Display the user info
   //               self.userInfoTextView.text = userInfo;
              }
              [IOSRequest createOrUpdateFBUser:[user.id integerValue]
                                    fbFullname:user.name
                                       fbEmail:user[@"email"]
                                    fbLocation:user.location[@"name"]
                                         fbDOB:user.birthday
                                      fbGender:user[@"gender"]
                                  onCompletion:^(NSDictionary *results) {
                                      [self.preferences logInSession:[results[@"user_id"] integerValue]
                                                        andPicString:@""
                                                         andUserName:@""];
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          self.sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
                                          UITabBarController *newsTabBarController = [self.sb instantiateViewControllerWithIdentifier:@"TabControl"];
                                          UIViewController *topViewController = [self.navController2 topViewController];
                                          [topViewController presentViewController:newsTabBarController animated:NO completion:nil];
                                      });
                                      
                                  }];
              
          }];
     }];
 */
    /*
    self.sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    UITabBarController *newsTabBarController = [self.sb instantiateViewControllerWithIdentifier:@"TabControl"];
    UIViewController *topViewController = [self.navController2 topViewController];
    [topViewController presentViewController:newsTabBarController animated:NO completion:nil];*/
//}
/*
- (void)showLoginView
{
    self.sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    UINavigationController *loginVC = [self.sb instantiateViewControllerWithIdentifier:@"LoginNav"];
    
    UIViewController *topViewController = [self.navController2 topViewController];
    [topViewController presentViewController:loginVC animated:NO completion:nil];
*/

    
    /*
    UIViewController *topViewController = [self.navController topViewController];
    UIViewController *modalViewController = [topViewController modalViewController];
    
    // If the login screen is not already displayed, display it. If the login screen is
    // displayed, then getting back here means the login in progress did not successfully
    // complete. In that case, notify the login view so it can update its UI appropriately.
    if (![modalViewController isKindOfClass:[LoginTableVC class]]) {
   
  //      LoginTableVC *loginViewController = [[LoginTableVC alloc] initWithCoder:nil];
        //       LoginTableVC *loginViewController = [[LoginTableVC alloc] init];
        
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
        UINavigationController* loginViewController = [sb instantiateViewControllerWithIdentifier:@"LoginNav"];
        [self.navController setNavigationBarHidden:NO animated:YES];
        
        [topViewController presentViewController:loginViewController animated:NO completion:nil];
    } else {
        LoginTableVC* loginViewController =
        (LoginTableVC*)modalViewController;
        [loginViewController loginFailed];
    }*/
//}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
    return [FBSession.activeSession handleOpenURL:url];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadRunningManVideo"
                                                        object:self
                                                      userInfo:nil];
    
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [FBSession.activeSession handleDidBecomeActive];
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

//add at the bottom
- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    
	NSString *newToken = [deviceToken description];
	newToken = [newToken stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
	newToken = [newToken stringByReplacingOccurrencesOfString:@" " withString:@""];
    
	NSLog(@"My token is: %@", newToken);
    
	[self.preferences setDeviceToken:newToken];
    
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
	NSLog(@"Failed to get token, error: %@", error);
}
@end
