//
//  TagDetailTVC.m
//  Vaiden
//
//  Created by Turbo on 7/8/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "TagDetailTVC.h"
#import "StatTag.h"
#import "ChooseStatTagCell.h"

@interface TagDetailTVC ()
@property (nonatomic, strong) NSArray *filteredArray;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@end

@implementation TagDetailTVC

- (NSArray *)filteredArray
{
    if (!_filteredArray) _filteredArray = [[NSMutableArray alloc] init];
    return _filteredArray;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // set back button
    self.navigationController.navigationBar.topItem.title = @"";
    
    self.titleLabel.text = [[NSString stringWithFormat:@"%@", self.statLink] capitalizedString];
    [self.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:18]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"statLink = %@", self.statLink];
    self.filteredArray = [NSArray arrayWithArray:[self.tagsArray filteredArrayUsingPredicate:predicate]];
 
    [self.tableView reloadData];

}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.filteredArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ChooseStatTagCell *cell = [tableView dequeueReusableCellWithIdentifier:@"detail_input_cell" forIndexPath:indexPath];
    
    StatTag *tag = [self.filteredArray objectAtIndex:indexPath.row];
    
    cell.statNameLabel.text = [[NSString stringWithFormat:@"%@", tag.statTagName] capitalizedString];
    
    cell.statTextField.tag = tag.statTagID;
    cell.statTextField.text = tag.statTagValue;
    
    return cell;
}

- (IBAction)editingDidEndStatField:(UITextField *)sender
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"statTagID = %ld", sender.tag];
    NSArray *filteredArray = [NSArray arrayWithArray:[self.tagsArray filteredArrayUsingPredicate:predicate]];
    
    StatTag *tag = [filteredArray objectAtIndex:0]; // should only be one
    NSInteger myindex = [self.tagsArray indexOfObject:tag];
    
    if (sender.text.length == 0) {
        tag.statTagValue = nil;
    } else {
        tag.statTagValue = sender.text;
    }
    
    if ([sender.text length] > 0)
        tag.isSet = YES;
    else
        tag.isSet = NO;
    
    
    [self.tagsArray replaceObjectAtIndex:myindex withObject:tag];
}

- (IBAction)done:(UIBarButtonItem *)sender {
    [self.view endEditing:YES];
    
    // Always check the user's input
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"statLink = %@", self.statLink];
    NSArray *filteredArray = [NSArray arrayWithArray:[self.tagsArray filteredArrayUsingPredicate:predicate]];
    StatTag * numerator = (StatTag *)[filteredArray objectAtIndex:0];
    StatTag * denominator = (StatTag *)[filteredArray objectAtIndex:1];
    
    if ([numerator.statTagValue intValue] < [denominator.statTagValue intValue]) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Stats Input Error";
        alert.message = @"Your made times is above the attempt times. Please recheck your input.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
    } else {
        [self.delegate GetGroupedStats:self.tagsArray fromView:self];
        [self.navigationController popViewControllerAnimated:YES];
    }
        /*NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isSet = YES"];
    NSMutableArray *filteredArray = [NSMutableArray arrayWithArray:[self.tagsArray filteredArrayUsingPredicate:predicate]];
    
    if ([filteredArray count] <= 3) {
        [self.delegate setWithViewController:self withStatTags:filteredArray];
        NSLog(@"%@",filteredArray);
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Too Many Stats";
        alert.message = @"Please only enter up to 3 stats.  The rest of the fields should be blank.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
    }*/
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
