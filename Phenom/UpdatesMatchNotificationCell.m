//
//  OtherMatchNotificationCell.m
//  Phenom
//
//  Created by James Chung on 6/17/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "UpdatesMatchNotificationCell.h"

@implementation UpdatesMatchNotificationCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
