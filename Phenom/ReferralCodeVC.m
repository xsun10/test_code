//
//  ReferralCodeVC.m
//  Vaiden
//
//  Created by Turbo on 7/2/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "ReferralCodeVC.h"
#import "UIView+Manipulate.h"
#import "IOSRequest.h"
#import "UserPreferences.h"

@interface ReferralCodeVC () <UITextFieldDelegate>
@property (nonatomic, strong) UserPreferences *preferences;

@property (weak, nonatomic) IBOutlet UIScrollView *pageScrollView;
@property (weak, nonatomic) IBOutlet UIButton *sumitButton;
@property (weak, nonatomic) IBOutlet UITextField *inputTextField;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@end

@implementation ReferralCodeVC

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	// Do any additional setup after loading the view.
    [self.sumitButton makeRoundedBorderWithRadius:3];
    [self.inputTextField makeRoundedBorderWithRadius:3];
    [self.sumitButton setBackgroundColor:[UIColor lightGrayColor]];
    
    // Change the keyboard tyoe for the text field
    [self.inputTextField setKeyboardType:UIKeyboardTypeDefault];
    // Disable the auto correction
    [self.inputTextField setAutocorrectionType:UITextAutocorrectionTypeNo];
    
    [self.inputTextField setEnabled:NO];
    [self.sumitButton setEnabled:NO];
    
    [self.inputTextField setDelegate:self];
    [self.inputTextField setReturnKeyType:UIReturnKeyDone];
    
    // Add tap gesture to dismiss the keyboard
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyBoard)];
    [tap setCancelsTouchesInView:NO];
    [self.view addGestureRecognizer:tap];
    
    [self getReferralCode];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self.inputTextField addTarget:self action:@selector(textViewDidBeginEditing) forControlEvents:UIControlEventEditingDidBegin];
    self.pageScrollView.contentSize = CGSizeMake(320.0, 650.0);
}

- (void)textViewDidBeginEditing
{
    [self.pageScrollView setContentOffset:CGPointMake(0, 120 ) animated:YES];
}

- (void)textViewDidEndEditing
{
    [self.pageScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
}

- (void) getReferralCode
{
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.labelText = @"Loading...";
    [IOSRequest getReferralCodeForUSer:[self.preferences getUserID]
                          onCompletion:^(NSDictionary *results) {
                              
                              if ([results[@"outcome"] isEqualToString:@"failure"]) {
                                  dispatch_async(dispatch_get_main_queue(), ^ { // back to main queue to change the UI
                                      [MBProgressHUD hideHUDForView:self.view animated:YES];
                                      UIAlertView *alert = [[UIAlertView alloc] init];
                                      alert.title = @"Server Error";
                                      alert.message = @"There was a problem retrieving your data.  Please try again later.";
                                      [alert addButtonWithTitle:@"OK"];
                                      [alert show];
                                      self.sumitButton.enabled = YES;
                                  });
                              } else if ([results[@"outcome"] isEqualToString:@"notFound"]) {
                                  dispatch_async(dispatch_get_main_queue(), ^ { // back to main queue to change the UI
                                      [MBProgressHUD hideHUDForView:self.view animated:YES];
                                      UIAlertView *alert = [[UIAlertView alloc] init];
                                      alert.title = @"User Not Found";
                                      alert.message = @"We were unfortunately not able to find your account.";
                                      [alert addButtonWithTitle:@"OK"];
                                      [alert show];
                                      self.sumitButton.enabled = YES;
                                  });
                              } else if ([results[@"outcome"] isEqualToString:@"done"]) {
                                  [MBProgressHUD hideHUDForView:self.view animated:YES];
                                  if (results[@"code"] == (NSString *)[NSNull null]) {
                                      [self.inputTextField setEnabled:YES];
                                      [self.sumitButton setEnabled:YES];
                                      [self.sumitButton setBackgroundColor:[UIColor colorWithRed:199/255.0 green:160/255.0 blue:74/255.0 alpha:1.0]];
                                  } else {
                                      [self.inputTextField setText:results[@"code"]];
                                      [self.sumitButton setHidden:YES];
                                  }
                              }
                          }
     ];
    double delayInSeconds = 10.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        //code to be executed on the main queue after delay
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    });
}

- (IBAction)sumitCode:(UIButton *)sender {
    if ([self isCodeValid]) {
        [self.sumitButton setEnabled:NO];
        [self dismissKeyBoard];
        HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        HUD.labelText = @"Submitting";
        [IOSRequest saveReferralCode:self.inputTextField.text
                              ofUser:[self.preferences getUserID]
                        onCompletion:^(NSDictionary *results) {
                            NSLog(@"%@",results);
                            
                            if ([results[@"outcome"] isEqualToString:@"failure"]) {
                                dispatch_async(dispatch_get_main_queue(), ^ { // back to main queue to change the UI
                                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                                    UIAlertView *alert = [[UIAlertView alloc] init];
                                    alert.title = @"Submission Error";
                                    alert.message = @"There was a problem submitting your request.  Please try again later.";
                                    [alert addButtonWithTitle:@"OK"];
                                    [alert show];
                                    self.sumitButton.enabled = YES;
                                });
                            } else if ([results[@"outcome"] isEqualToString:@"notFound"]) {
                                dispatch_async(dispatch_get_main_queue(), ^ { // back to main queue to change the UI
                                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                                    UIAlertView *alert = [[UIAlertView alloc] init];
                                    alert.title = @"User Not Found";
                                    alert.message = @"We were unfortunately not able to find your account.";
                                    [alert addButtonWithTitle:@"OK"];
                                    [alert show];
                                    self.sumitButton.enabled = YES;
                                });
                            } else if ([results[@"outcome"] isEqualToString:@"notValid"]) {
                                 dispatch_async(dispatch_get_main_queue(), ^ {
                                     [MBProgressHUD hideHUDForView:self.view animated:YES];
                                     UIAlertView *alert = [[UIAlertView alloc] init];
                                     alert.title = @"Referral Code Error";
                                     alert.message = @"Please enter a valid Referral Code for Vaiden Brand Ambassador.";
                                     [alert addButtonWithTitle:@"OK"];
                                     [alert show];
                                     self.sumitButton.enabled = YES;
                                 });
                            } else if ([results[@"outcome"] isEqualToString:@"done"]) {
                                dispatch_async(dispatch_get_main_queue(), ^ {
                                    HUD.labelText = @"Submitted !";
                                });
                                double delayInSeconds = 1.0;
                                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
                                dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                                    //code to be executed on the main queue after delay
                                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                                    [self dismissViewControllerAnimated:YES completion:nil];
                                });
                                
                            }
                         }
         ];
        
        double delayInSeconds = 10.0;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            //code to be executed on the main queue after delay
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        });
    }
}

- (BOOL)isCodeValid
{
    if ([self.inputTextField.text length] != 5) { // 5 digit for the referral code
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Referral Code Error";
        alert.message = @"Please enter a valid Referral Code for Vaiden Brand Ambassador.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
        return NO;
    } else {
        return YES;
    }
}

- (IBAction)close:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)dismissKeyBoard
{
    [self.inputTextField resignFirstResponder];
    [self textViewDidEndEditing];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [self textViewDidEndEditing];
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
