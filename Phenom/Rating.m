//
//  Rating.m
//  Phenom
//
//  Created by James Chung on 7/4/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "Rating.h"

@implementation Rating


-(Rating *)initWithRating:(NSInteger)ratingValue
                   byUser:(NSInteger)userID
             withUserName:(NSString *)userName
      andProfilePicString:(NSString *)profilePicString
              withComment:(NSString *)comment
                   onDate:(NSDate *)ratingDate
{
    self = [super init];
    
    if (self) {
      
        _ratingValue = ratingValue;
        _userID = userID;
        _userName = userName;
        _profilePicString = profilePicString;
        _comment = comment;
        _ratingDate = ratingDate;
    }
    
    return self;
}


- (UIImage *)getRatingImage
{
    UIImage *ratingImage = nil;
    
    NSInteger rate = (int)floor(self.ratingValue);
    
    switch (rate) {

        case 0:
            ratingImage = [UIImage imageNamed:@"star zero"];
            break;
        
        case 1:
            ratingImage = [UIImage imageNamed:@"star one"];
            break;
            
        case 2:
            ratingImage = [UIImage imageNamed:@"star two"];
            break;
            
        case 3:
            ratingImage = [UIImage imageNamed:@"star three"];
            break;
            
        case 4:
            ratingImage = [UIImage imageNamed:@"star four"];
            break;
            
        case 5:
            ratingImage = [UIImage imageNamed:@"star five"];
            break;
            
        default:
            ratingImage = [UIImage imageNamed:@"star zero"];
            break;
    }
    return  ratingImage;
}

@end
