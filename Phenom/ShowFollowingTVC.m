//
//  ShowFollowingTVC.m
//  Phenom
//
//  Created by James Chung on 7/9/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "ShowFollowingTVC.h"
#import "IOSRequest.h"
#import "Contact.h"
#import "ProfileFollowingCell.h"
#import "S3Tools.h"
#import "UIImage+ProportionalFill.h"
#import "AFNetworking.h"
#import "UIButton+RoundBorder.h"
#import "UserPreferences.h"
#import "PlayerDetailTVC.h"
#import "UIColor+VaidenColors.h"
#import "UIView+Manipulate.h"
#import "NoResultTVC.h"
#import "StartByAddingFBContacts.h"

@interface ShowFollowingTVC ()

@property (nonatomic, strong) NSMutableArray *players;
@property (nonatomic, strong) UserPreferences *preferences;
@property (nonatomic) BOOL noResultsToDisplay;
@property (nonatomic) NSUInteger selectedUserRow;
@end

@implementation ShowFollowingTVC

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (NSMutableArray *)players
{
    if (!_players) _players = [[NSMutableArray alloc] init];
    return _players;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.preferences setChallengePlayersListRefreshState:NO];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
    
    self.noResultsToDisplay = NO;

    [self loadFollowing];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if ([self.preferences getChallengePlayersListRefreshState]) {
        self.noResultsToDisplay = NO;
        [self.preferences setChallengePlayersListRefreshState:NO];
        [self loadFollowing];
    }
}

- (void)loadFollowing
{
    [IOSRequest fetchUserFollowingList:self.baseUserID
                         sessionUserID:[self.preferences getUserID]
                          onCompletion:^(NSMutableArray *queryList) {
        [self.players removeAllObjects];
        for(id user in queryList) {
            
            if ([user isKindOfClass:[NSDictionary class]])
            {
                NSLog(@"%@",user);
                Contact *vContact = [[Contact alloc] initWithContactDetails:[user[@"user_id"] integerValue]
                                                              andProfileStr:user[@"profile_pic_string"]
                                                                andUserName:user[@"username"]
                                                                andFullName:user[@"fullname"]
                                                                andLocation:user[@"location"]
                                                                   andAbout:user[@"about"]
                                                                   andCoins:[user[@"coins"] floatValue]
                                                                contactType:CONTACT_TYPE_FOLLOWING
                                                            followingStatus:[user[@"follow_status"] integerValue]]; // follow flag is whether session user follows or not
 
                [self.players addObject:vContact];
                
            }
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([queryList count] == 0) {
                self.tableView.scrollEnabled = NO;
                self.noResultsToDisplay = YES;
            } else {
                self.tableView.scrollEnabled = YES;
                self.noResultsToDisplay = NO;
            }
            [self.tableView reloadData];
        });
    
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.noResultsToDisplay)
        return 1;
    
    return [self.players count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if (self.noResultsToDisplay) {
        NoResultTVC *cell = [tableView dequeueReusableCellWithIdentifier:@"no_followers"];
        [cell setBackgroundColor:[UIColor clearColor]];
        cell.icon.image = [UIImage imageNamed:@"no_friends"];
        [cell.icon setContentMode:UIViewContentModeCenter];
        
        cell.headline.text = @"No Following Found";
        cell.message.text = @"Invite friends to join by pressing ‘+’.";
        return cell;
    } else {
        static NSString *CellIdentifier = @"Player Cell";
        ProfileFollowingCell *cell = (ProfileFollowingCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        cell.textLabel.hidden = YES;
        cell.followButton.hidden = NO;
        cell.profilePic.hidden = NO;
        cell.usernameLabel.hidden = NO;
        cell.locationLabel.hidden = NO;
        Contact *vContact = [self.players objectAtIndex:indexPath.row];
        
        cell.usernameLabel.text = vContact.userName;
        cell.locationLabel.text = vContact.location;
        
        if (vContact.followingStatus == IS_FOLLOWING_STATUS && vContact.userID != [self.preferences getUserID]) {
            cell.followButton.hidden = NO;
            cell.followButton.enabled = YES;
            //cell.followButton.backgroundColor = [UIColor newBlueLight];
            [cell.followButton setTitle:@"UnFollow" forState:UIControlStateNormal];
        } else if (vContact.followingStatus == NOT_FOLLOWING_STATUS && vContact.userID != [self.preferences getUserID]){
            cell.followButton.hidden = NO;
            cell.followButton.enabled = YES;
            //cell.followButton.backgroundColor = [UIColor newBlueLight];
            [cell.followButton setTitle:@"Follow" forState:UIControlStateNormal];
        } else if (vContact.followingStatus == PENDING_FOLLOWING_STATUS && vContact.userID != [self.preferences getUserID]){
            cell.followButton.hidden = NO;
            //cell.followButton.backgroundColor = [UIColor lightGrayColor];
            [cell.followButton setTitle:@"Pending" forState:UIControlStateNormal];
            cell.followButton.enabled = NO;
        } else {
            cell.followButton.hidden = YES;
        }
        
        cell.followButton.tag = indexPath.row;
        
        NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                baseString:vContact.profileBaseString
                         ];
        
        [cell.profilePic setImageWithURL:[NSURL URLWithString:url]
                        placeholderImage:[UIImage imageNamed:@"avatar square"]];
        
        [UIImage makeRoundedImageNoBorder:cell.profilePic withRadius:10];
        
        [cell.followButton makeRoundedBorderWithRadius:3];
        
        
        return cell;
    }
}
/*
- (IBAction)UnfollowAction:(UIButton *)sender
{
    UIButton *button = (UIButton *)sender;
    CGRect buttonFrame = [button convertRect:button.bounds toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonFrame.origin];
    
    NSInteger playerID = [[self.players objectAtIndex:indexPath.row] userID];
    
    NSMutableArray *rowsToDelete = [[NSMutableArray alloc] init];
    
    [rowsToDelete addObject:indexPath];
    
    [IOSRequest removeFollowConnectionWithLeader:playerID
                                     andFollower:self.baseUserID
                                    onCompletion:^(NSMutableArray * results) {
                                        
                                        
                                        [self.players removeObjectAtIndex:indexPath.row];
                                        dispatch_async(dispatch_get_main_queue(), ^ {
                                            [self.tableView beginUpdates];
                                            [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationLeft];
                                            [self.tableView endUpdates];
                                        });
                                        
    }];
}

*/

- (void)showSubmissionError
{
    UIAlertView *alert = [[UIAlertView alloc] init];
    alert.title = @"Submission Error";
    alert.message = @"There was a problem submitting your request.  Please try again.";
    [alert addButtonWithTitle:@"OK"];
    [alert show];
}

- (IBAction)followPlayer:(UIButton *)sender
{
    if (!self.noResultsToDisplay) {
        
        Contact *vContact = [self.players objectAtIndex:sender.tag];
        
        if (vContact.followingStatus == IS_FOLLOWING_STATUS && vContact.userID != [self.preferences getUserID]) {
            [IOSRequest removeFollowConnectionWithLeader:vContact.userID
                                             andFollower:[self.preferences getUserID]
                                            onCompletion:^(NSDictionary *results) {
                                                
                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                    
                                                    
                                                    if (![results[@"outcome"] isEqualToString:@"failure"]) {
                                                        
                                                        vContact.followingStatus = NOT_FOLLOWING_STATUS;
                                                        [self.preferences setChallengePlayersListRefreshState:YES];
                                                        [self.preferences setProfilePageRefreshState:YES];
                                                        [self.preferences setNewsfeedRefreshState:YES];
                                                        [self.players replaceObjectAtIndex:sender.tag withObject:vContact];
                                                        [self.tableView reloadData];
                                                    } else {
                                                        [self showSubmissionError];
                                                    }
                                                    
                                                });
                                            }];
        } else if (vContact.followingStatus == NOT_FOLLOWING_STATUS && vContact.userID != [self.preferences getUserID]) {
            [IOSRequest makeFollowConnectionWithLeader:vContact.userID
                                           andFollower:[self.preferences getUserID]
                                          onCompletion:^(NSDictionary *results) {
                                              
                                              dispatch_async(dispatch_get_main_queue(), ^ {
                                                  
                                                  if (![results[@"outcome"] isEqualToString:@"failure"]) {
                                                      
                                                      if ([results[@"follow_status"] integerValue] == 1)
                                                          vContact.followingStatus = IS_FOLLOWING_STATUS;
                                                      else if ([results[@"follow_status"] integerValue] == 2)
                                                          vContact.followingStatus = PENDING_FOLLOWING_STATUS;
                                                      
                                                      [self.preferences setChallengePlayersListRefreshState:YES];
                                                      [self.preferences setProfilePageRefreshState:YES];
                                                      [self.preferences setNewsfeedRefreshState:YES];
                                                      [self.players replaceObjectAtIndex:sender.tag withObject:vContact];
                                                      [self.tableView reloadData];
                                                  } else {
                                                      [self showSubmissionError];
                                                  }
                                              });
                                          }];
            
            
        }
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.noResultsToDisplay) {
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        return screenRect.size.height - 105;
    }
    return 70.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!self.noResultsToDisplay) {
        self.selectedUserRow = indexPath.row;
        [self performSegueWithIdentifier:@"Player Detail Segue" sender:self];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Player Detail Segue"]) {
        PlayerDetailTVC *controller = segue.destinationViewController;
        Contact *vContact = [self.players objectAtIndex:self.selectedUserRow];
        
        controller.playerUserID = vContact.userID;
        controller.profilePicString = vContact.profileBaseString;
    } else if ([segue.identifier isEqualToString:@"find_friends_segue"]) {
        StartByAddingFBContacts *controller = segue.destinationViewController;
        controller.isLogin = YES;
    }
}


@end
