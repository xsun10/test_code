//
//  LeaderboardVC.m
//  Phenom
//
//  Created by James Chung on 7/15/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "LeaderboardVC.h"
#import "IOSRequest.h"
#import "AFNetworking.h"
#import "S3Tools.h"
#import "UIImage+ProportionalFill.h"
#import "Contact.h"
#import "LeaderboardCell.h"
#import "UserPreferences.h"
#import "PlayerDetailTVC.h"
#import "UIColor+VaidenColors.h"
#import "UIButton+RoundBorder.h"
#import "UIView+Manipulate.h"

@interface LeaderboardVC ()
@property (weak, nonatomic) IBOutlet UITableView *playersTableView;
@property (strong, nonatomic)  UILabel *sportLabel;
@property (nonatomic, strong) NSMutableArray *leaders;
@property (strong, nonatomic)  UIView *fixedMenu;
@property (nonatomic, strong) UserPreferences *preferences;
@property (nonatomic) NSUInteger selectedRow;
@property (strong, nonatomic) UIButton *sportButtonInHiddenFixedMenu;
@property (nonatomic, strong) UIView *tempView;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
@property (nonatomic, strong) UIView *overlay;

@end

@implementation LeaderboardVC


- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (NSMutableArray *)leaders
{
    if (!_leaders) _leaders = [[NSMutableArray alloc] init];
    return _leaders;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self showTempSpinner];
    if (self.filterSportID == 0)
        self.filterSportID = 1;

 //   [self.sportButtonInHiddenFixedMenu makeCircleWithColorAndBackground:[UIColor almostBlack] andRadius:5];
    
    self.playersTableView.delegate = self;
    self.playersTableView.dataSource = self;
    
 //   self.fixedMenu.layer.borderColor = [UIColor darkGrayColor].CGColor;
 //   self.fixedMenu.layer.borderWidth = .5f;
    
 //   self.fixedMenu.center = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height-10/2);
  /*
    self.fixedMenu = [[UIView alloc] initWithFrame:CGRectMake(0, 64, 320, 35)];
    self.fixedMenu.backgroundColor = [UIColor almostBlack];
    
    self.fixedMenu.layer.borderColor = [UIColor almostBlack].CGColor;
    self.fixedMenu.layer.borderWidth = .5f;
    
    [self.view addSubview:self.fixedMenu];
    
    self.sportLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, 180, 35)];
    self.sportLabel.textColor = [UIColor whiteColor];
    self.sportLabel.font = [UIFont systemFontOfSize:12];
    
    [self.fixedMenu addSubview:self.sportLabel];
    
    self.sportButtonInHiddenFixedMenu = [[UIButton alloc] initWithFrame:CGRectMake(270, 8, 45, 20)];
    [self.sportButtonInHiddenFixedMenu setTitle:@"Sport" forState:UIControlStateNormal];
    self.sportButtonInHiddenFixedMenu.titleLabel.textColor = [UIColor whiteColor];
    [self.sportButtonInHiddenFixedMenu.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-LightItalic" size:12]];
    self.sportButtonInHiddenFixedMenu.layer.borderColor = [UIColor whiteColor].CGColor;
    self.sportButtonInHiddenFixedMenu.layer.borderWidth = 0.5;
    [self.sportButtonInHiddenFixedMenu addTarget:self action:@selector(changeSport:) forControlEvents:UIControlEventTouchUpInside];
   
    [self.fixedMenu addSubview:self.sportButtonInHiddenFixedMenu];
  */
    self.fixedMenu = [[UIView alloc] initWithFrame:CGRectMake(0, 64, 320, 40)]; // was 35 height
    self.fixedMenu.backgroundColor = [UIColor colorWithRed:34/255.0 green:34/255.0 blue:34/255.0 alpha:1.0];
    
    self.fixedMenu.layer.borderColor = [UIColor colorWithRed:34/255.0 green:34/255.0 blue:34/255.0 alpha:1.0].CGColor;
    self.fixedMenu.layer.borderWidth = .5f;
    
    [self.view addSubview:self.fixedMenu];
    
    UIImageView *view = [[UIImageView alloc] initWithFrame:CGRectMake(10, 0, 20, 40)];
    [view setContentMode:UIViewContentModeCenter];
    [view setImage:[UIImage imageNamed:@"nearby_icon"]];
    [self.fixedMenu addSubview:view];
    
    UILabel *hintLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, 0, 150, 40)];
    [hintLabel setTextColor:[UIColor whiteColor]];
    [hintLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:15]];
    [hintLabel setText:@"Showing: "];
    [self.fixedMenu addSubview:hintLabel];
    
    self.sportLabel = [[UILabel alloc] initWithFrame:CGRectMake(100, 0, 180, 40)]; // was 35 height
    self.sportLabel.textColor = [UIColor whiteColor];
    self.sportLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:15];
    
    [self.fixedMenu addSubview:self.sportLabel];
    
    /*self.sportButtonInHiddenFixedMenu = [[UIButton alloc] initWithFrame:CGRectMake(255, 7, 60, 25)]; // was 20 height & 8 y location
    [self.sportButtonInHiddenFixedMenu setTitle:@"FILTER" forState:UIControlStateNormal];
    [self.sportButtonInHiddenFixedMenu setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    //    self.sportButtonInHiddenFixedMenu.titleLabel.textColor = [UIColor darkGrayColor];
    [self.sportButtonInHiddenFixedMenu.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-MediumItalic" size:13]];
    self.sportButtonInHiddenFixedMenu.layer.borderColor = [UIColor darkGrayColor].CGColor;
    self.sportButtonInHiddenFixedMenu.layer.borderWidth = 0.5;
    [self.sportButtonInHiddenFixedMenu addTarget:self action:@selector(changeSport:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.fixedMenu addSubview:self.sportButtonInHiddenFixedMenu];*/
    
    
 //   [self.playersTableView.superview addSubview:self.fixedMenu];
    
    if (self.filterSportName == nil && [self.filterSportName length] == 0)
        self.sportLabel.text = @"Basketball";
    else
        self.sportLabel.text = [self.filterSportName capitalizedString];


    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
    [self loadLeaderboard];
    
    [self.playersTableView setContentInset:UIEdgeInsetsMake(35,0,150,0)];
}

- (void)displayErrorOverlay
{
    self.overlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, [UIScreen mainScreen].bounds.size.height)];
    self.overlay.backgroundColor = [UIColor lightLightGray];
    
    UILabel *firstLine = [[UILabel alloc] initWithFrame:CGRectMake(30, ([UIScreen mainScreen].bounds.size.height / 2) - 115, 260, 30)];
    firstLine.textColor = [UIColor darkGrayColor];
    firstLine.font = [UIFont systemFontOfSize:23];
    firstLine.text = @"Leaderboard In Progress";
    
    UILabel *secondLine = [[UILabel alloc] initWithFrame:CGRectMake(30, ([UIScreen mainScreen].bounds.size.height / 2) - 110, 260, 110)];
    secondLine.textColor = [UIColor lightGrayColor];
    secondLine.font = [UIFont systemFontOfSize:17];
    secondLine.text = @"The leaderboard for this particular sport is not yet complete.";
    secondLine.numberOfLines = 0;
    
    [self.overlay addSubview:firstLine];
    [self.overlay addSubview:secondLine];
    
    [self.playersTableView addSubview:self.overlay];
}

- (void)showTempSpinner
{
    self.tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    self.tempView.backgroundColor = [UIColor lightGrayColor];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] init];
    [self.activityIndicator setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width/2.0, [UIScreen mainScreen].bounds.size.height/2.0)];
    [self.activityIndicator startAnimating];
    [self.tempView addSubview:self.activityIndicator];
    
    [self.view addSubview:self.tempView];
    
}

- (void)loadLeaderboard
{
    [IOSRequest fetchLeaderboardforSport:self.filterSportID
                                withUser:[self.preferences getUserID]
                            onCompletion:^(NSMutableArray *results) {
                                
                                [self.leaders removeAllObjects];
                                for (id object in results) {
                                    Contact *vContact = [[Contact alloc]
                                                         initWithContactDetails:[object[@"user_id"] integerValue]
                                                         andProfileStr:object[@"profile_pic_string"]
                                                         andUserName:object[@"username"]
                                                         andFullName:object[@"fullname"]
                                                         andLocation:@""
                                                         andAbout:@""
                                                         andCoins:[object[@"coins"] floatValue]
                                                         contactType:CONTACT_TYPE_FOLLOWING  // I think this must be removed
                                                         followingStatus:[object[@"follow_status"] integerValue]];
                                    
                                    
                                    NSDictionary *sportObj = object[@"sport"];
                                    
                                    
                                    PlayerSport *sport = [[PlayerSport alloc] initWithSportDetails:sportObj[@"sport_name"]
                                                                                           andType:sportObj[@"type"]
                                                                                          andLevel:sportObj[@"level"]
                                                                                    andSportRecord:sportObj[@"win_loss_record"]];
                                    
                                    [vContact addPlayerSport:sport];
                                    
                                    
                                    [self.leaders addObject:vContact];
                                    
                                }
                                
                                dispatch_async(dispatch_get_main_queue(), ^ {
                                    if ([results count] == 0)
                                        [self displayErrorOverlay];
                                    
                                    [self.playersTableView reloadData];
                                    [self.activityIndicator stopAnimating];
                                    self.tempView.hidden = YES;
                                });

                            }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.leaders count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Players Cell2";
    
    LeaderboardCell *cell = (LeaderboardCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    Contact *vContact = (self.leaders)[indexPath.row];
    
    PlayerSport *sport = [vContact.playerSports objectAtIndex:0]; // only one sport should be in array
    
    if ([vContact computeTotalNumMatchesPlayedForSport:sport.sportID] > 0)
        cell.levelLabel.text = [NSString stringWithFormat:@"%ld",  [sport.level integerValue]];
    else
        cell.levelLabel.text = @"0";
    
    cell.recordLabel.text = [NSString stringWithFormat:@"%lu %@ - %lu %@", sport.playerRecordForSport.wins, [[sport.playerRecordForSport getWinsText] capitalizedString], sport.playerRecordForSport.losses, [[sport.playerRecordForSport getLossesText] capitalizedString]];
    cell.coinLabel.text = [NSString stringWithFormat:@"%.0f", vContact.coins];
    
    cell.usernameLabel.text = vContact.userName;
    cell.rankLabel.text = [NSString stringWithFormat:@"%ld", indexPath.row + 1];
    cell.sportNameLabel.text = [sport.sportName capitalizedString];
    
    NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                            baseString:vContact.profileBaseString
                     ];
    
    [cell.profilePic setImageWithURL:[NSURL URLWithString:url]
                    placeholderImage:[UIImage imageNamed:@"avatar square"]];
    
    [UIImage makeRoundedImageNoBorder:cell.profilePic withRadius:10];

    [cell.followButton makeRoundedBorderWithRadius:3];
    cell.followButton.tag = indexPath.row;
    
    if (vContact.followingStatus == NOT_FOLLOWING_STATUS && (vContact.userID != [self.preferences getUserID])) {
        cell.followButton.enabled = YES;
        //cell.followButton.backgroundColor = [UIColor newBlueLight];
        [cell.followButton setTitle:@"Follow" forState:UIControlStateNormal];
    } else if (vContact.followingStatus == IS_FOLLOWING_STATUS && vContact.userID != [self.preferences getUserID]) {
        cell.followButton.enabled = YES;
        //cell.followButton.backgroundColor = [UIColor newBlueLight];
        [cell.followButton setTitle:@"UnFollow" forState:UIControlStateNormal];
    } else if (vContact.followingStatus == PENDING_FOLLOWING_STATUS && vContact.userID != [self.preferences getUserID]) {
        [cell.followButton setTitle:@"Pending" forState:UIControlStateNormal];
        cell.followButton.enabled = NO;
        //cell.followButton.backgroundColor = [UIColor lightGrayColor];
    } else
        cell.followButton.hidden = YES;
    
    
    return  cell;
}

- (void)setWithSportFromVC:(ChooseIndividualSportTVC *)controller withSport:(Sport *)sport
{
    [self.overlay removeFromSuperview];
    self.overlay = nil;
    
    self.sportLabel.text = [sport.sportName capitalizedString];
    self.filterSportID = sport.sportID;
    
    [self loadLeaderboard];
        
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 84.0;
}

- (IBAction)changeSport:(id)sender
{
    [self performSegueWithIdentifier:@"Choose Sport Segue" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Choose Sport Segue"]) {
        ChooseIndividualSportTVC *controller = segue.destinationViewController;
        controller.delegate = self;
    } else if ([segue.identifier isEqualToString:@"Player Detail Segue"]) {
        PlayerDetailTVC *controller = segue.destinationViewController;
        
        Contact *vContact = [self.leaders objectAtIndex:self.selectedRow];
        controller.playerUserID = vContact.userID;
        controller.profilePicString = vContact.profileBaseString;
    }
}
/*
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    // CGFloat stillViewDesiredOriginY; declared ivar
    CGRect newFrame = self.fixedMenu.frame;
    newFrame.origin.x = 0;
    newFrame.origin.y = self.playersTableView.contentOffset.y+(self.playersTableView.frame.size.height-35);
    self.fixedMenu.frame = newFrame;
}
 */

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedRow = indexPath.row;
    [self performSegueWithIdentifier:@"Player Detail Segue" sender:self];
}

- (void)showSubmissionError
{
    UIAlertView *alert = [[UIAlertView alloc] init];
    alert.title = @"Submission Error";
    alert.message = @"There was a problem submitting your request.  Please try again.";
    [alert addButtonWithTitle:@"OK"];
    [alert show];
}

- (IBAction)followAction:(UIButton *)sender
{
    Contact *vContact = [self.leaders objectAtIndex:sender.tag];
    sender.enabled = NO;
    if (vContact.followingStatus == IS_FOLLOWING_STATUS && vContact.userID != [self.preferences getUserID]) {
        [IOSRequest removeFollowConnectionWithLeader:vContact.userID
                                         andFollower:[self.preferences getUserID]
                                        onCompletion:^(NSDictionary *results) {
                                            
                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                sender.enabled = YES;
                                                
                                                if (![results[@"outcome"] isEqualToString:@"failure"]) {
                                                    [self.preferences setNewsfeedRefreshState:YES];
                                                    [self.preferences setChallengePlayersListRefreshState:YES];
                                                    vContact.followingStatus = NOT_FOLLOWING_STATUS;
                                                    [self.leaders replaceObjectAtIndex:sender.tag withObject:vContact];
                                                    [self.playersTableView reloadData];
                                                } else {
                                                    [self showSubmissionError];
                                                }
                                                
                                            });
                                        }];
    } else if (vContact.followingStatus == NOT_FOLLOWING_STATUS && vContact.userID != [self.preferences getUserID]) {
        [IOSRequest makeFollowConnectionWithLeader:vContact.userID
                                       andFollower:[self.preferences getUserID]
                                      onCompletion:^(NSDictionary *results) {
                                          
                                          dispatch_async(dispatch_get_main_queue(), ^ {
                                              sender.enabled = YES;
                                              if (![results[@"outcome"] isEqualToString:@"failure"]) {
                                                  [self.preferences setNewsfeedRefreshState: YES];
                                                  [self.preferences setChallengePlayersListRefreshState:YES];
                                                  
                                                  vContact.followingStatus = [results[@"follow_status"] integerValue];
                                                  
                                                  [self.leaders replaceObjectAtIndex:sender.tag withObject:vContact];
                                                  [self.playersTableView reloadData];
                                              } else {
                                                  [self showSubmissionError];
                                              }
                                          });
                                      }];
        
        
    }

}


@end
