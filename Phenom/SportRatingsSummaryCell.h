//
//  SportRatingsSummaryCell.h
//  Vaiden
//
//  Created by James Chung on 10/4/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SportRatingsSummaryCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *skillLabel;
@property (weak, nonatomic) IBOutlet UIImageView *starIcons;
@property (weak, nonatomic) IBOutlet UILabel *numRatings;
@end
