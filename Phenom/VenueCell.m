//
//  VenueCell.m
//  Phenom
//
//  Created by James Chung on 5/23/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "VenueCell.h"
#import "VenueSportsCVC.h"
#import "Sport.h"
#import "UIView+Manipulate.h"
#import "UIColor+VaidenColors.h"

@implementation VenueCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews
{
    self.sportsCV.delegate = self;
    self.sportsCV.dataSource = self;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if ([self.sportsArray count] > 2) {
        return 2;
    } else {
        return [self.sportsArray count];
    }
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Venue Sports";
    
    VenueSportsCVC *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    
    if ([self.sportsArray count] <= 2) {
        // then just show the cells
        Sport *sport = [self.sportsArray objectAtIndex:indexPath.row];
        
     //   cell.sportIcon.image = [[sport getSportIcon] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
     //   [cell.sportIcon setTintColor:[UIColor whiteColor]];
        
        [cell.circleIcon makeCircleWithColor:[UIColor whiteColor] andRadius:4];
        cell.circleIcon.hidden = NO;
        cell.sportName.text = [sport.sportName capitalizedString];
        
        return cell;
        
    } else {
        if (indexPath.row > 0) {
           cell.sportName.text = [NSString stringWithFormat:@"+ %ld more", [self calculateRemainder]];
    /*        CGRect labelFrame = cell.sportName.frame;
            CGRect newFrame = CGRectMake(labelFrame.origin.x - 15, labelFrame.origin.y, labelFrame.size.width, labelFrame.size.height);
         //   cell.sportName.frame = newFrame;
            UILabel *myLabel = [[UILabel alloc] initWithFrame:newFrame];
            myLabel.text = [NSString stringWithFormat:@"+ %d more", [self calculateRemainder]];
            myLabel.textColor = [UIColor whiteColor];
            cell.sportName.hidden = YES;
            myLabel.font = [UIFont systemFontOfSize:12.0];
            [cell addSubview:myLabel];
*/
            cell.circleIcon.hidden = YES;
            return cell;

        } else {
            Sport *sport = [self.sportsArray objectAtIndex:indexPath.row];
            
            [cell.circleIcon makeCircleWithColor:[UIColor whiteColor] andRadius:4];
            
            cell.sportName.text = [sport.sportName capitalizedString];
            cell.circleIcon.hidden = NO;
            
            return cell;

        }
    }
   
    
    
    
    return nil;
}

- (NSInteger)calculateRemainder
{
    return [self.sportsArray count] - 1;
}


@end
