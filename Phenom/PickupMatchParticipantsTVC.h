//
//  PickupMatchParticipantsTVC.h
//  Phenom
//
//  Created by James Chung on 4/10/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomBaseTVC.h"

@interface PickupMatchParticipantsTVC : CustomBaseTVC

@property (strong, nonatomic) NSArray *playerList;
@property (strong, nonatomic) NSString *matchSport;

@end
