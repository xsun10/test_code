//
//  FindNewPlayersTVC.m
//  Phenom
//
//  Created by James Chung on 7/11/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "FindNewPlayersTVC.h"
#import "UserPreferences.h"
#import "IOSRequest.h"
#import "contact.h"
#import "S3Tools.h"
#import "UIImage+ProportionalFill.h"
#import "AFNetworking.h"
#import "FindNewPlayersCell.h"
#import "UIButton+RoundBorder.h"
#import "PlayerDetailTVC.h"
#import "UIView+Manipulate.h"
#import "UIColor+VaidenColors.h"
#import "NoResultTVC.h"

@interface FindNewPlayersTVC ()

@property (nonatomic, strong) NSMutableArray *searchPlayers;
@property (nonatomic, strong) UserPreferences *preferences;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (nonatomic) NSUInteger selectedUserRow;
@property (nonatomic) BOOL noResultsToDisplay;

@end

@implementation FindNewPlayersTVC


#define ADDRESS_BOOK_SEGMENT 0
#define SEARCH_SEGMENT 1


- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (NSMutableArray *)searchPlayers
{
    if (!_searchPlayers) _searchPlayers = [[NSMutableArray alloc] init];
    return _searchPlayers;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];

    
    self.searchBar.delegate = self;
    self.selectedUserRow = 0;
    
    // Add tap gesture to dismiss the keyboard
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard)];
    [tap setCancelsTouchesInView:NO];
    [self.tableView addGestureRecognizer:tap];

}

/* Dismiss the keyboard when user touch the screen */
- (void) dismissKeyboard
{
    [self.searchBar resignFirstResponder];
}

- (void)showSubmissionError
{
    UIAlertView *alert = [[UIAlertView alloc] init];
    alert.title = @"Submission Error";
    alert.message = @"There was a problem submitting your request.  Please try again.";
    [alert addButtonWithTitle:@"OK"];
    [alert show];
}

- (IBAction)followPlayer:(UIButton *)sender
{
    if (!self.noResultsToDisplay) {
    
        Contact *vContact = [self.searchPlayers objectAtIndex:sender.tag];
        sender.enabled = NO;
        if (vContact.followingStatus == IS_FOLLOWING_STATUS && vContact.userID != [self.preferences getUserID]) {
        [IOSRequest removeFollowConnectionWithLeader:vContact.userID
                                         andFollower:[self.preferences getUserID]
                                        onCompletion:^(NSDictionary *results) {
                                            
                                            dispatch_async(dispatch_get_main_queue(), ^{
                        
                                                sender.enabled = YES;
                                                
                                                if (![results[@"outcome"] isEqualToString:@"failure"]) {
                                                    [self.preferences setNewsfeedRefreshState:YES];
                                                    [self.preferences setChallengePlayersListRefreshState:YES];
                                                    vContact.followingStatus = NOT_FOLLOWING_STATUS;
                                                    [self.searchPlayers replaceObjectAtIndex:sender.tag withObject:vContact];
                                                    [self.tableView reloadData];
                                                } else {
                                                    [self showSubmissionError];
                                                }
                                                
                                                
                                            });
                                        }];
        } else if (vContact.followingStatus == NOT_FOLLOWING_STATUS && vContact.userID != [self.preferences getUserID]) {
        [IOSRequest makeFollowConnectionWithLeader:vContact.userID
                                       andFollower:[self.preferences getUserID]
                                      onCompletion:^(NSDictionary *results) {
                                          
                                          dispatch_async(dispatch_get_main_queue(), ^ {
                                              sender.enabled = YES;
                                              
                                              if (![results[@"outcome"] isEqualToString:@"failure"]) {
                                                  [self.preferences setNewsfeedRefreshState: YES];
                                                  [self.preferences setChallengePlayersListRefreshState:YES];
                                                  vContact.followingStatus = [results[@"follow_status"] integerValue];
                                                  [self.searchPlayers replaceObjectAtIndex:sender.tag withObject:vContact];
                                                  [self.tableView reloadData];
                                              } else {
                                                  [self showSubmissionError];
                                              }
                                          });
                                      }];

        
        }
    }
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.noResultsToDisplay)
        return 1;
    
    return [self.searchPlayers count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.noResultsToDisplay) {
        NoResultTVC *cell = [tableView dequeueReusableCellWithIdentifier:@"no_followers"];
        [cell setBackgroundColor:[UIColor clearColor]];
        cell.icon.image = [UIImage imageNamed:@"no_nearby_user"];
        [cell.icon setContentMode:UIViewContentModeCenter];
        
        cell.headline.text = @"No User Found";
        cell.message.text = @"Search again to find more users.";
        return cell;
    } else {
        static NSString *CellIdentifier = @"Player Cell";
        
        FindNewPlayersCell *cell = (FindNewPlayersCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        
        cell.textLabel.hidden = YES;
        cell.followButton.hidden = NO;
        cell.profilePic.hidden = NO;
        cell.usernameLabel.hidden = NO;
        cell.locationLabel.hidden = NO;
        Contact *vContact = nil;
        vContact = [self.searchPlayers objectAtIndex:indexPath.row];
        
        /*
         if (!vContact.followingFlag)
         cell.followButton.hidden = NO;
         else
         cell.followButton.hidden = YES;
         */
        
        //   if (vContact.contactType == CONTACT_TYPE_FOLLOWING) {
        
        cell.usernameLabel.text = vContact.userName;
        cell.locationLabel.text = vContact.location;
        
        if (vContact.followingStatus == IS_FOLLOWING_STATUS && vContact.userID != [self.preferences getUserID]) {
            cell.followButton.hidden = NO;
            //cell.followButton.backgroundColor = [UIColor newBlueLight];
            cell.followButton.enabled = YES;
            [cell.followButton setTitle:@"UnFollow" forState:UIControlStateNormal];
        } else if (vContact.followingStatus == NOT_FOLLOWING_STATUS && vContact.userID != [self.preferences getUserID]){
            cell.followButton.hidden = NO;
            //cell.followButton.backgroundColor = [UIColor newBlueLight];
            cell.followButton.enabled = YES;
            [cell.followButton setTitle:@"Follow" forState:UIControlStateNormal];
        } else if (vContact.followingStatus == PENDING_FOLLOWING_STATUS) {
            cell.followButton.hidden = NO;
            //cell.followButton.backgroundColor = [UIColor lightGrayColor];
            [cell.followButton setTitle:@"Pending" forState:UIControlStateNormal];
            cell.followButton.enabled = NO;
        } else {
            cell.followButton.hidden = YES;
        }
        
        cell.followButton.tag = indexPath.row;
        
        NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                baseString:vContact.profileBaseString
                         ];
        
        [cell.profilePic setImageWithURL:[NSURL URLWithString:url]
                        placeholderImage:[UIImage imageNamed:@"avatar square"]];
        
        [UIImage makeRoundedImageNoBorder:cell.profilePic withRadius:10];
        
        [cell.followButton makeRoundedBorderWithRadius:3];
        
        return cell;
    }
}


- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    
    [self.searchPlayers removeAllObjects];
  //  [self.searchBar resignFirstResponder];
    
//    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    HUD.labelText = @"Loading";
    
    [IOSRequest searchForUser:searchBar.text
            notAssociatedWith:[self.preferences getUserID] onCompletion:^(NSMutableArray *resultArray) {
                
                for (id object in resultArray) {
                    
                    Contact *contact = [[Contact alloc] initWithContactDetails:[object[@"user_id"] integerValue]
                                                                 andProfileStr:object[@"profile_pic_string"]
                                                                   andUserName:object[@"username"]
                                                                   andFullName:object[@"fullname"]
                                                                   andLocation:object[@"location"]
                                                                      andAbout:object[@"about"]
                                                                      andCoins:[object[@"coins"] floatValue]
                                                                   contactType:CONTACT_TYPE_SEARCH
                                                                 followingStatus:[object[@"follow_status"] integerValue]];
                    
                    
                    [self.searchPlayers addObject:contact];
                    
                   
                }
                
                dispatch_async(dispatch_get_main_queue(), ^{
                //    if ([self.searchPlayers count] == [resultArray count]) {
                    
                    if ([self.searchPlayers count] == 0) {
                        self.noResultsToDisplay = YES;
                        self.tableView.scrollEnabled = NO;
                    } else {
                        self.tableView.scrollEnabled = YES;
                        self.noResultsToDisplay = NO;
                    }
                    [self.tableView reloadData];
                    [searchBar resignFirstResponder];
                        //                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                        
                 //   }
                });
                
  
                
            }];
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!self.noResultsToDisplay) {
        self.selectedUserRow = indexPath.row;
        [self performSegueWithIdentifier:@"Player Detail Segue" sender:self];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Player Detail Segue"]) {
        PlayerDetailTVC *controller = segue.destinationViewController;
        
        Contact *vContact = [self.searchPlayers objectAtIndex:self.selectedUserRow];
        
        controller.playerUserID = vContact.userID;
        controller.profilePicString = vContact.profileBaseString;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.noResultsToDisplay) {
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        return screenRect.size.height - 150;
    }
    return 70.0;
}

@end
