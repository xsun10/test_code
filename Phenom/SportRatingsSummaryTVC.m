//
//  SportRatingsSummaryTVC.m
//  Vaiden
//
//  Created by James Chung on 10/4/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "SportRatingsSummaryTVC.h"
#import "SportRatingsSummaryCell.h"
#import "UserPreferences.h"
#import "IOSRequest.h"
#import "AvgSportRatingSingleAttribute.h"
#import "UIButton+RoundBorder.h"
#import "UIColor+VaidenColors.h"
#import "SportRatingsListTVC.h"
#import "AFNetworking.h"
#import "S3Tools.h"
#import "UIImage+ProportionalFill.h"
#import "SportRatingsSummaryHeaderCell.h"
#import "UIView+Manipulate.h"

@interface SportRatingsSummaryTVC ()



@property (nonatomic, strong) UserPreferences *preferences;
@property (nonatomic, strong) NSMutableArray *sportRatings;
@property (nonatomic) BOOL noResultsToDisplay;
@property (nonatomic) NSUInteger selectedRow;

@property (nonatomic, strong) UIView *tempView;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;


@end

@implementation SportRatingsSummaryTVC

- (NSMutableArray *)sportRatings
{
    if (!_sportRatings) _sportRatings = [[NSMutableArray alloc] init];
    return _sportRatings;
}

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];

//    [self showTempSpinner];
 //   [self setPageValues];
    
    

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
//    if ([self.preferences getSportRatingsSummaryPageRefreshState]) {
        [self showTempSpinner];
        [self setPageValues];
        [self.preferences setSportRatingsSummaryPageRefreshState:NO];
        [self.preferences setProfilePageRefreshState:YES];
//    }
}


- (void)showTempSpinner
{
    self.tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    self.tempView.backgroundColor = [UIColor lightGrayColor];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] init];
    [self.activityIndicator setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width/2.0, [UIScreen mainScreen].bounds.size.height/2.0 - 50)];
    [self.activityIndicator startAnimating];
    [self.tempView addSubview:self.activityIndicator];
    
    [self.view addSubview:self.tempView];
    
}

- (void)setPageValues
{
    self.navigationItem.title = [NSString stringWithFormat:@"%@ Summary", [self.sport.sportName capitalizedString]];

    
    
    [IOSRequest getSportRatingsForUser:self.playerToBeRated.userID
                              forSport:self.sport.sportID
                          onCompletion:^(NSMutableArray *results) {
                              [self.sportRatings removeAllObjects];
                              for (id object in results) {
                                  AvgSportRatingSingleAttribute *avgRating = [[AvgSportRatingSingleAttribute alloc] initWithAvg:[object[@"avg_rating"] floatValue]
                                                                                   withNumRatings:[object[@"num_ratings"] integerValue]
                                                                              forSportRatingsType:object[@"sport_ratings_description"]
                                                                           withSportRatingsTypeID:[object[@"sport_ratings_type_id"] integerValue]];
                                  
                                  [self.sportRatings addObject:avgRating];
                              }
                              
                              dispatch_async(dispatch_get_main_queue(), ^ {
                                  
                                  if ([self.sportRatings count] == 0) {
                                      self.noResultsToDisplay = YES;
                                     
                                 

                                  } else {
                                      self.noResultsToDisplay = NO;
                                     
                                  }
                                  [self.tableView reloadData];
                                  [self.activityIndicator stopAnimating];
                                  self.tempView.hidden = YES;
                                  
                              });
                              
                          }];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.noResultsToDisplay)
        return 2;
    
    return [self.sportRatings count] + 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.000000000001;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        
        if ([self.preferences getUserID] == self.playerToBeRated.userID) {
            return 245.0; // 220
        } else {
            return 315.0; // 284
        }
    } else if (indexPath.row >= 1) {
        return 59.0;
    }
    return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
       static NSString *CellIdentifier = @"Header Cell";
        SportRatingsSummaryHeaderCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        
        cell.username.text = self.playerToBeRated.userName;
        cell.sportName.text = [self.sport.sportName capitalizedString];
        cell.sportIcon.image = [[self.sport getSportIcon] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [cell.sportIcon setTintColor:[UIColor peacock]];
        cell.sportLevel.text = [NSString stringWithFormat:@"Level %ld", [self.sport.level integerValue]];
        [cell.ratePlayerButton makeRoundedBorderWithRadius:3];
        
        NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                baseString:self.playerToBeRated.profileBaseString];
        
        [cell.profilePic setImageWithURL:[NSURL URLWithString:url]
                        placeholderImage:[UIImage imageNamed:@"avatar round"]];
        
        [UIImage makeRoundedImage:cell.profilePic withRadius:30];
        
    //    cell.bulbIcon.image = [[UIImage imageNamed:@"Bulb Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    //    [cell.bulbIcon setTintColor:[UIColor salmonColor]];
        
     //   cell.messageLabel.textColor = [UIColor salmonColor];
        
        
        // Should not be able to rate player if session user is same person as target user
        if ([self.preferences getUserID] == self.playerToBeRated.userID) {
            cell.ratePlayerButton.hidden = YES;
        } else {
            cell.ratePlayerButton.hidden = NO;
        }
        return cell;
        
    } else if (indexPath.row >= 1) {
    
        static NSString *CellIdentifier = @"Rating Cell";
        SportRatingsSummaryCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
        if (self.noResultsToDisplay) {
            
            if ([self.preferences getUserID] == self.playerToBeRated.userID)
                cell.textLabel.text = @"No one has rated you yet";
            else
                cell.textLabel.text = @"No Ratings.  Be the first to rate this player.";
            
            cell.textLabel.textAlignment = NSTextAlignmentCenter;
            cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-LightItalic" size:13];
            cell.skillLabel.hidden = YES;
            cell.starIcons.hidden = YES;
            cell.numRatings.hidden = YES;
            cell.backgroundColor = [UIColor clearColor];
            tableView.separatorColor = [UIColor clearColor];
            return cell;
        }
    
        AvgSportRatingSingleAttribute *avgRating = [self.sportRatings objectAtIndex:indexPath.row - 1];
        cell.skillLabel.text = [avgRating.sportRatingsType capitalizedString];
        cell.starIcons.image = [avgRating getRatingImage];
    
        if (avgRating.numRatings == 1) {
            cell.numRatings.text = [NSString stringWithFormat:@"%lu Rating", avgRating.numRatings];
        } else {
            cell.numRatings.text = [NSString stringWithFormat:@"%lu Ratings", avgRating.numRatings];
        }
    
        return cell;
    }
    return nil;
}


/*
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *sectionName;
    
    switch (section) {
        case 0:
            sectionName = NSLocalizedString(@"Player Skills", @"Player Skills");
            break;
            }
    
    return sectionName;
}*/

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Rate Segue"]) {
        RatePlayerSportTVC *controller = segue.destinationViewController;
        controller.sportID = self.sport.sportID;
        controller.targetUserID = self.playerToBeRated.userID;
        controller.delegate = self;
        
    } else if ([segue.identifier isEqualToString:@"Rating Detail Segue"]) {
        SportRatingsListTVC *controller = segue.destinationViewController;
        AvgSportRatingSingleAttribute *sportRating = [self.sportRatings objectAtIndex:self.selectedRow];
        controller.ratedPlayerID = self.playerToBeRated.userID;
        controller.sportRatingsTypeID = sportRating.sportRatingsTypeID;
        
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (!self.noResultsToDisplay && indexPath.row != 0) {
        self.selectedRow = indexPath.row - 1;
        [self performSegueWithIdentifier:@"Rating Detail Segue" sender:self];
    }
}

- (void)setRatePlayerSportViewController:(RatePlayerSportTVC *)controller withRefresh:(BOOL)refreshBool
{
    if (refreshBool)
        [self setPageValues];
}
- (IBAction)ratePlayer:(id)sender
{
    [self performSegueWithIdentifier:@"Rate Segue" sender:self];
}

@end
