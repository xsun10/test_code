//
//  PostUserContentVC.m
//  Phenom
//
//  Created by James Chung on 5/20/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "PostUserContentVC.h"
#import "UserPreferences.h"
#import "IOSRequest.h"
#import "AFNetworking.h"
#import "Constants.h"
#import "Response.h"
#import "AmazonKeyChainWrapper.h"
#import "AmazonClientManager.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "S3Tools.h"
#import "UIImage+ProportionalFill.h"


@interface PostUserContentVC ()  <UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *profilePic;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (nonatomic, strong) UserPreferences *preferences;

@property (weak, nonatomic) UIActionSheet *profilePicActionSheet;
@property (strong, nonatomic)UIPopoverController *imagePickerPopover;

@property (weak, nonatomic) IBOutlet UILabel *uploadImageText;
@property (weak, nonatomic) IBOutlet UIImageView *uploadImagePreview;

@property (nonatomic, strong) NSData *imageData;
@property (nonatomic, strong) NSString *dataType;
@property (nonatomic, strong) NSString *fileExtension;

@property (nonatomic, strong) UIBarButtonItem *doneButton;
@end

@implementation PostUserContentVC

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //[self setDoneButton];
    self.textView.delegate = self;
 //   [self setPlaceholderText];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.textView becomeFirstResponder];
    self.uploadImageText.hidden = YES;
    
    UIToolbar *toolbar = [[UIToolbar alloc] init];
    [toolbar setBarStyle:UIBarStyleBlackTranslucent];
    [toolbar sizeToFit];
    
    UIButton* starButt =[UIButton buttonWithType:UIButtonTypeCustom];
    UIImage* buttonImage = [[UIImage imageNamed:@"Camera Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [starButt addTarget:self action:@selector(choosePic)
       forControlEvents:UIControlEventTouchUpInside];
    [starButt setImage:buttonImage forState:UIControlStateNormal];
    starButt.frame = CGRectMake(0, 0, 35, 35);
    [starButt setTintColor:[UIColor whiteColor]];
    UIBarButtonItem *cameraButton = [[UIBarButtonItem alloc]initWithCustomView:starButt];
    
    
    
//    UIBarButtonItem *cameraButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Camera Icon"]
  //                                                                   style:UIBarButtonItemStylePlain
    //                                                                target:self
      //                                                              action:@selector(choosePic)];
    
    
    /*
    UIBarButtonItem *reelButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reel_20.png"]
                                                                     style:UIBarButtonItemStylePlain
                                                                    target:self
                                                                    action:@selector(chooseHighlightReel)];
    UIBarButtonItem *trickButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"pins_20.png"]
                                                                     style:UIBarButtonItemStylePlain
                                                                    target:self
                                                                    action:@selector(chooseTrickShot)];
    */
    UIBarButtonItem *fixedSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedSpace.width = 15;
    
    
    UIBarButtonItem *flexButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];

 //   NSArray *itemsArray = @[flexButton, cameraButton, fixedSpace, reelButton, fixedSpace, trickButton];
      NSArray *itemsArray = @[flexButton, cameraButton, fixedSpace];
  
    [toolbar setItems:itemsArray];
    
    [self.textView setInputAccessoryView:toolbar];
    
    NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                            baseString:[self.preferences getProfilePicString]];
        
    [self.profilePic setImageWithURL:[NSURL URLWithString:url]
                    placeholderImage:[UIImage imageNamed:@"avatar round"]];
    
    [UIImage makeRoundedImage:self.profilePic withRadius:25];
}


/*- (void)setDoneButton
{
    UIButton *btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 50, 21);
    btn.layer.borderColor = [UIColor whiteColor].CGColor;
    btn.layer.borderWidth = 1.0;
    
    [btn addTarget:self action:@selector(doneAction:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundColor:[UIColor clearColor]];
    [btn setTitle:@"DONE" forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-MediumItalic" size:12.0]];
    
    // Initialize UIBarbuttonitem...
    self.doneButton = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = self.doneButton;
}*/





- (void)setPlaceholderText
{
    self.textView.textColor = [UIColor lightGrayColor];
    [self.textView becomeFirstResponder];
    self.textView.text = @"Write Something...";
    
    
    // Count the characters on screen
    NSMutableString *numOfChar = [self.textView.text mutableCopy];
    
    // Skip that many characters to the left
    self.textView.selectedRange = NSMakeRange(self.textView.selectedRange.location-[numOfChar length], 0);
}

- (IBAction)doneAction:(id)sender
{
    [self.textView resignFirstResponder];

    if (([self.textView.text length] == 0 || [self.textView.text isEqualToString:@"Write Something..."]) &&
        self.imageData == nil) { // no text input and not image / video set
        [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
        self.doneButton.enabled = YES;

        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Comment Posting Error";
        alert.message = @"Sorry but we cannot submit a blank comment. Please try again.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
    } else if ([self.textView.text length] > 200) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Comment Posting Error";
        alert.message = @"Sorry but your comment cannot exceed 200 characters in length.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
    } else if (self.imageData == nil) {
        HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        HUD.labelText = @"Saving";
        
        
        self.doneButton.enabled = NO;
        [IOSRequest saveNewsfeedTextPost:self.textView.text
                                 forUser:[self.preferences getUserID]
                            onCompletion:^(NSDictionary *results) {
                                dispatch_async(dispatch_get_main_queue(), ^ {
                                    self.doneButton.enabled = YES;
                                    
                                    [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
                                    
                                    if ([results[@"limit"] isEqualToString:@"exceeded"]) {
                                        UIAlertView *alert = [[UIAlertView alloc] init];
                                        alert.title = @"Whoa there!";
                                        alert.message = @"You've reached your posting limit for today.  Please try again tomorrow.";
                                        [alert addButtonWithTitle:@"OK"];
                                        [alert show];
                                    } else {
                                        [self.preferences setNewsfeedRefreshState:YES];
                                        [self.navigationController popViewControllerAnimated:YES];
                                    }
                                    
                                });
                                
                                
                            }];
        double delayInSeconds = 10.0;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            //code to be executed on the main queue after delay
            self.doneButton.enabled = YES;
            [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
        });
        
    } else if (![AmazonClientManager hasCredentials]) {
        [[Constants credentialsAlert] show];
        NSLog(@"no credentials");
        self.doneButton.enabled = YES;
        [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
    }
    else {
        
        NSLog (@"has credentials");

        operationQueue = [NSOperationQueue new];
        operationQueue.maxConcurrentOperationCount = 3;
        
        
        NSString *fileNameBase = [S3Tools getFileNameBaseStringWithType:self.dataType forUser:[self.preferences getUserID]];
        S3Tools *imageUploader1 = [[S3Tools alloc] initWithData:self.imageData
                                                             forUser:[self.preferences getUserID]
                                                           dataType:self.dataType
                                               andBaseFileNameString:fileNameBase
                                                   andTextPostString:self.textView.text
                                                     andFileExt:self.fileExtension];

        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(newsfeedPicDidFinishUploading:)
                                                     name:@"NewsfeedImageDidFinishUploadingNotification"
                                                   object:nil];
        [operationQueue addOperation:imageUploader1];
        [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
        self.doneButton.enabled = YES;

        
        
        
    }
}









- (void) newsfeedPicDidFinishUploading:(NSNotification*)notification {
    [[NSNotificationCenter defaultCenter]
     removeObserver:self
     name:@"NewsfeedImageDidFinishUploadingNotification"
     object:nil];
    
    [self.preferences setNewsfeedRefreshState:YES];
    [self.navigationController popViewControllerAnimated:YES];
}



#define TITLE_OF_ACTIONSHEET @"Make an image post"
#define CANCEL_BUTTON_ACTIONSHEET @"Cancel "
#define FROM_CAMERA @"Take a pic"
#define FROM_LIBRARY @"Choose From Photo Library"

- (void)choosePic
{
    if (!self.profilePicActionSheet) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                      initWithTitle:TITLE_OF_ACTIONSHEET delegate:self
                                      cancelButtonTitle:CANCEL_BUTTON_ACTIONSHEET destructiveButtonTitle:nil
                                      otherButtonTitles:FROM_CAMERA, FROM_LIBRARY, nil];
        [actionSheet showFromTabBar:self.tabBarController.tabBar];
        
        
        self.profilePicActionSheet = actionSheet;
    }
    
}

- (void)chooseHighlightReel
{
    [self performSegueWithIdentifier:@"highlight reel segue" sender:self];
}

- (void)chooseTrickShot
{
    [self performSegueWithIdentifier:@"trick shot segue" sender:self];
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == actionSheet.destructiveButtonIndex) {
        [self.view.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    } else {
        NSString *choice = [actionSheet buttonTitleAtIndex:buttonIndex];
        
        if ([choice isEqualToString:FROM_CAMERA]) {
            [self changePicFromCamera];
        } else if ([choice isEqualToString:FROM_LIBRARY]){
            [self changePicFromPhotoLibrary];
        }
    }
}

- (void)changePicFromCamera
{
    [self presentImagePicker:UIImagePickerControllerSourceTypeCamera];
}


- (void) changePicFromPhotoLibrary
{
    [self presentImagePicker:UIImagePickerControllerSourceTypeSavedPhotosAlbum];
}

- (void) presentImagePicker:(UIImagePickerControllerSourceType)sourceType
{
    if ([UIImagePickerController isSourceTypeAvailable:sourceType]) {
        NSArray *availableMediaTypes = [UIImagePickerController availableMediaTypesForSourceType:sourceType];
        if ([availableMediaTypes containsObject:(NSString *)kUTTypeImage]) {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.sourceType = sourceType;
            picker.mediaTypes = @[(NSString *)kUTTypeImage];
            picker.allowsEditing = YES;
            picker.delegate = self;
            
            // present the picker
            
            //    if ((sourceType != UIImagePickerControllerSourceTypeCamera) && (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)) {
            //       self.imagePickerPopover = [[UIPopoverController alloc] initWithContentViewController:picker];
            //        [self.imagePickerPopover presentPopoverFromBarButtonItem:<#(UIBarButtonItem *)#> permittedArrowDirections:<#(UIPopoverArrowDirection)#> animated:<#(BOOL)#>]
            //    }
            [self presentViewController:picker animated:YES completion:nil];
        }
    }
}

- (void) imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

#define MAX_IMAGE_WIDTH 300 // might want to change this to height....

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = info[UIImagePickerControllerEditedImage];
 
    NSString *path = [NSTemporaryDirectory() stringByAppendingPathComponent:@"upload-image.tmp"];
    NSData *imageData = UIImageJPEGRepresentation(image, 1);
    //you can also use UIImageJPEGRepresentation(img,1); for jpegs
    [imageData writeToFile:path atomically:YES];
    
    
   if (!image) image = info[UIImagePickerControllerOriginalImage];
    if (image) {
   
        
        UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
        CGRect frame = imageView.frame;
        if (frame.size.width > MAX_IMAGE_WIDTH) {
            frame.size.height = (frame.size.height / frame.size.width) * MAX_IMAGE_WIDTH;
            frame.size.width = MAX_IMAGE_WIDTH;
        }
        imageView.frame = frame;
        //       [self.setRandomLocationForView:imageView];
        //       [self.changeImageButton.imageView addSubview:imageView];
        self.uploadImagePreview.image = imageView.image;
        self.uploadImageText.text = @"Upload to upload";
        self.uploadImageText.hidden = NO;
              
        self.imageData = UIImageJPEGRepresentation(imageView.image, 1.0);
        self.dataType = @"photo";
        self.fileExtension = @"jpg";
        
    }
    if (self.imagePickerPopover) {
        [self.imagePickerPopover dismissPopoverAnimated:YES];
        self.imagePickerPopover = nil;
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
  
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"highlight reel segue"]) {
        ChooseHighlightReelVC *controller = (ChooseHighlightReelVC *)segue.destinationViewController;
        controller.delegate = self;
    } if ([segue.identifier isEqualToString:@"trick shot segue"]) {
        ChooseTrickShotVC *controller = (ChooseTrickShotVC *)segue.destinationViewController;
        controller.delegate = self;
    }
}

-(void)setWithHighlighVC:(ChooseHighlightReelVC *)controller withVideoData:(NSData *)vidData andThumbnail:(UIImage *)vidThumbnail andFileExt:(NSString *)fileExt
{
    self.uploadImagePreview.image = vidThumbnail;
    self.imageData = vidData;
    self.dataType = @"highlight_video";
    self.fileExtension= fileExt;
}

- (void)setWithTrickShotVideoVC:(ChooseTrickShotVC *)controller withVideoData:(NSData *)vidData andThumbnail:(UIImage *)vidThumbnail andFileExt:(NSString *)fileExt
{
    self.uploadImagePreview.image = vidThumbnail;
    self.imageData = vidData;
    self.dataType = @"trick_video";
    self.fileExtension = fileExt;
}



@end
