//
//  ConfirmIndMatchScoreSuccessVC.h
//  Vaiden
//
//  Created by James Chung on 8/1/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomBaseVC.h"

@interface ConfirmIndMatchScoreSuccessVC : CustomBaseVC

//@property (nonatomic) float sessionUserOldCoins;
//@property (nonatomic) float sessionUserNewCoins;
//@property (nonatomic) float sessionUserNewLevel;
//@property (nonatomic) float sessionUserOldLevel;
//@property (nonatomic, strong) NSString *sportName;

// New version
@property (nonatomic) NSUInteger matchID;

@end
