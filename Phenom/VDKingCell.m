//
//  VDKingCell.m
//  Vaiden
//
//  Created by James Chung on 3/3/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "VDKingCell.h"
#import "VDKingCVCell.h"
#import "Player.h"
#import "S3Tools.h"
#import "AFNetworking.h"
#import "UIImage+ProportionalFill.h"
#import "UIColor+VaidenColors.h"

@implementation VDKingCell

- (void)initializeKingsCell
{
 /*   if ([self.kingsArray count] == 0) {
        UILabel *kingMissingLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.kingsCollectionView.frame.origin.x, self.kingsCollectionView.frame.origin.y + 30, self.kingsCollectionView.frame.size.width, 30)];
        kingMissingLabel.textColor = [UIColor lightGrayColor];
        kingMissingLabel.font = [UIFont fontWithName:@"HelveticaNeue-LightItalic" size:15];
        kingMissingLabel.text = @"There is no king for this venue";
        kingMissingLabel.textAlignment = NSTextAlignmentCenter;
        
        [self.contentView addSubview:kingMissingLabel];
        
        self.kingsPageControl.hidden = YES;
        self.kingsCollectionView.hidden = YES;
        
    } else {*/
        self.kingsCollectionView.delegate = self;
        self.kingsCollectionView.dataSource = self;
    
        self.kingsPageControl.numberOfPages = [self.kingsArray count];
//    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.kingsArray count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    VDKingCVCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"My Cell" forIndexPath:indexPath];
    
    if ([collectionView subviews]){
        for (UIView *subview in [collectionView subviews]) {
            if (subview.tag == 1)
                [subview removeFromSuperview];
            
        }
    }
    
    Player *king = [self.kingsArray objectAtIndex:indexPath.row];
    
    // there should be something in the array
    PlayerSport *kingSport = [king.playerSports objectAtIndex:0];
    
    
    NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ is King Of This Venue for %@", king.userName, [kingSport.sportName capitalizedString]]];
    
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor goldColor] range:NSMakeRange(0, [king.userName length])];
    cell.additionalTextLabel.attributedText = string;

    
//    cell.additionalTextLabel.text = [NSString stringWithFormat:@"is King Of This Venue for %@", [kingSport.sportName capitalizedString]];
    
 //   cell.usernameLabel.text = king.userName;
    
    NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                            baseString:king.profileBaseString];
    
    [cell.profilePic setImageWithURL:[NSURL URLWithString:url]
                  placeholderImage:[UIImage imageNamed:@"avatar square"]];
    
    [UIImage makeRoundedImage:cell.profilePic withRadius:10];
    [cell.profilePic.layer setBorderColor:[UIColor whiteColor].CGColor];
    [cell.profilePic.layer setBorderWidth:2.0f];
    
    UITapGestureRecognizer *pgr = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self action:@selector(handleTapOnProfilePic:)];
    cell.profilePic.userInteractionEnabled = YES;
    [cell.profilePic addGestureRecognizer:pgr];
    
    
    
    return cell;
}

- (void)handleTapOnProfilePic:(UITapGestureRecognizer *)tapGestureRecognizer
{
    VDKingCVCell *cell = (VDKingCVCell *)tapGestureRecognizer.view.superview.superview;
    NSIndexPath *selectedIndexPath = [self.kingsCollectionView indexPathForCell:cell];
    
    Player *theKing = [self.kingsArray objectAtIndex:selectedIndexPath.row];
  //  PlayerSport *kingSport = [theKing.playerSports objectAtIndex:0];

    [self.delegate setWithUserFromVC:self withUser:theKing.userID];
  

}

@end
