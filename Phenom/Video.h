//
//  Video.h
//  Phenom
//
//  Created by James Chung on 6/4/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Video : NSObject

@property (nonatomic, strong) NSString *videoBaseFileString;
@property (nonatomic, strong) UIImage *thumbnailImage;
@property (nonatomic, strong) NSString *videoType;

- (Video *)initWithVideoDetails:(NSString *)videoBaseFileString
             withThumbnailImage:(UIImage *)thumbnailImage
                   andVideoType:(NSString *)videoType;

@end
