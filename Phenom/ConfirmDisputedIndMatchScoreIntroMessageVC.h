//
//  ConfirmDisputedIndMatchScoreIntroMessageVC.h
//  Vaiden
//
//  Created by James Chung on 8/5/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IndividualMatch.h"
#import "CustomBaseVC.h"

@interface ConfirmDisputedIndMatchScoreIntroMessageVC : CustomBaseVC

@property (nonatomic, strong) IndividualMatch *individualMatch;

@end
