//
//  VideoPost.h
//  Phenom
//
//  Created by James Chung on 5/29/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "News.h"
#import "Video.h"

@interface VideoPost : News

/*
@property (nonatomic, strong) NSString *videoBaseFileString;
@property (nonatomic, strong) UIImage *thumbnailImage;
@property (nonatomic, strong) NSString *videoType;
 */
@property (nonatomic, strong) Video *vid;

- (VideoPost *)initWithNewsDetails:(NSInteger)newsMakerUserID
                       andUserName:(NSString *)newsMakerUserName
                     andProfileStr:(NSString *)profileBaseString
                          withDate:(NSDate *)newsDate
                       andPostType:(NSInteger)postType
                         andNewsID:(NSInteger)newsID
                       andTextPost:(NSString *)textPost
                andVideoFileString:(NSString *)videoBaseFileString
                 andThumbnailImage:(UIImage *)thumbnailImage
                      andVideoType:(NSString *)videoType
                    andNumComments:(NSUInteger)numComments
                     andNumRemixes:(NSUInteger)numRemixes;

@end
