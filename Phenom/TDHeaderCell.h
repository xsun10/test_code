//
//  TDHeaderCell.h
//  Vaiden
//
//  Created by James Chung on 4/7/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TDHeaderCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *teamLogo;
@property (weak, nonatomic) IBOutlet UILabel *teamNameLabel;

@end
