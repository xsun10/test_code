//
//  MatchNotificationsNewMessageCell.h
//  Vaiden
//
//  Created by James Chung on 12/29/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MatchNotificationsNewMessageCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;

@property (weak, nonatomic) IBOutlet UILabel *messageLabel;

@end
