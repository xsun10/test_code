//
//  AvgSportRatingAllAttributes.h
//  Vaiden
//
//  Created by James Chung on 10/7/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AvgSportRatingAllAttributes : NSObject

@property (nonatomic) float avgRating;
@property (nonatomic) NSUInteger numRatings;

-(AvgSportRatingAllAttributes *)initWithAvg:(float)avgRating
                             withNumRatings:(NSUInteger)numRatings;
@end
