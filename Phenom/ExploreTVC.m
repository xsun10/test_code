//
//  ExploreTVC.m
//  Vaiden
//
//  Created by James Chung on 4/22/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "ExploreTVC.h"
#import "UserPreferences.h"
#import "IOSRequest.h"
#import "S3Tools.h"
#import "UIImage+ProportionalFill.h"
#import "Contact.h"
#import "ExploreTVCell.h"
#import "AFNetworking.h"
#import "PlayerDetailTVC.h"
#import "UIColor+VaidenColors.h"
#import "UIButton+RoundBorder.h"
#import "UIView+Manipulate.h"


@interface ExploreTVC () <UISearchBarDelegate, UITextFieldDelegate>


@property (weak, nonatomic) IBOutlet UIView *sportIconBackground;
@property (weak, nonatomic) IBOutlet UIImageView *sportIcon;
@property (nonatomic, strong) NSMutableArray *nearbyPlayers;
@property (nonatomic, strong) UserPreferences *preferences;
@property (nonatomic) NSInteger filterSportID;
@property (nonatomic) NSInteger proximityRange;
@property (weak, nonatomic) IBOutlet UILabel *sportNameLabel;
//@property (strong, nonatomic) UIView *fixedMenu;
@property (nonatomic) NSInteger orderByType;
@property (nonatomic) NSUInteger selectedRow;

@property (nonatomic) BOOL noResultsToDisplay;
@property (nonatomic, strong) UIView *tempView;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
//@property (strong, nonatomic) UIButton *sportButtonInHiddenFixedMenu;

@property (nonatomic, strong) NSMutableArray *nearbyPlayersWithZeroPlayedMatches;
@property (nonatomic, strong) UIView *overlay;

@property (weak, nonatomic) IBOutlet UISearchBar *searchUsernameBar;

@property (nonatomic) BOOL isSearch;
@property (strong, nonatomic) NSString *sportFilterName;

@property (strong, nonatomic) UIView * loadingView;
@property (strong, nonatomic) NSTimer *pauseTimer;
@end

@implementation ExploreTVC


- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (NSMutableArray *)nearbyPlayers
{
    if (!_nearbyPlayers) _nearbyPlayers = [[NSMutableArray alloc] init];
    return _nearbyPlayers;
}

- (NSMutableArray *)nearbyPlayersWithZeroPlayedMatches
{
    if (!_nearbyPlayersWithZeroPlayedMatches) _nearbyPlayersWithZeroPlayedMatches = [[NSMutableArray alloc] init];
    return _nearbyPlayersWithZeroPlayedMatches;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
  
 
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];

    self.filterSportID = 1;
    self.selectedRow = 0;
    self.sportNameLabel.hidden = NO;
    self.isSearch = NO;

    //[self setDoneButton];
    
    // Setting up the search bar
    self.searchUsernameBar.delegate = self;
    
    // Add tap gesture to dismiss the keyboard
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard)];
    [tap setCancelsTouchesInView:NO];
    [self.tableView addGestureRecognizer:tap];
    
    self.loadingView = [[UIView alloc] initWithFrame:CGRectMake(0, 85, 320, [UIScreen mainScreen].bounds.size.height - 85)];
    self.loadingView.backgroundColor = [UIColor lightGrayColor];
    self.loadingView.alpha = 0.5;
    [self.loadingView setHidden:YES];
    
    UIActivityIndicatorView * tmpIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(155, 150, 10, 10)];
    [tmpIndicator startAnimating];
    [tmpIndicator setColor:[UIColor blackColor]];
    
    [self.loadingView addSubview:tmpIndicator];
    [self.tableView addSubview:self.loadingView];
    
    [self.searchUsernameBar setAutocorrectionType:UITextAutocorrectionTypeNo]; // disable auto correction for the UISearchBar
    
    if (self.fromDashboard) {
        // set back button
        self.navigationItem.title = @"DISCOVER";
    }
}

/* Dismiss the keyboard when user touch the screen */
- (void) dismissKeyboard
{
    [self.searchUsernameBar resignFirstResponder];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (self.tempView) {
        self.tempView.hidden = YES;
        self.tempView = nil;
    }
    
    //   [self.sportButtonInHiddenFixedMenu makeCircleWithColorAndBackground:[UIColor almostBlack] andRadius:5];
    
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Location Services Are Off";
        alert.message = @"Please turn on Location Services in your Phone Settings to use this feature";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
        self.noResultsToDisplay = YES;
        self.sportNameLabel.hidden = YES;
    } else {
        [self showTempSpinner];
        self.noResultsToDisplay = YES;
        
        //        self.orderByType = ORDER_BY_LEVEL;
        
        
        //    [self.playersTableView setContentInset:UIEdgeInsetsMake(35,0,150,0)];
        
        [self initLocation];
        [locationManager startUpdatingLocation];
        
        
        
    }
    
}

/*- (void)setDoneButton
{
    UIButton *btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 50, 21);
    [btn makeRoundedBorderWithRadius:3];
    //btn.layer.borderColor = [UIColor whiteColor].CGColor;
    //btn.layer.borderWidth = 1.0;
    
    [btn addTarget:self action:@selector(filterAction:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundColor:[UIColor clearColor]];
    [btn setTitle:@"Filter" forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Thin" size:15]];
    
    // Initialize UIBarbuttonitem...
    UIBarButtonItem *proe = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = proe;
}*/

/*- (void)filterAction:(UIButton *)sender
{
    [self performSegueWithIdentifier:@"Choose Sport Segue" sender:self];
}*/

- (IBAction)filterClicked:(UIBarButtonItem *)sender {
    [self performSegueWithIdentifier:@"Choose Sport Segue" sender:self];
}

- (void)showTempSpinner
{
    self.tableView.scrollEnabled = NO;
    self.tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    self.tempView.backgroundColor = [UIColor lightGrayColor];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] init];
    [self.activityIndicator setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width/2.0, [UIScreen mainScreen].bounds.size.height/2.0 )];
    [self.activityIndicator startAnimating];
    [self.tempView addSubview:self.activityIndicator];
    
    [self.view addSubview:self.tempView];
    
}

- (void)displayErrorOverlay
{
    [self.overlay removeFromSuperview];
    
    self.overlay = [[UIView alloc] initWithFrame:CGRectMake(0, 85, 320, [UIScreen mainScreen].bounds.size.height - 85)];
    self.overlay.backgroundColor = [UIColor colorWithRed:239/255.0 green:239/255.0 blue:244/255.0 alpha:1.0];
    self.overlay.tag = 1;
    
    UIImage *img = [UIImage imageNamed:@"no_nearby_user"];
    UIImageView *icon = [[UIImageView alloc] initWithImage:img];
    [icon setFrame:CGRectMake(140, 50, 50, 50)];
    [icon setContentMode:UIViewContentModeCenter];
    
    UILabel *firstLine = [[UILabel alloc] initWithFrame:CGRectMake(30, 130, 260, 21)];
  //  firstLine.textColor = [UIColor colorWithRed:59/255 green:59/255 blue:59/255 alpha:1.0];
    firstLine.textColor = [UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:1.0];
    firstLine.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:15];
    if (self.isSearch == YES) {
        firstLine.text = @"No Users Found";
    } else {
        firstLine.text = @"No Players Nearby";
    }
    
    [firstLine setTextAlignment:NSTextAlignmentCenter];
    
    UILabel *secondLine = [[UILabel alloc] initWithFrame:CGRectMake(25, 150, 280, 21)];
    secondLine.textColor = [UIColor colorWithRed:170/255.0 green:170/255.0 blue:170/255.0 alpha:1.0];
    secondLine.font = [UIFont fontWithName:@"HelveticaNeue" size:13];
    if (self.isSearch == YES) {
        secondLine.text = @"Search again to find more users.";
    } else {
        secondLine.text = @"Press filter to find more users.";
    }
    secondLine.numberOfLines = 0;
    [secondLine setTextAlignment:NSTextAlignmentCenter];
    
    [self.overlay addSubview:icon];
    [self.overlay addSubview:firstLine];
    [self.overlay addSubview:secondLine];
    
    [self.tableView addSubview:self.overlay];
    
    /* Set the title */
    if (self.isSearch == YES) {
        self.sportNameLabel.text = @"Search for Players";
    } else {
        self.sportNameLabel.text = [NSString stringWithFormat:@"%@ Players", self.sportFilterName];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger rowNum = [self.nearbyPlayers count];
    if (rowNum != 0) {
        for (UIView *view in self.tableView.subviews) {
            if (view.tag == 1) { // Make sure the error overlay is exist
                [view removeFromSuperview];
                break;
            }
        }
    }
    return rowNum;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Player Cell";
    
    ExploreTVCell *cell = (ExploreTVCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    Contact *vContact = (self.nearbyPlayers)[indexPath.row];
    
    if (vContact.userID == [self.preferences getUserID]) // dont' show follow button if session user has same user id
        cell.followButton.hidden = YES;
    
    //PlayerSport *sport = [vContact.playerSports objectAtIndex:0]; // only one sport should be in array
    
    /*if ([vContact computeTotalNumMatchesPlayedForSport:sport.sportID] > 0)
        cell.levelLabel.text = [NSString stringWithFormat:@"Level %ld",  [sport.level integerValue]];
    else
        cell.levelLabel.text = @"Level 0";*/
    
    
  //  cell.sportLabel.text = [NSString stringWithFormat:@"%@ Level", [sport.sportName capitalizedString]];
    //    cell.recordLabel.text = [NSString stringWithFormat:@"%d Wins - %d Losses", sport.playerRecordForSport.wins, sport.playerRecordForSport.losses];
  //  cell.coinsLabel.text = [NSString stringWithFormat:@"$%.0f", vContact.coins];
    
    
    [cell.followButton makeRoundedBorderWithRadius:3];
    cell.followButton.tag = indexPath.row;
    
    if (vContact.followingStatus == NOT_FOLLOWING_STATUS && (vContact.userID != [self.preferences getUserID])) {
        [cell.followButton setTitle:@"Follow" forState:UIControlStateNormal];
        cell.followButton.backgroundColor = [UIColor colorWithRed:74/255.0 green:74/255.0 blue:74/255.0 alpha:1.0];
        cell.followButton.enabled = YES;
    } else if (vContact.followingStatus == IS_FOLLOWING_STATUS) {
        [cell.followButton setTitle:@"UnFollow" forState:UIControlStateNormal];
        cell.followButton.backgroundColor = [UIColor colorWithRed:74/255.0 green:74/255.0 blue:74/255.0 alpha:1.0];
        cell.followButton.enabled = YES;
    } else if (vContact.followingStatus == PENDING_FOLLOWING_STATUS) {
        [cell.followButton setTitle:@"Pending" forState:UIControlStateNormal];
        cell.followButton.backgroundColor = [UIColor lightGrayColor];
        cell.followButton.enabled = NO;
    }
    
    cell.usernameLabel.text = vContact.userName;
    cell.locationLabel.text = vContact.location;
    
    
    NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                            baseString:vContact.profileBaseString
                     ];
    
    [cell.profilePic setImageWithURL:[NSURL URLWithString:url]
                    placeholderImage:[UIImage imageNamed:@"avatar square"]];
    
    [UIImage makeRoundedImageNoBorder:cell.profilePic withRadius:10];
    
    
    
    return cell;
}


- (void)initLocation
{
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
    self.proximityRange = 10;
    
    
}

- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations {
    self.isSearch = NO;
    
    CLLocation *location = locations[0];
    
    [locationManager stopUpdatingLocation];
    //    [self.playersTableView setUserInteractionEnabled:YES];
    
    
    [IOSRequest fetchProximityPlayers:[self.preferences getUserID]
                              andLong:location.coordinate.longitude
                               andLat:location.coordinate.latitude
                            withRange:self.proximityRange
                        filterBySport:self.filterSportID
                              orderBy:self.orderByType
     
                         onCompletion:^(NSMutableArray *resultArray) {
                             [self.nearbyPlayers removeAllObjects];
                             [self.nearbyPlayersWithZeroPlayedMatches removeAllObjects];
                             for (id object in resultArray) {
                                 Contact *vContact = [[Contact alloc]
                                                      initWithContactDetails:[object[@"user_id"] integerValue]
                                                      andProfileStr:object[@"profile_pic_string"]
                                                      andUserName:object[@"username"]
                                                      andFullName:object[@"fullname"]
                                                      andLocation:object[@"location"]
                                                      andAbout:object[@"about"]
                                                      andCoins:[object[@"coins"] floatValue]
                                                      contactType:CONTACT_TYPE_NEARBY
                                                      followingStatus:[object[@"follow_status"] integerValue]];
                                 
                                 [vContact setPercentProbabilitySessionUserCanBeatPlayer:[object[@"probability_session_user_wins"] floatValue]];
                                 
                                 NSDictionary *sportObj = object[@"sports"];
                                 
                                 PlayerSport *sport = [[PlayerSport alloc] initWithSportDetails:sportObj[@"sport_name"]
                                                                                        andType:sportObj[@"type"]
                                                                                       andLevel:sportObj[@"level"]
                                                                                 andSportRecord:sportObj[@"win_loss_record"]];
                                 
                                 
                                 [vContact addPlayerSport:sport];
                                 
                                 
                                 
                                 if ([vContact computeTotalNumMatchesPlayedForSport:sport.sportID] > 0) {
                                     
                                     [self.nearbyPlayers addObject:vContact]; // at least one match played, so add to nearbyPlayersArray
                                 } else {
                                     [self.nearbyPlayersWithZeroPlayedMatches addObject:vContact]; // only one match played so add to this array
                                 }
                             }
                             
                             // Now append the nearbyPlayersWithZeroPlayedMatches to end of nearbyPlayers
                             // Later when there is pagination, will append the nearbyPlayersWithZeroPlayedMatches after last paginated
                             // page is shown.
                             
                             [self.nearbyPlayers addObjectsFromArray:self.nearbyPlayersWithZeroPlayedMatches];
                             
                             dispatch_async(dispatch_get_main_queue(), ^ {
                                 if ([self.nearbyPlayers count] == 0) {
                                     self.noResultsToDisplay = YES;
                                     self.sportNameLabel.text = @"";
                                     [self displayErrorOverlay];
                                 } else {
                                     self.noResultsToDisplay = NO;
                                     Contact *tempContact = [self.nearbyPlayers objectAtIndex:0];
                                     
                                     if ([tempContact.playerSports count] > 0) {
                                         PlayerSport *tempSport = [tempContact.playerSports objectAtIndex:0];
                                         [self.sportIconBackground makeRoundedBorderWithRadius:35];
                                         self.sportIconBackground.layer.borderColor = [UIColor whiteColor].CGColor;
                                         self.sportIconBackground.layer.borderWidth = 2;
                                         self.sportIconBackground.backgroundColor = [UIColor clearColor];
                                         
                                         self.sportIcon.image = [UIImage imageNamed:@"nearby_icon"];
                                         [self.sportIcon setTintColor:[UIColor whiteColor]];
                                         if ([tempSport.sportName length] == 0) {
                                            self.sportNameLabel.text = @"All Players";
                                             //self.sportIcon.image = [[UIImage imageNamed:@"Player"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                                             //[self.sportIcon setTintColor:[UIColor whiteColor]];
                                         } else {
                                        
                                             self.sportNameLabel.text = [NSString stringWithFormat:@"%@ Players", [tempSport.sportName capitalizedString]];
                                             //self.sportIcon.image = [[tempSport getSportIcon] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                                             }
                                     }
                                 }
                                 
                                 [self.tableView reloadData];
                                 [self.activityIndicator stopAnimating];
                                 self.tempView.hidden = YES;
                                 self.tableView.scrollEnabled = YES;
                                 [self.loadingView setHidden:YES];
                                 //                          [MBProgressHUD hideHUDForView:self.view animated:YES];
                             });
                             
                             
                             
                         }];
    
    double delayInSeconds = 10.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        //code to be executed on the main queue after delay
        self.tableView.scrollEnabled = YES;
    });
    
    
}

- (void)setViewController:(ChooseOneSportTVC *)controller withSport:(Sport *)sp
{
    self.isSearch = NO;
    [self.overlay removeFromSuperview];
    self.filterSportID = sp.sportID;
    self.sportFilterName = sp.sportName;
    
    [locationManager startUpdatingLocation];
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [self.overlay removeFromSuperview];
    //   self.overlay = nil;
    
    if ([segue.identifier isEqualToString:@"Choose Sport Segue"]) {
        ChooseOneSportTVC *controller = segue.destinationViewController;
        controller.delegate = self;
    } else if ([segue.identifier isEqualToString:@"Player Detail Segue"]) {
        PlayerDetailTVC *controller = segue.destinationViewController;
        Contact *vContact = [self.nearbyPlayers objectAtIndex:self.selectedRow];
        controller.playerUserID = vContact.userID;
        controller.profilePicString = vContact.profileBaseString;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.noResultsToDisplay) {
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        return screenRect.size.height - 150;
    }
    
    return 70.0;
}

- (void)showSubmissionError
{
    UIAlertView *alert = [[UIAlertView alloc] init];
    alert.title = @"Submission Error";
    alert.message = @"There was a problem submitting your request.  Please try again.";
    [alert addButtonWithTitle:@"OK"];
    [alert show];
}


- (IBAction)followAction:(UIButton *)sender
{
    Contact *vContact = [self.nearbyPlayers objectAtIndex:sender.tag];
    sender.enabled = NO;
    if (vContact.followingStatus == IS_FOLLOWING_STATUS && vContact.userID != [self.preferences getUserID]) {
        [IOSRequest removeFollowConnectionWithLeader:vContact.userID
                                         andFollower:[self.preferences getUserID]
                                        onCompletion:^(NSDictionary *results) {
                                            
                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                sender.enabled = YES;
                                                
                                                if (![results[@"outcome"] isEqualToString:@"failure"]) {
                                                    [self.preferences setNewsfeedRefreshState:YES];
                                                    [self.preferences setProfilePageRefreshState:YES];

                                                    [self.preferences setChallengePlayersListRefreshState:YES];
                                                    vContact.followingStatus = NOT_FOLLOWING_STATUS;
                                                    [self.nearbyPlayers replaceObjectAtIndex:sender.tag withObject:vContact];
                                                    [self.tableView reloadData];
                                                } else {
                                                    [self showSubmissionError];
                                                }
                                                
                                            });
                                        }];
    } else if (vContact.followingStatus == NOT_FOLLOWING_STATUS && vContact.userID != [self.preferences getUserID]) {
        [IOSRequest makeFollowConnectionWithLeader:vContact.userID
                                       andFollower:[self.preferences getUserID]
                                      onCompletion:^(NSDictionary *results) {
                                          
                                          dispatch_async(dispatch_get_main_queue(), ^ {
                                              [self.activityIndicator stopAnimating];
                                              self.tempView.hidden = YES;
                                              self.tableView.scrollEnabled = YES;
                                              
                                              if ([self.nearbyPlayers count] == 0) {
                                                  self.noResultsToDisplay = YES;
                                                  self.sportNameLabel.text = @"";
                                                  [self displayErrorOverlay];
                                              } else {
                                                  self.noResultsToDisplay = NO;
                                                  [self.preferences setNewsfeedRefreshState:YES];
                                                  [self.preferences setProfilePageRefreshState:YES];
                                                  Contact *tempContact = [self.nearbyPlayers objectAtIndex:0];
                                                  
                                                  if ([tempContact.playerSports count] > 0) {
                                                      PlayerSport *tempSport = [tempContact.playerSports objectAtIndex:0];
                                                      [self.sportIconBackground makeRoundedBorderWithRadius:35];
                                                      self.sportIconBackground.layer.borderColor = [UIColor whiteColor].CGColor;
                                                      self.sportIconBackground.layer.borderWidth = 2;
                                                      self.sportIconBackground.backgroundColor = [UIColor clearColor];
                                                      
                                                      self.sportIcon.image = [UIImage imageNamed:@"nearby_icon"];
                                                      [self.sportIcon setTintColor:[UIColor whiteColor]];
                                                      if ([tempSport.sportName length] == 0) {
                                                          self.sportNameLabel.text = @"All Players";
                                                      } else {
                                                          self.sportNameLabel.text = [NSString stringWithFormat:@"%@ Players", [tempSport.sportName capitalizedString]];
                                                      }
                                                  }
                                                  
                                                  vContact.followingStatus = IS_FOLLOWING_STATUS;
                                                  [self.tableView reloadData];
                                              }
                                          });
                                      }];
        
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.nearbyPlayers count] > 0) {
        self.selectedRow = indexPath.row;
        [self performSegueWithIdentifier:@"Player Detail Segue" sender:self];
    }
    
}

- (void)disableTableviewScroll
{
    if (self.loadingView.hidden) {
        [self.loadingView setHidden:NO];
    }
    [self.tableView setScrollEnabled:NO];
}

- (void)searchForUsers:(NSString *)searchText {
    if ([searchText isEqualToString:@""]){
        self.isSearch = NO;
        [locationManager startUpdatingLocation];
    } else {
        self.isSearch = YES;
        [self.nearbyPlayers removeAllObjects];
        [self.nearbyPlayersWithZeroPlayedMatches removeAllObjects];
        
        [IOSRequest searchForUser:searchText
                notAssociatedWith:[self.preferences getUserID]
                     onCompletion:^(NSMutableArray *resultArray) {
                         [self.nearbyPlayers removeAllObjects];
                         [self.nearbyPlayersWithZeroPlayedMatches removeAllObjects];
                         for (id object in resultArray) {
                             Contact *contact = [[Contact alloc] initWithContactDetails:[object[@"user_id"] integerValue]
                                                                          andProfileStr:object[@"profile_pic_string"]
                                                                            andUserName:object[@"username"]
                                                                            andFullName:object[@"fullname"]
                                                                            andLocation:object[@"location"]
                                                                               andAbout:object[@"about"]
                                                                               andCoins:[object[@"coins"] floatValue]
                                                                            contactType:CONTACT_TYPE_SEARCH
                                                                        followingStatus:[object[@"follow_status"] integerValue]];
                             
                             NSDictionary *sportObj = object[@"sports"];
                             PlayerSport *sport = [[PlayerSport alloc] initWithSportDetails:sportObj[@"sport_name"]
                                                                                    andType:sportObj[@"type"]
                                                                                   andLevel:sportObj[@"level"]
                                                                             andSportRecord:sportObj[@"win_loss_record"]];
                             
                             
                             [contact addPlayerSport:sport];
                             
                             if ([contact computeTotalNumMatchesPlayedForSport:sport.sportID] > 0) {
                                 
                                 [self.nearbyPlayers addObject:contact]; // at least one match played, so add to nearbyPlayersArray
                             } else {
                                 [self.nearbyPlayersWithZeroPlayedMatches addObject:contact]; // only one match played so add to this array
                             }
                             
                         }
                         [self.nearbyPlayers addObjectsFromArray:self.nearbyPlayersWithZeroPlayedMatches];
                         
                         /* Change the UI */
                         dispatch_async(dispatch_get_main_queue(), ^{
                             [self.activityIndicator stopAnimating];
                             self.tempView.hidden = YES;
                             self.tableView.scrollEnabled = YES;
                             
                             if ([self.nearbyPlayers count] == 0) {
                                 self.noResultsToDisplay = YES;
                                 self.sportNameLabel.text = @"";
                                 [self displayErrorOverlay];
                             } else {
                                 self.noResultsToDisplay = NO;
                                 Contact *tempContact = [self.nearbyPlayers objectAtIndex:0];
                                 
                                 if ([tempContact.playerSports count] > 0) {
                                     [self.sportIconBackground makeRoundedBorderWithRadius:35];
                                     self.sportIconBackground.layer.borderColor = [UIColor whiteColor].CGColor;
                                     self.sportIconBackground.layer.borderWidth = 2;
                                     self.sportIconBackground.backgroundColor = [UIColor clearColor];
                                     
                                     self.sportIcon.image = [UIImage imageNamed:@"nearby_icon"];
                                     [self.sportIcon setTintColor:[UIColor whiteColor]];
                                     self.sportNameLabel.text = @"Search for Players";
                                 }
                             }
                             
                             [self.tableView reloadData];
                             [self.loadingView setHidden:YES];
                         });
                     }
         ];
    }
}

-(void)doUserSearch:(NSTimer *)timer
{
    assert(timer == self.pauseTimer);
    [self searchForUsers:self.pauseTimer.userInfo];
    self.pauseTimer = nil; // important because the timer is about to release and dealloc itself
}

#pragma - mark SEARCH BAR DELEGATE

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [self disableTableviewScroll];
    NSLog(@"Timer=%@",self.pauseTimer);
    if (self.pauseTimer) {
        if ([self.pauseTimer isValid]) {
            [self.pauseTimer invalidate];
        }
        self.pauseTimer = nil;
    }
    self.pauseTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(doUserSearch:) userInfo:searchText repeats:NO]; // DELAY 1 SEC FOR USER'S INPUT
    
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self.searchUsernameBar resignFirstResponder];
}

@end
