//
//  LockerTagMultiple.h
//  Vaiden
//
//  Created by James Chung on 5/7/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "LockerTag.h"

@interface LockerTagMultiple : LockerTag

@property (nonatomic, strong) NSMutableArray *tagSelectionArray;

- (LockerTagMultiple *) initMatchWithDetails:(NSInteger)lockerTagID
                                  andTagType:(NSInteger)lockerTagType
                                  andTagName:(NSString *)lockerTagName
                                 andTagArray:(NSMutableArray *)lockerTagArray;

@end
