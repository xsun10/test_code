//
//  RecordIndMatchScoreSuccess.m
//  Phenom
//
//  Created by James Chung on 7/23/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "RecordIndMatchScoreSuccess.h"
#import "S3Tools.h"
#import "IOSRequest.h"
#import "UIImage+ProportionalFill.h"
#import "AFNetworking.h"
#import "IndividualMatchRoundScore.h"
#import "UIView+Manipulate.h"
#import "UIImage+ImageEffects.h"
#import "UIColor+VaidenColors.h"
#import "RecordIndMatchScoreSuccessCVCell.h"
#import "UserPreferences.h"

@interface RecordIndMatchScoreSuccess ()
@property (weak, nonatomic) IBOutlet UIImageView *picWinner;
@property (weak, nonatomic) IBOutlet UIView *subBackground1;
@property (weak, nonatomic) IBOutlet UIView *subBackground2;
@property (weak, nonatomic) IBOutlet UIImageView *winIcon;
@property (weak, nonatomic) IBOutlet UIImageView *blurredImage;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;
@property (weak, nonatomic) IBOutlet UILabel *player1Username;
@property (weak, nonatomic) IBOutlet UILabel *player2Username;
@property (weak, nonatomic) IBOutlet UILabel *player1Score;
@property (weak, nonatomic) IBOutlet UILabel *player2Score;
@property (weak, nonatomic) IBOutlet UIScrollView *pageScrollView;
@property (weak, nonatomic) IBOutlet UICollectionView *scoreCollectionView;
@property (nonatomic, strong) UserPreferences *preferences;
@property (weak, nonatomic) IBOutlet UIImageView *summaryIcon;

@end

@implementation RecordIndMatchScoreSuccess

- (UserPreferences *)preferences
{
    if (_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.scoreCollectionView.delegate = self;
    self.scoreCollectionView.dataSource = self;
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
    
//    self.blurredImage.contentMode = UIViewContentModeScaleAspectFill;
//    self.blurredImage.clipsToBounds = YES;
    
 //   [self.doneButton makeCircleWithColorAndBackground:[UIColor peacock] andRadius:5];

//    [self setBlurredBackgroundImage:1];
    
	[self setPageValues];
    
    //[self.subBackground1.layer setBorderColor:[UIColor midGray2].CGColor];
    //[self.subBackground1.layer setBorderWidth:0.5];
    //[self.subBackground1.layer setMasksToBounds:YES];
    
    //[self setDoneButton];
    
    //self.summaryIcon.image = [[UIImage imageNamed:@"Score Summary Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    //[self.summaryIcon setTintColor:[UIColor whiteColor]];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.navigationItem setHidesBackButton:YES animated:YES];
}
- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    self.pageScrollView.contentSize = CGSizeMake(320.0, 570.0);
}

- (void)setPageValues
{/*
    [self.subBackground1 makeRoundedBorderWithColor:[UIColor whiteColor]];
    [self.subBackground2 makeRoundedBorderWithColor:[UIColor whiteColor]];
    

    NSMutableArray *roundScores = self.individualMatch.scoreIndMatch.roundScoresIndMatch;
    NSInteger player1TotalScore = 0;
    NSInteger player2TotalScore = 0;
    
    if ([roundScores count] > 1) {
       
        for (IndividualMatchRoundScore *score in roundScores) {
        
            if (score.scorePlayer1 > score.scorePlayer2) {
                player1TotalScore ++;
            } else if (score.scorePlayer2 > score.scorePlayer1) {
                player2TotalScore ++;
            } else if (score.scorePlayer1 == score.scorePlayer2) {
                player1TotalScore ++;
                player2TotalScore ++;
            }
        
        }
    
            } else {
                // only 1 round
                IndividualMatchRoundScore *score = [roundScores objectAtIndex:0];
                player1TotalScore = score.scorePlayer1;
                player2TotalScore = score.scorePlayer2;
        
    
    }
    
 //   self.player1Username.text = self.individualMatch.player1.userName;
 //   self.player2Username.text = self.individualMatch.player2.userName;
    
    self.player1Score.text = [NSString stringWithFormat:@"%d", player1TotalScore];
    self.player2Score.text = [NSString stringWithFormat:@"%d", player2TotalScore];

    
    if (player1TotalScore > player2TotalScore) {
        
        
        NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                baseString:self.individualMatch.player1.profileBaseString];
        
        [self.picWinner setImageWithURL:[NSURL URLWithString:url]];
        
        [UIImage makeRoundedImage:self.picWinner withRadius:20];
    //    self.usernameWinner.text = [NSString stringWithFormat:@"%@ wins", self.individualMatch.player1.userName];
        [self.player1Score setTextColor:[UIColor salmonColor]];
        
    } else if (player2TotalScore > player1TotalScore) {
    //     self.scoreLabel.text = [NSString stringWithFormat:@"Score: %d - %d", player2RoundCount, player1RoundCount];
        NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                baseString:self.individualMatch.player2.profileBaseString];
        
        [self.picWinner setImageWithURL:[NSURL URLWithString:url]];
        
        [UIImage makeRoundedImage:self.picWinner withRadius:20];
    //    self.usernameWinner.text = [NSString stringWithFormat:@"%@ wins", self.individualMatch.player2.userName];
        [self.player2Score setTextColor:[UIColor salmonColor]];
        
    } else if (player1TotalScore == player2TotalScore) {
     //    self.scoreLabel.text = [NSString stringWithFormat:@"Score: %d - %d", player1RoundCount, player2RoundCount];
        [self.player1Score setTextColor:[UIColor salmonColor]];
        [self.player2Score setTextColor:[UIColor salmonColor]];
    }
    
    */
    
    
}

/*- (void)setDoneButton
{
    UIButton *btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 50, 21);
    btn.layer.borderColor = [UIColor whiteColor].CGColor;
    btn.layer.borderWidth = 1.0;
    
    [btn addTarget:self action:@selector(doneAction:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundColor:[UIColor clearColor]];
    [btn setTitle:@"DONE" forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-MediumItalic" size:12.0]];
    
    // Initialize UIBarbuttonitem...
    UIBarButtonItem *proe = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = proe;
}*/


- (IBAction)doneAction:(id)sender
{
//    [self performSegueWithIdentifier:@"Go Newsfeed Segue" sender:self];
 //   [self dismissViewControllerAnimated:YES completion:nil];
//    [self.preferences setMatchNotificationRefreshState:YES];
    [self.preferences setAlertsPageRefreshState:YES];
    [self.preferences setNewsfeedRefreshState:YES];
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}

/*
- (void)setBlurredBackgroundImage:(NSUInteger)imageIndex
{
    NSString *effectText = @"";
    UIImage *effectImage = nil;
    UIImage *backImage   = [UIImage imageNamed:@"BBall Player Background"];

    effectImage = backImage;
    
    switch (imageIndex)
    {
        case 0:
            effectImage = backImage;
            break;
        case 1:
            effectImage = [backImage applyLightEffect];
            effectText = NSLocalizedString(@"Light", @"");
            break;
        case 2:
            effectImage = [backImage applyExtraLightEffect];
            effectText = NSLocalizedString(@"Extra light", @"");
            break;
        case 3:
            effectImage = [backImage applyDarkEffect];
            effectText = NSLocalizedString(@"Dark", @"");
            break;
        case 4:
            effectImage = [backImage applyTintEffectWithColor:[UIColor blueColor]];
            effectText = NSLocalizedString(@"Color tint", @"");
            break;
    }
    
    self.blurredImage.image = effectImage;
    
}
*/
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.individualMatch.scoreIndMatch.roundScoresIndMatch count];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 2;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"My Cell";
    
    RecordIndMatchScoreSuccessCVCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    IndividualMatchRoundScore *matchScore = [self.individualMatch.scoreIndMatch.roundScoresIndMatch objectAtIndex:indexPath.row];
    
    if (indexPath.section == 0) { // first section
        cell.scoreLabel.text = [NSString stringWithFormat:@"%ld", matchScore.scorePlayer1];
        self.player1Username.text = self.individualMatch.player1.userName; // I know this is not efficient...doing this for time's sake for now.
    } else if (indexPath.section == 1) { // second section
        cell.scoreLabel.text = [NSString stringWithFormat:@"%ld", matchScore.scorePlayer2];
        self.player2Username.text = self.individualMatch.player2.userName; // I know this is not efficient...doing this for time's sake for now.
    }
    
    return cell;
}

@end
