//
//  ImageSegue.h
//  Vaiden
//
//  Created by Turbo on 6/11/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageSegue : UIStoryboardSegue
// Originating point for animation
@property CGPoint originatingPoint;
@property CGFloat contentoffset; // for scroll tableview
@property CGFloat scrollOffset; // for collectionView

@property (strong, nonatomic) UIImage *image;
@property (strong, nonatomic) UIImageView *showupView;
@property (assign) CGRect originFrame;
@property BOOL unwind;
@end
