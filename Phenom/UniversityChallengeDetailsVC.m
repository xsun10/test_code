//
//  UniversityChallengeDetailsVC.m
//  Vaiden
//
//  Created by James Chung on 2/28/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "UniversityChallengeDetailsVC.h"
#import "UniversityChallengeDetailsCVCell.h"
#import "UIView+Manipulate.h"
#import "UIColor+VaidenColors.h"
#import "UniversityChallengeRankingsTVC.h"

@interface UniversityChallengeDetailsVC ()
@property (weak, nonatomic) IBOutlet UICollectionView *detailsCollectionView;
@property (weak, nonatomic) IBOutlet UIPageControl *detailsPageControl;
@property (weak, nonatomic) IBOutlet UIButton *registerButton;

@end

@implementation UniversityChallengeDetailsVC


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
    
    if (!self.canRegister) {
        [self.registerButton setTitle:@"Rankings" forState:UIControlStateNormal];
    }
    
    self.detailsCollectionView.delegate = self;
    self.detailsCollectionView.dataSource = self;
    
    self.registerButton.layer.borderColor = [UIColor midGray2].CGColor;
    self.registerButton.layer.borderWidth = 1.0;
    
    [self.detailsCollectionView reloadData];
    
}
- (IBAction)registerAction:(UIButton *)sender
{
    if ([sender.titleLabel.text isEqualToString:@"Register"])
        [self performSegueWithIdentifier:@"Register Segue" sender:self];
    else
        [self performSegueWithIdentifier:@"Rankings Segue" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Rankings Segue"]) {
        UniversityChallengeRankingsTVC *controller = segue.destinationViewController;
        controller.competitionID = self.competitionID;
        controller.universityID = self.universityID;
        controller.universityName = self.universityName;

    }
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 7;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UniversityChallengeDetailsCVCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"My Cell" forIndexPath:indexPath];
    
    cell.stepTitle.text = [self getTitleTextForRow:indexPath.row];
    cell.stepDetails.text = [self getDetailTextForRow:indexPath.row];
    
    cell.iconPic.image = [[self getStepImage:indexPath.row] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    cell.iconPic.tintColor = [UIColor blackColor];
    
    self.detailsPageControl.currentPage = indexPath.row;
    
    return cell;
}

- (NSString *)getTitleTextForRow:(NSInteger)row
{
    NSString *returnString = @"";
    
    switch (row) {
        case 0:
            returnString = @"Register For The Competition";
            break;
            
        case 1:
            returnString = @"Challenge Your Friends";
            break;
        case 2:
            returnString = @"Earn Your Spot";
            break;
        case 3:
            returnString = @"Rep Your School";
            break;
        case 4:
            returnString = @"Dominate And Earn Cash";
            break;
        case 5:
            returnString = @"Stay Tuned For Updates";
            break;
        case 6:
            returnString = @"One More Thing...";
            break;
        default:
            break;
    }
    return returnString;
}

- (NSString *)getDetailTextForRow:(NSInteger)row
{
    NSString *returnString = @"";
    
    switch (row) {
        case 0:
            returnString = @"Get started by telling us your university and school organization. You must provide a valid .edu email address to participate.";
            break;
            
        case 1:
            returnString = @"Start playing to qualify!  Compete against your friends in 1 vs 1 basketball and record the scores in the Vaiden app.";
            break;
            
        case 2:
            returnString = @"Prove you’re the best on campus. Defeat your friends in challenge matches, and raise your sport level.  The person in your school with the highest sports level will qualify for the Intercollegiate Tournament";
            break;
            
        case 3:
            returnString = @"At the Intercollegiate Tournament, you will compete against the best from other universities in 1 vs 1 basketball.";
            break;
            
        case 4:
            returnString = @"The winner of the competition will receive $500.  An additional $500 will be given to the student organization they represent.";
            break;
            
        case 5:
            returnString = @"We'll periodically send updates to you via email and via posts in the Vaiden app. Keep checking in for updates.";
            break;
        case 6:
            returnString = @"If we have enough competitors from at least 20 universities, we will double the cash prize.  Invite your friends from other schools to participate and increase the payout";
            break;
        default:
            break;
    }
    
    return returnString;
}

- (UIImage *)getStepImage:(NSInteger)row
{
    UIImage *returnImage = nil;
    
    switch (row) {
        case 0:
            returnImage = [UIImage imageNamed:@"Univ Step2 Icon"];
            break;
            
        case 1:
            returnImage = [UIImage imageNamed:@"Univ Step3 Icon"];
            break;
            
        case 2:
            returnImage = [UIImage imageNamed:@"Univ Step4 Icon"];
            break;
            
        case 3:
            returnImage = [UIImage imageNamed:@"Univ Step1 Icon"];
            break;
            
        case 4:
            returnImage = [UIImage imageNamed:@"Univ Step5 Icon"];
            break;
            
        case 5:
            returnImage = [UIImage imageNamed:@"Univ Step6 Icon"];
            break;
            
        case 6:
            returnImage = [UIImage imageNamed:@"Univ Step7 Icon"];
            break;
            
        default:
            break;
    }
    
    return returnImage;
}

@end
