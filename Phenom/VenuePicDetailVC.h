//
//  VenuePicDetailVC.h
//  Vaiden
//
//  Created by James Chung on 10/10/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VenueImage.h"
#import "CustomBaseVC.h"

@interface VenuePicDetailVC : CustomBaseVC <UIScrollViewDelegate>

@property (nonatomic, strong) VenueImage *venueImage;
@property (weak, nonatomic) IBOutlet UIImageView *venuePic;
@property CGFloat originX;
@property CGFloat originY;
@property (assign) CGRect frame;

@property (strong, nonatomic) UIImageView *imageView;
@end
