//
//  VenueCheckinNews.h
//  Vaiden
//
//  Created by James Chung on 4/25/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "News.h"
#import "VenueCheckIn.h"

@interface VenueCheckinNews : News

@property (nonatomic, strong) Venue *venue;
@property (nonatomic, strong) VenueCheckIn *vci;

- (VenueCheckinNews *)initWithNewsID:(NSInteger)newsMakerID
                      andUsername:(NSString *)newsMakerUsername
                    andProfileStr:(NSString *)newsMakerProfileString
                         withDate:(NSDate *)newsDate
                      andPostType:(NSInteger)postType
                   andNumComments:(NSInteger)numComments
                    andNumRemixes:(NSInteger)numRemixes
                        andNewsID:(NSInteger)newsID
                         andVenue:(Venue *)venueObj
                     andVenueCheckIn:(VenueCheckIn *)vciObj;

@end
