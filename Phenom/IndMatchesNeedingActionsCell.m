//
//  ScoresToRecordConfirmCell.m
//  Phenom
//
//  Created by James Chung on 5/13/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "IndMatchesNeedingActionsCell.h"

@implementation IndMatchesNeedingActionsCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
