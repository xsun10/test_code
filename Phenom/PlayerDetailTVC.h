//
//  PlayerDetailTVC.h
//  Phenom
//
//  Created by James Chung on 4/25/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomBaseTVC.h"
#import "PlayerDetailNewsfeedPaginator.h"
#import "PDNewsPickupMatchPostCell.h"
#import "PDNewsIndividualMatchPostCell.h"
#import "PDMatchNewsFinalScoreUpdateCell.h"
#import "CommentsTVC.h"
#import "MBProgressHUD.h"


@interface PlayerDetailTVC : CustomBaseTVC <NMPaginatorDelegate, PDNewsPickupMatchPostCell_delegate, PDNewsIndividualMatchPostCell_delegate, PDMatchNewsFinalScoreUpdateCell_delegate, CommentsTVC_Delegate, MBProgressHUDDelegate>
{
    MBProgressHUD *HUD;
}

@property (nonatomic) NSInteger playerUserID;
@property (nonatomic, strong) NSString *profilePicString;

@property (nonatomic, strong) PlayerDetailNewsfeedPaginator *newsfeedPaginator;

@property (nonatomic) BOOL fromComment;
@end
