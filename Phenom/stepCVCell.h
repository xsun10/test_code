//
//  stepCVCell.h
//  Vaiden
//
//  Created by Turbo on 6/5/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface stepCVCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *headlineLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UIImageView *stepsImage;

@end
