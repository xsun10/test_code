//
//  LockerAlbumCover.h
//  Vaiden
//
//  Created by James Chung on 5/10/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Sport.h"

@interface LockerAlbumCover : NSObject

- (LockerAlbumCover *)initWithLockerAlbumCoverDetails:(NSInteger)lockerID
                                           andCreator:(NSInteger)creatorID
                                         andImageName:(NSString *)imageName
                                            andSeason:(NSString *)season
                                              andYear:(NSInteger)year
                                             andSport:(Sport *)sp
                                           andNumPics:(NSInteger)numPics;
@property (nonatomic) NSInteger lockerID;
@property (nonatomic) NSInteger creatorID;
@property (nonatomic) NSString *imageName;
@property (nonatomic, strong) NSString *season;
@property (nonatomic) NSInteger year;
@property (nonatomic) Sport *sp;
@property (nonatomic) NSInteger numPics;

@end
