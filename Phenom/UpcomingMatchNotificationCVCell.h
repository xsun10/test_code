//
//  UpcomingMatchNotificationCVCell.h
//  Vaiden
//
//  Created by James Chung on 2/5/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UpcomingMatchNotificationCVCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *profilePic;

@end
