//
//  ResetPasswordTVC.h
//  Vaiden
//
//  Created by James Chung on 4/2/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface ResetPasswordTVC : UITableViewController <MBProgressHUDDelegate>
{
    MBProgressHUD *HUD;
}

@end
