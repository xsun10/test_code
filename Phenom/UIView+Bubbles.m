//
//  UIView+Bubbles.m
//  Vaiden
//
//  Created by James Chung on 12/23/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "UIView+Bubbles.h"

@implementation UIView (Bubbles)

+ (UIView*)makeBubbleWithWidth:(CGFloat)w font:(UIFont*)f text:(NSString*)s caps:(CGSize)caps padding:(CGFloat*)padTRBL withPosition:(NSString *)position
{
	// Create label
	UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, w, 1)];
    
    
    NSString *fn = @"";
    
    if ([position isEqualToString:@"left"]) {
        fn = @"Bubble Left";
    } else {
        fn = @"Bubble Right";
        label.textAlignment = NSTextAlignmentRight;
    }
    
	// Configure (for multi-line word-wrapping)
	label.font = f;
    label.textColor = [UIColor whiteColor];
	label.numberOfLines = 0;
	label.lineBreakMode = NSLineBreakByWordWrapping;
    
	// Set and size
	label.text = s;
	[label sizeToFit];
    
	// Size and create final view
	CGSize finalSize = CGSizeMake(label.frame.size.width+padTRBL[1]+padTRBL[3], label.frame.size.height+padTRBL[0]+padTRBL[2]);
	UIView* finalView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, finalSize.width, finalSize.height)];
    
	// Create stretchable BG image
	UIImage* bubble = [[UIImage imageNamed:fn] stretchableImageWithLeftCapWidth:caps.width topCapHeight:caps.height];
	UIImageView* background = [[UIImageView alloc] initWithImage:bubble];
	background.frame = finalView.frame;
    
	// Assemble composite (with padding for label)
	[finalView addSubview:background];
	[finalView addSubview:label];
	label.backgroundColor = [UIColor clearColor];
	label.frame = CGRectMake(padTRBL[3], padTRBL[0] + 1, label.frame.size.width, label.frame.size.height);
    
    return finalView;
}

@end
