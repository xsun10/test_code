//
//  ChallengeTeamListTVC.h
//  Vaiden
//
//  Created by James Chung on 4/10/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "CustomBaseTVC.h"
#import "TEam.h"

@interface ChallengeTeamListTVC : CustomBaseTVC

@property (nonatomic) Team  *sessionUserTeamToUse;

@end
