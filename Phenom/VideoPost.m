//
//  VideoPost.m
//  Phenom
//
//  Created by James Chung on 5/29/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "VideoPost.h"

@implementation VideoPost

- (VideoPost *)initWithNewsDetails:(NSInteger)newsMakerUserID
                       andUserName:(NSString *)newsMakerUserName
                     andProfileStr:(NSString *)profileBaseString
                          withDate:(NSDate *)newsDate
                       andPostType:(NSInteger)postType
                         andNewsID:(NSInteger)newsID
                       andTextPost:(NSString *)textPost
                andVideoFileString:(NSString *)videoBaseFileString
                 andThumbnailImage:(UIImage *)thumbnailImage
                      andVideoType:(NSString *)videoType
                    andNumComments:(NSUInteger)numComments
                     andNumRemixes:(NSUInteger)numRemixes
{
    self = [super initWithNewsDetails:newsMakerUserID
                          andUserName:newsMakerUserName
                        andProfileStr:profileBaseString
                             withDate:newsDate
                          andPostType:postType
                       andNumComments:numComments
                        andNumRemixes:numRemixes
                            andNewsID:newsID
                          andTextPost:textPost];
    
    if (self) {
        
        _vid = [[Video alloc] initWithVideoDetails:videoBaseFileString
                                withThumbnailImage:thumbnailImage
                                      andVideoType:videoType];
    }
    return self;
}
@end
