//
//  UpcomingMatchNotificationCell.h
//  Phenom
//
//  Created by James Chung on 6/17/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UpcomingPickupMatchNotificationCell : UITableViewCell <UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, weak) IBOutlet UILabel *matchCount;
@property (nonatomic, weak) IBOutlet UILabel *headlineMessage;
@property (nonatomic, weak) IBOutlet UILabel *description;
@property (weak, nonatomic) IBOutlet UIView *noteBackground;
@property (weak, nonatomic) IBOutlet UIImageView *notePic;
@property (weak, nonatomic) IBOutlet UILabel *pendingConfirmedTag;
@property (weak, nonatomic) IBOutlet UILabel *montLabel;
@property (weak, nonatomic) IBOutlet UILabel *dayLabel;
@property (weak, nonatomic) IBOutlet UILabel *todayTomorrowLabel;
@property (weak, nonatomic) IBOutlet UIImageView *sportIcon;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *venueLabel;
@property (weak, nonatomic) IBOutlet UIImageView *starIcon;
@property (weak, nonatomic) IBOutlet UICollectionView *playerCollectionView;
@property (weak, nonatomic) IBOutlet UILabel *withLabel;
@property (weak, nonatomic) IBOutlet UILabel *sportLabel;
@property (weak, nonatomic) IBOutlet UILabel *matchCategoryLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *additionalPlayersLabel;

@property (weak, nonatomic) IBOutlet UILabel *cityStateLabel;
@property (nonatomic, strong) NSString *matchCategory;
@property (nonatomic, strong) NSMutableArray *competitors;
@property (weak, nonatomic) IBOutlet UIView *headlineBackground;
@property (weak, nonatomic) IBOutlet UIButton *detailButton;
@property (weak, nonatomic) IBOutlet UIView *mainBackgroundViewCell;
@property (weak, nonatomic) IBOutlet UILabel *matchNameLabel;

@property (nonatomic, strong) NSMutableArray *confirmedParticipants;
@property (weak, nonatomic) IBOutlet UIImageView *sportPicBackground;

- (void)setPlayerValues;

@end
