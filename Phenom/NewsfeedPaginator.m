//
//  NewsfeedPaginator.m
//  Vaiden
//
//  Created by James Chung on 9/6/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "NewsfeedPaginator.h"
#import "PickupMatchCreationNews.h"
#import "IndividualMatchCreationNews.h"
#import "TeamMatchCreationNews.h"
#import "MyHelpers.h"
#import "IOSRequest.h"
#import "UserPreferences.h"
#import "TextPost.h"
#import "ImagePost.h"
#import "S3Tools.h"
#import "UIImage+VidTools.h"
#import "UIImage+ProportionalFill.h"
#import "MatchPlayerNews.h"
#import "MatchNews.h"
#import "MatchPlayerNewsOfficialFinishedIndividualMatch.h"
#import "NSDate+Utilities.h"
#import "AdUnit.h"
#import "CompetitionNews.h"
#import "VenueKingNews.h"
#import "VenueCheckIn.h"
#import "VenueCheckinNews.h"
#import "LockerPost.h"
#import "StatTag.h"
#import "TrainingTag.h"
#import "VoteReceiving.h"

@interface NewsfeedPaginator() {
}

// protected properties

@property (nonatomic, strong) UserPreferences *preferences;
@property (nonatomic, strong) NSMutableArray *newsPosts;

@end


@implementation NewsfeedPaginator


#define PICKUP_MATCH_CREATION 1
#define INDIVIDUAL_MATCH_CREATION 2
#define INDIVIDUAL_MATCH_CONFIRMATION 3
#define INDIVIDUAL_MATCH_RESULT 4
#define USER_TEXT_POST 5
#define USER_TEXT_POST_WITH_IMAGE 6
#define INDIVIDUAL_MATCH_UPDATE 7
#define PICKUP_MATCH_UPDATE 8
#define INDIVIDUAL_MATCH_PLAYER_UPDATE 9
#define PICKUP_MATCH_PLAYER_UPDATE 10
#define INDIVIDUAL_MATCH_RECORD_SCORE_UPDATE 11
#define INDIVIDUAL_MATCH_CONFIRM_SCORE_UPDATE 12
#define INDIVIDUAL_MATCH_DISPUTE_SCORE_UPDATE 13
#define COMPETITION_RSVP 14
#define VENUE_KING_UPDATE 15
#define TEAM_CREATION 16
#define TEAM_MATCH_CREATION 17 
#define VENUE_CHECKIN 18
#define USER_LOCKER_POST 19
#define VOTE_RECEIVING 20
#define VOTE_GIVING 21
#define SENT_THANKS 22
#define LEVEL_UP 23
#define VOTE_WINNER 24

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (NSMutableArray *)newsPosts
{
    if (!_newsPosts) _newsPosts = [[NSMutableArray alloc] init];
    return _newsPosts;
}

- (void)fetchFirstPageWithTab:(NSInteger)tabToShow andNewsfeedDetailID:(NSInteger)newsfeedDetailID
{
    [self reset];
    
    if (self.tabToShow != tabToShow) {
        NSLog(@"changed!");
        self.tabToShow = tabToShow;
        [self.results removeAllObjects];
        [self.newsPosts removeAllObjects];
    }
    self.newsfeedDetailID = newsfeedDetailID;
    
    [self fetchNextPage];
    
}

- (void)fetchResultsWithPage:(NSInteger)page pageSize:(NSInteger)pageSize
{
    //NSLog(@"This is in the %@:", self.tabToShow == NEWSFEED_MYNEWS_SEGMENT?@"Mynews":@"Browse");
    if (self.tabToShow == NEWSFEED_MYNEWS_SEGMENT) {
    [IOSRequest fetchNewsfeeds:[self.preferences getUserID]  withPageNum:page andSpan:pageSize onCompletion:^(NSArray *results) {
        [self.newsPosts removeAllObjects];
        
        NSLog(@"%@",results);
        
        for (id object in results) {
            if ([object[@"post_type"] integerValue] == PICKUP_MATCH_CREATION) {
                [self loadPickupMatch:object];
            } else if ([object[@"post_type"] integerValue] == INDIVIDUAL_MATCH_CREATION) {
                [self loadIndividualMatches:object];
            } else if ([object[@"post_type"] integerValue] == INDIVIDUAL_MATCH_CONFIRMATION) {
                [self loadIndividualMatches:object];
            } else if ([object[@"post_type"] integerValue ] == INDIVIDUAL_MATCH_RESULT) {
                 [self loadIndividualMatches:object];
            } else if ([object[@"post_type"] integerValue ] == USER_TEXT_POST) {
                [self loadUserGeneratedPosts:object];
            } else if ([object[@"post_type"] integerValue] == USER_TEXT_POST_WITH_IMAGE) {
                [self loadUserGeneratedPosts:object];
            } else if ([object[@"post_type"] integerValue] == INDIVIDUAL_MATCH_UPDATE) {
//                [self loadMatchNewsUpdate:object];
                [self loadIndividualMatches:object];
            } else if ([object[@"post_type"] integerValue] == PICKUP_MATCH_UPDATE) {
//                [self loadMatchNewsUpdate:object];
                [self loadPickupMatch:object];
            } else if ([object[@"post_type"] integerValue] == INDIVIDUAL_MATCH_PLAYER_UPDATE) {
       //         [self loadMatchNewsPlayerUpdate:object];
                 [self loadIndividualMatches:object];
            } else if ([object[@"post_type"] integerValue] == PICKUP_MATCH_PLAYER_UPDATE) {
//                [self loadMatchNewsPlayerUpdate:object];
                [self loadPickupMatch:object];
            } else if ([object[@"post_type"] integerValue] == INDIVIDUAL_MATCH_RECORD_SCORE_UPDATE) {
//                [self loadMatchNewsPlayerScoreUpdate:object];
                [self loadIndividualMatches:object];
            } else if ([object[@"post_type"] integerValue] == INDIVIDUAL_MATCH_CONFIRM_SCORE_UPDATE) {
                [self loadMatchPlayerNewsOfficialFinishedIndividualMatch:object];
            } else if ([object[@"post_type"] integerValue] == INDIVIDUAL_MATCH_DISPUTE_SCORE_UPDATE) {
              //  [self loadMatchNewsPlayerScoreUpdate:object];
                [self loadIndividualMatches:object];
            } else if ([object[@"post_type"] integerValue] == COMPETITION_RSVP) {
                [self loadCompetitionRSVP:object];
            } else if ([object[@"post_type"] integerValue] == VENUE_KING_UPDATE) {
                [self loadVenueKingNews:object];
            } else if ([object[@"post_type"] integerValue] == TEAM_CREATION) {
                
            } else if ([object[@"post_type"] integerValue] == TEAM_MATCH_CREATION) {
                [self loadTeamMatches:object];
            } else if ([object[@"post_type"] integerValue] == VENUE_CHECKIN) {
                [self loadVenueCheckin:object];
            } else if ([object[@"post_type"] integerValue] == USER_LOCKER_POST) {
                [self loadLockerPost:object];
            } else if ([object[@"post_type"] integerValue] == VOTE_RECEIVING) {
                [self loadVoteNews:object withType:@"receiving"];
            } else if ([object[@"post_type"] integerValue] == VOTE_GIVING) {
                [self loadVoteNews:object withType:@"giving"];
            } else if ([object[@"post_type"] integerValue] == SENT_THANKS) {
                [self loadVoteNews:object withType:@"thanks"];
            } else if ([object[@"post_type"] integerValue] == LEVEL_UP) {
                [self loadVoteNews:object withType:@"level"];
            } else if ([object[@"post_type"] integerValue] == VOTE_WINNER) {
                [self loadVoteNews:object withType:@"winner"];
            }
        }
        
        //      [self loadIndividualMatches:results[@"im_newsfeed"]];
        //      [self loadPickupMatches:results[@"pm_newsfeed"]];
        
        //       [self loadUserGeneratedPosts:results[@"user_txt_with_file_newsfeed"]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (page == 1) {
                // first page show ad
                AdUnit *ad = [[AdUnit alloc] init];
                [self.newsPosts insertObject:ad atIndex:0];
            }
            //[self.newsPosts insertObject:@"My News" atIndex:self.newsPosts.count];
            [self receivedResults:self.newsPosts total:100];
        });
 //       [self endRefreshWhenSynced];
        
    }];
    } else if (self.tabToShow == NEWSFEED_BROWSE_SEGMENT) {
        [IOSRequest fetchBrowseNewsfeeds:[self.preferences getUserID] withPageNum:page andSpan:pageSize onCompletion:^(NSArray *results) {
            [self.newsPosts removeAllObjects];

            for (id object in results) {
                if ([object[@"post_type"] integerValue] == USER_LOCKER_POST) {
                    [self loadLockerPost:object];
                }
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if (page == 1) {
                    // first page show ad
                    AdUnit *ad = [[AdUnit alloc] init];
                    [self.newsPosts insertObject:ad atIndex:0];
                }
                [self receivedResults:self.newsPosts total:100];
                
                //NSMutableArray * tag = [[NSMutableArray alloc] init];
                //[tag addObject:@"Browse"];
                //[self.results addObjectsFromArray:tag];
            });

        }];
    } else if (self.tabToShow == NEWSFEED_DETAIL_POST && self.newsfeedDetailID != 0) {
        [IOSRequest fetchNewsfeedDetail:self.newsfeedDetailID
                                 byUser:[self.preferences getUserID]
                           onCompletion:^(NSMutableArray *results) {
                               
                               [self.newsPosts removeAllObjects];
                               
                               
                               for (id object in results) {
                                   if ([object[@"post_type"] integerValue] == PICKUP_MATCH_CREATION) {
                                       [self loadPickupMatch:object];
                                   } else if ([object[@"post_type"] integerValue] == INDIVIDUAL_MATCH_CREATION) {
                                       [self loadIndividualMatches:object];
                                   } else if ([object[@"post_type"] integerValue] == INDIVIDUAL_MATCH_CONFIRMATION) {
                                       [self loadIndividualMatches:object];
                                   } else if ([object[@"post_type"] integerValue ] == INDIVIDUAL_MATCH_RESULT) {
                                       [self loadIndividualMatches:object];
                                   } else if ([object[@"post_type"] integerValue ] == USER_TEXT_POST) {
                                       [self loadUserGeneratedPosts:object];
                                   } else if ([object[@"post_type"] integerValue] == USER_TEXT_POST_WITH_IMAGE) {
                                       [self loadUserGeneratedPosts:object];
                                   } else if ([object[@"post_type"] integerValue] == INDIVIDUAL_MATCH_UPDATE) {
                                       [self loadIndividualMatches:object];
                                   } else if ([object[@"post_type"] integerValue] == PICKUP_MATCH_UPDATE) {
                                       [self loadPickupMatch:object];
                                   } else if ([object[@"post_type"] integerValue] == INDIVIDUAL_MATCH_PLAYER_UPDATE) {
                                       [self loadIndividualMatches:object];
                                   } else if ([object[@"post_type"] integerValue] == PICKUP_MATCH_PLAYER_UPDATE) {
                                       [self loadPickupMatch:object];
                                   } else if ([object[@"post_type"] integerValue] == INDIVIDUAL_MATCH_RECORD_SCORE_UPDATE) {
                                       [self loadIndividualMatches:object];
                                   } else if ([object[@"post_type"] integerValue] == INDIVIDUAL_MATCH_CONFIRM_SCORE_UPDATE) {
                                       [self loadMatchPlayerNewsOfficialFinishedIndividualMatch:object];
                                   } else if ([object[@"post_type"] integerValue] == INDIVIDUAL_MATCH_DISPUTE_SCORE_UPDATE) {
                                       [self loadIndividualMatches:object];
                                   } else if ([object[@"post_type"] integerValue] == COMPETITION_RSVP) {
                                       [self loadCompetitionRSVP:object];
                                   } else if ([object[@"post_type"] integerValue] == VENUE_KING_UPDATE) {
                                       [self loadVenueKingNews:object];
                                   } else if ([object[@"post_type"] integerValue] == TEAM_CREATION) {
                                       
                                   } else if ([object[@"post_type"] integerValue] == TEAM_MATCH_CREATION) {
                                       [self loadTeamMatches:object];
                                   } else if ([object[@"post_type"] integerValue] == VENUE_CHECKIN) {
                                       [self loadVenueCheckin:object];
                                   } else if ([object[@"post_type"] integerValue] == USER_LOCKER_POST) {
                                       [self loadLockerPost:object];
                                   } else if ([object[@"post_type"] integerValue] == VOTE_RECEIVING) {
                                       [self loadVoteNews:object withType:@"receiving"];
                                   } else if ([object[@"post_type"] integerValue] == VOTE_GIVING) {
                                       [self loadVoteNews:object withType:@"giving"];
                                   } else if ([object[@"post_type"] integerValue] == SENT_THANKS) {
                                       [self loadVoteNews:object withType:@"thanks"];
                                   } else if ([object[@"post_type"] integerValue] == LEVEL_UP) {
                                       [self loadVoteNews:object withType:@"level"];
                                   } /*else if ([object[@"post_type"] integerValue] == VOTE_WINNER) {
                                       [self loadVoteNews:object withType:@"winner"];
                                   }*/
                               }
                               
                               
                               
                               dispatch_async(dispatch_get_main_queue(), ^{
                                   if (page == 1) {
                                       // first page show ad
                                       AdUnit *ad = [[AdUnit alloc] init];
                                       [self.newsPosts insertObject:ad atIndex:0];
                                   }
                                   [self receivedResults:self.newsPosts total:100];
                                   
                                   //NSMutableArray * tag = [[NSMutableArray alloc] init];
                                   //[tag addObject:@"Detail Post"];
                                   //[self.results addObjectsFromArray:tag];
                               });
                               
                               
                               
                           }];
    }
}

- (void)loadCompetitionRSVP:(NSDictionary *)object
{
    id dataObj = object[@"data_obj"];
    
    NSDate *newsfeedDateTime    = [MyHelpers getDateTimeFromString:object[@"newsfeed_ts"]];

    
    CompetitionNews *cNews = [[CompetitionNews alloc] initWithNewsID:[object[@"post_user_id"] integerValue]
                                                         andUsername:object[@"post_username"]
                                                       andProfileStr:object[@"profile_pic_string"]
                                                            withDate:newsfeedDateTime
                                                         andPostType:[object[@"post_type"] integerValue]
                                                      andNumComments:[object[@"num_comments"] integerValue]
                                                       andNumRemixes:[object[@"num_remixes"] integerValue]
                                                           andNewsID:[object[@"newsfeed_id"] integerValue]
                                                   andUniversityName:dataObj[@"college_name"]
                                           andUniversityOrganization:dataObj[@"college_organization"]];
    
    id subObj = object[@"remix_details"];
    
    cNews.sharedByPlayer = [[Player alloc] initWithPlayerDetails:[subObj[@"user_id"] integerValue] andUserName:subObj[@"username"]];
    cNews.didSessionUserRemixPost = [object[@"did_session_user_remix_post"] boolValue];
    
    cNews.numLikes = [object[@"num_likes"] integerValue];
    cNews.doesSessionUserLikePost = [object[@"does_session_user_like_post"] boolValue];
    
    [self.newsPosts addObject:cNews];
    

}

- (void)loadVenueKingNews:(NSDictionary *)object
{
    id dataObj = object[@"data_obj"];
    
    NSDate *newsfeedDateTime    = [MyHelpers getDateTimeFromString:object[@"newsfeed_ts"]];
    
    /*
    CompetitionNews *cNews = [[CompetitionNews alloc] initWithNewsID:[object[@"post_user_id"] integerValue]
                                                         andUsername:object[@"post_username"]
                                                       andProfileStr:object[@"profile_pic_string"]
                                                            withDate:newsfeedDateTime
                                                         andPostType:[object[@"post_type"] integerValue]
                                                      andNumComments:[object[@"num_comments"] integerValue]
                                                       andNumRemixes:[object[@"num_remixes"] integerValue]
                                                           andNewsID:[object[@"newsfeed_id"] integerValue]
                                                   andUniversityName:dataObj[@"college_name"]
                                           andUniversityOrganization:dataObj[@"college_organization"]];*/
    Venue *vkVenue = [[Venue alloc] initVenueWithName:dataObj[@"venue_name"]
                                           andVenueID:[dataObj[@"venue_id"] integerValue]];
    [vkVenue setCity:dataObj[@"city"]];
    [vkVenue setStateName:dataObj[@"state"]];
    [vkVenue setVenueImageThumb:[[VenueImage alloc] initWithVenueImageInfo:dataObj[@"venue_thumb"][@"filestring"]
                                                                 andImage:nil
                                                                isDefault:YES]];
    
    Sport *vkSport = [[Sport alloc] initWithSportDetails:[dataObj[@"sport_id"] integerValue]
                                                 andName:dataObj[@"sport_name"]
                                                 andType:@"individual"];
    
    VenueKingNews *vNews = [[VenueKingNews alloc] initWithNewsID:[object[@"post_user_id"] integerValue]
                                                     andUsername:object[@"post_username"]
                                                   andProfileStr:object[@"profile_pic_string"]
                                                        withDate:newsfeedDateTime
                                                     andPostType:[object[@"post_type"] integerValue]
                                                  andNumComments:[object[@"num_comments"] integerValue]
                                                   andNumRemixes:[object[@"num_remixes"] integerValue]
                                                       andNewsID:[object[@"newsfeed_id"] integerValue]
                                                        andVenue:vkVenue
                                                        andSport:vkSport];
    
    id subObj = object[@"remix_details"];
    
    vNews.sharedByPlayer = [[Player alloc] initWithPlayerDetails:[subObj[@"user_id"] integerValue] andUserName:subObj[@"username"]];
    vNews.didSessionUserRemixPost = [object[@"did_session_user_remix_post"] boolValue];
    
    vNews.numLikes = [object[@"num_likes"] integerValue];
    vNews.doesSessionUserLikePost = [object[@"does_session_user_like_post"] boolValue];
    
    [self.newsPosts addObject:vNews];
}

- (void)loadVenueCheckin:(NSDictionary *)object
{
    id dataObj = object[@"data_obj"];
    
    NSDate *newsfeedDateTime    = [MyHelpers getDateTimeFromString:object[@"newsfeed_ts"]];
   
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *dateTime = [[formatter dateFromString:dataObj[@"check_in_ts"]] toLocalTime];
    
    Player *checkInPlayer = [[Player alloc] initWithPlayerDetails:[dataObj[@"user_id"] integerValue]
                                                      andUserName:dataObj[@"username"]
                                              andProfilePicString:dataObj[@"profile_pic_string"]];
    
    VenueCheckIn *vci = [[VenueCheckIn alloc] initWithCheckInID:[dataObj[@"checkin_id"] integerValue]
                                              checkedInDateTime:dateTime
                                               checkedInMessage:dataObj[@"message"]
                                                       isPublic:[dataObj[@"is_public"] boolValue]
                                                          isNow:[dataObj[@"is_now"] boolValue]
                                                  checkedInUser:checkInPlayer
                                                 checkedInSport:[[Sport alloc] initWithSportDetails:[dataObj[@"sport_id"] integerValue]
                                                                                            andName:dataObj[@"sport_name"]
                                                                                            andType:nil]];
    
    Venue *vkVenue = [[Venue alloc] initVenueWithName:dataObj[@"venue_name"]
                                           andVenueID:[dataObj[@"venue_id"] integerValue]];
    
    
    VenueCheckinNews *vNews = [[VenueCheckinNews alloc] initWithNewsID:[object[@"post_user_id"] integerValue]
                                                     andUsername:object[@"post_username"]
                                                   andProfileStr:object[@"profile_pic_string"]
                                                        withDate:newsfeedDateTime
                                                     andPostType:[object[@"post_type"] integerValue]
                                                  andNumComments:[object[@"num_comments"] integerValue]
                                                   andNumRemixes:[object[@"num_remixes"] integerValue]
                                                       andNewsID:[object[@"newsfeed_id"] integerValue]
                                                        andVenue:vkVenue
                                                    andVenueCheckIn:vci];
    
    vNews.numLikes = [object[@"num_likes"] integerValue];
    vNews.doesSessionUserLikePost = [object[@"does_session_user_like_post"] boolValue];
    
    id subObj = object[@"remix_details"];
    
    vNews.sharedByPlayer = [[Player alloc] initWithPlayerDetails:[subObj[@"user_id"] integerValue] andUserName:subObj[@"username"]];
    vNews.didSessionUserRemixPost = [object[@"did_session_user_remix_post"] boolValue];
    [self.newsPosts addObject:vNews];
    
}

- (void)loadPickupMatch:(NSDictionary *)object
{
    id dataObj = object[@"data_obj"];
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *matchDateTime = [[formatter dateFromString:dataObj[@"ps_ts"]] toLocalTime];
    
    NSDateFormatter *formatter2 = [[NSDateFormatter alloc]init];
    [formatter2 setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *newsfeedDateTime = [[formatter2 dateFromString:object[@"newsfeed_ts"]] toLocalTime];
    
    
    PickupMatchCreationNews *pNews = [[PickupMatchCreationNews alloc]
                                      initWithPickupMatchCreationNewsDetails:[dataObj[@"match_id"] integerValue]
                                      andMatchName:dataObj[@"match_name"]
                                      matchCreatorID:[dataObj[@"creator_id"] integerValue]
                                      matchCreatorUserName:dataObj[@"creator_username"]
                                      andProfileStr:dataObj[@"creator_profile_pic_string"]
                                      matchDate:matchDateTime
                                      newsPostDate:newsfeedDateTime
                                      andPostType:1 //[object[@"post_type"] integerValue]
                                      atVenueID:[dataObj[@"venue_id"] integerValue]
                                      atVenueName:dataObj[@"venue_name"]
                                      withSportID:[dataObj[@"sport_id"] integerValue]
                                      andSportName:dataObj[@"sport_name"]
                                      andSportType:dataObj[@"sport_type"]
                                      isPrivate:[dataObj[@"is_private"] boolValue]
                                      andMatchStatus:dataObj[@"match_status"]
                                      minCompetitors:[dataObj[@"min_competitors"] integerValue]
                                      requiredExperience:[dataObj[@"required_experience"] mutableCopy] // I really don't need NSMutableArray...Just doing this to get rid of warning...in future, should just change requiredExperience to an NSArray.
                                      andMatchTypeID:[dataObj[@"match_type_id"] integerValue]
                                      andMatchTypeDesc:dataObj[@"match_type_description"]
                                      andNewsID:[object[@"newsfeed_id"] integerValue]
                                      andNumComments:[object[@"num_comments"] integerValue]
                                      andNumRemixes:[object[@"num_remixes"] integerValue]];
    [pNews.pickupMatch setGender:dataObj[@"gender"]];
  
    pNews.numLikes = [object[@"num_likes"] integerValue];
    pNews.doesSessionUserLikePost = [object[@"does_session_user_like_post"] boolValue];
    
    id subObj = object[@"remix_details"];
    
    [pNews.pickupMatch loadCompetitorArrayFromObject:dataObj[@"competitors"]];
    
    pNews.sharedByPlayer = [[Player alloc] initWithPlayerDetails:[subObj[@"user_id"] integerValue] andUserName:subObj[@"username"]];
    
    pNews.didSessionUserRemixPost = [object[@"did_session_user_remix_post"] boolValue];
    
  //  pNews.pickupMatch.matchHistoryArray = [[NSMutableArray alloc] init];
    [pNews.pickupMatch setMatchHistoryWithDataObj:dataObj[@"match_history"]];
   
    [self.newsPosts addObject:pNews];
    
}

- (void)loadIndividualMatches:(NSDictionary *)object
{
    id dataObj = object[@"data_obj"];
    
    NSDate *matchDateTime       = [MyHelpers getDateTimeFromString:dataObj[@"match_ts"]];
    NSDate *newsfeedDateTime    = [MyHelpers getDateTimeFromString:object[@"newsfeed_ts"]];
    
    Sport *sport = [[Sport alloc] initWithSportDetails:dataObj[@"sport_name"] andType:dataObj[@"sport_type"]];
    [sport setGameUnitName:dataObj[@"game_unit_name"]];
    [sport setSetUnitName:dataObj[@"set_unit_name"]];
    [sport setLargerScoreWins:[dataObj[@"score_order"] boolValue]];
    
    Venue *venue = [[Venue alloc] initVenueWithName:dataObj[@"venue_name"] andVenueID:[dataObj[@"venue_id"] integerValue]];
    
    
  //  NSDictionary *prob = dataObj[@"probabilities"];
    
  //  float prob_user1;
  //  float prob_user2;
    /*
    if ([prob[@"player1_id"] integerValue] == player1.userID) {
        prob_user1 = [prob[@"prob_player1"] floatValue];
        prob_user2 = [prob[@"prob_player2"] floatValue];
    } else {
        prob_user1 = [prob[@"prob_player2"] floatValue];
        prob_user2 = [prob[@"prob_player1"] floatValue];
    }*/
    
    
    
    IndividualMatch *individualMatch = [[IndividualMatch alloc] initMatchWithDetails:[dataObj[@"match_id"] integerValue]
                                                                        andMatchName:dataObj[@"match_name"]
                                                                           isPrivate:[dataObj[@"is_private"] boolValue]
                                                                           withSport:sport
                                                                         withMessage:dataObj[@"message"]
                                                                          onDateTime:matchDateTime
                                                                             atVenue:venue
                                                                       createdByUser:[dataObj[@"creator_id"] integerValue]
                                                                    withMatchPlayer1:nil
                                                                    withMatchPlayer2:nil
                                                                           andActive:YES
                                                           andProbabilityPlayer1Wins:0 // was prob_user1
                                                           andProbabilityPlayer2Wins:0]; // was prob_user2
    [individualMatch setMatchStatus:dataObj[@"match_status"]];
    
    
    /////////////NEW

    id scoreSummaryObject = dataObj[@"score_summary"];

    if (scoreSummaryObject[@"recorder_user_id"] != nil && ![scoreSummaryObject[@"recorder_user_id"] isKindOfClass:[NSNull class]]) {
        for (id roundScoreObj in scoreSummaryObject[@"match_scores"]) { // really should be no need to loop because only 1 version allowed for now.
        
            MatchPlayer *player1 = [[MatchPlayer alloc] initWithPlayerDetails:[roundScoreObj[@"player1_id"] integerValue]
                                                              andUserName:roundScoreObj[@"player1_username"]];
            
            MatchPlayer *player2 = [[MatchPlayer alloc] initWithPlayerDetails:[roundScoreObj[@"player2_id"] integerValue]
                                                              andUserName:roundScoreObj[@"player2_username"]];
            [player1 setProfileBaseString:roundScoreObj[@"player1_profile_pic_string"]];
            [player2 setProfileBaseString:roundScoreObj[@"player2_profile_pic_string"]];
        
            PlayerSport *ps1 = [[PlayerSport alloc] initWithSportDetails:[dataObj[@"sport_id"] integerValue]
                                                             andName:dataObj[@"sport_name"]
                                                             andType:@"individual"
                                                            andLevel:roundScoreObj[@"player1_level"]
                                                      andSportRecord:nil];
            [player1 addPlayerSport:ps1];
        
            PlayerSport *ps2 = [[PlayerSport alloc] initWithSportDetails:[dataObj[@"sport_id"] integerValue]
                                                             andName:dataObj[@"sport_name"]
                                                             andType:@"individual"
                                                            andLevel:roundScoreObj[@"player2_level"]
                                                      andSportRecord:nil];
            [player2 addPlayerSport:ps2];
        
            [individualMatch setPlayer1:player1];
            [individualMatch setPlayer2:player2];
        
            [individualMatch.scoreIndMatch addRoundScore:[roundScoreObj[@"round"] integerValue]
                                 forPlayer1:[roundScoreObj[@"player1_id"] integerValue]
                           withScorePlayer1:[roundScoreObj[@"score_player1"] integerValue]
                                 forPlayer2:[roundScoreObj[@"player2_id"] integerValue]
                           withScorePlayer2:[roundScoreObj[@"score_player2"] integerValue]];
            [individualMatch.scoreIndMatch setVersion:[scoreSummaryObject[@"version"] integerValue]];
            [individualMatch.scoreIndMatch setScoreRecordedBy:[scoreSummaryObject[@"recorder_user_id"] integerValue]];
            [individualMatch.scoreIndMatch setScoreConfirmedBy:[scoreSummaryObject[@"confirmer_user_id"] integerValue]];
            [individualMatch.scoreIndMatch setScoreDisputedBy:[scoreSummaryObject[@"disputor_user_id"] integerValue]];
            [individualMatch.scoreIndMatch setWasFirstVersionIndividualScoreRecordedBySessionUserAndDisputedByOtherUser:[scoreSummaryObject[@"was_session_user_score_overruled"] boolValue]];
        }
    } else {
        
        // START OLD
         MatchPlayer *player2 = [self makeMatchCompetitorObject:dataObj[@"competitorArray"]
         forSportName:dataObj[@"sport_name"]
         andType:dataObj[@"sport_type"]];
         
         
         MatchPlayer *player1 = [[MatchPlayer alloc] initWithPlayerDetails:[dataObj[@"creator_id"] integerValue]
         andProfileStr:dataObj[@"creator_profile_pic_string"]
         andUserName:dataObj[@"creator_username"]
         andFullName:nil
         andLocation:nil
         andAbout:nil
         andCoins:0
         andStatus:nil];
         
         Sport *player1Sport = [[PlayerSport alloc] initWithSportDetails:dataObj[@"sport_name"]
         andType:dataObj[@"sport_type"]
         andLevel:dataObj[@"creator_level"]
         andSportRecord:dataObj[@"win_loss_record"]];
         
         [player1.playerSports addObject:player1Sport];
        
        [individualMatch setPlayer1:player1];
        [individualMatch setPlayer2:player2];
         
         // END OLD

    }
    /////////////END NEW
    
    IndividualMatchCreationNews *iNews = [[IndividualMatchCreationNews alloc] initWithIndividualMatchCreationNewsDetails:individualMatch
                                                                                                    matchCreatorUserName:dataObj[@"creator_username"]
                                                                                                           andProfileStr:dataObj[@"creator_profile_pic_string"]
                                                                                                            newsPostDate:newsfeedDateTime
                                                                                                             andPostType: 2 // [object[@"post_type"] integerValue]
                                                                                                               andNewsID:[object[@"newsfeed_id"] integerValue]
                                                                                                          andNumComments:[object[@"num_comments"] integerValue]
                                                                                                           andNumRemixes:[object[@"num_remixes"] integerValue]];
    id subObj = object[@"remix_details"];
    
    iNews.sharedByPlayer = [[Player alloc] initWithPlayerDetails:[subObj[@"user_id"] integerValue] andUserName:subObj[@"username"]];
    
    iNews.didSessionUserRemixPost = [object[@"did_session_user_remix_post"] boolValue];
    [iNews setProfileBaseString:dataObj[@"creator_profile_pic_string"]];
    [iNews.individualMatch setMatchHistoryWithDataObj:dataObj[@"match_history"]];
    
    iNews.numLikes = [object[@"num_likes"] integerValue];
    iNews.doesSessionUserLikePost = [object[@"does_session_user_like_post"] boolValue];
    
    [self.newsPosts addObject:iNews];
 
}




- (void)loadTeamMatches:(NSDictionary *)object
{
    id dataObj = object[@"data_obj"];
    
    NSDate *matchDateTime       = [MyHelpers getDateTimeFromString:dataObj[@"match_ts"]];
    NSDate *newsfeedDateTime    = [MyHelpers getDateTimeFromString:object[@"newsfeed_ts"]];
    
    Sport *sport = [[Sport alloc] initWithSportDetails:dataObj[@"sport_name"] andType:dataObj[@"sport_type"]];
    [sport setGameUnitName:dataObj[@"game_unit_name"]];
    [sport setSetUnitName:dataObj[@"set_unit_name"]];
    [sport setLargerScoreWins:[dataObj[@"score_order"] boolValue]];
    
    Venue *venue = [[Venue alloc] initVenueWithName:dataObj[@"venue_name"] andVenueID:[dataObj[@"venue_id"] integerValue]];
    
    id team1Obj = dataObj[@"teams_array"][0];
    id team2Obj = dataObj[@"teams_array"][1];
    
    Team *team1 = [[Team alloc] initWithTeamDetails:[team1Obj[@"team_id"] integerValue]
                                        andTeamName:team1Obj[@"team_name"]
                                         andCaptain:nil
                                       andPicString:team1Obj[@"logo_string"]];
    
    Team *team2 = [[Team alloc] initWithTeamDetails:[team2Obj[@"team_id"] integerValue]
                                        andTeamName:team2Obj[@"team_name"]
                                         andCaptain:nil
                                       andPicString:team2Obj[@"logo_string"]];
    
    MatchType *mType = [[MatchType alloc] initWithMatchType:[dataObj[@"match_type_id"] integerValue]
                                            withDescription:dataObj[@"match_type_description"]];
    
    
    TeamMatch *tMatch = [[TeamMatch alloc] initMatchWithDetails:[dataObj[@"match_id"] integerValue]
                                                   andMatchName:dataObj[@"match_name"]
                                                      isPrivate:[dataObj[@"is_private"] boolValue]
                                                      withSport:sport
                                                    withMessage:dataObj[@"message"]
                                                     onDateTime:matchDateTime
                                                        atVenue:venue
                                                  createdByUser:[dataObj[@"creator_id"] integerValue]
                                                      withTeam1:team1
                                                      withTeam2:team2
                                                      andActive:YES
                                                   andMatchType:mType];
    
       [tMatch setMatchStatus:dataObj[@"match_status"]];
    
    /*
    
    /////////////NEW
    
    id scoreSummaryObject = dataObj[@"score_summary"];
    
    if (scoreSummaryObject[@"recorder_user_id"] != nil && ![scoreSummaryObject[@"recorder_user_id"] isKindOfClass:[NSNull class]]) {
        for (id roundScoreObj in scoreSummaryObject[@"match_scores"]) { // really should be no need to loop because only 1 version allowed for now.
            
            MatchPlayer *player1 = [[MatchPlayer alloc] initWithPlayerDetails:[roundScoreObj[@"player1_id"] integerValue]
                                                                  andUserName:roundScoreObj[@"player1_username"]];
            
            MatchPlayer *player2 = [[MatchPlayer alloc] initWithPlayerDetails:[roundScoreObj[@"player2_id"] integerValue]
                                                                  andUserName:roundScoreObj[@"player2_username"]];
            [player1 setProfileBaseString:roundScoreObj[@"player1_profile_pic_string"]];
            [player2 setProfileBaseString:roundScoreObj[@"player2_profile_pic_string"]];
            
            PlayerSport *ps1 = [[PlayerSport alloc] initWithSportDetails:[dataObj[@"sport_id"] integerValue]
                                                                 andName:dataObj[@"sport_name"]
                                                                 andType:@"individual"
                                                                andLevel:roundScoreObj[@"player1_level"]
                                                          andSportRecord:nil];
            [player1 addPlayerSport:ps1];
            
            PlayerSport *ps2 = [[PlayerSport alloc] initWithSportDetails:[dataObj[@"sport_id"] integerValue]
                                                                 andName:dataObj[@"sport_name"]
                                                                 andType:@"individual"
                                                                andLevel:roundScoreObj[@"player2_level"]
                                                          andSportRecord:nil];
            [player2 addPlayerSport:ps2];
            
            [individualMatch setPlayer1:player1];
            [individualMatch setPlayer2:player2];
            
            [individualMatch.scoreIndMatch addRoundScore:[roundScoreObj[@"round"] integerValue]
                                              forPlayer1:[roundScoreObj[@"player1_id"] integerValue]
                                        withScorePlayer1:[roundScoreObj[@"score_player1"] integerValue]
                                              forPlayer2:[roundScoreObj[@"player2_id"] integerValue]
                                        withScorePlayer2:[roundScoreObj[@"score_player2"] integerValue]];
            [individualMatch.scoreIndMatch setVersion:[scoreSummaryObject[@"version"] integerValue]];
            [individualMatch.scoreIndMatch setScoreRecordedBy:[scoreSummaryObject[@"recorder_user_id"] integerValue]];
            [individualMatch.scoreIndMatch setScoreConfirmedBy:[scoreSummaryObject[@"confirmer_user_id"] integerValue]];
            [individualMatch.scoreIndMatch setScoreDisputedBy:[scoreSummaryObject[@"disputor_user_id"] integerValue]];
            [individualMatch.scoreIndMatch setWasFirstVersionIndividualScoreRecordedBySessionUserAndDisputedByOtherUser:[scoreSummaryObject[@"was_session_user_score_overruled"] boolValue]];
        }
    } else {
        
        // START OLD
        MatchPlayer *player2 = [self makeMatchCompetitorObject:dataObj[@"competitorArray"]
                                                  forSportName:dataObj[@"sport_name"]
                                                       andType:dataObj[@"sport_type"]];
        
        
        MatchPlayer *player1 = [[MatchPlayer alloc] initWithPlayerDetails:[dataObj[@"creator_id"] integerValue]
                                                            andProfileStr:dataObj[@"creator_profile_pic_string"]
                                                              andUserName:dataObj[@"creator_username"]
                                                              andFullName:nil
                                                              andLocation:nil
                                                                 andAbout:nil
                                                                 andCoins:0
                                                                andStatus:nil];
        
        Sport *player1Sport = [[PlayerSport alloc] initWithSportDetails:dataObj[@"sport_name"]
                                                                andType:dataObj[@"sport_type"]
                                                               andLevel:dataObj[@"creator_level"]
                                                         andSportRecord:dataObj[@"win_loss_record"]];
        
        [player1.playerSports addObject:player1Sport];
        
        [individualMatch setPlayer1:player1];
        [individualMatch setPlayer2:player2];
        
        // END OLD
        
    }
    /////////////END NEW
     */
    
    TeamMatchCreationNews *tNews = [[TeamMatchCreationNews alloc] initWithIndividualMatchCreationNewsDetails:tMatch
                                                                                        matchCreatorUserName:dataObj[@"creator_username"]
                                                                                               andProfileStr:dataObj[@"creator_profile_pic_string"]
                                                                                                newsPostDate:newsfeedDateTime
                                                                                                 andPostType: TEAM_MATCH_CREATION
                                                                                                   andNewsID:[object[@"newsfeed_id"] integerValue]
                                                                                              andNumComments:[object[@"num_comments"] integerValue]
                                                                                               andNumRemixes:[object[@"num_remixes"] integerValue]];
    id subObj = object[@"remix_details"];
    
    tNews.sharedByPlayer = [[Player alloc] initWithPlayerDetails:[subObj[@"user_id"] integerValue] andUserName:subObj[@"username"]];
    
    tNews.didSessionUserRemixPost = [object[@"did_session_user_remix_post"] boolValue];
    [tNews setProfileBaseString:dataObj[@"creator_profile_pic_string"]];
    [tNews.tMatch setMatchHistoryWithDataObj:dataObj[@"match_history"]];
    
    tNews.numLikes = [object[@"num_likes"] integerValue];
    tNews.doesSessionUserLikePost = [object[@"does_session_user_like_post"] boolValue];
    
    [self.newsPosts addObject:tNews];
    
}

- (void)loadUserGeneratedPosts:(NSDictionary *)object
{
    id dataObj = object[@"data_obj"];
 
    NSDate *newsfeedDateTime    = [MyHelpers getDateTimeFromString:object[@"newsfeed_ts"]];
 
    if (dataObj[@"file_type"] ==  [NSNull null]) { // most likely a text post
 
        TextPost *tPost = [[TextPost alloc] initWithNewsDetails:[object[@"post_user_id"] integerValue]
                                                    andUserName:object[@"post_username"]
                                                  andProfileStr:object[@"profile_pic_string"]
                                                       withDate:newsfeedDateTime
                                                    andPostType:[object[@"post_type"] integerValue]
                                                      andNewsID:[object[@"newsfeed_id"] integerValue]
                                                    andTextPost:dataObj[@"text_post"]
                                                 andNumComments:[object[@"num_comments"] integerValue]
                                                  andNumRemixes:[object[@"num_remixes"] integerValue]];
        
        id subObj = object[@"remix_details"];
                
        tPost.sharedByPlayer = [[Player alloc] initWithPlayerDetails:[subObj[@"user_id"] integerValue] andUserName:subObj[@"username"]];
        tPost.didSessionUserRemixPost = [object[@"did_session_user_remix_post"] boolValue];
        
        tPost.numLikes = [object[@"num_likes"] integerValue];
        tPost.doesSessionUserLikePost = [object[@"does_session_user_like_post"] boolValue];
        
        [self.newsPosts addObject:tPost];
        
    } else if ([dataObj[@"file_type"] isEqualToString:@"photo"]) {
        
        
        ImagePost *iPost = [[ImagePost alloc] initWithNewsDetails:[object[@"post_user_id"] integerValue]
                                                      andUserName:object[@"post_username"]
                                                    andProfileStr:object[@"profile_pic_string"]
                                                         withDate:newsfeedDateTime
                                                      andPostType:[object[@"post_type"] integerValue]
                                                        andNewsID:[object[@"newsfeed_id"] integerValue]
                                                      andTextPost:dataObj[@"text_post"]
                                               andImageFileString:dataObj[@"filestring"]
                                                   andNumComments:[object[@"num_comments"] integerValue]
                                                    andNumRemixes:[object[@"num_remixes"] integerValue]];
        
        id subObj = object[@"remix_details"];
        
        iPost.sharedByPlayer = [[Player alloc] initWithPlayerDetails:[subObj[@"user_id"] integerValue] andUserName:subObj[@"username"]];
        iPost.didSessionUserRemixPost = [object[@"did_session_user_remix_post"] boolValue];
        
        iPost.numLikes = [object[@"num_likes"] integerValue];
        iPost.doesSessionUserLikePost = [object[@"does_session_user_like_post"] boolValue];
        
        [self.newsPosts addObject:iPost];
        
    } else if ([dataObj[@"file_type"] isEqualToString:@"highlight_video"]) {
        
    /*    NSURL *videoURL = [NSURL URLWithString:[S3Tools getFileNameStringWithType:@"highlight_video"
                                                                       baseString:dataObj[@"filestring"]]];
        UIImage *thumb = [[UIImage imageFromMovie2:videoURL] imageCroppedToFitSize:CGSizeMake(70.0, 70.0)];
        
        VideoPost *vPost = [[VideoPost alloc] initWithNewsDetails:[object[@"post_user_id"] integerValue]
                                                      andUserName:object[@"post_username"]
                                                    andProfileStr:object[@"profile_pic_string"]
                                                         withDate:newsfeedDateTime
                                                      andPostType:[object[@"post_type"] integerValue]
                                                        andNewsID:[object[@"newsfeed_id"] integerValue]
                                                      andTextPost:dataObj[@"text_post"]
                                               andVideoFileString:dataObj[@"filestring"]
                                                andThumbnailImage:thumb
                                                     andVideoType:dataObj[@"file_type"]
                                                   andNumComments:[object[@"num_comments"] integerValue]
                                                    andNumRemixes:[object[@"num_remixes"] integerValue]];
        vPost.didSessionUserRemixPost = [object[@"did_session_user_remix_post"] boolValue];
        [self.newsPosts addObject:vPost];
      */
    } else if ([dataObj[@"file_type"] isEqualToString:@"trick_video"]) {
  /*      NSURL *videoURL = [NSURL URLWithString:[S3Tools getFileNameStringWithType:@"trick_video"
                                                                       baseString:dataObj[@"filestring"]]];
        UIImage *thumb = [[UIImage imageFromMovie2:videoURL] imageCroppedToFitSize:CGSizeMake(70.0, 70.0)];
        
        VideoPost *vPost = [[VideoPost alloc] initWithNewsDetails:[object[@"post_user_id"] integerValue]
                                                      andUserName:object[@"post_username"]
                                                    andProfileStr:object[@"profile_pic_string"]
                                                         withDate:newsfeedDateTime
                                                      andPostType:[object[@"post_type"] integerValue]
                                                        andNewsID:[object[@"newsfeed_id"] integerValue]
                                                      andTextPost:dataObj[@"text_post"]
                                               andVideoFileString:dataObj[@"filestring"]
                                                andThumbnailImage:thumb
                                                     andVideoType:dataObj[@"file_type"]
                                                   andNumComments:[object[@"num_comments"] integerValue]
                                                    andNumRemixes:[object[@"num_remixes"] integerValue]];
        vPost.didSessionUserRemixPost = [object[@"did_session_user_remix_post"] boolValue];
        [self.newsPosts addObject:vPost];
        
  */
        
    }
    
    
    
}

- (void)loadLockerPost:(NSDictionary *)object
{
    id dataObj = object[@"data_obj"];
    
    NSDate *newsfeedDateTime    = [MyHelpers getDateTimeFromString:object[@"newsfeed_ts"]];

    
    LockerPost *iPost = [[LockerPost alloc] initWithNewsDetails:[object[@"post_user_id"] integerValue]
                                                  andUserName:object[@"post_username"]
                                                andProfileStr:object[@"profile_pic_string"]
                                                     withDate:newsfeedDateTime
                                                  andPostType:[object[@"post_type"] integerValue]
                                                    andNewsID:[object[@"newsfeed_id"] integerValue]
                                                  andTextPost:dataObj[@"text_post"]
                                           andImageFileString:dataObj[@"filestring"]
                                               andNumComments:[object[@"num_comments"] integerValue]
                                                andNumRemixes:[object[@"num_remixes"] integerValue]
                                                    andLockerID:[dataObj[@"locker_id"] integerValue]];
    
    id subObj = object[@"remix_details"];
    
    iPost.sharedByPlayer = [[Player alloc] initWithPlayerDetails:[subObj[@"user_id"] integerValue] andUserName:subObj[@"username"]];
    iPost.didSessionUserRemixPost = [object[@"did_session_user_remix_post"] boolValue];
    
    iPost.numLikes = [object[@"num_likes"] integerValue];
    iPost.doesSessionUserLikePost = [object[@"does_session_user_like_post"] boolValue];
    
 /*   for (id statObj in dataObj[@"stat_tags"]) {
        StatTag *tag = [[StatTag alloc] initWithStatTag:[statObj[@"stat_id"] integerValue]
                                         andStatTagName:statObj[@"stat_name"]
                                        andStatTagValue:statObj[@"stat_value"]
                                            andStatLink:statObj[@"stat_link"]
                                        andStatLinkType:statObj[@"stat_linktype"]
                                         andSubCategory:statObj[@"stat_subcategory"]
                                     andShowSubcategory:[statObj[@"stat_showsubcategory"] boolValue]];
        [iPost.lockPic.statsTagsArray addObject:tag];
    } */

    iPost.lockPic.statsGrp = [[StatsGroup alloc] initWithServerObject:dataObj[@"stat_tags"]];
    
    for (id trainingObj in dataObj[@"training_tags"]) {
        TrainingTag *tag = [[TrainingTag alloc] initWithTrainingTagInfo:[trainingObj[@"training_id"] integerValue]
                                                     andTrainingTagName:trainingObj[@"training_name"]];
        [iPost.lockPic.trainingTagsArray addObject:tag];
    }
    
    [self.newsPosts addObject:iPost];

}

- (void)loadVoteNews:(NSDictionary *)object withType:(NSString *)type
{
    id dataObj = object[@"data_obj"];
    
    NSDate *newsfeedDateTime = [MyHelpers getDateTimeFromString:object[@"newsfeed_ts"]];
    
    VoteReceiving *iPost;
    if ([type isEqualToString:@"winner"]) {
        iPost = [[VoteReceiving alloc] initWithNewsDetails:[object[@"post_user_id"] integerValue]
                                               andUserName:object[@"post_username"]
                                             andProfileStr:object[@"profile_pic_string"]
                                                  withDate:newsfeedDateTime
                                               andPostType:[object[@"post_type"] integerValue]
                                                 andNewsID:[object[@"newsfeed_id"] integerValue]
                                               andTextPost:nil
                                            andNumComments:[object[@"num_comments"] integerValue]
                                             andNumRemixes:[object[@"num_remixes"] integerValue]
                                                 andwinner:dataObj[@"winner"]
                                                   andType:type
                                             andUserRegion:dataObj[@"self_region"]];
    } else {
        iPost = [[VoteReceiving alloc] initWithNewsDetails:[object[@"post_user_id"] integerValue]
                                               andUserName:object[@"post_username"]
                                             andProfileStr:object[@"profile_pic_string"]
                                                  withDate:newsfeedDateTime
                                               andPostType:[object[@"post_type"] integerValue]
                                                 andNewsID:[object[@"newsfeed_id"] integerValue]
                                               andTextPost:dataObj[@"text_post"]
                                               andTargetID:[dataObj[@"target_user_id"] integerValue]
                                         andTargetUsername:dataObj[@"target_username"]
                                       andtargetProfileStr:dataObj[@"target_profile_base_string"]
                                            andNumComments:[object[@"num_comments"] integerValue]
                                             andNumRemixes:[object[@"num_remixes"] integerValue]
                                               andLockerID:[dataObj[@"locker_id"] integerValue]
                                                   andType:type];
    }
    
    id subObj = object[@"remix_details"];
    
    iPost.sharedByPlayer = [[Player alloc] initWithPlayerDetails:[subObj[@"user_id"] integerValue] andUserName:subObj[@"username"]];
    iPost.didSessionUserRemixPost = [object[@"did_session_user_remix_post"] boolValue];
    
    iPost.numLikes = [object[@"num_likes"] integerValue];
    iPost.doesSessionUserLikePost = [object[@"does_session_user_like_post"] boolValue];
    
    [self.newsPosts addObject:iPost];
}

// For Match Cancelling, Ready to go Confirmations

- (void)loadMatchNewsUpdate:(NSDictionary *)object
{
    id dataObj = object[@"data_obj"];
    
    NSDate *newsDateTime    = [MyHelpers getDateTimeFromString:object[@"newsfeed_ts"]];
    
    if ([dataObj[@"match_category"] isEqualToString:@"individual"]) {
        Sport *sp = [[Sport alloc] initWithSportDetails:[dataObj[@"sport_id"] integerValue]
                                                andName:dataObj[@"sport_name"]
                                                andType:@"individual"];
        Venue *vn = [[Venue alloc] initVenueWithName:dataObj[@"venue_name"]
                                          andVenueID:[dataObj[@"venue_id"] integerValue]];
        
        NSDate *matchDateTime = [MyHelpers getDateTimeFromString:dataObj[@"match_date_time"]];
        
        IndividualMatch *iMatch = [[IndividualMatch alloc] init];
        [iMatch setMatchID:[dataObj[@"match_id"] integerValue]];
        [iMatch setMatchName:dataObj[@"match_name"]];
        [iMatch setMatchStatus:dataObj[@"match_status"]];
        [iMatch setSport:sp];
        [iMatch setVenue:vn];
        [iMatch setDateTime:matchDateTime];
        [iMatch setMatchCreatorID:[dataObj[@"creator_id"] integerValue]];
        [iMatch loadCompetitorArrayFromObject:dataObj[@"competitors"]];
        
        MatchNews *mn = [[MatchNews alloc] initWithDetails:iMatch
                                                  postDate:newsDateTime
                                           headLineMessage:@"Match Update"
                                            generalMessage:dataObj[@"general_message"]];
        [mn setNumComments:[object[@"num_comments"] integerValue]];
        [mn setNumRemixes:[object[@"num_remixes"] integerValue]];
        [mn setNewsID:[object[@"newsfeed_id"] integerValue]];
        
        id subObj = object[@"remix_details"];
        
        mn.sharedByPlayer = [[Player alloc] initWithPlayerDetails:[subObj[@"user_id"] integerValue] andUserName:subObj[@"username"]];
        mn.didSessionUserRemixPost = [object[@"did_session_user_remix_post"] boolValue];
        
        mn.numLikes = [object[@"num_likes"] integerValue];
        mn.doesSessionUserLikePost = [object[@"does_session_user_like_post"] boolValue];
        
        
        [self.newsPosts addObject:mn];
        
    } else if ([dataObj[@"match_category"] isEqualToString:@"pickup"]) {
        Sport *sp = [[Sport alloc] initWithSportDetails:[dataObj[@"sport_id"] integerValue]
                                                andName:dataObj[@"sport_name"]
                                                andType:@"pickup"];
        Venue *vn = [[Venue alloc] initVenueWithName:dataObj[@"venue_name"]
                                          andVenueID:[dataObj[@"venue_id"] integerValue]];
        
        NSDate *matchDateTime = [MyHelpers getDateTimeFromString:dataObj[@"match_date_time"]];
        
        PickupMatch *pMatch = [[PickupMatch alloc] init];
        [pMatch setMatchID:[dataObj[@"match_id"] integerValue]];
        [pMatch setMatchName:dataObj[@"match_name"]];
        [pMatch setMatchStatus:dataObj[@"match_status"]];
        [pMatch setSport:sp];
        [pMatch setVenue:vn];
        [pMatch setDateTime:matchDateTime];
        [pMatch loadCompetitorArrayFromObject:dataObj[@"competitors"]];
        
        MatchNews *mn = [[MatchNews alloc] initWithDetails:pMatch
                                                  postDate:newsDateTime
                                           headLineMessage:@"Match Update"
                                            generalMessage:dataObj[@"general_message"]];
        
        [mn setNumComments:[object[@"num_comments"] integerValue]];
        [mn setNumRemixes:[object[@"num_remixes"] integerValue]];
        [mn setNewsID:[object[@"newsfeed_id"] integerValue]];
        
        id subObj = object[@"remix_details"];
        
        mn.sharedByPlayer = [[Player alloc] initWithPlayerDetails:[subObj[@"user_id"] integerValue] andUserName:subObj[@"username"]];
        mn.didSessionUserRemixPost = [object[@"did_session_user_remix_post"] boolValue];
        
        mn.numLikes = [object[@"num_likes"] integerValue];
        mn.doesSessionUserLikePost = [object[@"does_session_user_like_post"] boolValue];
        
        [self.newsPosts addObject:mn];
        
    }

}

// For updates regarding to players joining or declining matches

- (void)loadMatchNewsPlayerUpdate:(NSDictionary *)object
{
    id dataObj = object[@"data_obj"];
    
    /*'match_id' => $row['match_id'],
     'match_name' => $row['match_name'],
     'sport_id' => $row['sport_id'],
     'general_message' => $row['general_message'],
     'comment' => $row['comment'],
     'player_id' => $row['player_id'],
     'username' => $row['username'],
     'post_ts' => $row['post_ts']
     */
    
    if ([object[@"newsfeed_id"] integerValue] == 134) {
        
    }
    NSDate *newsDateTime = [MyHelpers getDateTimeFromString:object[@"newsfeed_ts"]];
    
    if ([dataObj[@"match_category"] isEqualToString:@"individual"]) {
        
        Sport *sp = [[Sport alloc] initWithSportDetails:[dataObj[@"sport_id"] integerValue]
                                                andName:dataObj[@"sport_name"]
                                                andType:@"individual"];
        
        IndividualMatch *iMatch = [[IndividualMatch alloc] init];
        [iMatch setMatchID:[dataObj[@"match_id"] integerValue]];
        [iMatch setMatchName:dataObj[@"match_name"]];
        [iMatch setMatchStatus:dataObj[@"match_status"]];
        [iMatch setMatchCreatorID:[dataObj[@"creator_id"] integerValue]];
        [iMatch loadCompetitorArrayFromObject:dataObj[@"competitors"]];

        [iMatch setSport:sp];
        
        Player *player = [[Player alloc] initWithPlayerDetails:[dataObj[@"player_id"] integerValue]
                                                   andUserName:dataObj[@"username"]
                                           andProfilePicString:dataObj[@"profile_pic_string"]];
        
        PlayerSport *ps = [[PlayerSport alloc] initWithSportDetails:dataObj[@"sport_name"]
                                                            andType:@"individual"
                                                           andLevel:dataObj[@"player_sport_level"]
                                                     andSportRecord:nil];
        [ps setSportID:[dataObj[@"sport_id"] integerValue]];
        
        [player addPlayerSport:ps];
        
        MatchPlayerNews *mpn = [[MatchPlayerNews alloc] initWithDetails:iMatch
                                                               postDate:newsDateTime
                                                        headLineMessage:@"Match Update"
                                                         generalMessage:dataObj[@"general_message"]
                                                              forPlayer:player
                                                            withMessage:dataObj[@"comment"]];
        
        [mpn setNumComments:[object[@"num_comments"] integerValue]];
        [mpn setNumRemixes:[object[@"num_remixes"] integerValue]];
        [mpn setNewsID:[object[@"newsfeed_id"] integerValue]];
        
        id subObj = object[@"remix_details"];
        
        mpn.sharedByPlayer = [[Player alloc] initWithPlayerDetails:[subObj[@"user_id"] integerValue] andUserName:subObj[@"username"]];
        mpn.didSessionUserRemixPost = [object[@"did_session_user_remix_post"] boolValue];
        
        mpn.numLikes = [object[@"num_likes"] integerValue];
        mpn.doesSessionUserLikePost = [object[@"does_session_user_like_post"] boolValue];
        
        
        [self.newsPosts addObject:mpn];
        
    } else if ([dataObj[@"match_category"] isEqualToString:@"pickup"]) {
        
        Sport *sp = [[Sport alloc] initWithSportDetails:[dataObj[@"sport_id"] integerValue]
                                                andName:dataObj[@"sport_name"]
                                                andType:@"individual"];

        
        PickupMatch *pMatch = [[PickupMatch alloc] init];
        [pMatch setMatchID:[dataObj[@"match_id"] integerValue]];
        [pMatch setMatchName:dataObj[@"match_name"]];
        [pMatch setMatchStatus:dataObj[@"match_status"]];
        [pMatch setSport:sp];
        [pMatch loadCompetitorArrayFromObject:dataObj[@"competitors"]];

        
        Player *player = [[Player alloc] initWithPlayerDetails:[dataObj[@"player_id"] integerValue]
                                                   andUserName:dataObj[@"username"]
                                           andProfilePicString:dataObj[@"profile_pic_string"]];
        
        PlayerSport *ps = [[PlayerSport alloc] initWithSportDetails:dataObj[@"sport_name"]
                                                            andType:@"individual"
                                                           andLevel:dataObj[@"player_sport_level"]
                                                     andSportRecord:nil];
        [ps setSportID:[dataObj[@"sport_id"] integerValue]];
        [player addPlayerSport:ps];
        
        
        
        MatchPlayerNews *mpn = [[MatchPlayerNews alloc] initWithDetails:pMatch
                                                               postDate:newsDateTime
                                                        headLineMessage:@"Match Update"
                                                         generalMessage:dataObj[@"general_message"]
                                                              forPlayer:player
                                                            withMessage:dataObj[@"comment"]];
        [mpn setNewsID:[object[@"newsfeed_id"] integerValue]];
        [mpn setNumComments:[object[@"num_comments"] integerValue]];
        [mpn setNumRemixes:[object[@"num_remixes"] integerValue]];
        
        id subObj = object[@"remix_details"];
        
        mpn.sharedByPlayer = [[Player alloc] initWithPlayerDetails:[subObj[@"user_id"] integerValue] andUserName:subObj[@"username"]];
        mpn.didSessionUserRemixPost = [object[@"did_session_user_remix_post"] boolValue];
        
        mpn.numLikes = [object[@"num_likes"] integerValue];
        mpn.doesSessionUserLikePost = [object[@"does_session_user_like_post"] boolValue];

        
        [mpn setNewsID:[object[@"newsfeed_id"] integerValue]];
        [self.newsPosts addObject:mpn];
    }
}


- (void)loadMatchNewsPlayerScoreUpdate:(NSDictionary *)object
{
    id dataObj = object[@"data_obj"];
    
    NSDate *newsDateTime = [MyHelpers getDateTimeFromString:object[@"newsfeed_ts"]];
    
    if ([dataObj[@"match_category"] isEqualToString:@"individual"]) {
        
        Sport *sp = [[Sport alloc] initWithSportDetails:[dataObj[@"sport_id"] integerValue]
                                                andName:dataObj[@"sport_name"]
                                                andType:@"individual"];
        [sp setGameUnitName:dataObj[@"game_unit_name"]];
        [sp setSetUnitName:dataObj[@"set_unit_name"]];
        [sp setLargerScoreWins:[dataObj[@"score_order"] boolValue]];
        
        IndividualMatch *iMatch = [[IndividualMatch alloc] init];
        [iMatch setMatchID:[dataObj[@"match_id"] integerValue]];
        [iMatch setMatchName:dataObj[@"match_name"]];
        [iMatch setMatchStatus:dataObj[@"match_status"]];
//        [iMatch setMatchCreatorID:[dataObj[@"creator_id"] integerValue]];
//        [iMatch loadCompetitorArrayFromObject:dataObj[@"competitors"]];
        [iMatch setSport:sp];
        
        
        Player *player = [[Player alloc] initWithPlayerDetails:[dataObj[@"player_id"] integerValue]
                                                   andUserName:dataObj[@"username"]
                                           andProfilePicString:dataObj[@"profile_pic_string"]];
        
  
        id scoreSummaryObject = dataObj[@"score_summary"];
        
        for (id roundScoreObj in scoreSummaryObject[@"match_scores"]) { // really should be no need to loop because only 1 version allowed for now.
            
            MatchPlayer *player1 = [[MatchPlayer alloc] initWithPlayerDetails:[roundScoreObj[@"player1_id"] integerValue]
                                                        andUserName:roundScoreObj[@"player1_username"]];
            MatchPlayer *player2 = [[MatchPlayer alloc] initWithPlayerDetails:[roundScoreObj[@"player2_id"] integerValue]
                                                        andUserName:roundScoreObj[@"player2_username"]];
        
            [player1 setProfileBaseString:roundScoreObj[@"player1_profile_pic_string"]];
            [player2 setProfileBaseString:roundScoreObj[@"player2_profile_pic_string"]];
            
            [iMatch setPlayer1:player1];
            [iMatch setPlayer2:player2];
            
            [iMatch.scoreIndMatch addRoundScore:[roundScoreObj[@"round"] integerValue]
                                                   forPlayer1:[roundScoreObj[@"player1_id"] integerValue]
                                             withScorePlayer1:[roundScoreObj[@"score_player1"] integerValue]
                                                   forPlayer2:[roundScoreObj[@"player2_id"] integerValue]
                                             withScorePlayer2:[roundScoreObj[@"score_player2"] integerValue]];
            [iMatch.scoreIndMatch setVersion:[scoreSummaryObject[@"version"] integerValue]];
            [iMatch.scoreIndMatch setScoreRecordedBy:[scoreSummaryObject[@"recorder_user_id"] integerValue]];
            [iMatch.scoreIndMatch setScoreConfirmedBy:[scoreSummaryObject[@"confirmer_user_id"] integerValue]];
            [iMatch.scoreIndMatch setScoreDisputedBy:[scoreSummaryObject[@"disputor_user_id"] integerValue]];
            [iMatch.scoreIndMatch setWasFirstVersionIndividualScoreRecordedBySessionUserAndDisputedByOtherUser:[scoreSummaryObject[@"was_session_user_score_overruled"] boolValue]];
        }

        
        MatchPlayerNews *mpn = [[MatchPlayerNews alloc] initWithDetails:iMatch
                                                               postDate:newsDateTime
                                                        headLineMessage:@"Match Update"
                                                         generalMessage:dataObj[@"general_message"]
                                                              forPlayer:player
                                                            withMessage:dataObj[@"comment"]];
        
      
        [mpn setNumComments:[object[@"num_comments"] integerValue]];
        [mpn setNumRemixes:[object[@"num_remixes"] integerValue]];
        [mpn setNewsID:[object[@"newsfeed_id"] integerValue]];
        
        id subObj = object[@"remix_details"];
        
        mpn.sharedByPlayer = [[Player alloc] initWithPlayerDetails:[subObj[@"user_id"] integerValue] andUserName:subObj[@"username"]];
        mpn.didSessionUserRemixPost = [object[@"did_session_user_remix_post"] boolValue];
        
        mpn.numLikes = [object[@"num_likes"] integerValue];
        mpn.doesSessionUserLikePost = [object[@"does_session_user_like_post"] boolValue];

        
        [self.newsPosts addObject:mpn];
        
    }
}

- (void)loadMatchPlayerNewsOfficialFinishedIndividualMatch:(NSDictionary *)object
{
    id dataObj = object[@"data_obj"];
    
    NSDate *newsDateTime = [MyHelpers getDateTimeFromString:object[@"newsfeed_ts"]];
    
    if ([dataObj[@"match_category"] isEqualToString:@"individual"]) {
        
        Sport *sp = [[Sport alloc] initWithSportDetails:[dataObj[@"sport_id"] integerValue]
                                                andName:dataObj[@"sport_name"]
                                                andType:@"individual"];
        [sp setGameUnitName:dataObj[@"game_unit_name"]];
        [sp setSetUnitName:dataObj[@"set_unit_name"]];
        [sp setLargerScoreWins:[dataObj[@"score_order"] boolValue]];
        
        IndividualMatch *iMatch = [[IndividualMatch alloc] init];
        [iMatch setMatchID:[dataObj[@"match_id"] integerValue]];
        [iMatch setMatchName:dataObj[@"match_name"]];
        [iMatch setMatchStatus:dataObj[@"match_status"]];
        [iMatch setSport:sp];
        
        Player *player = [[Player alloc] initWithPlayerDetails:[dataObj[@"player_id"] integerValue]
                                                   andUserName:dataObj[@"username"]
                                           andProfilePicString:dataObj[@"profile_pic_string"]];
        
        
        id scoreSummaryObject = dataObj[@"score_summary"];
      
        
        for (id roundScoreObj in scoreSummaryObject[@"match_scores"]) { // really should be no need to loop because only 1 version allowed for now.
            
            MatchPlayer *player1 = [[MatchPlayer alloc] initWithPlayerDetails:[roundScoreObj[@"player1_id"] integerValue]
                                                                  andUserName:roundScoreObj[@"player1_username"]];
            MatchPlayer *player2 = [[MatchPlayer alloc] initWithPlayerDetails:[roundScoreObj[@"player2_id"] integerValue]
                                                                  andUserName:roundScoreObj[@"player2_username"]];
            [player1 setProfileBaseString:roundScoreObj[@"player1_profile_pic_string"]];
            [player2 setProfileBaseString:roundScoreObj[@"player2_profile_pic_string"]];
            
            PlayerSport *ps1 = [[PlayerSport alloc] initWithSportDetails:[dataObj[@"sport_id"] integerValue]
                                                                 andName:dataObj[@"sport_name"]
                                                                 andType:@"individual"
                                                                andLevel:roundScoreObj[@"player1_level"]
                                                          andSportRecord:nil];
            [player1 addPlayerSport:ps1];
            
            PlayerSport *ps2 = [[PlayerSport alloc] initWithSportDetails:[dataObj[@"sport_id"] integerValue]
                                                                 andName:dataObj[@"sport_name"]
                                                                 andType:@"individual"
                                                                andLevel:roundScoreObj[@"player2_level"]
                                                          andSportRecord:nil];
            [player2 addPlayerSport:ps2];
            
            [iMatch setPlayer1:player1];
            [iMatch setPlayer2:player2];
            
            [iMatch.scoreIndMatch addRoundScore:[roundScoreObj[@"round"] integerValue]
                                     forPlayer1:[roundScoreObj[@"player1_id"] integerValue]
                               withScorePlayer1:[roundScoreObj[@"score_player1"] integerValue]
                                     forPlayer2:[roundScoreObj[@"player2_id"] integerValue]
                               withScorePlayer2:[roundScoreObj[@"score_player2"] integerValue]];
            [iMatch.scoreIndMatch setVersion:[scoreSummaryObject[@"version"] integerValue]];
            [iMatch.scoreIndMatch setScoreRecordedBy:[scoreSummaryObject[@"recorder_user_id"] integerValue]];
            [iMatch.scoreIndMatch setScoreConfirmedBy:[scoreSummaryObject[@"confirmer_user_id"] integerValue]];
            [iMatch.scoreIndMatch setScoreDisputedBy:[scoreSummaryObject[@"disputor_user_id"] integerValue]];
            [iMatch.scoreIndMatch setWasFirstVersionIndividualScoreRecordedBySessionUserAndDisputedByOtherUser:[scoreSummaryObject[@"was_session_user_score_overruled"] boolValue]];
        }
        
        iMatch.matchHistoryArray = [[NSMutableArray alloc] init];
        [iMatch setMatchHistoryWithDataObj:dataObj[@"match_history"]];

        
        MatchPlayerNewsOfficialFinishedIndividualMatch *mpn = [[MatchPlayerNewsOfficialFinishedIndividualMatch alloc] initWithDetails:iMatch
                                                               postDate:newsDateTime
                                                        headLineMessage:@"Match Update"
                                                         generalMessage:dataObj[@"general_message"]
                                                              forPlayer:player
                                                            withMessage:dataObj[@"comment"]
                                                               andCoinTransferValue:[dataObj[@"coins_earned"] integerValue]];
        
        [mpn.match setMatchCreatorID:[dataObj[@"creator_id"] integerValue]];
        [mpn.match setMatchCreatorUsername:[iMatch getPlayerForID:[dataObj[@"creator_id"] integerValue]].userName];
        
        [mpn setNumComments:[object[@"num_comments"] integerValue]];
        [mpn setNumRemixes:[object[@"num_remixes"] integerValue]];
        [mpn setNewsID:[object[@"newsfeed_id"] integerValue]];
        
        id subObj = object[@"remix_details"];
        
        mpn.sharedByPlayer = [[Player alloc] initWithPlayerDetails:[subObj[@"user_id"] integerValue] andUserName:subObj[@"username"]];
        mpn.didSessionUserRemixPost = [object[@"did_session_user_remix_post"] boolValue];
        
        mpn.numLikes = [object[@"num_likes"] integerValue];
        mpn.doesSessionUserLikePost = [object[@"does_session_user_like_post"] boolValue];

        [self.newsPosts addObject:mpn];
        
    }
}

- (MatchPlayer *)makeMatchCompetitorObject:(id)object forSportName:(NSString *)sportName andType:(NSString *)sportType
{
    
    MatchPlayer *player = [[MatchPlayer alloc] initWithPlayerDetails:[object[@"competitor_id"] integerValue]
                                                       andProfileStr:object[@"profile_pic_string"]
                                                         andUserName:object[@"username"]
                                                         andFullName:nil
                                                         andLocation:nil
                                                            andAbout:nil
                                                            andCoins:0
                                                           andStatus:nil];
    
    
    PlayerSport *sport = [[PlayerSport alloc] initWithSportDetails:sportName
                                                           andType:sportType
                                                          andLevel:object[@"level"]
                                                    andSportRecord:object[@"win_loss_record"]];
    [player.playerSports addObject:sport];
    
    
    return player;
}

@end
