//
//  ImagePost.h
//  Phenom
//
//  Created by James Chung on 5/28/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "News.h"

@interface ImagePost : News

@property (nonatomic, strong) NSString *imageBaseFileString;

- (ImagePost *)initWithNewsDetails:(NSInteger)newsMakerUserID
                       andUserName:(NSString *)newsMakerUserName
                     andProfileStr:(NSString *)profileBaseString
                          withDate:(NSDate *)newsDate
                       andPostType:(NSInteger)postType
                         andNewsID:(NSInteger)newsID
                       andTextPost:(NSString *)textPost
                andImageFileString:(NSString *)imageBaseFileString
                    andNumComments:(NSUInteger)numComments
                     andNumRemixes:(NSUInteger)numRemixes;

@end
