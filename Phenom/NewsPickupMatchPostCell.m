//
//  NewPickupMatchPostCell.m
//  Phenom
//
//  Created by James Chung on 4/23/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "NewsPickupMatchPostCell.h"
#import "NewsfeedMatchHeadingCVCell.h"
#import "Player.h"
#import "S3Tools.h"
#import "AFNetworking.h"
#import "UIImage+ProportionalFill.h"

@implementation NewsPickupMatchPostCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
/*
- (void)setFrame:(CGRect)frame
{
    frame = CGRectMake(- [self cellsMargin] , frame.origin.y, self.myTableView.frame.size.width + 2 *[self cellsMargin], frame.size.height);
    self.contentView.frame = frame;
    [super setFrame:frame];
}
*/
- (CGFloat)cellsMargin {
    
    return 4;
    
    /*    // No margins for plain table views
     if (self.myTableView.style == UITableViewStylePlain) {
     return 0;
     }
     
     // iPhone always have 10 pixels margin
     if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
     {
     return 10;
     }
     
     CGFloat tableWidth = self.myTableView.frame.size.width;
     
     // Really small table
     if (tableWidth <= 20) {
     return tableWidth - 10;
     }
     
     // Average table size
     if (tableWidth < 400) {
     return 10;
     }
     
     // Big tables have complex margin's logic
     // Around 6% of table width,
     // 31 <= tableWidth * 0.06 <= 45
     
     CGFloat marginWidth  = tableWidth * 0.06;
     marginWidth = MAX(31, MIN(45, marginWidth));
     return marginWidth;
     */
}

- (void)layoutSubviews
{
    self.matchHeadingCV.delegate = self;
    self.matchHeadingCV.dataSource = self;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    if ([self.matchCategory isEqualToString:@"individual"]) {
        return 3;
    } else {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"status = %@", @"ACCEPTED"];
        NSArray *matchParticipants = [self.competitors filteredArrayUsingPredicate:predicate];
        
        return [matchParticipants count];
    }
    
    return 0;
}

- (void)goToProfile:(UIButton *)sender
{
    [self.delegate setNewsPickupMatchPostCellViewController:self withUserIDToSegue:sender.tag];
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Competitor Pics";
    
    NewsfeedMatchHeadingCVCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    
    Player *p = nil;
    
    if ([self.matchCategory isEqualToString:@"individual"]) {
        if (indexPath.row == 1) {
            // show "vs" not the pic
            cell.playerPic.hidden = YES;
            UILabel *vsLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];
            vsLabel.text = @"VS";
            [vsLabel setFont:[UIFont fontWithName:@"HelveticaNeue-BoldItalic" size:22]];
            
            [cell addSubview:vsLabel];
            
            
        } else {
            
            
            if (indexPath.row == 2)
                p  = [self.competitors objectAtIndex:1];
            else
                p = [self.competitors objectAtIndex:0];
            
            NSString *urlMain = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                        baseString:p.profileBaseString];
            
            [cell.playerPic setImageWithURL:[NSURL URLWithString:urlMain]
                           placeholderImage:[UIImage imageNamed:@"avatar square"]];
            [UIImage makeRoundedImage:cell.playerPic withRadius:25];
            
            cell.playerPic.layer.borderColor = [UIColor whiteColor].CGColor;
            cell.playerPic.layer.borderWidth = 1;
        }
        
        return  cell;
    } else {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"status = %@", @"ACCEPTED"];
        NSArray *matchParticipants = [self.competitors filteredArrayUsingPredicate:predicate];
        
        //   if (indexPath.item < [matchParticipants count]) {
        p = matchParticipants[indexPath.row];

        
  //      p = [self.competitors objectAtIndex:indexPath.row];
        NSString *urlMain = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                    baseString:p.profileBaseString];
        
        [cell.playerPic setImageWithURL:[NSURL URLWithString:urlMain]
                       placeholderImage:[UIImage imageNamed:@"avatar square"]];
   //     [UIImage makeRoundedImage:cell.playerPic withRadius:25];
        
        cell.playerPic.layer.borderColor = [UIColor whiteColor].CGColor;
        cell.playerPic.layer.borderWidth = 1;
        
        cell.playerUsername.text = p.userName;
        
        // profile pic button
        UIButton *picButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];
        [picButton addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
        picButton.tag = p.userID;
        [cell.playerPic addSubview:picButton];
        cell.playerPic.userInteractionEnabled = YES;
        
        
        return  cell;
    }
    
    return nil;
}
@end
