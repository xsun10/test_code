//
//  AddFriendsTVC.m
//  Vaiden
//
//  Created by James Chung on 11/26/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "SynchronizeFBFriendsTVC.h"
#import <FacebookSDK/FacebookSDK.h>
#import "UserPreferences.h"
#import "IOSRequest.h"
#import "Contact.h"
#import "SynchronizeFBFriendsCell.h"
#import "UIColor+VaidenColors.h"
#import "S3Tools.h"
#import "AFNetworking.h"
#import "UIImage+ProportionalFill.h"
#import "UIButton+RoundBorder.h"
#import "UIView+Manipulate.h"
#import "UIColor+VaidenColors.h"

@interface SynchronizeFBFriendsTVC ()

@property (nonatomic, strong) NSMutableArray *fbContacts;
@property (nonatomic, strong) UserPreferences *preferences;
@property (nonatomic, strong) UIView *tempView;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
@property (nonatomic, strong) UIView *overlay;

@end

@implementation SynchronizeFBFriendsTVC

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (NSMutableArray *)fbContacts
{
    if (!_fbContacts) _fbContacts = [[NSMutableArray alloc] init];
    return  _fbContacts;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self showTempSpinner];
    [self.navigationItem setBackBarButtonItem: backButton];
    [self synchronizeFBFriends];
  
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.overlay removeFromSuperview];
    self.overlay = nil;
    
}

- (void)displayErrorOverlay
{
    self.overlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, [UIScreen mainScreen].bounds.size.height)];
    self.overlay.backgroundColor = [UIColor colorWithRed:239/255.0 green:239/255.0 blue:244/255.0 alpha:1.0];
    
    UIImage *img = [UIImage imageNamed:@"no_facebook_friend"];
    UIImageView *icon = [[UIImageView alloc] initWithImage:img];
    [icon setFrame:CGRectMake(140,110,50,50)];
    [icon setContentMode:UIViewContentModeCenter];
    
    UILabel *firstLine = [[UILabel alloc] initWithFrame:CGRectMake(35, 200, 240, 21)];
    firstLine.textColor = [UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:1.0];
    firstLine.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:15];
    firstLine.text = @"No Friends Found";
    [firstLine setTextAlignment:NSTextAlignmentCenter];
    
    UILabel *secondLine = [[UILabel alloc] initWithFrame:CGRectMake(20, 210, 260, 63)];
    secondLine.textColor = [UIColor colorWithRed:170/255.0 green:170/255.0 blue:170/255.0 alpha:1.0];
    secondLine.font = [UIFont fontWithName:@"HelveticaNeue" size:13];
    secondLine.text = @"Your Facebook friends are not connected to Vaiden. Invite them to join!";
    secondLine.numberOfLines = 0;
    [secondLine setTextAlignment:NSTextAlignmentCenter];
    
    [self.overlay addSubview:icon];
    [self.overlay addSubview:firstLine];
    [self.overlay addSubview:secondLine];
    
    [self.tableView addSubview:self.overlay];
}

- (void)showTempSpinner
{
    self.tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    self.tempView.backgroundColor = [UIColor lightGrayColor];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] init];
    [self.activityIndicator setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width/2.0, [UIScreen mainScreen].bounds.size.height/2.0 - 50)];
    [self.activityIndicator startAnimating];
    [self.tempView addSubview:self.activityIndicator];
    
    [self.view addSubview:self.tempView];
    
}


- (void)synchronizeFBFriends
{
    NSMutableDictionary *fbData = [[NSMutableDictionary alloc] init];
    NSNumber *sessionID = [[NSNumber alloc] initWithInteger:[self.preferences getUserID]];
    
    [fbData setValue:sessionID forKey:@"session_user_id"];
    
    NSMutableArray *fbFriendsArray = [[NSMutableArray alloc] init];
    
    FBRequest* friendsRequest = [FBRequest requestForMyFriends];
    [friendsRequest startWithCompletionHandler: ^(FBRequestConnection *connection,
                                                  NSDictionary* result,
                                                  NSError *error) {
        NSArray* friends = [result objectForKey:@"data"];
        for (NSDictionary<FBGraphUser>* friend in friends) {
            [fbFriendsArray addObject:friend.id];
            
   //         NSLog(@"I have a friend named %@ with id %@", friend.name, friend.id);
        }
        [fbData setObject:fbFriendsArray forKey:@"friend_ids"];
        
        [self checkAgainstVaidenDB:fbData];
    }];
}

- (void)checkAgainstVaidenDB:(NSDictionary *)fbData
{
    [IOSRequest checkFBFriendsAgainstVaidenDBForUser:[self.preferences getUserID]
                                        withFriends:fbData
                                        onCompletion:^(NSArray *results) {
                                            for (id object in results) {
                                                Contact *contact = [[Contact alloc] init];
                                                [contact setUserID:[object[@"user_id"] integerValue]];
                                                [contact setUserName:object[@"username"]];
                                                [contact setProfileBaseString:object[@"profile_pic_string"]];
                                                [contact setFollowingStatus:[object[@"follow_status"] integerValue]];
                                                [contact setFullName:object[@"fullname"]];
                                                [self.fbContacts addObject:contact];
                                            }
                                            
                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                [self.activityIndicator stopAnimating];
                                                self.tempView.hidden = YES;
                                                if ([results count] == 0)
                                                    [self displayErrorOverlay];
                                                else
                                                    [self.tableView reloadData];
                                            });
         
     }];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.fbContacts count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"My FB";
    SynchronizeFBFriendsCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    Contact *contact = [self.fbContacts objectAtIndex:indexPath.row];
    
    NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                            baseString:contact.profileBaseString
                     ];
    
    [cell.profilePic setImageWithURL:[NSURL URLWithString:url]
                    placeholderImage:[UIImage imageNamed:@"avatar square"]];
    
    [UIImage makeRoundedImageNoBorder:cell.profilePic withRadius:10];
    
    [cell.followButtonHandle makeRoundedBorderWithRadius:3];
    cell.followButtonHandle.tag = indexPath.row;
    
    cell.usernameLabel.text = [NSString stringWithFormat:@"@ %@", contact.userName];
    cell.fullnameLabel.text = contact.fullName;
    
    if (contact.followingStatus == IS_FOLLOWING_STATUS) {
        [cell.followButtonHandle setTitle:@"Unfollow" forState:UIControlStateNormal];
        cell.followButtonHandle.enabled = YES;
        //cell.followButtonHandle.backgroundColor = [UIColor newBlueLight];
    } else if (contact.followingStatus == NOT_FOLLOWING_STATUS) {
        [cell.followButtonHandle setTitle:@"Follow" forState:UIControlStateNormal];
        cell.followButtonHandle.enabled = YES;
        //cell.followButtonHandle.backgroundColor = [UIColor newBlueLight];
    } else if (contact.followingStatus == PENDING_FOLLOWING_STATUS) {
        [cell.followButtonHandle setTitle:@"Pending" forState:UIControlStateNormal];
        cell.followButtonHandle.enabled = NO;
        //cell.followButtonHandle.backgroundColor = [UIColor lightGrayColor];
    }
    
    // Configure the cell...
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70.0;
}

- (void)showSubmissionError
{
    UIAlertView *alert = [[UIAlertView alloc] init];
    alert.title = @"Submission Error";
    alert.message = @"There was a problem submitting your request.  Please try again.";
    [alert addButtonWithTitle:@"OK"];
    [alert show];
}

- (IBAction)followAction:(UIButton *)sender
{
    Contact *vContact = [self.fbContacts objectAtIndex:sender.tag];
    sender.enabled = NO;
    if (vContact.followingStatus == IS_FOLLOWING_STATUS && vContact.userID != [self.preferences getUserID]) {
        [IOSRequest removeFollowConnectionWithLeader:vContact.userID
                                         andFollower:[self.preferences getUserID]
                                        onCompletion:^(NSDictionary *results) {
                                            
                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                sender.enabled = YES;
                                                
                                                if (![results[@"outcome"] isEqualToString:@"failure"]) {
                                                    [self.preferences setNewsfeedRefreshState:YES];
                                                    [self.preferences setChallengePlayersListRefreshState:YES];
                                                    vContact.followingStatus = NOT_FOLLOWING_STATUS;
                                                    [self.fbContacts replaceObjectAtIndex:sender.tag withObject:vContact];
                                                    [self.tableView reloadData];
                                                } else {
                                                    [self showSubmissionError];
                                                }
                                                
                                            });
                                        }];
    } else if (vContact.followingStatus == NOT_FOLLOWING_STATUS && vContact.userID != [self.preferences getUserID]) {
        [IOSRequest makeFollowConnectionWithLeader:vContact.userID
                                       andFollower:[self.preferences getUserID]
                                      onCompletion:^(NSDictionary *results) {
                                          
                                          dispatch_async(dispatch_get_main_queue(), ^ {
                                              sender.enabled = YES;
                                              
                                              if (![results[@"outcome"] isEqualToString:@"failure"]) {
                                                  [self.preferences setNewsfeedRefreshState: YES];
                                                  [self.preferences setChallengePlayersListRefreshState:YES];
                                                  vContact.followingStatus = [results[@"follow_status"] integerValue];
                                                  [self.fbContacts replaceObjectAtIndex:sender.tag withObject:vContact];
                                                  [self.tableView reloadData];
                                              } else {
                                                  [self showSubmissionError];
                                              }
                                          });
                                      }];
        
        
    }
    
}
@end
