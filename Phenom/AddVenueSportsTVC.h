//
//  AddVenueSportTVC.h
//  Phenom
//
//  Created by James Chung on 7/8/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomBaseTVC.h"

@class AddVenueSportsTVC;

@protocol AddVenueSportsTVC_Delegate <NSObject>

-(void)setViewController:(AddVenueSportsTVC *)controller withSports:(NSMutableArray *)sports;

@end


@interface AddVenueSportsTVC : CustomBaseTVC

@property (nonatomic, weak) id <AddVenueSportsTVC_Delegate> delegate;

@end




