//
//  University.m
//  Vaiden
//
//  Created by James Chung on 2/21/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "University.h"

@implementation University


- (University *)initWithUniversityDetails:(NSUInteger)universityID
                                  andName:(NSString *)universityName
{
    self = [super init];
    
    if (self) {
        _universityID = universityID;
        _universityName = universityName;
    }
    
    return self;
}

@end
