//
//  InviteMoreTeamMembersCell.h
//  Vaiden
//
//  Created by James Chung on 4/9/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InviteMoreTeamMembersCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *profilePic;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UIButton *inviteButton;

@end
