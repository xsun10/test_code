//
//  ExperienceEmptyTVCell.h
//  Vaiden
//
//  Created by Turbo on 7/29/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExperienceEmptyTVCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *emptycell;

@end
