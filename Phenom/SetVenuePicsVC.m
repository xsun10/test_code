//
//  SetVenuePicsVC.m
//  Vaiden
//
//  Created by James Chung on 10/8/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "SetVenuePicsVC.h"
#import "SetVenuePicsCVCell.h"
#import "S3Tools.h"
#import "IOSRequest.h"
#import "AFNetworking.h"
#import "VenueImage.h"
#import <MobileCoreServices/MobileCoreServices.h>

#import "Constants.h"
#import "Response.h"
#import "AmazonKeyChainWrapper.h"
#import "AmazonClientManager.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "UIButton+RoundBorder.h"
#import "UIColor+VaidenColors.h"
#import "UserPreferences.h"
#import "UIView+Manipulate.h"

@interface SetVenuePicsVC () <UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>


@property (weak, nonatomic) IBOutlet UIPageControl *mypicsPageControl;
@property (weak, nonatomic) IBOutlet UICollectionView *mypicsCollectionView;
@property (nonatomic, strong) NSMutableArray *picsArray;

@property (weak, nonatomic) UIActionSheet *venuePicActionSheet;
@property (strong, nonatomic)UIPopoverController *imagePickerPopover;
@property (nonatomic, strong) NSData *imageData;
@property (weak, nonatomic) IBOutlet UIButton *myuploadPicButton;
@property (weak, nonatomic) IBOutlet UIScrollView *mypageScrollView;
@property (nonatomic, strong) UserPreferences *preferences;
//@property (nonatomic) BOOL existsNewPic;
@property (nonatomic, strong) UILabel *noPicsMessage;
@property (weak, nonatomic) IBOutlet UIView *mypageView;
@property (nonatomic, strong) UIBarButtonItem *doneButton;

@property (weak, nonatomic) IBOutlet UIImageView *nonPicIcon;
@property (weak, nonatomic) IBOutlet UILabel *nonPicTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *nonPicContentLabel;
@end

@implementation SetVenuePicsVC

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (NSMutableArray *)picsArray
{
    if (!_picsArray) _picsArray = [[NSMutableArray alloc] init];
    return _picsArray;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (self.numVCsToPopBack == 3) // this means that the flow to this vc was performed to create a new venue...so we don't want to allow back button to be shown.
        [self.navigationItem setHidesBackButton:YES animated:YES];
    
	self.mypicsCollectionView.delegate = self;
    self.mypicsCollectionView.dataSource = self;
    
    //    [self.pageScrollView setContentInset:UIEdgeInsetsMake(0,0,0,0)];
    
    [self.myuploadPicButton makeRoundedBorderWithRadius:3];
    [self setImages];
    //[self setDoneButton];
    
    //[self.picsCollectionView sendSubviewToBack:self.mypageScrollView];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    self.mypageScrollView.contentSize = CGSizeMake(320.0, 650.0);
}

/*- (void)setDoneButton
 {
 UIButton *btn = [UIButton  buttonWithType:UIButtonTypeCustom];
 btn.frame = CGRectMake(0, 0, 50, 21);
 btn.layer.borderColor = [UIColor whiteColor].CGColor;
 btn.layer.borderWidth = 1.0;
 
 [btn addTarget:self action:@selector(saveImages:) forControlEvents:UIControlEventTouchUpInside];
 [btn setBackgroundColor:[UIColor clearColor]];
 [btn setTitle:@"DONE" forState:UIControlStateNormal];
 [btn.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-MediumItalic" size:12.0]];
 
 // Initialize UIBarbuttonitem...
 self.doneButton = [[UIBarButtonItem alloc] initWithCustomView:btn];
 self.navigationItem.rightBarButtonItem = self.doneButton;
 }*/


- (void)setImages
{
    [IOSRequest fetchVenuePics:self.venueID
                  onCompletion:^(NSMutableArray *results) {
                      
                      for (id object in results) {
                          
                          VenueImage *vI = [[VenueImage alloc] initWithVenueImageInfo:object[@"filestring"]
                                                                             andImage:nil
                                                                            isDefault:[object[@"is_default"] boolValue]];
                          //             [self.picsArray addObject:vI];
                          [self.picsArray insertObject:vI atIndex:0];
                      }
                      
                      dispatch_async(dispatch_get_main_queue(), ^{
                          [self.mypicsCollectionView reloadData];
                          
                          if ([self.picsArray count] == 0) {
                              
                              /*self.noPicsMessage = [[UILabel alloc] initWithFrame:CGRectMake(30, 55, 260, 70)];
                               self.noPicsMessage.text = @"This Venue Does Not Have Any Pics Yet";
                               self.noPicsMessage.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:25];
                               self.noPicsMessage.numberOfLines = 0;
                               [self.pageView addSubview:self.noPicsMessage];*/
                              self.nonPicIcon.hidden = NO;
                              self.nonPicContentLabel.hidden = NO;
                              self.nonPicTitleLabel.hidden = NO;
                          }
                          
                      });
                      
                  }];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    [self.mypicsPageControl setNumberOfPages:[self.picsArray count]];
    
    
    return [self.picsArray count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Pic Cell";
    
    SetVenuePicsCVCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    if ([self.picsArray count] == 0) {
        cell.venuePic.image = [UIImage imageNamed:@"Landscape Placeholder"];
        return cell;
    }
    
    VenueImage *vI = [self.picsArray objectAtIndex:indexPath.row];
    
    if (vI.venuePic != nil) {
        cell.venuePic.image = vI.venuePic;
    } else {
        NSString *url = [S3Tools getFileNameStringWithType:@"venue_pic"
                                                baseString:vI.venuePicString];
        
        [cell.venuePic setImageWithURL:[NSURL URLWithString:url]
                      placeholderImage:[UIImage imageNamed:@"Landscape Placeholder"]];
        
    }
    
    if (vI.isDefaultImage) {
        // Need to show a green checkmark
        cell.checkMarkIcon.hidden = NO;
    } else {
        cell.checkMarkIcon.hidden = YES;
    }
    
    cell.venuePic.contentMode = UIViewContentModeScaleAspectFill;
    cell.venuePic.clipsToBounds = YES;
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath{
    
    SetVenuePicsCVCell* currentCell = ([[collectionView visibleCells]count] > 0) ? [collectionView visibleCells][0] : nil;
    
    if(cell != nil){
        NSInteger rowIndex = [collectionView indexPathForCell:currentCell].row;
        
        /*  UIImage * toImage = (self.slides)[rowIndex];
         [UIView transitionWithView:self.view
         duration:0.5
         options:UIViewAnimationOptionTransitionCrossDissolve
         animations:^{
         self.backgroundImage.image = toImage;
         } completion:nil];
         */
        [self.mypicsPageControl setCurrentPage:rowIndex];
        
    }
}

// Actually this is "doneImages"
- (IBAction)saveImages:(id)sender
{
    NSInteger currentIndex = [self.navigationController.viewControllers indexOfObject:self];
    
    if( currentIndex-2 >= 0) {
        [self.preferences setVenuesListPageRefreshState:YES];
        [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:currentIndex-self.numVCsToPopBack] animated:YES];
    }
}

- (void)showNoImagesMessage
{
    UILabel *noPicsMessage =[[UILabel alloc] init];
    noPicsMessage.text = @"No pics for this venue yet";
    
}

- (void)saveAmazonImage
{
    if (![AmazonClientManager hasCredentials]) {
        [[Constants credentialsAlert] show];
        NSLog(@"no credentials");
    }
    else {
        
        NSLog (@"has credentials");
        
        [self.preferences setVenuesListPageRefreshState:YES];
        
        operationQueue = [NSOperationQueue new];
        operationQueue.maxConcurrentOperationCount = 3;
        
        self.doneButton.enabled = NO;
        self.myuploadPicButton.enabled = NO;
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(newsfeedPicDidFinishUploading:)
                                                     name:@"VenueImageDidFinishUploadingNotification"
                                                   object:nil];
        
        NSString *fileNameBase = [S3Tools getFileNameBaseStringWithType:@"profile" forVenue:1];
        S3Tools *imageUploader1 = [[S3Tools alloc] initWithData:self.imageData
                                                       forVenue:self.venueID
                                                       dataType:@"venue_pic"
                                          andBaseFileNameString:fileNameBase
                                                     andFileExt:@"jpg"];
        
        
        /*      // this is temporary solution...really need to disable mbprogress after upload complete
         NSTimer *timer;
         
         timer = [NSTimer scheduledTimerWithTimeInterval: 3.0
         target: self
         selector: @selector(handleTimer)
         userInfo: nil
         repeats: NO];
         
         */
        HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        HUD.labelText = @"Uploading";
        
        [operationQueue addOperation:imageUploader1];
        
        
        //     NSArray *ops = [NSArray arrayWithObject:imageUploader1];
        //     [operationQueue addOperations:ops waitUntilFinished:YES];
        
        
        
        
        
        /*
         
         S3Tools *imageUploader2 = [[S3Tools alloc] initWithData:self.thumbnailImageData
         forUser:[self.preferences getUserID]
         dataType:@"thumbnail"
         andBaseFileNameString:fileNameBase
         andFileExt:self.fileExtension];
         [operationQueue addOperation:imageUploader2];
         
         S3Tools *imageUploader3 = [[S3Tools alloc] initWithData:self.thumbnailRetinaImageData
         forUser:[self.preferences getUserID]
         dataType:@"thumbnail_retina"
         andBaseFileNameString:fileNameBase
         andFileExt:self.fileExtension];
         [operationQueue addOperation:imageUploader3];*/
        
    }
}

// Will trigger once the image is doene uploading...check S3Tools.h for other side of implementation
- (void) newsfeedPicDidFinishUploading:(NSNotification*)notification {
    [[NSNotificationCenter defaultCenter]
     removeObserver:self
     name:@"VenueImageDidFinishUploadingNotification"
     object:nil];
    
    //   [self.navigationController popViewControllerAnimated:YES];
    self.doneButton.enabled = YES;
    self.myuploadPicButton.enabled = YES;
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

/*
 - (void)handleTimer
 {
 [MBProgressHUD hideHUDForView:self.view animated:YES];
 }
 */

#define TITLE_OF_ACTIONSHEET @"Add a venue pic"
#define CANCEL_BUTTON_ACTIONSHEET @"Cancel "
#define FROM_CAMERA @"Take a picture"
#define FROM_LIBRARY @"Choose From Photo Library"

- (IBAction)addVenuePicClicked:(id)sender
{
    if (!self.venuePicActionSheet) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                      initWithTitle:TITLE_OF_ACTIONSHEET delegate:self
                                      cancelButtonTitle:CANCEL_BUTTON_ACTIONSHEET destructiveButtonTitle:nil
                                      otherButtonTitles:FROM_CAMERA, FROM_LIBRARY, nil];
        [actionSheet showFromTabBar:self.tabBarController.tabBar];
        
        self.venuePicActionSheet = actionSheet;
    }
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == actionSheet.destructiveButtonIndex) {
        [self.view.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    } else {
        NSString *choice = [actionSheet buttonTitleAtIndex:buttonIndex];
        
        if ([choice isEqualToString:FROM_CAMERA]) {
            [self changePicFromCamera];
        } else if ([choice isEqualToString:FROM_LIBRARY]){
            [self changePicFromPhotoLibrary];
        }
    }
}

- (void)changePicFromCamera
{
    [self presentImagePicker:UIImagePickerControllerSourceTypeCamera];
}


- (void) changePicFromPhotoLibrary
{
    [self presentImagePicker:UIImagePickerControllerSourceTypeSavedPhotosAlbum];
}

- (void) presentImagePicker:(UIImagePickerControllerSourceType)sourceType
{
    if ([UIImagePickerController isSourceTypeAvailable:sourceType]) {
        NSArray *availableMediaTypes = [UIImagePickerController availableMediaTypesForSourceType:sourceType];
        if ([availableMediaTypes containsObject:(NSString *)kUTTypeImage]) {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.sourceType = sourceType;
            picker.mediaTypes = @[(NSString *)kUTTypeImage];
            picker.allowsEditing = YES;
            picker.delegate = self;
            
            
            [self presentViewController:picker animated:YES completion:nil];
        }
    }
}

- (void) imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

#define MAX_IMAGE_WIDTH 320

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = info[UIImagePickerControllerEditedImage];
    
    if (!image) image = info[UIImagePickerControllerOriginalImage];
    if (image) {
        VenueImage *vI = [[VenueImage alloc] initWithVenueImageInfo:nil
                                                           andImage:image
                                                          isDefault:YES];
        
        //    [self.picsArray addObject:vI];
        [self.picsArray insertObject:vI atIndex:0];
        //self.noPicsMessage.hidden = YES;
        //   self.existsNewPic = YES;
        //self.noPicsMessage.hidden = YES;
        
        self.nonPicContentLabel.hidden = YES;
        self.nonPicIcon.hidden = YES;
        self.nonPicTitleLabel.hidden = YES;
        
        //       self.profilePicOriginal = image;
        UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
        CGRect frame = imageView.frame;
        if (frame.size.width > MAX_IMAGE_WIDTH) {
            frame.size.height = (frame.size.height / frame.size.width) * MAX_IMAGE_WIDTH;
            frame.size.width = MAX_IMAGE_WIDTH;
        }
        imageView.frame = frame;
        
        self.imageData = UIImageJPEGRepresentation(vI.venuePic, 1.0);
        
        
        [self.myuploadPicButton setTitle:@"Upload Another" forState:UIControlStateNormal];
        
        /*        self.thumbnailImageData = UIImageJPEGRepresentation([image imageCroppedToFitSize:CGSizeMake(70, 70)], 1.0);
         self.thumbnailRetinaImageData = UIImageJPEGRepresentation([image imageCroppedToFitSize:CGSizeMake(140, 140)], 1.0);
         self.fileExtension = @"jpg";
         */
        //       self.profilePicOriginal = imageView.image;
    }
    if (self.imagePickerPopover) {
        
        [self.imagePickerPopover dismissPopoverAnimated:YES];
        self.imagePickerPopover = nil;
        [self saveAmazonImage];
        
    } else {
        [self dismissViewControllerAnimated:YES completion:^{
            [self saveAmazonImage];
        }];
        
        
    }
    [self.mypicsCollectionView reloadData];
    
}


@end
