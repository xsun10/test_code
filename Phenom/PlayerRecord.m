//
//  PlayerRecord.m
//  Vaiden
//
//  Created by James Chung on 9/23/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "PlayerRecord.h"

@implementation PlayerRecord


- (PlayerRecord *)init
{
    self = [super init];
    
    if (self) {
        _wins = 0;
        _losses = 0;
        _ties = 0;
    }
    return self;
}

- (PlayerRecord *) initWithPlayerRecordWins:(NSUInteger)wins
                                  andLosses:(NSUInteger)losses
                                    andTies:(NSUInteger)ties
{
    self = [super init];
    
    if (self) {
        _wins = wins;
        _losses = losses;
        _ties = ties;
    }
    return self;
    
}

- (NSString *)getWinsText
{
    if (self.wins == 1)
        return @"win";
    
    return  @"wins";
}

- (NSString *)getLossesText
{
    if (self.losses == 1)
        return @"loss";
    
    return @"losses";
}

- (NSString *)getTiesText
{
    if (self.ties == 1)
        return @"ties";
    
    return @"tie";
}
@end
