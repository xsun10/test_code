//
//  ChooseTeamSportTVC.h
//  Vaiden
//
//  Created by James Chung on 4/10/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "CustomBaseTVC.h"
#import "Sport.h"

@class ChooseTeamSportTVC;

@protocol ChooseTeamSportTVC_Delegate <NSObject>
- (void)setWithSportFromVC:(ChooseTeamSportTVC *)controller withSport:(Sport *)sport;

@end

@interface ChooseTeamSportTVC : CustomBaseTVC

@property (nonatomic, weak) id <ChooseTeamSportTVC_Delegate> delegate;


@end
