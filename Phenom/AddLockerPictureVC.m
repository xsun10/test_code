//
//  AddLockerPictureVC.m
//  Vaiden
//
//  Created by James Chung on 5/7/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "AddLockerPictureVC.h"
#import "LockerTagCollectionViewCell.h"
#import "UIView+Manipulate.h"
#import <AssetsLibrary/AssetsLibrary.h>
//#import "S3Tools.h"
#import "AFNetworking.h"
#import "UIImage+ProportionalFill.h"
#import "IOSRequest.h"
#import "UserPreferences.h"
#import "LockerTag.h"
#import "PlayingSeason.h"
#import "PlayingYear.h"
#import "AddLockerPictureShareTVC.h"

#import "Constants.h"
#import "Response.h"
#import "AmazonKeyChainWrapper.h"
#import "AmazonClientManager.h"
#import "AFNetworking.h"



@interface AddLockerPictureVC () <UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UICollectionViewDataSource, UICollectionViewDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *photoPreviewImage;
@property (weak, nonatomic) IBOutlet UIButton *changePhotoButton;
@property (weak, nonatomic) IBOutlet UICollectionView *tagCollectionView;

@property (weak, nonatomic) UIActionSheet *choosePicActionSheet;
@property (strong, nonatomic)UIPopoverController *imagePickerPopover;
@property (weak, nonatomic) IBOutlet UIScrollView *pageScrollView;
@property (nonatomic, strong) UserPreferences *preferences;
@property (nonatomic, strong) NSMutableArray *tagsArray;

@property (nonatomic, strong) PlayingSeason *seasonObject;
@property (nonatomic, strong) PlayingYear *yearObject;
@property (nonatomic, strong) Sport *sportObject;
@property (nonatomic, strong) Venue *venueObject;
@property (nonatomic, strong) NSMutableArray *trainingTags;
@property (nonatomic, strong) NSMutableArray *statsTags;

@property (nonatomic, strong) NSMutableArray *contactsToTagArray;
@property (nonatomic) BOOL wasPhotoChanged;
@property (nonatomic, strong) EventTag *event;
@property (weak, nonatomic) IBOutlet UISwitch *modeSwitch;
@property (nonatomic) NSInteger total;

@end

@implementation AddLockerPictureVC

- (NSMutableArray *)tagsArray
{
    if (!_tagsArray) _tagsArray = [[NSMutableArray alloc] init];
    return _tagsArray;
}

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
    
    self.wasPhotoChanged = NO;
    self.tagCollectionView.delegate = self;
    self.tagCollectionView.dataSource = self;
    [self.changePhotoButton makeRoundedBorderWithRadius:3];
    self.photoPreviewImage.image = self.viObject.imageHandle;
    
    self.seasonObject = [[PlayingSeason alloc] init];
    [self.seasonObject setSeasonWithCurrentMonth];
    
    self.yearObject = [[PlayingYear alloc] init];
    [self.yearObject setWithCurrentYear];
    
    self.total = 0;
    
    [self loadLockerTags];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    self.pageScrollView.contentSize = CGSizeMake(320.0, 770);
}

#define  SHOW_TAG_TYPE_COMPETITION 1
#define  SHOW_TAG_TYPE_TRAINING 2

- (void)loadLockerTags
{
    NSInteger tagType;
    
    if (self.isCompetition)
        tagType = SHOW_TAG_TYPE_COMPETITION;
    else
        tagType = SHOW_TAG_TYPE_TRAINING;
    
    [IOSRequest getLockerTagsWithType:tagType
                         onCompletion:^(NSMutableArray *results) {
                             
                             for (id object in results) {
                                 LockerTag *tag = [[LockerTag alloc] initWithTagDetails:[object[@"tag_id"] integerValue]
                                                                             andTagType:[object[@"tag_type"] integerValue]
                                                                             andTagName:object[@"tag_name"]];
                                 [self.tagsArray addObject:tag];
                             }
                             
                             dispatch_async(dispatch_get_main_queue(), ^{
                                 
                                 
                                 
                                 [self.tagCollectionView reloadData];
                             });
                             
                         }];
}

- (IBAction)changeModeSwitch:(UISwitch *)sender
{
    
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.tagsArray count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    LockerTagCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Tag Cell" forIndexPath:indexPath];
    
    cell.tagValueLabel.text = @"TAG VALUE"; // initialize
    
    LockerTag *tag = [self.tagsArray objectAtIndex:indexPath.row];
    cell.tagNameLabel.text = [tag.tagName uppercaseString];
    
    cell.requiredMessageLabel.hidden = NO;

    
    if (tag.tagID == TAG_TYPE_SEASON) {
        cell.tagValueLabel.text = [self.seasonObject.season uppercaseString];
    } else if (tag.tagID == TAG_TYPE_SPORT && self.sportObject != nil) {
        cell.tagValueLabel.text = [self.sportObject.sportName uppercaseString];
    } else if (tag.tagID == TAG_TYPE_YEAR) {
        cell.tagValueLabel.text = self.yearObject.year;
    } else if (tag.tagID == TAG_TYPE_VENUE) {
        cell.tagValueLabel.text = [self.venueObject.venueName uppercaseString];
    } else if (tag.tagID == TAG_TYPE_PEOPLE && [self.contactsToTagArray count] > 0) {
        if ([self.contactsToTagArray count] == 1)
            cell.tagValueLabel.text = [[NSString stringWithFormat:@"%ld person", [self.contactsToTagArray count]] uppercaseString];
        else
            cell.tagValueLabel.text = [NSString stringWithFormat:@"%ld people", [self.contactsToTagArray count]];
    } else if (tag.tagID == TAG_TYPE_TRAINING && [self.trainingTags count] > 0) {
        if ([self.trainingTags count] == 1)
            cell.tagValueLabel.text = [[NSString stringWithFormat:@"%ld drill", [self.trainingTags count]] uppercaseString];
        else
            cell.tagValueLabel.text = [[NSString stringWithFormat:@"%ld drills", [self.trainingTags count]] uppercaseString];
    } else if (tag.tagID == TAG_TYPE_STATS  && self.total > 0) {
        if (self.total == 1)
            cell.tagValueLabel.text = [[NSString stringWithFormat:@"%d stat", self.total] uppercaseString];
        else
            cell.tagValueLabel.text = [[NSString stringWithFormat:@"%d stats", self.total] uppercaseString];
    } else if (tag.tagID == TAG_TYPE_EVENT && [self.event.eventName length] > 0) {
        cell.tagValueLabel.text = [self.event.eventName uppercaseString];
    }
    
    if ([cell.tagValueLabel.text isEqualToString:@"TAG VALUE"] || [cell.tagValueLabel.text length] == 0) {
        cell.tagPlusIcon.hidden = NO;
        cell.tagValueLabel.hidden = YES;
        cell.tagEditIcon.hidden = YES;
    } else {
        cell.tagPlusIcon.hidden = YES;
        cell.tagValueLabel.hidden = NO;
        cell.tagEditIcon.hidden = NO;
    }
    
    if (tag.tagID == TAG_TYPE_SEASON || tag.tagID == TAG_TYPE_YEAR || tag.tagID == TAG_TYPE_SPORT)
        cell.requiredMessageLabel.hidden = NO;
    else
        cell.requiredMessageLabel.hidden = YES;
    
    
    cell.tagButton.tag = tag.tagID;
    
    return cell;
}

- (void)showSportMissingError
{
    UIAlertView *alert = [[UIAlertView alloc] init];
    alert.title = @"Please choose a sport";
    alert.message = @"You cannot perform this tag until you choose a sport.";
    [alert addButtonWithTitle:@"OK"];
    [alert show];
}

- (IBAction)nextAction:(id)sender
{
    if (self.sportObject != nil) {
        if (self.wasPhotoChanged) // only save if image was changed (chose image in prev view controller)
            [self saveImageToAmazon];
        
        [self performSegueWithIdentifier:@"Share Segue" sender:self];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Sport Missing";
        alert.message = @"Please tag your picture with a sport.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
        
    }
}

- (void)saveImageToAmazon
{
    if (![AmazonClientManager hasCredentials]) {
        [[Constants credentialsAlert] show];
        NSLog(@"no credentials");
    }
    else {
        
        NSLog (@"has credentials");
        
        operationQueue = [NSOperationQueue new];
        operationQueue.maxConcurrentOperationCount = 3;
        
        self.regImageSizeUploader = [[S3Tools2 alloc] initWithData:self.viObject.regularSizeImageData
                                                           forUser:[self.preferences getUserID]
                                                          dataType:@"photo_regsize"
                                             andBaseFileNameString:self.imageFileNameBaseString
                                                        andFileExt:self.viObject.fileExtension];
        
        [operationQueue addOperation:self.regImageSizeUploader];
        
        self.regImageRetinaSizeUploader = [[S3Tools2 alloc] initWithData:self.viObject.regularSizeRetinaImageData
                                                                 forUser:[self.preferences getUserID]
                                                                dataType:@"photo_regsize_retina"
                                                   andBaseFileNameString:self.imageFileNameBaseString
                                                              andFileExt:self.viObject.fileExtension];
        
        [operationQueue addOperation:self.regImageRetinaSizeUploader];
        
        
        self.thumbnailImageSizeUploader = [[S3Tools2 alloc] initWithData:self.viObject.thumbnailImageData
                                                                 forUser:[self.preferences getUserID]
                                                                dataType:@"photo_thumbnail"
                                                   andBaseFileNameString:self.imageFileNameBaseString
                                                              andFileExt:self.viObject.fileExtension];
        
        [operationQueue addOperation:self.thumbnailImageSizeUploader];
        
        self.thumbnailImageRetinaSizeUploader = [[S3Tools2 alloc] initWithData:self.viObject.thumbnailRetinaImageData
                                                                       forUser:[self.preferences getUserID]
                                                                      dataType:@"photo_thumbnail_retina"
                                                         andBaseFileNameString:self.imageFileNameBaseString
                                                                    andFileExt:self.viObject.fileExtension];
        
        [operationQueue addOperation:self.thumbnailImageRetinaSizeUploader];
        
        
    }
}



- (IBAction)tagAction:(UIButton *)sender
{
    switch (sender.tag) {
        case TAG_TYPE_SEASON:
            [self performSegueWithIdentifier:@"Season Select Segue" sender:self];
            break;
        case TAG_TYPE_YEAR:
            [self performSegueWithIdentifier:@"Year Select Segue" sender:self];
            break;
        case TAG_TYPE_SPORT:
            [self performSegueWithIdentifier:@"Sport Select Segue" sender:self];
            break;
        case TAG_TYPE_PEOPLE:
            [self performSegueWithIdentifier:@"Contacts Select Segue" sender:self];
            break;
        case TAG_TYPE_VENUE:
            [self performSegueWithIdentifier:@"Venue Select Segue" sender:self];
            break;
        case TAG_TYPE_TRAINING:
            if (self.sportObject.sportID != 0) {
                [self performSegueWithIdentifier:@"Training Select Segue" sender:self];
            } else {
                [self showSportMissingError];
            }
            break;
        case TAG_TYPE_STATS:
            if (self.sportObject.sportID != 0) {
                [self performSegueWithIdentifier:@"Stats Select Segue" sender:self];
            } else {
                [self showSportMissingError];
            }
            break;
        case TAG_TYPE_EVENT:
            [self performSegueWithIdentifier:@"Event Select Segue" sender:self];
            break;
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Season Select Segue"]) {
        ChooseSeasonTVC *controller = segue.destinationViewController;
        controller.delegate = self;
        controller.chosenSeason = [self.seasonObject.season lowercaseString];
    } else if ([segue.identifier isEqualToString:@"Year Select Segue"]) {
        ChooseYearTVC *controller = segue.destinationViewController;
        controller.delegate = self;
        controller.chosenYear = self.yearObject.year;
    } else if ([segue.identifier isEqualToString:@"Sport Select Segue"]) {
        ChooseSportUtilityTVC *controller = segue.destinationViewController;
        controller.delegate = self;
        controller.selectMultiple = NO;
        controller.sportTypeInt = SPORT_TYPE_ALL;
        controller.chosenSport = self.sportObject;
        
        if (self.isCompetition)
            controller.hideFitnessSport = YES;
        else
            controller.hideFitnessSport = NO;
        
    } else if ([segue.identifier isEqualToString:@"Contacts Select Segue"]) {
        ChooseContactsUtilityTVC *controller = segue.destinationViewController;
        controller.delegate = self;
        controller.maxContactsSelected = 10;
        controller.pageIsForLockerPeopleTag = YES;
        controller.errorOverlayLongMessage = @"You currently do not have any contacts you are following nor do you have any followers.";
    } else if ([segue.identifier isEqualToString:@"Venue Select Segue"]) {
        ChooseLocationTVC *controller = segue.destinationViewController;
        controller.delegate = self;
        
    } else if ([segue.identifier isEqualToString:@"Share Segue"]) {
        AddLockerPictureShareTVC *controller = segue.destinationViewController;
        controller.viObject = self.viObject;
        controller.seasonObject = self.seasonObject;
        controller.yearObject = self.yearObject;
        controller.sportObject = self.sportObject;
        controller.venueObject = self.venueObject;
        controller.contactsToTagArray = self.contactsToTagArray;
        controller.regImageSizeUploader = self.regImageSizeUploader;
        controller.regImageRetinaSizeUploader = self.regImageRetinaSizeUploader;
        controller.thumbnailImageSizeUploader = self.thumbnailImageSizeUploader;
        controller.thumbnailImageRetinaSizeUploader = self.thumbnailImageRetinaSizeUploader;
        controller.imageFileNameBaseString = self.imageFileNameBaseString;
        controller.imageFileName = self.imageFileName;
        controller.statsTagsArray = self.statsTags;
        controller.trainingTagsArray = self.trainingTags;
        controller.eventObject = self.event;
    } else if ([segue.identifier isEqualToString:@"Training Select Segue"]) {
        ChooseTrainingTagTVC *controller = segue.destinationViewController;
        controller.delegate = self;
        controller.sportID = self.sportObject.sportID;
    } else if ([segue.identifier isEqualToString:@"Stats Select Segue"]) {
        ChooseStatTagTVC *controller = segue.destinationViewController;
        controller.delegate = self;
        controller.sportObj = self.sportObject;
    } else if ([segue.identifier isEqualToString:@"Event Select Segue"]) {
        ChooseEventTagTVC *controller = segue.destinationViewController;
        controller.sportID = self.sportObject.sportID;
        controller.delegate = self;
    }
}

#define TITLE_OF_ACTIONSHEET @"Change Your Photo"
#define CANCEL_BUTTON_ACTIONSHEET @"Cancel "
#define FROM_CAMERA @"Take A Picture"
#define FROM_LIBRARY @"Choose From Photo Library"

- (IBAction)choosePic:(id)sender
{
    if (!self.choosePicActionSheet) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                      initWithTitle:TITLE_OF_ACTIONSHEET delegate:self
                                      cancelButtonTitle:CANCEL_BUTTON_ACTIONSHEET destructiveButtonTitle:nil
                                      otherButtonTitles:FROM_CAMERA, FROM_LIBRARY, nil];
        [actionSheet showFromTabBar:self.tabBarController.tabBar];
        
        self.choosePicActionSheet = actionSheet;
    }
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == actionSheet.destructiveButtonIndex) {
        [self.view.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    } else {
        NSString *choice = [actionSheet buttonTitleAtIndex:buttonIndex];
        
        if ([choice isEqualToString:FROM_CAMERA]) {
            [self changePicFromCamera];
        } else if ([choice isEqualToString:FROM_LIBRARY]){
            [self changePicFromPhotoLibrary];
        }
    }
}

- (void)changePicFromCamera
{
    [self presentImagePicker:UIImagePickerControllerSourceTypeCamera];
}


- (void) changePicFromPhotoLibrary
{
    [self presentImagePicker:UIImagePickerControllerSourceTypeSavedPhotosAlbum];
}

- (void) presentImagePicker:(UIImagePickerControllerSourceType)sourceType
{
    if ([UIImagePickerController isSourceTypeAvailable:sourceType]) {
        NSArray *availableMediaTypes = [UIImagePickerController availableMediaTypesForSourceType:sourceType];
        if ([availableMediaTypes containsObject:(NSString *)kUTTypeImage]) {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.sourceType = sourceType;
            picker.mediaTypes = @[(NSString *)kUTTypeImage];
            picker.allowsEditing = YES;
            picker.delegate = self;
            
            
            [self presentViewController:picker animated:YES completion:nil];
        }
    }
}

- (void) imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

#define MAX_IMAGE_WIDTH 70 // might want to change this to height....

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = info[UIImagePickerControllerEditedImage];
    
    if (!image) image = info[UIImagePickerControllerOriginalImage];
    if (image) {
        self.wasPhotoChanged = YES;
        //  self.profilePicOriginal = image;
        UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
        CGRect frame = imageView.frame;
        if (frame.size.width > MAX_IMAGE_WIDTH) {
            frame.size.height = (frame.size.height / frame.size.width) * MAX_IMAGE_WIDTH;
            frame.size.width = MAX_IMAGE_WIDTH;
        }
        imageView.frame = frame;
        
        self.photoPreviewImage.image = image;
        
        self.viObject = [[VaidenImageObject alloc] initWithOrigImageData:UIImageJPEGRepresentation([image imageScaledToFitSize:CGSizeMake(1000, 1000)], 1.0)
                                                 andRegularSizeImageData:UIImageJPEGRepresentation([image imageScaledToFitSize:CGSizeMake(320, 320)], 1.0)
                                           andRegularSizeRetinaImageData:UIImageJPEGRepresentation([image imageScaledToFitSize:CGSizeMake(640, 640)], 1.0)
                                                        andThumbnailData:UIImageJPEGRepresentation([image imageScaledToFitSize:CGSizeMake(70, 70)], 1.0)
                                                  andRetinaThumbnailData:UIImageJPEGRepresentation([image imageScaledToFitSize:CGSizeMake(140, 140)], 1.0)
                                                        andFileExtension:@"jpg"
                                                                andImage:image];
        
        
    }
    
    if (self.imagePickerPopover) {
        
        [self.imagePickerPopover dismissPopoverAnimated:YES];
        self.imagePickerPopover = nil;
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
}

- (void)setWithViewController:(ChooseSeasonTVC *)controller withSeason:(NSString *)season
{
    self.seasonObject.season = season;
    [self.tagCollectionView reloadData];
}

- (void)setWithViewController:(ChooseYearTVC *)controller withYear:(NSString *)year
{
    self.yearObject.year = year;
    [self.tagCollectionView reloadData];
}

- (void)setWithViewController:(ChooseSportUtilityTVC *)controller withSports:(NSMutableArray *)sportsArray isMultiple:(BOOL)isMultiple
{
    if (!isMultiple) {
        self.sportObject = [sportsArray objectAtIndex:0]; // should only be one object in array
        [self.tagCollectionView reloadData];
    }
}

- (void)setWithAllContactsController:(ChooseContactsUtilityTVC *)controller withContactsArray:(NSMutableArray *)contactsArray
{
    self.contactsToTagArray = contactsArray;
    [self.tagCollectionView reloadData];
}

- (void)setWithViewController:(ChooseLocationTVC *)controller withVenue:(Venue *)selectedVenue
{
    self.venueObject = selectedVenue;
    [self.tagCollectionView reloadData];
}

- (void)setWithViewController:(ChooseTrainingTagTVC *)controller withTrainingTags:(NSMutableArray *)trainingTags
{
    self.trainingTags = trainingTags;
    [self.tagCollectionView reloadData];
}

- (void)setWithViewController:(ChooseStatTagTVC *)controller withStatTags:(NSMutableArray *)statTags withTotal:(NSInteger)total
{
    self.total = total;
    self.statsTags = statTags;
    [self.tagCollectionView reloadData];
}

- (void)setWithViewController:(ChooseEventTagTVC *)controller withEvent:(EventTag *)event
{
    self.event = event;
    [self.tagCollectionView reloadData];
}
@end
