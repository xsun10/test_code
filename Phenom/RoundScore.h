//
//  RoundScore.h
//  Phenom
//
//  Created by James Chung on 7/23/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RoundScore : NSObject

// A single round

@property (nonatomic) NSInteger numRound;

-(RoundScore*)initWithScore:(NSInteger)numRound;

@end
