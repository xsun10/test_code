//
//  RatingsVenuesListTVC.h
//  Phenom
//
//  Created by James Chung on 7/5/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RatingsVenuesListTVC : UITableViewController

@property (nonatomic) NSInteger venueID;
@end
