//
//  ChooseSportToRateTVC.m
//  Vaiden
//
//  Created by James Chung on 12/27/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "ChooseSportToRateTVC.h"
#import "IOSRequest.h"
#import "UserPreferences.h"
#import "ChooseSportToRateCell.h"
#import "UIColor+VaidenColors.h"
#import "SportRatingsSummaryTVC.h"
#import "PlayerSport.h"

@interface ChooseSportToRateTVC ()

@property (nonatomic, strong) NSMutableArray *playerSports;
@property (nonatomic, strong) UserPreferences *preferences;
@property (nonatomic, strong) UIView *tempView;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
@property (nonatomic) BOOL resultsExist;

@property (nonatomic) NSUInteger selectedRow;

@end

@implementation ChooseSportToRateTVC

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (NSMutableArray *)playerSports
{
    if (!_playerSports) _playerSports = [[NSMutableArray alloc] init];
    return _playerSports;
}

- (void)showTempSpinner
{
    self.tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    self.tempView.backgroundColor = [UIColor lightGrayColor];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] init];
    [self.activityIndicator setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width/2.0, [UIScreen mainScreen].bounds.size.height/2.0 - 50)];
    [self.activityIndicator startAnimating];
    [self.tempView addSubview:self.activityIndicator];
    
    [self.view addSubview:self.tempView];
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];

    [self showTempSpinner];
    self.resultsExist = NO;

    [IOSRequest fetchPlayerSports:self.playerToRate.userID
                     onCompletion:^(NSMutableArray *results) {
        
                         for (id object in results) {
                             
                             PlayerSport *sport = [[PlayerSport alloc] initWithSportDetails:[object[@"sport_id"] integerValue]
                                                                                    andName:object[@"sport_name"]
                                                                                    andType:@"individual"
                                                                                   andLevel:object[@"sport_level"]
                                                                             andSportRecord:object[@"win_loss_record"]];
                             
                             
                             [sport setSportID:[object[@"sport_id"] integerValue]];
                             
                       
                             
                             [self.playerSports addObject:sport];

                         }
                         
                         dispatch_async(dispatch_get_main_queue(), ^{
                             [self.activityIndicator stopAnimating];
                             self.tempView.hidden = YES;
                             
                             if ([results count] > 0)
                                 self.resultsExist = YES;
                             
                             [self.tableView reloadData];
                         });
        
    }];
   
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.playerSports count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Sport Cell";
    ChooseSportToRateCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    Sport *uSport = [self.playerSports objectAtIndex:indexPath.row];
    
    cell.sportLabel.text = [uSport.sportName capitalizedString];
    cell.sportIcon.image = [[uSport getSportIcon] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [cell.sportIcon setTintColor:[UIColor peacock]];
    
    // Configure the cell...
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.resultsExist) {
        self.selectedRow = indexPath.row;
        [self performSegueWithIdentifier:@"Sport Rate Segue" sender:self];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Sport Rate Segue"]) {
        SportRatingsSummaryTVC *controller = segue.destinationViewController;
        PlayerSport *sport = [self.playerSports objectAtIndex:self.selectedRow];
        controller.sport = sport;
        controller.playerToBeRated = self.playerToRate;
    }
}

@end
