//
//  SetVenuePicsCVCell.h
//  Vaiden
//
//  Created by James Chung on 10/8/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SetVenuePicsCVCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *venuePic;
@property (weak, nonatomic) IBOutlet UIImageView *checkMarkIcon;
@end
