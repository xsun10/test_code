//
//  IndividualMatchResultCell.h
//  Phenom
//
//  Created by James Chung on 5/1/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsIndividualMatchResultCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *profilePic;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeFrameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *userImage1;
@property (weak, nonatomic) IBOutlet UIImageView *userImage2;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel1;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel2;
@property (weak, nonatomic) IBOutlet UILabel *levelUser1;
@property (weak, nonatomic) IBOutlet UILabel *levelUser2;
@property (weak, nonatomic) IBOutlet UILabel *probabilityLabel;
@property (weak, nonatomic) IBOutlet UILabel *probabilityMessageLabel;
@property (weak, nonatomic) IBOutlet UILabel *scoreUser1Label;
@property (weak, nonatomic) IBOutlet UILabel *scoreUser2Label;
@property (weak, nonatomic) IBOutlet UILabel *sportLabel;
@property (weak, nonatomic) IBOutlet UILabel *scoreUsername1Label;
@property (weak, nonatomic) IBOutlet UILabel *scoreUsername2Label;
@property (weak, nonatomic) IBOutlet UILabel *recordUsername1Label;
@property (weak, nonatomic) IBOutlet UILabel *recordUsername2Label;
@property (weak, nonatomic) IBOutlet UILabel *recordUser1;
@property (weak, nonatomic) IBOutlet UILabel *recordUser2;
@property (weak, nonatomic) IBOutlet UIButton *commentButton;
@property (weak, nonatomic) IBOutlet UIImageView *commentIcon;
@property (weak, nonatomic) IBOutlet UILabel *commentLabel;
@property (weak, nonatomic) IBOutlet UIButton *remixButton;
@property (weak, nonatomic) IBOutlet UIImageView *remixIcon;
@property (weak, nonatomic) IBOutlet UILabel *remixLabel;
@property (weak, nonatomic) IBOutlet UIImageView *timerIcon;

@end
