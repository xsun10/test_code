//
//  IndividualMatchScore.h
//  Phenom
//
//  Created by James Chung on 7/29/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IndividualMatchScore : NSObject

@property (nonatomic, strong) NSMutableArray *roundScoresIndMatch;

// up to two versions of a match score are allowed
@property (nonatomic) NSInteger scoreRecordedBy;
@property (nonatomic) NSInteger scoreConfirmedBy;
@property (nonatomic) NSInteger scoreDisputedBy;
// so we know to display message to session user stating their score has been disputed
@property (nonatomic) BOOL wasFirstVersionIndividualScoreRecordedBySessionUserAndDisputedByOtherUser;

@property (nonatomic) NSInteger version;

- (IndividualMatchScore *) initWithMatchScore;
- (void)addRoundScore:(NSInteger)numRound
           forPlayer1:(NSInteger)userIDPlayer1
     withScorePlayer1:(NSInteger)scorePlayer1
           forPlayer2:(NSInteger)userIDPlayer2
     withScorePlayer2:(NSInteger)scorePlayer2;
- (NSMutableDictionary *)getNetScore;

@end
