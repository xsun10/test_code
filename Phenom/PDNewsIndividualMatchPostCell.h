//
//  PDNewsIndividualMatchPostCell.h
//  Vaiden
//
//  Created by James Chung on 2/22/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewsfeedMatchHeadingCV.h"


@class PDNewsIndividualMatchPostCell;

@protocol PDNewsIndividualMatchPostCell_delegate <NSObject>
-(void)setViewController:(PDNewsIndividualMatchPostCell *)controller withUserIDToSegue:(NSInteger)userID;
@end

@interface PDNewsIndividualMatchPostCell : UITableViewCell <UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, weak) id <PDNewsIndividualMatchPostCell_delegate> delegate;
@property (assign) NSInteger currentID;
@property (weak, nonatomic) IBOutlet UIImageView *userImage1;
@property (weak, nonatomic) IBOutlet UIImageView *userImage2;
@property (weak, nonatomic) IBOutlet UIButton *userImageButton1;
@property (weak, nonatomic) IBOutlet UIButton *userImageButton2;
@property (weak, nonatomic) IBOutlet UIImageView *sportIcon;
@property (weak, nonatomic) IBOutlet UIView *vsCircleView;
@property (weak, nonatomic) IBOutlet UIView *sportIconBackground;
@property (weak, nonatomic) IBOutlet UIImageView *newsfeedIcon;

@property (weak, nonatomic) IBOutlet UILabel *userNameLabel1;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel2;
@property (weak, nonatomic) IBOutlet UILabel *levelUser1;
@property (weak, nonatomic) IBOutlet UILabel *levelUser2;
@property (weak, nonatomic) IBOutlet UILabel *sharedByLabel;
@property (weak, nonatomic) IBOutlet UILabel *matchNameLabel;
//@property (weak, nonatomic) IBOutlet NewsfeedMatchHeadingCV *playerCV;
@property (weak, nonatomic) IBOutlet UIView *backgroundViewArea;
@property (weak, nonatomic) IBOutlet UIView *detailButtonView;
@property (weak, nonatomic) IBOutlet UIButton *detailButton;
@property (weak, nonatomic) IBOutlet NewsfeedMatchHeadingCV *matchHeadingCV;
@property (nonatomic, strong) NSString *matchCategory;
@property (weak, nonatomic) IBOutlet UILabel *sportLabel;
@property (nonatomic, strong) NSMutableArray *competitors;
@property (weak, nonatomic) IBOutlet UIView *headerBackground;
@property (weak, nonatomic) IBOutlet UILabel *headlineSportLabel;
@property (weak, nonatomic) IBOutlet UIImageView *headlineSportPic;

@property (weak, nonatomic) IBOutlet UIImageView *matchStatusIcon;
@property (weak, nonatomic) IBOutlet UILabel *matchStatus;
@property (weak, nonatomic) IBOutlet UIImageView *sportBackgroundImage;
@property (weak, nonatomic) IBOutlet UILabel *commentNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *commentTextLabel;
@property (weak, nonatomic) IBOutlet UIButton *commentButton;
@property (weak, nonatomic) IBOutlet UILabel *shareNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *shareTextLabel;
@property (weak, nonatomic) IBOutlet UIButton *shareButton;
@property (weak, nonatomic) IBOutlet UILabel *postDateTimeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *commentButtonIcon;
@property (weak, nonatomic) IBOutlet UIView *commentButtonView;
@property (weak, nonatomic) IBOutlet UIImageView *shareButtonIcon;
@property (weak, nonatomic) IBOutlet UIView *shareButtonView;
@property (weak, nonatomic) IBOutlet UIView *section2View;
@property (weak, nonatomic) IBOutlet UIButton *actionButtonHandle;
@property (weak, nonatomic) IBOutlet UIView *smallCircleView;

@property (weak, nonatomic) IBOutlet UIView *likeButtonBackground;
@property (weak, nonatomic) IBOutlet UIImageView *likeIcon;
@property (weak, nonatomic) IBOutlet UILabel *likeNumberLabel;
@property (weak, nonatomic) IBOutlet UIButton *likeButton;
@property (weak, nonatomic) IBOutlet UIButton *viewLikesButton;
@property (weak, nonatomic) IBOutlet UIButton *viewCommentsButton;
@end
