//
//  NewsfeedAlert.m
//  Vaiden
//
//  Created by James Chung on 6/1/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "NewsfeedAlert.h"
@interface NewsfeedAlert ()

@end

@implementation NewsfeedAlert

// newsfeed types
#define PICKUP_MATCH_CREATION 1
#define INDIVIDUAL_MATCH_CREATION 2
#define INDIVIDUAL_MATCH_CONFIRMATION 3
#define INDIVIDUAL_MATCH_RESULT 4
#define USER_TEXT_POST 5
#define USER_TEXT_POST_WITH_IMAGE 6
#define INDIVIDUAL_MATCH_UPDATE 7
#define PICKUP_MATCH_UPDATE 8
#define INDIVIDUAL_MATCH_PLAYER_UPDATE 9
#define PICKUP_MATCH_PLAYER_UPDATE 10
#define INDIVIDUAL_MATCH_RECORD_SCORE_UPDATE 11
#define INDIVIDUAL_MATCH_CONFIRM_SCORE_UPDATE 12
#define INDIVIDUAL_MATCH_DISPUTE_SCORE_UPDATE 13
#define COMPETITION_RSVP 14
#define VENUE_KING_UPDATE 15
#define TEAM_CREATION 16
#define TEAM_MATCH_CREATION 17
#define VENUE_CHECKIN 18
#define USER_LOCKER_POST 19

- (NewsfeedAlert *)initWithPlayerAlertDetails:(NSInteger)userID
                                  andUserName:(NSString *)userName
                          andProfilePicString:(NSString *)profilePicString
                                   andMessage:(NSString *)message
                                 andAlertType:(NSInteger)alertType
                                 andAlertDate:(NSDate *)alertDate
                                 andWasViewed:(BOOL)wasViewed
                                andNewsfeedID:(NSInteger)newsfeedID
                          andNewsfeedPostType:(NSInteger)newsfeedPostType
{
    self = [super initWithPlayerAlertDetails:userID
                                 andUserName:userName
                         andProfilePicString:profilePicString
                                  andMessage:message
                                andAlertType:alertType
                                andAlertDate:alertDate
                                andWasViewed:wasViewed];
    if (self) {
        _newsfeedID = newsfeedID;
        _newsfeedPostType = newsfeedPostType;
    }
    return self;
}

- (NSString *)getStringRepresentationOfNewsfeedType
{
    if (self.newsfeedPostType == PICKUP_MATCH_CREATION ||
        self.newsfeedPostType == PICKUP_MATCH_UPDATE ||
        self.newsfeedPostType == PICKUP_MATCH_PLAYER_UPDATE) {
            return @"pickup";
    } else if (self.newsfeedPostType == INDIVIDUAL_MATCH_CREATION ||
               self.newsfeedPostType == INDIVIDUAL_MATCH_CONFIRMATION ||
               self.newsfeedPostType == INDIVIDUAL_MATCH_RESULT ||
               self.newsfeedPostType == INDIVIDUAL_MATCH_UPDATE ||
               self.newsfeedPostType == INDIVIDUAL_MATCH_PLAYER_UPDATE ||
               self.newsfeedPostType == INDIVIDUAL_MATCH_RECORD_SCORE_UPDATE ||
               self.newsfeedPostType == INDIVIDUAL_MATCH_CONFIRM_SCORE_UPDATE ||
               self.newsfeedPostType == INDIVIDUAL_MATCH_DISPUTE_SCORE_UPDATE) {
        return @"individual";
    } else if (self.newsfeedPostType == VENUE_CHECKIN) {
        return @"venue_checkin";
    } else if (self.newsfeedPostType == VENUE_KING_UPDATE) {
        return @"venue_king";
    } else if (self.newsfeedPostType == USER_LOCKER_POST) {
        return @"locker";
    }
    
    return @"";
}
@end
