//
//  NewsComment.h
//  Vaiden
//
//  Created by James Chung on 9/27/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "Comment.h"

@interface NewsComment : Comment

@property (nonatomic) NSUInteger newsID;

- (NewsComment *)initWithCommentDetails:(NSInteger)commentID
                     andCommentText:(NSString *)commentText
                           byUserID:(NSInteger)userID
                        andUsername:(NSString *)username
                      andProfileStr:(NSString *)profileBaseString
                        forNewsPost:(NSInteger)newsID
                             onDate:(NSDate *)postDate;

@end
