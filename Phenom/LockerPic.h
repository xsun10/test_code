//
//  LockerPic.h
//  Vaiden
//
//  Created by James Chung on 5/12/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Player.h"
#import "StatsGroup.h"

@interface LockerPic : NSObject

@property (nonatomic) NSInteger lockerID;
@property (nonatomic, strong) NSString *imageFileName;
@property (nonatomic, strong) Player *creator;
@property (nonatomic, strong) NSString *picSeason;
@property (nonatomic) NSInteger picYear;
@property (nonatomic, strong) NSMutableArray *tagsArray;
@property (nonatomic, strong) NSMutableArray *taggedPeopleArray;
@property (nonatomic, strong) NSMutableArray *sharedFollowersArray;
//@property (nonatomic, strong) NSMutableArray *statsTagsArray;
@property (nonatomic, strong) StatsGroup *statsGrp;
@property (nonatomic, strong) NSMutableArray *trainingTagsArray;
@property (nonatomic) BOOL shareToAllFollowers;
@property (nonatomic, strong) NSString *caption;
@property (nonatomic, strong) NSMutableArray *comments;
@property (nonatomic, strong) NSMutableArray *likes;
@property (nonatomic, strong) NSDate *postDate;
@property (nonatomic, strong) Sport *sp;

@property (nonatomic) NSInteger numComments;
@property (nonatomic) NSInteger numLikes;

// use this on pic detail page
- (LockerPic *)initWithDetails:(NSInteger)lockerID
                   andFileName:(NSString *)imageFileName
                    andCreator:(Player *)creator
                     andSeason:(NSString *)picSeason
                       andYear:(NSInteger)picYear
                       andTags:(NSMutableArray *)tagsArray
               andTaggedPeople:(NSMutableArray *)taggedPeopleArray
            andSharedFollowers:(NSMutableArray *)sharedFollowersArray
    andShareToAllFollowersFlag:(BOOL)shareToAllFollowers
                    andCaption:(NSString *)caption
                   andComments:(NSMutableArray *)comments
                      andLikes:(NSMutableArray *)likes
                   andPostDate:(NSDate *)postDate;

// use this when showing in 1 album
- (LockerPic *)initWithDetails:(NSInteger)lockerID
                   andFileName:(NSString *)imageFileName
                    andCreator:(Player *)creator
                     andSeason:(NSString *)picSeason
                       andYear:(NSInteger)picYear
                andNumComments:(NSInteger)numComments
                   andNumLikes:(NSInteger)numLikes
                   andPostDate:(NSDate *)postDate;

- (LockerPic *)init;
@end

