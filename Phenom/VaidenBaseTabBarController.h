//
//  VaidenBaseTabBarController.h
//  Vaiden
//
//  Created by James Chung on 3/6/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VaidenBaseTabBarController : UITabBarController

@property (nonatomic) BOOL showVPage;

@end
