//
//  VMainVC.m
//  Vaiden
//
//  Created by James Chung on 4/22/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "VMainVC.h"
#import "UIView+Manipulate.h"
#import "MKNumberBadgeView.h"
#import "UIColor+VaidenColors.h"
#import "UserPreferences.h"
#import "IOSRequest.h"
#import "VCVCell.h"
#import "UIImage+ProportionalFill.h"
#import "Constants.h"
#import "Response.h"
#import "AmazonKeyChainWrapper.h"
#import "AmazonClientManager.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "S3Tools.h"
#import "AFNetworking.h"
#import "VaidenImageObject.h"
#import "AddLockerPicturePreStepTVC.h"
#import "VMainTVC.h"
#import "ExploreTVC.h"
#import "VoteShopVC.h"

#import "S3Tools2.h"

@interface VMainVC () <UITableViewDelegate, UITableViewDataSource>//<UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *pageScrollView;
@property (nonatomic, strong) UserPreferences *preferences;
#pragma - mark old version
/*@property (weak, nonatomic) IBOutlet UIImageView *shirtIcon;
@property (weak, nonatomic) IBOutlet UIView *shirtCircle;
@property (weak, nonatomic) IBOutlet UIView *achieveCircle;
@property (weak, nonatomic) IBOutlet UIImageView *achieveIcon;
@property (weak, nonatomic) IBOutlet UIView *checkinCircle;
@property (weak, nonatomic) IBOutlet UIImageView *checkinIcon;
@property (weak, nonatomic) IBOutlet UIView *competeCircle;
@property (weak, nonatomic) IBOutlet UIImageView *competeIcon;
@property (weak, nonatomic) IBOutlet UIView *darkenedOverlayOverCompete;
@property (weak, nonatomic) IBOutlet UIView *darkenedOverlayOverAchieve;
@property (weak, nonatomic) IBOutlet UIView *darkenedOverlayOverCheckin;
@property (weak, nonatomic) IBOutlet UIView *darkenedOverlayOverGear;

@property (nonatomic, strong) UIButton *alertButton;
@property (nonatomic, strong) UIBarButtonItem *alertBarButtonItem;
@property (nonatomic, strong) MKNumberBadgeView *alertNumber;

@property (weak, nonatomic) IBOutlet UIPageControl *actionPageControl;
@property (weak, nonatomic) IBOutlet UICollectionView *vCollectionView;

@property (weak, nonatomic) IBOutlet UIImageView *subPic1;
@property (weak, nonatomic) IBOutlet UIImageView *subPic2;
@property (weak, nonatomic) IBOutlet UIImageView *subPic3;
@property (weak, nonatomic) IBOutlet UIImageView *subPic4;


@property (weak, nonatomic) UIActionSheet *choosePicActionSheet;
@property (strong, nonatomic)UIPopoverController *imagePickerPopover;
@property (nonatomic, strong) VaidenImageObject *viObject;

@property (nonatomic, strong) S3Tools2 *regImageSizeUploader;
@property (nonatomic, strong) S3Tools2 *regImageRetinaSizeUploader;
@property (nonatomic, strong) S3Tools2 *thumbnailImageSizeUploader;
@property (nonatomic, strong) S3Tools2 *thumbnailImageRetinaSizeUploader;
@property (nonatomic, strong) NSString *imageFileName;
@property (nonatomic, strong) NSString *imageFileNameBaseString;*/
#pragma - mark new version
@property (weak, nonatomic) IBOutlet UIView *hourView;
@property (weak, nonatomic) IBOutlet UIView *minuteView;
@property (weak, nonatomic) IBOutlet UIView *secondView;
@property (weak, nonatomic) IBOutlet UILabel *hourLabel;
@property (weak, nonatomic) IBOutlet UILabel *minuteLabel;
@property (weak, nonatomic) IBOutlet UILabel *secondLabel;
@property (weak, nonatomic) IBOutlet UIView *votesBarView;
@property (weak, nonatomic) IBOutlet UILabel *votes;
@property (weak, nonatomic) IBOutlet UILabel *rankLabel;
@property (weak, nonatomic) IBOutlet UILabel *daysLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UILabel *h;
@property (weak, nonatomic) IBOutlet UILabel *s;
@property (weak, nonatomic) IBOutlet UILabel *m;
@property (weak, nonatomic) IBOutlet UIView *hTag;
@property (weak, nonatomic) IBOutlet UIView *mTag;
@property (weak, nonatomic) IBOutlet UIView *sTag;
@property (weak, nonatomic) IBOutlet UIButton *buyVoteBtn;
@property (nonatomic) NSInteger targetTimestamp;
@property (nonatomic) BOOL isVoted;

@property (nonatomic, strong) NSTimer *timer; // timer for count down

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeight;

@property (weak, nonatomic) IBOutlet UILabel *locationInfo;
@property (nonatomic) NSInteger nextdays;
@property (weak, nonatomic) IBOutlet UIView *votedView;
@property (weak, nonatomic) IBOutlet UIView *unvotedView;
@end

@implementation VMainVC

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
    self.modalPresentationStyle = UIModalPresentationCurrentContext;
    self.tabBarController.modalPresentationStyle = UIModalPresentationCurrentContext;
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    self.targetTimestamp = 24*3600; //24hours second

    /*self.vCollectionView.delegate = self;
    self.vCollectionView.dataSource = self;
    
//    [self initializeAlertButtonWithoutNumber];
    
    [self.vCollectionView reloadData];
    
    self.subPic1.image = [UIImage imageNamed:@"V Photo"];
    self.subPic2.image = [UIImage imageNamed:@"V Checkin"];
    self.subPic3.image = [UIImage imageNamed:@"V Compete"];
    self.subPic4.image = [UIImage imageNamed:@"V Gear"];
    
    NSShadow *shadow = [[NSShadow alloc] init];
    shadow.shadowColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8];
    shadow.shadowOffset = CGSizeMake(0, 1);*/
/*    [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                                           [UIColor whiteColor], NSForegroundColorAttributeName,
                                                           shadow, NSShadowAttributeName,
                                                           [UIFont fontWithName:@"TrebuchetMS-Bold" size:21.0], NSFontAttributeName, nil]];
*/
    
    // Set the title image for navigation bar
    UIImage *title = [UIImage imageNamed:@"dashboard_title"];
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:title];
    
    [self.hourView makeRoundedBorderWithRadius:5];
    [self.minuteView makeRoundedBorderWithRadius:5];
    [self.secondView makeRoundedBorderWithRadius:5];
    [self.votesBarView makeRoundedBorderWithRadius:3];
    
    [self.hTag makeRoundedBorderWithRadius:5];
    [self.mTag makeRoundedBorderWithRadius:5];
    [self.sTag makeRoundedBorderWithRadius:5];
    [self.buyVoteBtn makeRoundedBorderWithRadius:5];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    //[self.pageScrollView setBounces:NO];
    [self.tableView setBounces:NO];
    [self firstlogin];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self preparePage];
    /*if ([self.preferences getNewsfeedRefreshState]) {
        [self.tabBarController setSelectedIndex:0];
    }*/
    //[self loadVData];
    
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    if ([[UIScreen mainScreen] bounds].size.height <= 480) {
        self.pageScrollView.contentSize = CGSizeMake(320.0, 640.0);
    } else {
        self.pageScrollView.contentSize = CGSizeMake(320.0, 550.0);
    }
    
}

- (void) preparePage
{
    [self getVote]; // Get the vote for user
    [self getMonthlyRank];
}

- (void) getVote
{
    [IOSRequest getTotalVoteForUser:[self.preferences getUserID]
                       onCompletion:^(NSDictionary *result) {
                           if ([result[@"outcome"] isEqualToString:@"failure"]) {
                               UIAlertView *alert = [[UIAlertView alloc] init];
                               alert.title = @"Submission Error";
                               alert.message = @"There was a problem submitting your request.  Please try again.";
                               [alert addButtonWithTitle:@"OK"];
                               [alert show];
                           } else if ([result[@"outcome"] isEqualToString:@"not_found"]) {
                               UIAlertView *alert = [[UIAlertView alloc] init];
                               alert.title = @"Submission Error";
                               alert.message = @"There was a problem submitting your request.  Please try again.";
                               [alert addButtonWithTitle:@"OK"];
                               [alert show];
                           } else if ([result[@"outcome"] isEqualToString:@"no_result"]) {
                               [self setTotalVoteNumber:self.votes withNumber:0];
                           } else if ([result[@"outcome"] isEqualToString:@"success"]) {
                               if ([(NSString *)result[@"voted"] isEqualToString:@"1"]) {
                                   self.isVoted = YES;
                                   [self.votedView setHidden:NO];
                                   [self.unvotedView setHidden:YES];
                               } else {
                                   self.isVoted = NO;
                                   [self.votedView setHidden:YES];
                                   [self.unvotedView setHidden:NO];
                               }
                               if (![self.timer isValid]) {
                                   [self getServerTime];
                               }
                               
                               [self setTotalVoteNumber:self.votes withNumber:[(NSString *)result[@"vote_num"] integerValue]];
                               
                               [self setRegion:result[@"region"]];
                           }
                       }];
}

- (void) setRegion:(NSString *)region
{
    if ([region isEqualToString:@"Northeast"]) {
        self.locationInfo.text = @"NE";
    } else if ([region isEqualToString:@"Southeast"]) {
        self.locationInfo.text = @"SE";
    } else if ([region isEqualToString:@"Midwest"]) {
        self.locationInfo.text = @"MW";
    } else if ([region isEqualToString:@"South"]) {
        self.locationInfo.text = @"S";
    } else if ([region isEqualToString:@"West"]) {
        self.locationInfo.text = @"W";
    } else { // unknown
        self.locationInfo.text = @"--";
    }
}

- (void) getMonthlyRank
{
    // There is no action, just display information, not necessary to disable the UI
    [IOSRequest getVotesInfoForUser:[self.preferences getUserID]
                       onCompletion:^(NSDictionary *result) {
                           NSLog(@"%@",result);
                           if ([result[@"outcome"] isEqualToString:@"success"]) {
                               self.rankLabel.text = result[@"rank"];
                           } else if ([result[@"outcome"] isEqualToString:@"no_result"]) {
                               self.rankLabel.text = @"--";
                           } else if ([result[@"outcome"] isEqualToString:@"not_found"]) {
                               UIAlertView *alert = [[UIAlertView alloc] init];
                               alert.title = @"Submission Error";
                               alert.message = @"There was a problem submitting your request.  Please try again.";
                               [alert addButtonWithTitle:@"OK"];
                               [alert show];
                           } else if ([result[@"outcome"] isEqualToString:@"failure"]) {
                               UIAlertView *alert = [[UIAlertView alloc] init];
                               alert.title = @"Submission Error";
                               alert.message = @"There was a problem submitting your request.  Please try again.";
                               [alert addButtonWithTitle:@"OK"];
                               [alert show];
                           }
                       }];
}

- (void) setTotalVoteNumber:(UILabel *)label withNumber:(NSInteger)num
{
    NSInteger total = 0;
    if (!self.isVoted) {
        total = num + 1; // 1 for expiring vote each day
        label.text = [NSString stringWithFormat:@"%d",total];
    } else {
        label.text = [NSString stringWithFormat:@"%d",num];
    }
}

- (void) getServerTime
{
    [IOSRequest getTimeFromServerWithCompletion:^(NSDictionary *result) {
        NSLog(@"%@",result);
        NSInteger now = [result[@"hour"] integerValue]*3600 + [result[@"min"] integerValue]*60 + [result[@"sec"] integerValue];
        self.targetTimestamp = 24 * 3600 - now;
        //self.targetTimestamp = 10;
        [self updateTimeBar:[NSString stringWithFormat:@"%d",self.targetTimestamp / 3600]
                    minutes:[NSString stringWithFormat:@"%d",(self.targetTimestamp % 3600) / 60]
                    seconds:[NSString stringWithFormat:@"%d",(self.targetTimestamp %3600) % 60]];

        NSInteger daysLeft = [result[@"days"] integerValue] - [result[@"day"] integerValue] + 1;
        self.daysLabel.text = [NSString stringWithFormat:@"%d", daysLeft];
        //self.daysLabel.text = [NSString stringWithFormat:@"%d", 1];
        self.nextdays = [result[@"daysNext"] integerValue];
        [self startTimer];
    }];
}

- (void) firstlogin
{
    [IOSRequest getLoginNums:[self.preferences getUserID]
                               onCompletion:^(NSDictionary *results) {
                                   NSLog(@"%@",results);

                                   dispatch_async(dispatch_get_main_queue(), ^{
                                       if([results[@"num"] isEqualToString:@"1"]){
                                    [self performSegueWithIdentifier:@"coach_segue" sender:self];

                                       }
                                       
                                   });
                               }];
    
}

- (void)updateTimeBar:(NSString *)hour minutes:(NSString *)minutes seconds:(NSString *)seconds
{
    if (self.isVoted) {
        self.h.text = hour;
        self.m.text = minutes;
        self.s.text = seconds;
    } else {
        self.hourLabel.text = hour;
        self.minuteLabel.text = minutes;
        self.secondLabel.text = seconds;
    }
    
}

- (void)startTimer
{
    if ([self.timer isValid]) {
        [self.timer invalidate];
        self.timer = nil;
    }
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(updateCounter:) userInfo:nil repeats:YES];
}

- (void)updateCounter:(NSTimer *)theTimer
{
    if(self.targetTimestamp > 0 ){
        self.targetTimestamp -- ;
        if (self.targetTimestamp == 0) {
            if ([self.daysLabel.text integerValue] - 1 == 0) {
                self.daysLabel.text = [NSString stringWithFormat:@"%d",self.nextdays];
            } else {
                self.daysLabel.text = [NSString stringWithFormat:@"%d",[self.daysLabel.text integerValue] - 1];
            }
            self.targetTimestamp = 24 * 3600;
            [self updateTimeBar:[NSString stringWithFormat:@"%d",self.targetTimestamp / 3600]
                        minutes:[NSString stringWithFormat:@"%d",(self.targetTimestamp % 3600) / 60]
                        seconds:[NSString stringWithFormat:@"%d",(self.targetTimestamp % 3600) % 60]];
        } else {
            [self updateTimeBar:[NSString stringWithFormat:@"%d",self.targetTimestamp / 3600]
                        minutes:[NSString stringWithFormat:@"%d",(self.targetTimestamp % 3600) / 60]
                        seconds:[NSString stringWithFormat:@"%d",(self.targetTimestamp %3600) % 60]];
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 49;
}

#pragma - mark table view datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    VMainTVC *cell = (VMainTVC *)[tableView dequeueReusableCellWithIdentifier:@"vmain_cell"];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    if (indexPath.row == 0) {
        cell.icon.image = [UIImage imageNamed:@"rank"];
        [cell.icon setContentMode:UIViewContentModeCenter];
        
        cell.title.text = @"RANKINGS";
    } else if (indexPath.row == 1) {
        cell.icon.image = [UIImage imageNamed:@"profile_gold"];
        [cell.icon setContentMode:UIViewContentModeCenter];
        
        cell.title.text = @"YOUR SUPPORT";
    } else if (indexPath.row == 2) {
        cell.icon.image = [UIImage imageNamed:@"search_gold"];
        [cell.icon setContentMode:UIViewContentModeCenter];
        
        cell.title.text = @"DISCOVER USERS";
    } else if (indexPath.row == 3) {
        cell.icon.image = [UIImage imageNamed:@"attention_gold"];
        [cell.icon setContentMode:UIViewContentModeCenter];
        
        cell.title.text = @"HOW IT WORKS";
    } else {
        cell.icon.image = [UIImage imageNamed:@"cart"];
        [cell.icon setContentMode:UIViewContentModeCenter];
        
        cell.title.text = @"SHOP";
    }
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        [self performSegueWithIdentifier:@"ranking_segue" sender:self];
    } else if (indexPath.row == 1) {
        [self performSegueWithIdentifier:@"support_segue" sender:self];
    } else if (indexPath.row == 2) {
        [self performSegueWithIdentifier:@"discover_segue" sender:self];
    } else if (indexPath.row == 3) {
        [self performSegueWithIdentifier:@"howitworks_segue" sender:self];
    } else {
        [self performSegueWithIdentifier:@"shop_segue" sender:self];
    }
}
- (IBAction)coach_mark:(id)sender {
     [self performSegueWithIdentifier:@"coach_segue" sender:sender];
}

- (IBAction)buyVote:(id)sender {
    [self performSegueWithIdentifier:@"shop_segue" sender:sender];
}

#pragma - mark segue
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"shop_segue"]) {
        VoteShopVC *controller = (VoteShopVC *)segue.destinationViewController;
        controller.fromProfile = NO;
    }
    if([segue.identifier isEqualToString:@"coach_segue"])  {
        id theSegue = segue.destinationViewController;
        [theSegue setValue:@"coach_main" forKey:@"title"];
    }
}

#pragma - mark old version implementations
/*- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 4;
}

#define PHOTO_ACTION 0
#define CHECKIN_ACTION 1
#define COMPETE_ACTION 2
#define GEAR_ACTION 3

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    VCVCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"My Cell" forIndexPath:indexPath];

    [cell.goButton makeRoundedBorderWithRadius:3];
    self.actionPageControl.currentPage = indexPath.row;
    
    if (indexPath.row == PHOTO_ACTION) {
        
        cell.actionImage.image = [UIImage imageNamed:@"V Photo"];
        cell.iconImage.image = [[UIImage imageNamed:@"Camera Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [cell.iconImage setTintColor:[UIColor whiteColor]];
        
        cell.actionLabel.text = @"ADD PICS";
        cell.actionDescriptionLabel.text = @"Upload and share your sports pics with your followers.";
    } else if (indexPath.row == COMPETE_ACTION) {
        cell.actionImage.image = [UIImage imageNamed:@"V Compete"];
        cell.iconImage.image = [[UIImage imageNamed:@"Versus Logo"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [cell.iconImage setTintColor:[UIColor whiteColor]];
        cell.actionLabel.text = @"COMPETE";
        cell.actionDescriptionLabel.text = @"Schedule 1v1 matches against your friends or nearby players.";
    } else if (indexPath.row == CHECKIN_ACTION) {
        cell.actionImage.image = [UIImage imageNamed:@"V Checkin"];
        cell.iconImage.image = [[UIImage imageNamed:@"Location Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [cell.iconImage setTintColor:[UIColor whiteColor]];
        cell.actionLabel.text = @"CHECK-IN";
        cell.actionDescriptionLabel.text = @"Let your friends know when and where you will be playing.";
    } else if (indexPath.row == GEAR_ACTION) {
        cell.actionImage.image = [UIImage imageNamed:@"V Gear"];
        cell.iconImage.image = [[UIImage imageNamed:@"V Shirt Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [cell.iconImage setTintColor:[UIColor whiteColor]];
        cell.actionLabel.text = @"GEAR";
        cell.actionDescriptionLabel.text = @"Check out our Kickstarter Campaign.";
    }
    
    [cell.iconBackground makeRoundedBorderWithRadius:35];
    cell.iconBackground.backgroundColor = [UIColor clearColor];
    cell.iconBackground.layer.borderColor = [UIColor whiteColor].CGColor;
    cell.iconBackground.layer.borderWidth = 2.0;
    
    return cell;
}

- (IBAction)alertsAction:(id)sender
{
    [self performSegueWithIdentifier:@"Alerts Segue" sender:self];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == PHOTO_ACTION) {
        [self choosePic:nil];
    } else if (indexPath.row == COMPETE_ACTION) {
        [self competeAction:nil];
    } else if (indexPath.row == CHECKIN_ACTION) {
        [self checkInAction:nil];
    } else if (indexPath.row == GEAR_ACTION) {
        [self gearAction:nil];
    }

}*/

/*
- (void)loadVData
{
    [IOSRequest fetchNumUnreadAlertsForUser:[self.preferences getUserID]
                               onCompletion:^(NSDictionary *results) {
                                   
                                   dispatch_async(dispatch_get_main_queue(), ^{
                                       [self setAlertButtonNumber:[results[@"outcome"] integerValue]];
                                   });
                               }];
  
}
*/
/*
- (void)initializeAlertButtonWithoutNumber
{
    // Allocate UIButton
    self.alertButton = [UIButton  buttonWithType:UIButtonTypeCustom];
    self.alertButton.frame = CGRectMake(0, 0, 30, 30);
    self.alertButton.layer.cornerRadius = 8;
    [self.alertButton setImage:[[UIImage imageNamed:@"Bell Bar Button"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]  forState:UIControlStateNormal];
    self.alertButton.imageView.tintColor = [UIColor midLightGray];
    
    //  [btn setTitle:@"Button" forState:UIControlStateNormal];
    [self.alertButton addTarget:self action:@selector(alertsAction:) forControlEvents:UIControlEventTouchUpInside];
    //[btn setBackgroundColor:[UIColor blueColor]];
    [self.alertButton setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.1 alpha:0.2]];
    self.alertButton.titleLabel.font = [UIFont systemFontOfSize:13];
    // [self.alertButton addSubview:number]; //Add NKNumberBadgeView as a subview on UIButton
    
    // Initialize UIBarbuttonitem...
    self.alertBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.alertButton];
    self.navigationItem.leftBarButtonItem = self.alertBarButtonItem;
}

- (void)setAlertButtonNumber:(NSInteger)numAlerts
{
    [self.alertNumber removeFromSuperview];
    
    // Initialize NKNumberBadgeView...
    self.alertNumber = [[MKNumberBadgeView alloc] initWithFrame:CGRectMake(17, 00, 30,20)];
    self.alertNumber.value = numAlerts;
    
    if (numAlerts == 0)
        self.alertNumber.hidden = YES;
    
    [self.alertButton addSubview:self.alertNumber];
    
}
*/


/*
- (IBAction)checkInAction:(id)sender
{
    [self performSegueWithIdentifier:@"Venue Segue" sender:self];
}
- (IBAction)competeAction:(id)sender
{
    [self performSegueWithIdentifier:@"Compete Segue" sender:self];
}

- (IBAction)gearAction:(id)sender
{
    [self performSegueWithIdentifier:@"Gear Segue" sender:self];
}

- (IBAction)vaidenAction:(id)sender
{*/
    /*
    if (self.actionPageControl.currentPage == PHOTO_ACTION) {
        [self choosePic:sender];
    } else if (self.actionPageControl.currentPage == COMPETE_ACTION) {
        [self competeAction:sender];
    } else if (self.actionPageControl.currentPage == CHECKIN_ACTION) {
        [self checkInAction:sender];
    } else if (self.actionPageControl.currentPage == GEAR_ACTION) {
        [self gearAction:sender];
    }*/
//}

/*#define TITLE_OF_ACTIONSHEET @"Add A Pic To Your Locker"
#define CANCEL_BUTTON_ACTIONSHEET @"Cancel "
#define FROM_CAMERA @"Take A Picture"
#define FROM_LIBRARY @"Choose From Photo Library"

- (IBAction)choosePic:(id)sender
{
    if (!self.choosePicActionSheet) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                      initWithTitle:TITLE_OF_ACTIONSHEET delegate:self
                                      cancelButtonTitle:CANCEL_BUTTON_ACTIONSHEET destructiveButtonTitle:nil
                                      otherButtonTitles:FROM_CAMERA, FROM_LIBRARY, nil];
        [actionSheet showFromTabBar:self.tabBarController.tabBar];
        
        self.choosePicActionSheet = actionSheet;
    }
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == actionSheet.destructiveButtonIndex) {
        [self.view.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    } else {
        NSString *choice = [actionSheet buttonTitleAtIndex:buttonIndex];
        
        if ([choice isEqualToString:FROM_CAMERA]) {
            [self changePicFromCamera];
        } else if ([choice isEqualToString:FROM_LIBRARY]){
            [self changePicFromPhotoLibrary];
        }
    }
}

- (void)changePicFromCamera
{
    [self presentImagePicker:UIImagePickerControllerSourceTypeCamera];
}


- (void) changePicFromPhotoLibrary
{
    [self presentImagePicker:UIImagePickerControllerSourceTypeSavedPhotosAlbum];
}

- (void) presentImagePicker:(UIImagePickerControllerSourceType)sourceType
{
    if ([UIImagePickerController isSourceTypeAvailable:sourceType]) {
        NSArray *availableMediaTypes = [UIImagePickerController availableMediaTypesForSourceType:sourceType];
        if ([availableMediaTypes containsObject:(NSString *)kUTTypeImage]) {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.sourceType = sourceType;
            picker.mediaTypes = @[(NSString *)kUTTypeImage];
            picker.allowsEditing = YES;
            picker.delegate = self;
            
            
            [self presentViewController:picker animated:YES completion:nil];
        }
    }
}

- (void) imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

#define MAX_IMAGE_WIDTH 70 // might want to change this to height....

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = info[UIImagePickerControllerEditedImage];
    
    if (!image) image = info[UIImagePickerControllerOriginalImage];
    if (image) {
      //  self.profilePicOriginal = image;
        UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
        CGRect frame = imageView.frame;
        if (frame.size.width > MAX_IMAGE_WIDTH) {
            frame.size.height = (frame.size.height / frame.size.width) * MAX_IMAGE_WIDTH;
            frame.size.width = MAX_IMAGE_WIDTH;
        }
        imageView.frame = frame;
        
        self.viObject = [[VaidenImageObject alloc] initWithOrigImageData:UIImageJPEGRepresentation([image imageScaledToFitSize:CGSizeMake(1000, 1000)], 1.0)
                                                 andRegularSizeImageData:UIImageJPEGRepresentation([image imageScaledToFitSize:CGSizeMake(320, 320)], 1.0)
                                           andRegularSizeRetinaImageData:UIImageJPEGRepresentation([image imageScaledToFitSize:CGSizeMake(640, 640)], 1.0)
                                                        andThumbnailData:UIImageJPEGRepresentation([image imageScaledToFitSize:CGSizeMake(70, 70)], 1.0)
                                                  andRetinaThumbnailData:UIImageJPEGRepresentation([image imageScaledToFitSize:CGSizeMake(140, 140)], 1.0)
                                                        andFileExtension:@"jpg"
                                                                andImage:image];

        
    }
    
    if (self.imagePickerPopover) {
        
        [self.imagePickerPopover dismissPopoverAnimated:YES];
        self.imagePickerPopover = nil;
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
    [self saveImageToAmazon];
    [self performSegueWithIdentifier:@"Add Pic Segue" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Add Pic Segue"]) {
        AddLockerPicturePreStepVC *controller = segue.destinationViewController;
        controller.viObject = self.viObject;
        controller.regImageSizeUploader = self.regImageSizeUploader;
        controller.regImageRetinaSizeUploader = self.regImageRetinaSizeUploader;
        controller.thumbnailImageSizeUploader = self.thumbnailImageSizeUploader;
        controller.thumbnailImageRetinaSizeUploader = self.thumbnailImageRetinaSizeUploader;
        controller.imageFileNameBaseString = self.imageFileNameBaseString;
        controller.imageFileName = self.imageFileName;
    }
}

- (void)saveImageToAmazon
{
    if (![AmazonClientManager hasCredentials]) {
        [[Constants credentialsAlert] show];
        NSLog(@"no credentials");
    }
    else {
        
        NSLog (@"has credentials");
        
        operationQueue = [NSOperationQueue new];
        operationQueue.maxConcurrentOperationCount = 3;
        
        NSMutableDictionary *imageNameDictionary = [S3Tools2 getFileNameBaseStringWithType:@"photo" forUser:[self.preferences getUserID]];
        
        self.imageFileName = [imageNameDictionary objectForKey:@"image_name"];
        self.imageFileNameBaseString = [imageNameDictionary objectForKey:@"base_string"];

        self.regImageSizeUploader = [[S3Tools2 alloc] initWithData:self.viObject.regularSizeImageData
                                                          forUser:[self.preferences getUserID]
                                                         dataType:@"photo_regsize"
                                             andBaseFileNameString:self.imageFileNameBaseString
                                                        andFileExt:self.viObject.fileExtension];
        
        [operationQueue addOperation:self.regImageSizeUploader];
        
        self.regImageRetinaSizeUploader = [[S3Tools2 alloc] initWithData:self.viObject.regularSizeRetinaImageData
                                                          forUser:[self.preferences getUserID]
                                                         dataType:@"photo_regsize_retina"
                                                   andBaseFileNameString:self.imageFileNameBaseString
                                                       andFileExt:self.viObject.fileExtension];
        
        [operationQueue addOperation:self.regImageRetinaSizeUploader];
        
        
        self.thumbnailImageSizeUploader = [[S3Tools2 alloc] initWithData:self.viObject.thumbnailImageData
                                                          forUser:[self.preferences getUserID]
                                                         dataType:@"photo_thumbnail"
                                                   andBaseFileNameString:self.imageFileNameBaseString
                                                       andFileExt:self.viObject.fileExtension];
        
        [operationQueue addOperation:self.thumbnailImageSizeUploader];
        
        self.thumbnailImageRetinaSizeUploader = [[S3Tools2 alloc] initWithData:self.viObject.thumbnailRetinaImageData
                                                          forUser:[self.preferences getUserID]
                                                         dataType:@"photo_thumbnail_retina"
                                                         andBaseFileNameString:self.imageFileNameBaseString
                                                       andFileExt:self.viObject.fileExtension];
        
        [operationQueue addOperation:self.thumbnailImageRetinaSizeUploader];
        
        
    }
}*/

@end
