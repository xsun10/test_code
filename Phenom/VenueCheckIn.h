//
//  VenueCheckIn.h
//  Vaiden
//
//  Created by James Chung on 4/24/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Player.h"

@interface VenueCheckIn : NSObject

@property (nonatomic) NSInteger checkInID;
@property (nonatomic, strong) Player *checkedInUser;
@property (nonatomic, strong) NSDate *checkedInDateTime;
@property (nonatomic, strong) NSString *message;
@property (nonatomic) BOOL isPublic;
@property (nonatomic) BOOL isNow; // otherwise would be future check in
@property (nonatomic, strong) Sport *checkedInSport;

-(VenueCheckIn *)initWithCheckInID:(NSInteger)checkInID
                 checkedInDateTime:(NSDate *)checkedInDateTime
                  checkedInMessage:(NSString *)message
                          isPublic:(BOOL)isPublic
                             isNow:(BOOL)isNow
                     checkedInUser:(Player *)checkedInUser
                    checkedInSport:(Sport *)checkedInSport;

- (NSString *)getCheckedInStatusMessage;
// an abbreviated version getCheckedInStatusMessage
- (NSString *)getTimeStatusMessage:(BOOL)showAbbrev;
- (NSString *)getAbbrevTimeStatusMessage:(BOOL)showAbbrev;

@end
