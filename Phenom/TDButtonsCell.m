//
//  TDButtonsCell.m
//  Vaiden
//
//  Created by James Chung on 4/7/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "TDButtonsCell.h"
#import "UIColor+VaidenColors.h"
#import "UIView+Manipulate.h"

@implementation TDButtonsCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)displayUserButtonsForTeam:(Team *)theTeam andSessionUser:(NSInteger)sessionUserID
{
    if (sessionUserID == theTeam.captain.userID) {
        [self makeCaptainButtons];
    } else if ([theTeam isSessionUserAlreadyConfirmedMember:sessionUserID]) {
        [self makeConfirmedMemberButtons];
    } else if ([theTeam isSessionUserUnconfirmedAndAllowedToJoinTeam:sessionUserID]) {
        [self makeUnconfirmedMemberButtons];
    }
    // else don't display buttons
}

- (void)makeCaptainButtons
{
    UIButton *deleteButton = [[UIButton alloc] initWithFrame:CGRectMake(30, 10, 125, 40)];
    [deleteButton setTitle:@"Delete Team" forState:UIControlStateNormal];
    deleteButton.tag = 1;
    deleteButton.backgroundColor = [UIColor newBlueLight];
    [deleteButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [deleteButton makeRoundedBorderWithRadius:3];
    [deleteButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:13]];
    [deleteButton addTarget:self action:@selector(deleteAction:) forControlEvents:UIControlEventTouchUpInside];

    [self.contentView addSubview:deleteButton];
    
    UIButton *inviteButton = [[UIButton alloc] initWithFrame:CGRectMake(165, 10, 125, 40)];
    [inviteButton setTitle:@"Invite Members" forState:UIControlStateNormal];
    inviteButton.tag = 1;
    inviteButton.backgroundColor = [UIColor newBlueLight];
    [inviteButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [inviteButton makeRoundedBorderWithRadius:3];
    [inviteButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:13]];
    [inviteButton addTarget:self action:@selector(inviteAction:) forControlEvents:UIControlEventTouchUpInside];

    [self.contentView addSubview:inviteButton];
}

// someone who was invited and confirmed to be a member
- (void)makeConfirmedMemberButtons
{
    UIButton *leaveButton = [[UIButton alloc] initWithFrame:CGRectMake(30, 10, 260, 40)];
    [leaveButton setTitle:@"Leave Team" forState:UIControlStateNormal];
    leaveButton.tag = 1;
    leaveButton.backgroundColor = [UIColor newBlueLight];
    [leaveButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [leaveButton makeRoundedBorderWithRadius:3];
    [leaveButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:13]];
    [leaveButton addTarget:self action:@selector(leaveAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:leaveButton];
}

// someone who was invited but has not confirmed to be a member yet
- (void)makeUnconfirmedMemberButtons
{
    UIButton *joinButton = [[UIButton alloc] initWithFrame:CGRectMake(30, 10, 260, 40)];
    [joinButton setTitle:@"Join Team" forState:UIControlStateNormal];
    joinButton.tag = 1;
    joinButton.backgroundColor = [UIColor newBlueLight];
    [joinButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [joinButton makeRoundedBorderWithRadius:3];
    [joinButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:13]];
    [joinButton addTarget:self action:@selector(joinAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:joinButton];
}

- (void)clearAllButtonsFromContentView
{
    // clear subviews
    if ([self.contentView subviews]){
        for (UIView *subview in [self.contentView subviews]) {
            if (subview.tag == 1) {
                [subview removeFromSuperview];
                
            }
        }
    }

}

- (void)deleteAction:(UIButton *)sender
{
    [self.delegate setWithCellInfo:self withActionString:@"DELETE"];

}

- (void)inviteAction:(UIButton *)sender
{
    [self.delegate setWithCellInfo:self withActionString:@"INVITE"];
}

- (void)leaveAction:(UIButton *)sender
{
    [self.delegate setWithCellInfo:self withActionString:@"LEAVE"];
}

- (void)joinAction:(UIButton *)sender
{
    [self.delegate setWithCellInfo:self withActionString:@"JOIN"];
}
@end
