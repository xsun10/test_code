//
//  QuickInfoTVC.m
//  Vaiden
//
//  Created by James Chung on 9/30/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "QuickInfoTVC.h"
#import "UIImage+ImageEffects.h"
#import "QuickInfoCVCell.h"
#import "UIColor+VaidenColors.h"
#import "UIView+Manipulate.h"

@interface QuickInfoTVC ()

@property (nonatomic, strong) UIImage *backImage;
@property (weak, nonatomic) IBOutlet UIImageView *backImageView;
@property (weak, nonatomic) IBOutlet UICollectionView *slideCollectionView;
@property (weak, nonatomic) IBOutlet UIPageControl *slidePageControl;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *startButton;
@property (weak, nonatomic) IBOutlet UILabel *headlineTextLabel;
@property (nonatomic, strong) UIBarButtonItem *doneButton;

@property (nonatomic, strong) NSMutableArray *myToolBarButtons;

@end

@implementation QuickInfoTVC

- (NSMutableArray *)myToolBarButtons
{
    if (!_myToolBarButtons) _myToolBarButtons = [[NSMutableArray alloc] init];
    return  _myToolBarButtons;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];

    [self.navigationItem setHidesBackButton:YES animated:NO];

    
    self.backImageView.image = [UIImage imageNamed:@"Quick Info Background"];
   
 //   [self setBackgroundImage:1];
    self.myToolBarButtons = [self.navigationItem.rightBarButtonItems mutableCopy];
    self.slideCollectionView.delegate = self;
    self.slideCollectionView.dataSource = self;
    self.slideCollectionView.decelerationRate = UIScrollViewDecelerationRateFast;
    [self.slidePageControl setCurrentPage:0];
    self.headlineTextLabel.textColor = [UIColor peacock];
    [self.slideCollectionView reloadData];
 //   [self setNextButton];
}
/*
- (void)setNextButton
{
    UIButton *btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 50, 21);
    btn.layer.borderColor = [UIColor whiteColor].CGColor;
    btn.layer.borderWidth = 1.0;
    
    [btn addTarget:self action:@selector(nextAction:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundColor:[UIColor clearColor]];
    [btn setTitle:@"NEXT" forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-MediumItalic" size:12.0]];
    
    // Initialize UIBarbuttonitem...
    self.doneButton = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = self.doneButton;
}*/

- (IBAction)doNextAction:(id)sender
{
    [self performSegueWithIdentifier:@"Next Segue" sender:self];
}
/*
- (void)nextAction:(UIButton *)sender
{
    [self performSegueWithIdentifier:@"Next Segue" sender:self];
}
*/
- (void)setBackgroundImage:(NSUInteger)imageIndex
{
    NSString *effectText = @"";
    UIImage *effectImage = nil;
    
    effectImage = self.backImage;
    
    switch (imageIndex)
    {
        case 0:
            effectImage = self.backImage;
            break;
        case 1:
            effectImage = [self.backImage applyLightEffect];
            effectText = NSLocalizedString(@"Light", @"");
            //           self.effectLabel.textColor = [UIColor whiteColor];
            break;
        case 2:
            effectImage = [self.backImage applyExtraLightEffect];
            effectText = NSLocalizedString(@"Extra light", @"");
            //            self.effectLabel.textColor = [UIColor lightGrayColor];
            break;
        case 3:
            effectImage = [self.backImage applyDarkEffect];
            effectText = NSLocalizedString(@"Dark", @"");
            //           self.effectLabel.textColor = [UIColor darkGrayColor];
            break;
        case 4:
            effectImage = [self.backImage applyTintEffectWithColor:[UIColor blueColor]];
            effectText = NSLocalizedString(@"Color tint", @"");
            //           self.effectLabel.textColor = [UIColor darkGrayColor];
            break;
    }
    
    //    self.infoTableview.backgroundColor = [UIColor colorWithPatternImage:effectImage];
   // self.backImageView.image = effectImage;
    
    UIImageView *tempImageView = [[UIImageView alloc] initWithImage:effectImage];
    [tempImageView setFrame:self.tableView.frame];
    
    self.tableView.backgroundView = tempImageView;
    
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 5;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Info Cell";
/*
    if (indexPath.row == 5) {
        if (![self.myToolBarButtons containsObject:self.startButton]) {
            // The following line adds the object to the end of the array.
            // If you want to add the button somewhere else, use the `insertObject:atIndex:`
            // method instead of the `addObject` method.
            [self.myToolBarButtons addObject:self.startButton];
            [self.navigationItem setRightBarButtonItems:self.myToolBarButtons animated:YES];
        //    [self setToolbarItems:self.myToolBarButtons animated:YES];
        }
    } else {
        if ([self.myToolBarButtons containsObject:self.startButton]) {
            [self.myToolBarButtons removeObject:self.startButton];
       //     [self setToolbarItems:self.myToolBarButtons animated:YES];
             [self.navigationItem setRightBarButtonItems:self.myToolBarButtons animated:YES];
        }
    }*/
    
    QuickInfoCVCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    [cell.iconPicBackground makeCircleWithColorAndBackground:[self getColor:indexPath.row] andRadius:40];
    
    [self.slidePageControl setCurrentPage:indexPath.row];
    cell.messageLabel.text = [self getMessageText:indexPath.row];
    cell.titleLabel.text = [self getTitleText:indexPath.row];
    cell.iconPic.image = [self getIconPic:indexPath.row];
    [cell.iconPic setTintColor:[UIColor whiteColor]];
  
    return cell;
}


- (NSString *)getTitleText:(NSInteger)row
{
    NSString *resultText = @"";
    
    switch (row)
    {
        case 0: resultText = @"Tools For Athletes";
            break;
        case 1: resultText = @"Sports Through Photos";
            
            break;
        case 2: resultText = @"Compete";
            break;
        case 3: resultText = @"Check-In";
            
            break;
        case 4: resultText = @"A Tech Athletic Brand";
            
            break;
    
    }
    return resultText;
}

- (UIColor *)getColor:(NSInteger)row
{
    UIColor *resultColor = nil;
    
    switch (row)
    {
        case 0: resultColor = [UIColor peacock];
            break;
        case 1: resultColor = [UIColor sienna];
            
            break;
        case 2: resultColor = [UIColor burgundyColor];
            break;
        case 3: resultColor = [UIColor skyBlue];
            
            break;
        case 4: resultColor = [UIColor blackColor];
            
            break;
            
    }
    return resultColor;
}
- (NSString *)getMessageText:(NSInteger)row
{
    NSString *resultText = @"";
    
    switch (row)
    {
        case 0: resultText = @"With the Vaiden app, you can access a plethora of tools to enhance the way you play sports.";
            break;
        case 1: resultText = @"Upload your sports photos and tag them with our categories. We'll create a timeline of your photos organized into sports seasons and years.  It's a new way of remembering your sports history.";
           
            break;
        case 2: resultText = @"Find people to play sports with in your area.  Challenge them to play 1 vs 1 directly or invite them to play in pickup matches.";
                        break;
        case 3: resultText = @"Let your friends know when and where you will be playing sports.  We've automated the process through a check-in system.  It's simple to use.";
            
            break;
        case 4: resultText = @"We're creating a new type of sports brand by combining the best elements of tech with sports.";
            
            break;
       }
    return resultText;
}

- (UIImage *)getIconPic:(NSInteger)row
{
    UIImage *i = nil;
    switch (row) {
        case 0:
            i = [[UIImage imageNamed:@"Tool Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            break;
        case 1:
            i = [[UIImage imageNamed:@"Photo Icon Large"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            break;
        case 2:
            i = [[UIImage imageNamed:@"Sport Icon Large"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            break;
        case 3:
            i = [[UIImage imageNamed:@"Location Large Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            break;
        case 4:
            i = [[UIImage imageNamed:@"Vaiden Logo White"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            break;
             default:
            break;
    }
    return  i;
}
/*
- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath{
    QuickInfoCVCell* currentCell = ([[collectionView visibleCells]count] > 0) ? [collectionView visibleCells][0] : nil;
    
    if(cell != nil){
        
        NSInteger rowIndex = [collectionView indexPathForCell:currentCell].row;
        
        UIImage * toImage = (self.slides)[rowIndex];
        [UIView transitionWithView:self.view
                          duration:0.5
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{
                            self.backgroundImage.image = toImage;
                        } completion:nil];
        
        [self.slidePageControl setCurrentPage:rowIndex];
        
    }
}*/

@end
