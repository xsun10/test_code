//
//  ChangeExperienceTVC.h
//  Vaiden
//
//  Created by Turbo on 7/28/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@protocol ExperienceDelegate <NSObject>
-(void)reloadTable:(NSString *)type;
@end

@interface ChangeExperienceTVC : UITableViewController <MBProgressHUDDelegate>
{
    MBProgressHUD *HUD;
}

@property (nonatomic, weak) id <ExperienceDelegate> delegate;
@property (assign) NSString *type;

@property (assign) BOOL isChange;
@property (assign) NSInteger changeID;
@property (assign) NSString *name;
@property (assign) NSString *time;
@property (assign) NSString *sportname;
@end
