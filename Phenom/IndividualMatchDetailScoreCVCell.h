//
//  IndividualMatchDetailScoreCVCell.h
//  Vaiden
//
//  Created by James Chung on 12/13/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IndividualMatchDetailScoreCVCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@end
