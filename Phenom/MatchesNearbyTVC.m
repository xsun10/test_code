//
//  MatchesNearbyTVC.m
//  Phenom
//
//  Created by James Chung on 5/22/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "MatchesNearbyTVC.h"
#import "UserPreferences.h"
#import "IndividualMatch.h"
#import "PickupMatch.h"
#import "IOSRequest.h"
#import "MatchesNearbyCell.h"
#import "AFNetworking.h"
#import "PickupMatchDetailTVC.h"
#import "UIColor+VaidenColors.h"
#import "UIView+Manipulate.h"
#import "NSDate+Utilities.h"

@interface MatchesNearbyTVC ()
@property (nonatomic, strong) UserPreferences *preferences;
@property (nonatomic, strong) NSMutableArray *matches;
//@property (readwrite, nonatomic) BOOL noResultsToDisplay;
@property (nonatomic) NSInteger proximityRange;
@property (nonatomic, strong) UIView *tempView;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
@property (nonatomic) NSUInteger selectedSection;
@property (nonatomic, strong) UIView *overlay;

@end

@implementation MatchesNearbyTVC

#define  MATCH_RANGE 50

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (NSMutableArray *)matches
{
    if (!_matches) _matches = [[NSMutableArray alloc] init];
    return _matches;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
    
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Location Services Are Off";
        alert.message = @"Please turn on Location Services in your Phone Settings to use this feature";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
  //      self.noResultsToDisplay = YES;
    } else {
        [self showTempSpinner];
  //      self.noResultsToDisplay = YES;
        self.selectedSection = 0;
        
        [self refresh];

    }
    
    
   
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.overlay removeFromSuperview];
    self.overlay = nil;
}

- (void)displayErrorOverlay
{
    self.overlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, [UIScreen mainScreen].bounds.size.height)];
    self.overlay.backgroundColor = [UIColor colorWithRed:239/255.0 green:239/255.0 blue:244/255.0 alpha:1.0];
    
    UIImageView *icon = [[UIImageView alloc] initWithFrame:CGRectMake(140, 100, 50, 50)];
    UIImage *image = [UIImage imageNamed:@"no_match_icon"];
    [icon setImage:image];
    [icon setContentMode:UIViewContentModeCenter];
    [self.overlay addSubview:icon];
    
    
    /*UIImageView *eclipse = [[UIImageView alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/2 - 55,
                                                                         [UIScreen mainScreen].bounds.size.width/2-40,
                                                                         95,
                                                                         30)];
    UIImage *eclipse_img = [UIImage imageNamed:@"eclipse"];
    [eclipse setImage:eclipse_img];
    [eclipse setContentMode:UIViewContentModeCenter];
    [self.overlay addSubview:eclipse];
    [self.overlay sendSubviewToBack:eclipse];*/
    
    UILabel *firstLine = [[UILabel alloc] initWithFrame:CGRectMake(30, 180, 260, 21)];
    firstLine.textColor = [UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:1.0];
    firstLine.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:15];
    firstLine.text = @"No Nearby Pick-Up Matches";
    [firstLine setTextAlignment:NSTextAlignmentCenter];
    
    
    /*UILabel *secondLine = [[UILabel alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/2 - 130,
                                                                    [UIScreen mainScreen].bounds.size.width/2 + 7,
                                                                    250,
                                                                    80)];
    secondLine.textColor = [UIColor colorWithRed:170/255.0 green:170/255.0 blue:170/255.0 alpha:1.0];
    secondLine.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:13];
    secondLine.text = @"Create a pick-up match by clicking ‘Create’ above.";
    secondLine.numberOfLines = 2;
    [secondLine setTextAlignment:NSTextAlignmentCenter];*/
    
    UIButton *startButton = [[UIButton alloc] initWithFrame:CGRectMake(55, 215, 210, 40)];
    [startButton setTitle:@"Create A Pickup Match" forState:UIControlStateNormal];
    [startButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    startButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:15];
    [startButton setBackgroundColor:[UIColor colorWithRed:170/255.0 green:170/255.0 blue:170/255.0 alpha:1.0]];
    [startButton addTarget:self action:@selector(makeNewPickupMatch) forControlEvents:UIControlEventTouchUpInside];
    [startButton makeRoundedBorderWithRadius:3];
    
    
    //[startButton.layer setBorderWidth:1.0];
    //[startButton.layer setBorderColor:[[UIColor peacock] CGColor]];
    //   [startButton makeRoundedBorderWithColor:[UIColor peacock]];
    
    [self.overlay addSubview:firstLine];
    //[self.overlay addSubview:secondLine];
    [self.overlay addSubview:startButton];
    
    [self.tableView addSubview:self.overlay];

}

- (void)makeNewPickupMatch
{
    [self performSegueWithIdentifier:@"New Pickup Match Segue" sender:self];
}
- (void)showTempSpinner
{
    self.tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    self.tempView.backgroundColor = [UIColor lightGrayColor];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] init];
    [self.activityIndicator setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width/2.0, [UIScreen mainScreen].bounds.size.height/2.0 - 50)];
    [self.activityIndicator startAnimating];
    [self.tempView addSubview:self.activityIndicator];
    
    [self.view addSubview:self.tempView];

}

- (void)refresh
{
    
    [self initLocation];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
//    if (self.noResultsToDisplay)
//        return 1;
    
    return [self.matches count];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"pickup matches nearby cell";
    MatchesNearbyCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
  
 /*
    if (self.noResultsToDisplay || [self.matches count] == 0) {
        cell.matchHeading.hidden = YES;
        cell.sportLabel.hidden = YES;
        cell.sportIcon.hidden = YES;
        cell.forwardIcon.hidden = YES;
        cell.experienceLabel.hidden = YES;
        cell.playerCollectionView.hidden = YES;
        cell.venueLabel.hidden = YES;
        cell.messageLabel.hidden = YES;
        cell.distanceLabel.hidden = YES;
        cell.genderLabel.hidden = YES;
        cell.barBackground.hidden = YES;
        cell.headlineBackground.hidden = YES;
        cell.textLabel.text = @"No Open Matches Nearby";
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.textLabel.textColor = [UIColor lightGrayColor];
        cell.textLabel.hidden = NO;
        
        return cell;
        
    } else {
        cell.matchHeading.hidden = NO;
        cell.headlineBackground.hidden = NO;
        cell.sportLabel.hidden = NO;
        cell.sportIcon.hidden = NO;
        cell.forwardIcon.hidden = NO;
        cell.experienceLabel.hidden = NO;
        cell.playerCollectionView.hidden = NO;
        cell.venueLabel.hidden = NO;
        cell.messageLabel.hidden = NO;
        cell.distanceLabel.hidden = NO;
        cell.genderLabel.hidden = NO;
        cell.textLabel.hidden = YES;
        cell.barBackground.hidden = NO;
    
    }
    */
   
    
    PickupMatch *pMatch = (self.matches)[indexPath.section];
    
    cell.matchNameLabel.text = pMatch.matchName;
    NSLog(@"the match name is: %@", pMatch.matchName);
    cell.sportLabelTop.text = [NSString stringWithFormat:@"%@ PICKUP MATCH", [pMatch.sport.sportName uppercaseString]];
    
    cell.sportIcon.image = [[pMatch.sport getSportIcon2] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [cell.sportIcon setTintColor:[UIColor darkGrayColor2]];
    [cell.sportIconBackground makeRoundedBorderWithRadius:10];
    
    cell.sportIconDetail.image = [[pMatch.sport getSportIcon] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [cell.sportIconDetail setTintColor:[UIColor lightGrayColor]];
    
    cell.sportLabel.text = [pMatch.sport.sportName capitalizedString];
    
    cell.locationIcon.image = [[UIImage imageNamed:@"Location Icon 2"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [cell.locationIcon setTintColor:[UIColor darkGrayColor]];

    
 //   cell.sportLabelTop.text = [pMatch.sport.sportName capitalizedString];
    
    cell.minCompetitors = pMatch.minCompetitors;
    cell.competitors = pMatch.competitorArray;
//    cell.experienceLabel.text = [NSString stringWithFormat:@"%@ Experience", [[PickupMatch getExperienceString:pMatch] capitalizedString]];
  
 /*   if ([pMatch.experienceArray count] > 0)
        cell.experienceLabel.text = [NSString stringWithFormat:@"%@ Experience", [[pMatch.experienceArray componentsJoinedByString:@", "] capitalizedString]];
    else
        cell.experienceLabel.text = @"Any Experience";
   */
    
    [cell.playerCollectionView reloadData];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMM d"];
    NSString *dateString = [dateFormatter stringFromDate:pMatch.dateTime];
   
    cell.dateLabel.text = [NSString stringWithFormat:@"%@", dateString];
    
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    [timeFormatter setDateFormat:@"h:mm a"];
    
    cell.timeLabel.text = [NSString stringWithFormat:@"%@", [timeFormatter stringFromDate:pMatch.dateTime]];
    
    
    
    cell.venueLabel.text = pMatch.venue.venueName;
    cell.messageLabel.text = [pMatch getMatchStatus];

    cell.distanceLabel.text = [NSString stringWithFormat:@"Within %.02f miles", pMatch.venue.distanceFromSessionUser];
    cell.padlockImage.hidden = !pMatch.private;
    cell.genderLabel.text = [pMatch.gender capitalizedString];
    
    cell.genderIcon.image = [[Player getIconForGender:pMatch.gender] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [cell.genderIcon setTintColor:[UIColor midGray]];
    
    cell.detailButton.tag = indexPath.section;
    [cell.detailButton addTarget:self action:@selector(detailsAction:) forControlEvents:UIControlEventTouchUpInside];
    
 //   [cell.detailButton makeCircleWithColor:[UIColor midGray2] andRadius:5.0];
    
    [cell.mainBackgroundViewCell makeRoundedBorderWithRadius:3];
    cell.mainBackgroundViewCell.layer.borderColor = [UIColor midGray2].CGColor;
    cell.mainBackgroundViewCell.layer.borderWidth = 1.0;
    
    // Configure the cell...
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

}
- (IBAction)detailsAction:(UIButton *)sender
{
    //   if (!self.noResultsToDisplay) {
    if ([self.matches count] > 0) {
        self.selectedSection = sender.tag;
        [self performSegueWithIdentifier:@"Pickup Match Detail" sender:self];
    }
}

- (void)initLocation
{
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
    self.proximityRange = 40;
    [locationManager startUpdatingLocation];
    
    
}

- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations {
    CLLocation *location = locations[0];
    NSLog(@"user location: lat%f - lon%f", location.coordinate.latitude, location.coordinate.longitude);
    
    
    [IOSRequest fetchMatchesNearMe:[self.preferences getUserID]
                         withRange:MATCH_RANGE
                     userLongitude:location.coordinate.longitude
                      userLatitude:location.coordinate.latitude
                      onCompletion:^(NSMutableArray *results) {
                          [self.matches removeAllObjects];
                          for (id object in results) {
                          NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
                          [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                          NSDate *dateTime = [[formatter dateFromString:object[@"ts"]] toLocalTime];
                          
                          Sport *sport = [[Sport alloc] initWithSportDetails:object[@"sport_name"]
                                                                     andType:object[@"sport_type"]];
                              
                              // eventually have to add distances into the initializer. doing this right now for speed sake
                              Venue *venue = [[Venue alloc] initVenueWithName:object[@"venue_name"]
                                                                   andVenueID:[object[@"venue_id"] integerValue]];
                              [venue setStreetAddress:object[@"street"]];
                              [venue setCity:object[@"city"]];
                              [venue setStateName:object[@"state_abbr"]];
                              
                              venue.distanceFromSessionUser = [object[@"distance"] floatValue];
                          
                          NSMutableArray *competitorArray = [PickupMatch makeCompetitorArray:object[@"competitors"] forSportName:object[@"sport_name"] andType:object[@"sport_type"]];
                          
                          PickupMatch *iMatch = [[PickupMatch alloc]
                                                 initMatchWithDetails:[object[@"match_id"] integerValue]
                                                 andMatchName:object[@"match_name"]
                                                 isPrivate:[object[@"is_private"] boolValue]
                                                 withSport:sport
                                                 withMessage:nil
                                                 onDateTime:dateTime
                                                 atVenue:venue
                                                 createdByUser:[object[@"creator_id"] integerValue]
                                                 withUsername:[Match getUsernameForID:[object[@"creator_id"] integerValue] inArray:competitorArray]
                                                 andProfileStr:nil
                                                 minCompetitors:[object[@"min_competitors"] integerValue]
                                                 withCompetitors:competitorArray
                                                 withExperience:[NSMutableArray arrayWithArray:object[@"experience"]]
                                                 andMatchType:[[MatchType alloc] initWithMatchType:[object[@"match_type_id"] integerValue] withDescription:object[@"match_type_description"]]
                                                 andActive:YES
                                                 andMatchStatus:object[@"match_status"]];
                          
                          [iMatch setGender:object[@"gender"]];
                              
                          [self.matches addObject:iMatch];
                             
                          }
                          
                          
                          
                          dispatch_async(dispatch_get_main_queue(), ^{
                              [self.activityIndicator stopAnimating];
                              self.tempView.hidden = YES;
                              
                              if ([self.matches count] == 0) {
                                  [self displayErrorOverlay];
                           //       self.noResultsToDisplay = YES;
                              } else {
                           //       self.noResultsToDisplay = NO;
                              }
                              
                              [self.tableView reloadData];
                          });
                      }];
    
    [locationManager stopUpdatingLocation];
}

- (CGFloat)tableView:(UITableView *)t heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([(self.matches)[indexPath.section] isKindOfClass:[PickupMatch class]]) {
        return 294;
    }
    return 0;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Pickup Match Detail"]) {
   //      NSIndexPath *selectedRowIndexPath = [self.tableView indexPathForSelectedRow];
        PickupMatchDetailTVC *controller = segue.destinationViewController;
        PickupMatch *pickupMatch = (self.matches)[self.selectedSection];
        controller.matchID = pickupMatch.matchID;
    }
}
/*
- (IBAction)detailsAction:(UIButton *)sender
{
    self.selectedSection = sender.tag;

    [self performSegueWithIdentifier:@"Pickup Match Detail Segue" sender:self];
}*/


@end
