//
//  LockerPost.m
//  Vaiden
//
//  Created by James Chung on 5/12/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "LockerPost.h"

@implementation LockerPost
- (LockerPost *)initWithNewsDetails:(NSInteger)newsMakerUserID
                       andUserName:(NSString *)newsMakerUserName
                     andProfileStr:(NSString *)profileBaseString
                          withDate:(NSDate *)newsDate
                       andPostType:(NSInteger)postType
                         andNewsID:(NSInteger)newsID
                       andTextPost:(NSString *)textPost
                andImageFileString:(NSString *)imageBaseFileString
                    andNumComments:(NSUInteger)numComments
                     andNumRemixes:(NSUInteger)numRemixes
                        andLockerID:(NSInteger)lockerID

{
    self = [super initWithNewsDetails:newsMakerUserID
                          andUserName:newsMakerUserName
                        andProfileStr:profileBaseString
                             withDate:newsDate
                          andPostType:postType
                       andNumComments:numComments
                        andNumRemixes:numRemixes
                            andNewsID:newsID
                          andTextPost:textPost];
    
    if (self) {
        _lockPic = [[LockerPic alloc] init];
        _lockPic.imageFileName = imageBaseFileString;
        _lockPic.lockerID = lockerID;
        _lockPic.statsGrp = [[StatsGroup alloc] init];
     //   _lockPic.statsTagsArray = [[NSMutableArray alloc] init];
        _lockPic.trainingTagsArray = [[NSMutableArray alloc] init];
    }
    return self;
}

- (UIImage *)getNewsIcon
{
    return [UIImage imageNamed:@"Locker Post Icon"];
}

@end
