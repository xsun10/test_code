//
//  UniversityChallengeRankingsCell.h
//  Vaiden
//
//  Created by James Chung on 3/1/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UniversityChallengeRankingsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *numRankingLabel;
@property (weak, nonatomic) IBOutlet UIImageView *profilePic;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UILabel *sportLevelLabel;
@property (weak, nonatomic) IBOutlet UILabel *sportNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *recordLabel;
@property (weak, nonatomic) IBOutlet UIButton *challengeButton;

@end
