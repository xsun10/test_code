//
//  NSDate+Utilities.h
//  Vaiden
//
//  Created by James Chung on 10/11/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Utilities)

+ (NSString *)timeDiffCalc:(NSDate *)refDate;
- (BOOL)isDateOverWeekAgo;
-(NSDate *) toLocalTime;
-(NSDate *) toGlobalTime;
- (NSString *)getTodayTomorrowDateStatus;
+ (NSInteger)minDifference:(NSDate *)dateA andSecondDate:(NSDate *)dateB;
- (NSString *)getDateFullDateString;
@end
