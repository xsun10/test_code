//
//  CreateAccountViewController.m
//  Phenom
//
//  Created by James Chung on 3/19/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "CreateAccountViewController.h"
#import "IOSRequest.h"
#import "UITextField+FormValidations.h"
#import "UserPreferences.h"
#import "UIView+Manipulate.h"
#import "Constants.h"
#import "Response.h"
#import "AmazonKeyChainWrapper.h"
#import "AmazonClientManager.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "S3Tools.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "UIImage+ProportionalFill.h"


@interface CreateAccountViewController ()  <UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate>

@property (nonatomic, strong) NSDictionary *user;
@property (weak, nonatomic) IBOutlet UIButton *btn1;
@property (weak, nonatomic) IBOutlet UIButton *btn2;
@property (weak, nonatomic) IBOutlet UIButton *btn3;
@property (weak, nonatomic) IBOutlet UIButton *btn4;
@property (weak, nonatomic) IBOutlet UIButton *btn5;

@property (weak, nonatomic) IBOutlet UITextField *usernameField;
@property (weak, nonatomic) IBOutlet UITextField *emailField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (weak, nonatomic) IBOutlet UITextField *locationField;
@property (weak, nonatomic) IBOutlet UITextField *fullnameField;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingIndicator;
@property (weak, nonatomic) IBOutlet UIButton *joinButton;
@property (nonatomic, strong) UserPreferences *preferences;
@property (weak, nonatomic) IBOutlet UIView *topBk;
@property (weak, nonatomic) IBOutlet UIView *lowBk;
@property (weak, nonatomic) IBOutlet UIImageView *profilePic;
@property (weak, nonatomic) IBOutlet UILabel *addProfilePicText;
@property (weak, nonatomic) UIActionSheet *profilePicActionSheet;
@property (weak, nonatomic) UIActionSheet *stateActionSheet;
@property (strong, nonatomic)UIPopoverController *imagePickerPopover;

@property (strong, nonatomic) UIImage *profilePicOriginal;

@property (nonatomic, strong) NSData *imageData;
@property (nonatomic, strong) NSData *thumbnailImageData;
@property (nonatomic, strong) NSData *thumbnailRetinaImageData;
@property (nonatomic, strong) NSString *fileExtension;
@property (weak, nonatomic) IBOutlet UIButton *changeImageButton;
@property (strong, nonatomic)  UIBarButtonItem *doneButton;

@property (weak, nonatomic) IBOutlet UIImageView *emailIcon;
@property (weak, nonatomic) IBOutlet UIImageView *usernameIcon;
@property (weak, nonatomic) IBOutlet UIImageView *nameIcon;
@property (weak, nonatomic) IBOutlet UIImageView *locationIcon;
@property (weak, nonatomic) IBOutlet UIImageView *passwordIcon;

@property (weak, nonatomic) IBOutlet UIButton *joinBtn;
@property (weak, nonatomic) IBOutlet UIButton *dropdownButton;
@property (weak, nonatomic) IBOutlet UILabel *stateLabel;
@property (strong, nonatomic) NSMutableArray *states;
@property (strong, nonatomic) NSString *region;
@end

@implementation CreateAccountViewController

#define NORTHEAST @"Northeast"
#define SOUTHEAST @"Southeast"
#define MIDWEST @"Midwest"
#define SOUTH @"South"
#define WEST @"West"

// lazy instantiation
- (UserPreferences *) preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (NSMutableArray *) states
{
    if (!_states) _states = [[NSMutableArray alloc] init];
    return _states;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    //[self.profilePic makeCircleWithColor:[UIColor darkGrayColor] andRadius:35];
    //[self setDoneButton];
    
    /*UIColor *grayColor = [UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:1.0];
    UIColor *darkBackground = [UIColor colorWithRed:34/255.0 green:34/255.0 blue:34/255.0 alpha:1.0];
    
    self.emailIcon.image = [[UIImage imageNamed:@"email"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.emailIcon setTintColor:grayColor];
    
    self.usernameIcon.image = [[UIImage imageNamed:@"username"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.usernameIcon setTintColor:grayColor];
    
    self.nameIcon.image = [[UIImage imageNamed:@"name_icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.nameIcon setTintColor:grayColor];
    
    self.locationIcon.image = [[UIImage imageNamed:@"Location_icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.locationIcon setTintColor:grayColor];
    
    self.passwordIcon.image = [[UIImage imageNamed:@"lock_grey"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.passwordIcon setTintColor:grayColor];*/
    
    [self.usernameField setAutocorrectionType:UITextAutocorrectionTypeNo];
    [self.emailField setAutocorrectionType:UITextAutocorrectionTypeNo];
    [self.passwordField setAutocorrectionType:UITextAutocorrectionTypeNo];
    [self.locationField setAutocorrectionType:UITextAutocorrectionTypeNo];
    [self.fullnameField setAutocorrectionType:UITextAutocorrectionTypeNo];
    
    self.usernameField.delegate = self;
    self.emailField.delegate = self;
    self.passwordField.delegate = self;
    self.locationField.delegate = self;
    self.fullnameField.delegate = self;
    
    [self.joinBtn makeRoundedBorderWithRadius:3];

    self.profilePic.image = [UIImage imageNamed:@"profile_hold"];
    [self.profilePic makeRoundedBorderWithRadius:3];
    [self.profilePic setContentMode:UIViewContentModeCenter];
    
    [self getStateArray];
    self.region = NORTHEAST;
    //[self.topBk setBackgroundColor:darkBackground];
    //[self.tableView setBackgroundColor:darkBackground];
}

- (void) getStateArray {
    [self.states addObject:@"CT"];
    [self.states addObject:@"DE"];
    [self.states addObject:@"ME"];
    [self.states addObject:@"MD"];
    [self.states addObject:@"MA"];
    [self.states addObject:@"NH"];
    [self.states addObject:@"NJ"];
    [self.states addObject:@"NY"];
    [self.states addObject:@"PA"];
    [self.states addObject:@"RI"];
    [self.states addObject:@"VT"];
    [self.states addObject:@"AL"];
    [self.states addObject:@"AR"];
    [self.states addObject:@"FL"];
    [self.states addObject:@"GA"];
    [self.states addObject:@"KY"];
    [self.states addObject:@"LA"];
    [self.states addObject:@"MS"];
    [self.states addObject:@"NC"];
    [self.states addObject:@"SC"];
    [self.states addObject:@"TN"];
    [self.states addObject:@"VA"];
    [self.states addObject:@"WV"];
    [self.states addObject:@"IL"];
    [self.states addObject:@"IN"];
    [self.states addObject:@"IA"];
    [self.states addObject:@"KS"];
    [self.states addObject:@"NE"];
    [self.states addObject:@"ND"];
    [self.states addObject:@"MI"];
    [self.states addObject:@"MN"];
    [self.states addObject:@"MO"];
    [self.states addObject:@"OH"];
    [self.states addObject:@"SD"];
    [self.states addObject:@"AZ"];
    [self.states addObject:@"NM"];
    [self.states addObject:@"OK"];
    [self.states addObject:@"TX"];
    [self.states addObject:@"AK"];
    [self.states addObject:@"CA"];
    [self.states addObject:@"CO"];
    [self.states addObject:@"HI"];
    [self.states addObject:@"ID"];
    [self.states addObject:@"MT"];
    [self.states addObject:@"NV"];
    [self.states addObject:@"UT"];
    [self.states addObject:@"WA"];
    [self.states addObject:@"WY"];
}

- (void) getRegionForState:(NSString *)state
{
    if ([state isEqualToString:@"CT"] || [state isEqualToString:@"DE"] || [state isEqualToString:@"ME"] || [state isEqualToString:@"MD"] || [state isEqualToString:@"MA"] || [state isEqualToString:@"NH"] || [state isEqualToString:@"NJ"] || [state isEqualToString:@"NY"] || [state isEqualToString:@"PA"] || [state isEqualToString:@"RI"] || [state isEqualToString:@"VT"]) {
        self.region = NORTHEAST;
    } else if ([state isEqualToString:@"AL"] || [state isEqualToString:@"AR"] || [state isEqualToString:@"FL"] || [state isEqualToString:@"GA"] || [state isEqualToString:@"KY"] || [state isEqualToString:@"LA"] || [state isEqualToString:@"MS"] || [state isEqualToString:@"NC"] || [state isEqualToString:@"SC"] || [state isEqualToString:@"TN"] || [state isEqualToString:@"VA"] || [state isEqualToString:@"WV"]) {
        self.region = SOUTHEAST;
    } else if ([state isEqualToString:@"IL"] || [state isEqualToString:@"IN"] || [state isEqualToString:@"IA"] || [state isEqualToString:@"KS"] || [state isEqualToString:@"NE"] || [state isEqualToString:@"ND"] || [state isEqualToString:@"MI"] || [state isEqualToString:@"MN"] || [state isEqualToString:@"MO"] || [state isEqualToString:@"OH"] || [state isEqualToString:@"SD"]) {
        self.region = MIDWEST;
    } else if ([state isEqualToString:@"AZ"] || [state isEqualToString:@"NM"] || [state isEqualToString:@"OK"] || [state isEqualToString:@"TX"]) {
        self.region = SOUTH;
    } else if ([state isEqualToString:@"AK"] || [state isEqualToString:@"CA"] || [state isEqualToString:@"CO"] || [state isEqualToString:@"HI"] || [state isEqualToString:@"ID"] || [state isEqualToString:@"MT"] || [state isEqualToString:@"NV"] || [state isEqualToString:@"UT"] || [state isEqualToString:@"WA"] || [state isEqualToString:@"WY"]) {
        self.region = WEST;
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    self.btn1.hidden = YES;
    self.btn2.hidden = YES;
    self.btn3.hidden = YES;
    self.btn4.hidden = YES;
    self.btn5.hidden = YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if ([textField.restorationIdentifier isEqualToString:@"1"]) {
        self.btn1.hidden = NO;
        self.btn2.hidden = YES;
        self.btn3.hidden = YES;
        self.btn4.hidden = YES;
        self.btn5.hidden = YES;
    } else if ([textField.restorationIdentifier isEqualToString:@"2"]) {
        self.btn1.hidden = YES;
        self.btn2.hidden = NO;
        self.btn3.hidden = YES;
        self.btn4.hidden = YES;
        self.btn5.hidden = YES;
    } else if ([textField.restorationIdentifier isEqualToString:@"3"]) {
        self.btn1.hidden = YES;
        self.btn2.hidden = YES;
        self.btn3.hidden = NO;
        self.btn4.hidden = YES;
        self.btn5.hidden = YES;
    } else if ([textField.restorationIdentifier isEqualToString:@"4"]) {
        self.btn1.hidden = YES;
        self.btn2.hidden = YES;
        self.btn3.hidden = YES;
        self.btn4.hidden = NO;
        self.btn5.hidden = YES;
    } else {
        self.btn1.hidden = YES;
        self.btn2.hidden = YES;
        self.btn3.hidden = YES;
        self.btn4.hidden = YES;
        self.btn5.hidden = NO;
    }
}

/*- (void)setDoneButton
{
    UIButton *btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 50, 21);
    btn.layer.borderColor = [UIColor whiteColor].CGColor;
    btn.layer.borderWidth = 1.0;
    
    [btn addTarget:self action:@selector(createAccount:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundColor:[UIColor clearColor]];
    [btn setTitle:@"JOIN" forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-MediumItalic" size:12.0]];
    
    // Initialize UIBarbuttonitem...
    self.doneButton = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = self.doneButton;
}*/

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self.loadingIndicator stopAnimating];
}

-(void)createAccountWithUserName:(NSString *)userName
                     andPassword:(NSString *)password
                 andEmailAddress:(NSString *)email
                     andFullName:(NSString *)fullName
                     andLocation:(NSString *)location
                       andRegion:(NSString *)region
{
    __weak CreateAccountViewController *weakSelf = self;
    
    NSLog(@"%@ : %@ : %@", userName, password, email);
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.labelText = @"Creating Account";
    self.doneButton.enabled = NO;
    [IOSRequest createAccountWithUserName:userName
                       andPassword:password
                          andEmailAddress:email
                              andFullName:fullName
                              andLocation:location
                                andRegion:region
                           andDeviceToken:[self.preferences getDeviceToken]
                      onCompletion:^(NSDictionary *user)
    {
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            self.doneButton.enabled = YES;
            [MBProgressHUD hideHUDForView:self.view animated:YES];

            [weakSelf setUser:user];
            [weakSelf endCreateAccount];
            
        
            
            NSInteger userID = [user[@"user_id"] integerValue];
      //      NSLog(@"User ID is %ld", userID);
            
            if (userID > 0) {
                

                [self.preferences logInSession:userID
                                 andPicString:@""
                                  andUserName:userName
                              andVerifiedState:[user[@"verified"] boolValue]];
                
                [self saveProfilePic]; // should only be done if account actually created

                
          //      [weakSelf performSegueWithIdentifier:@"Add Sport Segue" sender:weakSelf];
                [weakSelf performSegueWithIdentifier:@"1st step segue" sender:self];

            } else {
                UIAlertView *alert = [[UIAlertView alloc] init];
                alert.title = @"Account Creation Error";
                
                if ([user[@"username_error"] isEqualToString:@"TRUE"]) {
                    alert.message = @"That username already exists. Please choose another";
                } else if ([user[@"email_error"] isEqualToString:@"TRUE"]) {
                    alert.message = @"That email already exists. Please enter another";
                } else {
                    alert.message = @"There was a problem creating your account";
                }
                
                
                [alert addButtonWithTitle:@"OK"];
                [alert show];
            }
            
            
        });
        
    }];
    double delayInSeconds = 10.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        //code to be executed on the main queue after delay
        self.doneButton.enabled = YES;
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    });
    
    
    
    
    
}

- (IBAction)createAccount:(id)sender
{
    
   // not the best validation system...but will improve later. Need better error messages and validations
    if (![self.usernameField checkAlphanumeric]) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Username Error";
        alert.message = @"Username is not valid.  Please enter a username between 3 and 15 letters with only alphanumeric characters.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
        
    } else if (![self.emailField checkEmail]){
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Email Error";
        alert.message = @"Email is not valid.  Please enter another one.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
    } else if (![self.passwordField checkPassword]) { // later need to have better password validation
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Password Error";
        alert.message = @"Password is not valid.  Please enter a password between 6 and 20 letters with alphanumeric or special characters.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
    } else if (![self.locationField checkLocationName]) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Location Error";
        alert.message = @"Location is not valid.  Please enter a valid city and state less than 50 characters";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
    } else if (![self.fullnameField checkCityName]) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Full Name Error";
        alert.message = @"Full Name is not valid.  Please enter a full name less than 50 characters";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
    
    } else if (self.region.length == 0 || self.stateLabel.text.length == 0) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"State Error";
        alert.message = @"State is not valid.  Please select your state.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
    } else {
        [self beginCreateAccount];
        [self createAccountWithUserName:self.usernameField.text
                            andPassword:self.passwordField.text
                        andEmailAddress:self.emailField.text
                            andFullName:self.fullnameField.text
                            andLocation:[NSString stringWithFormat:@"%@,%@", self.locationField.text, self.stateLabel.text]
                              andRegion:self.region];
    }
}

- (IBAction)cancelAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)setUser:(NSDictionary *)user
{
    if (_user != user) {
        _user = user;
    }
//    [self updateUIWithUser:user]; this would be DB call to NSUserDefaults
}

-(void)beginCreateAccount
{
    NSLog(@"Attempting to Create Account");
    [self.loadingIndicator startAnimating];
    self.joinButton.enabled = NO;
    [self.usernameField resignFirstResponder];
    [self.emailField resignFirstResponder];
    [self.passwordField resignFirstResponder];
    [self.locationField resignFirstResponder];
    
}

-(void)endCreateAccount
{
    NSLog(self.user ? @"Create Account Successful!" : @"Failed To Create Account!");
    [self.loadingIndicator stopAnimating];
    self.joinButton.enabled = YES;
}

-(void)resetUI
{
    self.usernameField.text = @"";
    self.emailField.text = @"";
    self.passwordField.text = @"";
    self.locationField.text = @"";
    [self endCreateAccount];
}
- (IBAction)clearUserNameField:(id)sender
{
    self.usernameField.text = @"";
}
- (IBAction)clearUsernameField:(id)sender {
    self.fullnameField    .text = @"";
}

- (IBAction)clearEmailField:(id)sender
{
    self.emailField.text = @"";
}

- (IBAction)clearPasswordField:(id)sender
{
    self.passwordField.text = @"";
}
- (IBAction)clearCityField:(id)sender {
    self.locationField.text = @"";
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
 /*   [self.usernameField resignFirstResponder];
    [self.emailField resignFirstResponder];
    [self.passwordField resignFirstResponder];*/
}

#define TITLE_OF_ACTIONSHEET @"Add a profile pic"
#define CANCEL_BUTTON_ACTIONSHEET @"Cancel "
#define FROM_CAMERA @"Take a profile pic"
#define FROM_LIBRARY @"Choose From Photo Library"

- (IBAction)addProfilePicAction:(id)sender
{
    if (!self.profilePicActionSheet) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                      initWithTitle:TITLE_OF_ACTIONSHEET delegate:self
                                      cancelButtonTitle:CANCEL_BUTTON_ACTIONSHEET destructiveButtonTitle:nil
                                      otherButtonTitles:FROM_CAMERA, FROM_LIBRARY, nil];
    //    [actionSheet showFromTabBar:self.tabBarController.tabBar];
        [actionSheet showInView:self.view];
        
        self.profilePicActionSheet = actionSheet;
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == actionSheet.destructiveButtonIndex) {
        [self.view.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    } else {
        NSString *choice = [actionSheet buttonTitleAtIndex:buttonIndex];
        
        if ([choice isEqualToString:FROM_CAMERA]) {
            [self changePicFromCamera];
        } else if ([choice isEqualToString:FROM_LIBRARY]){
            [self changePicFromPhotoLibrary];
        } else if (![choice isEqualToString:CANCEL_BUTTON_ACTIONSHEET]) {
            NSLog(@"%@",choice);    
            self.stateLabel.text = choice;
            [self getRegionForState:choice];
        }
    }
}

- (void)changePicFromCamera
{
    [self presentImagePicker:UIImagePickerControllerSourceTypeCamera];
}


- (void) changePicFromPhotoLibrary
{
    [self presentImagePicker:UIImagePickerControllerSourceTypeSavedPhotosAlbum];
}

- (void) presentImagePicker:(UIImagePickerControllerSourceType)sourceType
{
    if ([UIImagePickerController isSourceTypeAvailable:sourceType]) {
        NSArray *availableMediaTypes = [UIImagePickerController availableMediaTypesForSourceType:sourceType];
        if ([availableMediaTypes containsObject:(NSString *)kUTTypeImage]) {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.sourceType = sourceType;
            picker.mediaTypes = @[(NSString *)kUTTypeImage];
            picker.allowsEditing = YES;
            picker.delegate = self;
            
            
            [self presentViewController:picker animated:YES completion:nil];
        }
    }
}

- (void) imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

#define MAX_IMAGE_WIDTH 70 // might want to change this to height....

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = info[UIImagePickerControllerEditedImage];
    
    if (!image) image = info[UIImagePickerControllerOriginalImage];
    if (image) {
        self.profilePicOriginal = image;
        UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
        CGRect frame = imageView.frame;
        if (frame.size.width > MAX_IMAGE_WIDTH) {
            frame.size.height = (frame.size.height / frame.size.width) * MAX_IMAGE_WIDTH;
            frame.size.width = MAX_IMAGE_WIDTH;
        }
        imageView.frame = frame;
      
        [self.changeImageButton setImage:imageView.image forState:UIControlStateNormal];
        self.imageData = UIImageJPEGRepresentation(imageView.image, 1.0);
        self.thumbnailImageData = UIImageJPEGRepresentation([image imageCroppedToFitSize:CGSizeMake(70, 70)], 1.0);
        self.thumbnailRetinaImageData = UIImageJPEGRepresentation([image imageCroppedToFitSize:CGSizeMake(140, 140)], 1.0);
        self.fileExtension = @"jpg";
        
        self.changeImageButton.contentMode = UIViewContentModeScaleAspectFill;
        self.changeImageButton.clipsToBounds = YES;
        [self.changeImageButton makeCircleWithColor:[UIColor whiteColor] andRadius:35];

        
    }
    
    if (self.imagePickerPopover) {
        
        [self.imagePickerPopover dismissPopoverAnimated:YES];
        self.imagePickerPopover = nil;
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)saveProfilePic
{
    if (![AmazonClientManager hasCredentials]) {
        [[Constants credentialsAlert] show];
        NSLog(@"no credentials");
    }
    else {
        
        NSLog (@"has credentials");
        
        operationQueue = [NSOperationQueue new];
        operationQueue.maxConcurrentOperationCount = 3;
        
        NSString *fileNameBase = [S3Tools getFileNameBaseStringWithType:@"profile" forUser:[self.preferences getUserID]];
        S3Tools *imageUploader1 = [[S3Tools alloc] initWithData:self.imageData
                                                        forUser:[self.preferences getUserID]
                                                       dataType:@"profile"
                                          andBaseFileNameString:fileNameBase
                                                     andFileExt:self.fileExtension];
        [operationQueue addOperation:imageUploader1];
        
        
        S3Tools *imageUploader2 = [[S3Tools alloc] initWithData:self.thumbnailImageData
                                                        forUser:[self.preferences getUserID]
                                                       dataType:@"thumbnail"
                                          andBaseFileNameString:fileNameBase
                                                     andFileExt:self.fileExtension];
        [operationQueue addOperation:imageUploader2];
        
        S3Tools *imageUploader3 = [[S3Tools alloc] initWithData:self.thumbnailRetinaImageData
                                                        forUser:[self.preferences getUserID]
                                                       dataType:@"thumbnail_retina"
                                          andBaseFileNameString:fileNameBase
                                                     andFileExt:self.fileExtension];
        [operationQueue addOperation:imageUploader3];
        
     //   [self.preferences setProfilePicString:[NSString stringWithFormat:@"%@.jpg", fileNameBase]];
     //   [self.preferences setProfilePageRefreshState:YES];
    }
}

- (IBAction)dropDownClicked:(id)sender {
    [self.usernameField resignFirstResponder];
    [self.emailField resignFirstResponder];
    [self.passwordField resignFirstResponder];
    [self.locationField resignFirstResponder];
    [self.fullnameField resignFirstResponder];
    if (!self.stateActionSheet) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] init];
        [actionSheet setTitle:@"Select States"];
        actionSheet.delegate = self;
        for (int i = 0; i<[self.states count]; i++) // Add states as items
        {
            [actionSheet addButtonWithTitle:self.states[i]];
        }
        [actionSheet addButtonWithTitle:CANCEL_BUTTON_ACTIONSHEET];
        [actionSheet setCancelButtonIndex:self.states.count];
        [actionSheet showInView:self.view];
        
        self.stateActionSheet = actionSheet;
    }
}

@end
