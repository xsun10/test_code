//
//  IsAccountVerifiedVC.m
//  Vaiden
//
//  Created by James Chung on 9/15/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "IsAccountVerifiedVC.h"
#import "IOSRequest.h"
#import "UserPreferences.h"
#import "S3Tools.h"
#import "AFNetworking.h"
#import "UIImage+ProportionalFill.h"
#import "UIView+Manipulate.h"
#import "UIColor+VaidenColors.h"

@interface IsAccountVerifiedVC ()
@property (weak, nonatomic) IBOutlet UIScrollView *pageScrollView;
@property (weak, nonatomic) IBOutlet UILabel *phoneNumberLabel;
@property (weak, nonatomic) IBOutlet UIImageView *headlineIcon;
@property (weak, nonatomic) IBOutlet UILabel *headlineMessage;
@property (weak, nonatomic) IBOutlet UILabel *subMessage;
@property (weak, nonatomic) IBOutlet UIImageView *profilePic;
@property (weak, nonatomic) IBOutlet UIImageView *phoneIcon;
@property (weak, nonatomic) IBOutlet UIImageView *checkIcon;
@property (weak, nonatomic) IBOutlet UIView *checkIconBackground;

@property (nonatomic, strong) UserPreferences *preferences;

@end

@implementation IsAccountVerifiedVC

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
   
    self.pageScrollView.contentSize = CGSizeMake(320.0, 570.0);
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.phoneNumberLabel.text = [self.preferences getUserPhoneNumber];
    NSLog(@"phone%@", [self.preferences getUserPhoneNumber]);
    
    
    NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                            baseString:[self.preferences getProfilePicString]];
    
    [self.profilePic setImageWithURL:[NSURL URLWithString:url]
                    placeholderImage:[UIImage imageNamed:@"avatar round"]];
  //  [UIImage makeRoundedImage:self.profilePic withRadius:40];
    [self.profilePic makeCircleWithColor:[UIColor whiteColor] andRadius:40];

    
    self.phoneIcon.image = [[UIImage imageNamed:@"iPhone Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.phoneIcon setTintColor:[UIColor whiteColor]];
    
    self.checkIcon.image = [[UIImage imageNamed:@"Check Mark"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.checkIcon setTintColor:[UIColor whiteColor]];
    
    [self.checkIconBackground makeCircleWithColorAndBackground:[UIColor salmonColor] andRadius:15];
}


@end
