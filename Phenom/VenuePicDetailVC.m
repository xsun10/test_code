//
//  VenuePicDetailVC.m
//  Vaiden
//
//  Created by James Chung on 10/10/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "VenuePicDetailVC.h"
#import "S3Tools.h"
#import "AFNetworking.h"
#import "VDMapCell.h"
#import "ImageSegue.h"
#import "UIView+Manipulate.h"

@interface VenuePicDetailVC ()

@property (strong, nonatomic) IBOutlet UIScrollView *pageScrollView;
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;
@end

@implementation VenuePicDetailVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    /*self.pageScrollView.delegate = self;
    
	self.venuePic.contentMode = UIViewContentModeScaleAspectFit;
    self.venuePic.clipsToBounds = YES;
    [self.venuePic setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width/2, [UIScreen mainScreen].bounds.size.height/2+self.contentoffset-self.tabBarHeight - 20)];
    
    self.closeBtn.hidden = NO;
    
    NSString *url = [S3Tools getFileNameStringWithType:@"venue_pic"
                                            baseString:self.venueImage.venuePicString];
    
    [self.venuePic setImageWithURL:[NSURL URLWithString:url]
                  placeholderImage:[UIImage imageNamed:@"Landscape Placeholder"]];*/
    //NSLog(@"final frame is %f, %f, %f, %f", self.frame.origin.x, self.frame.origin.y, self.frame.size.width, self.frame.size.height);
    self.pageScrollView = [[UIScrollView alloc]initWithFrame:[[UIScreen mainScreen] bounds]];
    self.imageView = [[UIImageView alloc] init];
    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
    self.imageView.clipsToBounds = YES;
    NSString *url = [S3Tools getFileNameStringWithType:@"venue_pic"
                                            baseString:self.venueImage.venuePicString];
    
    [self.imageView setImageWithURL:[NSURL URLWithString:url]
                  placeholderImage:[UIImage imageNamed:@"Landscape Placeholder"]];
    //CGRect newframe = [self.pageScrollView convertRect:CGRectMake(0 ,0 , self.imageView.image.size.width, self.imageView.image.size.height) fromView:[[UIApplication sharedApplication].windows objectAtIndex:0]];
    [self.imageView setFrame:CGRectMake(0 ,0 , self.imageView.image.size.width, self.imageView.image.size.height)];
    //CGPoint newpoint = [scrollView convertPoint:CGPointMake([UIScreen mainScreen].bounds.size.width/2, [UIScreen mainScreen].bounds.size.height/2) fromView:[[UIApplication sharedApplication].windows objectAtIndex:0]];
    [self.imageView setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width/2, [UIScreen mainScreen].bounds.size.height/2)];
    [self.pageScrollView addSubview:self.imageView];
    self.imageView.hidden = YES;
    
    self.closeBtn.hidden = NO;
    
    self.pageScrollView.contentSize = self.imageView.image.size;
    self.pageScrollView.minimumZoomScale = 1.0;
    self.pageScrollView.maximumZoomScale = 3.0;
    self.pageScrollView.delegate = self;
    
    // Add Double Click Gesture On ImageView
    UITapGestureRecognizer *doubleTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(zoomImageForImageView)];
    doubleTapGesture.numberOfTapsRequired = 2;
    [self.imageView addGestureRecognizer:doubleTapGesture];
    
    [self.view addSubview:self.pageScrollView];
    [self.view sendSubviewToBack:self.pageScrollView];
    //[self.view bringSubviewToFront:self.imageView];
    [self.imageView setUserInteractionEnabled:YES];
    
    // Close button with border
    self.closeBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    self.closeBtn.layer.borderWidth = 1.0;
    [self.closeBtn setBackgroundColor:[UIColor colorWithRed:34/255.0 green:34/255.0 blue:34/255.0 alpha:1.0]];
    [self.closeBtn.layer setCornerRadius:3];
    
    // Add Single Click Gesture On ScrollView
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(controlCloseBtnDisplay)];
    //[self.pageScrollView addGestureRecognizer:tapGesture];
    [self.imageView addGestureRecognizer:tapGesture];
    //[self.view addGestureRecognizer:tapGesture];
    [tapGesture requireGestureRecognizerToFail:doubleTapGesture];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

/* Double click the image in imageview to zoom in the image */
- (void)zoomImageForImageView
{
    if(self.pageScrollView.zoomScale > self.pageScrollView.minimumZoomScale){
        [UIView animateWithDuration:0.3 animations:^{
            self.closeBtn.alpha = 1;
            [self.pageScrollView setZoomScale:self.pageScrollView.minimumZoomScale animated:YES];
        } completion: ^(BOOL finished) {
            self.closeBtn.hidden = !finished;
            
        }];
    } else
        [UIView animateWithDuration:0.3 animations:^{
            self.closeBtn.alpha = 0;
        } completion: ^(BOOL finished) {
            self.closeBtn.hidden = finished;
            [self.pageScrollView setZoomScale:self.pageScrollView.maximumZoomScale animated:YES];
        }];
}

/* Click the scrollView to dismiss the close button */
- (void)controlCloseBtnDisplay
{
    if (self.closeBtn.hidden == YES) { // is hidden
        [self showupButton:self.closeBtn duration:1];
    } else {
        [self dismissButton:self.closeBtn duration:1];
    }
}

/* Dismiss the button view with animation */
- (void)dismissButton:(UIButton *)button duration:(CGFloat) duration
{
    [UIView animateWithDuration:duration animations:^{
        button.alpha = 0;
    } completion: ^(BOOL finished) {
        button.hidden = finished;
    }];
}

/* Show up the button view with animation */
- (void)showupButton:(UIButton *)button duration:(CGFloat) duration
{
    if(self.pageScrollView.zoomScale > self.pageScrollView.minimumZoomScale) {
        return;
    }
    [UIView animateWithDuration:duration animations:^{
        button.alpha = 1;
    } completion: ^(BOOL finished) {
        button.hidden = !finished;
    }];
}

#pragma mark - Scroll & Zoom

- (UIView *) viewForZoomingInScrollView:(UIScrollView *)scrollView {
	return self.imageView;
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

@end
