//
//  VaidenBaseTabBarController.m
//  Vaiden
//
//  Created by James Chung on 3/6/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "VaidenBaseTabBarController.h"

@interface VaidenBaseTabBarController ()

@property (nonatomic, strong) UIButton *vButton;

@end

@implementation VaidenBaseTabBarController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.showVPage = YES;
    /* Change the background color for tab-bar */
    CGRect tabRec = self.tabBar.bounds;
    UIGraphicsBeginImageContext(tabRec.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    UIColor *color = [UIColor colorWithRed:34/255.0 green:34/255.0 blue:34/255.0 alpha:1.0];
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, tabRec);
    
    UIImage *tabBarBgImg = UIGraphicsGetImageFromCurrentImageContext();
    [[UITabBar appearance] setBackgroundImage:tabBarBgImg];
   
    /* Change the button color when it is clicked */
    
    for (UITabBarItem *item in self.tabBar.items) {
        item.imageInsets = UIEdgeInsetsMake(6,0,-6,0);
        item.title = nil;
        switch (item.tag) {
            case 1:
                [item setFinishedSelectedImage:[UIImage imageNamed:@"Feed_select"] withFinishedUnselectedImage:[UIImage imageNamed:@"Feed"]];
                break;
            case 2:
                [item setFinishedSelectedImage:[UIImage imageNamed:@"alert_select"] withFinishedUnselectedImage:[UIImage imageNamed:@"Alerts"]];
                break;
            case 3:
                [item setFinishedSelectedImage:[UIImage imageNamed:@"Profile_select"] withFinishedUnselectedImage:[UIImage imageNamed:@"Profile"]];
                break;
            case 4:
                [item setFinishedSelectedImage:[UIImage imageNamed:@"Nearby_select"] withFinishedUnselectedImage:[UIImage imageNamed:@"Nearby"]];
                break;
            case 0:
                item.imageInsets = UIEdgeInsetsMake(0,0,0,0);
                UIGraphicsBeginImageContextWithOptions(CGSizeMake(55, 55), NO, 0.0);
                UIImage *image = [UIImage imageNamed:@"Vaiden Tab Button"];
                [image drawInRect:CGRectMake(0, 0, 55, 55)];
                UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();
                [item setFinishedSelectedImage:newImage withFinishedUnselectedImage:newImage];
                break;
        }
    }
    
    if (self.showVPage)
        self.selectedIndex = 2;
    else
        self.selectedIndex = 0;
    
    [self setIndicatorImageWithRoundCorner];
    
}

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
    if (item.tag != 0) {
        [self setIndicatorImageWithRoundCorner];
    } else {
        [self clearIndicatorImageWithRoundCorner];
    }
}

- (void)clearIndicatorImageWithRoundCorner
{
    CGRect rect = CGRectMake(0, 0, 40, 40);
    UIColor *select_color = [UIColor colorWithRed:34/255.0 green:34/255.0 blue:34/255.0 alpha:1.0];
    //UIColor *select_color = [UIColor greenColor];
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [select_color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.tabBar.selectionIndicatorImage = image;
}

- (void)setIndicatorImageWithRoundCorner
{
    CGRect rect = CGRectMake(0, 0, 40, 40);
    UIColor *select_color = [UIColor colorWithRed:17/255.0 green:17/255.0 blue:17/255.0 alpha:1.0];
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [select_color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    CALayer *imageLayer = [CALayer layer];
    imageLayer.frame = rect;
    imageLayer.contents = (__bridge id)(image.CGImage);
    
    imageLayer.masksToBounds = YES;
    imageLayer.cornerRadius = 5;
    
    UIGraphicsBeginImageContext(rect.size);
    [imageLayer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *roundedImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.tabBar.selectionIndicatorImage = roundedImg;
}


@end
