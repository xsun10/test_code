//
//  TMDHeaderInfoCell.m
//  Vaiden
//
//  Created by James Chung on 4/16/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "TMDHeaderInfoCell.h"
#import "TMDHeaderInfoButtonCVCell.h"
#import "UIView+Manipulate.h"


@interface TMDHeaderInfoCell ()
@property (nonatomic) NSInteger buttonConfig;

@end

@implementation TMDHeaderInfoCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)matchButtonAction:(UIButton *)sender
{
    TMDHeaderInfoButtonCVCell *cell = (TMDHeaderInfoButtonCVCell *)[self.buttonCollectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:sender.tag inSection:0]];
    [self.delegate setWithButtonMessage:self withMessage:cell.actionName.text];
}


// configureButtonDisplay must be called first

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    switch (self.buttonConfig) {
        case BUTTON_CONFIG_EMPTY_SCORE:
            return 2;
            
            break;
            
        case BUTTON_CONFIG_SHOW_SCORE:
            return 2;
            break;
            
        case BUTTON_CONFIG_CONFIRM_BUTTONS:
            return 4;
            
            break;
            
        case BUTTON_CONFIG_RECORD_BUTTONS:
            return 4;
            
            break;
            
        case BUTTON_CONFIG_EMPTY_MATCH_OUTCOME:
            return 2;
            
            break;
            
        case BUTTON_CONFIG_CANCELLED_MATCH:
            
            return 2;
            break;
            
        case BUTTON_CONFIG_MATCH_CREATOR:
            return 3;
            break;
            
        case BUTTON_CONFIG_CONFIRMED_INVITED_CAPTAIN:
            return 3;
            break;
            
        case BUTTON_CONFIG_UNCONFIRMED_INVITED_CAPTAIN:
            return 4;
            break;
            
        case BUTTON_CONFIG_CONFIRMED_TEAM_PLAYER:
            return 3;
            break;
            
        case BUTTON_CONFIG_UNCONFIRMED_TEAM_PLAYER:
            return 4;
            break;
            
        case BUTTON_CONFIG_MINIMAL_DISPLAY:
            return 0;
            break;
        default:
            break;
    }
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"My Cell";
    
    TMDHeaderInfoButtonCVCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.matchActionButton.tag = indexPath.row;
    
    switch (self.buttonConfig) {
        case BUTTON_CONFIG_EMPTY_SCORE:
            if (indexPath.row == 0) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Message Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor whiteColor]];
                
                cell.actionName.text = @"Messages";
                
            } else if (indexPath.row == 1) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Share Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor whiteColor]];
                cell.actionName.text = @"Share";
                
            } else if (indexPath.row == 2) {

                
            } else if (indexPath.row == 3) {
                
            }
            
            break;
            
        case BUTTON_CONFIG_SHOW_SCORE:
            if (indexPath.row == 0) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Message Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor whiteColor]];
                cell.actionName.text = @"Messages";
            } else if (indexPath.row == 1) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Share Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor whiteColor]];
                cell.actionName.text = @"Share";
            } else if (indexPath.row == 2) {
               
            } else if (indexPath.row == 3) {
                
            }
            
            break;
            
        case BUTTON_CONFIG_CONFIRM_BUTTONS:
            if (indexPath.row == 0) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Confirm Score Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor whiteColor]];
                cell.actionName.text = @"Confirm Score";
                
            } else if (indexPath.row == 1) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Dispute Score Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor whiteColor]];
                cell.actionName.text = @"Dispute Score";
            } else if (indexPath.row == 2) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Message Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor whiteColor]];
                cell.actionName.text = @"Messages";
            } else if (indexPath.row == 3) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Share Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor whiteColor]];
                cell.actionName.text = @"Share";
            }
            break;
            
        case BUTTON_CONFIG_RECORD_BUTTONS:
            if (indexPath.row == 0) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Record Score Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor whiteColor]];
                cell.actionName.text = @"Record Score";
            } else if (indexPath.row == 1) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Void Match Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor whiteColor]];
                cell.actionName.text = @"Void Match";
            } else if (indexPath.row == 2) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Message Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor whiteColor]];
                cell.actionName.text = @"Messages";
            } else if (indexPath.row == 3) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Share Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor whiteColor]];
                cell.actionName.text = @"Share";
            }
            
            break;
            
        case BUTTON_CONFIG_EMPTY_MATCH_OUTCOME:
            if (indexPath.row == 0) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Message Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor whiteColor]];
                cell.actionName.text = @"Messages";
            } else if (indexPath.row == 1) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Share Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor whiteColor]];
                cell.actionName.text = @"Share";
            } else if (indexPath.row == 2) {
               
            } else if (indexPath.row == 3) {
                
            }
            
            break;
            
        case BUTTON_CONFIG_CANCELLED_MATCH:
            
            if (indexPath.row == 0) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Message Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor whiteColor]];
                cell.actionName.text = @"Messages";
            } else if (indexPath.row == 1) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Share Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor whiteColor]];
                cell.actionName.text = @"Share";
            } else if (indexPath.row == 2) {
               
            } else if (indexPath.row == 3) {
                
            }
            
            break;
            
        case BUTTON_CONFIG_MATCH_CREATOR:
            if (indexPath.row == 0) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Cancel Match Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor whiteColor]];
                cell.actionName.text = @"Cancel Match";
            } else if (indexPath.row == 1) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Message Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor whiteColor]];
                cell.actionName.text = @"Messages";
            } else if (indexPath.row == 2) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Share Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor whiteColor]];
                cell.actionName.text = @"Share";
            } else if (indexPath.row == 3) {
                
            }
            
            break;
            
        case BUTTON_CONFIG_CONFIRMED_INVITED_CAPTAIN:
            
            if (indexPath.row == 0) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Decline Match Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor whiteColor]];
                cell.actionName.text = @"Withdraw RSVP";
            } else if (indexPath.row == 1) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Message Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor whiteColor]];
                cell.actionName.text = @"Messages";
            } else if (indexPath.row == 2) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Share Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor whiteColor]];
                cell.actionName.text = @"Share";
            } else if (indexPath.row == 3) {
                
            }
            break;
            
        case BUTTON_CONFIG_UNCONFIRMED_INVITED_CAPTAIN:
            
            if (indexPath.row == 0) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Join Match Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor whiteColor]];
                cell.actionName.text = @"Join Match";
            } else if (indexPath.row == 1) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Decline Match Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor whiteColor]];
                cell.actionName.text = @"Decline Match";
            } else if (indexPath.row == 2) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Message Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor whiteColor]];
                cell.actionName.text = @"Messages";
            } else if (indexPath.row == 3) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Share Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor whiteColor]];
                cell.actionName.text = @"Share";
            }
            
            break;
            
        case BUTTON_CONFIG_CONFIRMED_TEAM_PLAYER:  // these buttons will have to change when I implement team player level rsvp
            if (indexPath.row == 0) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Decline Match Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor whiteColor]];
                cell.actionName.text = @"Withdraw RSVP";
            } else if (indexPath.row == 1) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Message Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor whiteColor]];
                cell.actionName.text = @"Messages";
            } else if (indexPath.row == 2) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Share Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor whiteColor]];
                cell.actionName.text = @"Share";
            } else if (indexPath.row == 3) {
                
            }

            break;
            
        case BUTTON_CONFIG_UNCONFIRMED_TEAM_PLAYER:  // these buttons will have to change when I implement team player level rsvp
            if (indexPath.row == 0) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Join Match Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor whiteColor]];
                cell.actionName.text = @"Join Match";
            } else if (indexPath.row == 1) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Decline Match Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor whiteColor]];
                cell.actionName.text = @"Decline Match";
            } else if (indexPath.row == 2) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Message Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor whiteColor]];
                cell.actionName.text = @"Messages";
            } else if (indexPath.row == 3) {
                cell.iconPic.image = [[UIImage imageNamed:@"BTN Share Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.iconPic setTintColor:[UIColor whiteColor]];
                cell.actionName.text = @"Share";
            }
            
            break;
            
        case BUTTON_CONFIG_MINIMAL_DISPLAY:
            cell.iconPic = nil;
            cell.actionName.text = @"";
            
            break;
        default:
            break;
    }
    return cell;
}

#define INSET_2_BUTTONS 40
#define INSET_3_BUTTONS 30
#define INSET_4_BUTTONS 0
#define INSET_0_BUTTONS 0

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    
    
    switch (self.buttonConfig) {
        case BUTTON_CONFIG_EMPTY_SCORE:
            return UIEdgeInsetsMake(0, INSET_2_BUTTONS, 0, 0);
            
            break;
            
        case BUTTON_CONFIG_SHOW_SCORE:
            return UIEdgeInsetsMake(0, INSET_2_BUTTONS, 0, 0);
            break;
            
        case BUTTON_CONFIG_CONFIRM_BUTTONS:
            return UIEdgeInsetsMake(0, INSET_4_BUTTONS, 0, 0);
            
            break;
            
        case BUTTON_CONFIG_RECORD_BUTTONS:
            return UIEdgeInsetsMake(0, INSET_4_BUTTONS, 0, 0);
            
            break;
            
        case BUTTON_CONFIG_EMPTY_MATCH_OUTCOME:
            return UIEdgeInsetsMake(0, INSET_2_BUTTONS, 0, 0);
            
            break;
            
        case BUTTON_CONFIG_CANCELLED_MATCH:
            
            return UIEdgeInsetsMake(0, INSET_2_BUTTONS, 0, 0);
            break;
            
        case BUTTON_CONFIG_MATCH_CREATOR:
            return UIEdgeInsetsMake(0, INSET_3_BUTTONS, 0, 0);
            break;
            
        case BUTTON_CONFIG_CONFIRMED_INVITED_CAPTAIN:
            return UIEdgeInsetsMake(0, INSET_3_BUTTONS, 0, 0);
            
            break;
            
        case BUTTON_CONFIG_UNCONFIRMED_INVITED_CAPTAIN:
            return UIEdgeInsetsMake(0, INSET_4_BUTTONS, 0, 0);
            
            break;
            
        case BUTTON_CONFIG_CONFIRMED_TEAM_PLAYER:
            return UIEdgeInsetsMake(0, INSET_3_BUTTONS, 0, 0);
            break;
            
        case BUTTON_CONFIG_UNCONFIRMED_TEAM_PLAYER:
            return UIEdgeInsetsMake(0, INSET_4_BUTTONS, 0, 0);
            break;
            
        case BUTTON_CONFIG_MINIMAL_DISPLAY:
            return UIEdgeInsetsMake(0, INSET_0_BUTTONS, 0, 0);
            break;
        default:
            break;
    }
    return UIEdgeInsetsMake(0, INSET_0_BUTTONS, 0, 0);
    
}


@end
