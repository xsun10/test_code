//
//  UIButton+RoundBorder.m
//  Vaiden
//
//  Created by James Chung on 8/22/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "UIButton+RoundBorder.h"

@implementation UIButton (RoundBorder)

- (void)makeRoundBorderWithColor:(UIColor *)myColor
{
    [[self layer] setBorderWidth:1.0f];
    [[self layer] setBorderColor:myColor.CGColor];
    self.layer.cornerRadius = 5; // this value vary as per your desire
    self.clipsToBounds = YES;
    
}

@end
