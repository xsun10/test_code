//
//  PickupMatchPlayersCVCell.h
//  Phenom
//
//  Created by James Chung on 5/21/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PickupMatchPlayersCVCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *profilePic;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UILabel *levelLabel;
@property (weak, nonatomic) IBOutlet UILabel *matchCreatorLabel;

@end
