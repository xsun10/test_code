//
//  MatchPlayerNews.h
//  Vaiden
//
//  Created by James Chung on 10/18/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "MatchNews.h"

@interface MatchPlayerNews : MatchNews

@property (nonatomic, strong) Player *player;
@property (nonatomic, strong) NSString *playerMessage;

- (MatchPlayerNews *)initWithDetails:(Match *)matchInfo
                            postDate:(NSDate *)newsDate
                     headLineMessage:(NSString *)headlineMessage
                      generalMessage:(NSString *)generalMessage
                           forPlayer:(Player *)player
                         withMessage:(NSString *)playerMessage;

@end
