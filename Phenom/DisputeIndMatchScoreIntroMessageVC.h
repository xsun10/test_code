//
//  DisputeIndMatchScoreIntroMessageVC.h
//  Phenom
//
//  Created by James Chung on 8/1/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IndividualMatch.h"
#import "CustomBaseVC.h"

@interface DisputeIndMatchScoreIntroMessageVC : CustomBaseVC

@property (nonatomic, strong) IndividualMatch *individualMatch;
@end
