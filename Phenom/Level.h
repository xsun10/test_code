//
//  Level.h
//  Phenom
//
//  Created by James Chung on 5/20/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Level : NSObject

@property (nonatomic) NSInteger levelID;
@property (nonatomic, strong) NSString *level;
@property (nonatomic) float value;

- (Level *) initWithLevelDetails:(NSInteger)levelID
                        andLevel:(NSString *)level
                        andValue:(float)value;

@end
