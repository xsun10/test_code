//
//  Score.m
//  Phenom
//
//  Created by James Chung on 7/23/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "RoundScore.h"

@implementation RoundScore

-(RoundScore*)initWithScore:(NSInteger)numRound

{
    self = [super init];
    
    if (self) {
        _numRound = numRound;
        
    }
    
    return self;
}

@end
