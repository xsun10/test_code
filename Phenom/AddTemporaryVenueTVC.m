//
//  AddTemporaryVenueTVC.m
//  Vaiden
//
//  Created by James Chung on 12/30/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "AddTemporaryVenueTVC.h"
#import "IOSRequest.h"
#import "UITextField+FormValidations.h"
#import "UITextView+FormValidations.h"
#import "UserPreferences.h"


@interface AddTemporaryVenueTVC ()
@property (weak, nonatomic) IBOutlet UITextField *venueNameTextField;
@property (weak, nonatomic) IBOutlet UITextView *descriptionTextView;
@property (weak, nonatomic) IBOutlet UISwitch *privatePublicSwitch;
@property (nonatomic, strong) UserPreferences *preferences;
@property (nonatomic) BOOL descriptionFieldEmpty;
@property (weak, nonatomic) IBOutlet UITextField *cityTextField;
@property (nonatomic) NSInteger stateID;
@property (nonatomic) NSInteger venueID;
@property (nonatomic, strong) NSString *sportsCodedString;
@property (nonatomic, strong) UIBarButtonItem *doneButton;

@end


@implementation AddTemporaryVenueTVC


- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.descriptionTextView.text = @"Venue Description";
    self.descriptionTextView.textColor = [UIColor lightGrayColor];
    self.descriptionFieldEmpty = YES;
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
    
    //[self setDoneButton];
}

/*- (void)setDoneButton
{
    UIButton *btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 50, 21);
    btn.layer.borderColor = [UIColor whiteColor].CGColor;
    btn.layer.borderWidth = 1.0;
    
    [btn addTarget:self action:@selector(done:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundColor:[UIColor clearColor]];
    [btn setTitle:@"DONE" forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-MediumItalic" size:12.0]];
    
    // Initialize UIBarbuttonitem...
    self.doneButton = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = self.doneButton;
}*/


- (BOOL)textViewShouldBeginEditing:(UITextView*)textView {
    if (self.descriptionFieldEmpty) {
        self.descriptionTextView.text = @"";
        self.descriptionTextView.textColor = [UIColor blackColor];
        self.descriptionFieldEmpty = NO;
        [self.descriptionTextView becomeFirstResponder];
    }
    return YES;
}

// did not use this method...
- (void) textViewDidEndEditing:(UITextView*)textView {
    if(self.descriptionTextView.text.length == 0){
        self.descriptionTextView.textColor = [UIColor lightGrayColor];
        self.descriptionTextView.text = @"Description";
        self.descriptionFieldEmpty = YES;
    }
}

- (IBAction)descriptionFieldTap:(id)sender
{
    [self textViewShouldBeginEditing:self.descriptionTextView];
}

- (IBAction)clearVenueName:(id)sender
{
    self.venueNameTextField.text = @"";
    [self.venueNameTextField resignFirstResponder];
}

- (IBAction)clearCity:(id)sender
{
    self.cityTextField.text = @"";
    [self.cityTextField resignFirstResponder];
}

- (IBAction)done:(id)sender
{
    if ([self isFormOK]) {
        
        self.doneButton.enabled = NO;
        HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        HUD.labelText = @"Saving";
        
        NSString *venueDescription;
        if ([self.descriptionTextView.text isEqualToString:@"Venue Description"]) {
            venueDescription = @"";
        } else {
            venueDescription = self.descriptionTextView.text;
        }
        
        [IOSRequest saveTemporaryVenueForUser:[self.preferences getUserID]
                                    venueName:self.venueNameTextField.text
                                         city:self.cityTextField.text
                                      stateID:self.stateID
                                  description:venueDescription
                                   withSports:self.sportsCodedString
                                 onCompletion:^(NSDictionary *complete) {
                                     
                                     dispatch_async(dispatch_get_main_queue(), ^{
                                         self.doneButton.enabled = YES;
                                          [MBProgressHUD hideHUDForView:self.view animated:YES];
                                         if ([complete[@"reachedMaxError"] isEqualToString:@"TRUE"]) {
                                             UIAlertView *alert = [[UIAlertView alloc] init];
                                             alert.title = @"Venue Creation Error";
                                             alert.message = @"You have reached your max venues.  Please delete a venue before creating another.";
                                             [alert addButtonWithTitle:@"OK"];
                                             [alert show];
                                             
                                         } else if ([complete[@"outcome"] isEqualToString:@"failure"]) {
                                             
                                             UIAlertView *alert = [[UIAlertView alloc] init];
                                             alert.title = @"Venue Creation Error";
                                             alert.message = @"User not located.";
                                             [alert addButtonWithTitle:@"OK"];
                                             [alert show];
                                         } else {
                                             [self.preferences setVenuesListPageRefreshState:YES];
                                             self.venueID = [complete[@"venue_id"] integerValue];
                                             
                                             [self.delegate setViewController:self withVenueID:self.venueID andVenueName:self.venueNameTextField.text];
                                             
                                             NSInteger currentIndex = [self.navigationController.viewControllers indexOfObject:self];
                                            
                                              [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:currentIndex-self.numVCsToPop] animated:YES];
                                             
                                             
                                             

                        //                     [self.delegate setMyVenueViewController:self];
                                          //   [self performSegueWithIdentifier:@"Set Venue Pics Segue" sender:self];
                                         }
                                         
                                     });

                                 }];
        double delayInSeconds = 10.0;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            //code to be executed on the main queue after delay
            self.doneButton.enabled = YES;
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        });
         
         
         
        
    }
    
}


- (BOOL)isFormOK
{
    if ([self.venueNameTextField.text length] == 0) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Venue Name Needed";
        alert.message = @"Please enter the official name of the venue.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
        return NO;
    }
    
    if (![self.venueNameTextField checkLocationName]) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Venue Name Not Valid";
        alert.message = @"The venue name you entered does not fit our formats.  Please retry.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
        return NO;
    }
    
    if ([self.cityTextField.text length] == 0) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"City Name Needed";
        alert.message = @"Please enter the city where the venue is located.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
        return NO;
    }
    
    if (![self.cityTextField checkCityName]) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"City Name Not Valid";
        alert.message = @"The city name you entered does not fit our formats.  Please retry.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
        return NO;
    }
    
    UITableViewCell *stateCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]];
    if ([stateCell.detailTextLabel.text isEqualToString:@"Detail"]) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"State Needed";
        alert.message = @"Please enter the state where the venue is located.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
        return NO;
    }
    
    UITableViewCell *sportsCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
    if ([sportsCell.detailTextLabel.text isEqualToString:@"Detail"]) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Sports Needed";
        alert.message = @"Please enter the sports that can be played at this venue.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
        return NO;
    }
    
    if (![self.descriptionTextView checkVenueDescription]) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Description Not Valid";
        alert.message = @"The venue description you entered does not fit our formats.  Please retry.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
        return NO;
    }
    return YES
    ;
}

#pragma mark - Table view delegate


// delegate method for choose states

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Venue To States"]) {
        ChooseStatesCDTVC *controller = (ChooseStatesCDTVC *)segue.destinationViewController;
        controller.delegate = self;
    } else if ([segue.identifier isEqualToString:@"Choose Sports Segue"]) {
        AddVenueSportsTVC *controller = (AddVenueSportsTVC *)segue.destinationViewController;
        controller.delegate = self;

    }
}

- (void)setMyViewController:(ChooseStatesCDTVC *)controller withStateName:(NSString *)stateName withStateID:(NSInteger)stateID
{
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
    cell.detailTextLabel.text = stateName;
    self.stateID = stateID;
    
}

- (void)setViewController:(AddVenueSportsTVC *)controller withSports:(NSMutableArray *)sports
{
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
    
    if ([sports count] > 1)
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%d sports", [sports count]];
    else if ([sports count] == 1)
        cell.detailTextLabel.text = [[sports objectAtIndex:0] sportName];
    else
        cell.detailTextLabel.text = @"";
    
    
    self.sportsCodedString = @"";
    
    for (int i = 0; i < [sports count]; i++) {
        if (i == 0)
            self.sportsCodedString = [[sports objectAtIndex:i] sportName];
        else
            self.sportsCodedString = [NSString stringWithFormat:@"%@_%@", self.sportsCodedString, [[sports objectAtIndex:i] sportName]];
    }
}


@end
