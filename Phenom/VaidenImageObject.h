//
//  VaidenImageObject.h
//  Vaiden
//
//  Created by James Chung on 5/7/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VaidenImageObject : NSObject

@property (nonatomic, strong) NSData *origImageData;
@property (nonatomic, strong) NSData *regularSizeImageData;
@property (nonatomic, strong) NSData *regularSizeRetinaImageData;
@property (nonatomic, strong) NSData *thumbnailImageData;
@property (nonatomic, strong) NSData *thumbnailRetinaImageData;
@property (nonatomic, strong) NSString *fileExtension;
@property (nonatomic, strong) UIImage *imageHandle;

- (VaidenImageObject *) initWithOrigImageData:(NSData *)origImageData
                      andRegularSizeImageData:(NSData *)regularSizeImageData
                    andRegularSizeRetinaImageData:(NSData *)regularSizeRetinaImageData
                             andThumbnailData:(NSData *)thumbnailData
                       andRetinaThumbnailData:(NSData *)retinaThumbnailData
                             andFileExtension:(NSString *)fileExtension
                                     andImage:(UIImage *)imageHandle;
@end
