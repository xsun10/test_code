//
//  UIView+Manipulate.h
//  Vaiden
//
//  Created by James Chung on 8/27/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Manipulate)


- (void)makeRoundedBorderWithColor:(UIColor *)myColor;
- (void)makeCircleWithColor:(UIColor *)myColor andRadius:(float)myRadius;
- (void)makeCircleWithColorAndBackground:(UIColor *)myColor andRadius:(float)myRadius;
- (void)makeRoundedBorderwithColorAndDropshadow:(UIColor *)myColor;
- (void)makeEdgyDropshadow:(UIColor *)myColor;
- (void)makeRoundedBorderWithRadius:(float)myRadius;
@end
