//
//  CommentsTVC.h
//  Phenom
//
//  Created by James Chung on 5/6/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomBaseTVC.h"

@class CommentsTVC;

@protocol CommentsTVC_Delegate <NSObject>
-(void)setWithNumCommentsFromVC:(CommentsTVC *)controller withNumComments:(NSUInteger)numComments forSection:(NSUInteger)currentTableSection;
@end


@interface CommentsTVC : CustomBaseTVC <UITextFieldDelegate>


@property (nonatomic) NSInteger newsID;
@property (nonatomic, weak) id <CommentsTVC_Delegate>delegate;
@property (nonatomic) NSUInteger currentTableSection;
@property (nonatomic) BOOL showKeyboardOnLoad;

@end

