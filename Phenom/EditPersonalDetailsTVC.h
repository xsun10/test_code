//
//  EditPersonalDetailsTVC.h
//  Phenom
//
//  Created by James Chung on 6/5/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "CustomBaseTVC.h"

@interface EditPersonalDetailsTVC : CustomBaseTVC <MBProgressHUDDelegate, UITextViewDelegate> {
    MBProgressHUD *HUD;
    NSOperationQueue         *operationQueue;
}


@end
