//
//  ShowAlertsTagCell.m
//  Vaiden
//
//  Created by James Chung on 5/16/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "ShowAlertsTagCell.h"
#import "ShowAlertsTagCVCell.h"
#import "S3Tools2.h"
#import "AFNetworking.h"

@implementation ShowAlertsTagCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)myInit
{
    self.imageCollectionView.delegate = self;
    self.imageCollectionView.dataSource = self;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.pa.tagDataArray count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ShowAlertsTagCVCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Tag Image Cell" forIndexPath:indexPath];
    
    LockerPic *lp  = [self.pa.tagDataArray objectAtIndex:indexPath.row];
    
    NSString *urlPostPic = [S3Tools2 getFileNameStringWithType:@"photo_regsize" baseString:[NSString stringWithFormat:@"%ld/photo/%@", self.pa.refPlayer.userID, lp.imageFileName]];
    
    cell.tagImage.contentMode = UIViewContentModeScaleAspectFill;
    cell.tagImage.clipsToBounds = YES;
    
    [cell.tagImage setImageWithURL:[NSURL URLWithString:urlPostPic] placeholderImage:[UIImage imageNamed:@"Landscape Placeholder"]];
    
    
//    UIButton *picButton1 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
//    [picButton1 addTarget:self action:@selector(goToLockerPost:) forControlEvents:UIControlEventTouchUpInside];
//    picButton1.tag = indexPath.row;
//    [cell.tagImage addSubview:picButton1];
  
    cell.picButton.tag = lp.lockerID;
    cell.tagImage.userInteractionEnabled = YES;
    
    
    return cell;
}

- (void)goToLockerPost:(UIButton *)sender
{
    LockerPic *lp = [self.pa.tagDataArray objectAtIndex:sender.tag];
    
    [self.delegate setWithLockerIDFrom:self withLockerPic:lp];
}

@end
