//
//  S3Tools.m
//  Phenom
//
//  Created by James Chung on 5/24/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "S3Tools.h"
#import "AmazonClientManager.h"
#import "IOSRequest.h"
#import <ImageIO/ImageIO.h>
#import "UserPreferences.h"

@interface S3Tools ()

@property (nonatomic, strong) UserPreferences *preferences;

@end

@implementation S3Tools

//#define PICTURES_BUCKET @"vaderen_pictures"
//#define VENUES_BUCKET @"vaderen_venues"
#define PICTURES_BUCKET @"test_pictures"
#define VENUES_BUCKET @"test_venues"

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

// for profile pic uploads
-(id)initWithData:(NSData *)theData
               forUser:(NSInteger)userID
             dataType:(NSString *)dataType
 andBaseFileNameString:(NSString *)fileNameBaseString
       andFileExt:(NSString *)fileExtension
{
    self = [super init];
    if (self)
    {
        _fileData = theData ;
        _userID = userID;
        _dataType = dataType;
        _fileNameBase = fileNameBaseString;
        _fileExtension = fileExtension;
        _isNewsfeedPost = NO;
        
    }
    
    return self;
}

// for newsfeed posts (pics, text, and video uploads)
-(id)initWithData:(NSData *)theData
          forUser:(NSInteger)userID
         dataType:(NSString *)dataType
 andBaseFileNameString:(NSString *)fileNameBaseString
     andTextPostString:(NSString *)textPostString
        andFileExt:(NSString *)fileExtension
{
    self = [super init];
    if (self)
    {
        _fileData = theData ;
        _userID = userID;
        _dataType = dataType;
        _fileNameBase = fileNameBaseString;
        _textPostString = textPostString;
        _fileExtension = fileExtension;
        _isNewsfeedPost = YES;
        
    }
    
    return self;
}

// for Venue pic uploads
-(id)initWithData:(NSData *)theData
         forVenue:(NSInteger)venueID
         dataType:(NSString *)dataType
andBaseFileNameString:(NSString *)fileNameBaseString
       andFileExt:(NSString *)fileExtension
{
    self = [super init];
    if (self)
    {
        _fileData = theData ;
        _venueID = venueID;
        _dataType = dataType;
        _fileNameBase = fileNameBaseString;
        _fileExtension = fileExtension;
        _isNewsfeedPost = NO;
        
    }
    
    return self;
}


- (void)start
{
    NSString *bucketName = [self getBucketName];
    NSString *keyName = [self getModifiedFilenameFromBase];
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        
        // Upload image data.  Remember to set the content type.
        S3PutObjectRequest *por = [[S3PutObjectRequest alloc] initWithKey:keyName
                                                                  inBucket:bucketName];
        
        
                
        if ([self.dataType isEqualToString:@"profile"] || [self.dataType isEqualToString:@"photo"] || [self.dataType isEqualToString:@"thumbnail"] || [self.dataType isEqualToString:@"thumbnail_retina"]) { // only save the profile name to database.
            
            if (self.isNewsfeedPost) {
                [self saveFilenameToServer:keyName withFileType:self.dataType forUser:self.userID withText:self.textPostString];
            } else {
                [self saveFilenameToServer:keyName withFileType:self.dataType forUser:self.userID];
            }
            por.contentType = @"image/jpeg";
            
        } else if ([self.dataType isEqualToString:@"highlight_video"] ||
                   [self.dataType isEqualToString:@"trick_video"]) {
            [self saveFilenameToServer:keyName withFileType:self.dataType forUser:self.userID withText:self.textPostString];
            por.contentType = [NSString stringWithFormat:@"video/%@", self.fileExtension];
        } else if ([self.dataType isEqualToString:@"venue_thumbnail"] || [self.dataType isEqualToString:@"venue_pic"]) {
            [self saveFilenameToServer:keyName withFileType:self.dataType forVenue:self.venueID];
            por.contentType = @"image/jpeg";
        }
        
        por.data        = self.fileData;
        // Put the image data into the specified s3 bucket and object.
        [[AmazonClientManager s3] putObject:por];
        
    });
    

}

+ (NSString *)getBaseBucketURLForDataType:(NSString *)dataType
{
// should do someplace closer than west 2...
//     return @"https:/d6scf6uttw5kd.cloudfront.net/"; // default

    
    if ([dataType isEqualToString:@"profile"] ||
        [dataType isEqualToString:@"thumbnail"] ||
        [dataType isEqualToString:@"thumbnail_retina"] ||
        [dataType isEqualToString:@"photo"] ) {
        
        return [NSString stringWithFormat:@"https://s3.amazonaws.com/%@/", PICTURES_BUCKET ];
        
    } else if ([dataType isEqualToString:@"highlight_video"] ||
               [dataType isEqualToString:@"trick_video"]) {
        
        return @"http://d9ks8a70ejduv.cloudfront.net/";
    } else if ([dataType isEqualToString:@"venue_thumbnail"] ||
               [dataType isEqualToString:@"venue_pic"]) {
        return [NSString stringWithFormat:@"https://s3.amazonaws.com/%@/", VENUES_BUCKET];
    }
    return nil;
    
}


- (NSString *)getModifiedFilenameFromBase
{
    NSString *suffix = @"";
    NSString *resultString = @"";
    
    if ([self.dataType isEqualToString:@"thumbnail"]) {
        suffix = @"_thumbnail";
    } else if ([self.dataType isEqualToString:@"thumbnail_retina"]) {
        suffix = @"_thumbnail@2x";
    }
    
    if ([self.dataType isEqualToString:@"profile"] ||
        [self.dataType isEqualToString:@"thumbnail"] ||
        [self.dataType isEqualToString:@"thumbnail_retina"] ||
        [self.dataType isEqualToString:@"photo"] ) {
        
        resultString = [NSString stringWithFormat:@"%@%@.%@", self.fileNameBase, suffix, self.fileExtension];
        
    } else if ([self.dataType isEqualToString:@"highlight_video"] ||
               [self.dataType isEqualToString:@"trick_video"]) {
        resultString = [NSString stringWithFormat:@"%@.%@", self.fileNameBase, @"mp4"];
    } else if ([self.dataType isEqualToString:@"venue_thumbnail"] ||
               [self.dataType isEqualToString:@"venue_pic"]) {
        resultString = [NSString stringWithFormat:@"%@%@.%@", self.fileNameBase, suffix, self.fileExtension];
    }
    
    NSLog(@"newstring = %@", resultString);
    
    return  resultString;
}

- (void)saveFilenameToServer:(NSString *)fileName withFileType:(NSString *)fileType forUser:(NSInteger)userID
{
    [IOSRequest savePicCredentials:fileName
                           forUser:userID
                      withFileType:fileType
                      onCompletion:^(NSDictionary *results) {
                          
                          dispatch_async(dispatch_get_main_queue(), ^{
                              
                              if ([results[@"limit"] isEqualToString:@"exceeded"]) {
                                  UIAlertView *alert = [[UIAlertView alloc] init];
                                  alert.title = @"Whoa there!";
                                  alert.message = @"You've reached your posting limit for today.  Please try again tomorrow.";
                                  [alert addButtonWithTitle:@"OK"];
                              } else if ([results[@"outcome"] isEqualToString:@"failure"]) {
                                  UIAlertView *alert = [[UIAlertView alloc] init];
                                  alert.title = @"Upload Error";
                                  alert.message = @"There was an error in completing your request.  Please retry.";
                                  [alert addButtonWithTitle:@"OK"];
                                  [alert show];
                              } else {
                                  if ([fileType isEqualToString:@"profile"]) {
                                      [self.preferences setProfilePicString:fileName];
                                      [self.preferences setProfilePageRefreshState:YES];
                                  }
                              }
                          });
                          
                          
                      }];
}

// for venues

- (void)saveFilenameToServer:(NSString *)fileName withFileType:(NSString *)fileType forVenue:(NSUInteger)venueID
{
    [IOSRequest savePicCredentials:fileName
                          forVenue:venueID
                      withFileType:fileType
                      onCompletion:^(NSDictionary *results) {
                       
                          dispatch_async(dispatch_get_main_queue(), ^{
                              // these won't actually display right now...becasuse the nsnotification call will pop the vc
                              if ([results[@"limit"] isEqualToString:@"exceeded"]) {
                                  UIAlertView *alert = [[UIAlertView alloc] init];
                                  alert.title = @"Whoa there!";
                                  alert.message = @"You've reached your posting limit for today.  Please try again tomorrow.";
                                  [alert addButtonWithTitle:@"OK"];
                              } else if ([results[@"outcome"] isEqualToString:@"failure"]) {
                                  UIAlertView *alert = [[UIAlertView alloc] init];
                                  alert.title = @"Upload Error";
                                  alert.message = @"There was an error in completing your request.  Please retry.";
                                  [alert addButtonWithTitle:@"OK"];
                                  [alert show];
                              }
                          });
                        
                          [[NSNotificationCenter defaultCenter] postNotificationName:@"VenueImageDidFinishUploadingNotification"
                                                                              object:self
                                                                            userInfo:nil];

                      }];
}

// called for newsfeed posts
- (void)saveFilenameToServer:(NSString *)fileName
                withFileType:(NSString *)fileType
                     forUser:(NSInteger)userID
                    withText:(NSString *)textString
{
    [IOSRequest saveNewsfeedTextPostWithFile:fileName
                                      forUser:userID
                                 withFileType:fileType
                                     withText:textString
                                 onCompletion:^(NSDictionary *results) {
                                     dispatch_async(dispatch_get_main_queue(), ^{
                                         [[NSNotificationCenter defaultCenter] postNotificationName:@"NewsfeedImageDidFinishUploadingNotification"
                                                                                             object:self
                                                                                           userInfo:nil];
                                     });
                                    
                                 }];
    
}

- (NSString *)getBucketName
{
    if ([self.dataType isEqualToString:@"profile"] ||
        [self.dataType isEqualToString:@"thumbnail"] ||
        [self.dataType isEqualToString:@"thumbnail_retina"] ||
        [self.dataType isEqualToString:@"photo"] ) {
        
   //     return @"vaderen_pictures"; // default
        return PICTURES_BUCKET;
        
    } else if ([self.dataType isEqualToString:@"highlight_video"] ||
               [self.dataType isEqualToString:@"trick_video"]) {
        return @"vaderen_videos";
    } else if ([self.dataType isEqualToString:@"venue_thumbnail"] ||
               [self.dataType isEqualToString:@"venue_pic"]) {
     //   return @"vaderen_venues";
        return VENUES_BUCKET;
    }

    
  //  return @"vaderen_pictures"; // default
    return PICTURES_BUCKET;
}


+ (NSString *)getFileNameBaseStringWithType:(NSString *)dataType forUser:(NSInteger)userID
{
    NSTimeInterval timeStamp = [[NSDate date] timeIntervalSince1970];
    // NSTimeInterval is defined as double
    NSNumber *timeStampObj = @(timeStamp); // get timestamp
    
    int r = arc4random() % 1000000; // random number
 
    return [NSString stringWithFormat:@"%ld/%@/%ld%d", userID, dataType, [timeStampObj integerValue], r]; // default
}

+ (NSString *)getFileNameBaseStringWithType:(NSString *)dataType forVenue:(NSInteger)venueID
{
    NSTimeInterval timeStamp = [[NSDate date] timeIntervalSince1970];
    // NSTimeInterval is defined as double
    NSNumber *timeStampObj = @(timeStamp); // get timestamp
    
    int r = arc4random() % 1000000; // random number
    
    return [NSString stringWithFormat:@"%ld/%@/%ld%d", venueID, dataType, [timeStampObj integerValue], r]; // default

}

// this call method takes a base file string and appends a suffix (if necessary such as 'thumbnail')
// and also a file extension

+ (NSString *)getFileNameStringWithType:(NSString *)dataType
                             baseString:(NSString *)baseFileStringIn
                                
{
    /*
     * Will use facebook image if no image is set.
     */
    if ([baseFileStringIn rangeOfString:@"http"].location != NSNotFound) {
        return baseFileStringIn;
    }
    
    // Otherwise, must be an image saved to Amazon
    
    if ([baseFileStringIn isKindOfClass:[NSNull class]]) return @"";
    
    NSString *suffix = @"";
    NSString *fileExt = [baseFileStringIn pathExtension];
    
    // need to remove the filename extension
    NSString *baseFileString = [baseFileStringIn stringByDeletingPathExtension];
    
    if ([dataType isEqualToString:@"thumbnail"]) {
        suffix = @"_thumbnail";
    } else if ([dataType isEqualToString:@"thumbnail_retina"]) {
        suffix = @"_thumbnail@2x";
    }
    
    NSString *resultString = @"";
    
    if ([dataType isEqualToString:@"profile"] ||
        [dataType isEqualToString:@"thumbnail"] ||
        [dataType isEqualToString:@"thumbnail_retina"] ||
        [dataType isEqualToString:@"photo"] ) {
        
        resultString = [NSString stringWithFormat:@"%@%@%@.%@", [S3Tools getBaseBucketURLForDataType:dataType], baseFileString, suffix, fileExt];
        
    } else if ([dataType isEqualToString:@"highlight_video"] ||
               [dataType isEqualToString:@"trick_video"]) {
        resultString = [NSString stringWithFormat:@"%@%@%@.mp4", [S3Tools getBaseBucketURLForDataType:dataType], baseFileString, suffix];
    } else if ([dataType isEqualToString:@"venue_thumbnail"] ||
               [dataType isEqualToString:@"venue_pic"]) {
        resultString = [NSString stringWithFormat:@"%@%@%@.%@", [S3Tools getBaseBucketURLForDataType:dataType], baseFileString, suffix, fileExt];
    }
    
    return resultString;
}

@end
