//
//  AvgSportRating.m
//  Vaiden
//
//  Created by James Chung on 10/4/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "AvgSportRatingSingleAttribute.h"

@implementation AvgSportRatingSingleAttribute

-(AvgSportRatingSingleAttribute *)initWithAvg:(float)avgRating
                withNumRatings:(NSUInteger)numRatings
           forSportRatingsType:(NSString *)sportRatingsType
        withSportRatingsTypeID:(NSUInteger)sportRatingsTypeID

{
    self = [super init];
    
    if (self) {
        _avgRating = avgRating;
        _sportRatingsType = sportRatingsType;
        _sportRatingsTypeID = sportRatingsTypeID;
        _numRatings = numRatings;
        
    }
    
    return self;
}

- (UIImage *)getRatingImage
{
    UIImage *ratingImage = nil;
    NSInteger rate = (int)floor(_avgRating);
    
    switch (rate) {
        case 0:
            ratingImage = [UIImage imageNamed:@"star zero"];
            break;
            
        case 1:
            ratingImage = [UIImage imageNamed:@"star one"];
            break;
            
        case 2:
            ratingImage = [UIImage imageNamed:@"star two"];
            break;
            
        case 3:
            ratingImage = [UIImage imageNamed:@"star three"];
            break;
            
        case 4:
            ratingImage = [UIImage imageNamed:@"star four"];
            break;
            
        case 5:
            ratingImage = [UIImage imageNamed:@"star five"];
            break;
            
        default:
            ratingImage = [UIImage imageNamed:@"star zero"];
            break;
    }
    return  ratingImage;
}



@end
