//
//  RecordIndMatchScoreIntroMessage.m
//  Phenom
//
//  Created by James Chung on 7/22/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "RecordIndMatchScoreIntroMessage.h"
#import "IOSRequest.h"
#import "AFNetworking.h"
#import "UIImage+ProportionalFill.h"
#import "S3Tools.h"
#import "RecordIndMatchScoreVC.h"
#import "UIView+Manipulate.h"
#import "UIButton+RoundBorder.h"
#import "UserPreferences.h"
#import "UIImage+ImageEffects.h"
#import "UIColor+VaidenColors.h"

@interface RecordIndMatchScoreIntroMessage () <UIActionSheetDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *pageScrollView;
@property (weak, nonatomic) IBOutlet UIView *pageView;
@property (weak, nonatomic) IBOutlet UILabel *topMessage;

@property (weak, nonatomic) IBOutlet UIView *displayIconBackground;
@property (weak, nonatomic) IBOutlet UIImageView *displayIcon;
@property (weak, nonatomic) IBOutlet UIButton *recordButton;

@property (nonatomic, strong) IndividualMatch *individualMatch;
@property (nonatomic, strong) UserPreferences *preferences;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;
@property (nonatomic, strong) UIImage *backImage;
@property (weak, nonatomic) IBOutlet UIButton *neverOccurredButton;
@property (weak, nonatomic) IBOutlet UIImageView *checkImage1;
@property (weak, nonatomic) IBOutlet UIImageView *checkImage2;

@end

@implementation RecordIndMatchScoreIntroMessage

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //self.backImage = [UIImage imageNamed:@"Baseball Player Background"];
    //[self setBlurredBackgroundImage:1];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
    //[self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back_button"] style:UIBarButtonItemStyleBordered target:self.navigationController action:@selector(popViewControllerAnimated:)]];
    [self setPageInfo];
    
    //[self.pageScrollView setContentInset:UIEdgeInsetsMake(64, 0, 44, 0)];
    
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    self.pageScrollView.contentSize = CGSizeMake(320.0, 575);
    
}

- (void)setPageInfo
{
    [self.recordButton makeRoundedBorderWithRadius:3];
    [self.neverOccurredButton makeRoundedBorderWithRadius:3];
  
//    [self.displayIconBackground makeCircleWithColor:[UIColor redColor] andRadius:45];
    
/*    self.displayIcon.image = [[UIImage imageNamed:@"Record Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.displayIcon setTintColor:[UIColor whiteColor]];
    
    self.checkImage1.image = [[UIImage imageNamed:@"Check Mark"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.checkImage1 setTintColor:[UIColor greenColor]];
    
    self.checkImage2.image = [[UIImage imageNamed:@"Check Mark"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.checkImage2 setTintColor:[UIColor greenColor]];
*/
//    [self.recordButton makeCircleWithColor:[UIColor redColor] andRadius:5];
//    [self.neverOccurredButton makeCircleWithColor:[UIColor darkGrayColor] andRadius:5];
 //   self.recordButton.backgroundColor = [UIColor redColor];
 //   self.neverOccurredButton.backgroundColor = [UIColor redColor];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Record Score Segue"]) {
        RecordIndMatchScoreVC *controller = segue.destinationViewController;
        controller.matchID = self.matchID;
    }
}
/*
- (IBAction)matchNeverOccurredAction:(id)sender
{
    HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    HUD.labelText = @"Updating Match Status";
    [IOSRequest setIndividualMatchAsNeverOccurred:self.matchID
                                           byUser:[self.preferences getUserID]
                                     onCompletion:^(NSDictionary *results) {
                                         dispatch_async(dispatch_get_main_queue(), ^ {
                                             if ([results[@"outcome"] isEqualToString:@"success"]) {
                                                 HUD.labelText = @"Success!";
                                             } else {
                                                 HUD.labelText = @"Match Update Error";
                                             }
                                             [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
                                             
                                             if ([results[@"outcome"] isEqualToString:@"success"])
                                                 [self.navigationController popViewControllerAnimated:YES];
                                         });
                                    }];
}
 */

#define TITLE_OF_ACTIONSHEET @"Void the match if it never occurred. It will be removed from our records."
#define CANCEL_BUTTON_ACTIONSHEET @"Cancel "
#define VOID_MATCH @"Void Match"

- (IBAction)voidMatch:(id)sender
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:TITLE_OF_ACTIONSHEET delegate:self
                                  cancelButtonTitle:CANCEL_BUTTON_ACTIONSHEET destructiveButtonTitle:nil
                                  otherButtonTitles:VOID_MATCH,  nil];
    [actionSheet showFromTabBar:self.tabBarController.tabBar];
    
}


- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == actionSheet.destructiveButtonIndex) {
        [self.view.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    } else {
        NSString *choice = [actionSheet buttonTitleAtIndex:buttonIndex];
        
        if ([choice isEqualToString:VOID_MATCH]) {
            [self voidTheMatch];
        }
    }
}

- (void)voidTheMatch
{
    HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    HUD.labelText = @"Updating Match Status";
    
    [IOSRequest setIndividualMatchAsNeverOccurred:self.matchID
                                           byUser:[self.preferences getUserID]
                                     onCompletion:^(NSDictionary *results) {
                                         
                                         if ([results[@"outcome"] isEqualToString:@"success"]) {
                                             HUD.labelText = @"Success!";
                                         } else {
                                             HUD.labelText = @"Match Update Error";
                                         }
                                         [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
                                         
                                         if ([results[@"outcome"] isEqualToString:@"success"])
                                             [self.navigationController popViewControllerAnimated:YES];
                                         
                                     }];
}

- (void)setBlurredBackgroundImage:(NSUInteger)imageIndex
{
    NSString *effectText = @"";
    UIImage *effectImage = nil;
    
    effectImage = self.backImage;
    
    switch (imageIndex)
    {
        case 0:
            effectImage = self.backImage;
            break;
        case 1:
            effectImage = [self.backImage applyMidLightEffectLessBlur];
            effectText = NSLocalizedString(@"Light", @"");
            //           self.effectLabel.textColor = [UIColor whiteColor];
            break;
        case 2:
            effectImage = [self.backImage applyExtraLightEffect];
            effectText = NSLocalizedString(@"Extra light", @"");
            //            self.effectLabel.textColor = [UIColor lightGrayColor];
            break;
        case 3:
            effectImage = [self.backImage applyDarkEffect];
            effectText = NSLocalizedString(@"Dark", @"");
            //           self.effectLabel.textColor = [UIColor darkGrayColor];
            break;
        case 4:
            effectImage = [self.backImage applyTintEffectWithColor:[UIColor blueColor]];
            effectText = NSLocalizedString(@"Color tint", @"");
            //           self.effectLabel.textColor = [UIColor darkGrayColor];
            break;
    }
    
    self.backgroundImage.image = effectImage;
    
}
@end
