//
//  PhysicalAttributesTVC.h
//  Phenom
//
//  Created by James Chung on 6/21/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChooseProfileGenderTVC.h"
#import "MBProgressHUD.h"
#import "CustomBaseTVC.h"

@interface PhysicalAttributesTVC : CustomBaseTVC <ChooseProfileGenderTVC_Delegate, UIPickerViewDelegate, UIPickerViewDataSource, MBProgressHUDDelegate>
{
    MBProgressHUD *HUD;
}
@end
