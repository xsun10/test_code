//
//  UITextField+FormValidations.h
//  Phenom
//
//  Created by James Chung on 3/20/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (FormValidations)

- (BOOL)checkEmail;
- (BOOL)checkAlphanumeric;
- (BOOL)checkPassword;
- (BOOL)checkCityName;
- (BOOL)checkLocationName;
- (BOOL)checkMatchName;
- (BOOL)checkStreetAddress;
- (BOOL)checkAlphanumericNoLengthLimit;
- (BOOL)checkTeamName;


@end
