//
//  RatePlayerSportTVC.m
//  Vaiden
//
//  Created by James Chung on 10/4/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "RatePlayerSportTVC.h"
#import "UserPreferences.h"
#import "RatePlayerSportCell.h"
#import "IOSRequest.h"
#import "SportSkillAttribute.h"
#import "SportRating.h"

@interface RatePlayerSportTVC ()

@property (nonatomic, strong) UserPreferences *preferences;
@property (nonatomic, strong) NSMutableArray *skillTypeRatings;
@property (nonatomic) NSUInteger selectedRow;
@property (nonatomic, strong) UIView *tempView;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
@property (nonatomic, strong) UIBarButtonItem *doneButton;

@end

@implementation RatePlayerSportTVC

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (NSMutableArray *)skillTypeRatings
{
    if (!_skillTypeRatings) _skillTypeRatings = [[NSMutableArray alloc] init];
    return _skillTypeRatings;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];

    
    self.selectedRow = 0;
    [self loadSkillTypes];
    //[self setDoneButton];
}

/*- (void)setDoneButton
{
    UIButton *btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 50, 21);
    btn.layer.borderColor = [UIColor whiteColor].CGColor;
    btn.layer.borderWidth = 1.0;
    
    [btn addTarget:self action:@selector(doneAction:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundColor:[UIColor clearColor]];
    [btn setTitle:@"DONE" forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-MediumItalic" size:12.0]];
    
    // Initialize UIBarbuttonitem...
    self.doneButton = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = self.doneButton;
}*/

- (void)showTempSpinner
{
    self.tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    self.tempView.backgroundColor = [UIColor lightGrayColor];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] init];
    [self.activityIndicator setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width/2.0, [UIScreen mainScreen].bounds.size.height/2.0 - 50)];
    [self.activityIndicator startAnimating];
    [self.tempView addSubview:self.activityIndicator];
    
    [self.view addSubview:self.tempView];
    
}

- (void)loadSkillTypes
{
    [self.skillTypeRatings removeAllObjects];
    
    [IOSRequest getSkillTypesForSport:self.sportID
     withSessionUser:[self.preferences getUserID]
                        andTargetUser:self.targetUserID
                         onCompletion:^(NSMutableArray *results) {
                             
                             for (id object in results) {
                                 
                                 id prevRatingObj = object[@"session_user_rating_for_target_user"];
                                 
                                 SportSkillAttribute *ssType = [[SportSkillAttribute alloc] initWillSportSkillType:[object[@"srtype_id"] integerValue]
                                                                                andSportSkillDescription:object[@"rating_description"]];
                                 SportRating *sportRating = [[SportRating alloc] init];
                                 [sportRating setSportSkillAttribute:ssType];
                                 [sportRating setRating:[prevRatingObj[@"rating_value"] floatValue]];
                                 [sportRating setComment:prevRatingObj[@"comment"]];
                                 [sportRating setRatedPlayerID:self.targetUserID];
                                 [sportRating setRatingPlayer:[[Player alloc] initWithPlayerDetails:[self.preferences getUserID] andUserName:[self.preferences getUserName]]];
                                 
                                 [self.skillTypeRatings addObject:sportRating];
                             }
                             
                             dispatch_async(dispatch_get_main_queue(), ^{
                                 [self.tableView reloadData];
                                 [self.tableView reloadData];
                                 [self.activityIndicator stopAnimating];
                             });
                         }];
}


- (IBAction)doneAction:(id)sender
{
    [self.preferences setSportRatingsSummaryPageRefreshState:YES];
    [self.delegate setRatePlayerSportViewController:self withRefresh:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.skillTypeRatings count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Rate Cell";
    RatePlayerSportCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    SportRating *sportRating = [self.skillTypeRatings objectAtIndex:indexPath.row];
    cell.skillLabel.text = [sportRating.sportSkillAttribute.skillTypeDescription capitalizedString];
    cell.starIcon.image = [SportRating getRatingImageForRating:sportRating.rating];
    
    return cell;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Rate Attribute Segue"]) {
        SportRating *sRating = [self.skillTypeRatings objectAtIndex:self.selectedRow];
        
        RatePlayerSportActionTVC *controller = segue.destinationViewController;
        controller.delegate = self;
        controller.sRating = sRating;
 //       controller.targetUserID = self.targetUserID;
        controller.srTypeID = sRating.sportSkillAttribute.skillTypeID;
        controller.srTypeName = sRating.sportSkillAttribute.skillTypeDescription;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedRow = indexPath.row;
    [self performSegueWithIdentifier:@"Rate Attribute Segue" sender:self];
}

- (void)setRatePlayerSportActionViewController:(RatePlayerSportActionTVC *)controller
                               withSportRating:(NSUInteger)sportRating
                                    andComment:(NSString *)comment
{
    SportRating *arrayObjectSportRating = [self.skillTypeRatings objectAtIndex:self.selectedRow];
    arrayObjectSportRating.rating = sportRating;
    arrayObjectSportRating.comment = comment;
    
    [self.skillTypeRatings replaceObjectAtIndex:self.selectedRow withObject:arrayObjectSportRating];
    [self.tableView reloadData];
    
}

@end
