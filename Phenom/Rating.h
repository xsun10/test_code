//
//  Rating.h
//  Phenom
//
//  Created by James Chung on 7/4/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Rating : NSObject

@property (nonatomic) NSInteger ratingValue;
@property (nonatomic) NSInteger userID; // should actually create a diff object for these user values
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSString *profilePicString;

@property (nonatomic, strong) NSDate *ratingDate;
@property (nonatomic, strong) NSString *comment;

-(Rating *)initWithRating:(NSInteger)ratingValue
                   byUser:(NSInteger)userID
             withUserName:(NSString *)userName
      andProfilePicString:(NSString *)profilePicString
              withComment:(NSString *)comment
                   onDate:(NSDate *)ratingDate;

- (UIImage *)getRatingImage;

@end
