//
//  PhenomAppDelegate.h
//  Phenom
//
//  Created by James Chung on 3/14/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface PhenomAppDelegate : UIResponder <UIApplicationDelegate, UIAlertViewDelegate, UINavigationControllerDelegate>
{
    UIWindow *window;
    UINavigationController *navigationController;
}

//@property (strong, nonatomic) UIWindow *window;
- (void)openSession;
@property (nonatomic, retain) UITabBarController *myTabBarController;



@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet UINavigationController *navigationController;


@end


