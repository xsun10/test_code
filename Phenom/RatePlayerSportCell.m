//
//  RatePlayerSportCell.m
//  Vaiden
//
//  Created by James Chung on 10/4/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "RatePlayerSportCell.h"

@implementation RatePlayerSportCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
