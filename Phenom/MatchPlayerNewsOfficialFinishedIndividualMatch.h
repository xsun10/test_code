//
//  MatchPlayerNewsOfficialFinishedIndividualMatch.h
//  Vaiden
//
//  Created by James Chung on 11/1/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "MatchPlayerNews.h"

@interface MatchPlayerNewsOfficialFinishedIndividualMatch : MatchPlayerNews

@property (nonatomic) NSInteger coinTransferValue;

- (MatchPlayerNewsOfficialFinishedIndividualMatch *)initWithDetails:(Match *)matchInfo
                                                           postDate:(NSDate *)newsDate
                                                    headLineMessage:(NSString *)headlineMessage
                                                     generalMessage:(NSString *)generalMessage
                                                          forPlayer:(Player *)player
                                                        withMessage:(NSString *)playerMessage
                                               andCoinTransferValue:(NSInteger)coinTransferValue;
@end
