//
//  MatchUpdateInfo.m
//  Vaiden
//
//  Created by James Chung on 10/10/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "MatchUpdateInfo.h"

@implementation MatchUpdateInfo

- (MatchUpdateInfo *) initWithMatchUpdateInfo:(NSInteger)alertsID
                                withAlertText:(NSString *)alertText
                                       onDate:(NSDate *)postDate
                               postedByPlayer:(Player *)postingPlayer
                                     forMatch:(NSInteger)matchID
                                withMatchName:(NSString *)matchName
                             forMatchCategory:(NSString *)matchCategory

{
    self = [super init];
    
    if (self) {
        _alerts_id = alertsID;
        _alertText = alertText;
        _postDate = postDate;
        _postingPlayer = postingPlayer;
        _matchName = matchName;
        _matchID = matchID;
        _matchCategory = matchCategory;
    }
    
    return self;
    
}


@end
