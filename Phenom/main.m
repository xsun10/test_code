     //
//  main.m
//  Phenom
//
//  Created by James Chung on 3/14/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PhenomAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PhenomAppDelegate class]));
    }
}
