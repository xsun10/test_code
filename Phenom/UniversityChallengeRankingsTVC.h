//
//  UniversityChallengeRankingsTVC.h
//  Vaiden
//
//  Created by James Chung on 3/1/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UniversityChallengeRankingsTVC : UITableViewController

@property (nonatomic) NSInteger competitionID;
@property (nonatomic) NSInteger universityID;
@property (nonatomic, strong) NSString *universityName;


@end
