//
//  FeaturedUsersTVC.m
//  Vaiden
//
//  Created by James Chung on 9/15/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "StartByAddingFBContacts.h"
#import "UserPreferences.h"
#import "StartByAddingFBContactsCell.h"
#import "Player.h"
#import "S3Tools.h"
#import "UIImage+ProportionalFill.h"
#import "AFNetworking.h"
#import "IOSRequest.h"
#import "UIColor+VaidenColors.h"
#import "UIButton+RoundBorder.h"
#import <FacebookSDK/FacebookSDK.h>
#import "Contact.h"
#import "UIView+Manipulate.h"
#import <AddressBook/AddressBook.h> // header file for contacts in device
#import "AddressBookFriendsPickerTVC.h"
#import <Accounts/Accounts.h>
#import <Social/Social.h>
#import "PlayerDetailTVC.h"

#define FACEBOOK_TAB 0
#define TWITTER_TAB 1
#define ADDRESS_BOOK_TAB 2
#define SEARCH_TAB 3

@interface StartByAddingFBContacts () <UITableViewDataSource, UITableViewDelegate, FBFriendPickerDelegate, addressBookFriendsPickerDelegate, UISearchBarDelegate, UITextFieldDelegate>

@property (nonatomic, strong) UserPreferences *preferences;

@property (nonatomic, strong) NSMutableArray *fbContacts;
@property (nonatomic, strong) UIView *tempView;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
@property (nonatomic) BOOL noResultsToDisplay;
@property (nonatomic, strong) UIView *overlay;
@property (nonatomic, strong) UIBarButtonItem *startButton;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *superview;

@property (weak, nonatomic) IBOutlet UISwitch *inviteFriendSwitch;

@property NSInteger selectedTab;
@property (strong, nonatomic) FBFriendPickerViewController *friendPickerController;
@property (strong, nonatomic) UIView * loadingView;
@property (strong, nonatomic) UILabel * number;
@property NSInteger allFollowNumber;
@property NSInteger unfollowedUsers;
@property (strong, nonatomic) NSMutableArray *addressBookContacts;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableheight;
@property (strong, nonatomic) NSMutableArray *abFriendsArray;
@property (strong, nonatomic) NSMutableDictionary *abFriendsProfilePics;
@property (strong, nonatomic) NSMutableDictionary *abFriendsEmail;
@property (weak, nonatomic) IBOutlet UISearchBar *searchNameBar;

@property (strong, nonatomic) AddressBookFriendsPickerTVC *addressFriendPickerController;
@property (strong, nonatomic) NSMutableArray *twitterFriendsScreenNames;
@property (strong, nonatomic) NSMutableArray *twitterFriendsNames;
@property (strong, nonatomic) NSMutableDictionary *friendsList;

@property (strong, nonatomic) UIView *tableLoadingView;
@property (strong, nonatomic) NSTimer *pauseTimer;
@property (nonatomic) NSInteger selectUserRow;
@property (weak, nonatomic) IBOutlet UIButton *segueButton;
@property (weak, nonatomic) IBOutlet UIView *inviteBar;
@end

@implementation StartByAddingFBContacts

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (NSMutableArray *)fbContacts
{
    if (!_fbContacts) _fbContacts = [[NSMutableArray alloc] init];
    return  _fbContacts;
}

- (NSMutableArray *)addressBookContacts
{
    if (!_addressBookContacts) _addressBookContacts = [[NSMutableArray alloc] init];
    return  _addressBookContacts;
}

- (NSMutableArray *)twitterFriendsScreenNames
{
    if (!_twitterFriendsScreenNames) _twitterFriendsScreenNames = [[NSMutableArray alloc] init];
    return  _twitterFriendsScreenNames;
}

- (NSMutableArray *)twitterFriendsNames
{
    if (!_twitterFriendsNames) _twitterFriendsNames = [[NSMutableArray alloc] init];
    return  _twitterFriendsNames;
}

- (NSMutableDictionary *)friendsList
{
    if (!_friendsList) _friendsList = [[NSMutableDictionary alloc] init];
    return  _friendsList;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
    self.modalPresentationStyle = UIModalPresentationCurrentContext;
    self.tabBarController.modalPresentationStyle = UIModalPresentationCurrentContext;
    
    if (self.isLogin) {
        self.navigationItem.rightBarButtonItem = nil;
    }
    //[self.view bringSubviewToFront:self.inviteFriendSwitch];
    // Resize the tableview size based on the sreen size
    CGRect tableOriginSize = self.tableView.frame;
    NSLog(@"%f,%f",self.tableView.frame.size.height, self.tableView.frame.origin.y);
    CGRect screenSize = [[UIScreen mainScreen] bounds];
    NSLog(@"%f",screenSize.size.height);
    NSLog(@"%f",(screenSize.size.height - tableOriginSize.origin.y - self.navigationController.navigationBar.frame.size.height));
    if (self.isLogin) { // if it's login, there is a tab bar in the screen
        [self.tableheight setConstant:(screenSize.size.height - tableOriginSize.origin.y - self.navigationController.navigationBar.frame.size.height - self.tabBarController.tabBar.frame.size.height) - 20];
    } else { // if it's not, the tab bar will dismiss
        [self.tableheight setConstant:(screenSize.size.height - tableOriginSize.origin.y - self.navigationController.navigationBar.frame.size.height) - 20];
    }
    
    [self loadTempPage];
    [self.inviteBar sendSubviewToBack:self.segueButton];
 //   self.noResultsToDisplay = YES;
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];

    //[self synchronizeFBFriends];
    //[self setStartButton];
    
    self.selectedTab = FACEBOOK_TAB; // Initial the select tab
    
    // temp view for loading the data;
    self.loadingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    self.loadingView.backgroundColor = [UIColor midGray2];
    self.loadingView.alpha = 0.5;
    [self.loadingView setHidden:YES];
    
    UIActivityIndicatorView * tmpIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/2-5, [UIScreen mainScreen].bounds.size.height/2-5, 10, 10)];
    [tmpIndicator setColor:[UIColor blackColor]];
    [tmpIndicator startAnimating];
    
    [self.loadingView addSubview:tmpIndicator];
    [self.view addSubview:self.loadingView];
    
    // temp view for loading the data;
    self.tableLoadingView = [[UIView alloc] initWithFrame:self.tableView.frame];
    self.tableLoadingView.backgroundColor = [UIColor midGray2];
    self.tableLoadingView.alpha = 0.5;
    [self.tableLoadingView setHidden:YES];
    
    UIActivityIndicatorView * tmpIndicator2 = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/2-5, 70, 10, 10)];
    [tmpIndicator2 setColor:[UIColor blackColor]];
    [tmpIndicator2 startAnimating];
    
    [self.tableLoadingView addSubview:tmpIndicator2];
    [self.view addSubview:self.tableLoadingView];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView setBounces:NO];
    [self.tableView setAlwaysBounceVertical:NO];
    
    [self setExtraCellLineHidden:self.tableView];
    
    self.allFollowNumber = 0;
    self.unfollowedUsers = 0;
    
    // Setting up the search bar
    self.searchNameBar.delegate = self;
    for(UIView *subView in [self.searchNameBar subviews]) {
        if([subView conformsToProtocol:@protocol(UITextInputTraits)]) {
            [(UITextField *)subView setReturnKeyType: UIReturnKeyDone];
            [(UITextField *)subView setEnablesReturnKeyAutomatically:NO];
        } else {
            for(UIView *subSubView in [subView subviews]) {
                if([subSubView conformsToProtocol:@protocol(UITextInputTraits)]) {
                    [(UITextField *)subSubView setReturnKeyType: UIReturnKeyDone];
                    [(UITextField *)subSubView setEnablesReturnKeyAutomatically:NO];
                }
            }      
        }
    }
    [self.searchNameBar setAutocorrectionType:UITextAutocorrectionTypeNo]; // forbid the auto correction for search bar
    
    // Add tap gesture to dismiss the keyboard
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard)];
    [tap setCancelsTouchesInView:NO];
    [self.view addGestureRecognizer:tap];
    
    self.abFriendsArray = [[NSMutableArray alloc] init];
    self.abFriendsEmail = [[NSMutableDictionary alloc] init];
    self.abFriendsProfilePics = [[NSMutableDictionary alloc] init];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //[self.overlay removeFromSuperview];
    //self.overlay = nil;
    if (self.selectedTab == FACEBOOK_TAB) {
        [self synchronizeFBFriends];
    } else if (self.selectedTab == ADDRESS_BOOK_TAB) {
        [self synchronizeAddressBookFriends];
    } else if (self.selectedTab == TWITTER_TAB) {
        [self synchronizeTwitterFriendsType:@"followings"];
    }
}

/*
- (void)setStartButton
{
    UIButton *btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 55, 21);
    btn.layer.borderColor = [UIColor whiteColor].CGColor;
    btn.layer.borderWidth = 1.0;
    
    [btn addTarget:self action:@selector(startAction:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundColor:[UIColor clearColor]];
    [btn setTitle:@"START" forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-MediumItalic" size:12.0]];
    
    // Initialize UIBarbuttonitem...
    self.startButton = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = self.startButton;
}*/

- (IBAction)start:(id)sender
{
    [self performSegueWithIdentifier:@"Start Segue" sender:self];
}

- (void)startAction:(UIButton *)sender
{
    [self performSegueWithIdentifier:@"Start Segue" sender:self];
}

- (void) disableInteraction
{
    if (self.loadingView.hidden) {
        [self.loadingView setHidden:NO];
    }
    [self.view setUserInteractionEnabled:NO];
}

- (void)synchronizeFBFriends
{
    [self disableInteraction];
    // FBSample logic
    // if the session is open, then load the data for our view controller
    if (!FBSession.activeSession.isOpen) {
        // if the session is closed, then we open it here, and establish a handler for state changes
        [FBSession openActiveSessionWithReadPermissions:nil
                                           allowLoginUI:YES
                                      completionHandler:^(FBSession *session,
                                                          FBSessionState state,
                                                          NSError *error) {
                                          if (error) {
                                              UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                                                  message:error.localizedDescription
                                                                                                 delegate:nil
                                                                                        cancelButtonTitle:@"OK"
                                                                                        otherButtonTitles:nil];
                                              [alertView show];
                                          } else {
                                              [self synchronizeFBFriends];
                                          }
                                      }
         ];
        return;
    }
    
    NSMutableDictionary *fbData = [[NSMutableDictionary alloc] init];
    NSNumber *sessionID = [[NSNumber alloc] initWithInteger:[self.preferences getUserID]];
    
    [fbData setValue:sessionID forKey:@"session_user_id"];
    
    NSMutableArray *fbFriendsArray = [[NSMutableArray alloc] init];
    
    FBRequest* friendsRequest = [FBRequest requestForMyFriends];
    [friendsRequest startWithCompletionHandler: ^(FBRequestConnection *connection,
                                                  NSDictionary* result,
                                                  NSError *error) {
        NSArray* friends = [result objectForKey:@"data"];
        NSLog(@"Found: %d FB friends", friends.count);
        for (NSDictionary<FBGraphUser>* friend in friends) {
            [fbFriendsArray addObject:friend.id];
        }
        [fbData setObject:fbFriendsArray forKey:@"friend_ids"];
        
        [self checkAgainstVaidenDB:fbData];
    }];
}

- (void)displayErrorOverlay
{
    [self.overlay setHidden:YES];
    self.overlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, self.tableView.bounds.size.height)];
    self.overlay.backgroundColor = [UIColor lightLightGray];
    
    UIImage *img = [UIImage imageNamed:@"no_friends"];
    UIImageView *icon = [[UIImageView alloc] initWithImage:img];
    [icon setFrame:CGRectMake(140, 40, 50, 50)];
    [icon setContentMode:UIViewContentModeCenter];
    
    UILabel *firstLine = [[UILabel alloc] initWithFrame:CGRectMake(30, 120, 260, 21)];
    firstLine.textColor = [UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:1.0];
    firstLine.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:15];
    if (self.selectedTab == FACEBOOK_TAB) {
        firstLine.text = @"No Facebook Friends Found";
    } else if (self.selectedTab == TWITTER_TAB) {
        firstLine.text = @"No Twitter Contacts Found";
    } else if (self.selectedTab == ADDRESS_BOOK_TAB) {
        firstLine.text = @"No Address Book Friends Found";
    } else {
        firstLine.text = @"No Users Found";
    }
    [firstLine setTextAlignment:NSTextAlignmentCenter];
    
    UILabel *secondLine = [[UILabel alloc] initWithFrame:CGRectMake(25, 140, 280, 63)];
    secondLine.textColor = [UIColor colorWithRed:170/255.0 green:170/255.0 blue:170/255.0 alpha:1.0];
    secondLine.font = [UIFont fontWithName:@"HelveticaNeue" size:13];
    if (self.selectedTab != SEARCH_TAB) {
        secondLine.text = @"Search again to find more users.";
    } else {
        secondLine.text = @"None of your friends are on Vaiden yet. Don’t worry! You can invite them now.";
    }
    secondLine.numberOfLines = 0;
    [secondLine setTextAlignment:NSTextAlignmentCenter];
    
    [self.overlay addSubview:icon];
    [self.overlay addSubview:firstLine];
    [self.overlay addSubview:secondLine];
    
    [self.tableView addSubview:self.overlay];
}

- (void)checkAgainstVaidenDB:(NSDictionary *)data
{
    if (self.selectedTab == FACEBOOK_TAB) {
        [IOSRequest checkFBFriendsAgainstVaidenDBForUser:[self.preferences getUserID]
                                             withFriends:data
                                            onCompletion:^(NSArray *results) {
                                                self.unfollowedUsers = 0;
                                                [self.fbContacts removeAllObjects];
                                                for (id object in results) {
                                                    Contact *contact = [[Contact alloc] init];
                                                    [contact setUserID:[object[@"user_id"] integerValue]];
                                                    [contact setUserName:object[@"username"]];
                                                    [contact setProfileBaseString:object[@"profile_pic_string"]];
                                                    [contact setFollowingStatus:[object[@"follow_status"] integerValue]];
                                                    if ([object[@"follow_status"] integerValue] == NOT_FOLLOWING_STATUS) {
                                                        self.unfollowedUsers ++;
                                                    }
                                                    [contact setFullName:object[@"fullname"]];
                                                    [contact setLocation:object[@"location"]];
                                                    [self.fbContacts addObject:contact];
                                                }
                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                    
                                                    if ([results count] == 0) {
                                                        self.tableView.scrollEnabled = NO;
                                                        self.noResultsToDisplay = YES;
                                                        [self displayErrorOverlay];
                                                    } else {
                                                        self.noResultsToDisplay = NO;
                                                        self.tableView.scrollEnabled = YES;
                                                        [self.overlay setHidden:YES];
                                                    }
                                                    
                                                    //[self.activityIndicator stopAnimating];
                                                    self.tempView.hidden = YES;
                                                    [self.tableView reloadData];
                                                    // When finish, recover the user interaction
                                                    [self.view setUserInteractionEnabled:YES];
                                                    [self.loadingView setHidden:YES];
                                                });
                                                
                                            }
         ];
    } else if (self.selectedTab == ADDRESS_BOOK_TAB) {
        [IOSRequest checkAddressBookFriendsAgainstVaidenDBByName:[self.preferences getUserID]
                                                     withFriends:data
                                                    onCompletion:^(NSArray *results) {
                                                        self.unfollowedUsers = 0;
                                                        [self.addressBookContacts removeAllObjects];
                                                        NSLog(@"%@", results);
                                                        for (id object in results) {
                                                            NSLog(@"%@", object);
                                                            Contact *contact = [[Contact alloc] init];
                                                            [contact setUserID:[object[@"user_id"] integerValue]];
                                                            [contact setUserName:object[@"username"]];
                                                            [contact setProfileBaseString:object[@"profile_pic_string"]];
                                                            [contact setFollowingStatus:[object[@"follow_status"] integerValue]];
                                                            if ([object[@"follow_status"] integerValue] == NOT_FOLLOWING_STATUS) {
                                                                self.unfollowedUsers ++;
                                                            }
                                                            [contact setFullName:object[@"fullname"]];
                                                            [contact setLocation:object[@"location"]];
                                                            [self.addressBookContacts addObject:contact];
                                                        }
                                                        NSLog(@"%@",self.addressBookContacts);
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            
                                                            if ([results count] == 0) {
                                                                self.tableView.scrollEnabled = NO;
                                                                self.noResultsToDisplay = YES;
                                                                [self displayErrorOverlay];
                                                            } else {
                                                                self.noResultsToDisplay = NO;
                                                                self.tableView.scrollEnabled = YES;
                                                                [self.overlay setHidden:YES];
                                                            }
                                                            
                                                            //[self.activityIndicator stopAnimating];
                                                            self.tempView.hidden = YES;
                                                            [self.tableView reloadData];
                                                            // When finish, recover the user interaction
                                                            [self.view setUserInteractionEnabled:YES];
                                                            [self.loadingView setHidden:YES];
                                                        });

                                                    }
         ];
    } else if (self.selectedTab == TWITTER_TAB) {
        [IOSRequest checkTwitterFriendsAgainstVaidenDBByName:[self.preferences getUserID]
                                                 withFriends:data
                                                onCompletion:^(NSArray *results) {
                                                    self.unfollowedUsers = 0;
                                                    [self.addressBookContacts removeAllObjects];
                                                    for (id object in results) {
                                                        Contact *contact = [[Contact alloc] init];
                                                        [contact setUserID:[object[@"user_id"] integerValue]];
                                                        [contact setUserName:object[@"username"]];
                                                        [contact setProfileBaseString:object[@"profile_pic_string"]];
                                                        [contact setFollowingStatus:[object[@"follow_status"] integerValue]];
                                                        if ([object[@"follow_status"] integerValue] == NOT_FOLLOWING_STATUS) {
                                                            self.unfollowedUsers ++;
                                                        }
                                                        [contact setFullName:object[@"fullname"]];
                                                        [contact setLocation:object[@"location"]];
                                                        [self.addressBookContacts addObject:contact];
                                                    }
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        
                                                        if ([results count] == 0) {
                                                            self.tableView.scrollEnabled = NO;
                                                            self.noResultsToDisplay = YES;
                                                            [self displayErrorOverlay];
                                                        } else {
                                                            self.noResultsToDisplay = NO;
                                                            self.tableView.scrollEnabled = YES;
                                                            [self.overlay setHidden:YES];
                                                        }
                                                        
                                                        //[self.activityIndicator stopAnimating];
                                                        self.tempView.hidden = YES;
                                                        [self.tableView reloadData];
                                                        // When finish, recover the user interaction
                                                        [self.view setUserInteractionEnabled:YES];
                                                        [self.loadingView setHidden:YES];
                                                    });
                                                }
         ];
    }
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView * header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 38)];
    [header setBackgroundColor:[UIColor whiteColor]];
    
    UILabel * title = [[UILabel alloc] initWithFrame:CGRectMake(11, 0, 100, 38)];
    [title setText:@"Follow All"];
    [title setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:13]];
    [title setTextColor:[UIColor colorWithRed:34/255.0 green:34/255.0 blue:34/255.0 alpha:1.0]];
    
    self.number = [[UILabel alloc] initWithFrame:CGRectMake(74, 0, 50, 38)];
    [self.number setText:[NSString stringWithFormat:@"(%d)",self.unfollowedUsers]];
    [self.number setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:13]];
    [self.number setTextColor:[UIColor colorWithRed:34/255.0 green:34/255.0 blue:34/255.0 alpha:1.0]];
    
    UISwitch * btn = [[UISwitch alloc] initWithFrame:CGRectMake(251, 3, 30, 20)];
    if (self.selectedTab == FACEBOOK_TAB) {
        [btn setOnTintColor:[UIColor colorWithRed:54/255.0 green:100/255.0 blue:162/255.0 alpha:1.0]];
    } else if (self.selectedTab == TWITTER_TAB) {
        [btn setOnTintColor:[UIColor colorWithRed:85/255.0 green:172/255.0 blue:238/255.0 alpha:1.0]];
    } else if (self.selectedTab == ADDRESS_BOOK_TAB) {
        [btn setOnTintColor:[UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:1.0]];
    } else if (self.selectedTab == SEARCH_TAB) {
        [btn setOnTintColor:[UIColor colorWithRed:34/255.0 green:34/255.0 blue:34/255.0 alpha:1.0]];
    }
    [btn addTarget:self action:@selector(followAllSwitchClicked:) forControlEvents:UIControlEventTouchUpInside];
    if (self.unfollowedUsers == 0) {
        [btn setEnabled:NO]; //disable the switch button
    }
    
    UIView * sep = [[UIView alloc] initWithFrame:CGRectMake(0, 38, [[UIScreen mainScreen] bounds].size.width, 1)];
    [sep setBackgroundColor:[UIColor colorWithRed:201/255.0 green:201/255.0 blue:206/255.0 alpha:1.0]];
    
    [header addSubview:title];
    [header addSubview:self.number];
    [header addSubview:btn];
    [header addSubview:sep];
    
    return header;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 38;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.noResultsToDisplay) {
        return  0;
    }
    // Return the number of rows in the section.
    if (self.selectedTab == FACEBOOK_TAB) {
        return [self.fbContacts count];
    } else if (self.selectedTab == ADDRESS_BOOK_TAB || self.selectedTab == TWITTER_TAB || self.selectedTab == SEARCH_TAB) {
        return [self.addressBookContacts count];
    } else {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"My FB";
    StartByAddingFBContactsCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    /*if (self.noResultsToDisplay) {
        cell.usernameLabel.hidden = YES;
        cell.fullnameLabel.hidden = YES;
        cell.followButton.hidden = YES;
        cell.profilePic.hidden = YES;
        cell.textLabel.text = @"No results found";
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.textLabel.textColor = [UIColor lightGrayColor];
        cell.textLabel.hidden = NO;
        return cell;
    } else {*/
        cell.usernameLabel.hidden = NO;
        cell.fullnameLabel.hidden = NO;
        cell.followButton.hidden = NO;
        cell.profilePic.hidden = NO;
        cell.textLabel.hidden = YES;
        [cell.followButton makeRoundedBorderWithRadius:3];
    //}
    Contact *contact;
    if (self.selectedTab == FACEBOOK_TAB) {
        contact = [self.fbContacts objectAtIndex:indexPath.row];
    } else if (self.selectedTab == ADDRESS_BOOK_TAB || self.selectedTab == TWITTER_TAB || self.selectedTab == SEARCH_TAB) {
        contact = [self.addressBookContacts objectAtIndex:indexPath.row];
    }
    
    NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                            baseString:contact.profileBaseString
                     ];
    
    [cell.profilePic setImageWithURL:[NSURL URLWithString:url]
                    placeholderImage:[UIImage imageNamed:@"avatar square"]];
    
    [UIImage makeRoundedImageNoBorder:cell.profilePic withRadius:10];
    
//    [cell.followButton makeRoundBorderWithColor:[UIColor peacock]];
    cell.followButton.tag = indexPath.row;
    
    if (self.selectedTab != SEARCH_TAB) {
        cell.fullnameLabel.text = contact.fullName;
    } else {
        cell.fullnameLabel.text = contact.userName;
    }
    
    cell.usernameLabel.text = contact.location;
    
    if (contact.followingStatus == IS_FOLLOWING_STATUS) {
        [cell.followButton setTitle:@"Unfollow" forState:UIControlStateNormal];
        cell.followButton.enabled = YES;
        cell.followButton.backgroundColor = [UIColor colorWithRed:74/255.0 green:74/255.0 blue:74/255.0 alpha:1.0];
    } else if (contact.followingStatus == NOT_FOLLOWING_STATUS) {
        [cell.followButton setTitle:@"Follow" forState:UIControlStateNormal];
        cell.followButton.enabled = YES;
        cell.followButton.backgroundColor = [UIColor colorWithRed:74/255.0 green:74/255.0 blue:74/255.0 alpha:1.0];
    } else if (contact.followingStatus == PENDING_FOLLOWING_STATUS) {
        [cell.followButton setTitle:@"Pending" forState:UIControlStateNormal];
        cell.followButton.backgroundColor = [UIColor lightGrayColor];
        cell.followButton.enabled = NO;
    }
    
    if (self.selectedTab == FACEBOOK_TAB) {
        [cell.cornerIcon setImage:[UIImage imageNamed:@"fb_corner_icon"]];
        [cell.cornerIcon setContentMode:UIViewContentModeCenter];
    } else if (self.selectedTab == TWITTER_TAB) {
        [cell.cornerIcon setImage:[UIImage imageNamed:@"twitter_corner_logo"]];
        [cell.cornerIcon setContentMode:UIViewContentModeCenter];
    } else if (self.selectedTab == ADDRESS_BOOK_TAB) {
        [cell.cornerIcon setImage:[UIImage imageNamed:@"address_book_corner_icon"]];
        [cell.cornerIcon setContentMode:UIViewContentModeCenter];
    } else if (self.selectedTab == SEARCH_TAB) {
        [cell.cornerIcon setImage:[UIImage imageNamed:@"search_corner_logo"]];
        [cell.cornerIcon setContentMode:UIViewContentModeCenter];
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.noResultsToDisplay) {
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        return screenRect.size.height - 150;
    }
    
    return 70.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!self.noResultsToDisplay && self.isLogin) { // Won't show user information at the beginning
        self.selectUserRow = indexPath.row;
        [self performSegueWithIdentifier:@"Player Detail Segue" sender:self];
    }
}

- (void)showSubmissionError
{
    UIAlertView *alert = [[UIAlertView alloc] init];
    alert.title = @"Submission Error";
    alert.message = @"There was a problem submitting your request.  Please try again.";
    [alert addButtonWithTitle:@"OK"];
    [alert show];
}

- (IBAction)followAllSwitchClicked:(UISwitch *)sender
{
    if (sender.on) {
            self.allFollowNumber = 0;
            [self followAllFriends];
    }
    // It will be reset to off
}

- (void)followAllFriends
{
    /*if (self.allFollowNumber >= self.fbContacts.count) {
        self.allFollowNumber = 0;
    }*/
    [self disableInteraction]; // block the UI to prevent unknown error during the background process
    Contact *contact;
    int count;
    if (self.selectedTab == FACEBOOK_TAB) {
        contact = [self.fbContacts objectAtIndex:self.allFollowNumber];
        count = self.fbContacts.count;
    } else if (self.selectedTab == ADDRESS_BOOK_TAB || self.selectedTab == TWITTER_TAB || self.selectedTab == SEARCH_TAB) {
        contact = [self.addressBookContacts objectAtIndex:self.allFollowNumber];
        count = self.addressBookContacts.count;
    }
    
    if (contact.followingStatus == NOT_FOLLOWING_STATUS && contact.userID != [self.preferences getUserID]) {
        [IOSRequest makeFollowConnectionWithLeader:contact.userID
                                       andFollower:[self.preferences getUserID]
                                      onCompletion:^(NSDictionary *results) {
                                          
                                          dispatch_async(dispatch_get_main_queue(), ^ {
                                              
                                              if (![results[@"outcome"] isEqualToString:@"failure"]) {
                                                  [self.preferences setNewsfeedRefreshState: YES];
                                                  [self.preferences setChallengePlayersListRefreshState:YES];
                                                  contact.followingStatus = [results[@"follow_status"] integerValue];
                                                  if (self.selectedTab == FACEBOOK_TAB) {
                                                      [self.fbContacts replaceObjectAtIndex:self.allFollowNumber withObject:contact];
                                                  } else if (self.selectedTab == ADDRESS_BOOK_TAB || self.selectedTab == TWITTER_TAB || self.selectedTab == SEARCH_TAB) {
                                                      [self.addressBookContacts replaceObjectAtIndex:self.allFollowNumber withObject:contact];
                                                  }
                                                  self.unfollowedUsers --;
                                                  [self.tableView reloadData];
                                                  self.allFollowNumber ++;
                                                  if (self.selectedTab == FACEBOOK_TAB && self.allFollowNumber <= self.fbContacts.count - 1) {
                                                      [self followAllFriends];
                                                  } else if ((self.selectedTab == ADDRESS_BOOK_TAB || self.selectedTab == TWITTER_TAB || self.selectedTab == SEARCH_TAB) && self.allFollowNumber <= self.addressBookContacts.count - 1) {
                                                      [self followAllFriends];
                                                  } else { // the last friends unfollowed
                                                      self.allFollowNumber = 0;
                                                      [self.loadingView setHidden:YES];
                                                      [self.view setUserInteractionEnabled:YES];
                                                  }
                                              } else {
                                                  [self showSubmissionError];
                                                  self.allFollowNumber = 0;
                                              }
                                          });
                                      }
         ];
    } else {
        self.allFollowNumber ++;
        if (self.selectedTab == FACEBOOK_TAB && self.allFollowNumber <= self.fbContacts.count - 1) {
            [self followAllFriends];
        } else if ((self.selectedTab == ADDRESS_BOOK_TAB || self.selectedTab == TWITTER_TAB || self.selectedTab == SEARCH_TAB) && self.allFollowNumber <= self.addressBookContacts.count - 1) {
            [self followAllFriends];
        } else {
            self.allFollowNumber = 0;
            [self.loadingView setHidden:YES];
            [self.view setUserInteractionEnabled:YES];
        }
    }
}

- (IBAction)followAction:(UIButton *)sender
{
    Contact *vContact;
    if (self.selectedTab == FACEBOOK_TAB) {
        vContact = [self.fbContacts objectAtIndex:sender.tag];
    } else if (self.selectedTab == ADDRESS_BOOK_TAB || self.selectedTab == TWITTER_TAB || self.selectedTab == SEARCH_TAB) {
        vContact = [self.addressBookContacts objectAtIndex:sender.tag];
    }
    sender.enabled = NO;
    if (vContact.followingStatus == IS_FOLLOWING_STATUS && vContact.userID != [self.preferences getUserID]) {
        [IOSRequest removeFollowConnectionWithLeader:vContact.userID
                                         andFollower:[self.preferences getUserID]
                                        onCompletion:^(NSDictionary *results) {
                                            
                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                sender.enabled = YES;
                                                
                                                if (![results[@"outcome"] isEqualToString:@"failure"]) { // success
                                                    [self.preferences setNewsfeedRefreshState:YES];
                                                    [self.preferences setChallengePlayersListRefreshState:YES];
                                                    vContact.followingStatus = NOT_FOLLOWING_STATUS;
                                                    if (self.selectedTab == FACEBOOK_TAB) {
                                                        [self.fbContacts replaceObjectAtIndex:sender.tag withObject:vContact];
                                                    } else if (self.selectedTab == ADDRESS_BOOK_TAB || self.selectedTab == TWITTER_TAB || self.selectedTab == SEARCH_TAB) {
                                                        [self.addressBookContacts replaceObjectAtIndex:sender.tag withObject:vContact];
                                                    }
                                                    
                                                    self.unfollowedUsers ++;
                                                    [self.tableView reloadData];
                                                } else {
                                                    [self showSubmissionError];
                                                }
                                                
                                            });
                                        }];
    } else if (vContact.followingStatus == NOT_FOLLOWING_STATUS && vContact.userID != [self.preferences getUserID]) {
        [IOSRequest makeFollowConnectionWithLeader:vContact.userID
                                       andFollower:[self.preferences getUserID]
                                      onCompletion:^(NSDictionary *results) {
                                          
                                          dispatch_async(dispatch_get_main_queue(), ^ {
                                              sender.enabled = YES;
                                              
                                              if (![results[@"outcome"] isEqualToString:@"failure"]) { // fail
                                                  [self.preferences setNewsfeedRefreshState: YES];
                                                  [self.preferences setChallengePlayersListRefreshState:YES];
                                                  vContact.followingStatus = [results[@"follow_status"] integerValue];
                                                  if (self.selectedTab == FACEBOOK_TAB) {
                                                      [self.fbContacts replaceObjectAtIndex:sender.tag withObject:vContact];
                                                  } else if (self.selectedTab == ADDRESS_BOOK_TAB || self.selectedTab == TWITTER_TAB || self.selectedTab == SEARCH_TAB) {
                                                      [self.addressBookContacts replaceObjectAtIndex:sender.tag withObject:vContact];
                                                  }
                                                  
                                                  self.unfollowedUsers --;
                                                  [self.tableView reloadData];
                                              } else {
                                                  [self showSubmissionError];
                                              }
                                          });
                                      }];
        
        
    }
}

// hide extra seperator lines
- (void)setExtraCellLineHidden: (UITableView *)tableView
{
    UIView *view =[[UIView alloc]init];
    view.backgroundColor = [UIColor clearColor];
    [tableView setTableFooterView:view];
    //[self.tableView setTableHeaderView:view];
}

- (void)loadTempPage
{
    self.tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    self.tempView.backgroundColor = [UIColor lightGrayColor];
    
    /*self.activityIndicator = [[UIActivityIndicatorView alloc] init];
    [self.activityIndicator setCenter:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2 - 50)];
    [self.activityIndicator startAnimating];
    [self.tempView addSubview:self.activityIndicator];*/
    
    [self.view addSubview:self.tempView];
}

// Click the button to change the tab
- (IBAction)changeTabToFindFriends:(UIButton *)sender {
    [self dismissKeyboard];
    //[self.inviteFriendSwitch setEnabled:YES];
    [self.segueButton setEnabled:YES];
    [self.segueButton setBackgroundColor:[UIColor whiteColor]];
    [self.searchNameBar setText:nil];
    [self.tableView setUserInteractionEnabled:YES];
    [self.tableLoadingView setHidden:YES];
    if ([sender.restorationIdentifier isEqualToString:@"facebook"]) {
        self.selectedTab = FACEBOOK_TAB;
        //[self.inviteFriendSwitch setOn:NO];
        //[self.inviteFriendSwitch setOnTintColor:[UIColor colorWithRed:54/255.0 green:100/255.0  blue:162/255.0  alpha:1.0]];
        [self synchronizeFBFriends]; // retrieve the friend data
    } else if ([sender.restorationIdentifier isEqualToString:@"twitter"]) {
        self.selectedTab = TWITTER_TAB;
        //[self.inviteFriendSwitch setOn:NO];
        //[self.inviteFriendSwitch setOnTintColor:[UIColor colorWithRed:85/255.0 green:172/255.0  blue:238/255.0  alpha:1.0]];
        [self synchronizeTwitterFriendsType:@"followings"];
    } else if ([sender.restorationIdentifier isEqualToString:@"addressbook"]) {
        self.selectedTab = ADDRESS_BOOK_TAB;
        //[self.inviteFriendSwitch setOn:NO];
        //[self.inviteFriendSwitch setOnTintColor:[UIColor colorWithRed:119/255.0 green:119/255.0  blue:119/255.0  alpha:1.0]];
        [self synchronizeAddressBookFriends];
    }
}

// Click the switch to invite the friends in certain tab
- (IBAction)inviteFriendSwitchClicked:(UIButton *)sender {
    if (self.selectedTab == FACEBOOK_TAB) { // facebook tab
        [self inviteFacebookFriends];
    } else if (self.selectedTab == TWITTER_TAB) { // twitter tab
        [self inviteTwitterFriendsPicker];
    } else if (self.selectedTab == ADDRESS_BOOK_TAB) { // address book tab
        [self inviteFriendsPicker];
    }
}

# pragma mark - Invite facebook friends
// Invite the facebook friends for user
- (void) inviteFacebookFriends
{
    [self disableInteraction];
    // FBSample logic
    // if the session is open, then load the data for our view controller
    if (!FBSession.activeSession.isOpen) {
        // if the session is closed, then we open it here, and establish a handler for state changes
        [FBSession openActiveSessionWithReadPermissions:nil
                                           allowLoginUI:YES
                                      completionHandler:^(FBSession *session,
                                                          FBSessionState state,
                                                          NSError *error) {
                                          if (error) {
                                              UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                                                  message:error.localizedDescription
                                                                                                 delegate:nil
                                                                                        cancelButtonTitle:@"OK"
                                                                                        otherButtonTitles:nil];
                                              [alertView show];
                                          } else if (session.isOpen) {
                                              [self inviteFacebookFriends];
                                          }
        }];
        return;
    }
    
    if (self.friendPickerController == nil) {
        // Create friend picker, and get data loaded into it.
        self.friendPickerController = [[FBFriendPickerViewController alloc] init];
        self.friendPickerController.title = @"Invite Friends";
        self.friendPickerController.delegate = self;
    }
    
    [self.friendPickerController loadData];
    [self.friendPickerController clearSelection];
    
    
    [self presentViewController:self.friendPickerController animated:YES completion:nil];
}

- (void)facebookViewControllerCancelWasPressed:(id)sender {
    [self fillTextBoxAndDismiss];
}

- (void)fillTextBoxAndDismiss {
    //[self.inviteFriendSwitch setOn:NO];
    // When finish, recover the user interaction
    [self.view setUserInteractionEnabled:YES];
    [self.loadingView setHidden:YES];
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)facebookViewControllerDoneWasPressed:(id)sender {
    NSMutableString *text = [[NSMutableString alloc] init];
    
    // we pick up the users from the selection, and create a string that we use to update the text view
    // at the bottom of the display; note that self.selection is a property inherited from our base class
    for (id<FBGraphUser> user in self.friendPickerController.selection) {
        if ([text length]) {
            [text appendString:@", "];
        }
        [text appendString:user.id];
    }
    
    [self fillTextBoxAndDismiss];
    [self sendInvite:text];
}

- (void)sendInvite:(NSString *)invitedPeople
{
    NSMutableDictionary* params =   [NSMutableDictionary dictionaryWithObjectsAndKeys:invitedPeople, @"to", nil];
    [FBWebDialogs presentRequestsDialogModallyWithSession:nil
                                                  message:[NSString stringWithFormat:@"%@ invited you to join the mobile app Vaiden to play sports like Basketball and Tennis.  Download Vaiden from the Apple App Store Today!", [self.preferences getUserName]]
                                                    title:nil
                                               parameters:params
                                                  handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
                                                      if (error) {
                                                          // Case A: Error launching the dialog or sending request.
                                                          NSLog(@"Error sending request.");
                                                      } else {
                                                          if (result == FBWebDialogResultDialogNotCompleted) {
                                                              // Case B: User clicked the "x" icon
                                                              NSLog(@"User canceled request.");
                                                          } else {
                                                              NSLog(@"Request Sent.");
                                                          }
                                                      }
                                                      // When finish, recover the user interaction
                                                      [self.view setUserInteractionEnabled:YES];
                                                      [self.loadingView setHidden:YES];
                                                  }
     ];
}

#pragma - mark Invite friends in contacts
- (void)inviteFriendsPicker
{
    [self performSegueWithIdentifier:@"invite_addressbook_friends_segue" sender:self];
}

- (void)synchronizeAddressBookFriends
{
    CFErrorRef *error = NULL;
    ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(NULL, error);// Request authorization to Address Book
    
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined) {
        ABAddressBookRequestAccessWithCompletion(addressBookRef, ^(bool granted, CFErrorRef error) {
            if (granted) {
                // First time access has been granted, add the contact
                [self disableInteraction];
                [self getContactsForUserFromBook:addressBookRef];
            } else {
                // User denied access, Display an alert telling user the contact could not be added
                UIAlertView *alert = [[UIAlertView alloc] init];
                alert.title = @"Permission Error";
                alert.message = @"Please allow the permission to the adress book.";
                [alert addButtonWithTitle:@"OK"];
                [alert show];
            } 
        });
    } else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized) {
        // The user has previously given access, add the contact
        [self disableInteraction];
        [self getContactsForUserFromBook:addressBookRef];
    } else {
        // The user has previously denied access, send an alert telling user to change privacy setting in settings app
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Permission Error";
        alert.message = @"Please allow the permission to the adress book.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
    }
}

- (void)getContactsForUserFromBook:(ABAddressBookRef)addressBook
{
    [self.abFriendsProfilePics removeAllObjects];
    [self.abFriendsEmail removeAllObjects];
    [self.abFriendsArray removeAllObjects];
    //[self.addressBookContacts removeAllObjects];
    NSMutableDictionary *abData = [[NSMutableDictionary alloc] init];
    NSNumber *sessionID = [[NSNumber alloc] initWithInteger:[self.preferences getUserID]];
    [abData setValue:sessionID forKey:@"session_user_id"];
    
    CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeople(addressBook);
    CFIndex numberOfPeople = ABAddressBookGetPersonCount(addressBook);
    CFDataRef imageData;
    NSInteger k = 0;
    for(int i = 0; i < numberOfPeople; i++) {
        
        ABRecordRef person = CFArrayGetValueAtIndex(allPeople, i);
        
        // Get the full name of the contact
        NSString *firstName = (__bridge NSString *)(ABRecordCopyValue(person, kABPersonFirstNameProperty));
        NSString *lastName = (__bridge NSString *)(ABRecordCopyValue(person, kABPersonLastNameProperty));
        NSString *fullName = [NSString stringWithFormat:@"%@ %@",firstName,lastName];
        if (firstName == nil || lastName == nil) {
            continue; // No name saved in the addressbook
        }
        
        [self.abFriendsArray addObject:fullName];
        
        // get email address for contact
        ABMutableMultiValueRef email  = ABRecordCopyValue(person, kABPersonEmailProperty);
        if(ABMultiValueGetCount(email) > 0) {
            [self.abFriendsEmail setObject:(__bridge NSString *)ABMultiValueCopyValueAtIndex(email, 0) forKey:[NSString stringWithFormat:@"%d", k]];
        }
        
        // get image for contact
        imageData = ABPersonCopyImageData(person);
        UIImage *image = [UIImage imageWithData:(__bridge NSData *)imageData];
        if (image != nil) {
            [self.abFriendsProfilePics setObject:image forKey:[NSString stringWithFormat:@"%d", k]];
        }
        
        k++;
    }
    
    /*NSLog(@"%lu->%@",(unsigned long)self.abFriendsArray.count, self.abFriendsArray);
    NSLog(@"%lu->%@", (unsigned long)self.abFriendsEmail.count, self.abFriendsEmail);
    NSLog(@"%lu->%@", (unsigned long)self.abFriendsProfilePics.count, self.abFriendsProfilePics);*/
    [abData setObject:self.abFriendsArray forKey:@"contact_fullnames"];
    
    
    [self checkAgainstVaidenDB:abData];
}

- (void) addressBookFriendsPickerDoneClicked:(UIBarButtonItem *)sender withData:(NSMutableDictionary *)picker withType:(NSInteger)type
{
    if (type == ADDRESS_BOOK_TAB) {
        NSMutableArray * sendList = [[NSMutableArray alloc] init];
        
        for (int i=0; i<picker.count; i++) {
            NSMutableArray *pick = [picker objectForKey:[NSString stringWithFormat:@"%d", i]];
            if ([[pick objectAtIndex:1] boolValue]) { // is picked
                [sendList addObject:pick];
            }
        }
        
        [IOSRequest sendInviteEmailToFriends:sendList
                                        from:[self.preferences getUserID]
                                onCompletion:^(NSDictionary *results) {
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        NSLog(@"%@",results);
                                        if ([results[@"outcome"] isEqualToString:@"failure"]) {
                                            UIAlertView *alert = [[UIAlertView alloc] init];
                                            alert.title = @"Submission Error";
                                            alert.message = @"There was a problem completing your request.  Please retry later.";
                                            [alert addButtonWithTitle:@"OK"];
                                            [alert show];
                                        } else if ([results[@"outcome"] isEqualToString:@"unknown"]) {
                                            UIAlertView *alert = [[UIAlertView alloc] init];
                                            alert.title = @"User not found";
                                            alert.message = @"We were unfortunately not able to find your account.";
                                            [alert addButtonWithTitle:@"OK"];
                                            [alert show];
                                        }
                                        
                                        // When finish, recover the user interaction
                                        [self dismissViewControllerAnimated:YES completion:NULL];
                                        //[self.inviteFriendSwitch setOn:NO];
                                        [self.view setUserInteractionEnabled:YES];
                                        [self.loadingView setHidden:YES];
                                    });
                                }
         ];
    } else { // send tweet for the twitter friends
        for (int i = 0; i < picker.count; i++) {
            NSMutableArray *pick = [picker objectForKey:[NSString stringWithFormat:@"%d", i]];
            if ([[pick objectAtIndex:1] boolValue]) {
                NSDictionary *user = [pick objectAtIndex:0];
                NSString *screenName = user[@"screen_name"];
                [self sendTweetRequestToFriend:screenName];
            }
        }
        
        // When finish, recover the user interaction
        [self dismissViewControllerAnimated:YES completion:NULL];
        //[self.inviteFriendSwitch setOn:NO];
        [self.view setUserInteractionEnabled:YES];
        [self.loadingView setHidden:YES];
    }
}

- (void) addressBookFriendsPickerCancelWasClicked:(id)sender
{
    //[self.inviteFriendSwitch setOn:NO];
    // When finish, recover the user interaction
    [self.view setUserInteractionEnabled:YES];
    [self.loadingView setHidden:YES];
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - Invite friends in Twitter
- (void)synchronizeTwitterFriendsType:(NSString *)type
{
    [self disableInteraction];
    /*Twitter API in IOS*/
    /*Retrieve the followers first, then followings*/
    ACAccountStore *accountStore = [[ACAccountStore alloc] init];
    ACAccountType *accountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    [accountStore requestAccessToAccountsWithType:accountType
                                          options:nil
                                       completion:^(BOOL granted, NSError *error){
                                           if (granted) {
                                               NSArray *accounts = [accountStore accountsWithAccountType:accountType];
                                               // Check if the users has setup at least one Twitter account
                                               if (accounts.count > 0)
                                               {
                                                   // Use the twitter account if the username is equal to account username, otherwise, use the first account setup in this device
                                                   ACAccount *twitterAccount = [accounts objectAtIndex:0];
                                                   for(ACAccount *account in accounts)
                                                   {
                                                       if([account.username isEqualToString:[self.preferences getUserName]]){
                                                           twitterAccount = account;
                                                           break;
                                                       }
                                                   }
                                                   // Retrieve the following data
                                                   SLRequest *twitterRequest;
                                                   if ([type isEqualToString:@"followings"]) {
                                                       twitterRequest = [SLRequest requestForServiceType:SLServiceTypeTwitter
                                                                                                              requestMethod:SLRequestMethodGET
                                                                                                                        URL:[NSURL URLWithString:@"https://api.twitter.com/1.1/friends/list.json"]
                                                                                                                 parameters:[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%@", twitterAccount.username],
                                                                                                                             @"screen_name",
                                                                                                                             @"-1",
                                                                                                                             @"cursor",
                                                                                                                             nil]];
                                                   } else { // Retrieve the followers data
                                                        twitterRequest = [SLRequest requestForServiceType:SLServiceTypeTwitter
                                                                                                              requestMethod:SLRequestMethodGET
                                                                                                                        URL:[NSURL URLWithString:@"https://api.twitter.com/1.1/followers/list.json"]
                                                                                                                 parameters:[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%@", twitterAccount.username],
                                                                                                                             @"screen_name",
                                                                                                                             @"-1",
                                                                                                                             @"cursor",
                                                                                                                             nil]];
                                                   }
                                                   
                                                   [twitterRequest setAccount:twitterAccount];
                                                   // Making the request
                                                   [twitterRequest performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
                                                       dispatch_async(dispatch_get_main_queue(), ^{
                                                           // Check if we reached the reate limit
                                                           if ([urlResponse statusCode] == 429) {
                                                               NSLog(@"Rate limit reached");
                                                               return;
                                                           }
                                                           // Check if there was an error
                                                           if (error) {
                                                               UIAlertView *alert = [[UIAlertView alloc] init];
                                                               alert.title = @"Submission Error";
                                                               alert.message = [NSString stringWithFormat:@"Error: %@", error.localizedDescription];
                                                               [alert addButtonWithTitle:@"OK"];
                                                               [alert show];
                                                               return;
                                                           }
                                                           // Check if there is some response data
                                                           if (responseData) {
                                                               [self.twitterFriendsScreenNames removeAllObjects];
                                                               [self.twitterFriendsNames removeAllObjects];
                                                               NSError *error = nil;
                                                               NSDictionary *data = [NSJSONSerialization JSONObjectWithData:responseData
                                                                                                                 options:NSJSONReadingMutableLeaves
                                                                                                                   error:&error];
                                                               NSDictionary *list = data[@"users"];
                                                               int keyNumber = 0;
                                                               NSURL *url;
                                                               UIImage *image;
                                                               for (NSDictionary * user in list) {
                                                                   if ([type isEqualToString:@"followings"]) {
                                                                       [self.twitterFriendsScreenNames addObject:user[@"screen_name"]];
                                                                       [self.twitterFriendsNames addObject:user[@"name"]];
                                                                   } else {
                                                                       NSMutableDictionary *userInfo = [[NSMutableDictionary alloc] init];
                                                                       [userInfo setObject:user[@"id"] forKey:@"id"];
                                                                       [userInfo setObject:user[@"screen_name"] forKey:@"screen_name"];
                                                                       
                                                                       url = [NSURL URLWithString:user[@"profile_image_url"]];
                                                                       NSData *data = [NSData dataWithContentsOfURL:url];
                                                                       image = [[UIImage alloc] initWithData:data];
                                                                       [userInfo setObject:image forKey:@"profile_image"];
                                                                       
                                                                       [self.friendsList setObject:userInfo forKey:[NSString stringWithFormat:@"%d", keyNumber]];
                                                                       keyNumber ++;
                                                                   }
                                                               }
                                                               if ([type isEqualToString:@"followings"]) {
                                                                   NSMutableDictionary * dic = [[NSMutableDictionary alloc] init];
                                                                   NSNumber *sessionID = [[NSNumber alloc] initWithInteger:[self.preferences getUserID]];
                                                                   [dic setValue:sessionID forKey:@"session_user_id"];
                                                                   [dic setValue:self.twitterFriendsNames forKey:@"names"];
                                                                   [dic setValue:self.twitterFriendsScreenNames forKey:@"screenNames"];
                                                                   [self checkAgainstVaidenDB:dic];
                                                               } else {
                                                                   [self performSegueWithIdentifier:@"invite_addressbook_friends_segue" sender:self];
                                                               }
                                                           }
                                                       });
                                                   }];
                                               } else {
                                                   UIAlertView *alert = [[UIAlertView alloc] init];
                                                   alert.title = @"Submission Error";
                                                   alert.message = @"No twitter account setup for this device!";
                                                   [alert addButtonWithTitle:@"OK"];
                                                   [alert show];
                                               }
                                           } else {
                                               UIAlertView *alert = [[UIAlertView alloc] init];
                                               alert.title = @"Permission Error";
                                               alert.message = @"No access granted.";
                                               [alert addButtonWithTitle:@"OK"];
                                               [alert show];
                                           }
                                       }
     ];
}

- (void)inviteTwitterFriendsPicker
{
    [self synchronizeTwitterFriendsType:@"followers"];
}

- (void)sendTweetRequestToFriend:(NSString *)screenName
{
    ACAccountStore *accountStore = [[ACAccountStore alloc] init];
    ACAccountType *accountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    [accountStore requestAccessToAccountsWithType:accountType options:nil completion:^(BOOL granted, NSError *error) {
        
        if (granted && !error) {
            
            NSArray *accounts = [accountStore accountsWithAccountType:accountType];
            
            if (accounts.count > 0)
            {
                // Use the twitter account if the username is equal to account username, otherwise, use the first account setup in this device
                ACAccount *twitterAccount = [accounts objectAtIndex:0];
                for(ACAccount *account in accounts)
                {
                    if([account.username isEqualToString:[self.preferences getUserName]]){
                        twitterAccount = account;
                        break;
                    }
                }
                
                // Send private tweet to a friend
                NSString *message = @"Join me on @VaidenSports!";
                NSURL *url = [NSURL URLWithString:@"https://api.twitter.com/1.1/statuses/update.json"];
                NSDictionary *param =@{@"status":[NSString stringWithFormat:@"%@ @%@",message,screenName]};
                SLRequest *postRequest = [SLRequest requestForServiceType:SLServiceTypeTwitter
                                                            requestMethod:SLRequestMethodPOST
                                                                      URL:url parameters:param];
                
                /*UIImage *image = [UIImage imageNamed:@"vaiden_logo_for_twitter"];
                NSData *imageData = UIImageJPEGRepresentation(image,0.5);
                [postRequest addMultipartData:imageData withName:@"media[]" type:@"image/jpeg" filename:@"vaiden_logo_for_twitter.jpg"];*/
                
                [postRequest setAccount:twitterAccount];
                [postRequest performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResposnse, NSError *error){
                    NSError *jsonError = nil;
                    NSDictionary *friendsdata = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves error:&jsonError];
                    NSLog(@"response value is: %@ %d ",friendsdata,[urlResposnse statusCode]);
                    
                }];
            }
        }
    }];
}

#pragma mark - Prepare for segue
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"invite_addressbook_friends_segue"]) {
        AddressBookFriendsPickerTVC * controller = (AddressBookFriendsPickerTVC *)[[[segue destinationViewController] viewControllers] objectAtIndex:0];
        if (self.selectedTab == ADDRESS_BOOK_TAB) {
            controller.contacts = self.abFriendsArray; // pass the contacts to the list
            controller.profilePics = self.abFriendsProfilePics;
            controller.emails = self.abFriendsEmail;
            controller.currentTab = ADDRESS_BOOK_TAB;
            controller.delegate = self;
        } else if (self.selectedTab == TWITTER_TAB) {
            controller.friendList = self.friendsList;
            controller.currentTab = TWITTER_TAB;
            controller.delegate = self;
        }
    } else if ([segue.identifier isEqualToString:@"Player Detail Segue"]) {
        PlayerDetailTVC *controller = segue.destinationViewController;
            
        Contact *contact;
        if (self.selectedTab == FACEBOOK_TAB) {
            contact = [self.fbContacts objectAtIndex:self.selectUserRow];
        } else if (self.selectedTab == ADDRESS_BOOK_TAB || self.selectedTab == TWITTER_TAB || self.selectedTab == SEARCH_TAB) {
            contact = [self.addressBookContacts objectAtIndex:self.selectUserRow];
        }
        
        controller.playerUserID = contact.userID;
        controller.profilePicString = contact.profileBaseString;
    }
}

/* Dismiss the keyboard when user touch the screen */
- (void) dismissKeyboard
{
    [self.searchNameBar resignFirstResponder];
}

- (void)searchForUsers:(NSString *)searchText
{
    self.selectedTab = SEARCH_TAB;
    //[self.inviteFriendSwitch setEnabled:NO];
    [self.segueButton setEnabled:NO];
    [self.segueButton setBackgroundColor:[UIColor lightGrayColor2]];
    //[self disableInteraction];
    //[self disableTableviewScroll];
    if (searchText.length > 0){ // search name is not empty
        [self.addressBookContacts removeAllObjects];
        
        [IOSRequest searchForUser:searchText
                notAssociatedWith:[self.preferences getUserID]
                     onCompletion:^(NSMutableArray *resultArray) {
                         [self.addressBookContacts removeAllObjects];
                         self.unfollowedUsers = 0;
                         for (id object in resultArray) {
                             Contact *contact = [[Contact alloc] initWithContactDetails:[object[@"user_id"] integerValue]
                                                                          andProfileStr:object[@"profile_pic_string"]
                                                                            andUserName:object[@"username"]
                                                                            andFullName:object[@"fullname"]
                                                                            andLocation:object[@"location"]
                                                                               andAbout:object[@"about"]
                                                                               andCoins:[object[@"coins"] floatValue]
                                                                            contactType:CONTACT_TYPE_SEARCH
                                                                        followingStatus:[object[@"follow_status"] integerValue]];
                             if (contact.followingStatus == NOT_FOLLOWING_STATUS) {
                                 self.unfollowedUsers ++;
                             }
                             [self.addressBookContacts addObject:contact];
                             
                         }
                         
                         /* Change the UI */
                         dispatch_async(dispatch_get_main_queue(), ^{
                             /*[self.activityIndicator stopAnimating];
                              self.tempView.hidden = YES;*/
                             self.tableView.scrollEnabled = YES;
                             
                             if ([self.addressBookContacts count] == 0) {
                                 self.noResultsToDisplay = YES;
                                 [self displayErrorOverlay];
                                 NSLog(@"No result!");
                             } else {
                                 self.noResultsToDisplay = NO;
                                 if (!self.overlay.hidden) {
                                     [self.overlay setHidden:YES];
                                 }
                                 [self.tableView reloadData];
                                 // When finish, recover the user interaction
                                 [self.tableView setUserInteractionEnabled:YES];
                                 [self.tableLoadingView setHidden:YES];
                             }
                             
                         });
                     }
         ];
    } else {
        [self displayErrorOverlay];
        [self.tableView setUserInteractionEnabled:NO];
        [self.tableLoadingView setHidden:YES];
    }

}

#pragma - mark SEARCH BAR DELEGATE
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [self.tableLoadingView setHidden:NO];
    [self.tableView setUserInteractionEnabled:NO];
    //[self.tableView bringSubviewToFront:self.tableLoadingView];
    
    NSLog(@"Timer=%@",self.pauseTimer);
    if (self.pauseTimer) {
        if ([self.pauseTimer isValid]) {
            [self.pauseTimer invalidate];
        }
        self.pauseTimer = nil;
    }
    self.pauseTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(doUserSearch:) userInfo:searchText repeats:NO]; // DELAY 1 SEC FOR USER'S INPUT
}

-(void)doUserSearch:(NSTimer *)timer
{
    assert(timer == self.pauseTimer);
    [self searchForUsers:self.pauseTimer.userInfo];
    self.pauseTimer = nil; // important because the timer is about to release and dealloc itself
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self dismissKeyboard];
}


@end
