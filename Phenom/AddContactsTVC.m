//
//  AddContactsTVC.m
//  Vaiden
//
//  Created by James Chung on 11/27/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "AddContactsTVC.h"
#import "StartByAddingFBContacts.h"

@interface AddContactsTVC ()

@end

@implementation AddContactsTVC


- (void)viewDidLoad
{
    [super viewDidLoad];

    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];

}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Search Users Segue"]) {
        StartByAddingFBContacts *controller = segue.destinationViewController;
        controller.isLogin = YES;
    }
}

@end
