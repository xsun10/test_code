//
//  UIButton+RoundBorder.h
//  Vaiden
//
//  Created by James Chung on 8/22/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (RoundBorder)

- (void)makeRoundBorderWithColor:(UIColor *)myColor;

@end
