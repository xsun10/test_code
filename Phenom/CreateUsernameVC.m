//
//  CreateUsernameVC.m
//  Vaiden
//
//  Created by James Chung on 9/18/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "CreateUsernameVC.h"
#import "UITextField+FormValidations.h"
#import "IOSRequest.h"
#import "UserPreferences.h"
#import "UIView+Manipulate.h"
#import "AmazonKeyChainWrapper.h"
#import "AmazonClientManager.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "S3Tools.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "UIImage+ProportionalFill.h"
#import "UIImageView+AFNetworking.h"


@interface CreateUsernameVC () <UITextFieldDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *locationTextField;
@property (weak, nonatomic) IBOutlet UIButton *btn1;
@property (weak, nonatomic) IBOutlet UIButton *btn2;
@property (weak, nonatomic) IBOutlet UIButton *btn3;
@property (weak, nonatomic) IBOutlet UIButton *btn4;
@property (weak, nonatomic) IBOutlet UILabel *stateLabel;
@property (weak, nonatomic) IBOutlet UIButton *stateDropdown;
@property (nonatomic, strong) UserPreferences *preferences;
@property (weak, nonatomic) IBOutlet UIScrollView *pageScrollView;
@property (nonatomic, strong) UIBarButtonItem *submitButton;
@property (weak, nonatomic) IBOutlet UIImageView *usernameIcon;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;
@property (weak, nonatomic) IBOutlet UIImageView *profilePic;
@property (weak, nonatomic) IBOutlet UIButton *profilePicButton;
@property (strong, nonatomic) NSString *region;
@property (strong, nonatomic) NSMutableArray *states;
@property (weak, nonatomic) UIActionSheet *profilePicActionSheet;
@property (weak, nonatomic) UIActionSheet *stateActionSheet;
@property (strong, nonatomic)UIPopoverController *imagePickerPopover;
@property (strong, nonatomic) UIImage *profilePicOriginal;

@property (nonatomic, strong) NSData *imageData;
@property (nonatomic, strong) NSData *thumbnailImageData;
@property (nonatomic, strong) NSData *thumbnailRetinaImageData;
@property (nonatomic, strong) NSString *fileExtension;


@end

@implementation CreateUsernameVC

#define NORTHEAST @"Northeast"
#define SOUTHEAST @"Southeast"
#define MIDWEST @"Midwest"
#define SOUTH @"South"
#define WEST @"West"

#define TITLE_OF_ACTIONSHEET @"Add a profile pic"
#define CANCEL_BUTTON_ACTIONSHEET @"Cancel "
#define FROM_CAMERA @"Take a profile pic"
#define FROM_LIBRARY @"Choose From Photo Library"

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (NSMutableArray *) states
{
    if (!_states) _states = [[NSMutableArray alloc] init];
    return _states;
}

- (IBAction)profileButtonClicked:(id)sender {
    if (!self.profilePicActionSheet) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                      initWithTitle:TITLE_OF_ACTIONSHEET delegate:self
                                      cancelButtonTitle:CANCEL_BUTTON_ACTIONSHEET destructiveButtonTitle:nil
                                      otherButtonTitles:FROM_CAMERA, FROM_LIBRARY, nil];
        //    [actionSheet showFromTabBar:self.tabBarController.tabBar];
        [actionSheet showInView:self.view];
        
        self.profilePicActionSheet = actionSheet;
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == actionSheet.destructiveButtonIndex) {
        [self.view.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    } else {
        NSString *choice = [actionSheet buttonTitleAtIndex:buttonIndex];
        
        if ([choice isEqualToString:FROM_CAMERA]) {
            [self changePicFromCamera];
        } else if ([choice isEqualToString:FROM_LIBRARY]){
            [self changePicFromPhotoLibrary];
        } else if (![choice isEqualToString:CANCEL_BUTTON_ACTIONSHEET]) {
            NSLog(@"%@",choice);
            self.stateLabel.text = choice;
            [self getRegionForState:choice];
        }
    }
}

- (void) getRegionForState:(NSString *)state
{
    if ([state isEqualToString:@"CT"] || [state isEqualToString:@"DE"] || [state isEqualToString:@"ME"] || [state isEqualToString:@"MD"] || [state isEqualToString:@"MA"] || [state isEqualToString:@"NH"] || [state isEqualToString:@"NJ"] || [state isEqualToString:@"NY"] || [state isEqualToString:@"PA"] || [state isEqualToString:@"RI"] || [state isEqualToString:@"VT"]) {
        self.region = NORTHEAST;
    } else if ([state isEqualToString:@"AL"] || [state isEqualToString:@"AR"] || [state isEqualToString:@"FL"] || [state isEqualToString:@"GA"] || [state isEqualToString:@"KY"] || [state isEqualToString:@"LA"] || [state isEqualToString:@"MS"] || [state isEqualToString:@"NC"] || [state isEqualToString:@"SC"] || [state isEqualToString:@"TN"] || [state isEqualToString:@"VA"] || [state isEqualToString:@"WV"]) {
        self.region = SOUTHEAST;
    } else if ([state isEqualToString:@"IL"] || [state isEqualToString:@"IN"] || [state isEqualToString:@"IA"] || [state isEqualToString:@"KS"] || [state isEqualToString:@"NE"] || [state isEqualToString:@"ND"] || [state isEqualToString:@"MI"] || [state isEqualToString:@"MN"] || [state isEqualToString:@"MO"] || [state isEqualToString:@"OH"] || [state isEqualToString:@"SD"]) {
        self.region = MIDWEST;
    } else if ([state isEqualToString:@"AZ"] || [state isEqualToString:@"NM"] || [state isEqualToString:@"OK"] || [state isEqualToString:@"TX"]) {
        self.region = SOUTH;
    } else if ([state isEqualToString:@"AK"] || [state isEqualToString:@"CA"] || [state isEqualToString:@"CO"] || [state isEqualToString:@"HI"] || [state isEqualToString:@"ID"] || [state isEqualToString:@"MT"] || [state isEqualToString:@"NV"] || [state isEqualToString:@"UT"] || [state isEqualToString:@"WA"] || [state isEqualToString:@"WY"]) {
        self.region = WEST;
    }
}

- (void)changePicFromCamera
{
    [self presentImagePicker:UIImagePickerControllerSourceTypeCamera];
}


- (void) changePicFromPhotoLibrary
{
    [self presentImagePicker:UIImagePickerControllerSourceTypeSavedPhotosAlbum];
}

- (void) presentImagePicker:(UIImagePickerControllerSourceType)sourceType
{
    if ([UIImagePickerController isSourceTypeAvailable:sourceType]) {
        NSArray *availableMediaTypes = [UIImagePickerController availableMediaTypesForSourceType:sourceType];
        if ([availableMediaTypes containsObject:(NSString *)kUTTypeImage]) {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.sourceType = sourceType;
            picker.mediaTypes = @[(NSString *)kUTTypeImage];
            picker.allowsEditing = YES;
            picker.delegate = self;
            
            
            [self presentViewController:picker animated:YES completion:nil];
        }
    }
}

- (void) imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

#define MAX_IMAGE_WIDTH 70 // might want to change this to height....

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = info[UIImagePickerControllerEditedImage];
    
    if (!image) image = info[UIImagePickerControllerOriginalImage];
    if (image) {
        self.profilePicOriginal = image;
        UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
        CGRect frame = imageView.frame;
        if (frame.size.width > MAX_IMAGE_WIDTH) {
            frame.size.height = (frame.size.height / frame.size.width) * MAX_IMAGE_WIDTH;
            frame.size.width = MAX_IMAGE_WIDTH;
        }
        imageView.frame = frame;
        
        [self.profilePicButton setImage:imageView.image forState:UIControlStateNormal];
        self.imageData = UIImageJPEGRepresentation(imageView.image, 1.0);
        self.thumbnailImageData = UIImageJPEGRepresentation([image imageCroppedToFitSize:CGSizeMake(70, 70)], 1.0);
        self.thumbnailRetinaImageData = UIImageJPEGRepresentation([image imageCroppedToFitSize:CGSizeMake(140, 140)], 1.0);
        self.fileExtension = @"jpg";
        
        self.profilePicButton.contentMode = UIViewContentModeScaleAspectFill;
        self.profilePicButton.clipsToBounds = YES;
        [self.profilePicButton makeCircleWithColor:[UIColor whiteColor] andRadius:35];
        
        
    }
    
    if (self.imagePickerPopover) {
        
        [self.imagePickerPopover dismissPopoverAnimated:YES];
        self.imagePickerPopover = nil;
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)saveProfilePic
{
    if (![AmazonClientManager hasCredentials]) {
        [[Constants credentialsAlert] show];
        NSLog(@"no credentials");
    }
    else {
        
        NSLog (@"has credentials");
        
        operationQueue = [NSOperationQueue new];
        operationQueue.maxConcurrentOperationCount = 3;
        
        NSString *fileNameBase = [S3Tools getFileNameBaseStringWithType:@"profile" forUser:[self.preferences getUserID]];
        S3Tools *imageUploader1 = [[S3Tools alloc] initWithData:self.imageData
                                                        forUser:[self.preferences getUserID]
                                                       dataType:@"profile"
                                          andBaseFileNameString:fileNameBase
                                                     andFileExt:self.fileExtension];
        [operationQueue addOperation:imageUploader1];
        
        
        S3Tools *imageUploader2 = [[S3Tools alloc] initWithData:self.thumbnailImageData
                                                        forUser:[self.preferences getUserID]
                                                       dataType:@"thumbnail"
                                          andBaseFileNameString:fileNameBase
                                                     andFileExt:self.fileExtension];
        [operationQueue addOperation:imageUploader2];
        
        S3Tools *imageUploader3 = [[S3Tools alloc] initWithData:self.thumbnailRetinaImageData
                                                        forUser:[self.preferences getUserID]
                                                       dataType:@"thumbnail_retina"
                                          andBaseFileNameString:fileNameBase
                                                     andFileExt:self.fileExtension];
        [operationQueue addOperation:imageUploader3];
        
        //   [self.preferences setProfilePicString:[NSString stringWithFormat:@"%@.jpg", fileNameBase]];
        //   [self.preferences setProfilePageRefreshState:YES];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationController setNavigationBarHidden:NO animated:NO];

    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
//    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
//                                   initWithTitle: @""
//                                   style: UIBarButtonItemStyleBordered
//                                   target: nil action: nil];
    
//    [self.navigationItem setBackBarButtonItem: backButton];
    [self.navigationItem setHidesBackButton:YES animated:YES];

    
    /*UIColor *greyColor = [UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:1.0];
    self.usernameIcon.image = [[UIImage imageNamed:@"Univ Step2 Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.usernameIcon setTintColor:greyColor];*/

    [self.submitBtn makeRoundedBorderWithRadius:5];
    
    [self.usernameTextField setAutocorrectionType:UITextAutocorrectionTypeNo];
    [self.emailTextField setAutocorrectionType:UITextAutocorrectionTypeNo];
    [self.passwordTextField setAutocorrectionType:UITextAutocorrectionTypeNo];
    [self.locationTextField setAutocorrectionType:UITextAutocorrectionTypeNo];
    
    self.usernameTextField.delegate = self;
    self.emailTextField.delegate = self;
    self.passwordTextField.delegate = self;
    self.locationTextField.delegate = self;
    
    if ([self.preferences getProfilePicString].length == 0) {
        self.profilePic.image = [UIImage imageNamed:@"profile_hold"];
    } else {
        NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                baseString:[self.preferences getProfilePicString]];
        [self.profilePic setImageWithURL:[NSURL URLWithString:url]
                        placeholderImage:[UIImage imageNamed:@"avatar round"]];
    }
    
    self.emailTextField.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"email"];
    
    [self.profilePic makeRoundedBorderWithRadius:3];
    [self.profilePic setContentMode:UIViewContentModeCenter];
    
    [self getStateArray];
    self.region = NORTHEAST;
    
    self.usernameTextField.delegate = self;
    //[self setSubmitButton];
    
	// Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)keyboardWillShow:(NSNotification*)notification
{
    self.pageScrollView.contentSize = CGSizeMake(320.0, 700);
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    self.pageScrollView.contentSize = CGSizeMake(320.0, 600);
}

- (void) getStateArray {
    [self.states addObject:@"CT"];
    [self.states addObject:@"DE"];
    [self.states addObject:@"ME"];
    [self.states addObject:@"MD"];
    [self.states addObject:@"MA"];
    [self.states addObject:@"NH"];
    [self.states addObject:@"NJ"];
    [self.states addObject:@"NY"];
    [self.states addObject:@"PA"];
    [self.states addObject:@"RI"];
    [self.states addObject:@"VT"];
    [self.states addObject:@"AL"];
    [self.states addObject:@"AR"];
    [self.states addObject:@"FL"];
    [self.states addObject:@"GA"];
    [self.states addObject:@"KY"];
    [self.states addObject:@"LA"];
    [self.states addObject:@"MS"];
    [self.states addObject:@"NC"];
    [self.states addObject:@"SC"];
    [self.states addObject:@"TN"];
    [self.states addObject:@"VA"];
    [self.states addObject:@"WV"];
    [self.states addObject:@"IL"];
    [self.states addObject:@"IN"];
    [self.states addObject:@"IA"];
    [self.states addObject:@"KS"];
    [self.states addObject:@"NE"];
    [self.states addObject:@"ND"];
    [self.states addObject:@"MI"];
    [self.states addObject:@"MN"];
    [self.states addObject:@"MO"];
    [self.states addObject:@"OH"];
    [self.states addObject:@"SD"];
    [self.states addObject:@"AZ"];
    [self.states addObject:@"NM"];
    [self.states addObject:@"OK"];
    [self.states addObject:@"TX"];
    [self.states addObject:@"AK"];
    [self.states addObject:@"CA"];
    [self.states addObject:@"CO"];
    [self.states addObject:@"HI"];
    [self.states addObject:@"ID"];
    [self.states addObject:@"MT"];
    [self.states addObject:@"NV"];
    [self.states addObject:@"UT"];
    [self.states addObject:@"WA"];
    [self.states addObject:@"WY"];
}


- (void)setSubmitButton
{
    UIButton *btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 55, 21);
    btn.layer.borderColor = [UIColor whiteColor].CGColor;
    btn.layer.borderWidth = 1.0;
    
    [btn addTarget:self action:@selector(submitAction:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundColor:[UIColor clearColor]];
    [btn setTitle:@"SUBMIT" forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-MediumItalic" size:12.0]];
    
    // Initialize UIBarbuttonitem...
    self.submitButton = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = self.submitButton;
}


- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    self.pageScrollView.contentSize = CGSizeMake(320.0, 600);
}

- (IBAction)submitAction:(id)sender
{
    [self submitUsername];
}

- (void)submitUsername
{
    [self.usernameTextField resignFirstResponder];
    
    if (![self.usernameTextField checkAlphanumeric]) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Username Error";
        alert.message = @"Username is not valid.  Please enter a username between 3 and 15 letters with only alphanumeric characters.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
        
    } else if (![self.emailTextField checkEmail]){
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Email Error";
        alert.message = @"Email is not valid.  Please enter another one.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
    } else if (![self.passwordTextField checkPassword]) { // later need to have better password validation
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Password Error";
        alert.message = @"Password is not valid.  Please enter a password between 6 and 20 letters with alphanumeric or special characters.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
    } else if (![self.locationTextField checkLocationName]) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Location Error";
        alert.message = @"Location is not valid.  Please enter a valid city and state less than 50 characters";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
    } else if (self.region.length == 0 || self.stateLabel.text.length == 0) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"State Error";
        alert.message = @"State is not valid.  Please select your state.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
    } else {
        HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        HUD.labelText = @"Submitting";
        self.submitButton.enabled = NO;
        [self.usernameTextField resignFirstResponder];
        [self.emailTextField resignFirstResponder];
        [self.passwordTextField resignFirstResponder];
        [self.locationTextField resignFirstResponder];
        
        [IOSRequest createUsername:self.usernameTextField.text
                          andEmail:self.emailTextField.text
                       andPassword:self.passwordTextField.text
                       andlocation:[NSString stringWithFormat:@"%@,%@", self.locationTextField.text, self.stateLabel.text]
                         andRegion:self.region
                           forUser:[self.preferences getUserID]
                      onCompletion:^(NSDictionary *results) {
                          dispatch_async(dispatch_get_main_queue(), ^{
                              [MBProgressHUD hideHUDForView:self.view animated:YES];
                              if ([results[@"username_result"] isEqualToString:@"Username Already Exists"]) {
                                  UIAlertView *alert = [[UIAlertView alloc] init];
                                  alert.title = @"Username Error";
                                  alert.message = @"That username is already taken.  Please enter another one.";
                                  [alert addButtonWithTitle:@"OK"];
                                  [alert show];
                              } else {
                                  [self performSegueWithIdentifier:@"Username Submitted Segue" sender:self];
                              }
                              self.submitButton.enabled = YES;
                          });
                      }];
        double delayInSeconds = 10.0;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            //code to be executed on the main queue after delay
            self.submitButton.enabled = YES;
            [MBProgressHUD hideHUDForView:self.view animated:YES];// just
        });
    }
}

- (IBAction)clearUsername:(id)sender {
    self.usernameTextField.text = @"";
}

- (IBAction)clearEmail:(id)sender {
    self.emailTextField.text = @"";
}

- (IBAction)clearPassword:(id)sender {
    self.passwordTextField.text = @"";
}

- (IBAction)clearLocation:(id)sender {
    self.locationTextField.text = @"";
}

- (IBAction)dropdownClicked:(id)sender {
    [self.usernameTextField resignFirstResponder];
    [self.emailTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
    [self.locationTextField resignFirstResponder];
    if (!self.stateActionSheet) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] init];
        [actionSheet setTitle:@"Select States"];
        actionSheet.delegate = self;
        for (int i = 0; i<[self.states count]; i++) // Add states as items
        {
            [actionSheet addButtonWithTitle:self.states[i]];
        }
        [actionSheet addButtonWithTitle:CANCEL_BUTTON_ACTIONSHEET];
        [actionSheet setCancelButtonIndex:self.states.count];
        [actionSheet showInView:self.view];
        
        self.stateActionSheet = actionSheet;
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.usernameTextField resignFirstResponder];
    [self.emailTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
    [self.locationTextField resignFirstResponder];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    self.btn1.hidden = YES;
    self.btn2.hidden = YES;
    self.btn3.hidden = YES;
    self.btn4.hidden = YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if ([textField.restorationIdentifier isEqualToString:@"1"]) {
        self.btn1.hidden = NO;
        self.btn2.hidden = YES;
        self.btn3.hidden = YES;
        self.btn4.hidden = YES;
    } else if ([textField.restorationIdentifier isEqualToString:@"2"]) {
        self.btn1.hidden = YES;
        self.btn2.hidden = NO;
        self.btn3.hidden = YES;
        self.btn4.hidden = YES;
    } else if ([textField.restorationIdentifier isEqualToString:@"3"]) {
        self.btn1.hidden = YES;
        self.btn2.hidden = YES;
        self.btn3.hidden = NO;
        self.btn4.hidden = YES;
    } else if ([textField.restorationIdentifier isEqualToString:@"4"]) {
        self.btn1.hidden = YES;
        self.btn2.hidden = YES;
        self.btn3.hidden = YES;
        self.btn4.hidden = NO;
    }
}

@end
