//
//  PlayerDetailTVC.m
//  Phenom
//
//  Created by James Chung on 4/25/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "PlayerDetailTVC.h"
#import <QuartzCore/QuartzCore.h>
#import "IOSRequest.h"
#import "UserPreferences.h"
#import "S3Tools.h"
#import "S3Tools2.h"
#import "AFNetworking.h"
#import "UIImage+ProportionalFill.h"
//#import "Video.h"
#import "AddDirectIndividualMatchTVC.h"
#import "PlayerDetailSportCell.h"
#import "ShowFollowersTVC.h"
#import "ShowFollowingTVC.h"
#import "SportRatingsSummaryTVC.h"
#import "AvgSportRatingAllAttributes.h"
#import "SportRating.h"
#import "UIColor+VaidenColors.h"
#import "UIView+Manipulate.h"
#import "NSDate+Utilities.h"
#import "ComposeNewMessageIntroTVC.h"
#import "ChooseSportToRateTVC.h"
#import "PickupMatchCreationNews.h"
#import "IndividualMatchCreationNews.h"
#import "SpecialUIButton.h"
#import "SpecialUILabel.h"
#import "MKNumberBadgeView.h"
#import "MatchPlayerNewsOfficialFinishedIndividualMatch.h"
#import "ImagePost.h"
#import "PDImagePostCell.h"
#import "PDTextPostCell.h"
#import "TextPost.h"
#import "PickupMatchDetailTVC.h"
#import "IndividualMatchDetailTVC.h"
#import <FacebookSDK/FacebookSDK.h>
#import "CompetitionNews.h"
#import "PDCompetitionPostCell.h"
#import "VenueKingNews.h"
#import "PDVenueKingCell.h"
#import "VenueDetailTVC.h"
#import "PlayerDetailDetailsCell.h"
#import "PlayerDetailAboutCell.h"
#import "MyHelpers.h"
#import "PDVenueCheckinCell.h"
#import "VenueCheckinNews.h"
#import "LockerAlbumCover.h"
#import "PDLockerAlbumCell.h"
#import "ShowPhotosInLockerAlbumCVC.h"
#import "LockerPost.h"
#import "StatTag.h"
#import "TrainingTag.h"
#import "LockerPhotoDetailTVC.h"
#import "ShowLikesTVC.h"
#import "StatTagCombination.h"
#import "NoResultTVC.h"
#import "FBShareRequestDialog.h"
#import "AmazonClientManager.h"
#import "VotePopupWindowVC.h"
#import "ExperienceTVCell.h"
#import "ChangeExperienceTVC.h"
#import "ExperienceEmptyTVCell.h"
#import "SupportProfileTVCell.h"
#import "TextPostCell.h"
#import "VoteReceiving.h"
#import "VoteCell.h"
#import "VoteShopVC.h"

@interface PlayerDetailTVC () <UIActionSheetDelegate, FBShareRequestDialogDelegate, UIImagePickerControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, VotePopupWindowDelegate, ExperienceDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *profilePic;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UILabel *fullNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UILabel *numFollowingLabel;
@property (weak, nonatomic) IBOutlet UILabel *numFollowersLabel;
@property (weak, nonatomic) IBOutlet UIButton *challengeButton;
@property (weak, nonatomic) IBOutlet UIView *challengeButtonBackground;
@property (weak, nonatomic) IBOutlet UIView *mainBackgrondView;

@property (strong, nonatomic) NSMutableArray *sportsArray;
@property (strong, nonatomic) UserPreferences *preferences;
@property (weak, nonatomic) IBOutlet UIImageView *locationIcon;


@property (weak, nonatomic) IBOutlet UIButton *btn_posts;
@property (weak, nonatomic) IBOutlet UIButton *btn_details;
@property (weak, nonatomic) IBOutlet UIButton *btn_sports;


//@property (nonatomic, strong) NSMutableArray *highlightVideosArray;
//@property (nonatomic, strong) NSMutableArray *trickshotVideosArray;
@property (nonatomic) BOOL isDataLoaded;
@property (nonatomic) BOOL doesSessionUserFollowUser;

@property (nonatomic) NSUInteger selectedRow;
@property (nonatomic, strong) UIView *tempView;
@property (nonatomic, strong) UIActivityIndicatorView *onLoadActivityIndicator;
@property (nonatomic, strong) UIActivityIndicatorView *footerActivityIndicator;

@property (weak, nonatomic) IBOutlet UIView *followersCircleBackground;
@property (weak, nonatomic) IBOutlet UIView *followingCircleBackground;
@property (weak, nonatomic) IBOutlet UIButton *playersAbilityButton;
@property (weak, nonatomic) IBOutlet UILabel *challengeButtonLabel;
@property (weak, nonatomic) IBOutlet UIImageView *followerProfilePic;
@property (weak, nonatomic) IBOutlet UIImageView *followingProfilePic;
@property (weak, nonatomic) IBOutlet UILabel *whoCanChallengeUser;
@property (nonatomic) BOOL isProfileTab;
@property (weak, nonatomic) IBOutlet UIImageView *gearsIcon;

@property (weak, nonatomic) IBOutlet UIButton *btn_challenge;
@property (weak, nonatomic) IBOutlet UIButton *btn_follow;
@property (weak, nonatomic) IBOutlet UIButton *btn_rate_ability;
@property (weak, nonatomic) IBOutlet UIButton *btn_message;

@property (nonatomic, strong) UIRefreshControl *refreshControl;
@property (nonatomic) NSInteger listToDisplay;


@property (nonatomic) NSInteger selectedSection;

@property (nonatomic) NSInteger remixedNewsID;
@property (nonatomic, strong) NSIndexPath *remixedIndexPath;

@property (nonatomic, strong) NSString *fbMessage;
//@property (nonatomic, strong) NSURL *fbImage;
@property (nonatomic, strong) NSString *fbImageString;
@property (nonatomic, strong) NSString *fbTitle;

@property (nonatomic, strong) UIView *redUnderline;
@property (weak, nonatomic) IBOutlet UIView *playerPicOuterBorder;
@property (weak, nonatomic) IBOutlet UIView *aboutSectionBackgroundView;
@property (nonatomic) NSInteger selectedVenue;

@property (nonatomic, strong) NSString *aboutString;
@property (nonatomic, strong) NSString *aboutUsernameString;

@property (nonatomic, strong) NSString *heightString;
@property (nonatomic, strong) NSString *genderString;
@property (nonatomic, strong) NSString *ageString;

@property (nonatomic, strong) UILabel *permissionsErrorLabel;
@property (weak, nonatomic) IBOutlet UIView *mainButtonBar;
@property (weak, nonatomic) IBOutlet UILabel *followLabel;
@property (weak, nonatomic) IBOutlet UIImageView *followButtonPicHandle;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *actionBarButton;
@property (nonatomic) NSInteger selectedLockerID;
@property (nonatomic) BOOL showKeyboardOnCommentsPage;
@property (nonatomic, strong) NSString *playerUsername;
@property (nonatomic) BOOL wereServerResultsLoaded;

@property (assign) NSInteger targetID;
@property (nonatomic) BOOL isLockerShare;
@property (nonatomic, strong) NSArray *stats;
@property (nonatomic, strong) NSArray *drills;
@property (nonatomic, strong) UIImage * currentPickedImage;

@property (weak, nonatomic) IBOutlet UIView *buttomLine;
@property (weak, nonatomic) IBOutlet UIImageView *tabIcon1;
@property (weak, nonatomic) IBOutlet UIImageView *tabIcon2;
@property (weak, nonatomic) IBOutlet UIImageView *tabIcon3;
@property (weak, nonatomic) IBOutlet UIImageView *tabIcon4;

@property (weak, nonatomic) IBOutlet UIView *profilePicView;
@property (weak, nonatomic) IBOutlet UIButton *profilePicButton;

@property (weak, nonatomic) UIActionSheet *profilePicActionSheet;
@property (strong, nonatomic) UIImage *profilePicOriginal;
@property (nonatomic, strong) NSData *imageData;
@property (nonatomic, strong) NSData *thumbnailImageData;
@property (nonatomic, strong) NSData *thumbnailRetinaImageData;
@property (nonatomic, strong) NSString *fileExtension;
@property (strong, nonatomic)UIPopoverController *imagePickerPopover;

// Button for vote
@property (weak, nonatomic) IBOutlet UIButton *voteButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingSpace;

// Votes Display
@property (weak, nonatomic) IBOutlet UILabel *votesLable;
@property (weak, nonatomic) IBOutlet UILabel *rankLabel;
@property (nonatomic) BOOL nodata;
@property (nonatomic, strong) NSMutableArray *exp;
@property (nonatomic, strong) NSMutableArray *awd;
@property (nonatomic, strong) NSMutableArray *data;

@property (nonatomic) NSInteger changeID;
@property (nonatomic) NSString *name;
@property (nonatomic) NSString *date;
@property (nonatomic) NSString *sport;
@property (nonatomic) BOOL change;
@property (assign) NSString *type;

@property (strong, nonatomic) UIView *hintView;
@property (strong, nonatomic) UIView *hintWindowView;
@end

@implementation PlayerDetailTVC

#define DISPLAY_NEWSFEED 2
#define DISPLAY_DETAILS 0
#define DISPLAY_LOCKER 1
#define DISPLAY_MATCHES 3

#define CELL_CONTENT_WIDTH 275
#define CELL_CONTENT_MARGIN 22.5
#define FONT_SIZE 13 // is actually 13
#define HEIGHT_OF_REST_OF_CELL 370
#define HEIGHT_OF_REST_OF_CELL_NO_IMAGE 174 //was 208

// definition for change profile pic action sheet
#define TITLE_OF_ACTIONSHEET @"Change your profile pic"
#define CANCEL_BUTTON_ACTIONSHEET @"Cancel "
#define FROM_CAMERA @"Take a profile pic"
#define FROM_LIBRARY @"Choose From Photo Library"
#define MAX_IMAGE_WIDTH 70

#define TITLE_OF_ACTIONSHEET_SHARE @"Share this post with your followers"
#define TITLE_OF_ACTIONSHEET_ALREADY_SHARED @"You've already shared this post with your followers"
#define CANCEL_BUTTON_ACTIONSHEET @"Cancel "
#define SHARE_POST @"Share in newsfeed"
#define UNSHARE_POST @"Unshare from newsfeed"
#define FACEBOOK_SHARE @"Facebook Share"
#define DELETE_POST @"Delete Post"
#define HIDE_POST @"Hide Post"
#define FLAG_POST @"Flag Post"

#define BLOCK_USER @"Block User"

#define EXPERIENCE @"exp"
#define AWARD @"awd"

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (NSMutableArray *)sportsArray
{
    if (!_sportsArray) _sportsArray = [[NSMutableArray alloc] init];
    return _sportsArray;
}

- (NSMutableArray *)data
{
    if (!_data) _data = [[NSMutableArray alloc] init];
    return _data;
}

- (NSMutableArray *)exp
{
    if (!_exp) _exp = [[NSMutableArray alloc] init];
    return _exp;
}

- (NSMutableArray *)awd
{
    if (!_awd) _awd = [[NSMutableArray alloc] init];
    return _awd;
}

#pragma mark - View Function
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
    self.modalPresentationStyle = UIModalPresentationCurrentContext;
    self.tabBarController.modalPresentationStyle = UIModalPresentationCurrentContext;
    
    [self formatButtons];
    self.wereServerResultsLoaded = NO;
    
    //self.listToDisplay = DISPLAY_NEWSFEED;
    self.listToDisplay = DISPLAY_LOCKER;
    //[self.menuBarBackground makeEdgyDropshadow:[UIColor midGray2]];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
    
    if (self.fromComment) {
        // set back button
        self.navigationController.navigationBar.topItem.title = @"";
    }
    
    
    self.permissionsErrorLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, 360, 240, 42)];
    self.permissionsErrorLabel.textAlignment = NSTextAlignmentCenter;
    self.permissionsErrorLabel.numberOfLines = 2;
    self.permissionsErrorLabel.font = [UIFont fontWithName:@"HelveticaNeue-Italic" size:13];
    self.permissionsErrorLabel.textColor = [UIColor lightGrayColor];
    self.permissionsErrorLabel.text = @"You do not have permissions to view these posts";
    self.permissionsErrorLabel.hidden = YES;
    
    [self.tableView addSubview: self.permissionsErrorLabel];
    
    [self.preferences setProfilePageRefreshState:YES];
    self.tableView.scrollEnabled = NO;
    self.tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    self.tempView.backgroundColor = [UIColor lightGrayColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(refreshView) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshControl];
    
    NSInteger playerIDToShow;
    
    if (self.playerUserID == 0)
        playerIDToShow = [self.preferences getUserID];
    else
        playerIDToShow = self.playerUserID;
    
    [self loadPlayerData1];
    
    self.newsfeedPaginator = [[PlayerDetailNewsfeedPaginator alloc] initWithPDNPageSize:10 delegate:self andPlayerID:playerIDToShow];
    
    [self setupTableViewFooter];
    
    self.tabIcon1.image = [[UIImage imageNamed:@"image_icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.tabIcon1 setTintColor:[UIColor colorWithRed:199/255.0 green:160/255.0 blue:74/255.0 alpha:1.0]];
    
    // There will be no vote button if looking at myself
    if ([self.preferences getUserID] == self.playerUserID) {
        UIBarButtonItem *settingButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"setting"]
                                                                          style:UIBarButtonItemStylePlain
                                                                         target:self
                                                                         action:@selector(settingButtonClicked:)];
        [self.navigationItem setRightBarButtonItem: settingButton];
    }
    
    if ([self.preferences getUserID] == self.playerUserID) {
        //[self.voteButton setEnabled:NO];
        [self.voteButton setBackgroundColor:[UIColor colorWithRed:34/255.0 green:34/255.0 blue:34/255.0 alpha:1.0]];
        [self.voteButton setTitle:@"BOOST" forState:UIControlStateNormal];
    }
    
    if (self.playerUserID != [self.preferences getUserID]) {
        [self.profilePicButton setEnabled:NO];
    }
    [self firstlogin];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    
    [self checkBoosted];
    if ([self.preferences getProfilePageRefreshState]) {
        [self.preferences setProfilePageRefreshState:NO];
        [self refreshView];
    }
    if (self.listToDisplay == DISPLAY_NEWSFEED) {
        //NSLog(@"4");
        [self.leadingSpace setConstant:19+3*79];
    } else if (self.listToDisplay == DISPLAY_DETAILS) {
        //NSLog(@"3");
        [self.leadingSpace setConstant:19+2*79];
    } else if (self.listToDisplay == DISPLAY_LOCKER) {
        //NSLog(@"1");
        [self.leadingSpace setConstant:19];
    } else if (self.listToDisplay == DISPLAY_MATCHES) {
        //NSLog(@"2");
        [self.leadingSpace setConstant:19+79];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (self.newsfeedPaginator.noPermissions && self.playerUserID != [self.preferences getUserID] && self.listToDisplay != DISPLAY_DETAILS) {
        
        self.permissionsErrorLabel.hidden = NO;
        
    } else {
        self.permissionsErrorLabel.hidden = YES;
    }
}

- (void)refreshView
{
    self.wereServerResultsLoaded = NO;
    
    if ([self.preferences getUserID] == self.playerUserID)
        //[self displayProfileSettingsButtonForSessionUser];
    
    
    [self loadPlayerData1];
    [self loadPlayerData];
    [self loadVotesData];
    [self.refreshControl endRefreshing];
    
    self.mainButtonBar.hidden = NO;
    
    
    
    [self.newsfeedPaginator.results removeAllObjects];
    
    self.newsfeedPaginator.noPermissions = NO;
    self.permissionsErrorLabel.hidden = YES;
    
    
    if (self.listToDisplay == DISPLAY_NEWSFEED) {
        [self.newsfeedPaginator fetchFirstPageWithTab:PLAYER_DETAIL_SHOW_NEWS_POSTS];
    } else if (self.listToDisplay == DISPLAY_LOCKER) {
        [self.newsfeedPaginator fetchFirstPageWithTab:PLAYER_DETAIL_SHOW_LOCKER_COVERS];
    } else if (self.listToDisplay == DISPLAY_DETAILS) {
        [self fetchExperienceAndAward];
    } else if (self.listToDisplay == DISPLAY_MATCHES) {
        [self loadSupportersForUserInRegion:@"any" forAll:YES];
    }
    
    [self.preferences setProfilePageRefreshState:NO];
    
}

- (void) firstlogin
{
    [IOSRequest getLoginNums:[self.preferences getUserID]
                onCompletion:^(NSDictionary *results) {
                    NSLog(@"%@",results);
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if([results[@"num"] isEqualToString:@"1"]){
                            [self performSegueWithIdentifier:@"coach_segue" sender:self];
                            
                        }
                        
                    });
                }];
    
}

- (void)checkBoosted
{
    if ([self.preferences getUserID] == self.playerUserID) {
        [IOSRequest checkBoostedUserViewed:[self.preferences getUserID] onCompletion:^(NSDictionary *result) {
            if ([result[@"outcome"] isEqualToString:@"success"]) {
                self.hintView = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
                [self.hintView setBackgroundColor:[UIColor colorWithRed:119/255.0 green:119/255.0  blue:122/255.0  alpha:0.8]];
                self.hintView.center = self.navigationController.view.center;
                
                self.hintWindowView = [[UIView alloc] initWithFrame:CGRectMake(20, 91, 280, 130)];
                [self.hintWindowView makeRoundedBorderWithRadius:5];
                [self.hintWindowView setBackgroundColor:[UIColor whiteColor]];
                [self.hintWindowView setAlpha:0];
                
                UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(0, 10,  280, 24)];
                [title setText:@"Boosts"];
                [title setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:16]];
                [title setTextAlignment:NSTextAlignmentCenter];
                
                UILabel *text = [[UILabel alloc] initWithFrame:CGRectMake(40, 30,  200, 48)];
                [text setNumberOfLines:0];
                [text setText:[NSString stringWithFormat:@"You have applied a +%d boost for the next 24 hours.", [result[@"type"] integerValue]]];
                [text setFont:[UIFont fontWithName:@"HelveticaNeue" size:13]];
                [text setTextAlignment:NSTextAlignmentCenter];
                
                UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(40, 80, 200, 28)];
                button.backgroundColor = [UIColor colorWithRed:34/255.0 green:34/255.0 blue:34/255.0 alpha:1.0];
                [button setTitle:@"Got It!" forState:UIControlStateNormal];
                [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                button.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:15];
                [button addTarget:self action:@selector(quit:) forControlEvents:UIControlEventTouchUpInside];
                [button makeRoundedBorderWithRadius:5];
                
                [self.hintWindowView addSubview:title];
                [self.hintWindowView addSubview:text];
                [self.hintWindowView addSubview:button];
                
                [self.tabBarController.tabBar setHidden:YES];
                
                [self.navigationController.view addSubview:self.hintView];
                [self.navigationController.view addSubview:self.hintWindowView];
                
                [UIView animateWithDuration:0.5
                                      delay:0
                                    options:UIViewAnimationOptionCurveEaseIn
                                 animations:^{
                                     self.hintWindowView.alpha = 1;
                                 }
                                 completion: nil];

                
            }
        }];
    }
}

- (void)quit:(UIButton *)sender
{
    [self.hintView setHidden:YES];
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.hintWindowView.alpha = 0;
                     }
                     completion:nil];
    [self.tabBarController.tabBar setHidden:NO];
}

- (void)loadVotesData
{
    // There is no action, just display information, not necessary to disable the UI
    [IOSRequest getVotesInfoForUser:self.playerUserID
                       onCompletion:^(NSDictionary *result) {
                           NSLog(@"%@",result);
                           if ([result[@"outcome"] isEqualToString:@"success"]) {
                               self.votesLable.text = result[@"total"];
                               self.rankLabel.text = result[@"rank"];
                           } else if ([result[@"outcome"] isEqualToString:@"no_result"]) {
                               self.votesLable.text = @"0";
                               self.rankLabel.text = @"--";
                           } else if ([result[@"outcome"] isEqualToString:@"not_found"]) {
                               UIAlertView *alert = [[UIAlertView alloc] init];
                               alert.title = @"Submission Error";
                               alert.message = @"There was a problem submitting your request.  Please try again.";
                               [alert addButtonWithTitle:@"OK"];
                               [alert show];
                           } else if ([result[@"outcome"] isEqualToString:@"failure"]) {
                               UIAlertView *alert = [[UIAlertView alloc] init];
                               alert.title = @"Submission Error";
                               alert.message = @"There was a problem submitting your request.  Please try again.";
                               [alert addButtonWithTitle:@"OK"];
                               [alert show];
                           }
                       }];
}

- (void)loadPlayerData1
{
    self.onLoadActivityIndicator = [[UIActivityIndicatorView alloc] init];
    [self.onLoadActivityIndicator setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width/2.0, [UIScreen mainScreen].bounds.size.height/2.0 - 50)];
    [self.onLoadActivityIndicator startAnimating];
    [self.tempView addSubview:self.onLoadActivityIndicator];
    [self.view setUserInteractionEnabled:NO];
    
    [self.view addSubview:self.tempView];
    
    
    self.isDataLoaded = NO;
    self.doesSessionUserFollowUser = NO;
    
    // Begin - if the playerUserID is not set, then session user must be looking at his own profile in the Profile Tab
    //    if (self.playerUserID == 0) {
    if (self.playerUserID == 0 || self.playerUserID == [self.preferences getUserID]) {
        self.playerUserID = [self.preferences getUserID];
        self.profilePicString = [self.preferences getProfilePicString];
        self.challengeButtonBackground.backgroundColor = [UIColor clearColor];
        self.challengeButtonBackground.layer.borderColor = [UIColor darkGrayColor].CGColor;
        self.challengeButtonBackground.layer.borderWidth = 1.0;
        self.challengeButtonLabel.text = @"Profile Settings";
        self.challengeButtonLabel.textColor = [UIColor whiteColor];
        self.mainButtonBar.hidden = YES;
        self.isProfileTab = YES;
        self.navigationItem.title = @"My Profile";
        self.gearsIcon.image = [[UIImage imageNamed:@"gears"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        self.gearsIcon.tintColor = [UIColor whiteColor];
        
        self.btn_challenge.hidden = YES;
        self.btn_follow.hidden = YES;
        self.btn_message.hidden = YES;
        self.btn_rate_ability.hidden = YES;
        
        NSMutableArray *toolbarButtons = [self.toolbarItems mutableCopy];
        
        // This is how you remove the button from the toolbar and animate it
        [toolbarButtons removeObject:self.actionBarButton];
        [self setToolbarItems:toolbarButtons animated:YES];
        
    } else {
        self.isProfileTab = NO;
        self.gearsIcon.hidden = YES;
        
        /*
         if (![toolbarButtons containsObject:self.myButton]) {
         // The following line adds the object to the end of the array.
         // If you want to add the button somewhere else, use the `insertObject:atIndex:`
         // method instead of the `addObject` method.
         [toolbarButtons addObject:self.myButton];
         [self setToolbarItems:toolbarButtons animated:YES];
         }
         */
    }
    
    // End - if the playerUserID is not set, then session user must be looking at his own profile in the Profile Tab
    //  [self.followersCircleBackground makeCircleWithColor:[UIColor peacock] andRadius:40];
    //  [self.followingCircleBackground makeCircleWithColor:[UIColor peacock] andRadius:40];
    //  [self.playersAbilityButton setTitleColor:[UIColor peacock] forState:UIControlStateNormal];
    self.selectedRow = 0;
}

- (IBAction)picButtonClicked:(id)sender {
    UIButton *tmp = (UIButton *)sender;
    self.targetID = tmp.tag;
    if (self.targetID != [self.preferences getUserID]) {
        [self performSegueWithIdentifier:@"playerdetail_segue_from_self" sender:sender];
    }
}

- (void)formatButtons
{
    [self.btn_challenge makeRoundedBorderWithRadius:3];
    [self.btn_follow.imageView setContentMode:UIViewContentModeCenter];
    [self.btn_rate_ability makeRoundedBorderWithRadius:3];
    [self.btn_message makeRoundedBorderWithRadius:3];
}

#pragma - mark Actions
- (IBAction)settingButtonClicked:(id)sender{
    [self performSegueWithIdentifier:@"Settings Segue" sender:sender];
}

- (IBAction)changeProfilePic:(id)sender {
    if (!self.profilePicActionSheet) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                      initWithTitle:TITLE_OF_ACTIONSHEET delegate:self
                                      cancelButtonTitle:CANCEL_BUTTON_ACTIONSHEET destructiveButtonTitle:nil
                                      otherButtonTitles:FROM_CAMERA, FROM_LIBRARY, nil];
        [actionSheet showFromTabBar:self.tabBarController.tabBar];
        
        self.profilePicActionSheet = actionSheet;
    }
}

- (IBAction)challengePlayer:(UIButton *)sender
{
    //   if (self.isProfileTab) {
    //       [self performSegueWithIdentifier:@"Settings Segue" sender:self];
    
    //   } else {
    sender.enabled = NO;
    if (![self.preferences isVerifiedAccount]) {
        sender.enabled = YES;
        [self performSegueWithIdentifier:@"Should Verify Segue" sender:self];
    } else {
        [IOSRequest checkOKToChallengeUser:self.playerUserID
                             bySessionUser:[self.preferences getUserID
                                            ] onCompletion:^(NSDictionary *results) {
                                 dispatch_async(dispatch_get_main_queue(), ^{
                                     sender.enabled = YES;
                                     if ([results[@"ok"] isEqualToString:@"yes"]) {
                                         [self performSegueWithIdentifier:@"Challenge Segue" sender:self];
                                     } else {
                                         UIAlertView *alert = [[UIAlertView alloc] init];
                                         alert.title = @"Can't Challenge User";
                                         alert.message = @"This user can only be challenged by players he / she follows";
                                         [alert addButtonWithTitle:@"OK"];
                                         [alert show];
                                     }
                                 });
                                 
                             }];
        
    }
    
    // }
    
}

- (IBAction)followPlayer:(id)sender
{
    [self followPlayerAction];
}

- (IBAction)rateAbilityAction:(id)sender
{
    [self performSegueWithIdentifier:@"Rate Ability Segue" sender:self];
}

- (IBAction)messageAction:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc] init];
    alert.title = @"Coming Soon...";
    alert.message = @"This feature is in the works.  It will be added soon...";
    [alert addButtonWithTitle:@"OK"];
    [alert show];
    //   [self performSegueWithIdentifier:@"Compose Message Segue" sender:self];
}

- (void)followPlayerAction
{
    if (self.isDataLoaded && self.doesSessionUserFollowUser == NO && self.playerUserID != [self.preferences getUserID]) {
        self.btn_follow.enabled = NO;
        [IOSRequest makeFollowConnectionWithLeader:self.playerUserID
                                       andFollower:[self.preferences getUserID]
                                      onCompletion:^(NSDictionary *results) {
                                          
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              self.btn_follow.enabled = YES;
                                              
                                              if (![results[@"outcome"] isEqualToString:@"failure"]) {
                                                  if ([results[@"follow_status"] integerValue] == 1) {
                                                      [self.btn_follow setBackgroundImage:[UIImage imageNamed:@"unfollow"] forState:UIControlStateNormal];
                                                      self.followLabel.text = @"Unfollow";
                                                      self.followButtonPicHandle.image = [UIImage imageNamed:@"UnFollow Icon"];
                                                      [self.preferences setNewsfeedRefreshState:YES];
                                                      [self.preferences setChallengePlayersListRefreshState:YES];
                                                      self.doesSessionUserFollowUser = YES;
                                                  } else if ([results[@"follow_status"] integerValue] == 2) {
                                                      self.followLabel.text = @"Pending";
                                                      [self.btn_follow setBackgroundImage:[UIImage imageNamed:@"pending"] forState:UIControlStateNormal];
                                                      self.followButtonPicHandle.image = [UIImage imageNamed:@"Follow Icon"];
                                                      self.btn_follow.enabled = NO;
                                                  }
                                                  
                                              } else {
                                                  [self showSubmissionError];
                                              }
                                          });
                                          
                                      }];
    } else if (self.isDataLoaded && self.doesSessionUserFollowUser == YES && self.playerUserID != [self.preferences getUserID]) {
        self.btn_follow.enabled = NO;
        [IOSRequest removeFollowConnectionWithLeader:self.playerUserID
                                         andFollower:[self.preferences getUserID]
                                        onCompletion:^(NSDictionary *results) {
                                            
                                            dispatch_async(dispatch_get_main_queue(), ^ {
                                                self.btn_follow.enabled = YES;
                                                
                                                if (![results[@"outcome"] isEqualToString:@"failure"]) {
                                                    [self.btn_follow setBackgroundImage:[UIImage imageNamed:@"follow"] forState:UIControlStateNormal];
                                                    self.followLabel.text = @"Follow";
                                                    self.followButtonPicHandle.image = [UIImage imageNamed:@"Follow Icon"];
                                                    [self.preferences setNewsfeedRefreshState:YES];
                                                    [self.preferences setChallengePlayersListRefreshState:YES];
                                                    self.doesSessionUserFollowUser = NO;
                                                } else {
                                                    [self showSubmissionError];
                                                }
                                            });
                                        }];
    }
}




- (IBAction)voteButtonClicked:(id)sender {
    if (self.playerUserID == [self.preferences getUserID]) {
        // Boost
        [self performSegueWithIdentifier:@"shop_from_profile" sender:sender];
    } else {
        [IOSRequest getOptSettingsForUser:self.playerUserID
                             onCompletion:^(NSDictionary *results) {
                                 
                                 dispatch_async(dispatch_get_main_queue(), ^{
                                     
                                     if ([results[@"outcome"] isEqualToString:@"failure"]) {
                                         
                                     } else {
                                         if(![results[@"approval_flag"] isEqualToString:@"in"] ){
                                             UIAlertView *alert = [[UIAlertView alloc] init];
                                             alert.title = @"User Opted Out";
                                             alert.message = @"This user has opted out of the endorsement competition.";
                                             [alert addButtonWithTitle:@"OK"];
                                             [alert show];
                                             
                                         }
                                         else{[self performSegueWithIdentifier:@"vote_popup_segue" sender:sender];}
                                     }
                                 });
                             }];
    }
    

    
}


#pragma - mark Action Sheet Delegate
- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == actionSheet.destructiveButtonIndex) {
        [self.view.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    } else {
        NSString *choice = [actionSheet buttonTitleAtIndex:buttonIndex];
        
        if ([choice isEqualToString:FROM_CAMERA]) {
            [self changePicFromCamera];
        } else if ([choice isEqualToString:FROM_LIBRARY]){
            [self changePicFromPhotoLibrary];
        } else if ([choice isEqualToString:SHARE_POST] || [choice isEqualToString:UNSHARE_POST]) {
            [self submitRemixPost];
        } else if ([choice isEqualToString:FACEBOOK_SHARE]) {
            [self startFBShare];
        } else if ([choice isEqualToString:BLOCK_USER]) {
            [self blockUser];
        } else if ([choice isEqualToString:DELETE_POST]) {
            [self deletePost];
        } else if ([choice isEqualToString:FLAG_POST]) {
            [self flagPost];
        }
    }
}

#pragma - mark Image Picker Delegate
- (void) imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = info[UIImagePickerControllerEditedImage];
    
    if (!image) image = info[UIImagePickerControllerOriginalImage];
    if (image) {
        self.profilePicOriginal = image;
        self.profilePic.image = self.profilePicOriginal; // Display the image temp
        /*CGRect frame = self.profilePic.frame;
        if (frame.size.width > MAX_IMAGE_WIDTH) {
            frame.size.height = (frame.size.height / frame.size.width) * MAX_IMAGE_WIDTH;
            frame.size.width = MAX_IMAGE_WIDTH;
        }
        self.profilePic.frame = frame;*/
        
        self.imageData = UIImageJPEGRepresentation(self.profilePic.image, 1.0);
        self.thumbnailImageData = UIImageJPEGRepresentation([image imageCroppedToFitSize:CGSizeMake(70, 70)], 1.0);
        self.thumbnailRetinaImageData = UIImageJPEGRepresentation([image imageCroppedToFitSize:CGSizeMake(140, 140)], 1.0);
        self.fileExtension = @"jpg";
        [self saveProfilePic];
    }
    
    if (self.imagePickerPopover) {
        [self.imagePickerPopover dismissPopoverAnimated:YES];
        self.imagePickerPopover = nil;
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

#pragma - mark Change Profile Pic
- (void)saveProfilePic
{
    if (![AmazonClientManager hasCredentials]) {
        [[Constants credentialsAlert] show];
    }
    else {
        NSOperationQueue *operationQueue = [NSOperationQueue new];
        operationQueue.maxConcurrentOperationCount = 3;
        
        NSString *fileNameBase = [S3Tools getFileNameBaseStringWithType:@"profile" forUser:[self.preferences getUserID]];
        S3Tools *imageUploader1 = [[S3Tools alloc] initWithData:self.imageData
                                                        forUser:[self.preferences getUserID]
                                                       dataType:@"profile"
                                          andBaseFileNameString:fileNameBase
                                                     andFileExt:self.fileExtension];
        [operationQueue addOperation:imageUploader1];
        
        
        S3Tools *imageUploader2 = [[S3Tools alloc] initWithData:self.thumbnailImageData
                                                        forUser:[self.preferences getUserID]
                                                       dataType:@"thumbnail"
                                          andBaseFileNameString:fileNameBase
                                                     andFileExt:self.fileExtension];
        [operationQueue addOperation:imageUploader2];
        
        S3Tools *imageUploader3 = [[S3Tools alloc] initWithData:self.thumbnailRetinaImageData
                                                        forUser:[self.preferences getUserID]
                                                       dataType:@"thumbnail_retina"
                                          andBaseFileNameString:fileNameBase
                                                     andFileExt:self.fileExtension];
        [operationQueue addOperation:imageUploader3];
        
        [self.preferences setProfilePicString:[NSString stringWithFormat:@"%@.jpg", fileNameBase]];
    }
}

- (void)changePicFromCamera
{
    [self presentImagePicker:UIImagePickerControllerSourceTypeCamera];
}

- (void) changePicFromPhotoLibrary
{
    [self presentImagePicker:UIImagePickerControllerSourceTypeSavedPhotosAlbum];
}

- (void) presentImagePicker:(UIImagePickerControllerSourceType)sourceType
{
    if ([UIImagePickerController isSourceTypeAvailable:sourceType]) {
        NSArray *availableMediaTypes = [UIImagePickerController availableMediaTypesForSourceType:sourceType];
        if ([availableMediaTypes containsObject:(NSString *)kUTTypeImage]) {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.sourceType = sourceType;
            picker.mediaTypes = @[(NSString *)kUTTypeImage];
            picker.allowsEditing = YES;
            picker.delegate = self;
            
            [self presentViewController:picker animated:YES completion:nil];
        }
    }
}

/*- (void)displayProfileSettingsButtonForSessionUser
{
    UIView *buttonBackground = [[UIView alloc] initWithFrame:CGRectMake(0, 200, 320, 61)];
    buttonBackground.backgroundColor = [UIColor clearColor];
    
    UIButton *settingsButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 320, 61)];
    [settingsButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [settingsButton setTitle:@"Profile Settings" forState:UIControlStateNormal];
    [settingsButton addTarget:self action:@selector(goToSettings) forControlEvents:UIControlEventTouchUpInside];
    settingsButton.backgroundColor = [UIColor lightGrayColor2];
    
    UIImageView *iv = [[UIImageView alloc] initWithFrame:CGRectMake(70, 20, 20, 20)];
    iv.image = [[UIImage imageNamed:@"gears"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    iv.tintColor = [UIColor darkGrayColor];
    self.mainButtonBar.hidden = YES;
    
    
    [buttonBackground addSubview:settingsButton];
    [buttonBackground addSubview:iv];
    [self.view addSubview:buttonBackground];
    
}*/

#pragma - mark Segue Functions
- (void)goToSettings
{
    [self performSegueWithIdentifier:@"Settings Segue" sender:self];
}

- (void)showSubmissionError
{
    UIAlertView *alert = [[UIAlertView alloc] init];
    alert.title = @"Submission Error";
    alert.message = @"There was a problem submitting your request.  Please try again.";
    [alert addButtonWithTitle:@"OK"];
    [alert show];
}

- (void)loadPlayerData
{
    NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                            baseString:[MyHelpers getNewPicURLIfFacebookSquareSize:self.profilePicString]];
    
    [self.profilePic setImageWithURL:[NSURL URLWithString:url]
                    placeholderImage:[UIImage imageNamed:@"avatar square"]];
    self.profilePic.contentMode = UIViewContentModeScaleAspectFill;
    self.profilePic.clipsToBounds = YES;
    
  

    [IOSRequest fetchUserInfo:self.playerUserID
                bySessionUser:[self.preferences getUserID]
                  withPageNum:1
                 andSpanCells:10
                 onCompletion:^(NSDictionary *resultDictionary) {
                     [self.sportsArray removeAllObjects];
                     // iterate through sports array in the dictionary
                     for (id object in resultDictionary[@"sports"]) {
                         
                         PlayerSport *sport = [[PlayerSport alloc] initWithSportDetails:[object[@"sport_id"] integerValue]
                                                                                 andName:object[@"sport_name"]
                                                                                 andType:object[@"sport_type"]
                                                                                andLevel:object[@"level"]
                                                                          andSportRecord:object[@"win_loss_record"]];
                         id subobj = object[@"sport_ratings"];
                         
                         AvgSportRatingAllAttributes *avgSportRatingAllAttributes = [[AvgSportRatingAllAttributes alloc ] initWithAvg:[subobj[@"avg_rating"] floatValue]
                                                                                                                       withNumRatings:[subobj[@"num_ratings"] integerValue]];
                         [sport setAvgSportRatingAllAttributes:avgSportRatingAllAttributes];
                         
                         [self.sportsArray addObject:sport];
                     }
                     
                    
                     
                     /* set age */
                     
                     NSDate *date = nil;
                     NSDateComponents *ageComponents = nil;
                     
                     if (![resultDictionary[@"dob"] isEqualToString:@"0000-00-00 00:00:00"]) {
                         NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                         dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
                         date = [[dateFormatter dateFromString:resultDictionary[@"dob"]] toLocalTime];
                     
                         ageComponents = [[NSCalendar currentCalendar]
                                                        components:NSYearCalendarUnit
                                                        fromDate:date
                                                        toDate:[NSDate date]
                                                        options:0];
                     
                     }
                     
                     /* end set age */
                     
                     // iterate through videos array in dictionary
      /*
                     for (id object in resultDictionary[@"videos"]) {
                         NSURL *videoURL = nil;
                         
                         if ([object[@"filetype"] isEqualToString:@"highlight_video"]) {
                             
                             videoURL = [NSURL URLWithString:[S3Tools getFileNameStringWithType:@"highlight_video"
                                                                                     baseString:object[@"filestring"]]];
                             
                         } else if ([object[@"filetype"] isEqualToString:@"trick_video"]) {
                             videoURL = [NSURL URLWithString:[S3Tools getFileNameStringWithType:@"trick_video"
                                                                                     baseString:object[@"filestring"]]];
                         }
                         NSLog (@"%@", videoURL);
      //                   UIImage *thumb = [[UIImage imageFromMovie2:videoURL] imageCroppedToFitSize:CGSizeMake(70.0, 70.0)];
                         
                         Video *vid = [[Video alloc] initWithVideoDetails:object[@"filestring"]
                                                       withThumbnailImage:nil
                                                             andVideoType:object[@"filetype"]];
                         
                         if ([object[@"filetype"] isEqualToString:@"highlight_video"]) {
                             [self.highlightVideosArray addObject:vid];
                         } else if ([object[@"filetype"] isEqualToString:@"trick_video"]) {
                             [self.trickshotVideosArray addObject:vid];
                         }
                     }
                     
            */         
                    
                     dispatch_async(dispatch_get_main_queue(), ^{
                         self.tableView.scrollEnabled = YES;
                         NSString *userLocation;
                         
                         NSString *tempString = [resultDictionary[@"location"] stringByReplacingOccurrencesOfString:@" " withString:@""];

                        
                         if ([tempString length] == 0) {
                             userLocation = @"Location not specified";
                         } else {
                            userLocation = resultDictionary[@"location"];
                         }
                         
                         if ([resultDictionary[@"ht_feet"] isKindOfClass:[NSNull class]] || [resultDictionary[@"ht_feet"] length] == 0 || [resultDictionary[@"ht_feet"] integerValue] == 0) {
                             self.heightString = @"N/A";
                         } else {
                             self.heightString = [NSString stringWithFormat:@"%@' %@\"", resultDictionary[@"ht_feet"], resultDictionary[@"ht_inches"]];

                         }
                         
                         if ([resultDictionary[@"gender"] isKindOfClass:[NSNull class]] || [resultDictionary[@"gender"] length] == 0) {
                             self.genderString = @"N/A";
                         } else {
                             self.genderString = [[resultDictionary[@"gender"] substringToIndex:1] uppercaseString];
                         }
                         
                         if (ageComponents)
                             self.ageString = [NSString stringWithFormat:@"%ld yrs", (long)[ageComponents year]];
                         else
                             self.ageString = @"N/A";
                         
                         if ([resultDictionary[@"fullname"] length] > 0) {
                             NSString *fullName = resultDictionary[@"fullname"];
                             NSArray *wordsAndEmptyStrings = [fullName componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                             NSArray *words = [wordsAndEmptyStrings filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"length > 0"]];
                             self.fullNameLabel.text = [[words objectAtIndex:words.count - 1] uppercaseString]; // Last name
                             
                             if (words.count > 1) {
                                 NSString * restName;
                                 for (int i=0; i<words.count; i++) {
                                     NSString *str = [words objectAtIndex:i];
                                     if (restName.length == 0) {
                                         restName = str;
                                     } else if (i == words.count - 1) {
                                         break;
                                     } else {
                                         restName = [restName stringByAppendingString:[NSString stringWithFormat:@" %@", str]];
                                     }
                                 }
                                 self.usernameLabel.text = [restName uppercaseString];
                             } else {
                                 self.usernameLabel.text = @"";
                             }
                             
                         } else
                             self.fullNameLabel.text = resultDictionary[@"username"];
                         
                         self.playerUsername = resultDictionary[@"username"];
                         
                         if (self.playerUserID != [self.preferences getUserID])
                             self.navigationItem.title = resultDictionary[@"username"];

                         
                         //self.usernameLabel.text = [NSString stringWithFormat:@"@%@", resultDictionary[@"username"]];
                         self.aboutUsernameString = [NSString stringWithFormat:@"About %@", resultDictionary[@"username"]];
                         self.aboutString = resultDictionary[@"about"];
                         
                         if ([userLocation isEqualToString:@"(null)"]) {
                             self.locationLabel.text = @"Location not specified";
                         } else {
                             self.locationLabel.text = userLocation;
                         }
                         
                         if ([resultDictionary[@"who_can_challenge"] isEqualToString:@"following"]) {
                             self.whoCanChallengeUser.text = [NSString stringWithFormat:@"Can only be challenged by players %@ follows", resultDictionary[@"username"]];
                         } else {
                             self.whoCanChallengeUser.text = [NSString stringWithFormat:@"%@ can be challenged by Anyone", resultDictionary[@"username"]];
                         }
                         
                         
                         self.numFollowersLabel.text = [NSString stringWithFormat:@"%ld", [resultDictionary[@"num_followers"] integerValue]];
                         self.numFollowingLabel.text = [NSString stringWithFormat:@"%ld", [resultDictionary[@"num_following"] integerValue]];
                         
                         
                         if ([resultDictionary[@"follow_status"] integerValue] == 0 && self.playerUserID != [self.preferences getUserID]) {
                             self.doesSessionUserFollowUser = NO;
                             self.btn_follow.enabled = YES;
      //                       [self.btn_follow setTitle:@"Follow" forState:UIControlStateNormal];
                             self.followLabel.text = @"Follow";
                             self.followButtonPicHandle.image = [UIImage imageNamed:@"Follow Icon"];
                             [self.btn_follow setBackgroundImage:[UIImage imageNamed:@"follow"] forState:UIControlStateNormal];
                         } else if (self.playerUserID == [self.preferences getUserID]) {
                             self.doesSessionUserFollowUser = NO;
                             self.btn_follow.enabled = YES;
                         } else if ([resultDictionary[@"follow_status"] integerValue] == 2) {
                             self.doesSessionUserFollowUser = NO;
                         //    [self.btn_follow setTitle:@"Pending" forState:UIControlStateNormal];
                             self.followLabel.text = @"Pending";
                             [self.btn_follow setBackgroundImage:[UIImage imageNamed:@"pending"] forState:UIControlStateNormal];
                            // self.followButtonPicHandle.image = [UIImage imageNamed:@"Follow Icon"];
                             self.followButtonPicHandle.image = [[UIImage imageNamed:@"Follow Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                             self.followButtonPicHandle.tintColor = [UIColor lightGrayColor];

                             self.btn_follow.enabled = NO;
                         }else {
                             self.btn_follow.enabled = YES;
                             self.doesSessionUserFollowUser = YES;
                        //    [self.btn_follow setTitle:@"UnFollow" forState:UIControlStateNormal];
                             self.followLabel.text = @"Unfollow";
                             self.followButtonPicHandle.image = [UIImage imageNamed:@"UnFollow Icon"];
                             [self.btn_follow setBackgroundImage:[UIImage imageNamed:@"unfollow"] forState:UIControlStateNormal];
                         }
                        
                         
                         self.isDataLoaded = YES;
                         
                    //     if ([self.preferences getUserID] == self.playerUserID)
                    //         [self displayProfileSettingsButtonForSessionUser];
                         
                         [self.onLoadActivityIndicator stopAnimating];
                         self.tempView.hidden = YES;
                         [self.view setUserInteractionEnabled:YES];
                         
                         if ([self.profilePicString isEqualToString:@""] || [self.profilePicString isKindOfClass:[NSNull class]] || self.profilePicString == nil) {
                             NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                                     baseString:[MyHelpers getNewPicURLIfFacebookSquareSize:resultDictionary[@"profile_pic_string"]]];
                             
                             [self.profilePic setImageWithURL:[NSURL URLWithString:url]
                                                      placeholderImage:[UIImage imageNamed:@"avatar square"]];
                             
                          //   [UIImage makeRoundedImage:self.profilePic withRadius:50];
                             
                         }
                         
                         // set up the paginator
                     // no    self.newsfeedPaginator = [[PlayerDetailNewsfeedPaginator alloc] initWithPDNPageSize:10 delegate:self andDataObject:resultDictionary[@"player_newsfeed"]];
                     //    self.newsfeedPaginator = [[PlayerDetailNewsfeedPaginator alloc] initWithPDNPageSize:10 delegate:self andPlayerID:self.playerUserID];
                       //  [self.newsfeedPaginator fetchFirstPage];
                       //  [self.newsfeedPaginator fetchFirstPageWithTab:PLAYER_DETAIL_SHOW_LOCKER_COVERS];
                         
                         [self.tableView reloadData];
                         
                     });
                     
                     //                       }];
                    
                 }];
    
    
}

#pragma - mark ScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (self.listToDisplay == DISPLAY_DETAILS || self.listToDisplay == DISPLAY_MATCHES) {
        return;
    }
    CGPoint offset = scrollView.contentOffset;
    CGRect bounds = scrollView.bounds;
    CGSize size = scrollView.contentSize;
    UIEdgeInsets inset = scrollView.contentInset;
    float y = offset.y + bounds.size.height - inset.bottom;
    float h = size.height;
    
    float reload_distance = 10;
    if(y > h + reload_distance) {
        // ask next page only if we haven't reached last page
        if(![self.newsfeedPaginator reachedLastPage])
        {
            // fetch next page of results
            [self fetchNextPage];
        }
    }
}

- (void)fetchNextPage
{
    [self.newsfeedPaginator fetchNextPage];
    
    if (self.newsfeedPaginator.page > 0)
        [self.footerActivityIndicator startAnimating];
}


- (void)setupTableViewFooter
{
    // set up label
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    footerView.backgroundColor = [UIColor clearColor];
    
    // set up activity indicator
 //   UIActivityIndicatorView *activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.footerActivityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.footerActivityIndicator.center = CGPointMake(160, 22);
    self.footerActivityIndicator.hidesWhenStopped = YES;
    
    [footerView addSubview:self.footerActivityIndicator];
    
    self.tableView.tableFooterView = footerView;
}


# pragma - mark Paginator
- (void)paginator:(id)paginator didReceiveResults:(NSArray *)results
{
    // update tableview footer
    self.wereServerResultsLoaded = YES;
    [self.footerActivityIndicator stopAnimating];
    
    // update tableview content
    // easy way : call [tableView reloadData];
    // nicer way : use insertRowsAtIndexPaths:withAnimation:
    NSMutableArray *indexPaths = [[NSMutableArray alloc] init];
    NSInteger i = [self.newsfeedPaginator.results count] - [results count];
    
    for(NSDictionary *result in results)
    {
        [indexPaths addObject:[NSIndexPath indexPathForRow:0 inSection:i]];
        i++;
    }
    
   /*
    if ([self.newsfeedPaginator.results count] > 0)
        self.noResultsToDisplay = NO;
    else
        self.noResultsToDisplay = YES;
 */
    [self.tableView reloadData];
    //[self.onLoadActivityIndicator stopAnimating];
    self.tableView.scrollEnabled = YES;
    
    //self.tempView.hidden = YES;
    
}

- (void)paginatorDidReset:(id)paginator
{
    [self.tableView reloadData];
    
}

- (void)paginatorDidFailToRespond:(id)paginator
{
    // Todo
}

- (void)endRefreshWhenSynced
{
    dispatch_async(dispatch_get_main_queue(), ^{
        //           [self.newsPosts sortUsingDescriptors:
        //            @[[NSSortDescriptor sortDescriptorWithKey:@"newsDate" ascending:NO]]];
        [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
   //     self.displayTableOK = YES;
        
        [self.tableView reloadData];
        
    });
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (self.listToDisplay == DISPLAY_NEWSFEED) {
        if ([self.newsfeedPaginator.results count] == 0 && self.wereServerResultsLoaded)
            return 1;
        else
           return [self.newsfeedPaginator.results count];
     
    }
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (self.listToDisplay == DISPLAY_DETAILS)
        return 2 + (self.exp.count==0?1:self.exp.count) + 1 + (self.awd.count==0?1:self.awd.count);

    else if (self.listToDisplay == DISPLAY_LOCKER) {
        if ([self.newsfeedPaginator.results count] == 0 && self.wereServerResultsLoaded)
            return 1;
        else
            return [self.newsfeedPaginator.results count];
    } else if (self.listToDisplay == DISPLAY_MATCHES) {
        if (!self.newsfeedPaginator.noPermissions) {
            return self.data.count==0?2:self.data.count+1;
        } else {
            return 0;
        }
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.listToDisplay == DISPLAY_DETAILS) {
        
        if (indexPath.row == 0) {
            PlayerDetailDetailsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Details Cell" forIndexPath:indexPath];
    
            cell.ageLabel.text = self.ageString;
            cell.heightLabel.text = self.heightString;
            cell.genderLabel.text = self.genderString;
            cell.aboutTextLabel.text = self.aboutString;
            cell.aboutUsernameLabel.text = self.aboutUsernameString;
        
            return cell;
            
        } else if (indexPath.row == 1) { // experience title
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"About Cell" forIndexPath:indexPath];
            return cell;
        } else if (indexPath.row > 1 && indexPath.row < self.exp.count + 2 && self.exp.count > 0) {
            ExperienceTVCell *cell = [tableView dequeueReusableCellWithIdentifier:@"exp_profile_cell"];
            cell.tag = [[self.exp objectAtIndex:indexPath.row - 2][@"id"] integerValue];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            cell.title.text = [self.exp objectAtIndex:indexPath.row - 2][@"team_name"];
            cell.time.text = [NSString stringWithFormat:@"%@ to %@",[self.exp objectAtIndex:indexPath.row - 2][@"start"],[self.exp objectAtIndex:indexPath.row - 2][@"end"]];
            cell.sport.text = [self.exp objectAtIndex:indexPath.row - 2][@"sport"];
            if ([self.preferences getUserID] == self.playerUserID) {
                [cell.icon setHidden:NO];
            } else {
                [cell.icon setHidden:YES];
            }
            
            return cell;
        } else if (self.exp.count == 0 && indexPath.row == 2) {
            ExperienceEmptyTVCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"empty_cell"];
            cell.emptycell.text = @"No Experience Listed.";
            
            return cell;
        } else if ((indexPath.row == self.exp.count + 2 && self.exp.count > 0)||(indexPath.row == 3 && self.exp.count == 0)){ // award title
            UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"award_title_cell" forIndexPath:indexPath];
            return cell;
        } else if ((self.exp.count>0 && self.awd.count==0 && indexPath.row==self.exp.count+3) || (self.exp.count==0 && self.awd.count==0 && indexPath.row==4)) {
            ExperienceEmptyTVCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"empty_cell"];
            cell.emptycell.text = @"No Awards Listed.";
            
            return cell;
        } else {
            NSInteger num = 0;
            if (self.exp.count == 0 && self.awd.count > 0) {
                num = indexPath.row - 2 - 1 - 1;
            } else if (self.exp.count > 0 && self.awd.count > 0) {
                num = indexPath.row - 2 - self.exp.count - 1;
            }
            ExperienceTVCell *cell = [tableView dequeueReusableCellWithIdentifier:@"exp_profile_cell"];
            cell.tag = [[self.awd objectAtIndex:num][@"id"] integerValue];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            cell.title.text = [self.awd objectAtIndex:num][@"award_name"];
            cell.time.text = [self.awd objectAtIndex:num][@"date"];
            cell.sport.text = [self.awd objectAtIndex:num][@"sport"];
            if ([self.preferences getUserID] == self.playerUserID) {
                [cell.icon setHidden:NO];
            } else {
                [cell.icon setHidden:YES];
            }
            
            return cell;
        }
        
    } else if (self.listToDisplay == DISPLAY_MATCHES) {
        if (indexPath.row == 0) {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"support_title_cell" forIndexPath:indexPath];
            return cell;
        } else {
            if (self.data.count > 0) {
                SupportProfileTVCell *cell = [tableView dequeueReusableCellWithIdentifier:@"support_profile_cell" forIndexPath:indexPath];
                [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
                cell.name.text = [self.data objectAtIndex:indexPath.row-1][@"username"];
                
                NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%d votes",[[self.data objectAtIndex:indexPath.row-1][@"vote_num"] integerValue]]];
                [string addAttribute:NSForegroundColorAttributeName value:[UIColor goldColor] range:NSMakeRange(0,[NSString stringWithFormat:@"%d",[[self.data objectAtIndex:indexPath.row-1][@"vote_num"] integerValue]].length)];
                cell.votes.attributedText = string;
                
                cell.btn.tag = [[self.data objectAtIndex:indexPath.row-1][@"user_id"] integerValue];
                
                NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                        baseString:[self.data objectAtIndex:indexPath.row-1][@"profilePic"]];
                
                [cell.image setImageWithURL:[NSURL URLWithString:url]
                           placeholderImage:[UIImage imageNamed:@"avatar square"]];
                
                [UIImage makeRoundedImageNoBorder:cell.image withRadius:3];
                
                return cell;
            } else {
                ExperienceEmptyTVCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"empty_cell"];
                if (self.playerUserID == [self.preferences getUserID]) {
                    cell.emptycell.text = @"You don't have any supports yet.";
                } else {
                    cell.emptycell.text = [NSString stringWithFormat:@"%@ doesn't have any supports yet.", self.usernameLabel.text];
                }
                
                return cell;

            }
        }
        
    } else if (self.listToDisplay == DISPLAY_NEWSFEED) {
        
        if ([self.newsfeedPaginator.results count] == 0) {
            // no results
            NoResultTVC *cell;
            if (self.listToDisplay == DISPLAY_NEWSFEED) {
                cell = [tableView dequeueReusableCellWithIdentifier:@"No Results Cell Expand" forIndexPath:indexPath];
                cell.backgroundColor = [UIColor clearColor];
                UIImage *img = [UIImage imageNamed:@"no_newsfeed"];
                cell.icon.image = img;
                [cell.icon setContentMode:UIViewContentModeCenter];
                cell.headline.text = @"No News Feed Posts Yet";
                //cell.message.text = @"Press 'Compete' in V menu to create challenge or pickup matches.";
                cell.message.hidden = YES;
                cell.contentView.frame = CGRectMake(0, 0, self.tableView.frame.size.width, [self tableView:tableView heightForRowAtIndexPath:indexPath]);
            }/*else if (self.listToDisplay == DISPLAY_MATCHES) {
                cell = [tableView dequeueReusableCellWithIdentifier:@"No Results Cell Expand" forIndexPath:indexPath];
                cell.backgroundColor = [UIColor clearColor];
                UIImage *img = [UIImage imageNamed:@"no_match_history"];
                cell.icon.image = img;
                [cell.icon setContentMode:UIViewContentModeCenter];
                cell.headline.text = @"No Match History";
                //cell.message.text = @"Press 'Compete' in V menu to create challenge or pickup matches.";
                cell.message.hidden = YES;
                cell.contentView.frame = CGRectMake(0, 0, self.tableView.frame.size.width, [self tableView:tableView heightForRowAtIndexPath:indexPath]);
            }*/
            return cell;
        }
       if ([(self.newsfeedPaginator.results)[indexPath.section] isKindOfClass:[PickupMatchCreationNews class]]) {
            static NSString *CellIdentifier = @"Pickup Match News Cell";
            PickupMatchCreationNews *pickupMatchNews = (PickupMatchCreationNews *)(self.newsfeedPaginator.results)[indexPath.section];
            
            PDNewsPickupMatchPostCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
            
            // clear subviews
            if ([cell.contentView subviews]){
                for (UIView *subview in [cell.contentView subviews]) {
                    if ([subview isKindOfClass:[SpecialUIButton class]]) {
                        SpecialUIButton * newSubview = (SpecialUIButton *)subview;
                        if (newSubview.shouldRemoveFromMySuperview)  // only items I created programmatically
                            [subview removeFromSuperview];
                        
                    } else if ([subview isKindOfClass:[SpecialUILabel class]]) {
                        SpecialUILabel *newSubview = (SpecialUILabel *)subview;
                        if (newSubview.shouldRemoveFromMySuperview)
                            [subview removeFromSuperview];
                    }
                    
                }
            }
           [cell.actionButtonHandle addTarget:self action:@selector(postExtraActions:) forControlEvents:UIControlEventTouchUpInside];
           cell.actionButtonHandle.tag = indexPath.section;

            cell.sportIcon.image = [[pickupMatchNews.pickupMatch.sport getSportIcon2] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [cell.sportIcon setTintColor:[UIColor darkGrayColor2]];
           [cell.sportIconBackground makeRoundedBorderWithRadius:10];
           
            cell.sportLabel.text = [pickupMatchNews.pickupMatch.sport.sportName capitalizedString];
            
           
            cell.postTime.text = [NSDate timeDiffCalc:pickupMatchNews.newsDate];

            
            cell.matchName.text = pickupMatchNews.pickupMatch.matchName;
            
            
            if ([[pickupMatchNews.pickupMatch getMatchStatus] length] > 0) {
                cell.starIcon.hidden = NO;
                cell.messageLabel.text = [pickupMatchNews.pickupMatch getMatchStatus];
            } else {
                cell.messageLabel.text = @"";
                cell.starIcon.hidden = YES;
            }
            
      //      NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
      //                                              baseString:pickupMatchNews.profileBaseString];
                   cell.delegate = self;
            
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"MMM d"];
            
            NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
            [timeFormatter setDateFormat:@"h:mm a"];
            
            
            cell.monthLabel.text = [NSString stringWithFormat:@"%@", [dateFormatter stringFromDate:pickupMatchNews.pickupMatch.dateTime]];
            cell.timeLabel.text = [NSString stringWithFormat:@"%@", [timeFormatter stringFromDate:pickupMatchNews.pickupMatch.dateTime]];
            
            
            cell.venueLabel.text = pickupMatchNews.pickupMatch.venue.venueName;
            
            cell.detailButton.tag = indexPath.section;
           [cell.detailButton addTarget:self action:@selector(matchDetailsForPickupMatchCreation:) forControlEvents:UIControlEventTouchUpInside];
           
           UIButton *newsfeedIconButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
           [newsfeedIconButton addTarget:self action:@selector(matchDetailsForPickupMatchCreation:) forControlEvents:UIControlEventTouchUpInside];
           newsfeedIconButton.tag = indexPath.section;
           [cell.newsfeedIcon addSubview:newsfeedIconButton];
           cell.newsfeedIcon.userInteractionEnabled = YES;
           
            if ([pickupMatchNews.sharedByPlayer.userName length] > 0) {
                cell.sharedByLabel.text = [NSString stringWithFormat:@"Shared by %@", pickupMatchNews.sharedByPlayer.userName];
            } else {
                cell.sharedByLabel.text = [NSString stringWithFormat:@"Created by %@", pickupMatchNews.pickupMatch.matchCreatorUsername];
            }
            
         /*
            if ([pickupMatchNews.pickupMatch.experienceArray count] > 0)
                cell.experienceLabel.text = [NSString stringWithFormat:@"%@ Experience", [[pickupMatchNews.pickupMatch.experienceArray componentsJoinedByString:@", "] capitalizedString]];
            else
                cell.experienceLabel.text = @"Any Experience";
           */
           cell.competitors = [[NSMutableArray alloc] initWithArray:pickupMatchNews.pickupMatch.competitorArray];
           cell.matchCategory = @"pickup";
           [cell.matchHeadingCV reloadData];
           
           cell.currentUserID = self.playerUserID;
            
            cell.genderLabel.text = [pickupMatchNews.pickupMatch.gender capitalizedString];
            cell.genderIcon.image = [[Player getIconForGender:pickupMatchNews.pickupMatch.gender] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [cell.genderIcon setTintColor:[UIColor midGray]];
            
            
             /*************** SHOW MATCH HISTORY ********************/
       /*     NSInteger maxVisibleMatchHistory;
            
            if ([pickupMatchNews.pickupMatch.matchHistoryArray count] > 3)
                maxVisibleMatchHistory = 3;
            else
                maxVisibleMatchHistory = [pickupMatchNews.pickupMatch.matchHistoryArray count];
            
            NSInteger startPoint = 325;
            
            NSInteger myCounter = 0;
            for (MatchHistoryObj *matchHist in pickupMatchNews.pickupMatch.matchHistoryArray) {
                SpecialUILabel *updateLine = [[SpecialUILabel alloc] initWithFrame:CGRectMake(20, startPoint, 250, 15)];
                
                if ([matchHist.participant.userName isKindOfClass:[NSNull class]]) {
                    NSString *firstCapChar = [[matchHist.update substringToIndex:1] capitalizedString];
                    NSString *cappedString = [matchHist.update stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:firstCapChar];
                    updateLine.text = [NSString stringWithFormat:@"%@", cappedString];
                }   else
                    updateLine.text = [NSString stringWithFormat:@"%@ %@", matchHist.participant.userName, matchHist.update];
                
                updateLine.font = [UIFont systemFontOfSize:12];
                updateLine.textColor = [UIColor lightGrayColor];
                updateLine.shouldRemoveFromMySuperview = YES;
                [cell.contentView sendSubviewToBack:updateLine];
                [updateLine sizeToFit];
                
                [cell.contentView addSubview:updateLine];
                
                SpecialUILabel *timeLabel = [[SpecialUILabel alloc] initWithFrame:CGRectMake(updateLine.frame.size.width + 30, startPoint, 20, 15)];
                timeLabel.text = [NSDate timeDiffCalc:matchHist.occurrenceDate];
                timeLabel.font = [UIFont fontWithName:@"HelveticaNeue-LightItalic" size:12];
                timeLabel.textColor = [UIColor lightGrayColor];
                [timeLabel setTextAlignment:NSTextAlignmentLeft];
                [timeLabel sizeToFit];
                timeLabel.shouldRemoveFromMySuperview = YES;
                [cell.contentView sendSubviewToBack:timeLabel];
                [cell.contentView addSubview:timeLabel];
                
                startPoint += 20;
                myCounter ++;
                
                if (myCounter == maxVisibleMatchHistory)
                    break;
                
                
            }*/
            
            /*************** END SHOW MATCH HISTORY ********************/
            
         //   cell.contentView.frame = CGRectMake(0, 0, self.tableView.frame.size.width, [self tableView:tableView heightForRowAtIndexPath:indexPath]);
            
            
            // Show comment button
            
            [cell.commentButton addTarget:self action:@selector(commentPost:) forControlEvents:UIControlEventTouchUpInside];
            cell.commentButton.tag = indexPath.section;
            cell.commentNumberLabel.text = [NSString stringWithFormat:@"%lu comments", pickupMatchNews.numComments];
           
            
           
            cell.commentButtonIcon.image = [[UIImage imageNamed:@"Comment Small Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [cell.commentButtonIcon setTintColor:[UIColor darkGrayColor2]];
            
           
            [cell.mainBackgroundViewCell makeRoundedBorderWithRadius:3];
            cell.mainBackgroundViewCell.layer.borderColor = [UIColor midGray2].CGColor;
            cell.mainBackgroundViewCell.layer.borderWidth = 1.0;
           
           [cell.likeButton addTarget:self action:@selector(likePost:) forControlEvents:UIControlEventTouchUpInside];
           cell.likeButton.tag = indexPath.section;
           cell.likeNumberLabel.text = [NSString stringWithFormat:@"%lu props", pickupMatchNews.numLikes];
           
           cell.likeIcon.image = [[UIImage imageNamed:@"Like Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
           
           if (pickupMatchNews.doesSessionUserLikePost)
               [cell.likeIcon setTintColor:[UIColor goldColor]];
           else
               [cell.likeIcon setTintColor:[UIColor darkGrayColor2]];
           
           
           [cell.likeButtonBackground makeRoundedBorderWithRadius:3];
           cell.likeButtonBackground.layer.borderColor = [UIColor grayBorder1].CGColor;
           cell.likeButtonBackground.layer.borderWidth = 1.0;

           [cell.smallCircleView makeRoundedBorderWithRadius:2];
           cell.smallCircleView.backgroundColor = [UIColor darkGrayColor2];
           cell.commentNumberLabel.textColor = [UIColor darkGrayColor2];
           cell.likeNumberLabel.textColor = [UIColor darkGrayColor2];
           
           cell.viewLikesButton.tag = indexPath.section;
           cell.viewCommentsButton.tag = indexPath.section;
           
           [cell.viewLikesButton addTarget:self action:@selector(viewLikes:) forControlEvents:UIControlEventTouchUpInside];
           [cell.viewCommentsButton addTarget:self action:@selector(viewComments:) forControlEvents:UIControlEventTouchUpInside];
           
           
           
           return cell;
            
        } else  if ([(self.newsfeedPaginator.results)[indexPath.section] isKindOfClass:[IndividualMatchCreationNews class]]) {
            
            IndividualMatchCreationNews *individualMatchNews = (IndividualMatchCreationNews *)(self.newsfeedPaginator.results)[indexPath.section];
            NSString *CellIdentifier = @"";
            
                CellIdentifier = @"Individual Match Creation News Cell";
                
                PDNewsIndividualMatchPostCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
                
                // clear subviews
                if ([cell.contentView subviews]){
                    for (UIView *subview in [cell.contentView subviews]) {
                        if ([subview isKindOfClass:[SpecialUIButton class]]) {
                            SpecialUIButton * newSubview = (SpecialUIButton *)subview;
                            if (newSubview.shouldRemoveFromMySuperview)  // only items I created programmatically
                                [subview removeFromSuperview];
                            
                        } else if ([subview isKindOfClass:[SpecialUILabel class]]) {
                            SpecialUILabel *newSubview = (SpecialUILabel *)subview;
                            if (newSubview.shouldRemoveFromMySuperview)
                                [subview removeFromSuperview];
                        }
                        
                    }
                }
            [cell.actionButtonHandle addTarget:self action:@selector(postExtraActions:) forControlEvents:UIControlEventTouchUpInside];
            cell.actionButtonHandle.tag = indexPath.section;

            
                cell.postDateTimeLabel.text = [NSDate timeDiffCalc:individualMatchNews.newsDate];

                cell.matchNameLabel.text = individualMatchNews.individualMatch.matchName;
                
                cell.sportIcon.image = [[individualMatchNews.individualMatch.sport getSportIcon2] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                [cell.sportIcon setTintColor:[UIColor darkGrayColor2]];
            
            [cell.sportIconBackground makeRoundedBorderWithRadius:10];
            
          
                // Need to get the other player (not the match creator
                MatchPlayer *player1 = nil; // will set as match creator
                MatchPlayer *player2 = nil; // the other player
                
                if (individualMatchNews.individualMatch.player1.userID == individualMatchNews.individualMatch.matchCreatorID) {
                    player1 = individualMatchNews.individualMatch.player1;
                    player2 = individualMatchNews.individualMatch.player2;
                    
                } else {
                    player2 = individualMatchNews.individualMatch.player1;
                    player1 = individualMatchNews.individualMatch.player2;
                }
            
                if ([[individualMatchNews.individualMatch getMatchStatusMessage:[self.preferences getUserID]] length] > 0) {
                    cell.matchStatus.text = [individualMatchNews.individualMatch getMatchStatusMessage:[self.preferences getUserID]];
        //            cell.matchStatusIcon.hidden = NO;
                } else {
                    cell.matchStatus.text = @"";
        //            cell.matchStatusIcon.hidden = YES;
                }
                cell.userNameLabel1.text = player1.userName;
                cell.userNameLabel2.text = player2.userName;
                
                // username1  button
                UIButton *username1Button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 153, 125)];
                [username1Button addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
                username1Button.tag = player1.userID;
                [cell.userNameLabel1 addSubview:username1Button];
                cell.userNameLabel1.userInteractionEnabled = YES;
                // end username1 button
                
                // username2  button
                UIButton *username2Button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 153, 125)];
                [username2Button addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
                username2Button.tag = player2.userID;
                [cell.userNameLabel2 addSubview:username2Button];
                cell.userNameLabel2.userInteractionEnabled = YES;
                // end username2 button
            
            
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            
            // player 1 pic
            NSString *urlMain1 = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                         baseString:[MyHelpers getNewPicURLIfFacebookSquareSize:player1.profileBaseString]];
            
            [cell.userImage1 setImageWithURL:[NSURL URLWithString:urlMain1]
                            placeholderImage:[UIImage imageNamed:@"avatar square"]];
            
            cell.userImage1.contentMode = UIViewContentModeScaleAspectFill;
            cell.userImage1.clipsToBounds = YES;
            if (self.playerUserID == player1.userID) {
                cell.userImageButton1.hidden = YES;
            } else {
                cell.userImageButton1.hidden = NO;
                cell.userImageButton1.tag = player1.userID;
            }
            
            
            // profile pic button
       //     UIButton *picButton1 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 46, 46)];
       //     [picButton1 addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
       //     picButton1.tag = player1.userID;
       //     [cell.userImage1 addSubview:picButton1];
       //     cell.userImage1.userInteractionEnabled = YES;
            // end profile pic button
            
            
            // player2 pic
            NSString *urlMain2 = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                         baseString:[MyHelpers getNewPicURLIfFacebookSquareSize:player2.profileBaseString]];
            
            [cell.userImage2 setImageWithURL:[NSURL URLWithString:urlMain2]
                            placeholderImage:[UIImage imageNamed:@"avatar square"]];
            
            cell.userImage2.contentMode = UIViewContentModeScaleAspectFill;
            cell.userImage2.clipsToBounds = YES;
            if (self.playerUserID == player2.userID) {
                cell.userImageButton2.hidden = YES;
            } else {
                cell.userImageButton2.hidden = NO;
                cell.userImageButton2.tag = player2.userID;
            }
            
            [cell.vsCircleView makeRoundedBorderWithRadius:30];
     //       [cell.vsCircleView makeCircleWithColor:[UIColor goldColor] andRadius:30];

            
            // profile pic button
     //       UIButton *picButton2 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 46, 46)];
     //       [picButton2 addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
     //       picButton2.tag = player2.userID;
     //       [cell.userImage2 addSubview:picButton2];
     //       cell.userImage2.userInteractionEnabled = YES;
            // end profile pic button
            
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
            
                cell.levelUser1.text = [NSString stringWithFormat:@"%ld", [[player1 getLevelForSport:individualMatchNews.individualMatch.sport.sportID] integerValue]];
                cell.levelUser2.text = [NSString stringWithFormat:@"%ld", [[player2 getLevelForSport:individualMatchNews.individualMatch.sport.sportID] integerValue]];
                

                cell.detailButton.tag = indexPath.section;
            [cell.detailButton addTarget:self action:@selector(matchDetailsIndividualMatchCreation:) forControlEvents:UIControlEventTouchUpInside];
            
            UIButton *newsfeedIconButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
            [newsfeedIconButton addTarget:self action:@selector(matchDetailsIndividualMatchCreation:) forControlEvents:UIControlEventTouchUpInside];
            newsfeedIconButton.tag = indexPath.section;
            [cell.newsfeedIcon addSubview:newsfeedIconButton];
            cell.newsfeedIcon.userInteractionEnabled = YES;
            
          /*      if ([individualMatchNews.sharedByPlayer.userName length] > 0) {
                    cell.sharedByLabel.text = [NSString stringWithFormat:@"Shared by %@", individualMatchNews.sharedByPlayer.userName];
                } else {
                    cell.sharedByLabel.text = [NSString stringWithFormat:@"Created by %@", individualMatchNews.individualMatch.matchCreatorUsername];
                }*/
            
                /*************** SHOW MATCH HISTORY ********************/
            /*    NSInteger maxVisibleMatchHistory;
                
                if ([individualMatchNews.individualMatch.matchHistoryArray count] > 3)
                    maxVisibleMatchHistory = 3;
                else
                    maxVisibleMatchHistory = [individualMatchNews.individualMatch.matchHistoryArray count];
                
                NSInteger startPoint = 233;
                
                NSInteger myCounter = 0;
                for (MatchHistoryObj *matchHist in individualMatchNews.individualMatch.matchHistoryArray) {
                    SpecialUILabel *updateLine = [[SpecialUILabel alloc] initWithFrame:CGRectMake(20, startPoint, 250, 15)];
                    
                    if ([matchHist.participant.userName isKindOfClass:[NSNull class]]) {
                        NSString *firstCapChar = [[matchHist.update substringToIndex:1] capitalizedString];
                        NSString *cappedString = [matchHist.update stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:firstCapChar];
                        updateLine.text = [NSString stringWithFormat:@"%@", cappedString];
                    }   else
                        updateLine.text = [NSString stringWithFormat:@"%@ %@", matchHist.participant.userName, matchHist.update];
                    
                    updateLine.font = [UIFont systemFontOfSize:12];
                    updateLine.textColor = [UIColor lightGrayColor  ];
                    updateLine.shouldRemoveFromMySuperview = YES;
                    [updateLine sizeToFit];
                    
                    [cell.contentView addSubview:updateLine];
                    
                    SpecialUILabel *timeLabel = [[SpecialUILabel alloc] initWithFrame:CGRectMake(updateLine.frame.size.width + 30, startPoint, 20, 15)];
                    timeLabel.text = [NSDate timeDiffCalc:matchHist.occurrenceDate];
                    timeLabel.font = [UIFont fontWithName:@"HelveticaNeue-LightItalic" size:12];
                    timeLabel.textColor = [UIColor lightGrayColor];
                    [timeLabel setTextAlignment:NSTextAlignmentLeft];
                    [timeLabel sizeToFit];
                    timeLabel.shouldRemoveFromMySuperview = YES;
                    [cell.contentView addSubview:timeLabel];
                    
                    startPoint += 20;
                    myCounter ++;
                    
                    if (myCounter == maxVisibleMatchHistory)
                        break;
                    
                    
                }
                */
                /*************** END SHOW MATCH HISTORY ********************/
                
          //      cell.contentView.frame = CGRectMake(0, 0, self.tableView.frame.size.width, [self tableView:tableView heightForRowAtIndexPath:indexPath]);
                
                // Show comment button
            
                [cell.commentButton addTarget:self action:@selector(commentPost:) forControlEvents:UIControlEventTouchUpInside];
                cell.commentButton.tag = indexPath.section;
                cell.commentNumberLabel.text = [NSString stringWithFormat:@"%lu comments", individualMatchNews.numComments];
          /*      if (individualMatchNews.numComments == 1)
                    cell.commentTextLabel.text = @"Comment";
                else
                    cell.commentTextLabel.text = @"Comments";
            
                if (individualMatchNews.numRemixes == 1)
                    cell.shareTextLabel.text = @"Share";
                else
                    cell.shareTextLabel.text = @"Shares";
            */
            
          //      [cell.shareButton addTarget:self action:@selector(remixIndividualMatchPost:) forControlEvents:UIControlEventTouchUpInside];
          //      cell.shareButton.tag = indexPath.section;
          //      cell.shareNumberLabel.text = [NSString stringWithFormat:@"%lu", individualMatchNews.numRemixes];
          
            
     //       [cell.commentButtonView makeRoundedBorderWithRadius:3];
     //       [cell.shareButtonView makeRoundedBorderWithRadius:3];
     //       cell.commentButtonView.layer.borderColor = [UIColor grayBorder1].CGColor;
     //       cell.commentButtonView.layer.borderWidth = 1.0;
     //       cell.shareButtonView.layer.borderColor = [UIColor grayBorder1].CGColor;
     //       cell.shareButtonView.layer.borderWidth = 1.0;
            
            
            cell.commentButtonIcon.image = [[UIImage imageNamed:@"Comment Small Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [cell.commentButtonIcon setTintColor:[UIColor darkGrayColor2]];
            
     //       cell.shareButtonIcon.image = [[UIImage imageNamed:@"Remix Share Small Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
      //      [cell.shareButtonIcon setTintColor:[UIColor lightGrayColor]];
            
     //       cell.section2View.layer.borderColor = [UIColor midGray2].CGColor;
     //       cell.section2View.layer.borderWidth = 1.0;
            
            //  [cell.myBackgroundViewCell makeEdgyDropshadow:[UIColor darkGrayColor]];
            [cell.backgroundViewArea makeRoundedBorderWithRadius:3];
            cell.backgroundViewArea.layer.borderColor = [UIColor midGray2].CGColor;
            cell.backgroundViewArea.layer.borderWidth = 1.0;
            
            [cell.likeButton addTarget:self action:@selector(likePost:) forControlEvents:UIControlEventTouchUpInside];
            cell.likeButton.tag = indexPath.section;
            cell.likeNumberLabel.text = [NSString stringWithFormat:@"%lu props", individualMatchNews.numLikes];
            
            cell.likeIcon.image = [[UIImage imageNamed:@"Like Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            
            if (individualMatchNews.doesSessionUserLikePost)
                [cell.likeIcon setTintColor:[UIColor goldColor]];
            else
                [cell.likeIcon setTintColor:[UIColor darkGrayColor2]];
            
      //      [cell.likeButtonBackground makeRoundedBorderWithRadius:3];
      //      cell.likeButtonBackground.layer.borderColor = [UIColor grayBorder1].CGColor;
      //      cell.likeButtonBackground.layer.borderWidth = 1.0;

            [cell.smallCircleView makeRoundedBorderWithRadius:2];
            cell.smallCircleView.backgroundColor = [UIColor darkGrayColor2];
            cell.commentNumberLabel.textColor = [UIColor darkGrayColor2];
            cell.likeNumberLabel.textColor = [UIColor darkGrayColor2];
            
            cell.viewLikesButton.tag = indexPath.section;
            cell.viewCommentsButton.tag = indexPath.section;
            
            [cell.viewLikesButton addTarget:self action:@selector(viewLikes:) forControlEvents:UIControlEventTouchUpInside];
            [cell.viewCommentsButton addTarget:self action:@selector(viewComments:) forControlEvents:UIControlEventTouchUpInside];
            
                return cell;
                
          //  }
        } else if ([(self.newsfeedPaginator.results)[indexPath.section] isKindOfClass:[MatchPlayerNewsOfficialFinishedIndividualMatch class]]) {
            
            // Match is confirmed and can now display final results
            
            MatchPlayerNewsOfficialFinishedIndividualMatch *mpn = [self.newsfeedPaginator.results objectAtIndex:indexPath.section];
            IndividualMatch *iMatch = (IndividualMatch *)mpn.match;
            
            NSString *CellIdentifier = @"Match News Final Score Update Cell";
            PDMatchNewsFinalScoreUpdateCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
            
            // clear subviews
            if ([cell.contentView subviews]){
                for (UIView *subview in [cell.contentView subviews]) {
                    if ([subview isKindOfClass:[SpecialUIButton class]]) {
                        SpecialUIButton * newSubview = (SpecialUIButton *)subview;
                        if (newSubview.shouldRemoveFromMySuperview)  // only items I created programmatically
                            [subview removeFromSuperview];
                        
                    } else if ([subview isKindOfClass:[SpecialUILabel class]]) {
                        SpecialUILabel *newSubview = (SpecialUILabel *)subview;
                        if (newSubview.shouldRemoveFromMySuperview)
                            [subview removeFromSuperview];
                    }
                    
                }
            }
            [cell.actionButtonHandle addTarget:self action:@selector(postExtraActions:) forControlEvents:UIControlEventTouchUpInside];
            cell.actionButtonHandle.tag = indexPath.section;

            cell.iMatch = iMatch;
            
            cell.postDateTimeLabel.text = [NSDate timeDiffCalc:mpn.newsDate];
            
            cell.sportIcon.image = [[iMatch.sport getSportIcon2] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [cell.sportIcon setTintColor:[UIColor darkGrayColor2]];
            [cell.sportIconBackground makeRoundedBorderWithRadius:10];
            
           
            cell.matchNameLabel.text = iMatch.matchName;
            
            
            
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            
            // player 1 pic
            NSString *urlMain1 = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                         baseString:[MyHelpers getNewPicURLIfFacebookSquareSize:iMatch.player1.profileBaseString]];
            
            [cell.userImage1 setImageWithURL:[NSURL URLWithString:urlMain1]
                            placeholderImage:[UIImage imageNamed:@"avatar square"]];
            
            cell.userImage1.contentMode = UIViewContentModeScaleAspectFill;
            cell.userImage1.clipsToBounds = YES;
            if (self.playerUserID == iMatch.player1.userID) {
                cell.player1ImageButton.hidden = YES;
            } else {
                cell.player1ImageButton.hidden = NO;
                cell.player1ImageButton.tag = iMatch.player1.userID;
            }
            
       //     [UIImage makeRoundedImage:cell.userImage1 withRadius:25];
        //    [cell.userImage1.layer setBorderColor:[UIColor whiteColor].CGColor];
        //    [cell.userImage1.layer setBorderWidth:2.0f];
            
            
            
            // player2 pic
            NSString *urlMain2 = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                         baseString:[MyHelpers getNewPicURLIfFacebookSquareSize:iMatch.player2.profileBaseString]];
            
            [cell.userImage2 setImageWithURL:[NSURL URLWithString:urlMain2]
                            placeholderImage:[UIImage imageNamed:@"avatar square"]];
            
            cell.userImage2.contentMode = UIViewContentModeScaleAspectFill;
            cell.userImage2.clipsToBounds = YES;
            if (self.playerUserID == iMatch.player2.userID) {
                cell.player2ImageButton.hidden = YES;
            } else {
                cell.player2ImageButton.hidden = NO;
                cell.player2ImageButton.tag = iMatch.player2.userID;
            }
            
       //     [UIImage makeRoundedImage:cell.userImage2 withRadius:25];
        //    [cell.userImage2.layer setBorderColor:[UIColor whiteColor].CGColor];
        //    [cell.userImage2.layer setBorderWidth:2.0f];
            
            
            
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            
            
            [cell.mainBackgroundViewArea makeRoundedBorderWithRadius:3];
            cell.mainBackgroundViewArea.layer.borderColor = [UIColor midGray2].CGColor;
            cell.mainBackgroundViewArea.layer.borderWidth = 1.0;
     
            
            //    cell.dateTimeLabel.text = [NSDate timeDiffCalc:mpn.newsDate];
            
            
            [cell.vsCircleView makeCircleWithColor:[UIColor newBlueLight] andRadius:30];
            cell.player1Username.text = iMatch.player1.userName;
            cell.player2Username.text = iMatch.player2.userName;
            
            // username1  button
            UIButton *username1Button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 21)];
            [username1Button addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
            username1Button.tag = iMatch.player1.userID;
            [cell.player1Username addSubview:username1Button];
            cell.player1Username.userInteractionEnabled = YES;
            // end username1 button
            
            // username2  button
            UIButton *username2Button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 21)];
            [username2Button addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
            username2Button.tag = iMatch.player2.userID;
            [cell.player2Username addSubview:username2Button];
            cell.player2Username.userInteractionEnabled = YES;
            // end username2 button
            
            cell.player1UsernameV2.text = iMatch.player1.userName;
            cell.player2UsernameV2.text = iMatch.player2.userName;
            
            // username1  button
            UIButton *username1ButtonV2 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 21)];
            [username1ButtonV2 addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
            username1ButtonV2.tag = iMatch.player1.userID;
            [cell.player1UsernameV2 addSubview:username1ButtonV2];
            cell.player1UsernameV2.userInteractionEnabled = YES;
            // end username1 button
            
            // username2  button
            UIButton *username2ButtonV2 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 21)];
            [username2ButtonV2 addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
            username2ButtonV2.tag = iMatch.player2.userID;
            [cell.player2UsernameV2 addSubview:username2ButtonV2];
            cell.player2UsernameV2.userInteractionEnabled = YES;
            // end username2 button
            
            
            
            
        //    cell.sportLevelsHeader.text = [NSString stringWithFormat:@"New %@ Levels", [iMatch.sport.sportName capitalizedString]];
            //    Player *oppositePlayerOfPostingPlayer = [iMatch getOtherPlayer:mpn.player.userID];
            //    cell.scoreStatusLabel.text = [NSString stringWithFormat:@"%@ confirmed the score %@ entered", mpn.player.userName, oppositePlayerOfPostingPlayer.userName];
            
            //      cell.messageLabel.text = [NSString stringWithFormat:@"%@ confirmed the score", mpn.player.userName];
            cell.messageLabel.text = [iMatch getMatchStatusMessage:[self.preferences getUserID]];
        //    cell.messageLabel.textColor = [UIColor whiteColor];
            
            [cell.scoreCV reloadData];
            
            /*
            NSMutableDictionary *netScores = [iMatch.scoreIndMatch getNetScore];
            
            NSString *scoreUnitNamePlayer1 = @"";
            NSString *scoreUnitNamePlayer2 = @"";
            
            NSInteger player1Score = [[netScores objectForKey:@"player1_score"] integerValue];
            NSInteger player2Score = [[netScores objectForKey:@"player2_score"] integerValue];
            
            if (player1Score > 1 || player1Score == 0) {
                scoreUnitNamePlayer1 = [NSString stringWithFormat:@"%@s", [iMatch getScoreUnitValue]];
            } else {
                scoreUnitNamePlayer1 = [iMatch getScoreUnitValue];
            }
            
            if (player2Score > 1 || player2Score == 0) {
                scoreUnitNamePlayer2 = [NSString stringWithFormat:@"%@s", [iMatch getScoreUnitValue]];
            } else {
                scoreUnitNamePlayer2 = [iMatch getScoreUnitValue];
            }
            */
            //     cell.player1ScoreLabel.text = [NSString stringWithFormat:@"%d %@", player1Score, scoreUnitNamePlayer1];
            //     cell.player2ScoreLabel.text = [NSString stringWithFormat:@"%d %@", player2Score, scoreUnitNamePlayer2];
            /*
             
             if (player1Score > player2Score) {
             cell.winningPlayerUsername.text = iMatch.player1.userName;
             cell.losingPlayerUsername.text = iMatch.player2.userName;
             //       cell.numCoins.text = [NSString stringWithFormat:@"%d", mpn.coinTransferValue];
             
             
             
             } else if (player2Score > player1Score) {
             cell.winningPlayerUsername.text = iMatch.player2.userName;
             cell.losingPlayerUsername.text = iMatch.player1.userName;
             //        cell.numCoins.text = [NSString stringWithFormat:@"%d", mpn.coinTransferValue];
             
             
             } else { // tie
             cell.winningPlayerUsername.hidden = YES;
             cell.losingPlayerUsername.hidden = YES;
             cell.rightArrow.hidden = YES;
             //        cell.numCoins.text = @"Tie Score. No coins transferred.";
             
             }
             */
            //    cell.commentButton.tag = indexPath.section;
            //   cell.remixButton.tag = indexPath.section;
            
            if ([mpn.sharedByPlayer.userName length] > 0) {
                cell.sharedByLabel.text = [NSString stringWithFormat:@"Shared by %@", mpn.sharedByPlayer.userName];
            } else {
                cell.sharedByLabel.text = [NSString stringWithFormat:@"Created by %@", mpn.match.matchCreatorUsername];
            }
  
            cell.detailsButton.tag = indexPath.section;
            [cell.detailsButton addTarget:self action:@selector(matchDetailsIndividualMatchCreation:) forControlEvents:UIControlEventTouchUpInside];

            UIButton *newsfeedIconButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
            [newsfeedIconButton addTarget:self action:@selector(matchDetailsIndividualMatchCreation:) forControlEvents:UIControlEventTouchUpInside];
            newsfeedIconButton.tag = indexPath.section;
            [cell.newsfeedIcon addSubview:newsfeedIconButton];
            cell.newsfeedIcon.userInteractionEnabled = YES;
     
            /*************** END SHOW MATCH HISTORY ********************/
            
      //      cell.contentView.frame = CGRectMake(0, 0, self.tableView.frame.size.width, [self tableView:tableView heightForRowAtIndexPath:indexPath]);
            
            
            // Show comment button
            
         
            
            cell.commentButtonIcon.image = [[UIImage imageNamed:@"Comment Small Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [cell.commentButtonIcon setTintColor:[UIColor darkGrayColor2]];
            
       
            
            [cell.commentButton addTarget:self action:@selector(commentPost:) forControlEvents:UIControlEventTouchUpInside];
            cell.commentButton.tag = indexPath.section;
            cell.commentNumberLabel.text = [NSString stringWithFormat:@"%lu comments", mpn.numComments];
          /*  if (mpn.numComments == 1)
                cell.commentTextLabel.text = @"Comment";
            else
                cell.commentTextLabel.text = @"Comments";
            
            if (mpn.numRemixes == 1)
                cell.sharesTextLabel.text = @"Share";
            else
                cell.sharesTextLabel.text = @"Shares";
            
     */
        
            [cell.likeButton addTarget:self action:@selector(likePost:) forControlEvents:UIControlEventTouchUpInside];
            cell.likeButton.tag = indexPath.section;
            cell.likeNumberLabel.text = [NSString stringWithFormat:@"%lu props", mpn.numLikes];
            
            cell.likeIcon.image = [[UIImage imageNamed:@"Like Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            
            if (mpn.doesSessionUserLikePost)
                [cell.likeIcon setTintColor:[UIColor goldColor]];
            else
                [cell.likeIcon setTintColor:[UIColor darkGrayColor2]];
            
        
            
            [cell.smallCircleView makeRoundedBorderWithRadius:2];
            cell.smallCircleView.backgroundColor = [UIColor darkGrayColor2];
            cell.commentNumberLabel.textColor = [UIColor darkGrayColor2];
            cell.likeNumberLabel.textColor = [UIColor darkGrayColor2];
            
            cell.viewLikesButton.tag = indexPath.section;
            cell.viewCommentsButton.tag = indexPath.section;
            
            [cell.viewLikesButton addTarget:self action:@selector(viewLikes:) forControlEvents:UIControlEventTouchUpInside];
            [cell.viewCommentsButton addTarget:self action:@selector(viewComments:) forControlEvents:UIControlEventTouchUpInside];
            
            
            return cell;
            
            
        } else if ([(self.newsfeedPaginator.results)[indexPath.section] isKindOfClass:[ImagePost class]]) {
            
            NSString *CellIdentifier = @"Text Image Cell";
            
            PDImagePostCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
            
            // clear subviews
            if ([cell.contentView subviews]){
                for (UIView *subview in [cell.contentView subviews]) {
                    if ([subview isKindOfClass:[SpecialUIButton class]]) {
                        SpecialUIButton * newSubview = (SpecialUIButton *)subview;
                        if (newSubview.shouldRemoveFromMySuperview)  // only items I created programmatically
                            [subview removeFromSuperview];
                        
                    } else if ([subview isKindOfClass:[SpecialUILabel class]]) {
                        SpecialUILabel *newSubview = (SpecialUILabel *)subview;
                        if (newSubview.shouldRemoveFromMySuperview)
                            [subview removeFromSuperview];
                    }
                    
                }
            }
            
            
            ImagePost *iPost = (ImagePost *)(self.newsfeedPaginator.results)[indexPath.section];
            
            [cell.actionButtonHandle addTarget:self action:@selector(postExtraActions:) forControlEvents:UIControlEventTouchUpInside];
            cell.actionButtonHandle.tag = indexPath.section;

            cell.usernameLabel.text = iPost.newsMakerUserName;
            cell.messageLabel.text = [NSString stringWithFormat:@"posted a photo"];
            
            NSString *urlMain = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                        baseString:iPost.profileBaseString];
            
            [cell.profilePic setImageWithURL:[NSURL URLWithString:urlMain]
                            placeholderImage:[UIImage imageNamed:@"avatar square"] ];
            [UIImage makeRoundedImage:cell.profilePic withRadius:30];
            
            
            cell.profilePic.layer.borderColor = [UIColor whiteColor].CGColor;
            cell.profilePic.layer.borderWidth = 1.0;
            
            // profile pic button
            UIButton *picButton1 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];
            [picButton1 addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
            picButton1.tag = iPost.newsMakerUserID;
            [cell.profilePic addSubview:picButton1];
            cell.profilePic.userInteractionEnabled = YES;
            // end profile pic button
            
            
            NSString *urlPostPic = [S3Tools getFileNameStringWithType:@"photo"
                                                           baseString:iPost.imageBaseFileString];
            
            cell.imagePost.contentMode = UIViewContentModeScaleAspectFill;
            cell.imagePost.clipsToBounds = YES;
            
            [cell.imagePost setImageWithURL:[NSURL URLWithString:urlPostPic]
                           placeholderImage:[UIImage imageNamed:@"Landscape Placeholder"]]; /***************/
            
            cell.dateLabel.text = [NSDate timeDiffCalc:iPost.newsDate];
            
            cell.userTextPostLabel = [self makeDynamicHeighLabel:iPost.textPost forLabel:cell.userTextPostLabel];
            cell.userTextPostLabel.text = iPost.textPost;
            
            
            // adjust image starting point
            CGRect newFrame = cell.imagePost.frame;
            newFrame.origin.y = cell.userTextPostLabel.frame.origin.y + cell.userTextPostLabel.frame.size.height + 10;
            cell.imagePost.frame = newFrame;
            
            if ([iPost.sharedByPlayer.userName length] > 0) {
          //      cell.shareDot.hidden = NO;
          //      [cell.shareDot makeCircleWithColorAndBackground:[UIColor lightGrayColor] andRadius:3];
          //      cell.sharedByLabel.hidden = NO;
        //        cell.sharedByLabel.text = [NSString stringWithFormat:@"shared by %@", iPost.sharedByPlayer.userName];
                
            } else {
       //         cell.shareDot.hidden = YES;
          //      cell.sharedByLabel.hidden = YES;
            }
            
            [cell.mainBackgroundViewArea makeRoundedBorderWithRadius:3];
            cell.mainBackgroundViewArea.layer.borderColor = [UIColor midGray2].CGColor;
            cell.mainBackgroundViewArea.layer.borderWidth = 1.0;
         
            
            cell.contentView.frame = CGRectMake(0, 0, self.tableView.frame.size.width, [self tableView:tableView heightForRowAtIndexPath:indexPath]);
            
            
            // Show comment button
            
            cell.commentButtonIcon.image = [[UIImage imageNamed:@"Comment Small Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [cell.commentButtonIcon setTintColor:[UIColor lightGrayColor]];
    
            [cell.commentButton addTarget:self action:@selector(commentPost:) forControlEvents:UIControlEventTouchUpInside];
            cell.commentButton.tag = indexPath.section;
            cell.commentNumberLabel.text = [NSString stringWithFormat:@"%lu", iPost.numComments];
            
            [cell.likeButton addTarget:self action:@selector(likePost:) forControlEvents:UIControlEventTouchUpInside];
            cell.likeButton.tag = indexPath.section;
            cell.likeNumberLabel.text = [NSString stringWithFormat:@"%lu", iPost.numLikes];
            
            cell.likeIcon.image = [[UIImage imageNamed:@"Like Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            
            if (iPost.doesSessionUserLikePost)
                [cell.likeIcon setTintColor:[UIColor salmonColor]];
            else
                [cell.likeIcon setTintColor:[UIColor lightGrayColor]];
         
            
            
            
            return cell;
            
        } else if ([(self.newsfeedPaginator.results)[indexPath.section] isKindOfClass:[LockerPost class]]) {
            
            NSString *CellIdentifier = @"Text Image Cell";
            
            PDImagePostCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
            
            // clear subviews
            if ([cell.contentView subviews]){
                for (UIView *subview in [cell.contentView subviews]) {
                    if ([subview isKindOfClass:[SpecialUIButton class]]) {
                        SpecialUIButton * newSubview = (SpecialUIButton *)subview;
                        if (newSubview.shouldRemoveFromMySuperview)  // only items I created programmatically
                            [subview removeFromSuperview];
                        
                    } else if ([subview isKindOfClass:[SpecialUILabel class]]) {
                        SpecialUILabel *newSubview = (SpecialUILabel *)subview;
                        if (newSubview.shouldRemoveFromMySuperview)
                            [subview removeFromSuperview];
                    }
                    
                }
            }
            
            LockerPost *iPost = (LockerPost *)(self.newsfeedPaginator.results)[indexPath.section];
            
            
            [cell.actionButtonHandle addTarget:self action:@selector(postExtraActions:) forControlEvents:UIControlEventTouchUpInside];
            cell.actionButtonHandle.tag = indexPath.section;

            [cell.mainBackgroundViewArea makeRoundedBorderWithRadius:6];
            
            cell.usernameLabel.text = iPost.newsMakerUserName;
            
            NSString *urlMain = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                        baseString:iPost.profileBaseString];
            
            cell.profilePic.hidden = YES;
            [cell.profilePic setImageWithURL:[NSURL URLWithString:urlMain]
                            placeholderImage:[UIImage imageNamed:@"avatar square"] ];
            [UIImage makeRoundedImageNoBorder:cell.profilePic withRadius:5];
            
            // profile pic button
            UIButton *picButton1 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];
            [picButton1 addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
            picButton1.tag = iPost.newsMakerUserID;
            [cell.profilePic addSubview:picButton1];
            cell.profilePic.userInteractionEnabled = YES;
            // end profile pic button
            
            cell.profilePic.hidden = NO;
            
            NSString *urlPostPic = [S3Tools2 getFileNameStringWithType:@"photo_regsize" baseString:[NSString stringWithFormat:@"%d/photo/%@", iPost.newsMakerUserID, iPost.lockPic.imageFileName]];
            
            cell.imagePost.contentMode = UIViewContentModeScaleAspectFill;
            cell.imagePost.clipsToBounds = YES;
            
            [cell.imagePost setImageWithURL:[NSURL URLWithString:urlPostPic] placeholderImage:[UIImage imageNamed:@"Landscape Placeholder"]];
            
            cell.dateLabel.text = [NSDate timeDiffCalc:iPost.newsDate];
            
            cell.userTextPostLabel = [self makeDynamicHeighLabel:iPost.textPost forLabel:cell.userTextPostLabel];
            cell.userTextPostLabel.text = iPost.textPost;
            
            cell.contentView.frame = CGRectMake(0, 0, self.tableView.frame.size.width, [self tableView:tableView heightForRowAtIndexPath:indexPath]);
            
            
            // Show comment button
            cell.commentButtonIcon.image = [[UIImage imageNamed:@"Comment Small Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [cell.commentButtonIcon setTintColor:[UIColor darkGrayColor2]];
          
            
            
            [cell.commentButton addTarget:self action:@selector(commentPost:) forControlEvents:UIControlEventTouchUpInside];
            cell.commentButton.tag = indexPath.section;
            cell.commentNumberLabel.text = [NSString stringWithFormat:@"%d comments", iPost.numComments];
            cell.commentNumberLabel2.text = [NSString stringWithFormat:@"%d", iPost.numComments];
           
            
            [cell.likeButton addTarget:self action:@selector(likePost:) forControlEvents:UIControlEventTouchUpInside];
            cell.likeButton.tag = indexPath.section;
            cell.likeNumberLabel.text = [NSString stringWithFormat:@"%d props", iPost.numLikes];
            cell.likeNumberLabel2.text = [NSString stringWithFormat:@"%d", iPost.numLikes];
            cell.likeIcon.image = [[UIImage imageNamed:@"Like Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            
            if (iPost.doesSessionUserLikePost)
                [cell.likeIcon setTintColor:[UIColor goldColor]];
            else
                [cell.likeIcon setTintColor:[UIColor darkGrayColor2]];
            
           
            [cell.smallCircleView makeRoundedBorderWithRadius:2];
            cell.smallCircleView.backgroundColor = [UIColor darkGrayColor2];
            cell.commentNumberLabel.textColor = [UIColor darkGrayColor2];
            cell.likeNumberLabel.textColor = [UIColor darkGrayColor2];
            
            if ([iPost.lockPic.statsGrp.statsArray count] > 0) {
                cell.statBackgroundView.hidden = NO;
                [cell.value1 setHidden:NO];
                [cell.desc1 setHidden:NO];
                if ([iPost.lockPic.statsGrp.statsArray count] == 1) {
                    [cell.value2 setHidden:YES];
                    [cell.value3 setHidden:YES];
                    [cell.desc2 setHidden:YES];
                    [cell.desc3 setHidden:YES];
                    [cell.sep1 setHidden:YES];
                    [cell.sep2 setHidden:YES];
                    [cell.statIcon setHidden:YES];
                    [cell.line1 setHidden:NO];
                    [cell.line2 setHidden:NO];
                    [cell.drills setHidden:YES];
                    [cell.titleconst1 setConstant: 8 + 97];
                    [cell.descconst1 setConstant: 10 + 95];
                } else if ([iPost.lockPic.statsGrp.statsArray count] == 2) {
                    [cell.value2 setHidden:NO];
                    [cell.value3 setHidden:YES];
                    [cell.desc2 setHidden:NO];
                    [cell.desc3 setHidden:YES];
                    [cell.sep1 setHidden:NO];
                    [cell.sep2 setHidden:YES];
                    [cell.statIcon setHidden:YES];
                    [cell.line1 setHidden:YES];
                    [cell.line2 setHidden:YES];
                    [cell.drills setHidden:YES];
                    [cell.titleconst1 setConstant: 8 + 25];
                    [cell.descconst1 setConstant: 10 + 25];
                    [cell.descconst2 setConstant: 92 + 70];
                    [cell.titleconst2 setConstant: 92 + 70];
                    [cell.sepconst1 setConstant: 89 + 45];
                } else if ([iPost.lockPic.statsGrp.statsArray count] == 3) {
                    [cell.value2 setHidden:NO];
                    [cell.value3 setHidden:NO];
                    [cell.desc2 setHidden:NO];
                    [cell.desc3 setHidden:NO];
                    [cell.sep1 setHidden:NO];
                    [cell.sep2 setHidden:NO];
                    [cell.statIcon setHidden:YES];
                    [cell.line1 setHidden:YES];
                    [cell.line2 setHidden:YES];
                    [cell.drills setHidden:YES];
                    [cell.titleconst1 setConstant: 8];
                    [cell.descconst1 setConstant: 10];
                    [cell.descconst2 setConstant: 92];
                    [cell.titleconst2 setConstant: 92];
                    [cell.sepconst1 setConstant: 89];
                }
                
                int counter = 0;
                
                for (id obj in iPost.lockPic.statsGrp.statsArray) {
                    
                    if ([obj isKindOfClass:[StatTag class]]) {
                        
                        StatTag *stat = (StatTag *)obj;
                        
                        switch (counter) {
                            case 0:
                                cell.value1.text = [NSString stringWithFormat:@"%@", stat.statTagValue];
                                if (stat.showSubCategory) {
                                    cell.desc1.text = [NSString stringWithFormat:@"%@ %@", stat.statLink, stat.statTagName];
                                } else {
                                    cell.desc1.text = stat.statTagName;
                                }
                                break;
                                
                            case 1:
                                cell.value2.text = [NSString stringWithFormat:@"%@", stat.statTagValue];
                                if (stat.showSubCategory) {
                                    cell.desc2.text = [NSString stringWithFormat:@"%@ %@", stat.statLink, stat.statTagName];
                                } else {
                                    cell.desc2.text = stat.statTagName;
                                }
                                break;
                            case 2:
                                cell.value3.text = [NSString stringWithFormat:@"%@", stat.statTagValue];
                                if (stat.showSubCategory) {
                                    cell.desc3.text = [NSString stringWithFormat:@"%@ %@", stat.statLink, stat.statTagName];
                                } else
                                    cell.desc3.text = stat.statTagName;
                                break;
                            default:
                                break;
                        }
                    } else if ([obj isKindOfClass:[StatTagCombination class]]) {
                        StatTagCombination *sta = (StatTagCombination *)obj;
                        
                        switch (counter) {
                            case 0:
                                cell.value1.text = [NSString stringWithFormat:@"%@ / %@", sta.numerator.statTagValue, sta.denominator.statTagValue];
                                cell.desc1.text = [NSString stringWithFormat:@"%@", sta.numerator.statLink];
                                break;
                            case 1:
                                cell.value2.text = [NSString stringWithFormat:@"%@ / %@", sta.numerator.statTagValue, sta.denominator.statTagValue];
                                cell.desc2.text = [NSString stringWithFormat:@"%@", sta.numerator.statLink];
                                break;
                            case 2:
                                cell.value3.text = [NSString stringWithFormat:@"%@ / %@", sta.numerator.statTagValue, sta.denominator.statTagValue];
                                cell.desc3.text = [NSString stringWithFormat:@"%@", sta.numerator.statLink];
                                break;
                            default:
                                break;
                        }
                        
                    }
                    counter ++;
                }
            } else if ([iPost.lockPic.trainingTagsArray count] > 0) {
                cell.statBackgroundView.hidden = NO;
                
                NSString *otherTrainingText = @"";
                
                for (TrainingTag *tag in iPost.lockPic.trainingTagsArray) {
                    if ([otherTrainingText length] > 0)
                        otherTrainingText = [otherTrainingText stringByAppendingString:@", "];
                    
                    otherTrainingText = [otherTrainingText stringByAppendingString:tag.trainingTagName];
                }
                
                if ([otherTrainingText length] > 0){
                    cell.drills.text = [NSString stringWithFormat:@"%@", otherTrainingText];
                } else
                    cell.drills.hidden = YES;
                
                [cell.statIcon setHidden:NO];
                cell.statIcon.image = [UIImage imageNamed:@"white_drill"];
                [cell.statBackgroundView bringSubviewToFront:cell.statIcon];
                //[cell.statIcon setTintColor:[UIColor whiteColor]];
                
                [cell.value1 setHidden:YES];
                [cell.value2 setHidden:YES];
                [cell.value3 setHidden:YES];
                [cell.desc1 setHidden:YES];
                [cell.desc2 setHidden:YES];
                [cell.desc3 setHidden:YES];
                [cell.sep1 setHidden:YES];
                [cell.sep2 setHidden:YES];
                [cell.line1 setHidden:YES];
                [cell.line2 setHidden:YES];
                [cell.drills setHidden:NO];
            } else {
                cell.statBackgroundView.hidden = YES;
                [cell.statIcon setHidden:YES];
                [cell.value1 setHidden:YES];
                [cell.value2 setHidden:YES];
                [cell.value3 setHidden:YES];
                [cell.desc1 setHidden:YES];
                [cell.desc2 setHidden:YES];
                [cell.desc3 setHidden:YES];
                [cell.sep1 setHidden:YES];
                [cell.sep2 setHidden:YES];
                [cell.line1 setHidden:YES];
                [cell.line2 setHidden:YES];
                [cell.drills setHidden:YES];
            }
            
            cell.viewLikesButton.tag = indexPath.section;
            cell.viewCommentsButton.tag = indexPath.section;
            cell.lockerButton.tag = iPost.lockPic.lockerID;
            
            [cell.viewLikesButton addTarget:self action:@selector(viewLikes:) forControlEvents:UIControlEventTouchUpInside];
            [cell.viewCommentsButton addTarget:self action:@selector(viewComments:) forControlEvents:UIControlEventTouchUpInside];
            [cell.lockerButton addTarget:self action:@selector(viewLockerImage:) forControlEvents:UIControlEventTouchUpInside];
            

            /*UIButton *newsfeedIconButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
            [newsfeedIconButton addTarget:self action:@selector(viewLockerImage:) forControlEvents:UIControlEventTouchUpInside];
            newsfeedIconButton.tag = iPost.lockPic.lockerID;
            [cell.newsfeedIcon addSubview:newsfeedIconButton];
            cell.newsfeedIcon.userInteractionEnabled = YES;*/
            
            
            return cell;
            
        } else if ([(self.newsfeedPaginator.results)[indexPath.section] isKindOfClass:[TextPost class]]) {
            NSString *CellIdentifier  = @"User Text Post Cell";
            TextPostCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
            
            // clear subviews
            if ([cell.contentView subviews]){
                for (UIView *subview in [cell.contentView subviews]) {
                    if ([subview isKindOfClass:[SpecialUIButton class]]) {
                        SpecialUIButton * newSubview = (SpecialUIButton *)subview;
                        if (newSubview.shouldRemoveFromMySuperview)  // only items I created programmatically
                            [subview removeFromSuperview];
                        
                    } else if ([subview isKindOfClass:[SpecialUILabel class]]) {
                        SpecialUILabel *newSubview = (SpecialUILabel *)subview;
                        if (newSubview.shouldRemoveFromMySuperview)
                            [subview removeFromSuperview];
                    }
                    
                }
            }
            
            TextPost *nPost = (TextPost *)(self.newsfeedPaginator.results)[indexPath.section];
            
            [cell.actionButtonHandle addTarget:self action:@selector(postExtraActions:) forControlEvents:UIControlEventTouchUpInside];
            cell.actionButtonHandle.tag = indexPath.section;
            
            cell.usernameLabel.text = nPost.newsMakerUserName;
            
            [cell.sectionMainView makeRoundedBorderWithRadius:6];
            
            /*if ([nPost.sharedByPlayer.userName length] > 0) {
             cell.sharedByLabel.hidden = NO;
             cell.sharedByLabel.text = [NSString stringWithFormat:@"shared by %@", nPost.sharedByPlayer.userName];
             
             } else {
             cell.sharedByLabel.hidden = YES;
             }*/
            [cell.actionSheetHandler addTarget:self action:@selector(postExtraActions:) forControlEvents:UIControlEventTouchUpInside];
            cell.actionSheetHandler.tag = indexPath.section;
            
            cell.propsButton.tag = indexPath.section;
            cell.commentButtonLeft.tag = indexPath.section;
            //cell.viewLockerImageButton.tag = iPost.lockPic.lockerID;
            
            [cell.propsButton addTarget:self action:@selector(viewLikes:) forControlEvents:UIControlEventTouchUpInside];
            [cell.commentButtonLeft addTarget:self action:@selector(viewComments:) forControlEvents:UIControlEventTouchUpInside];
            
            NSString *urlMain = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                        baseString:nPost.profileBaseString];
            
            [cell.profilePic setImageWithURL:[NSURL URLWithString:urlMain]
                            placeholderImage:[UIImage imageNamed:@"avatar square"] ];
            [UIImage makeRoundedImageNoBorder:cell.profilePic withRadius:5];
            
            // profile pic button
            UIButton *picButton1 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];
            [picButton1 addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
            picButton1.tag = nPost.newsMakerUserID;
            [cell.profilePic addSubview:picButton1];
            cell.profilePic.userInteractionEnabled = YES;
            // end profile pic button
            
            
            cell.dateTimeLabel.text = [NSDate timeDiffCalc:nPost.newsDate];
            
            cell.userTextPostLabel = [self makeDynamicHeighLabel:nPost.textPost forLabel:cell.userTextPostLabel];
            cell.userTextPostLabel.text = nPost.textPost;
            
            cell.commentButtonIcon.image = [[UIImage imageNamed:@"Comment Small Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [cell.commentButtonIcon setTintColor:[UIColor darkGrayColor2]];
            
            cell.likeIcon.image = [[UIImage imageNamed:@"Like Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            if (nPost.doesSessionUserLikePost)
                [cell.likeIcon setTintColor:[UIColor goldColor]];
            else
                [cell.likeIcon setTintColor:[UIColor darkGrayColor2]];
            
            [cell.commentButton addTarget:self action:@selector(commentPost:) forControlEvents:UIControlEventTouchUpInside];
            cell.commentButton.tag = indexPath.section;
            cell.commentNumberLabel.text = [NSString stringWithFormat:@"%d comments", nPost.numComments];
            cell.commentNumberLabel2.text = [NSString stringWithFormat:@"%d", nPost.numComments];
            if (nPost.numComments == 1)
                cell.commentTextLabel.text = @"Comment";
            else
                cell.commentTextLabel.text = @"Comments";
            
            /*if (nPost.numRemixes == 1)
             cell.shareTextLabel.text = @"Share";
             else
             cell.shareTextLabel.text = @"Shares";*/
            
            [cell.smallCircle makeRoundedBorderWithRadius:2];
            cell.smallCircle.backgroundColor = [UIColor darkGrayColor2];
            cell.commentNumberLabel.textColor = [UIColor darkGrayColor2];
            cell.likeNumberLabel.textColor = [UIColor darkGrayColor2];
            
            [cell.likeButton addTarget:self action:@selector(likePost:) forControlEvents:UIControlEventTouchUpInside];
            cell.likeButton.tag = indexPath.section;
            cell.likeNumberLabel.text = [NSString stringWithFormat:@"%d props", nPost.numLikes];
            cell.likeNumberLabel2.text = [NSString stringWithFormat:@"%d", nPost.numLikes];
            
            
            cell.contentView.frame = CGRectMake(0, 0, self.tableView.frame.size.width, [self tableView:tableView heightForRowAtIndexPath:indexPath]);
            
            return cell;
            
            
        } else if ([(self.newsfeedPaginator.results)[indexPath.section] isKindOfClass:[CompetitionNews class]]) {
            NSString *CellIdentifier  = @"Competition Cell";
            PDCompetitionPostCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
            
            // clear subviews
            if ([cell.contentView subviews]){
                for (UIView *subview in [cell.contentView subviews]) {
                    if ([subview isKindOfClass:[SpecialUIButton class]]) {
                        SpecialUIButton * newSubview = (SpecialUIButton *)subview;
                        if (newSubview.shouldRemoveFromMySuperview)  // only items I created programmatically
                            [subview removeFromSuperview];
                        
                    } else if ([subview isKindOfClass:[SpecialUILabel class]]) {
                        SpecialUILabel *newSubview = (SpecialUILabel *)subview;
                        if (newSubview.shouldRemoveFromMySuperview)
                            [subview removeFromSuperview];
                    }
                    
                }
            }
            [cell.actionButtonHandle addTarget:self action:@selector(postExtraActions:) forControlEvents:UIControlEventTouchUpInside];
            cell.actionButtonHandle.tag = indexPath.section;

            
            CompetitionNews *cPost = (CompetitionNews *)(self.newsfeedPaginator.results)[indexPath.section];
            
            cell.sportIcon.image = [[UIImage imageNamed:@"U Bar Button Logo"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [cell.sportIcon setTintColor:[UIColor whiteColor]];
            
            cell.usernameLabel.text = cPost.newsMakerUserName;
            
            if ([cPost.sharedByPlayer.userName length] > 0) {
                cell.sharedByLabel.hidden = NO;
                cell.sharedByLabel.text = [NSString stringWithFormat:@"shared by %@", cPost.sharedByPlayer.userName];
                
            } else {
                cell.sharedByLabel.hidden = YES;
            }
            
            NSString *urlMain = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                        baseString:cPost.profileBaseString];
            
            [cell.profilePic setImageWithURL:[NSURL URLWithString:urlMain]
                            placeholderImage:[UIImage imageNamed:@"avatar square"]];
            [UIImage makeRoundedImage:cell.profilePic withRadius:30];
            
            
            cell.profilePic.layer.borderColor = [UIColor whiteColor].CGColor;
            cell.profilePic.layer.borderWidth = 1.0;
            
            // profile pic button
            UIButton *picButton1 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];
            [picButton1 addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
            picButton1.tag = cPost.newsMakerUserID;
            [cell.profilePic addSubview:picButton1];
            cell.profilePic.userInteractionEnabled = YES;
            // end profile pic button
            
            
            cell.postDateLabel.text = [NSDate timeDiffCalc:cPost.newsDate];
            
            [cell.mainBackgroundViewArea makeRoundedBorderWithRadius:3];
            cell.mainBackgroundViewArea.layer.borderColor = [UIColor midGray2].CGColor;
            cell.mainBackgroundViewArea.layer.borderWidth = 1.0;
            
            //       CGFloat cellHeight = [self calculateHeightForTextPost:indexPath.section];
            
         //   cell.contentView.frame = CGRectMake(0, 0, self.tableView.frame.size.width, [self tableView:tableView heightForRowAtIndexPath:indexPath]);
            
            if ([cPost.universityOrganization length] > 0) {
                cell.competitionDetailTextLabel.text = [NSString stringWithFormat:@"@%@ is playing in the Vaiden Games and is representing the %@ group of %@", cPost.newsMakerUserName, cPost.universityOrganization, cPost.universityName];
            } else {
                cell.competitionDetailTextLabel.text = [NSString stringWithFormat:@"@%@ is playing in the Vaiden Games and is representing %@",cPost.newsMakerUserName, cPost.universityName];
            }
            
            [cell.commentButton addTarget:self action:@selector(commentPost:) forControlEvents:UIControlEventTouchUpInside];
            cell.commentButton.tag = indexPath.section;
            cell.numCommentLabel.text = [NSString stringWithFormat:@"%lu", cPost.numComments];
            
            [cell.shareButton addTarget:self action:@selector(remixCompetitionPost:) forControlEvents:UIControlEventTouchUpInside];
            cell.shareButton.tag = indexPath.section;
            cell.numShareLabel.text = [NSString stringWithFormat:@"%lu", cPost.numRemixes];
            
            [cell.commentButtonBackgroundView makeRoundedBorderWithRadius:3];
            [cell.shareButtonBackgroundView makeRoundedBorderWithRadius:3];
            cell.commentButtonBackgroundView.layer.borderColor = [UIColor grayBorder1].CGColor;
            cell.commentButtonBackgroundView.layer.borderWidth = 1.0;
            cell.shareButtonBackgroundView.layer.borderColor = [UIColor grayBorder1].CGColor;
            cell.shareButtonBackgroundView.layer.borderWidth = 1.0;
            
            cell.commentButtonIcon.image = [[UIImage imageNamed:@"Comment Small Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [cell.commentButtonIcon setTintColor:[UIColor lightGrayColor]];
            
            cell.shareButtonIcon.image = [[UIImage imageNamed:@"Remix Share Small Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [cell.shareButtonIcon setTintColor:[UIColor lightGrayColor]];

            [cell.likeButton addTarget:self action:@selector(likePost:) forControlEvents:UIControlEventTouchUpInside];
            cell.likeButton.tag = indexPath.section;
            cell.likeNumberLabel.text = [NSString stringWithFormat:@"%lu", cPost.numLikes];
            
            cell.likeIcon.image = [[UIImage imageNamed:@"Like Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            
            if (cPost.doesSessionUserLikePost)
                [cell.likeIcon setTintColor:[UIColor salmonColor]];
            else
                [cell.likeIcon setTintColor:[UIColor lightGrayColor]];
            
            [cell.likeButtonBackground makeRoundedBorderWithRadius:3];
            cell.likeButtonBackground.layer.borderColor = [UIColor grayBorder1].CGColor;
            cell.likeButtonBackground.layer.borderWidth = 1.0;

            
            return cell;
            
            
        } else if ([(self.newsfeedPaginator.results)[indexPath.section] isKindOfClass:[VenueKingNews class]]) {
            
            PDVenueKingCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Venue King Cell" forIndexPath:indexPath];
            
            // clear subviews
            if ([cell.contentView subviews]){
                for (UIView *subview in [cell.contentView subviews]) {
                    if ([subview isKindOfClass:[SpecialUIButton class]]) {
                        SpecialUIButton * newSubview = (SpecialUIButton *)subview;
                        if (newSubview.shouldRemoveFromMySuperview)  // only items I created programmatically
                            [subview removeFromSuperview];
                        
                    } else if ([subview isKindOfClass:[SpecialUILabel class]]) {
                        SpecialUILabel *newSubview = (SpecialUILabel *)subview;
                        if (newSubview.shouldRemoveFromMySuperview)
                            [subview removeFromSuperview];
                    }
                    
                }
            }
            [cell.actionButtonHandle addTarget:self action:@selector(postExtraActions:) forControlEvents:UIControlEventTouchUpInside];
            cell.actionButtonHandle.tag = indexPath.section;

            
            VenueKingNews *vPost = (VenueKingNews *)(self.newsfeedPaginator.results)[indexPath.section];
            
            [cell.mainCellBackgroundView makeRoundedBorderWithRadius:3];
            cell.mainCellBackgroundView.layer.borderColor = [UIColor midGray2].CGColor;
            cell.mainCellBackgroundView.layer.borderWidth = 1.0;
     
            
            
            NSString *kingString = [NSString stringWithFormat:@"%@ became King of the Venue for %@ at", vPost.newsMakerUserName, [vPost.sport.sportName capitalizedString]];
            
            NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ %@", kingString, vPost.venue.venueName]];
            
            [string addAttribute:NSForegroundColorAttributeName value:[UIColor goldColor] range:NSMakeRange([kingString length] + 1, [vPost.venue.venueName length])];
            cell.messageLabel.attributedText = string;
            
          //  cell.venueNameLabel.text = vPost.venue.venueName;
          //  cell.usernameLabel.text = vPost.newsMakerUserName;

            [cell.venueButton addTarget:self action:@selector(goToVenue:) forControlEvents:UIControlEventTouchUpInside];
            cell.venueButton.tag = vPost.venue.venueID;
 
            NSString *urlMain = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                        baseString:vPost.profileBaseString];
            
            [cell.profilePic setImageWithURL:[NSURL URLWithString:urlMain]
                            placeholderImage:[UIImage imageNamed:@"avatar square"]];
            //                   placeholderImage:[[UIImage imageNamed:@"avatar round"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]];
            
            [UIImage makeRoundedImage:cell.profilePic withRadius:10];
            cell.profilePic.layer.borderColor = [UIColor clearColor].CGColor;
            
            if ([vPost.profileBaseString length] == 0) {
                [cell.profilePic.layer setBorderWidth:2.0f];
                [cell.profilePic.layer setBorderColor:[UIColor lightGrayColor].CGColor];
                [cell.profilePic setTintColor:[UIColor lightGrayColor]];
            } else {
                [cell.profilePic setTintColor:[UIColor clearColor]];
                cell.profilePic.layer.borderColor = [UIColor whiteColor].CGColor;
                cell.profilePic.layer.borderWidth = 1.0;
            }
            cell.profilePic.hidden = NO;
            
            // profile pic button
            UIButton *picButton1 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];
            [picButton1 addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
            picButton1.tag = vPost.newsMakerUserID;
            [cell.profilePic addSubview:picButton1];
            cell.profilePic.userInteractionEnabled = YES;
            // end profile pic button
            
    /*        // venue pic
            NSString *venueURLMain = [S3Tools getFileNameStringWithType:@"venue_pic"
                                                             baseString:vPost.venue.venueImageThumb.venuePicString];
            
            
            [cell.venuePic setImageWithURL:[NSURL URLWithString:venueURLMain]
                            placeholderImage:[UIImage imageNamed:@"Venue Default"]];
            
            cell.venuePic.contentMode = UIViewContentModeScaleAspectFill;
            cell.venuePic.clipsToBounds = YES;
*/
            
            
            cell.postDateTimeLabel.text = [NSDate timeDiffCalc:vPost.newsDate];
            
       //     if ([vPost.sharedByPlayer.userName length] > 0) {
      //          cell.sharedByLabel.hidden = NO;
       //         cell.sharedByLabel.text = [NSString stringWithFormat:@"shared by %@", vPost.sharedByPlayer.userName];
        //
        //    } else {
        //        cell.sharedByLabel.hidden = YES;
        //    }
            
            //    CGFloat cellHeight = [self calculateHeightForImagePost:indexPath.section];
            
        //    cell.contentView.frame = CGRectMake(0, 0, self.tableView.frame.size.width, [self tableView:tableView heightForRowAtIndexPath:indexPath]);
            
            
            // Show comment button
            
            [cell.commentButton addTarget:self action:@selector(commentPost:) forControlEvents:UIControlEventTouchUpInside];
            cell.commentButton.tag = indexPath.section;
            cell.numCommentLabel.text = [NSString stringWithFormat:@"%lu comments", vPost.numComments];
     /*       if (vPost.numComments == 1)
                cell.commentTextLabel.text = @"Comment";
            else
                cell.commentTextLabel.text = @"Comments";
            
            if (vPost.numRemixes == 1)
                cell.shareTextLabel.text = @"Share";
            else
                cell.shareTextLabel.text = @"Shares";
     */
            
            
            
            cell.commentButtonIcon.image = [[UIImage imageNamed:@"Comment Small Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [cell.commentButtonIcon setTintColor:[UIColor lightGrayColor]];
            
            
            cell.venueDetailButton.tag = vPost.venue.venueID;
          
            /*UIButton *newsfeedIconButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
            [newsfeedIconButton addTarget:self action:@selector(goToVenue:) forControlEvents:UIControlEventTouchUpInside];
            newsfeedIconButton.tag = vPost.venue.venueID;
            [cell.newsfeedIcon addSubview:newsfeedIconButton];
            cell.newsfeedIcon.userInteractionEnabled = YES;
            */

            [cell.likeButton addTarget:self action:@selector(likePost:) forControlEvents:UIControlEventTouchUpInside];
            cell.likeButton.tag = indexPath.section;
            cell.likeNumberLabel.text = [NSString stringWithFormat:@"%lu props", vPost.numLikes];
            
            cell.likeIcon.image = [[UIImage imageNamed:@"Like Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            
            if (vPost.doesSessionUserLikePost)
                [cell.likeIcon setTintColor:[UIColor goldColor]];
            else
                [cell.likeIcon setTintColor:[UIColor lightGrayColor]];
            
            
            [cell.smallCircleView makeRoundedBorderWithRadius:2];
            cell.smallCircleView.backgroundColor = [UIColor darkGrayColor2];
            cell.numCommentLabel.textColor = [UIColor darkGrayColor2];
            cell.likeNumberLabel.textColor = [UIColor darkGrayColor2];
            
            cell.viewLikesButton.tag = indexPath.section;
            cell.viewCommentsButton.tag = indexPath.section;
            
            [cell.viewLikesButton addTarget:self action:@selector(viewLikes:) forControlEvents:UIControlEventTouchUpInside];
            [cell.viewCommentsButton addTarget:self action:@selector(viewComments:) forControlEvents:UIControlEventTouchUpInside];
            
            
            
       
            return cell;
            
        } else if ([(self.newsfeedPaginator.results)[indexPath.section] isKindOfClass:[VenueCheckinNews class]]) {
            
            PDVenueCheckinCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Checkin Cell" forIndexPath:indexPath];
            
            // clear subviews
            if ([cell.contentView subviews]){
                for (UIView *subview in [cell.contentView subviews]) {
                    if ([subview isKindOfClass:[SpecialUIButton class]]) {
                        SpecialUIButton * newSubview = (SpecialUIButton *)subview;
                        if (newSubview.shouldRemoveFromMySuperview)  // only items I created programmatically
                            [subview removeFromSuperview];
                        
                    } else if ([subview isKindOfClass:[SpecialUILabel class]]) {
                        SpecialUILabel *newSubview = (SpecialUILabel *)subview;
                        if (newSubview.shouldRemoveFromMySuperview)
                            [subview removeFromSuperview];
                    }
                    
                }
            }
            
            
            VenueCheckinNews *vPost = (VenueCheckinNews *)(self.newsfeedPaginator.results)[indexPath.section];
            [cell.actionButtonHandle addTarget:self action:@selector(postExtraActions:) forControlEvents:UIControlEventTouchUpInside];
            cell.actionButtonHandle.tag = indexPath.section;

      /*
            if ([vPost.sharedByPlayer.userName length] > 0) {
                cell.sharedByLabel.hidden = NO;
                cell.sharedByLabel.text = [NSString stringWithFormat:@"shared by %@", vPost.sharedByPlayer.userName];
                
            } else {
                cell.sharedByLabel.hidden = YES;
            }
        */
            cell.contentView.frame = CGRectMake(0, 0, self.tableView.frame.size.width, [self tableView:tableView heightForRowAtIndexPath:indexPath]);
            
            [cell.mainCellBackgroundView makeRoundedBorderWithRadius:3];
            cell.mainCellBackgroundView.layer.borderColor = [UIColor midGray2].CGColor;
            cell.mainCellBackgroundView.layer.borderWidth = 1.0;
            cell.usernameLabel.text = vPost.vci.checkedInUser.userName;
            
            
            NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                    baseString:vPost.vci.checkedInUser.profileBaseString];
            
            [cell.profilePic setImageWithURL:[NSURL URLWithString:url]
                            placeholderImage:[UIImage imageNamed:@"avatar square"]];
            [cell.profilePic makeRoundedBorderWithRadius:10];
            
            [cell.profilePic.layer setBorderWidth:2.0f];
            [cell.profilePic.layer setBorderColor:[UIColor whiteColor].CGColor];
            
            // profile pic button
            UIButton *picButton1 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];
            [picButton1 addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
            picButton1.tag = vPost.newsMakerUserID;
            [cell.profilePic addSubview:picButton1];
            cell.profilePic.userInteractionEnabled = YES;
            
            
         //   UIButton *venueButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 280, 21)];
            [cell.venueButton addTarget:self action:@selector(goToVenue:) forControlEvents:UIControlEventTouchUpInside];

           
            if ([vPost.vci.message length] > 0) {
                NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ - Playing %@ at %@", vPost.vci.message, [vPost.vci.checkedInSport.sportName capitalizedString], vPost.venue.venueName]];
                
                [string addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, [vPost.vci.message length])];
                cell.messageLabel.attributedText = string;
            } else
                cell.messageLabel.text = [NSString stringWithFormat:@"Playing %@ at %@", [vPost.vci.checkedInSport.sportName capitalizedString], vPost.venue.venueName];
            
            
            
            
            cell.detail1Label.text = [vPost.vci getAbbrevTimeStatusMessage:NO];
          //  [cell.timeBackgroundView makeRoundedBorderWithRadius:3];
 
            
            cell.postDateTimeLabel.text = [NSDate timeDiffCalc:vPost.newsDate];
            
            [cell.commentButton addTarget:self action:@selector(commentPost:) forControlEvents:UIControlEventTouchUpInside];
            cell.commentButton.tag = indexPath.section;
            cell.numCommentLabel.text = [NSString stringWithFormat:@"%lu comments", vPost.numComments];
            
            
           
            cell.commentIcon.image = [[UIImage imageNamed:@"Comment Small Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [cell.commentIcon setTintColor:[UIColor darkGrayColor2]];
            
            
            [cell.likeButton addTarget:self action:@selector(likePost:) forControlEvents:UIControlEventTouchUpInside];
            cell.likeButton.tag = indexPath.section;
            cell.likeNumberLabel.text = [NSString stringWithFormat:@"%lu props", vPost.numLikes];
            
            cell.likeIcon.image = [[UIImage imageNamed:@"Like Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            
            if (vPost.doesSessionUserLikePost)
                [cell.likeIcon setTintColor:[UIColor goldColor]];
            else
                [cell.likeIcon setTintColor:[UIColor darkGrayColor2]];
            
        
            cell.viewLikesButton.tag = indexPath.section;
            cell.viewCommentsButton.tag = indexPath.section;
            cell.venueButton.tag = vPost.venue.venueID;
            
            [cell.viewLikesButton addTarget:self action:@selector(viewLikes:) forControlEvents:UIControlEventTouchUpInside];
            [cell.viewCommentsButton addTarget:self action:@selector(viewComments:) forControlEvents:UIControlEventTouchUpInside];
            [cell.venueButton addTarget:self action:@selector(goToVenue:) forControlEvents:UIControlEventTouchUpInside];
            
            UIButton *newsfeedIconButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
            [newsfeedIconButton addTarget:self action:@selector(goToVenue:) forControlEvents:UIControlEventTouchUpInside];
            newsfeedIconButton.tag = vPost.venue.venueID;
            [cell.newsfeedIcon addSubview:newsfeedIconButton];
            cell.newsfeedIcon.userInteractionEnabled = YES;
            
            
            return cell;
        }else if ([(self.newsfeedPaginator.results)[indexPath.section] isKindOfClass:[VoteReceiving class]]) {
            VoteCell *cell = [tableView dequeueReusableCellWithIdentifier:@"vote_newsfeed_cell" forIndexPath:indexPath];
            
            // clear subviews
            if ([cell.contentView subviews]){
                for (UIView *subview in [cell.contentView subviews]) {
                    if ([subview isKindOfClass:[SpecialUIButton class]]) {
                        SpecialUIButton * newSubview = (SpecialUIButton *)subview;
                        if (newSubview.shouldRemoveFromMySuperview)  // only items I created programmatically
                            [subview removeFromSuperview];
                        
                    } else if ([subview isKindOfClass:[SpecialUILabel class]]) {
                        SpecialUILabel *newSubview = (SpecialUILabel *)subview;
                        if (newSubview.shouldRemoveFromMySuperview)
                            [subview removeFromSuperview];
                    }
                    
                }
            }
            
            VoteReceiving *vPost = (VoteReceiving *)(self.newsfeedPaginator.results)[indexPath.section];
            
            // Action sheet
            [cell.actionSheet addTarget:self action:@selector(postExtraActions:) forControlEvents:UIControlEventTouchUpInside];
            cell.actionSheet.tag = indexPath.section;
            
            // View
            cell.contentView.frame = CGRectMake(0, 0, self.tableView.frame.size.width, [self tableView:tableView heightForRowAtIndexPath:indexPath]);
            [cell.outterView makeRoundedBorderWithRadius:6];
            [cell.innerView makeRoundedBorderWithRadius:5];
            
            // Change the attribute of the context for newsfeed
            if ([vPost.type isEqualToString:@"level"]) {
                NSString * str = [NSString stringWithFormat:@"%@ reached a new level; %@", vPost.newsMakerUserName, [vPost.textPost uppercaseString]];
                vPost.string = str;
                NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:str];
                [string addAttribute:NSForegroundColorAttributeName value:[UIColor goldColor] range:NSMakeRange(0,vPost.newsMakerUserName.length)];
                [string addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:13] range:NSMakeRange(0,vPost.newsMakerUserName.length)];
                [string addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:13] range:NSMakeRange(string.length - vPost.textPost.length,vPost.textPost.length)];
                cell.textpost.attributedText = string;
            } else {
                NSString * str = [NSString stringWithFormat:@"%@ %@ %@", vPost.newsMakerUserName, vPost.textPost, vPost.targetName];
                vPost.string = str;
                NSArray *array = [str componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]; // separate by space
                NSString * number = [array objectAtIndex:2]; // Get the number
                
                NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ %@ %@", vPost.newsMakerUserName, vPost.textPost, vPost.targetName]];
                [string addAttribute:NSForegroundColorAttributeName value:[UIColor goldColor] range:NSMakeRange(0,vPost.newsMakerUserName.length)];
                [string addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:13] range:NSMakeRange(0,vPost.newsMakerUserName.length)];
                [string addAttribute:NSForegroundColorAttributeName value:[UIColor goldColor] range:NSMakeRange(string.length - vPost.targetName.length, vPost.targetName.length)];
                [string addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:13] range:NSMakeRange(string.length - vPost.targetName.length, vPost.targetName.length)];
                if ([vPost.type isEqualToString:@"receiving"]) {
                    [string addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:13] range:NSMakeRange(vPost.newsMakerUserName.length+10, number.length + 5)];
                } else if ([vPost.type isEqualToString:@"giving"]) {
                    [string addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:13] range:NSMakeRange(vPost.newsMakerUserName.length+6, number.length + 5)];
                }
                cell.textpost.attributedText = string;
            }
            
            // icon for the vote newsfeed
            if ([vPost.type isEqualToString:@"receiving"]) {
                cell.image.image = [UIImage imageNamed:@"receive_vote"];
            } else if ([vPost.type isEqualToString:@"giving"]) {
                cell.image.image = [UIImage imageNamed:@"vote_gave"];
            } else if ([vPost.type isEqualToString:@"thanks"]) {
                cell.image.image = [UIImage imageNamed:@"sent_thanks"];
            } else if ([vPost.type isEqualToString:@"level"]) {
                cell.image.image = [UIImage imageNamed:@"level_up"];
            }
            
            cell.datetime.text = [NSDate timeDiffCalc:vPost.newsDate];
            
            [cell.commentsButton addTarget:self action:@selector(commentPost:) forControlEvents:UIControlEventTouchUpInside];
            cell.commentsButton.tag = indexPath.section;
            cell.commentNumberLabel.text = [NSString stringWithFormat:@"%d comments", vPost.numComments];
            [cell.smallCircle makeRoundedBorderWithRadius:2];
            cell.smallCircle.backgroundColor = [UIColor darkGrayColor2];
            cell.commentNumberLabel.textColor = [UIColor darkGrayColor2];
            cell.likeNumberLabel.textColor = [UIColor darkGrayColor2];
            cell.commentsIcon.image = [[UIImage imageNamed:@"Comment Small Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [cell.commentsIcon setTintColor:[UIColor darkGrayColor2]];
            [cell.likeButton addTarget:self action:@selector(likePost:) forControlEvents:UIControlEventTouchUpInside];
            cell.likeButton.tag = indexPath.section;
            cell.likeNumberLabel.text = [NSString stringWithFormat:@"%d props", vPost.numLikes];
            cell.likeIcon.image = [[UIImage imageNamed:@"Like Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            if (vPost.doesSessionUserLikePost)
                [cell.likeIcon setTintColor:[UIColor goldColor]];
            else
                [cell.likeIcon setTintColor:[UIColor darkGrayColor2]];
            
            cell.seeProps.tag = indexPath.section;
            cell.seeComments.tag = indexPath.section;
            [cell.seeProps addTarget:self action:@selector(viewLikes:) forControlEvents:UIControlEventTouchUpInside];
            [cell.seeComments addTarget:self action:@selector(viewComments:) forControlEvents:UIControlEventTouchUpInside];
            
            return cell;
        }
    } else if (self.listToDisplay == DISPLAY_LOCKER) {
        
        if ([self.newsfeedPaginator.results count] == 0) {
            // no results
            self.nodata = YES;
            NoResultTVC *cell = [tableView dequeueReusableCellWithIdentifier:@"No Results Cell Expand" forIndexPath:indexPath];
            cell.backgroundColor = [UIColor clearColor];
            UIImage *img = [UIImage imageNamed:@"no_locker_image"];
            cell.icon.image = img;
            [cell.icon setContentMode:UIViewContentModeCenter];
            cell.headline.text = @"No Image Posted";
            //cell.message.text = @"Upload your own pictures and set tags to start your locker.";
            cell.message.hidden = YES;
            cell.contentView.frame = CGRectMake(0, 0, self.tableView.frame.size.width, [self tableView:tableView heightForRowAtIndexPath:indexPath]);
            return cell;
        } else if ([(self.newsfeedPaginator.results)[indexPath.section] isKindOfClass:[LockerAlbumCover class]]) {
            self.nodata = NO;
            PDLockerAlbumCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Locker Cell" forIndexPath:indexPath];
            
            LockerAlbumCover *lac = (LockerAlbumCover *)(self.newsfeedPaginator.results)[indexPath.row];
            
            cell.sportLabel.text = [lac.sp.sportName uppercaseString];
            cell.seasonYearLabel.text = [NSString stringWithFormat:@"%@ %ld", [lac.season uppercaseString], lac.year];
            
            NSString *urlPostPic = [S3Tools2 getFileNameStringWithType:@"photo_regsize" baseString:[NSString stringWithFormat:@"%ld/photo/%@", lac.creatorID, lac.imageName]];
            
            cell.lockerImage.contentMode = UIViewContentModeScaleAspectFill;
            cell.lockerImage.clipsToBounds = YES;
            
            [cell.lockerImage setImageWithURL:[NSURL URLWithString:urlPostPic] placeholderImage:[UIImage imageNamed:@"Landscape Placeholder"]];
            
            if (lac.numPics == 1)
                cell.numPicsLabel.text = [NSString stringWithFormat:@"%ld picture", lac.numPics];
            else
                cell.numPicsLabel.text = [NSString stringWithFormat:@"%ld pictures", lac.numPics];
            
            
            return cell;
        }
    }
    return nil;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"coach_segue"])  {
        id theSegue = segue.destinationViewController;
        [theSegue setValue:@"coach_boost" forKey:@"title"];
    }
    if ([segue.identifier isEqualToString:@"Challenge Segue"]) {
        Player *player = [[Player alloc] initWithPlayerDetails:self.playerUserID
                                                   andUserName:self.playerUsername
                                           andProfilePicString:self.profilePicString];
        AddDirectIndividualMatchTVC *controller = segue.destinationViewController;
        controller.challengeUserProfileBaseString = player.profileBaseString;
        controller.userIDToChallenge = player.userID;
        controller.usernameToChallenge = player.userName;
    } else if ([segue.identifier isEqualToString:@"Followers Segue"]) {
        ShowFollowersTVC *controller = segue.destinationViewController;
        controller.baseUserID = self.playerUserID;
    } else if ([segue.identifier isEqualToString:@"Following Segue"]) {
        ShowFollowingTVC *controller = segue.destinationViewController;
        controller.baseUserID = self.playerUserID;
    } else if ([segue.identifier isEqualToString:@"Sport Ratings Segue"]) {
        SportRatingsSummaryTVC *controller = segue.destinationViewController;
        controller.sport = [self.sportsArray objectAtIndex:self.selectedRow];
    //    PlayerSport *sp = controller.sport;
        Player *pl = [[Player alloc] initWithPlayerDetails:self.playerUserID
                                               andUserName:self.usernameLabel.text
                                       andProfilePicString:self.profilePicString];
        controller.playerToBeRated = pl;
    } else if ([segue.identifier isEqualToString:@"Compose Message Segue"]) {
        ComposeNewMessageIntroTVC *controller = segue.destinationViewController;
        controller.filterUserID = self.playerUserID;
    } else if ([segue.identifier isEqualToString:@"Rate Ability Segue"]) {
        ChooseSportToRateTVC *controller = segue.destinationViewController;
        Player *pl = [[Player alloc] initWithPlayerDetails:self.playerUserID
                                               andUserName:self.usernameLabel.text
                                       andProfilePicString:self.profilePicString];
        controller.playerToRate = pl;
    } else if([segue.identifier isEqualToString:@"Pickup Match Detail Segue"]){
        PickupMatchDetailTVC *controller = segue.destinationViewController;
        
         if ([[self.newsfeedPaginator.results objectAtIndex:self.selectedSection] isKindOfClass:[PickupMatchCreationNews class]]) {
            PickupMatchCreationNews *pCreationNews = (self.newsfeedPaginator.results)[self.selectedSection];
            controller.matchID = pCreationNews.pickupMatch.matchID;
        }
        
    } else if ([segue.identifier isEqualToString:@"Individual Match Detail Segue"]) {
        IndividualMatchDetailTVC *controller = segue.destinationViewController;
        
        if ([[self.newsfeedPaginator.results objectAtIndex:self.selectedSection] isKindOfClass:[IndividualMatchCreationNews class]]) {
            IndividualMatchCreationNews *iNews = (self.newsfeedPaginator.results)[self.selectedSection];
            controller.matchID = iNews.individualMatch.matchID;
        } else if ([[self.newsfeedPaginator.results objectAtIndex:self.selectedSection] isKindOfClass:[MatchPlayerNewsOfficialFinishedIndividualMatch class]]) {
            MatchPlayerNewsOfficialFinishedIndividualMatch *iNews = (self.newsfeedPaginator.results)[self.selectedSection];
            controller.matchID = iNews.match.matchID;
        }
    } else if ([segue.identifier isEqualToString:@"Comment Segue"]) {
        
        CommentsTVC *controller = (CommentsTVC *)segue.destinationViewController;
        controller.delegate = self;
        controller.currentTableSection = self.selectedSection;
        controller.showKeyboardOnLoad = self.showKeyboardOnCommentsPage;

        if ([[self.newsfeedPaginator.results objectAtIndex:self.selectedSection] isKindOfClass:[News class]]) {
            News *n = (self.newsfeedPaginator.results)[self.selectedSection];
            controller.newsID = n.newsID;
        }
    }  else if ([segue.identifier isEqualToString:@"Venue Detail Segue"]) {
        VenueDetailTVC *controller = segue.destinationViewController;
        controller.venueID = self.selectedVenue;
    } else if ([segue.identifier isEqualToString:@"Locker Segue"]) {
        ShowPhotosInLockerAlbumCVC *controller = segue.destinationViewController;
        LockerAlbumCover *lac = (LockerAlbumCover *)(self.newsfeedPaginator.results)[self.selectedRow];
        controller.sport = lac.sp;
        controller.picsSeason = lac.season;
        controller.picsYear = lac.year;
        controller.albumHolderUserID = self.playerUserID;

    } else if ([segue.identifier isEqualToString:@"Locker Detail Segue"]) {
        LockerPhotoDetailTVC *controller = segue.destinationViewController;
        controller.lockerID = self.selectedLockerID;
    } else if ([segue.identifier isEqualToString:@"View Likes Segue"]) {
        ShowLikesTVC *controller = segue.destinationViewController;
        LockerPost *n = [self.newsfeedPaginator.results objectAtIndex:self.selectedSection];
        controller.newsfeedID = n.newsID;
    } else if ([segue.identifier isEqualToString:@"playerdetail_segue_from_self"]) {
        PlayerDetailTVC *controller = segue.destinationViewController;
        controller.playerUserID = self.targetID;
    } else if ([segue.identifier isEqualToString:@"player_share_segue"]) {
        FBShareRequestDialog *controller = segue.destinationViewController;
        UIImageView *tmpView = [[UIImageView alloc] init];
        [tmpView setImageWithURL:[NSURL URLWithString:self.fbImageString]
                placeholderImage:[UIImage imageNamed:@"avatar square"]];
        self.currentPickedImage = [self addOverlayToPostImage:tmpView.image];
        controller.image = self.currentPickedImage;
        controller.delegate = self;
    } else if ([segue.identifier isEqualToString:@"vote_popup_segue"]) {
        VotePopupWindowVC *controller = segue.destinationViewController;
        controller.targetID = self.playerUserID;
        controller.delegate = self;
    } else if ([segue.identifier isEqualToString:@"change_exp_from_profile"]) {
        ChangeExperienceTVC *controller = (ChangeExperienceTVC *)segue.destinationViewController;
        controller.type = self.type;
        controller.changeID = self.changeID;
        controller.name = self.name;
        controller.time = self.date;
        controller.sportname = self.sport;
        controller.isChange = self.change;
        controller.delegate = self;
    } else if ([segue.identifier isEqualToString:@"shop_from_profile"]) {
        VoteShopVC *controller = segue.destinationViewController;
        controller.fromProfile = YES;
    }
   
}

// Experience delegate to reload the data
- (void)reloadTable:(NSString *)type
{
    if ([type isEqualToString:EXPERIENCE]) {
        [self fetchExperience];
    } else if ([type isEqualToString:AWARD]) {
        [self fetchAward];
    }
}

- (void)fetchExperience
{
    [IOSRequest fetchExperienceForUser:[self.preferences getUserID]
                          onCompletion:^(NSDictionary *result) {
                              if ([result[@"outcome"] isEqualToString:@"failure"]) {
                                  UIAlertView *alert = [[UIAlertView alloc] init];
                                  alert.title = @"Submission Error";
                                  alert.message = @"There was a problem submitting your request.  Please try again.";
                                  [alert addButtonWithTitle:@"OK"];
                                  [alert show];
                              } else if ([result[@"outcome"] isEqualToString:@"success"]) {
                                  [self.exp removeAllObjects];
                                  self.type = EXPERIENCE;
                                  for (id object in result[@"list"]) {
                                      [self.exp addObject:object];
                                  }
                                  [self.tableView reloadData];
                              }
                          }];
}

- (void)fetchAward
{
    [IOSRequest fetchAwardForUser:[self.preferences getUserID]
                     onCompletion:^(NSDictionary *res) {
                         if ([res[@"outcome"] isEqualToString:@"failure"]) {
                             UIAlertView *alert = [[UIAlertView alloc] init];
                             alert.title = @"Submission Error";
                             alert.message = @"There was a problem submitting your request.  Please try again.";
                             [alert addButtonWithTitle:@"OK"];
                             [alert show];
                         } else if ([res[@"outcome"] isEqualToString:@"success"]) {
                             [self.awd removeAllObjects];
                             self.type = AWARD;
                             for (id object in res[@"list"]) {
                                 [self.awd addObject:object];
                             }
                             [self.tableView reloadData];
                         }
                     }];
}

- (void)refreshVoteInfo
{
    [self loadVotesData];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (self.listToDisplay == DISPLAY_LOCKER && !self.nodata) {
        self.selectedRow = indexPath.row;
        [self performSegueWithIdentifier:@"Locker Segue" sender:self];
    } else if (self.listToDisplay == DISPLAY_DETAILS && [self.preferences getUserID] == self.playerUserID){
        self.change = YES;
        if (indexPath.row > 1 && indexPath.row < self.exp.count + 2) { // Experience
            ExperienceTVCell *cell = (ExperienceTVCell *)[tableView cellForRowAtIndexPath:indexPath];
            self.type = EXPERIENCE;
            self.changeID = cell.tag;
            self.name = cell.title.text;
            self.date = cell.time.text;
            self.sport = cell.sport.text;
        } else if (indexPath.row > self.exp.count+2) { // Award
            ExperienceTVCell *cell = (ExperienceTVCell *)[tableView cellForRowAtIndexPath:indexPath];
            self.type = AWARD;
            self.changeID = cell.tag;
            self.name = cell.title.text;
            self.date = cell.time.text;
            self.sport = cell.sport.text;
        }
        [self performSegueWithIdentifier:@"change_exp_from_profile" sender:self];

    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (self.listToDisplay == DISPLAY_MATCHES || self.listToDisplay == DISPLAY_DETAILS) {
        return 0;
    }
    return 25.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.listToDisplay == DISPLAY_DETAILS) {
        if (indexPath.row == 0) {
            if ([self.aboutString length] > 0 && [self calculateHeightForAboutSection] > 144) {
                return [self calculateHeightForAboutSection];
            }
            return 144;
        } else if (indexPath.row == 1 || (indexPath.row == self.exp.count + 2 && self.exp.count > 0)||(indexPath.row == 3 && self.exp.count == 0)) { // title
            return 41;
        } else {
            return 64;
        }
        
  /*  } else if (self.listToDisplay == DISPLAY_SPORTS) {
        return 91.0;*/
    } else if (self.listToDisplay == DISPLAY_LOCKER) {
        if ([self.newsfeedPaginator.results count] == 0) {
            return 200;
        } else {
            return 320.0;
        }
    } else if (self.listToDisplay == DISPLAY_MATCHES) {
        if (indexPath.row == 0) {
            return 41;
        } else {
            return 50;
        }
    } else if (self.listToDisplay == DISPLAY_NEWSFEED) {
        if ([self.newsfeedPaginator.results count] == 0) {
            return 200;
        } else if ([(self.newsfeedPaginator.results)[indexPath.section] isKindOfClass:[PickupMatchCreationNews class]]) {
              /*
                PickupMatchCreationNews *pNews = (self.newsfeedPaginator.results)[indexPath.section];
                
                NSInteger numMatchHistoryToDisplay;
                
                if ([pNews.pickupMatch.matchHistoryArray count] > 3)
                    numMatchHistoryToDisplay = 3;
                else
                    numMatchHistoryToDisplay = [pNews.pickupMatch.matchHistoryArray count];
                
           //     return 345 + (numMatchHistoryToDisplay * 20) + 30 + 55;
            return 345 + (numMatchHistoryToDisplay * 20) + 30 + 45;
*/
            return 333;
          
        } else if ([(self.newsfeedPaginator.results)[indexPath.section] isKindOfClass:[IndividualMatchCreationNews class]]) {
           /* IndividualMatchCreationNews *iNews = (self.newsfeedPaginator.results)[indexPath.section];
         
            NSInteger numMatchHistoryToDisplay;
            
            if ([iNews.individualMatch.matchHistoryArray count] > 3)
                numMatchHistoryToDisplay = 3;
            else
                numMatchHistoryToDisplay = [iNews.individualMatch.matchHistoryArray count];
            
            return 353 + (numMatchHistoryToDisplay * 20) - 30;*/
            return 250;
            
        } else if ([(self.newsfeedPaginator.results)[indexPath.section] isKindOfClass:[MatchPlayerNewsOfficialFinishedIndividualMatch class]]) {
          /*  MatchPlayerNewsOfficialFinishedIndividualMatch *iNews = (self.newsfeedPaginator.results)[indexPath.section];
            
            NSInteger numMatchHistoryToDisplay;
            
            if ([iNews.match.matchHistoryArray count] > 3)
                numMatchHistoryToDisplay = 3;
            else
                numMatchHistoryToDisplay = [iNews.match.matchHistoryArray count];
            
       //     return 430 + (numMatchHistoryToDisplay * 20) + 30 - 19;
            return 420;*/
            return 365;
            
        } else if ([(self.newsfeedPaginator.results)[indexPath.section] isKindOfClass:[ImagePost class]]) {
           
            return [self calculateHeightForImagePost:indexPath.section];
            
        } else if ([(self.newsfeedPaginator.results)[indexPath.section] isKindOfClass:[LockerPost class]]) {
            return [self calculateHeightForLockerImagePost:indexPath.section];

        } else if ([(self.newsfeedPaginator.results)[indexPath.section] isKindOfClass:[TextPost class]]) {
            
            return [self calculateHeightForTextPost:indexPath.section];
            
        } else if ([(self.newsfeedPaginator.results)[indexPath.section] isKindOfClass:[CompetitionNews class]]) {
            return 327.0;
        } else if ([(self.newsfeedPaginator.results)[indexPath.section] isKindOfClass:[VenueKingNews class]]) {
            return 154;
        } else if ([(self.newsfeedPaginator.results)[indexPath.section] isKindOfClass:[VenueCheckinNews class]]) {
            
            return 250 + [self calculateHeightForCheckinTextPost:indexPath.section] - 15;
        } else if ([(self.newsfeedPaginator.results)[indexPath.section] isKindOfClass:[VoteReceiving class]]) {
            return 104;
        }
    } 
    return 0;
}

// 4
- (IBAction)detailsAction:(id)sender
{
    if (self.newsfeedPaginator.noPermissions)
        self.permissionsErrorLabel.hidden = NO;
    else
        self.permissionsErrorLabel.hidden = YES;
    
    self.listToDisplay = DISPLAY_NEWSFEED;
    self.newsfeedPaginator.tabToShow = PLAYER_DETAIL_SHOW_NEWS_POSTS;
    [self.newsfeedPaginator reset];
    //    [self.newsfeedPaginator fetchFirstPage];
    self.wereServerResultsLoaded = NO;
    [self.newsfeedPaginator fetchFirstPageWithTab:PLAYER_DETAIL_SHOW_NEWS_POSTS];
    
    CGRect orginFrame = self.buttomLine.frame;
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.buttomLine.frame = CGRectMake(19+3*79, orginFrame.origin.y, orginFrame.size.width, orginFrame.size.height);
                     }
                     completion:nil];
    // Change the background color for icons
    self.tabIcon1.image = [[UIImage imageNamed:@"image_icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.tabIcon1 setTintColor:[UIColor colorWithRed:225/255.0 green:225/255.0 blue:229/255.0 alpha:1.0]];
    self.tabIcon2.image = [[UIImage imageNamed:@"supporter"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.tabIcon2 setTintColor:[UIColor colorWithRed:225/255.0 green:225/255.0 blue:229/255.0 alpha:1.0]];
    self.tabIcon3.image = [[UIImage imageNamed:@"detail"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.tabIcon3 setTintColor:[UIColor colorWithRed:225/255.0 green:225/255.0 blue:229/255.0 alpha:1.0]];
    self.tabIcon4.image = [[UIImage imageNamed:@"feed_tab"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.tabIcon4 setTintColor:[UIColor colorWithRed:199/255.0 green:160/255.0 blue:74/255.0 alpha:1.0]];
    
    [self.tableView reloadData];
}

// 3
- (IBAction)postsAction:(id)sender
{
    self.permissionsErrorLabel.hidden = YES;
    self.listToDisplay = DISPLAY_DETAILS;
    
    CGRect orginFrame = self.buttomLine.frame;
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.buttomLine.frame = CGRectMake(19+2*79, orginFrame.origin.y, orginFrame.size.width, orginFrame.size.height);
                     } completion:nil];
    // Change the background color for icons
    self.tabIcon1.image = [[UIImage imageNamed:@"image_icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.tabIcon1 setTintColor:[UIColor colorWithRed:225/255.0 green:225/255.0 blue:229/255.0 alpha:1.0]];
    self.tabIcon2.image = [[UIImage imageNamed:@"supporter"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.tabIcon2 setTintColor:[UIColor colorWithRed:225/255.0 green:225/255.0 blue:229/255.0 alpha:1.0]];
    self.tabIcon3.image = [[UIImage imageNamed:@"detail"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.tabIcon3 setTintColor:[UIColor colorWithRed:199/255.0 green:160/255.0 blue:74/255.0 alpha:1.0]];
    self.tabIcon4.image = [[UIImage imageNamed:@"feed_tab"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.tabIcon4 setTintColor:[UIColor colorWithRed:225/255.0 green:225/255.0 blue:229/255.0 alpha:1.0]];
    
    [self fetchExperienceAndAward];
    
    //[self.tableView reloadData];
}

- (void)fetchExperienceAndAward
{
    [IOSRequest fetchExperienceForUser:self.playerUserID
                          onCompletion:^(NSDictionary *result) {
                              if ([result[@"outcome"] isEqualToString:@"failure"]) {
                                  UIAlertView *alert = [[UIAlertView alloc] init];
                                  alert.title = @"Submission Error";
                                  alert.message = @"There was a problem submitting your request.  Please try again.";
                                  [alert addButtonWithTitle:@"OK"];
                                  [alert show];
                              } else if ([result[@"outcome"] isEqualToString:@"success"]) {
                                  [self.exp removeAllObjects];
                                  for (id object in result[@"list"]) {
                                      [self.exp addObject:object];
                                  }
                                  
                                  [IOSRequest fetchAwardForUser:self.playerUserID
                                                   onCompletion:^(NSDictionary *res) {
                                                       if ([res[@"outcome"] isEqualToString:@"failure"]) {
                                                           UIAlertView *alert = [[UIAlertView alloc] init];
                                                           alert.title = @"Submission Error";
                                                           alert.message = @"There was a problem submitting your request.  Please try again.";
                                                           [alert addButtonWithTitle:@"OK"];
                                                           [alert show];
                                                       } else if ([res[@"outcome"] isEqualToString:@"success"]) {
                                                           [self.awd removeAllObjects];
                                                           for (id object in res[@"list"]) {
                                                               [self.awd addObject:object];
                                                           }
                                                           [self.tableView reloadData];
                                                       }
                                                   }];
                              }
                          }];
}

// 1
- (IBAction)lockerAction:(id)sender
{
    if (self.newsfeedPaginator.noPermissions)
        self.permissionsErrorLabel.hidden = NO;
    else
        self.permissionsErrorLabel.hidden = YES;
    
    self.listToDisplay = DISPLAY_LOCKER;
    self.newsfeedPaginator.tabToShow = PLAYER_DETAIL_SHOW_LOCKER_COVERS;
    [self.newsfeedPaginator reset];
 //   [self.newsfeedPaginator fetchFirstPage];
    self.wereServerResultsLoaded = NO;
    [self.newsfeedPaginator fetchFirstPageWithTab:PLAYER_DETAIL_SHOW_LOCKER_COVERS];
    
    CGRect orginFrame = self.buttomLine.frame;
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.buttomLine.frame = CGRectMake(19, orginFrame.origin.y, orginFrame.size.width, orginFrame.size.height);
                     } completion:nil];
    // Change the background color for icons
    self.tabIcon1.image = [[UIImage imageNamed:@"image_icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.tabIcon1 setTintColor:[UIColor colorWithRed:199/255.0 green:160/255.0 blue:74/255.0 alpha:1.0]];
    self.tabIcon2.image = [[UIImage imageNamed:@"supporter"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.tabIcon2 setTintColor:[UIColor colorWithRed:225/255.0 green:225/255.0 blue:229/255.0 alpha:1.0]];
    self.tabIcon3.image = [[UIImage imageNamed:@"detail"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.tabIcon3 setTintColor:[UIColor colorWithRed:225/255.0 green:225/255.0 blue:229/255.0 alpha:1.0]];
    self.tabIcon4.image = [[UIImage imageNamed:@"feed_tab"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.tabIcon4 setTintColor:[UIColor colorWithRed:225/255.0 green:225/255.0 blue:229/255.0 alpha:1.0]];
    
    [self.tableView reloadData];
}

// 2
- (IBAction)matchFeedAction:(id)sender
{
    if (self.newsfeedPaginator.noPermissions)
        self.permissionsErrorLabel.hidden = NO;
    else
        self.permissionsErrorLabel.hidden = YES;
    
    self.listToDisplay = DISPLAY_MATCHES;
    
    CGRect orginFrame = self.buttomLine.frame;
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.buttomLine.frame = CGRectMake(19+79, orginFrame.origin.y, orginFrame.size.width, orginFrame.size.height);
                     } completion:nil];
    // Change the background color for icons
    self.tabIcon1.image = [[UIImage imageNamed:@"image_icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.tabIcon1 setTintColor:[UIColor colorWithRed:225/255.0 green:225/255.0 blue:229/255.0 alpha:1.0]];
    self.tabIcon2.image = [[UIImage imageNamed:@"supporter"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.tabIcon2 setTintColor:[UIColor colorWithRed:199/255.0 green:160/255.0 blue:74/255.0 alpha:1.0]];
    self.tabIcon3.image = [[UIImage imageNamed:@"detail"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.tabIcon3 setTintColor:[UIColor colorWithRed:225/255.0 green:225/255.0 blue:229/255.0 alpha:1.0]];
    self.tabIcon4.image = [[UIImage imageNamed:@"feed_tab"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.tabIcon4 setTintColor:[UIColor colorWithRed:225/255.0 green:225/255.0 blue:229/255.0 alpha:1.0]];
    
    [self loadSupportersForUserInRegion:@"any" forAll:YES];
}

- (void)loadSupportersForUserInRegion:(NSString *)region forAll:(BOOL)flag
{
    [IOSRequest loadSupportersForUser:self.playerUserID
                             inRegion:region
                               forAll:flag
                         onCompletion:^(NSDictionary *result) {
                             [self.data removeAllObjects];
                             if ([result[@"outcome"] isEqualToString:@"success"]) {
                                 if (result[@"list"] != [NSNull null]) {
                                     for (id object in result[@"list"]) {
                                         [self.data addObject:object];
                                     }
                                 }
                                 [self.tableView reloadData];
                             } else if ([result[@"outcome"] isEqualToString:@"failure"]) {
                                 UIAlertView *alert = [[UIAlertView alloc] init];
                                 alert.title = @"Submission Error";
                                 alert.message = @"There was a problem submitting your request. Please try again later or contact us.";
                                 [alert addButtonWithTitle:@"OK"];
                                 [alert show];
                             }
                         }];
    
}

- (UILabel *)makeDynamicHeighLabel:(NSString *)myText forLabel:(UILabel *)myLabel
{
    CGSize maxLabelSize = CGSizeMake(300,500);
    
    
    /********** because sizeWithFont deprecated *************/
    
    NSMutableDictionary *attributes = [NSMutableDictionary new];
    [attributes setObject:[UIFont systemFontOfSize:myLabel.font.pointSize] forKey:NSFontAttributeName];
    
    CGRect sizeRect = [myText boundingRectWithSize:maxLabelSize options:NSLineBreakByWordWrapping | NSStringDrawingUsesLineFragmentOrigin  attributes:attributes context:nil];
    
    CGSize expectedLabelSize = sizeRect.size;
    //      CGSize expectedLabelSize = [myText sizeWithFont:myLabel.font
    //constrainedToSize:maxLabelSize
    //lineBreakMode:myLabel.lineBreakMode];
    
    /********* end because sizeWithFont deprecated *********/
    
    
    
    //adjust the label the new height.
    CGRect newFrame = myLabel.frame;
    newFrame.size.height = expectedLabelSize.height;
    myLabel.frame = newFrame;
    
    return myLabel;
}

- (CGFloat)calculateHeightForCheckinTextPost:(NSUInteger)section
{
    VenueCheckinNews *vPost = [self.newsfeedPaginator.results objectAtIndex:section];
    NSString *tempString = @"";
    
    if ([vPost.vci.message length] > 0) {
        tempString = [NSString stringWithFormat:@"%@ - Playing %@ at %@", vPost.vci.message, [vPost.vci.checkedInSport.sportName capitalizedString], vPost.venue.venueName];
    } else
        tempString = [NSString stringWithFormat:@"Playing %@ at %@", [vPost.vci.checkedInSport.sportName capitalizedString], vPost.venue.venueName];
    
    
    
    NSString *text = tempString;
    
    CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH, 1000.0f);
    
    /********** because sizeWithFont deprecated *************/
    
    NSMutableDictionary *attributes = [NSMutableDictionary new];
    [attributes setObject:[UIFont systemFontOfSize:FONT_SIZE] forKey:NSFontAttributeName];
    
    CGRect sizeRect = [text boundingRectWithSize:constraint options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
    
    CGSize size = sizeRect.size;
    //      CGSize size = [text sizeWithFont:[UIFont systemFontOfSize:FONT_SIZE] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    
    /********* end because sizeWithFont deprecated *********/
    
    return size.height;
}

- (CGFloat)calculateHeightForImagePost:(NSUInteger)section
{
    ImagePost *iPost = (self.newsfeedPaginator.results)[section];
    NSString *text = iPost.textPost;
    
    CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH, 1000.0f);
    
    /********** because sizeWithFont deprecated *************/
    
    NSMutableDictionary *attributes = [NSMutableDictionary new];
    [attributes setObject:[UIFont systemFontOfSize:FONT_SIZE] forKey:NSFontAttributeName];
    
    CGRect sizeRect = [text boundingRectWithSize:constraint options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
    
    CGSize size = sizeRect.size;
    //        CGSize size = [text sizeWithFont:[UIFont systemFontOfSize:FONT_SIZE] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    
    /********* end because sizeWithFont deprecated *********/
    
    NSInteger factorDueToShareString;
    
    if ([iPost.sharedByPlayer.userName length] > 0) {
        factorDueToShareString = 115;
    } else {
        factorDueToShareString = 100;
    }
    
  //  return size.height + HEIGHT_OF_REST_OF_CELL - 5 + factorDueToShareString + 24;
    return size.height + HEIGHT_OF_REST_OF_CELL - 5 + factorDueToShareString - 60;

}

- (CGFloat)calculateHeightForLockerImagePost:(NSUInteger)section
{
    LockerPost *iPost = (self.newsfeedPaginator.results)[section];
    NSString *text = iPost.textPost;
    
    if ([text length] == 0) return 385.0;
    
    CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH, 1000.0f);
    
    /********** because sizeWithFont deprecated *************/
    
    NSMutableDictionary *attributes = [NSMutableDictionary new];
    [attributes setObject:[UIFont systemFontOfSize:FONT_SIZE] forKey:NSFontAttributeName];
    
    CGRect sizeRect = [text boundingRectWithSize:constraint options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
    
    CGSize size = sizeRect.size;
    //        CGSize size = [text sizeWithFont:[UIFont systemFontOfSize:FONT_SIZE] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    
    /********* end because sizeWithFont deprecated *********/
    
    NSInteger factorDueToShareString;
    
    if ([iPost.sharedByPlayer.userName length] > 0) {
        factorDueToShareString = 115;
    } else {
        factorDueToShareString = 100;
    }
    
    return size.height + HEIGHT_OF_REST_OF_CELL - 5 + factorDueToShareString - 60;
}


- (CGFloat)calculateHeightForTextPost:(NSUInteger)section
{
    News *nPost = (self.newsfeedPaginator.results)[section];
    NSString *text = nPost.textPost;
    
    CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH, 1000.0f);
    
    /********** because sizeWithFont deprecated *************/
    
    NSMutableDictionary *attributes = [NSMutableDictionary new];
    [attributes setObject:[UIFont systemFontOfSize:FONT_SIZE] forKey:NSFontAttributeName];
    
    CGRect sizeRect = [text boundingRectWithSize:constraint options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
    
    CGSize size = sizeRect.size;
    //      CGSize size = [text sizeWithFont:[UIFont systemFontOfSize:FONT_SIZE] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    
    /********* end because sizeWithFont deprecated *********/
    
    NSInteger factorDueToShareString;
    
    if ([nPost.sharedByPlayer.userName length] > 0) {
        factorDueToShareString = 25;
    } else {
        factorDueToShareString = 30;
    }
    
    NSLog(@"%f",size.height + HEIGHT_OF_REST_OF_CELL_NO_IMAGE - factorDueToShareString);
    
    return  size.height + HEIGHT_OF_REST_OF_CELL_NO_IMAGE - factorDueToShareString;
}

- (CGFloat)calculateHeightForAboutSection
{
    NSString *text = self.aboutString;
    
    CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH, 1000.0f);
    
    /********** because sizeWithFont deprecated *************/
    
    NSMutableDictionary *attributes = [NSMutableDictionary new];
    [attributes setObject:[UIFont systemFontOfSize:FONT_SIZE] forKey:NSFontAttributeName];
    
    CGRect sizeRect = [text boundingRectWithSize:constraint options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
    
    CGSize size = sizeRect.size;
    //      CGSize size = [text sizeWithFont:[UIFont systemFontOfSize:FONT_SIZE] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    
    /********* end because sizeWithFont deprecated *********/
    
    
    
    return  size.height + 50;
}


- (IBAction)matchDetailsIndividualMatchCreation:(UIButton *)sender
{
    self.selectedSection = sender.tag;
    
    [self performSegueWithIdentifier:@"Individual Match Detail Segue" sender:self];
}

- (IBAction)matchDetailsIndividualMatchFinalScore:(UIButton *)sender
{
    self.selectedSection = sender.tag;
    
    [self performSegueWithIdentifier:@"Individual Match Detail Segue" sender:self];
}

- (IBAction)matchDetailsForPickupMatchCreation:(UIButton *)sender
{
    self.selectedSection = sender.tag;
    [self performSegueWithIdentifier:@"Pickup Match Detail Segue" sender:self];
}

- (void)commentPost:(UIButton *)sender
{
    self.selectedSection = sender.tag;
    [self performSegueWithIdentifier:@"Comment Segue" sender:self];
}

- (void)goToProfile:(UIButton *)sender
{
    if ([self.preferences getUserID] != self.playerUserID && self.playerUserID != sender.tag) { // not yourself, also not the one you are looking at
        self.targetID = sender.tag;
        [self performSegueWithIdentifier:@"playerdetail_segue_from_self" sender:sender];
    }
}

- (void)goToVenue:(UIButton *)sender
{
    self.selectedVenue = sender.tag;
    [self performSegueWithIdentifier:@"Venue Detail Segue" sender:self];
}

- (void)setViewController:(PDNewsIndividualMatchPostCell *)controller withUserIDToSegue:(NSInteger)userID
{

}

- (void)setPDMatchNewsFinalScoreUpdateCellParentViewController:(PDMatchNewsFinalScoreUpdateCell *)controller withUserIDToSegue:(NSInteger)userID
{

}

- (void)setPDNewsPickupMatchPostCellViewController:(PDNewsPickupMatchPostCell *)controller withUserIDToSegue:(NSInteger)userID
{
    self.targetID = userID;
}

- (void)setWithNumCommentsFromVC:(CommentsTVC *)controller withNumComments:(NSUInteger)numComments forSection:(NSUInteger)currentTableSection
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:currentTableSection];
    
    if ([[self.newsfeedPaginator.results objectAtIndex:indexPath.section] isKindOfClass:[IndividualMatchCreationNews class]]) {
        IndividualMatchCreationNews *iNews = [self.newsfeedPaginator.results objectAtIndex:currentTableSection];
        iNews.numComments = numComments;
        
    } else if ([[self.newsfeedPaginator.results objectAtIndex:indexPath.section] isKindOfClass:[PickupMatchCreationNews class]]) {
        PickupMatchCreationNews *pNews = [self.newsfeedPaginator.results objectAtIndex:currentTableSection];
        pNews.numComments = numComments;
        
    } else if ([[self.newsfeedPaginator.results objectAtIndex:indexPath.section] isKindOfClass:[ImagePost class]]) {
        ImagePost *iPost = [self.newsfeedPaginator.results objectAtIndex:currentTableSection];
        iPost.numComments = numComments;
        
    } else if ([[self.newsfeedPaginator.results objectAtIndex:indexPath.section] isKindOfClass:[TextPost class]]) {
        TextPost *tPost = [self.newsfeedPaginator.results objectAtIndex:currentTableSection];
        tPost.numComments = numComments;
    } else if ([[self.newsfeedPaginator.results objectAtIndex:indexPath.section] isKindOfClass:[MatchPlayerNewsOfficialFinishedIndividualMatch class]]) {
        MatchPlayerNewsOfficialFinishedIndividualMatch *mPost = [self.newsfeedPaginator.results objectAtIndex:currentTableSection];
        mPost.numComments = numComments;
    } else if ([[self.newsfeedPaginator.results objectAtIndex:indexPath.section] isKindOfClass:[CompetitionNews class]]) {
        CompetitionNews *cPost = [self.newsfeedPaginator.results objectAtIndex:currentTableSection];
        cPost.numComments = numComments;
    } else if ([[self.newsfeedPaginator.results objectAtIndex:indexPath.section] isKindOfClass:[VenueKingNews class]]) {
        VenueKingNews *vPost = [self.newsfeedPaginator.results objectAtIndex:currentTableSection];
        vPost.numComments = numComments;
    } else if ([[self.newsfeedPaginator.results objectAtIndex:indexPath.section] isKindOfClass:[VenueCheckinNews class]]) {
        VenueCheckinNews *vPost = [self.newsfeedPaginator.results objectAtIndex:currentTableSection];
        vPost.numComments = numComments;
    } else if ([[self.newsfeedPaginator.results objectAtIndex:indexPath.section] isKindOfClass:[LockerPost class]]) {
        LockerPost *iPost = [self.newsfeedPaginator.results objectAtIndex:currentTableSection];
        iPost.numComments = numComments;
        
    }
    
    [self.tableView beginUpdates];
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    [self.tableView endUpdates];
    //   self.scrollToSection = currentTableSection;
    
    

}

- (IBAction)remixTextPost:(UIButton *)sender
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:sender.tag];
    TextPost *tPost = [self.newsfeedPaginator.results objectAtIndex:sender.tag];
    
    self.remixedIndexPath = indexPath;
    self.remixedNewsID = tPost.newsID;
    
    NSString *checkIfPostCanBeShared = [tPost canPostBeSharedBySessionUser:[self.preferences getUserID]];
    self.fbMessage = tPost.textPost;
    //    self.fbImage = [UIImage imageNamed:@"Vaderen Tab"];
    //    self.fbTitle = [NSString stringWithFormat:@"%@ made a post in the Vaiden App", tPost.sharedByPlayer.userName];
    
    
    if ([checkIfPostCanBeShared isEqualToString:@"OK"]) {
        if (tPost.didSessionUserRemixPost) {
            
            [self showActionSheetForRemixNewsPost:UNSHARE_POST
                                    andSheetTitle:TITLE_OF_ACTIONSHEET_ALREADY_SHARED
                                     showFBButton:NO];
            
        } else {
            [self showActionSheetForRemixNewsPost:SHARE_POST
                                    andSheetTitle:TITLE_OF_ACTIONSHEET_SHARE
                                     showFBButton:NO];
            
        }
    } else {
        
        
    }
}


- (IBAction)remixImagePost:(UIButton *)sender
{
    /*
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:sender.tag];
    ImagePost *iPost = [self.newsfeedPaginator.results objectAtIndex:sender.tag];
    
    self.remixedIndexPath = indexPath;
    self.remixedNewsID = iPost.newsID;
    
    //   UIImageView *tempUIV = [[UIImageView alloc] init];
    
    NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                            baseString:iPost.imageBaseFileString];
    
    
    NSString *checkIfPostCanBeShared = [iPost canPostBeSharedBySessionUser:[self.preferences getUserID]];
    
    self.fbMessage = iPost.textPost;
  //  self.fbImage = [NSURL URLWithString:url];
    self.fbTitle = [NSString stringWithFormat:@"%@ shared an image from the Vaiden App", iPost.sharedByPlayer.userName];
    if ([checkIfPostCanBeShared isEqualToString:@"OK"]) {
        if (iPost.didSessionUserRemixPost) {
            [self showActionSheetForRemixNewsPost:UNSHARE_POST
                                    andSheetTitle:TITLE_OF_ACTIONSHEET_ALREADY_SHARED
                                     showFBButton:NO];
            
        } else {
            [self showActionSheetForRemixNewsPost:SHARE_POST
                                    andSheetTitle:TITLE_OF_ACTIONSHEET_SHARE
                                     showFBButton:NO];
        }
    } else {
        
    }*/
}


- (IBAction)remixCompetitionPost:(UIButton *)sender
{
/*    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:sender.tag];
    CompetitionNews *cPost = [self.newsfeedPaginator.results objectAtIndex:sender.tag];
    
    self.remixedIndexPath = indexPath;
    self.remixedNewsID = cPost.newsID;
    
    NSString *checkIfPostCanBeSharedInNewsfeed = [cPost canPostBeSharedBySessionUser:[self.preferences getUserID]];
    
    self.fbMessage = @"The 1v1 Intercollegiate Basketball Championships.  College students are welcome to register to compete!";
    //   self.fbImage = [NSURL URLWithString:url];
    self.fbTitle = [NSString stringWithFormat:@"%@ is playing in a competition", cPost.newsMakerUserName];
    if ([checkIfPostCanBeSharedInNewsfeed isEqualToString:@"OK"]) {
        if (cPost.didSessionUserRemixPost) {
            
            [self showActionSheetForRemixNewsPost:UNSHARE_POST
                                    andSheetTitle:TITLE_OF_ACTIONSHEET_ALREADY_SHARED
                                     showFBButton:YES];
        } else {
            [self showActionSheetForRemixNewsPost:SHARE_POST
                                    andSheetTitle:TITLE_OF_ACTIONSHEET_SHARE
                                     showFBButton:YES];
        }
    } else {
        [self showActionSheetForOnlyFBShare];
    }*/
}


- (IBAction)remixVenueKingPost:(UIButton *)sender
{
 /*   NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:sender.tag];
    VenueKingNews *vPost= [self.newsfeedPaginator.results objectAtIndex:sender.tag];
    
    self.remixedIndexPath = indexPath;
    self.remixedNewsID = vPost.newsID;
    
    NSString *checkIfPostCanBeSharedInNewsfeed = [vPost canPostBeSharedBySessionUser:[self.preferences getUserID]];
    
    self.fbMessage = [NSString stringWithFormat:@"%@ for %@", vPost.venue.venueName, [vPost.sport.sportName capitalizedString]];
    self.fbTitle = [NSString stringWithFormat:@"%@ became King Of a Venue", vPost.newsMakerUserName];

    if ([checkIfPostCanBeSharedInNewsfeed isEqualToString:@"OK"]) {
        if (vPost.didSessionUserRemixPost) {
            
            [self showActionSheetForRemixNewsPost:UNSHARE_POST
                                    andSheetTitle:TITLE_OF_ACTIONSHEET_ALREADY_SHARED
                                     showFBButton:YES];
        } else {
            [self showActionSheetForRemixNewsPost:SHARE_POST
                                    andSheetTitle:TITLE_OF_ACTIONSHEET_SHARE
                                     showFBButton:YES];
        }
    } else {
        [self showActionSheetForOnlyFBShare];
    }*/
}


- (IBAction)remixCheckinPost:(UIButton *)sender
{
 /*   NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:sender.tag];
    VenueCheckinNews *vPost= [self.newsfeedPaginator.results objectAtIndex:sender.tag];
    
    self.remixedIndexPath = indexPath;
    self.remixedNewsID = vPost.newsID;
    
    NSString *checkIfPostCanBeSharedInNewsfeed = [vPost canPostBeSharedBySessionUser:[self.preferences getUserID]];
    
    NSString *sportName = [vPost.vci.checkedInSport.sportName capitalizedString];
    
    if (vPost.vci.isNow) {
        if ([sportName length] > 0)
            self.fbTitle = [NSString stringWithFormat:@"@%@ checked in at %@ to play %@", vPost.vci.checkedInUser.userName, vPost.venue.venueName, sportName];
        else
            self.fbTitle = [NSString stringWithFormat:@"@%@ checked in at %@", vPost.vci.checkedInUser.userName, vPost.venue.venueName];
    } else {
        if ([sportName length] > 0)
            self.fbTitle = [NSString stringWithFormat:@"@%@ will be at %@ at %@ to play %@", vPost.vci.checkedInUser.userName, vPost.venue.venueName, [vPost.vci.checkedInDateTime getDateFullDateString], sportName];
        else
            self.fbTitle = [NSString stringWithFormat:@"@%@ will be at %@ at %@", vPost.vci.checkedInUser.userName, vPost.venue.venueName, [vPost.vci.checkedInDateTime getDateFullDateString]];
    }
    
    
    if ([vPost.vci.message length] > 0)
        self.fbMessage = [NSString stringWithFormat:@"%@ says: \"%@\"", vPost.vci.checkedInUser.userName, vPost.vci.message];
    else
        self.fbMessage = @"";
    
    if ([checkIfPostCanBeSharedInNewsfeed isEqualToString:@"OK"]) {
        if (vPost.didSessionUserRemixPost) {
            
            [self showActionSheetForRemixNewsPost:UNSHARE_POST
                                    andSheetTitle:TITLE_OF_ACTIONSHEET_ALREADY_SHARED
                                     showFBButton:YES];
        } else {
            [self showActionSheetForRemixNewsPost:SHARE_POST
                                    andSheetTitle:TITLE_OF_ACTIONSHEET_SHARE
                                     showFBButton:YES];
        }
    } else {
        [self showActionSheetForOnlyFBShare];
    }
    */
}

- (IBAction)remixIndividualMatchPost:(UIButton *)sender
{
    /*
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:sender.tag];
    IndividualMatchCreationNews *iPost = [self.newsfeedPaginator.results objectAtIndex:sender.tag];
    
    self.remixedIndexPath = indexPath;
    self.remixedNewsID = iPost.newsID;
    self.fbTitle = [NSString stringWithFormat:@"%@ challenged %@", iPost.individualMatch.matchCreatorUsername, [iPost.individualMatch getOtherPlayer:iPost.individualMatch.matchCreatorID].userName];
    
    NSString *checkIfPostCanBeSharedInNewsfeed = [iPost canPostBeSharedBySessionUser:[self.preferences getUserID]];
    
    self.fbMessage = [NSString stringWithFormat:@"Learn more about this %@ Challenge Match on the Vaiden iPhone App", [iPost.individualMatch.sport.sportName capitalizedString]];
    //  self.fbImage = [UIImage imageNamed:@"Vaderen Tab"];
    
    
    if ([checkIfPostCanBeSharedInNewsfeed isEqualToString:@"OK"]) {
        if (iPost.didSessionUserRemixPost) {
            
            [self showActionSheetForRemixNewsPost:UNSHARE_POST
                                    andSheetTitle:TITLE_OF_ACTIONSHEET_ALREADY_SHARED
                                     showFBButton:YES];
        } else {
            [self showActionSheetForRemixNewsPost:SHARE_POST
                                    andSheetTitle:TITLE_OF_ACTIONSHEET_SHARE
                                     showFBButton:YES];
        }
    } else {
        [self showActionSheetForOnlyFBShare];
    }*/
 
}

- (IBAction)remixPickupMatchPost:(UIButton *)sender
{
/*
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:sender.tag];
    PickupMatchCreationNews *pPost = [self.newsfeedPaginator.results objectAtIndex:sender.tag];
    
    self.remixedIndexPath = indexPath;
    self.remixedNewsID = pPost.newsID;
    self.fbTitle = [NSString stringWithFormat:@"%@ created a pickup match", pPost.pickupMatch.matchCreatorUsername];
    
    NSString *checkIfPostCanBeSharedInNewsfeed = [pPost canPostBeSharedBySessionUser:[self.preferences getUserID]];
    
    self.fbMessage = [NSString stringWithFormat:@"Learn more about this %@ Pickup Match on the Vaiden iPhone App", [pPost.pickupMatch.sport.sportName capitalizedString]];
    //  self.fbImage = [UIImage imageNamed:@"Vaderen Tab"];
    
    
    if ([checkIfPostCanBeSharedInNewsfeed isEqualToString:@"OK"]) {
        if (pPost.didSessionUserRemixPost) {
            
            [self showActionSheetForRemixNewsPost:UNSHARE_POST
                                    andSheetTitle:TITLE_OF_ACTIONSHEET_ALREADY_SHARED
                                     showFBButton:YES];
        } else {
            [self showActionSheetForRemixNewsPost:SHARE_POST
                                    andSheetTitle:TITLE_OF_ACTIONSHEET_SHARE
                                     showFBButton:YES];
        }
    } else {
        [self showActionSheetForOnlyFBShare];
    }
 */
}


- (IBAction)remixMatchPlayerScoreConfirmedNewsPost:(UIButton *)sender
{
    /*
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:sender.tag];
    MatchNews *iPost = [self.newsfeedPaginator.results objectAtIndex:sender.tag];
    
    self.remixedIndexPath = indexPath;
    self.remixedNewsID = iPost.newsID;
    
    IndividualMatch *iMatch = (IndividualMatch *)iPost.match;
    NSString *checkIfPostCanBeSharedInNewsfeed = [iPost canPostBeSharedBySessionUser:[self.preferences getUserID]];
    
    self.fbMessage  = [NSString stringWithFormat:@"Learn more about this %@ Challenge Match on the Vaiden iPhone App", [iMatch.sport.sportName capitalizedString]];
    //    self.fbImage = [UIImage imageNamed:@"Vaderen Tab"];
    self.fbTitle = [NSString stringWithFormat:@"%@ vs %@", iMatch.player1.userName, iMatch.player2.userName];
    
    
    if ([checkIfPostCanBeSharedInNewsfeed isEqualToString:@"OK"]) {
        if (iPost.didSessionUserRemixPost) {
            [self showActionSheetForRemixNewsPost:UNSHARE_POST
                                    andSheetTitle:TITLE_OF_ACTIONSHEET_ALREADY_SHARED
                                     showFBButton:YES
             ];
            
        } else {
            [self showActionSheetForRemixNewsPost:SHARE_POST
                                    andSheetTitle:TITLE_OF_ACTIONSHEET_SHARE
                                     showFBButton:YES
             ];
            
        }
    } else {
        [self showActionSheetForOnlyFBShare];
    }
     */
}



- (void)showActionSheetForRemixNewsPost:(NSString *)buttonTitle
                          andSheetTitle:(NSString *)sheetTitle
                           showFBButton:(BOOL)showFBButton
{
    
    if (showFBButton) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                      initWithTitle:sheetTitle delegate:self
                                      cancelButtonTitle:CANCEL_BUTTON_ACTIONSHEET destructiveButtonTitle:nil
                                      otherButtonTitles:FACEBOOK_SHARE, buttonTitle,  nil];
        [actionSheet showFromTabBar:self.tabBarController.tabBar];
    } else {
        UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                      initWithTitle:sheetTitle delegate:self
                                      cancelButtonTitle:CANCEL_BUTTON_ACTIONSHEET destructiveButtonTitle:nil
                                      otherButtonTitles:buttonTitle,  nil];
        [actionSheet showFromTabBar:self.tabBarController.tabBar];
    }
    
}

- (void)showActionSheetForRemixMatchNewsPost:(NSString *)buttonTitle
                               andSheetTitle:(NSString *)sheetTitle
                                showFBButton:(BOOL)showFBButton

{
    if (showFBButton) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                      initWithTitle:sheetTitle delegate:self
                                      cancelButtonTitle:CANCEL_BUTTON_ACTIONSHEET destructiveButtonTitle:nil
                                      otherButtonTitles:FACEBOOK_SHARE, buttonTitle,  nil];
        [actionSheet showFromTabBar:self.tabBarController.tabBar];
    } else {
        UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                      initWithTitle:sheetTitle delegate:self
                                      cancelButtonTitle:CANCEL_BUTTON_ACTIONSHEET destructiveButtonTitle:nil
                                      otherButtonTitles:buttonTitle,  nil];
        [actionSheet showFromTabBar:self.tabBarController.tabBar];
    }
}


- (void)showActionSheetForOnlyFBShare
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:TITLE_OF_ACTIONSHEET_SHARE delegate:self
                                  cancelButtonTitle:CANCEL_BUTTON_ACTIONSHEET destructiveButtonTitle:nil
                                  otherButtonTitles:FACEBOOK_SHARE,  nil];
    [actionSheet showFromTabBar:self.tabBarController.tabBar];
}

- (IBAction)playerDetailActions:(id)sender
{
    if (self.playerUserID != [self.preferences getUserID]) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                      initWithTitle:nil delegate:self
                                      cancelButtonTitle:CANCEL_BUTTON_ACTIONSHEET destructiveButtonTitle:nil
                                      otherButtonTitles:BLOCK_USER,  nil];
        [actionSheet showFromTabBar:self.tabBarController.tabBar];
    }
}

- (void)blockUser
{
    HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    HUD.labelText = @"Upating";
    [IOSRequest blockUser:self.playerUserID
                  forUser:[self.preferences getUserID]
             onCompletion:^(NSDictionary *results) {
                 
                 dispatch_async(dispatch_get_main_queue(), ^{
                    
                     if ([results[@"outcome"] isEqualToString:@"success"]) {
                         HUD.labelText = @"User Blocked!";
                         double delayInSeconds = 2.0;
                         dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
                         dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                             //code to be executed on the main queue after delay
                             [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
                             [self.preferences setProfilePageRefreshState:YES];
                             [self.navigationController popViewControllerAnimated:YES];
                         });

                     } else if ([results[@"outcome"] isEqualToString:@"failure"]) {
                         [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
                         UIAlertView *alert = [[UIAlertView alloc] init];
                         alert.title = @"Submission Error";
                         alert.message = @"There was a problem completing your request.  Please try again.";
                         [alert addButtonWithTitle:@"OK"];
                         [alert show];

                     }
                 });
             }];
    double delayInSeconds = 10.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        //code to be executed on the main queue after delay
        [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
    });
}

- (void)submitRemixPost
{
    [IOSRequest makeRemixForNewsfeed:self.remixedNewsID
                              byUser:[self.preferences getUserID]
                        onCompletion:^(NSDictionary *results) {
                            
                            dispatch_async(dispatch_get_main_queue(), ^ {
                                [self.tableView beginUpdates];
                                
                                id post = [self.newsfeedPaginator.results objectAtIndex:self.remixedIndexPath.section];
                                
                                if ([post isKindOfClass:[News class]]) {
                                    News *nPost = (News *)post;
                                    nPost.numRemixes = [results[@"num_remixes"] integerValue];
                                    nPost.didSessionUserRemixPost = [results[@"did_session_user_remix_post"] boolValue];
                                    
                                }
                                
                                [self.tableView reloadRowsAtIndexPaths:@[self.remixedIndexPath] withRowAnimation:UITableViewRowAnimationNone];
                                [self.tableView endUpdates];
                                
                                self.remixedIndexPath = nil;
                                self.remixedNewsID = 0;
                            });
                            
                        }];
    
}


- (void)startFBShare
{
    
  
    if (FBSession.activeSession.isOpen && [FBSession.activeSession.permissions indexOfObject:@"publish_actions"] == NSNotFound) {
        // Request publish_actions
        [FBSession.activeSession requestNewPublishPermissions:[NSArray arrayWithObject:@"publish_actions"]
                                              defaultAudience:FBSessionDefaultAudienceFriends
                                            completionHandler:^(FBSession *session, NSError *error) {
                                                __block NSString *alertText;
                                                __block NSString *alertTitle;
                                                if (!error) {
                                                    if ([FBSession.activeSession.permissions
                                                         indexOfObject:@"publish_actions"] == NSNotFound){
                                                        // Permission not granted, tell the user we will not publish
                                                        alertTitle = @"Permission not granted";
                                                        alertText = @"Your action will not be published to Facebook.";
                                                        [[[UIAlertView alloc] initWithTitle:@"title"
                                                                                    message:@"text"
                                                                                   delegate:self
                                                                          cancelButtonTitle:@"OK!"
                                                                          otherButtonTitles:nil] show];
                                                    } else {
                                                        // Permission granted, publish the OG story
                                                        [self presentFBDialog];
                                                    }
                                                    
                                                } else {
                                                    // There was an error, handle it
                                                    // See https://developers.facebook.com/docs/ios/errors/
                                                }
                                            }];
        
    } else {
        // permission exists
        [self presentFBDialog];
    }
    
    
    
    
    
}

- (void)presentFBDialog
{
    // Present share dialog
    if (!self.isLockerShare) {
        // Put together the dialog parameters
        NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                       self.fbTitle, @"name",
                                       self.fbMessage, @"caption",
                                       @"Vaiden is a community for athletes to play their favorite sports using a social platform.", @"description",
                                       @"https://itunes.apple.com/us/app/vaiden/id796761406?ls=1&mt=8", @"link",
                                       self.fbImageString, @"picture",
                                       nil];
        
        // Show the feed dialog
        [FBWebDialogs presentFeedDialogModallyWithSession:nil
                                               parameters:params
                                                  handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
                                                      if (error) {
                                                          // An error occurred, we need to handle the error
                                                          // See: https://developers.facebook.com/docs/ios/errors
                                                          //                    NSLog([NSString stringWithFormat:@"Error publishing story: %@", error.description]);
                                                      } else {
                                                          if (result == FBWebDialogResultDialogNotCompleted) {
                                                              // User cancelled.
                                                              NSLog(@"User cancelled.");
                                                          } else {
                                                              // Handle the publish feed callback
                                                              NSDictionary *urlParams = [self parseURLParams:[resultURL query]];
                                                              
                                                              if (![urlParams valueForKey:@"post_id"]) {
                                                                  // User cancelled.
                                                                  NSLog(@"User cancelled.");
                                                                  
                                                              } else {
                                                                  // User clicked the Share button
                                                                  NSString *result = [NSString stringWithFormat: @"Posted story, id: %@", [urlParams valueForKey:@"post_id"]];
                                                                  NSLog(@"result %@", result);
                                                              }
                                                          }
                                                      }
                                                  }];
    } else {
        [self performSegueWithIdentifier:@"player_share_segue" sender:self];
    }
}

- (void)startShareImage
{
    // Loading overlay
    HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    HUD.labelText = @"Sharing...";
    
    NSMutableDictionary* params = [[NSMutableDictionary alloc] init];
    NSString *myMessage = self.fbMessage;
    [params setObject:myMessage forKey:@"message"]; // caption showed on the top of the sharing image
    
    /*UIImageView *tmpView = [[UIImageView alloc] init];
     [tmpView setImageWithURL:[NSURL URLWithString:self.fbImageString]
     placeholderImage:[UIImage imageNamed:@"avatar square"]];*/
    UIImage * image = [self addOverlayToPostImage:self.currentPickedImage];
    
    [params setObject:UIImagePNGRepresentation(image) forKey:@"picture"];
    
    [FBRequestConnection startWithGraphPath:@"me/photos"
                                 parameters:params
                                 HTTPMethod:@"POST"
                          completionHandler:^(FBRequestConnection *connection,
                                              id result,
                                              NSError *error)
     {
         [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
         if (error){
             UIAlertView *alert = [[UIAlertView alloc] init];
             alert.title = @"Submission Error!";
             alert.message = [NSString stringWithFormat:@"%@", error];
             [alert addButtonWithTitle:@"OK"];
             [alert show];
         } else {
             HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
             HUD.labelText = @"Shared!";
             //[HUD setMode:MBProgressHUDModeText];
             double delayInSeconds = 1.0; // delay one sec for the sucess message, and dismiss
             dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
             dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                 [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
             });
         }
     }];
}

- (UIImage *)addOverlayToPostImage:(UIImage *)sourcePic
{
    int total = self.stats.count;
    
    UIImage *rightCornerLogo = [UIImage imageNamed:@"vaiden_overlay_logo"];
    UIImage *bgShadow = [UIImage imageNamed:@"overlay_shadow"];
    
    if (total > 0) {
        UIImage *seperator = [UIImage imageNamed:@"seperator"];
        UIImage *lineImage;
        
        // draw the lines for single stat
        if (total == 1) {
            CGSize imageSize = CGSizeMake(60, 1);
            UIColor *fillColor = [UIColor whiteColor];
            UIGraphicsBeginImageContextWithOptions(imageSize, YES, 0);
            CGContextRef context = UIGraphicsGetCurrentContext();
            [fillColor setFill];
            CGContextFillRect(context, CGRectMake(0, 0, imageSize.width, imageSize.height));
            lineImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
        }
        
        CGSize size = CGSizeMake(sourcePic.size.width, sourcePic.size.height);
        
        UIGraphicsBeginImageContext(size);
        
        [sourcePic drawInRect:CGRectMake(0, 0, size.width, size.height)];
        [bgShadow drawInRect:CGRectMake(0, size.height-bgShadow.size.height, size.width, bgShadow.size.height)];
        [rightCornerLogo drawInRect:CGRectMake(size.width-rightCornerLogo.size.width-20, size.height-rightCornerLogo.size.height-10, rightCornerLogo.size.width, rightCornerLogo.size.height)];
        
        // Write the stats upon the image
        NSString *upperStr = @"";
        NSString *lowerStr = @"";
        
        for (int i=0; i<total; i++) {
            id test = [self.stats objectAtIndex:i];
            if ([test isKindOfClass:[StatTag class]]) {
                StatTag *stat = (StatTag *)test;
                if (stat.showSubCategory) {
                    upperStr = stat.statTagValue;
                    lowerStr = [NSString stringWithFormat:@"%@ %@",stat.statLink, stat.statTagName];
                } else {
                    upperStr = stat.statTagValue;
                    lowerStr = stat.statTagName;
                }
            } else if ([test isKindOfClass:[StatTagCombination class]]) {
                StatTagCombination *sta = (StatTagCombination *)test;
                upperStr = [NSString stringWithFormat:@"%@/%@", sta.numerator.statTagValue, sta.denominator.statTagValue];
                lowerStr = sta.numerator.statLink;
            }
            
            CGFloat width = size.width - rightCornerLogo.size.width - 30; //10 for space
            if (total == 1) {
                width = width/2;
            } else if (total == 2) {
                width = width/2;
            } else if (total == 3) {
                width = width/3;
            }
            
            UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:15];
            CGSize lineSize = [upperStr sizeWithAttributes:@{NSFontAttributeName:font}];
            CGRect upperRect;
            if (total == 1) {
                upperRect = CGRectMake(width - lineSize.width/2, size.height- (10 + 35), size.width, width-10);
            } else {
                upperRect = CGRectMake(width*i + width/2 - lineSize.width/2, size.height- (10 + 35), size.width, width-10);
            }
            [[UIColor whiteColor] set];
            [upperStr drawInRect:CGRectIntegral(upperRect) withFont:font];
            
            if (i < total - 1) { // Not the last item
                [seperator drawInRect:CGRectMake(width*(i+1), size.height- (5 + 23), seperator.size.width, seperator.size.height)];
            }
            
            UIFont *font2 = [UIFont fontWithName:@"HelveticaNeue" size:9];
            CGSize lineSize2 = [lowerStr sizeWithAttributes:@{NSFontAttributeName:font2}];
            if (lineSize2.width > width - 10) { // check if the string is out of bound, if so wrap the string by space
                NSArray * sepStr = [self wrapSentences:lowerStr withFont:font2 withinBounds:(width - 10)];
                for (int k=0; k<sepStr.count; k++) {
                    lineSize2 = [[sepStr objectAtIndex:k] sizeWithAttributes:@{NSFontAttributeName:font2}];
                    CGRect lowerRect = CGRectMake(width*i + width/2 - lineSize2.width/2, size.height- (17 + k*8), size.width, width-10);
                    [[sepStr objectAtIndex:k] drawInRect:CGRectIntegral(lowerRect) withFont:font2];
                }
            } else {
                CGRect lowerRect;
                if (total == 1) {
                    lowerRect = CGRectMake(width - lineSize2.width/2, size.height- (10 + 12), size.width, width-10);
                } else {
                    lowerRect = CGRectMake(width*i + width/2 - lineSize2.width/2, size.height- (10 + 12), size.width, width-10);
                }
                [lowerStr drawInRect:CGRectIntegral(lowerRect) withFont:font2];
            }
            
            if (total == 1) {
                [lineImage drawInRect:CGRectMake(width - lineSize2.width/2 - 10 - lineImage.size.width, size.height - (10 + 17), lineImage.size.width, lineImage.size.height)];
                [lineImage drawInRect:CGRectMake(width + lineSize2.width/2 + 10, size.height - (10 + 17), lineImage.size.width, lineImage.size.height)];
            }
        }
        
        UIImage *finalImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return finalImage;
    } else if (self.drills.count > 0) {
        UIImage *drillIcon = [UIImage imageNamed:@"white_drill"];
        
        CGSize size = CGSizeMake(sourcePic.size.width, sourcePic.size.height);
        UIGraphicsBeginImageContext(size);
        
        [sourcePic drawInRect:CGRectMake(0, 0, size.width, size.height)];
        [bgShadow drawInRect:CGRectMake(0, size.height-bgShadow.size.height, size.width, bgShadow.size.height)];
        [rightCornerLogo drawInRect:CGRectMake(size.width-rightCornerLogo.size.width-20, size.height-rightCornerLogo.size.height-10, rightCornerLogo.size.width, rightCornerLogo.size.height)];
        [drillIcon drawInRect:CGRectMake(10, size.height-drillIcon.size.height - 28, drillIcon.size.width, drillIcon.size.height)];
        
        // Write the drills upon the image
        NSString *drillText = @"";
        
        for (TrainingTag *tag in self.drills) {
            if ([drillText length] > 0)
                drillText = [drillText stringByAppendingString:@", "];
            drillText = [drillText stringByAppendingString:tag.trainingTagName];
        }
        
        CGFloat width = size.width - rightCornerLogo.size.width - 30; //10 for space
        
        UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:10];
        //CGSize lineSize = [drillText sizeWithAttributes:@{NSFontAttributeName:font}];
        CGRect textFrame = CGRectMake(10 + drillIcon.size.width + 5, size.height- (10 + 30), width-30, width-10);
        [[UIColor whiteColor] set];
        [drillText drawInRect:CGRectIntegral(textFrame) withFont:font];
        
        UIImage *finalImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return finalImage;
    } else {
        return sourcePic;
    }
}


/* Wrap the sentence without UILabelView by space character based on bounds and font */
- (NSMutableArray *) wrapSentences:(NSString *)source withFont:(UIFont *)font withinBounds:(CGFloat)bounds
{
    NSArray *sec = [source componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSMutableArray * sperateString = [[NSMutableArray alloc] init];
    NSString * part = @"";
    NSString * tmp = @"";
    CGSize tmpSize;
    for (int i=0; i<sec.count; i++) {
        if (part.length == 0) {
            part = (NSString *)[sec objectAtIndex:i];
        } else {
            tmp = [part stringByAppendingString:[NSString stringWithFormat:@" %@", (NSString *)[sec objectAtIndex:i]]];
            tmpSize = [tmp sizeWithAttributes:@{NSFontAttributeName:font}];
        }
        if (tmpSize.width > bounds) {
            [sperateString addObject:part];
            part = @""; // clear string
            tmpSize = CGSizeZero;
            i--; //back to last item
        } else if (i==sec.count-1) { //last item of the array
            [sperateString addObject:part];
        } else if (tmp.length > 0){ // otherwise
            part = tmp;
        }
    }
    NSLog(@"%@",sperateString);
    return sperateString;
}

// A function for parsing URL parameters returned by the Feed Dialog.
- (NSDictionary*)parseURLParams:(NSString *)query {
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *pair in pairs) {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        NSString *val =
        [kv[1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        params[kv[0]] = val;
    }
    return params;
}


- (IBAction)competitionDetailsAction:(id)sender
{
    [self performSegueWithIdentifier:@"Competition Details Segue" sender:self];
}


- (IBAction)venueKingDetailsAction:(UIButton *)sender
{
    self.selectedVenue = sender.tag;
    [self performSegueWithIdentifier:@"Venue Detail Segue" sender:self];
}

- (IBAction)actionBarButtonAction:(id)sender
{

}

- (void)postExtraActions:(UIButton *)sender
{
    NSInteger userIDToTest = 0;
    
    News *lp = (self.newsfeedPaginator.results)[sender.tag];
    self.selectedSection = sender.tag;
    
    if ([lp isKindOfClass:[IndividualMatchCreationNews class]]) {
        IndividualMatchCreationNews *n = (IndividualMatchCreationNews *)lp;
        userIDToTest = n.individualMatch.matchCreatorID;
        
        self.fbTitle = [NSString stringWithFormat:@"%@ challenged %@", n.individualMatch.matchCreatorUsername, [n.individualMatch getOtherPlayer:n.individualMatch.matchCreatorID].userName];
        self.fbMessage = [NSString stringWithFormat:@"Learn more about this %@ Challenge Match on the Vaiden iPhone App", [n.individualMatch.sport.sportName capitalizedString]];
        self.fbImageString = @"https://s3.amazonaws.com/vaderen_pictures/vaiden_fb_icon.png";
        self.isLockerShare = NO;
    } else if ([lp isKindOfClass:[MatchPlayerNewsOfficialFinishedIndividualMatch class]]) {
        MatchPlayerNewsOfficialFinishedIndividualMatch *n = (MatchPlayerNewsOfficialFinishedIndividualMatch *)lp;
        IndividualMatch *m = (IndividualMatch *)n.match;
        userIDToTest = n.match.matchCreatorID;
        self.fbTitle = [NSString stringWithFormat:@"%@ challenged %@", n.match.matchCreatorUsername, [m getOtherPlayer:n.match.matchCreatorID].userName];
        self.fbMessage = [NSString stringWithFormat:@"Learn more about this %@ Challenge Match on the Vaiden iPhone App", [n.match.sport.sportName capitalizedString]];
        self.fbImageString = @"https://s3.amazonaws.com/vaderen_pictures/vaiden_fb_icon.png";
        self.isLockerShare = NO;
    } else if ([lp isKindOfClass:[PickupMatchCreationNews class]]) {
        PickupMatchCreationNews *n = (PickupMatchCreationNews *)lp;
        userIDToTest = n.pickupMatch.matchCreatorID;
        
        self.fbTitle = [NSString stringWithFormat:@"%@ created a pickup match", n.pickupMatch.matchCreatorUsername];
        self.fbMessage = [NSString stringWithFormat:@"Learn more about this %@ Pickup Match on the Vaiden iPhone App", [n.pickupMatch.sport.sportName capitalizedString]];
        self.fbImageString = @"https://s3.amazonaws.com/vaderen_pictures/vaiden_fb_icon.png";
        self.isLockerShare = NO;
    } else {
        userIDToTest = lp.newsMakerUserID;
    }
    
    if ([lp isKindOfClass:[LockerPost class]]) {
        LockerPost *temp = (LockerPost *)lp;
        self.stats = temp.lockPic.statsGrp.statsArray;
        self.drills = temp.lockPic.trainingTagsArray;
        self.fbImageString = [S3Tools2 getFileNameStringWithType:@"photo_regsize" baseString:[NSString stringWithFormat:@"%ld/photo/%@", temp.newsMakerUserID, temp.lockPic.imageFileName]];
        self.fbTitle = [NSString stringWithFormat:@"%@ shared an image via Vaiden", [self.preferences getUserName]];
        self.fbMessage = [NSString stringWithFormat:@"\"%@\"", temp.textPost];
        self.isLockerShare = YES;
    } else if ([lp isKindOfClass:[ImagePost class]]) {
        ImagePost *n = (ImagePost *)lp;
        
        NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                baseString:n.imageBaseFileString];
        self.fbMessage = n.textPost;
        self.fbImageString = url;
        self.fbTitle = [NSString stringWithFormat:@"%@ shared an image from the Vaiden App", n.sharedByPlayer.userName];
        self.isLockerShare = NO;
        
    } else if ([lp isKindOfClass:[TextPost class]]) {
        TextPost *n = (TextPost *)lp;
        self.fbTitle = [NSString stringWithFormat:@"%@ posts on the Vaiden App", n.newsMakerUserName];
        self.fbMessage = [NSString stringWithFormat:@"\"%@\"", n.textPost];
        self.fbImageString = @"https://s3.amazonaws.com/vaderen_pictures/vaiden_fb_icon.png";
        self.isLockerShare = NO;
    } else if ([lp isKindOfClass:[VoteReceiving class]]) {
        VoteReceiving *n = (VoteReceiving *)lp;
        if ([n.type isEqualToString:@"receiving"]) {
            self.fbTitle = [NSString stringWithFormat:@"%@ receives votes on the Vaiden App", n.newsMakerUserName];
        } else if ([n.type isEqualToString:@"giving"]) {
            self.fbTitle = [NSString stringWithFormat:@"%@ gave votes on the Vaiden App", n.newsMakerUserName];
        } else if ([n.type isEqualToString:@"thanks"]) {
            self.fbTitle = [NSString stringWithFormat:@"%@ sent thanks to %@ on the Vaiden App", n.newsMakerUserName, n.targetName];
        } else if ([n.type isEqualToString:@"level"]) {
            self.fbTitle = [NSString stringWithFormat:@"%@ reached a new vote level on the Vaiden App", n.newsMakerUserName];
        }
        self.fbMessage = [NSString stringWithFormat:@"\"%@\"", n.string];
        self.fbImageString = @"https://s3.amazonaws.com/vaderen_pictures/vaiden_fb_icon.png";
        self.isLockerShare = NO;
    } else if ([lp isKindOfClass:[CompetitionNews class]]) {
        self.isLockerShare = NO;
        CompetitionNews *n = (CompetitionNews *)lp;

        self.fbMessage = @"The 1v1 Intercollegiate Basketball Championships.  College students are welcome to register to compete!";
        self.fbImageString = @"https://s3.amazonaws.com/vaderen_pictures/vaiden_fb_icon.png";
        self.fbTitle = [NSString stringWithFormat:@"%@ is playing in a competition", n.newsMakerUserName];
        
    } else if ([lp isKindOfClass:[VenueKingNews class]]) {
        VenueKingNews *n = (VenueKingNews *)lp;
        
        self.fbMessage = [NSString stringWithFormat:@"%@ for %@", n.venue.venueName, [n.sport.sportName capitalizedString]];
        self.fbTitle = [NSString stringWithFormat:@"%@ became King Of a Venue", n.newsMakerUserName];
        self.fbImageString = @"https://s3.amazonaws.com/vaderen_pictures/vaiden_fb_icon.png";
        self.isLockerShare = NO;
    } else if ([lp isKindOfClass:[VenueCheckinNews class]]) {
        VenueCheckinNews *vPost = (VenueCheckinNews *)lp;
        self.isLockerShare = NO;
        if (vPost.vci.isNow) {
            if ([vPost.vci.checkedInSport.sportName length] > 0)
                self.fbTitle = [NSString stringWithFormat:@"@%@ checked in at %@ to play %@", vPost.vci.checkedInUser.userName, vPost.venue.venueName, vPost.vci.checkedInSport.sportName];
            else
                self.fbTitle = [NSString stringWithFormat:@"@%@ checked in at %@", vPost.vci.checkedInUser.userName, vPost.venue.venueName];
        } else {
            if ([vPost.vci.checkedInSport.sportName length] > 0)
                self.fbTitle = [NSString stringWithFormat:@"@%@ will be at %@ at %@ to play %@", vPost.vci.checkedInUser.userName, vPost.venue.venueName, [vPost.vci.checkedInDateTime getDateFullDateString], vPost.vci.checkedInSport.sportName];
            else
                self.fbTitle = [NSString stringWithFormat:@"@%@ will be at %@ at %@", vPost.vci.checkedInUser.userName, vPost.venue.venueName, [vPost.vci.checkedInDateTime getDateFullDateString]];
        }
        
        
        if ([vPost.vci.message length] > 0)
            self.fbMessage = [NSString stringWithFormat:@"%@ says: \"%@\"", vPost.vci.checkedInUser.userName, vPost.vci.message];
        else
            self.fbMessage = @"";
        
    }
    
    if (userIDToTest == [self.preferences getUserID]) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                      initWithTitle:nil delegate:self
                                      cancelButtonTitle:CANCEL_BUTTON_ACTIONSHEET destructiveButtonTitle:nil
                                      otherButtonTitles:DELETE_POST, FACEBOOK_SHARE, nil];
        [actionSheet showFromTabBar:self.tabBarController.tabBar];
    } else {
        UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                      initWithTitle:nil delegate:self
                                      cancelButtonTitle:CANCEL_BUTTON_ACTIONSHEET destructiveButtonTitle:nil
                                      otherButtonTitles:FLAG_POST, nil];
        [actionSheet showFromTabBar:self.tabBarController.tabBar];
    }
}

- (void)deletePost
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Delete Confirmation"
                                                    message:@"Are you sure you would like to delete this post?"
                                                   delegate:self cancelButtonTitle:@"Cancel"
                                          otherButtonTitles:@"Delete", nil];
    
    [alert show];
    
    
    
    
}

- (void)flagPost
{
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Report Confirmation"
                                                    message:@"Are you sure you would like to report this post as inappropriate?"
                                                   delegate:self cancelButtonTitle:@"Cancel"
                                          otherButtonTitles:@"Report", nil];
    
    [alert show];
    
    
    
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    News *n = (self.newsfeedPaginator.results)[self.selectedSection];
    
    if ([[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"Delete"]) {
        HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        HUD.labelText = @"Deleting";
        [IOSRequest deletePost:n.newsID
                        byUser:[self.preferences getUserID]
                  onCompletion:^(NSDictionary *results) {
                      
                      dispatch_async(dispatch_get_main_queue(), ^{
                          if ([results[@"outcome"] isEqualToString:@"success"]) {
                              HUD.labelText = @"Deleted!";
                              double delayInSeconds = 2.0;
                              dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
                              dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                                  //code to be executed on the main queue after delay
                                  [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
                                  [self.preferences setProfilePageRefreshState:YES];
                                  [self.newsfeedPaginator.results removeObjectAtIndex:self.selectedSection];
                                  [self.tableView reloadData];
                              });
                          } else {
                              [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
                              UIAlertView *alert = [[UIAlertView alloc] init];
                              alert.title = @"Deletion Error!";
                              alert.message = @"There was a problem completing your request.  Please try again.";
                              [alert addButtonWithTitle:@"OK"];
                              [alert show];
                          }
                      });
                  }];
        
        double delayInSeconds = 10.0;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            //code to be executed on the main queue after delay
            [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
        });
    } if ([[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"Report"]) {
        HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        HUD.labelText = @"Reporting Inappropriate";
        [IOSRequest flagPost:n.newsID
                      byUser:[self.preferences getUserID]
                withFeedback:@""
                onCompletion:^(NSDictionary *results) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if ([results[@"outcome"] isEqualToString:@"success"]) {
                            HUD.labelText = @"Post Reported!";
                            double delayInSeconds = 2.0;
                            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
                            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                                //code to be executed on the main queue after delay
                                [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
                                [self.preferences setProfilePageRefreshState:YES];
                            });
                        } else {
                            [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
                            UIAlertView *alert = [[UIAlertView alloc] init];
                            alert.title = @"Submission Error!";
                            alert.message = @"There was a problem completing your request.  Please try again.";
                            [alert addButtonWithTitle:@"OK"];
                            [alert show];
                        }
                    });
                    
                }];
    }
}

- (void)likePost:(UIButton *)sender
{
    News *n = (self.newsfeedPaginator.results)[sender.tag];
    
    if (n.doesSessionUserLikePost) {
        [IOSRequest UnLikeNewsfeedPost:n.newsID
                         sessionUserID:[self.preferences getUserID]
                          onCompletion:^(NSDictionary *results) {
                              dispatch_async(dispatch_get_main_queue(), ^{
                                  
                                  if ([results[@"outcome"] isEqualToString:@"failure"]) {
                                      UIAlertView *alert = [[UIAlertView alloc] init];
                                      alert.title = @"UnLike Error";
                                      alert.message = @"There was a problem completing your request.  Please try again later.";
                                      [alert addButtonWithTitle:@"OK"];
                                      [alert show];
                                  } else {
                                      n.doesSessionUserLikePost = NO;
                                      n.numLikes--;
                                      
                                      [self.newsfeedPaginator.results replaceObjectAtIndex:sender.tag withObject:n];
                                      
                                      // need to update in array
                                      [self.tableView reloadData];
                                  }
                              });
                              
                          }];
    } else {
        [IOSRequest likeNewsfeedPost:n.newsID
                       sessionUserID:[self.preferences getUserID]
                        onCompletion:^(NSDictionary *results) {
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                if ([results[@"outcome"] isEqualToString:@"failure"]) {
                                    UIAlertView *alert = [[UIAlertView alloc] init];
                                    alert.title = @"Like Error";
                                    alert.message = @"There was a problem completing your request.  Please try again later.";
                                    [alert addButtonWithTitle:@"OK"];
                                    [alert show];
                                } else {
                                    n.doesSessionUserLikePost = YES;
                                    n.numLikes++;
                                    
                                    [self.newsfeedPaginator.results replaceObjectAtIndex:sender.tag withObject:n];
                                    
                                    [self.tableView reloadData];
                                }
                            });
                        }];
    }
    
    
}

- (void)viewLikes:(UIButton *)sender
{
    self.selectedSection = sender.tag;
    [self performSegueWithIdentifier:@"View Likes Segue" sender:self];
}

- (void)viewComments:(UIButton *)sender
{
    self.selectedSection = sender.tag;
    self.showKeyboardOnCommentsPage = NO;
    [self performSegueWithIdentifier:@"Comment Segue" sender:self];
}

- (void)viewLockerImage:(UIButton *)sender
{
    self.selectedLockerID = sender.tag;
    [self performSegueWithIdentifier:@"Locker Detail Segue" sender:self];
}

- (IBAction)imageButtonClicked:(UIButton *)sender {
    self.targetID = sender.tag;
    [self performSegueWithIdentifier:@"playerdetail_segue_from_self" sender:self];
}

@end
