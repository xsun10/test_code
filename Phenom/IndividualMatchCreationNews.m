//
//  IndividualMatchCreationNews.m
//  Phenom
//
//  Created by James Chung on 4/24/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "IndividualMatchCreationNews.h"

@implementation IndividualMatchCreationNews


- (IndividualMatchCreationNews *) initWithIndividualMatchCreationNewsDetails:(IndividualMatch *)individualMatch
                                                        matchCreatorUserName:(NSString *)matchCreatorUserName
                                                             andProfileStr:(NSString *)profileBaseString
                                                                newsPostDate:(NSDate *)newsPostDate
                                                                 andPostType:(NSInteger)postType
                                                                   andNewsID:(NSInteger)newsID
                                                              andNumComments:(NSUInteger)numComments
                                                               andNumRemixes:(NSUInteger)numRemixes
                                                            


{
    self = [super initWithNewsDetails:individualMatch.matchCreatorID
                          andUserName:matchCreatorUserName
                        andProfileStr:profileBaseString
                             withDate:newsPostDate
                          andPostType:(NSInteger)postType
                       andNumComments:numComments
                        andNumRemixes:numRemixes
                            andNewsID:newsID];
    
    if (self) {
        
        _individualMatch = individualMatch; // do I have alloc init by copying???
        
     
    }
    return self;
}


@end
