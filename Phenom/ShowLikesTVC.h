//
//  ShowLikesTVC.h
//  Vaiden
//
//  Created by James Chung on 5/13/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "CustomBaseTVC.h"

@interface ShowLikesTVC : CustomBaseTVC

@property (nonatomic) NSInteger newsfeedID;

@end
