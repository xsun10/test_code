//
//  RatePlayerSportTVC.h
//  Vaiden
//
//  Created by James Chung on 10/4/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RatePlayerSportActionTVC.h"
#import "CustomBaseTVC.h"

@class RatePlayerSportTVC;

@protocol RatePlayerSportTVC_Delegate <NSObject>

- (void)setRatePlayerSportViewController:(RatePlayerSportTVC *)controller
                             withRefresh:(BOOL)refreshBool;

@end

@interface RatePlayerSportTVC : CustomBaseTVC <RatePlayerSportActionTVC_Delegate>


@property (nonatomic, weak) id <RatePlayerSportTVC_Delegate> delegate;

@property (nonatomic) NSUInteger sportID;
@property (nonatomic) NSUInteger targetUserID;

@end
