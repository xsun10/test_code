//
//  VotePopupWindowVC.h
//  Vaiden
//
//  Created by Turbo on 7/15/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@protocol VotePopupWindowDelegate <NSObject>
-(void)refreshVoteInfo;
@end

@interface VotePopupWindowVC : UIViewController <MBProgressHUDDelegate> {
    MBProgressHUD *HUD;
}

@property (nonatomic) NSInteger targetID;
@property (nonatomic, weak) id <VotePopupWindowDelegate>delegate;
@end
