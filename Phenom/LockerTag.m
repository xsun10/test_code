//
//  LockerTag.m
//  Vaiden
//
//  Created by James Chung on 5/7/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "LockerTag.h"

@implementation LockerTag

- (LockerTag *) initWithTagDetails:(NSInteger)lockerTagID
                        andTagType:(NSInteger)lockerTagType
                        andTagName:(NSString *)lockerTagName

{
    
    self = [super init];
    
    if (self) {
        _tagID = lockerTagID;
        _tagType = lockerTagType;
        _tagName = lockerTagName;
    }
    
    return self;
    
}

@end
