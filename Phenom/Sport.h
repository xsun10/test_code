//
//  Sport.h
//  Phenom
//
//  Created by James Chung on 5/1/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Sport : NSObject

@property (nonatomic, strong) NSString *sportName;
@property (nonatomic, strong) NSString *sportType;
@property (nonatomic) NSInteger sportID;
@property (nonatomic) BOOL largerScoreWins; // if false the lowest score wins (ex golf)

@property (nonatomic, strong) NSString *setUnitName;
@property (nonatomic, strong) NSString *gameUnitName;
@property (nonatomic) NSUInteger maxRound;

// used for lists when user selects sport
@property (nonatomic) BOOL isSelected;





- (Sport *) initWithSportDetails:(NSString *)sportName
                         andType:(NSString *)sportType;


- (Sport *) initWithSportDetails:(NSInteger)sportID
                         andName:(NSString *)sportName
                         andType:(NSString *)sportType;

- (Sport *) initWithSportDetails:(NSInteger)sportID
                         andName:(NSString *)sportName
                         andType:(NSString *)sportType
                 largerScoreWins:(BOOL)largerScoreWins;

- (Sport *)initWithSportDetails:(NSInteger)sportID
                        andName:(NSString *)sportName
                        andType:(NSString *)sportType
                largerScoreWins:(BOOL)largerScoreWins
                    setUnitName:(NSString *)setUnitName
                   gameUnitName:(NSString *)gameUnitName;


- (UIImage *)getSportIcon;
- (UIImage *)getSportIcon2;


- (UIImage *)backgroundImage;

@end
