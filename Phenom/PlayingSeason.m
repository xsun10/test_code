//
//  PlayingSeason.m
//  Vaiden
//
//  Created by James Chung on 5/8/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "PlayingSeason.h"

@implementation PlayingSeason

- (PlayingSeason *)initWithSeason:(NSString *)season
{
    self = [super init];
    
    if (self) {
        _season = season;
    }
    return self;
}


- (void)setSeasonWithCurrentMonth
{
    NSDate *date = [NSDate date];
    NSCalendar * calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *dateComponents = [calendar components:NSMonthCalendarUnit fromDate:date];
    
    self.month = [dateComponents month];
    self.season =  [PlayingSeason getSeasonByMonth:self.month];
}

+ (NSString *)getSeasonByMonth:(NSInteger)month
{
    if (month == 12 || month == 1 || month == 2) {
        return @"winter";
    } else if (month == 3 || month == 4 || month == 5) {
        return @"spring";
    } else if (month == 6 || month == 7 || month == 8) {
        return @"summer";
    } else if (month == 9 || month == 10 || month == 11) {
        return @"fall";
    }
    return @"";
    
    
}

- (UIImage *)seasonIcon
{
    if ([self.season isEqualToString:@"winter"]) {
        return [UIImage imageNamed:@"Winter Icon"];
    } else if ([self.season isEqualToString:@"spring"]) {
        return [UIImage imageNamed:@"Spring Icon"];
    } else if ([self.season isEqualToString:@"summer"]) {
        return [UIImage imageNamed:@"Summer Icon"];
    } else if ([self.season isEqualToString:@"autumn"]) {
        return [UIImage imageNamed:@"Autumn Icon"];
    }
    
    return nil;
}

@end
