//
//  VenueImage.m
//  Vaiden
//
//  Created by James Chung on 10/8/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "VenueImage.h"

@implementation VenueImage

-(VenueImage*)initWithVenueImageInfo:(NSString *)venuePicString
                            andImage:(UIImage *)venuePic
                           isDefault:(BOOL)isDefault
{
    self = [super init];
    
    if (self) {
        _venuePicString = venuePicString;
        _venuePic = venuePic;
        _isDefaultImage = isDefault;
    }
    
    return self;
}


@end
