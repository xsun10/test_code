//
//  StatTag.m
//  Vaiden
//
//  Created by James Chung on 5/27/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "StatTag.h"

@implementation StatTag

- (StatTag *)initWithStatTag:(NSInteger)statTagID
              andStatTagName:(NSString *)statTagName
             andStatTagValue:(NSString *)statTagValue
                 andStatLink:(NSString *)statLink
             andStatLinkType:(NSString *)statLinkType
              andSubCategory:(NSString *)subCategory
          andShowSubcategory:(BOOL)showSubCategory
{
    self = [super init];
    
    if (self) {
        _statTagID      = statTagID;
        _statTagName = statTagName;
        _statTagValue = statTagValue;
        _isSet = NO;
        _statLink = statLink;
        _statLinkType = statLinkType;
        _subCategory = subCategory;
        _showSubCategory = showSubCategory;
    }
    return self;
    
}
@end
