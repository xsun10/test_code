//
//  Comment.h
//  Phenom
//
//  Created by James Chung on 5/8/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Comment : NSObject

@property (nonatomic) NSInteger commentID;
@property (strong, nonatomic) NSString *commentText;
@property (nonatomic) NSInteger userID;
@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSDate *postDate;
@property (nonatomic, strong) NSString *profileBaseString;

- (Comment *)initWithCommentDetails:(NSInteger)commentID
                     andCommentText:(NSString *)commentText
                             byUserID:(NSInteger)userID
                        andUsername:(NSString *)username
                      andProfileStr:(NSString *)profileBaseString
                             onDate:(NSDate *)postDate;



@end
