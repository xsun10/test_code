//
//  MatchesNearbyCollectionCell.m
//  Phenom
//
//  Created by James Chung on 5/22/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "MatchesNearbyCollectionCell.h"

@implementation MatchesNearbyCollectionCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
