//
//  ShowPhotosInLockerAlbumCell.h
//  Vaiden
//
//  Created by James Chung on 5/12/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShowPhotosInLockerAlbumCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *lockerPic;
@property (weak, nonatomic) IBOutlet UIButton *imageButtonHandle;

@end
