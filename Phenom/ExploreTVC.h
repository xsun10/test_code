//
//  ExploreTVC.h
//  Vaiden
//
//  Created by James Chung on 4/22/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "CustomBaseTVC.h"
#import <CoreLocation/CoreLocation.h>
#import "ChooseOneSportTVC.h"

@interface ExploreTVC : CustomBaseTVC <CLLocationManagerDelegate, ChooseOneSportTVC_Delegate>
{
    CLLocationManager *locationManager;
}

@property (nonatomic) BOOL fromDashboard;
@end
