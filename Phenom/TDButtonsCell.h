//
//  TDButtonsCell.h
//  Vaiden
//
//  Created by James Chung on 4/7/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Team.h"


@class TDButtonsCell;

@protocol TDButtonsCell_Delegate <NSObject>
- (void)setWithCellInfo:(TDButtonsCell *)cellObj
       withActionString:(NSString *)actionString;
@end

@interface TDButtonsCell : UITableViewCell

// will display the buttons based on the session user participation in the team
- (void)displayUserButtonsForTeam:(Team *)theTeam andSessionUser:(NSInteger)sessionUserID;
- (void)clearAllButtonsFromContentView;
@property (nonatomic, weak) id <TDButtonsCell_Delegate> delegate;


@end

