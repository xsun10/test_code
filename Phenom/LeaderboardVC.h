//
//  LeaderboardVC.h
//  Phenom
//
//  Created by James Chung on 7/15/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChooseIndividualSportTVC.h"
#import "CustomBaseVC.h"

@interface LeaderboardVC : CustomBaseVC <UITableViewDataSource, UITableViewDelegate, ChooseIndividualSportTVC_Delegate>

@property (nonatomic) NSInteger filterSportID;
@property (nonatomic) NSString *filterSportName;

@end
