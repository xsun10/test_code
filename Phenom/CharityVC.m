//
//  CharityVC.m
//  Vaiden
//
//  Created by Turbo on 7/23/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "CharityVC.h"
#import "IOSRequest.h"
#import "UserPreferences.h"
#import "UIView+Manipulate.h"

@interface CharityVC ()
@property (nonatomic, strong) UserPreferences *preferences;
@property (nonatomic, strong) NSString *charity;
@end

@implementation CharityVC
- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}
- (NSString *)charity
{
    if (!_charity) _charity = [[NSString alloc] init];
    return _charity;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.cancel makeRoundedBorderWithRadius:5];
    [self.submit makeRoundedBorderWithRadius:5];
    [self.popView makeRoundedBorderWithRadius:10];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] init];
    [tapGesture addTarget:self action:@selector(handleTap:)];
    [self.view addGestureRecognizer:tapGesture];
    [IOSRequest displayCharity:[self.preferences getUserID]
                  onCompletion:^(NSDictionary *results) {
                      dispatch_async(dispatch_get_main_queue(), ^{
                          
                          self.charity=results[@"charity_id"];
                          self.charityInput.font=[UIFont fontWithName: @"Helvetica Neue" size: 13.0];
                          if([self.charity isKindOfClass:[NSNull class]]||[[self.charity stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]==0){
                              self.charityInput.placeholder=@"Recommend Charity";
                              }
                          else{
                              self.charityInput.placeholder=self.charity;
                          }

                      });
                      
                  }];

}


- (IBAction)submitCharity:(id)sender {

    [IOSRequest setCharity:[self.preferences getUserID]
                   charity:self.charityInput.text
              onCompletion:^(NSDictionary *results) {
                  
                  dispatch_async(dispatch_get_main_queue(), ^{
                      // [self.delegate holdUI];
                       [self dismissViewControllerAnimated:NO completion:nil];
                  });
              }];
   
    
  
}

- (IBAction)cancel:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (void) handleTap:(UITapGestureRecognizer *)gesture
{
    [self.charityInput resignFirstResponder];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
