//
//  ShowVideoVC.m
//  Phenom
//
//  Created by James Chung on 5/30/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "ShowVideoVC.h"
#import "UIImage+VidTools.h"
#import "UIImage+ProportionalFill.h"

@interface ShowVideoVC ()
@property (weak, nonatomic) IBOutlet UIImageView *videoPreviewImage;

@end

@implementation ShowVideoVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
//    self.videoPreviewImage.image = [UIImage imageFromMovie:self.webAddress atTime:0.0];
    
    
}


- (void)playMovie:(id)sender
{
    [self showVideo];
}



- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (self.showVideoFlag) {
        [self showVideo];
    } 
}

- (void)showVideo
{
//    VideoPost *vPost = [self.newsPosts objectAtIndex:indexPath.section];
//    NSURL *videoURL = [NSURL URLWithString:[S3Tools getFileNameStringWithType:@"highlight_video"
 //                                                                  baseString:vPost.videoBaseFileString]];
    
//    NSURL *url = [NSURL URLWithString:
//                  @"http://www.ebookfrenzy.com/ios_book/movie/movie.mov"];
    
    _moviePlayer =  [[MPMoviePlayerController alloc]
                     initWithContentURL:self.webAddress];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackDidFinish:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:_moviePlayer];
    
  
    
    
    _moviePlayer.controlStyle = MPMovieControlStyleDefault;
    _moviePlayer.shouldAutoplay = YES;
    [self.view addSubview:_moviePlayer.view];
    [_moviePlayer setFullscreen:YES animated:YES];
}

- (void) moviePlayBackDidFinish:(NSNotification*)notification {
    [[NSNotificationCenter defaultCenter]
     removeObserver:self
     name:MPMoviePlayerPlaybackDidFinishNotification
     object:self.moviePlayer];
    
    if ([self.moviePlayer
         respondsToSelector:@selector(setFullscreen:animated:)])
    {
        [self.moviePlayer stop];
        self.showVideoFlag = NO;
        [self.moviePlayer.view removeFromSuperview];
        [self dismissMoviePlayerViewControllerAnimated];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

@end
