//
//  AddTeamMatchTVC.h
//  Vaiden
//
//  Created by James Chung on 4/10/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "CustomBaseTVC.h"
#import "Team.h"
#import "ChooseTeamSportTVC.h"
#import "ChooseMatchTypeTVC.h"
#import "VenuesTVC.h"
#import "MBProgressHUD.h"

@interface AddTeamMatchTVC : CustomBaseTVC <ChooseTeamSportTVC_Delegate, ChooseMatchTypeTVC_Delegate, VenueTVC_Delegate, UIActionSheetDelegate, MBProgressHUDDelegate>
{
    MBProgressHUD *HUD;
}

@property (nonatomic, strong) Team *team1;
@property (nonatomic, strong) Team *team2;

@end
