//
//  SportRatingsListTVC.h
//  Vaiden
//
//  Created by James Chung on 10/7/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomBaseTVC.h"

@interface SportRatingsListTVC : CustomBaseTVC

@property (nonatomic) NSUInteger sportRatingsTypeID;
@property (nonatomic) NSUInteger ratedPlayerID;
@end
