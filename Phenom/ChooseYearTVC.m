//
//  ChooseYearTVC.m
//  Vaiden
//
//  Created by James Chung on 5/8/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "ChooseYearTVC.h"

@interface ChooseYearTVC ()

@property (nonatomic, strong) NSMutableArray *yearsArray;
@end

@implementation ChooseYearTVC

- (NSMutableArray *)yearsArray
{
    if (!_yearsArray) _yearsArray = [[NSMutableArray alloc] init];
    return _yearsArray;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadYears];
    [self.tableView reloadData];
}

- (void)loadYears
{
    NSDate *date = [NSDate date];
    NSCalendar * calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *dateComponents = [calendar components:NSYearCalendarUnit fromDate:date];
    
    int myCounter = 50;
    int counterYear = [dateComponents year];
    
    // go fifty years back
    while (myCounter > 0) {
        [self.yearsArray addObject:[NSString stringWithFormat:@"%d", counterYear]];
        myCounter --;
        counterYear --;
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Year Cell" forIndexPath:indexPath];
    
    cell.accessoryType = UITableViewCellAccessoryNone;
    
    NSString *year = [self.yearsArray objectAtIndex:indexPath.row];
    cell.textLabel.text = year;
    
    if ([year isEqualToString:self.chosenYear]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    
    [self.delegate setWithViewController:self withYear:cell.textLabel.text];
    [self.navigationController popViewControllerAnimated:YES];
}

@end
