//
//  ShowAlertsUpcomingMatchesCell.h
//  Vaiden
//
//  Created by James Chung on 5/26/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShowAlertsUpcomingMatchesCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *upcomingMatchesLabel;

@end
