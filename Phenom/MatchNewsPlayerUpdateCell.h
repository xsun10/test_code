//
//  MatchNewsPlayerUpdateCell.h
//  Vaiden
//
//  Created by James Chung on 10/18/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewsfeedMatchHeadingCV.h"

@class MatchNewsPlayerUpdateCell;

@protocol MatchNewsPlayerUpdateCell_delegate <NSObject>
-(void)setMatchNewsPlayerUpdateCellParentViewController:(MatchNewsPlayerUpdateCell *)controller withUserIDToSegue:(NSInteger)userID;
@end


@interface MatchNewsPlayerUpdateCell : UITableViewCell <UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic, weak) id <MatchNewsPlayerUpdateCell_delegate> delegate;


@property (weak, nonatomic) IBOutlet UIImageView *sportIcon;
@property (weak, nonatomic) IBOutlet UILabel *matchNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *updateTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIView *centerViewBackground;
@property (weak, nonatomic) IBOutlet UIImageView *profilePic;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UILabel *playerActionMessage;
@property (weak, nonatomic) IBOutlet UILabel *matchMessageLabel;
@property (weak, nonatomic) IBOutlet UIButton *detailsButton;
@property (weak, nonatomic) IBOutlet UIView *sportIconBackground;
@property (weak, nonatomic) IBOutlet UILabel *headlineLabel;
@property (weak, nonatomic) IBOutlet NewsfeedMatchHeadingCV *matchHeadingCV;
@property (weak, nonatomic) IBOutlet UIView *detailButtonView;
@property (weak, nonatomic) IBOutlet UIView *shareButtonView;
@property (weak, nonatomic) IBOutlet UIView *commentButtonView;
@property (weak, nonatomic) IBOutlet UILabel *numCommentLabel;
@property (weak, nonatomic) IBOutlet UILabel *numRemixLabel;
@property (weak, nonatomic) IBOutlet UIView *headlineBackground;
@property (weak, nonatomic) IBOutlet UIButton *commentButton;
@property (weak, nonatomic) IBOutlet UIButton *remixButton;
@property (weak, nonatomic) IBOutlet UIImageView *shareIcon;
@property (weak, nonatomic) IBOutlet UILabel *sharedByLabel;
@property (weak, nonatomic) IBOutlet UIView *shareDot;

@property (nonatomic, strong) NSString *matchCategory;
@property (nonatomic, strong) NSMutableArray *competitors;

@end
