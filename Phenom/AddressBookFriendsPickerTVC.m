//
//  AddressBookFriendsPickerTVC.m
//  Vaiden
//
//  Created by Turbo on 6/27/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "AddressBookFriendsPickerTVC.h"
#import "ContactListTVCell.h"
#import "UIImage+ProportionalFill.h"
#import "UIColor+VaidenColors.h"

@interface AddressBookFriendsPickerTVC ()
@property (nonatomic) NSInteger count;
@property (nonatomic, strong) UIBarButtonItem *doneButton;

@property (strong, nonatomic) UIView * loadingView;
@end

@implementation AddressBookFriendsPickerTVC

@synthesize delegate;

- (NSMutableDictionary *)picker
{
    if (!_picker) _picker = [[NSMutableDictionary alloc] init];
    return _picker;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    NSLog(@"%lu->%@",(unsigned long)self.friendList.count, self.friendList);
    
    self.tableView.delegate = self;
    
    self.doneButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @"Done"
                                   style: UIBarButtonItemStyleBordered
                                   target: self action: @selector(doneClicked:)];
    [self.doneButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                        [UIFont fontWithName:@"HelveticaNeue-Bold" size:15], NSFontAttributeName,
                                        [UIColor whiteColor], NSForegroundColorAttributeName, nil]
                              forState:UIControlStateNormal];
    [self.doneButton setEnabled:NO];
    [self.navigationItem setRightBarButtonItem: self.doneButton];
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @"Cancel"
                                   style: UIBarButtonItemStyleBordered
                                   target: self action: @selector(cancelClicked:)];
    [cancelButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                          [UIFont fontWithName:@"HelveticaNeue" size:15], NSFontAttributeName,
                                          [UIColor whiteColor], NSForegroundColorAttributeName,
                                          nil]
                              forState:UIControlStateNormal];
    [self.navigationItem setLeftBarButtonItem: cancelButton];
    
    self.count = 0;
    
    // temp view for loading the data;
    self.loadingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    self.loadingView.backgroundColor = [UIColor midGray2];
    self.loadingView.alpha = 0.5;
    [self.loadingView setHidden:YES];
    
    UIActivityIndicatorView * tmpIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/2-5, [UIScreen mainScreen].bounds.size.height/2-5, 10, 10)];
    [tmpIndicator startAnimating];
    
    [self.loadingView addSubview:tmpIndicator];
    [self.view addSubview:self.loadingView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.currentTab == ADDRESS_BOOK_TAB) {
        //NSLog(@"%d->%@",self.contacts.count, self.contacts);
        return self.contacts.count;
    } else {
        return self.friendList.count;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ContactListTVCell *cell = [tableView dequeueReusableCellWithIdentifier:@"contact_list" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (self.currentTab == ADDRESS_BOOK_TAB && [self.emails objectForKey:[NSString stringWithFormat:@"%ld", (long)indexPath.row]] == nil) {
        [cell.profilePic setHidden:YES];
        [cell.fullname setHidden:YES];
        return cell;
    } else {
        [cell.profilePic setHidden:NO];
        [cell.fullname setHidden:NO];
    if (self.currentTab == ADDRESS_BOOK_TAB) {
        [cell.profilePic setContentMode:UIViewContentModeScaleAspectFill];
        UIImage * image = [self.profilePics objectForKey:[NSString stringWithFormat:@"%ld", (long)indexPath.row]];
        
        if (image == nil) {
            [cell.profilePic setImage:[UIImage imageNamed:@"avatar square"]];
        } else {
            [cell.profilePic setImage:image];
        }
        [UIImage makeRoundedImageNoBorder:cell.profilePic withRadius:5];
        
        [cell.fullname setText:[self.contacts objectAtIndex:indexPath.row]];
        
        NSMutableArray * contactData = [[NSMutableArray alloc] init];
        [contactData addObject:[self.contacts objectAtIndex:indexPath.row]];
        [contactData addObject:[NSNumber numberWithBool:NO]];
        
        NSString * email = [self.emails objectForKey:[NSString stringWithFormat:@"%ld", (long)indexPath.row]];
        [contactData addObject:email==nil?@"":email];
        [self.picker setObject:contactData forKey:[NSString stringWithFormat:@"%ld", (long)indexPath.row]];
    } else {
        NSDictionary * user = [self.friendList objectForKey:[NSString stringWithFormat:@"%ld", (long)indexPath.row]];
        [cell.fullname setText:[NSString stringWithFormat:@"@%@", user[@"screen_name"]]];
        
        UIImage *image = user[@"profile_image"];
        [cell.profilePic setContentMode:UIViewContentModeScaleAspectFill];
        if (image == nil) {
            [cell.profilePic setImage:[UIImage imageNamed:@"avatar square"]];
        } else {
            [cell.profilePic setImage:image];
        }
        [UIImage makeRoundedImageNoBorder:cell.profilePic withRadius:5];
        
        NSMutableArray * contactData = [[NSMutableArray alloc] init];
        [contactData addObject:user];
        [contactData addObject:[NSNumber numberWithBool:NO]];
        [self.picker setObject:contactData forKey:[NSString stringWithFormat:@"%ld", (long)indexPath.row]];
    }
    
    return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ContactListTVCell *cell = (ContactListTVCell *)[tableView cellForRowAtIndexPath:indexPath];
    NSMutableArray *data = [self.picker objectForKey:[NSString stringWithFormat:@"%ld", (long)indexPath.row]];
    //total content size is 2
    BOOL isSelected = [[data objectAtIndex:1] boolValue];
    if (isSelected) {
        [data replaceObjectAtIndex:1 withObject:[NSNumber numberWithBool:NO]];
        cell.accessoryType = UITableViewCellAccessoryNone;
        self.count --;
    } else {
        [data replaceObjectAtIndex:1 withObject:[NSNumber numberWithBool:YES]];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        self.count ++;
    }
    
    [self.picker setObject:data forKey:[NSString stringWithFormat:@"%ld", (long)indexPath.row]];
    NSLog(@"%@", self.picker);
    
    if (self.count > 0) {
        [self.doneButton setEnabled:YES];
    } else {
        [self.doneButton setEnabled:NO];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.currentTab == ADDRESS_BOOK_TAB && [self.emails objectForKey:[NSString stringWithFormat:@"%ld", (long)indexPath.row]] == nil) {
        return 0;
    } else {
        return 44;
    }
}

- (void) disableInteraction
{
    if (self.loadingView.hidden) {
        [self.loadingView setHidden:NO];
    }
    [self.view setUserInteractionEnabled:NO];
}

#pragma - mark actions

- (IBAction)cancelClicked:(UIBarButtonItem *)sender {
    [self.delegate addressBookFriendsPickerCancelWasClicked:sender];
}

- (IBAction)doneClicked:(UIBarButtonItem *)sender {
    [self disableInteraction];
    [self.delegate addressBookFriendsPickerDoneClicked:sender withData:self.picker withType:self.currentTab];
}

@end
