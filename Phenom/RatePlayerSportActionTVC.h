//
//  RatePlayerSportActionTVC.h
//  Vaiden
//
//  Created by James Chung on 10/7/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SportRating.h"
#import "MBProgressHUD.h"
#import "CustomBaseTVC.h"

@class RatePlayerSportActionTVC;

@protocol RatePlayerSportActionTVC_Delegate <NSObject>

- (void)setRatePlayerSportActionViewController:(RatePlayerSportActionTVC *)controller
                               withSportRating:(NSUInteger)sportRating
                                    andComment:(NSString *)comment;

@end

@interface RatePlayerSportActionTVC : CustomBaseTVC <MBProgressHUDDelegate>
{
    MBProgressHUD *HUD;
}

@property (nonatomic, weak) id <RatePlayerSportActionTVC_Delegate> delegate;
//@property (nonatomic) NSUInteger targetUserID;
@property (nonatomic) NSUInteger srTypeID;
@property (nonatomic, strong) NSString *srTypeName;
@property (nonatomic, strong) SportRating *sRating;

@end

