//
//  SpecialUIButton.h
//  Vaiden
//
//  Created by James Chung on 1/30/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SpecialUIButton : UIButton

@property (nonatomic) BOOL shouldRemoveFromMySuperview;
- (SpecialUIButton *)init;

@end
