//
//  EventTag.h
//  Vaiden
//
//  Created by James Chung on 5/28/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Venue.h"

@interface EventTag : NSObject

@property (nonatomic) NSInteger eventID;
@property (nonatomic, strong) NSString *eventName;
@property (nonatomic, strong) NSDate *eventDate;
@property (nonatomic, strong) Venue *eventVenue;
@property (nonatomic, strong) NSString *webAddress;
@property (nonatomic, strong) NSString *phoneNumber;
@property (nonatomic, strong) NSString *emailAddress;

// Abbreviated Initializer

- (EventTag *)initWithEvent:(NSInteger)eventID
               andEventName:(NSString *)eventName
               andEventDate:(NSDate *)eventDate;

// Detailed Initializer
- (EventTag *)initWithEvent:(NSInteger)eventID
               andEventName:(NSString *)eventName
               andEventDate:(NSDate *)eventDate
                 andVenue:(Venue *)eventVenue
              andWebAddress:(NSString *)webAddress
             andPhoneNumber:(NSString *)phoneNumber
            andEmailAddress:(NSString *)emailAddress;

@end
