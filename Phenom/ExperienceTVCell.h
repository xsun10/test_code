//
//  ExperienceTVCell.h
//  Vaiden
//
//  Created by Turbo on 7/28/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExperienceTVCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *time;
@property (weak, nonatomic) IBOutlet UILabel *sport;

@property (weak, nonatomic) IBOutlet UIImageView *icon;
@end
