//
//  IMDOutcomeStatusCell.h
//  Vaiden
//
//  Created by James Chung on 1/2/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IMDOutcomeStatusCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *outcomeLabel;

@end
