//
//  ShouldVerifyAccountVC.m
//  Vaiden
//
//  Created by James Chung on 9/18/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "ShouldVerifyAccountVC.h"
#import "UIButton+RoundBorder.h"
#import "UserPreferences.h"
#import "UIColor+VaidenColors.h"
#import "UIView+Manipulate.h"

@interface ShouldVerifyAccountVC ()
@property (weak, nonatomic) IBOutlet UIButton *verifyButton;
@property (weak, nonatomic) IBOutlet UIScrollView *pageScrollView;
@property (weak, nonatomic) IBOutlet UIView *innerView;
@property (weak, nonatomic) IBOutlet UIImageView *contactIcon;
@property (nonatomic, strong) UserPreferences *preferences;
@property (weak, nonatomic) IBOutlet UIView *contactIconBackground;
@end

@implementation ShouldVerifyAccountVC

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.hidden = YES;
    //[self.verifyButton makeCircleWithColorAndBackground:[UIColor newBlueLight] andRadius:3.0];
    [self.verifyButton makeRoundedBorderWithRadius:3];
    self.contactIcon.image = [[UIImage imageNamed:@"Contact Card"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.contactIcon setTintColor:[UIColor whiteColor]];
    
//    [self.pageScrollView setContentInset:UIEdgeInsetsMake(0, 0, 20, 0)];
    
    [self.contactIconBackground makeCircleWithColor:[UIColor newBlueLight] andRadius:40];
//    self.pageScrollView.backgroundColor = [UIColor whiteColor];

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // No need to show this view if already verified
    if ([self.preferences isVerifiedAccount])
        [self.navigationController popViewControllerAnimated:YES];
    
    self.view.hidden = NO;
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    self.pageScrollView.contentSize = CGSizeMake(320.0, 700.0);
}

- (IBAction)verifyAccount:(id)sender
{
    [self performSegueWithIdentifier:@"Verify Segue" sender:self];
}

@end
