//
//  ChooseSportUtilityTVC.h
//  Vaiden
//
//  Created by James Chung on 5/8/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "CustomBaseTVC.h"
#import "Sport.h"

#define SPORT_TYPE_ALL 0
#define SPORT_TYPE_INDIVIDUAL 1
#define SPORT_TYPE_PICKUP 2
#define SPORT_TYPE_TEAM 3

@class ChooseSportUtilityTVC;

@protocol ChooseSportUtilityTVC_Delegate <NSObject>
- (void)setWithViewController:(ChooseSportUtilityTVC *)controller withSports:(NSMutableArray *)sportsArray isMultiple:(BOOL)isMultiple;

@end

@interface ChooseSportUtilityTVC : CustomBaseTVC

@property (nonatomic, weak) id <ChooseSportUtilityTVC_Delegate> delegate;


@property (nonatomic) BOOL selectMultiple;
@property (nonatomic) NSInteger sportTypeInt;
@property (nonatomic, strong) Sport *chosenSport;
@property (nonatomic) BOOL hideFitnessSport;

@end
