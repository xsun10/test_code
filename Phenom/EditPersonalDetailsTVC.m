//
//  EditPersonalDetailsTVC.m
//  Phenom
//
//  Created by James Chung on 6/5/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "EditPersonalDetailsTVC.h"
#import "UserPreferences.h"
#import "IOSRequest.h"
#import "UITextField+FormValidations.h"
#import <MobileCoreServices/MobileCoreServices.h>

#import "Constants.h"
#import "Response.h"
#import "AmazonKeyChainWrapper.h"
#import "AmazonClientManager.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "S3Tools.h"
#import "UIImage+ProportionalFill.h"
#import "AFNetworking.h"
#import "UIView+Manipulate.h"

@interface EditPersonalDetailsTVC () <UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (nonatomic, strong) UserPreferences *preferences;
@property (nonatomic, strong) NSDictionary *user;
@property (nonatomic, strong) NSDictionary *edits;

@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UITextField *userNameField;
@property (weak, nonatomic) IBOutlet UITextField *emailField;

@property (nonatomic, strong) NSDictionary *userData;
//@property (weak, nonatomic) IBOutlet UIBarButtonItem *saveButton;

@property (weak, nonatomic) UIActionSheet *profilePicActionSheet;
@property (strong, nonatomic)UIPopoverController *imagePickerPopover;
@property (weak, nonatomic) IBOutlet UIButton *changeImageButton;
@property (strong, nonatomic) UIImage *profilePicOriginal;

@property (weak, nonatomic) IBOutlet UITextField *locationField;
@property (weak, nonatomic) IBOutlet UITextView *about;

@property (nonatomic, strong) NSData *imageData;
@property (nonatomic, strong) NSData *thumbnailImageData;
@property (nonatomic, strong) NSData *thumbnailRetinaImageData;
@property (weak, nonatomic) IBOutlet UIImageView *profilePicOutlet;
@property (nonatomic, strong) NSString *fileExtension;
@property (nonatomic, strong) UIBarButtonItem *doneButton;
@property (weak, nonatomic) IBOutlet UILabel *characterLimitText;

@end

@implementation EditPersonalDetailsTVC

// lazy instantiation
- (UserPreferences *) preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    
    return _preferences;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];

    self.about.delegate = self;
    
    [self populateFields];
    //[self setDoneButton];
    
    
    
//    self.AddContactImage.image = [[UIImage imageNamed:@"Add Contact"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
//    [self.AddContactImage setTintColor:[UIColor whiteColor]];
    
}

/*- (void)setDoneButton
{
    UIButton *btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 50, 21);
    btn.layer.borderColor = [UIColor whiteColor].CGColor;
    btn.layer.borderWidth = 1.0;
    
    [btn addTarget:self action:@selector(saveEdit:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundColor:[UIColor clearColor]];
    [btn setTitle:@"DONE" forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-MediumItalic" size:12.0]];
    
    // Initialize UIBarbuttonitem...
    self.doneButton = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = self.doneButton;
}*/


// for some reason, the state is not clearing as expected.  It's clearing when app closes but not
// when view disappears...
/*
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.preferences clearStateInfo];
}*/

- (void)populateFields
{
    __weak EditPersonalDetailsTVC *weakSelf = self;
        
    // on profileviewcontroller we disregard whether session user is following himself.
    // this flag, is only used when session user views the profile of another user.
    
    [IOSRequest fetchUserInfo:[self.preferences getUserID]
                bySessionUser:[self.preferences getUserID]
                 onCompletion:^(NSDictionary *user){
                     dispatch_async(dispatch_get_main_queue(), ^{
                         [weakSelf setUser:user];
                         
                         if ([self.about.text length] > 0) {
                             self.characterLimitText.hidden = YES;
                         } else {
                             self.characterLimitText.hidden = NO;
                         }

                     });
                 }];
    

    
}

- (IBAction)saveEdit:(id)sender
{
    // not the best validation system...but will improve later. Need better error messages and validations
    if (![self.userNameField checkAlphanumeric]) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Username Error";
        alert.message = @"Username is not valid.  Please enter a username between 3 and 15 letters with only alphanumeric characters.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
        
    } else if (![self.emailField checkEmail]){
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Email Error";
        alert.message = @"Email is not valid.  Please enter another one.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
    } else if ([self.locationField.text length] == 0 || ![self.locationField checkLocationName]) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Location Name Error";
        alert.message = @"Location is not valid.  Please re-enter your location.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
        
        
    } else if ([self isEmptyForField:self.nameField]) { // If name field is empty
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Name Error";
        alert.message = @"Name cannot be empty.  Please re-enter your name.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
    } else {
        [self beginEditAccount];
        [self saveEditExecute];
   //     [self endEditAccount];
        [self.preferences setProfilePageRefreshState:YES];

        
    }
    
    
}

- (BOOL) checkTrimStringEmpty: (NSString *)string
{
    return [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0 ? YES : NO;
}

- (BOOL) isEmptyForField:(UITextField *)field
{
    return [self checkTrimStringEmpty:field.text];
}

- (void)saveEditExecute
{
    //    self.saveButton.enabled = NO;
 //   __weak EditPersonalDetailsTVC *weakSelf = self;
    
    
    if ([self.about.text length] > 250) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Posting Error";
        alert.message = @"Sorry but your About field cannot exceed 250 characters in length.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
    } else {
    
        HUD = [MBProgressHUD showHUDAddedTo:self.tableView animated:YES];
        HUD.labelText = @"Saving";
    
    [IOSRequest saveUserInfoWithUserID:[self.preferences getUserID]
                               andName:self.nameField.text
                           andUserName:self.userNameField.text
                              andEmail:self.emailField.text
                           andLocation:self.locationField.text
                          andAboutText:self.about.text
                          onCompletion:^(NSDictionary *edits){
                              dispatch_async(dispatch_get_main_queue(), ^{
                                  [MBProgressHUD hideHUDForView:self.tableView animated:YES];
                                  self.doneButton.enabled = YES;
                          //        NSInteger userID = [self.preferences getUserID];
                                  
                                  if ([edits[@"username_error"] isEqualToString:@"TRUE"]) {
                                      UIAlertView *alert = [[UIAlertView alloc] init];
                                      alert.title = @"Account Creation Error";
                                      alert.message = @"That username already exists. Please choose another";
                                      [alert addButtonWithTitle:@"OK"];
                                      [alert show];
                                  } else if ([edits[@"email_error"] isEqualToString:@"TRUE"]) {
                                      UIAlertView *alert = [[UIAlertView alloc] init];
                                      alert.title = @"Account Creation Error";
                                      alert.message = @"That email already exists. Please enter another";
                                      [alert addButtonWithTitle:@"OK"];
                                      [alert show];
                                  } else {
                                      [self.navigationController popViewControllerAnimated:YES];
                                      // can set values
                     //                 [weakSelf.user setValue:self.nameField.text forKey:@"fullname"];
                     //                 [weakSelf.user setValue:self.userNameField.text forKey:@"username"];
                     //                 [weakSelf.user setValue:self.emailField.text forKey:@"email"];
                     //                 [weakSelf.user setValue:self.locationField.text forKey:@"location"];
                                      
                                  }
                                  
                              });
                          }];
    
        double delayInSeconds = 10.0;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            //code to be executed on the main queue after delay
            self.doneButton.enabled = YES;
            [MBProgressHUD hideHUDForView:self.tableView animated:YES];
        });
        
        if (self.profilePicOriginal) {
            [self saveProfilePic];
        }
    
    }
}

- (void)saveProfilePic
{
    if (![AmazonClientManager hasCredentials]) {
        [[Constants credentialsAlert] show];
        NSLog(@"no credentials");
    }
    else {
        
        NSLog (@"has credentials");
        
        operationQueue = [NSOperationQueue new];
        operationQueue.maxConcurrentOperationCount = 3;
        
        NSString *fileNameBase = [S3Tools getFileNameBaseStringWithType:@"profile" forUser:[self.preferences getUserID]];
        S3Tools *imageUploader1 = [[S3Tools alloc] initWithData:self.imageData
                                                        forUser:[self.preferences getUserID]
                                                       dataType:@"profile"
                                          andBaseFileNameString:fileNameBase
                                                     andFileExt:self.fileExtension];
        [operationQueue addOperation:imageUploader1];
        
        
        S3Tools *imageUploader2 = [[S3Tools alloc] initWithData:self.thumbnailImageData
                                                        forUser:[self.preferences getUserID]
                                                       dataType:@"thumbnail"
                                          andBaseFileNameString:fileNameBase
                                                     andFileExt:self.fileExtension];
        [operationQueue addOperation:imageUploader2];
        
        S3Tools *imageUploader3 = [[S3Tools alloc] initWithData:self.thumbnailRetinaImageData
                                                        forUser:[self.preferences getUserID]
                                                       dataType:@"thumbnail_retina"
                                          andBaseFileNameString:fileNameBase
                                                     andFileExt:self.fileExtension];
        [operationQueue addOperation:imageUploader3];
        
        [self.preferences setProfilePicString:[NSString stringWithFormat:@"%@.jpg", fileNameBase]];
        [self.preferences setProfilePageRefreshState:YES];
    }
}
- (void)beginEditAccount
{
    NSLog(@"Attempting to Save User Edits");
    self.doneButton.enabled = NO;
    [self.userNameField resignFirstResponder];
    [self.emailField resignFirstResponder];
    [self.nameField resignFirstResponder];
    [self.locationField resignFirstResponder];
    
}

- (void)endEditAccount
{
    self.doneButton.enabled = YES;
    [self.navigationController popViewControllerAnimated:YES];
}
/*
- (IBAction)cancelButton:(UIBarButtonItem *)sender
{
    self.nameField.text = [self.user[@"fullname"] description];
    self.userNameField.text = [self.user[@"username"] description];
    self.emailField.text = [self.user[@"email"] description];
    self.locationField.text = [self.user[@"location"] description];
    
    [self.userNameField resignFirstResponder];
    [self.emailField resignFirstResponder];
    [self.nameField resignFirstResponder];
    [self.locationField resignFirstResponder];
    
}
*/

-(void)setUser:(NSDictionary *)user
{
    if (_user != user) {
        _user = user;
    }
    [self updateUIWithUser:user];
}

-(void)updateUIWithUser:(NSDictionary *)user
{
    if (user) {
        self.nameField.text = user[@"fullname"];
        self.userNameField.text = user[@"username"];
        self.emailField.text = user[@"email"];
        self.locationField.text = user[@"location"];
        self.about.text = user[@"about"];
        
               
        NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                baseString:user[@"profile_pic_string"]];
        
        [self.profilePicOutlet setImageWithURL:[NSURL URLWithString:url]
                              placeholderImage:[UIImage imageNamed:@"Add Profile Pic"]];
        
        
        [self.profilePicOutlet makeCircleWithColor:[UIColor whiteColor] andRadius:35];
        
    }
}

- (IBAction)clearNameField:(id)sender
{
    self.nameField.text = @"";
}

- (IBAction)clearUserNameField:(id)sender
{
    self.userNameField.text = @"";
}

- (IBAction)clearEmailField:(id)sender
{
    self.emailField.text = @"";
}
- (IBAction)clearLocationField:(id)sender
{
    self.locationField.text = @"";
}

#define TITLE_OF_ACTIONSHEET @"Change your profile pic"
#define CANCEL_BUTTON_ACTIONSHEET @"Cancel "
#define FROM_CAMERA @"Take a profile pic"
#define FROM_LIBRARY @"Choose From Photo Library"

- (IBAction)changeProfilePic:(id)sender
{
    if (!self.profilePicActionSheet) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                      initWithTitle:TITLE_OF_ACTIONSHEET delegate:self
                                      cancelButtonTitle:CANCEL_BUTTON_ACTIONSHEET destructiveButtonTitle:nil
                                      otherButtonTitles:FROM_CAMERA, FROM_LIBRARY, nil];
        [actionSheet showFromTabBar:self.tabBarController.tabBar];
        
        self.profilePicActionSheet = actionSheet;
    }
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == actionSheet.destructiveButtonIndex) {
        [self.view.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    } else {
        NSString *choice = [actionSheet buttonTitleAtIndex:buttonIndex];
        
        if ([choice isEqualToString:FROM_CAMERA]) {
            [self changePicFromCamera];
        } else if ([choice isEqualToString:FROM_LIBRARY]){
            [self changePicFromPhotoLibrary];
        }
    }
}

- (void)changePicFromCamera
{
    [self presentImagePicker:UIImagePickerControllerSourceTypeCamera];
}


- (void) changePicFromPhotoLibrary
{
    [self presentImagePicker:UIImagePickerControllerSourceTypeSavedPhotosAlbum];
}

- (void) presentImagePicker:(UIImagePickerControllerSourceType)sourceType
{
    if ([UIImagePickerController isSourceTypeAvailable:sourceType]) {
        NSArray *availableMediaTypes = [UIImagePickerController availableMediaTypesForSourceType:sourceType];
        if ([availableMediaTypes containsObject:(NSString *)kUTTypeImage]) {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.sourceType = sourceType;
            picker.mediaTypes = @[(NSString *)kUTTypeImage];
            picker.allowsEditing = YES;
            picker.delegate = self;
            
           
            [self presentViewController:picker animated:YES completion:nil];
        }
    }
}

- (void) imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

#define MAX_IMAGE_WIDTH 70 // might want to change this to height....

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = info[UIImagePickerControllerEditedImage];
    
    if (!image) image = info[UIImagePickerControllerOriginalImage];
    if (image) {
        self.profilePicOriginal = image;
        UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
        CGRect frame = imageView.frame;
        if (frame.size.width > MAX_IMAGE_WIDTH) {
            frame.size.height = (frame.size.height / frame.size.width) * MAX_IMAGE_WIDTH;
            frame.size.width = MAX_IMAGE_WIDTH;
        }
        imageView.frame = frame;
        //       [self.setRandomLocationForView:imageView];
        //       [self.changeImageButton.imageView addSubview:imageView];
        [self.changeImageButton setImage:imageView.image forState:UIControlStateNormal];
        self.imageData = UIImageJPEGRepresentation(imageView.image, 1.0);
        self.thumbnailImageData = UIImageJPEGRepresentation([image imageCroppedToFitSize:CGSizeMake(70, 70)], 1.0);
        self.thumbnailRetinaImageData = UIImageJPEGRepresentation([image imageCroppedToFitSize:CGSizeMake(140, 140)], 1.0);
        self.fileExtension = @"jpg";
        
        //       self.profilePicOriginal = imageView.image;
        [self.changeImageButton makeCircleWithColor:[UIColor whiteColor] andRadius:35];

    }
    
    if (self.imagePickerPopover) {
        
        [self.imagePickerPopover dismissPopoverAnimated:YES];
        self.imagePickerPopover = nil;
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)textViewDidChange:(UITextView *)textView
{
    if ([self.about.text length] > 0) {
        self.characterLimitText.hidden = YES;
    } else {
        self.characterLimitText.hidden = NO;
    }
}



@end
