//
//  ChooseLocationTVC.h
//  Vaiden
//
//  Created by James Chung on 5/8/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "CustomBaseTVC.h"
#import "Venue.h"

@class ChooseLocationTVC;

@protocol ChooseLocationTVC_Delegate <NSObject>
- (void)setWithViewController:(ChooseLocationTVC *)controller withVenue:(Venue *)selectedVenue;

@end

@interface ChooseLocationTVC : CustomBaseTVC

@property (nonatomic, weak) id <ChooseLocationTVC_Delegate> delegate;



@end
