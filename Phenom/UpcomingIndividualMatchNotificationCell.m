//
//  UpcomingIndividualMatchNotificationCell.m
//  Vaiden
//
//  Created by James Chung on 3/11/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "UpcomingIndividualMatchNotificationCell.h"

@implementation UpcomingIndividualMatchNotificationCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
