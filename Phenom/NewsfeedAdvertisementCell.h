//
//  NewsfeedAdvertisementCell.h
//  Vaiden
//
//  Created by James Chung on 3/4/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsfeedAdvertisementCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *adImage;
@end
