//
//  PlayerRecord.h
//  Vaiden
//
//  Created by James Chung on 9/23/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PlayerRecord : NSObject

- (PlayerRecord *)init;

- (PlayerRecord *) initWithPlayerRecordWins:(NSUInteger)wins
                                  andLosses:(NSUInteger)losses
                                    andTies:(NSUInteger)ties;

@property (nonatomic) NSUInteger wins;
@property (nonatomic) NSUInteger losses;
@property (nonatomic) NSUInteger ties;

- (NSString *)getWinsText;
- (NSString *)getLossesText;
- (NSString *)getTiesText;

@end
