//
//  ConfirmIndMatchScoreVC.m
//  Vaiden
//
//  Created by James Chung on 11/25/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "ConfirmIndMatchScoreVC.h"
#import "ConfirmIndMatchScoreCVCell.h"
#import "UserPreferences.h"
#import "IndividualMatchRoundScore.h"
#import "AFNetworking.h"
#import "S3Tools.h"
#import "UIImage+ProportionalFill.h"
#import "IOSRequest.h"
#import "ConfirmIndMatchScoreSuccessVC.h"
#import "DisputeIndMatchScoreIntroMessageVC.h"
#import "UIView+Manipulate.h"
#import "UIColor+VaidenColors.h"
#import "UIImage+ImageEffects.h"
#import "NSDate+Utilities.h"

@interface ConfirmIndMatchScoreVC ()

@property (weak, nonatomic) IBOutlet UIImageView *picWinner;
@property (nonatomic, strong) UserPreferences *preferences;

@property (weak, nonatomic) IBOutlet UIView *subBackground1;

@property (weak, nonatomic) IBOutlet UIButton *confirmButtonHandle;
@property (weak, nonatomic) IBOutlet UIButton *disputeButtonHandle;

@property (weak, nonatomic) IBOutlet UILabel *player1Username;
@property (weak, nonatomic) IBOutlet UILabel *player2Username;

@property (weak, nonatomic) IBOutlet UILabel *matchNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *challengeLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;

@property (nonatomic, strong) IndividualMatch *individualMatch;

@property (weak, nonatomic) IBOutlet UICollectionView *scoreCollectionView;
@property (weak, nonatomic) IBOutlet UIScrollView *pageScrollView;

@property (nonatomic) float sessionUserNewLevel;
@property (nonatomic) float sessionUserOldLevel;

@property (nonatomic, strong) UIView *tempView;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIImageView *confirmIcon;
@property (weak, nonatomic) IBOutlet UIImageView *disputeIcon;

@property (weak, nonatomic) IBOutlet UIImageView *scoreSummaryIcon;
@property (weak, nonatomic) IBOutlet UILabel *monthAbbrevLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeAndLocationLabel;
@property (weak, nonatomic) IBOutlet UILabel *dayLabel;
@property (weak, nonatomic) IBOutlet UIView *blueBackground;
@property (weak, nonatomic) IBOutlet UILabel *confirmButtonLabel;
@property (weak, nonatomic) IBOutlet UILabel *disputeButtonLabel;

@property (weak, nonatomic) IBOutlet UIView *inputView;
@property (weak, nonatomic) IBOutlet UIView *confirmView;
@property (weak, nonatomic) IBOutlet UIView *disputeView;


@end

@implementation ConfirmIndMatchScoreVC



- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self showTempSpinner];
    [self loadMatchData];
    
    /* Add corner for the View */
    [self.inputView makeRoundedBorderWithRadius:3];
    [self.confirmView makeRoundedBorderWithRadius:3];
    [self.disputeView makeRoundedBorderWithRadius:3];
    
    self.scoreCollectionView.delegate = self;
    self.scoreCollectionView.dataSource = self;
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
    
    /*[self.subBackground1.layer setBorderColor:[UIColor midGray2].CGColor];
    [self.subBackground1.layer setBorderWidth:0.5];
    [self.subBackground1.layer setMasksToBounds:YES];*/
    
 //   [self.confirmButtonHandle makeCircleWithColorAndBackground:[UIColor peacock] andRadius:5];
 //   [self.disputeButtonHandle makeCircleWithColorAndBackground:[UIColor midGray] andRadius:5];
    
    //self.scoreSummaryIcon.image = [[UIImage imageNamed:@"Score Summary Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.scoreSummaryIcon setTintColor:[UIColor whiteColor]];
    
    //self.confirmIcon.image = [[UIImage imageNamed:@"BTN Confirm Score Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    //[self.confirmIcon setTintColor:[UIColor newBlueDark]];
    
    //self.disputeIcon.image = [[UIImage imageNamed:@"BTN Dispute Score Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    //[self.disputeIcon setTintColor:[UIColor newBlueDark]];
    
    //self.blueBackground.backgroundColor = [UIColor newBlueLight];
    //self.confirmButtonLabel.textColor = [UIColor newBlueDark];
    //self.disputeButtonLabel.textColor = [UIColor newBlueDark];
    
    //[self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back_button"] style:UIBarButtonItemStyleBordered target:self.navigationController action:@selector(popViewControllerAnimated:)]];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    self.pageScrollView.contentSize = CGSizeMake(320.0, 600.0);
}

- (void)showTempSpinner
{
    self.tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    self.tempView.backgroundColor = [UIColor lightGrayColor];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] init];
    [self.activityIndicator setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width/2.0, [UIScreen mainScreen].bounds.size.height/2.0 - 50)];
    [self.activityIndicator startAnimating];
    [self.tempView addSubview:self.activityIndicator];
    
    [self.view addSubview:self.tempView];
    
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.individualMatch.scoreIndMatch.roundScoresIndMatch count];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 2;
}

- (void)loadMatchData
{
    [IOSRequest fetchIndividualMatchDetails:self.matchID
                                sessionUser:[self.preferences getUserID]
                               onCompletion:^(NSDictionary *object) {
                                   
                                   NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
                                   [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                                   NSDate *dateTime = [[formatter dateFromString:object[@"date_time"]] toLocalTime];
                                   
                                   Venue *venue = [[Venue alloc] initVenueWithName:object[@"venue_name"]
                                                                        andVenueID:[object[@"venue_id"] integerValue]
                                                                        andAddress:object[@"venue_street"]
                                                                           andCity:object[@"venue_city"]
                                                                      andStateName:object[@"venue_state"]
                                                                        andStateID:[object[@"venue_state_id"] integerValue]
                                                                       andLatitude:[object[@"latitude"] floatValue]
                                                                      andLongitude:[object[@"longitude"] floatValue]
                                                                    andDescription:object[@"description"]
                                                                        andCreator:[object[@"venue_creator_id"] integerValue]];
                                   
                                   Sport *sport = [[Sport alloc] initWithSportDetails:object[@"sport_name"]
                                                                              andType:object[@"sport_type"]];
                                   
                                   
                                   MatchPlayer *player2 = [MatchPlayer makeMatchCompetitorObject:object[@"competitors"]
                                                                                       forPlayer:2
                                                                                    forSportName:object[@"sport_name"]
                                                                                         andType:object[@"sport_type"]];
                                   
                                   MatchPlayer *player1 = [MatchPlayer makeMatchCompetitorObject:object[@"competitors"]
                                                                                       forPlayer:1
                                                                                    forSportName:object[@"sport_name"]
                                                                                         andType:object[@"sport_type"]];
                                   
                                   MatchPlayer *sessionPlayer = nil;
                                   MatchPlayer *otherPlayer = nil;
                                   
                                   if (player1.userID == [self.preferences getUserID]) {
                                       sessionPlayer = player1;
                                       otherPlayer = player2;
                                   } else {
                                       sessionPlayer = player2;
                                       otherPlayer = player1;
                                   }
                                   
                                   NSDictionary *prob = object[@"probabilities"];
                                   
                                   float prob_session_player;
                                   float prob_other_player;
                                   
                                   if ([prob[@"player1_id"] integerValue] == sessionPlayer.userID) {
                                       prob_session_player = [prob[@"prob_player1"] floatValue];
                                       prob_other_player = [prob[@"prob_player2"] floatValue];
                                   } else {
                                       prob_session_player = [prob[@"prob_player2"] floatValue];
                                       prob_other_player = [prob[@"prob_player1"] floatValue];
                                   }
                                   
                                   
                                   
                                   self.individualMatch = [[IndividualMatch alloc]
                                                           initMatchWithDetails:[object[@"match_id"] integerValue]
                                                           andMatchName:object[@"match_name"]
                                                           isPrivate:[object[@"is_private"] boolValue]
                                                           withSport:sport
                                                           withMessage:object[@"message"]
                                                           onDateTime:dateTime
                                                           atVenue:venue
                                                           createdByUser:[object[@"creator_id"] integerValue]
                                                           withMatchPlayer1:sessionPlayer
                                                           withMatchPlayer2:otherPlayer
                                                           andActive:[object[@"active"] boolValue]
                                                           //                           andScores:nil
                                                           andProbabilityPlayer1Wins:prob_session_player
                                                           andProbabilityPlayer2Wins:prob_other_player];
                                   
                                   id scoreSummaryObject = object[@"score_summary"];
                                   
                                   for (id roundScoreObj in scoreSummaryObject[@"match_scores"]) {
                                       
                                       
                                       [self.individualMatch.scoreIndMatch addRoundScore:[roundScoreObj[@"round"] integerValue]
                                                                              forPlayer1:[roundScoreObj[@"player1_id"] integerValue]
                                                                        withScorePlayer1:[roundScoreObj[@"score_player1"] integerValue]
                                                                              forPlayer2:[roundScoreObj[@"player2_id"] integerValue]
                                                                        withScorePlayer2:[roundScoreObj[@"score_player2"] integerValue]];
                                       [self.individualMatch.scoreIndMatch setVersion:[scoreSummaryObject[@"version"] integerValue]];
                                       [self.individualMatch.scoreIndMatch setScoreRecordedBy:[scoreSummaryObject[@"recorder_user_id"] integerValue]];
                                       [self.individualMatch.scoreIndMatch setScoreConfirmedBy:[scoreSummaryObject[@"confirmer_user_id"] integerValue]];
                                       [self.individualMatch.scoreIndMatch setScoreDisputedBy:[scoreSummaryObject[@"disputor_user_id"] integerValue]];
                                       [self.individualMatch.scoreIndMatch setWasFirstVersionIndividualScoreRecordedBySessionUserAndDisputedByOtherUser:[scoreSummaryObject[@"was_session_user_score_overruled"] boolValue]];
                                   }
                                   
                                   dispatch_async(dispatch_get_main_queue(), ^ {
                                       [self.activityIndicator stopAnimating];
                                       self.tempView.hidden = YES;
                                       [self setPageValues];
                                       [self.scoreCollectionView reloadData];
                                   });
                                   
                                   
                               }];
    
}



- (void)setPageValues
{
    /*
    [self.subBackground1 makeRoundedBorderWithColor:[UIColor whiteColor]];
    
    [self.confirmButtonHandle makeRoundedBorderWithColor:[UIColor peacock]];
    [self.disputeButtonHandle makeRoundedBorderWithColor:[UIColor sienna]];
    [self.confirmButtonHandle setBackgroundColor:[UIColor peacock]];
    [self.disputeButtonHandle setBackgroundColor:[UIColor sienna]];
    
    NSMutableDictionary *netScores = [self.individualMatch.scoreIndMatch getNetScore];
    
    NSInteger player1TotalScore = [[netScores objectForKey:@"player1_score"] integerValue];
    NSInteger player2TotalScore = [[netScores objectForKey:@"player2_score"] integerValue];
//    self.player1Username.text = self.individualMatch.player1.userName;
  //  self.player2Username.text = self.individualMatch.player2.userName;

    
    if (player1TotalScore > player2TotalScore) {
        NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                baseString:self.individualMatch.player1.profileBaseString];
        
        [self.picWinner setImageWithURL:[NSURL URLWithString:url]];
        
        [UIImage makeRoundedImage:self.picWinner withRadius:20];
        
    } else if (player2TotalScore > player1TotalScore) {
        NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                baseString:self.individualMatch.player2.profileBaseString];
        
        [self.picWinner setImageWithURL:[NSURL URLWithString:url]];
        
        [UIImage makeRoundedImage:self.picWinner withRadius:20];
        
    } else if (player1TotalScore == player2TotalScore) {
        
        NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                baseString:self.individualMatch.player2.profileBaseString];
        
        [self.picWinner setImageWithURL:[NSURL URLWithString:url]];
        
        [UIImage makeRoundedImage:self.picWinner withRadius:20];

    }
    
    */
    /*
    NSString *dateText = @"";
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMMM d, YYYY"];
    dateText = [dateFormatter stringFromDate:self.individualMatch.dateTime];
    
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    [timeFormatter setDateFormat:@"hh:mm a"];
    self.dateTimeLabel.text = [NSString stringWithFormat:@"%@ at %@", dateText, [timeFormatter stringFromDate: self.individualMatch.dateTime]];
    
    self.locationLabel.text = self.individualMatch.venue.venueName;
     */
    
    NSString *monthText = @"";
    NSString *dayText = @"";
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMM"];
    monthText = [dateFormatter stringFromDate:self.individualMatch.dateTime];
    
    NSDateFormatter *dateFormatterDay = [[NSDateFormatter alloc] init];
    [dateFormatterDay setDateFormat:@"d"];
    dayText = [dateFormatterDay stringFromDate:self.individualMatch.dateTime];
    
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    [timeFormatter setDateFormat:@"h:mm a"];
    self.dateTimeLabel.text = [NSString stringWithFormat:@"%@ at %@", monthText, [timeFormatter stringFromDate: self.individualMatch.dateTime]];
    
    // self.locationLabel.text = self.individualMatch.venue.venueName;
    self.monthAbbrevLabel.text = monthText;
    self.dayLabel.text = dayText;
    
    self.timeAndLocationLabel.text = [NSString stringWithFormat:@"%@ at %@", [timeFormatter stringFromDate:self.individualMatch.dateTime], self.individualMatch.venue.venueName];
    
    self.matchNameLabel.text = self.individualMatch.matchName;
    self.challengeLabel.text = [NSString stringWithFormat:@"%@ vs %@", self.individualMatch.player1.userName, self.individualMatch.player2.userName];

    
    
}


- (IBAction)confirmAction:(id)sender
{
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.labelText = @"Confirming Score";
    self.confirmButtonHandle.enabled = NO;
    [IOSRequest confirmScoreForIndividualMatchWithMultipleRounds:self.individualMatch.matchID
                                                      forVersion:self.individualMatch.scoreIndMatch.version
                                                     confirmedBy:[self.preferences getUserID]
                                                    onCompletion:^(NSDictionary *results) {
                                                        
                                                    dispatch_async(dispatch_get_main_queue(), ^ {
                                                        self.confirmButtonHandle.enabled = YES;
                                                        [MBProgressHUD hideHUDForView:self.view animated:YES];

                                                        if ([results[@"outcome"] isEqualToString:@"success"]) {
                                                            self.sessionUserOldLevel = [results[@"session_user_old_level"] integerValue];
                                                            self.sessionUserNewLevel = [results[@"session_user_new_level"] integerValue];
                                                                [self.preferences setMatchNotificationRefreshState:YES];
                                                                [self.preferences setNewsfeedRefreshState:YES];
                                                                [self.preferences setAlertsPageRefreshState:YES];
                                                                [self performSegueWithIdentifier:@"Confirm Success Segue" sender:self];
                                                        } else {
                                                            UIAlertView *alert = [[UIAlertView alloc] init];
                                                            alert.title = @"Confirm Score Error";
                                                            alert.message = @"There was a problem in submitting your request.  Please try again later.";
                                                            [alert addButtonWithTitle:@"OK"];
                                                            [alert show];
                                                        }
                                                        });
                                                    }];
    
}

- (IBAction)disputeAction:(id)sender
{
    //    if (self.individualMatch.scoreIndMatch.version > 1) {
    
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.labelText = @"Disputing Score";
    self.disputeButtonHandle.enabled = NO;
    /*
     [IOSRequest disputeScoreForIndividualMatchWithMultipleRounds:self.individualMatch.matchID
     disputedBy:[self.preferences getUserID]
     onCompletion:^(NSDictionary *results) {
     dispatch_async(dispatch_get_main_queue(), ^ {
     self.disputeButtonHandle.enabled = YES;
     [self.preferences setMatchNotificationRefreshState:YES];
     [self.preferences setNewsfeedRefreshState:YES];
     [self performSegueWithIdentifier:@"Excessive Dispute Segue" sender:self];
     });
     }];
     */
    [IOSRequest setMatchAsDisputed:self.individualMatch.matchID
                        disputedBy:[self.preferences getUserID]
                      onCompletion:^(NSDictionary *results) {
                          
                          dispatch_async(dispatch_get_main_queue(), ^{
                              [MBProgressHUD hideHUDForView:self.view animated:YES];
                              self.disputeButtonHandle.enabled = YES;
                              
                              if ([results[@"outcome"] isEqualToString:@"success"]) {
                                  [self.preferences setMatchNotificationRefreshState:YES];
                                  [self.preferences setNewsfeedRefreshState:YES];
                                  [self performSegueWithIdentifier:@"Excessive Dispute Segue" sender:self];

                              } else {
                                  UIAlertView *alert = [[UIAlertView alloc] init];
                                  alert.title = @"Dispute Score Error";
                                  alert.message = @"There was a problem in submitting your request.  Please try again later.";
                                  [alert addButtonWithTitle:@"OK"];
                                  [alert show];
                              }
                              
                          });
                          
                      }];
    //   } else {
    //        [self performSegueWithIdentifier:@"Dispute Segue" sender:self];
    //    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Confirm Success Segue"]) {
        ConfirmIndMatchScoreSuccessVC *controller = segue.destinationViewController;
   //     controller.sessionUserOldLevel = self.sessionUserOldLevel;
   //     controller.sessionUserNewLevel = self.sessionUserNewLevel;
   //     controller.sportName = self.individualMatch.sport.sportName;
        controller.matchID = self.individualMatch.matchID;
        
    } else if ([segue.identifier isEqualToString:@"Dispute Segue"]) {
        DisputeIndMatchScoreIntroMessageVC *controller = segue.destinationViewController;
        controller.individualMatch = self.individualMatch;
    }
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"My Cell";
    
    ConfirmIndMatchScoreCVCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    IndividualMatchRoundScore *matchScore = [self.individualMatch.scoreIndMatch.roundScoresIndMatch objectAtIndex:indexPath.row];
    
    if (indexPath.section == 0) { // first section
        cell.scoreLabel.text = [NSString stringWithFormat:@"%ld", matchScore.scorePlayer1];
        self.player1Username.text = [self.individualMatch getUsernameForScoreUserID:matchScore.userIDPlayer1];// I know this is not efficient...doing this for time's sake for now.
    } else if (indexPath.section == 1) { // second section
        cell.scoreLabel.text = [NSString stringWithFormat:@"%ld", matchScore.scorePlayer2];
        self.player2Username.text = [self.individualMatch getUsernameForScoreUserID:matchScore.userIDPlayer2]; // I know this is not efficient...doing this for time's sake for now.
    }
    
    return cell;
}



@end
