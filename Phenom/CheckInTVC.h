//
//  CheckInTVC.h
//  Vaiden
//
//  Created by James Chung on 4/22/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "CustomBaseTVC.h"
#import "ChooseContactsUtilityTVC.h"
#import "ChooseOneSportTVC.h"
#import <CoreLocation/CoreLocation.h>
#import "MBProgressHUD.h"


@interface CheckInTVC : CustomBaseTVC <ChooseContactsUtilityTVC_Delegate, ChooseOneSportTVC_Delegate, CLLocationManagerDelegate, MBProgressHUDDelegate>
{
    CLLocationManager *locationManager;
    MBProgressHUD *HUD;
}

@property (nonatomic) NSInteger venueID;

@property (nonatomic, strong) NSString *venueName;
@end
