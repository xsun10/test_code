//
//  PlayingSeason.h
//  Vaiden
//
//  Created by James Chung on 5/8/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PlayingSeason : NSObject

@property (nonatomic, strong) NSString *season;
@property (nonatomic) NSInteger month;

// this first function setSeasonWithCurrentMonth only initializes the season value to the current month...if the season is set again later
// then the season may not correspond to the appropriate month that is stored in this object.
- (void)setSeasonWithCurrentMonth;
+ (NSString *)getSeasonByMonth:(NSInteger)month;
- (UIImage *)seasonIcon;

- (PlayingSeason *)initWithSeason:(NSString *)season;
@end
