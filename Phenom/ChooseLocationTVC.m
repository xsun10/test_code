//
//  ChooseLocationTVC.m
//  Vaiden
//
//  Created by James Chung on 5/8/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "ChooseLocationTVC.h"
#import "ChooseLocationCell.h"
#import "IOSRequest.h"
#import "UserPreferences.h"

@interface ChooseLocationTVC () <UISearchBarDelegate>

@property (nonatomic, strong) UserPreferences *preferences;
@property (nonatomic, strong) NSMutableArray *venuesArray;
@property (weak, nonatomic) IBOutlet UISearchBar *venueSearchBar;
@property (nonatomic, strong) UIBarButtonItem *doneBarButton;

@end

@implementation ChooseLocationTVC

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (NSMutableArray *)venuesArray
{
    if (!_venuesArray) _venuesArray = [[NSMutableArray alloc] init];
    return _venuesArray;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.venueSearchBar.delegate = self;
//    [self setDoneButton];
}

// As text changes should query server for similar matches and update the tableview
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [self.venuesArray removeAllObjects];
    
    [IOSRequest getVenuesWithSimilarNamesTo:self.venueSearchBar.text
                               onCompletion:^(NSMutableArray *results) {
                                 
                                   for (id object in results) {
                                       Venue *v = [[Venue alloc] initVenueWithName:object[@"venue_name"]
                                                                    andVenueID:[object[@"venue_id"] integerValue]];
                                       [v setCity:object[@"city"]];
                                       [v setStateName:object[@"state"]];
                                   
                                       [self.venuesArray addObject:v];
                                   }
                                   dispatch_async(dispatch_get_main_queue(), ^{
                                       
                                       
                                       [self.tableView reloadData];
                                   });
                               }];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 63.0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.venuesArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ChooseLocationCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Location Cell" forIndexPath:indexPath];
    
    Venue *v = [self.venuesArray objectAtIndex:indexPath.row];
    
    cell.confirmationIcon.image = [UIImage imageNamed:@"Minus Circle Icon"];
    cell.venueNameLabel.text = v.venueName;
    
    if ([v.city length] > 0 && [v.stateName length] > 0)
        cell.cityStateLabel.text = [NSString stringWithFormat:@"%@, %@", v.city, v.stateName];
    else if ([v.city length] > 0 && [v.stateName length] == 0)
        cell.cityStateLabel.text = v.city;
    else if ([v.city length] == 0 && [v.stateName length] > 0)
        cell.cityStateLabel.text = v.stateName;
    else
        cell.cityStateLabel.text = @"No Address Specified";
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ChooseLocationCell *cell = (ChooseLocationCell *)[tableView cellForRowAtIndexPath:indexPath];
    
    Venue *selectedVenue = [self.venuesArray objectAtIndex:indexPath.row];

    cell.confirmationIcon.image = [UIImage imageNamed:@"Check Circle Icon"];
    
    [self.delegate setWithViewController:self withVenue:selectedVenue];
    
    [self.navigationController popViewControllerAnimated:YES];
}


/*- (void)setDoneButton
{
    UIButton *btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 50, 21);
    btn.layer.borderColor = [UIColor whiteColor].CGColor;
    btn.layer.borderWidth = 1.0;
    
    [btn addTarget:self action:@selector(doneAction:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundColor:[UIColor clearColor]];
    [btn setTitle:@"DONE" forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-MediumItalic" size:12.0]];
    
    // Initialize UIBarbuttonitem...
    self.doneBarButton = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = self.doneBarButton;
}*/

- (IBAction)doneAction2:(UIButton *)sender
{
    [self.delegate setWithViewController:self withVenue:[[Venue alloc] initVenueWithName:self.venueSearchBar.text
                                                                              andVenueID:0]];
    [self.navigationController popViewControllerAnimated:YES];

}

@end
