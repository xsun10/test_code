//
//  NumPastMatchesCell.h
//  Vaiden
//
//  Created by James Chung on 12/12/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XYPieChart.h"
#import "IndividualMatch.h"


@interface NumPastMatchesCell : UITableViewCell <XYPieChartDataSource, XYPieChartDelegate>

@property (weak, nonatomic) IBOutlet UILabel *numPastMatches;
@property (weak, nonatomic) IBOutlet UILabel *numWins;
@property (weak, nonatomic) IBOutlet UILabel *numLosses;
@property (weak, nonatomic) IBOutlet UILabel *numVoided;
@property (weak, nonatomic) IBOutlet UILabel *lastMatchDate;
@property (weak, nonatomic) IBOutlet UILabel *vsPlayer;
@property (weak, nonatomic) IBOutlet UILabel *numTies;
@property (weak, nonatomic) IBOutlet UILabel *winLabel;
@property (weak, nonatomic) IBOutlet UILabel *lossLabel;
@property (weak, nonatomic) IBOutlet UILabel *tieLabel;
@property (weak, nonatomic) IBOutlet UILabel *matchLabel;
@property (weak, nonatomic) IBOutlet UIView *horizBar;

@property (weak, nonatomic) IBOutlet UILabel *unofficialLabel;
@property (weak, nonatomic) IBOutlet UILabel *numUnofficial;

// For pie chart

@property (strong, nonatomic) IBOutlet XYPieChart *pieChartRight;
@property (strong, nonatomic) IBOutlet XYPieChart *pieChartLeft;
@property (strong, nonatomic) IBOutlet UILabel *percentageLabel;
@property (strong, nonatomic) IBOutlet UILabel *selectedSliceLabel;
@property (strong, nonatomic) IBOutlet UITextField *numOfSlices;
@property (strong, nonatomic) IBOutlet UISegmentedControl *indexOfSlices;
@property (strong, nonatomic) IBOutlet UIButton *downArrow;
@property(nonatomic, strong) NSMutableArray *slices;
@property(nonatomic, strong) NSArray        *sliceColors;
@property (weak, nonatomic) IBOutlet UIView *circlePortion;

@property (nonatomic) NSInteger winValue;
@property (nonatomic) NSInteger lossValue;
@property (nonatomic) NSInteger tieValue;
@property (nonatomic) NSInteger voidValue;

@property (weak, nonatomic) IBOutlet UIImageView *profilePic;
- (void)loadPie;

@property (weak, nonatomic) IBOutlet UIButton *viewListButton;

@end
