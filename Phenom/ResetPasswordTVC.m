//
//  ResetPasswordTVC.m
//  Vaiden
//
//  Created by James Chung on 4/2/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "ResetPasswordTVC.h"
#import "IOSRequest.h"
#import "UIView+Manipulate.h"

@interface ResetPasswordTVC () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *myTextField;
@property (strong, nonatomic)  UIBarButtonItem *doneButton;

@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet UIImageView *usernameIcon;

@end

@implementation ResetPasswordTVC


- (void)viewDidLoad
{
    [super viewDidLoad];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    //[self setDoneButton];
    
    UIColor *grayColor = [UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:1.0];
    
    self.usernameIcon.image = [[UIImage imageNamed:@"username"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.usernameIcon setTintColor:grayColor];
    [self.submitButton makeRoundedBorderWithRadius:3];
    
    self.myTextField.delegate = self;
}


/*- (void)setDoneButton
{
    UIButton *btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 50, 21);
    btn.layer.borderColor = [UIColor whiteColor].CGColor;
    btn.layer.borderWidth = 1.0;
    
    [btn addTarget:self action:@selector(submitData:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundColor:[UIColor clearColor]];
    [btn setTitle:@"DONE" forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-MediumItalic" size:12.0]];
    
    // Initialize UIBarbuttonitem...
    self.doneButton = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = self.doneButton;
}*/

- (IBAction)submitButtonClicked:(id)sender
{
    [self submitData];
}

- (BOOL) checkTrimStringEmpty: (NSString *)string
{
    return [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0 ? YES : NO;
}

- (void)submitData
{
    if ([self verifyResetString]) {
        
        [self.myTextField resignFirstResponder];
        
        if ([self checkTrimStringEmpty:self.myTextField.text]== YES) {
            UIAlertView *alert = [[UIAlertView alloc] init];
            alert.title = @"Username or Email Empty";
            alert.message = @"Please input a valid username or email.";
            [alert addButtonWithTitle:@"OK"];
            [alert show];
        } else {
            self.doneButton.enabled = NO;
            HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            HUD.labelText = @"Checking...";
            
            [IOSRequest resetPasswordWithEntry:self.myTextField.text onCompletion:^(NSDictionary *results) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.doneButton.enabled = YES;
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                    UIAlertView *alert = [[UIAlertView alloc] init];
                    
                    
                    if ([results[@"outcome"] isEqualToString:@"unknown"]) {
                        alert.title = @"User not found";
                        alert.message = @"We were unfortunately not able to find your account.";
                        [alert addButtonWithTitle:@"OK"];
                        [alert show];
                    } else if ([results[@"outcome"] isEqualToString:@"failure"]) {
                        alert.title = @"Submission Error";
                        alert.message = @"There was a problem completing your request.  Please retry later.";
                        [alert addButtonWithTitle:@"OK"];
                        [alert show];
                    } else {
                        // successfully sent reset email
                        [self performSegueWithIdentifier:@"Success Segue" sender:self];
                    }
                });
                
            }];
            
            double delayInSeconds = 10.0;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                //code to be executed on the main queue after delay
                self.doneButton.enabled = YES;
                [MBProgressHUD hideHUDForView:self.view animated:YES];
            });
        }
    }
    
}

- (BOOL)verifyResetString
{
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self submitData];
    return YES;
}

@end
