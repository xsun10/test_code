//
//  ChooseStatTagTVC.h
//  Vaiden
//
//  Created by James Chung on 5/27/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "CustomBaseTVC.h"
#import "Sport.h"

@class ChooseStatTagTVC;

@protocol ChooseStatTagTVC_Delegate <NSObject>

- (void)setWithViewController:(ChooseStatTagTVC *)controller withStatTags:(NSMutableArray *)statTags withTotal:(NSInteger)total;

@end


@interface ChooseStatTagTVC : CustomBaseTVC

@property (nonatomic, weak) id <ChooseStatTagTVC_Delegate> delegate;

@property (nonatomic) Sport *sportObj;

@end
