//
//  ResetPasswordSuccessVC.m
//  Vaiden
//
//  Created by James Chung on 4/2/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "ResetPasswordSuccessVC.h"

@interface ResetPasswordSuccessVC ()

@end

@implementation ResetPasswordSuccessVC


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    /*UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];*/
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationItem.hidesBackButton = YES;
}

- (IBAction)backButtonIsClicked:(id)sender{
    [self.navigationController popToRootViewControllerAnimated:YES];
}



@end
