//
//  ShowVideoVC.h
//  Phenom
//
//  Created by James Chung on 5/30/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>


@interface ShowVideoVC : UIViewController
@property (strong, nonatomic) MPMoviePlayerController *moviePlayer;
- (IBAction)playMovie:(id)sender;
@property (nonatomic, strong) NSURL *webAddress;

@property (nonatomic) BOOL showVideoFlag;

@end
