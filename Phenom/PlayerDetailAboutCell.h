//
//  PlayerDetailAboutCell.h
//  Vaiden
//
//  Created by James Chung on 3/17/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlayerDetailAboutCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *aboutTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *aboutUsernameLabel;

@end
