//
//  MatchesNearbyCell.h
//  Phenom
//
//  Created by James Chung on 5/22/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MatchesNearbyCell : UITableViewCell  <UICollectionViewDelegate, UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UILabel *sportLabel;
@property (weak, nonatomic) IBOutlet UILabel *experienceLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet UIImageView *padlockImage;
@property (weak, nonatomic) IBOutlet UICollectionView *playerCollectionView;
@property (weak, nonatomic) IBOutlet UIImageView *sportIcon;
@property (weak, nonatomic) IBOutlet UILabel *matchHeading;
@property (weak, nonatomic) IBOutlet UILabel *matchNameLabel;
@property (weak, nonatomic) IBOutlet UIView *sportIconBackground;

@property (weak, nonatomic) IBOutlet UILabel *venueLabel;
@property (weak, nonatomic) IBOutlet UILabel *genderLabel;
@property (weak, nonatomic) IBOutlet UIImageView *forwardIcon;
@property (weak, nonatomic) IBOutlet UIImageView *genderIcon;
@property (weak, nonatomic) IBOutlet UIImageView *sportIconDetail;

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (nonatomic) NSInteger minCompetitors;
@property (nonatomic, strong) NSMutableArray *competitors;
@property (weak, nonatomic) IBOutlet UIView *barBackground;
@property (weak, nonatomic) IBOutlet UIView *iconBackgroundView;
@property (weak, nonatomic) IBOutlet UIView *headlineBackground;
@property (weak, nonatomic) IBOutlet UIImageView *starIcon;
@property (weak, nonatomic) IBOutlet UIButton *detailButton;
@property (weak, nonatomic) IBOutlet UIImageView *locationIcon;
@property (weak, nonatomic) IBOutlet UIView *mainBackgroundViewCell;

@property (weak, nonatomic) IBOutlet UILabel *sportLabelTop;
@property (weak, nonatomic) IBOutlet UIImageView *sportPictureBackground;

@end
