//
//  IMDProbabilityCell.h
//  Vaiden
//
//  Created by James Chung on 12/26/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IMDProbabilityCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *numProbabilityLabel;
@property (weak, nonatomic) IBOutlet UIImageView *winnerPic;
@property (weak, nonatomic) IBOutlet UIView *probabilityIconPicBackground;
@property (weak, nonatomic) IBOutlet UIImageView *probabilityIconPic;
@property (weak, nonatomic) IBOutlet UIView *myBackgroundView;

@end
