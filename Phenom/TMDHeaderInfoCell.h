//
//  TMDHeaderInfoCell.h
//  Vaiden
//
//  Created by James Chung on 4/16/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TMDHeaderInfoCell;

@protocol TMDHeaderInfoCell_Delegate <NSObject>
- (void)setWithButtonMessage:(TMDHeaderInfoCell *)controller withMessage:(NSString *)messageAction;

@end


@interface TMDHeaderInfoCell : UITableViewCell <UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, weak) id <TMDHeaderInfoCell_Delegate> delegate;


@property (weak, nonatomic) IBOutlet UIImageView *team1Pic;
@property (weak, nonatomic) IBOutlet UIImageView *team2Pic;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundPic;
@property (weak, nonatomic) IBOutlet UILabel *matchNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *vsLabel;
@property (weak, nonatomic) IBOutlet UILabel *sportLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *buttonCollectionView;


//- (void)configureButtonDisplay:(NSInteger)matchButtonConfig;

#define BUTTON_CONFIG_EMPTY_SCORE 0
#define BUTTON_CONFIG_SHOW_SCORE 1
#define BUTTON_CONFIG_CONFIRM_BUTTONS 2
#define BUTTON_CONFIG_RECORD_BUTTONS 3
#define BUTTON_CONFIG_EMPTY_MATCH_OUTCOME 4
#define BUTTON_CONFIG_CANCELLED_MATCH 5
#define BUTTON_CONFIG_MATCH_CREATOR 6
#define BUTTON_CONFIG_CONFIRMED_INVITED_CAPTAIN 7 // then team is confirmed
#define BUTTON_CONFIG_UNCONFIRMED_INVITED_CAPTAIN 8 // then team is unconfirmed
#define BUTTON_CONFIG_CONFIRMED_TEAM_PLAYER 9 // team must be confirmed
#define BUTTON_CONFIG_UNCONFIRMED_TEAM_PLAYER 10 // team may be confirmed
#define BUTTON_CONFIG_MINIMAL_DISPLAY 11


@end
