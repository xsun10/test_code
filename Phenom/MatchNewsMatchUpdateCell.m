//
//  MatchNewsMatchUpdateCell.m
//  Vaiden
//
//  Created by James Chung on 10/18/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "MatchNewsMatchUpdateCell.h"
#import "Player.h"
#import "NewsfeedMatchHeadingCVCell.h"
#import "S3Tools.h"
#import "AFNetworking.h"
#import "UIImage+ProportionalFill.h"

@implementation MatchNewsMatchUpdateCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)layoutSubviews
{
    self.matchHeadingCV.delegate = self;
    self.matchHeadingCV.dataSource = self;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    if ([self.matchCategory isEqualToString:@"individual"]) {
        return 3;
    }
    return [self.competitors count];
}

- (void)goToProfile:(UIButton *)sender
{
    [self.delegate setMatchNewsMatchUpdateCellParentViewController:self withUserIDToSegue:sender.tag];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Competitor Pics";
    
    NewsfeedMatchHeadingCVCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    
    Player *p = nil;
    
    if ([self.matchCategory isEqualToString:@"individual"]) {
        if (indexPath.row == 1) {
            // show "vs" not the pic
            cell.playerPic.hidden = YES;
            UILabel *vsLabel = [[UILabel alloc] initWithFrame:CGRectMake(8, 0, 40, 40)];
            vsLabel.text = @"VS";
            [vsLabel setFont:[UIFont systemFontOfSize:18]];
            
            [cell addSubview:vsLabel];
            
            
        } else {
            
            
            if (indexPath.row == 2)
                p  = [self.competitors objectAtIndex:1];
            else
                p = [self.competitors objectAtIndex:0];
            
            NSString *urlMain = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                        baseString:p.profileBaseString];
            
            [cell.playerPic setImageWithURL:[NSURL URLWithString:urlMain]
                           placeholderImage:[UIImage imageNamed:@"avatar round"]];
            [UIImage makeRoundedImage:cell.playerPic withRadius:20];
            
            // profile pic button
            UIButton *picButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
            [picButton addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
            picButton.tag = p.userID;
            [cell.playerPic addSubview:picButton];
            cell.playerPic.userInteractionEnabled = YES;
            // end profile pic button
            
        }
        
        return  cell;
    } else {
        p = [self.competitors objectAtIndex:indexPath.row];
        NSString *urlMain = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                    baseString:p.profileBaseString];
        
        [cell.playerPic setImageWithURL:[NSURL URLWithString:urlMain]
                       placeholderImage:[UIImage imageNamed:@"avatar round"]];
        [UIImage makeRoundedImage:cell.playerPic withRadius:20];
        
        
        return  cell;
    }
    
    return nil;
}
@end
