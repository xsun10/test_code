//
//  RecordIndMatchScoreVC.m
//  Vaiden
//
//  Created by James Chung on 11/25/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "RecordIndMatchScoreVC.h"
#import "S3Tools.h"
#import "AFNetworking.h"
#import "IOSRequest.h"
#import "UIImage+ProportionalFill.h"
#import "IndividualMatchScore.h"
#import "IndividualMatchRoundScore.h"
#import "RecordIndMatchScoreSuccess.h"
#import "UserPreferences.h"
#import "UIView+Manipulate.h"
#import "UIImage+ImageEffects.h"
#import "UIButton+RoundBorder.h"
#import "UIColor+VaidenColors.h"
#import "RecordIndMatchScoreCell.h"
#import "NSDate+Utilities.h"

@interface RecordIndMatchScoreVC ()

@property (weak, nonatomic) IBOutlet UIImageView *picPlayer1;
@property (weak, nonatomic) IBOutlet UIImageView *picPlayer2;
@property (weak, nonatomic) IBOutlet UILabel *usernamePlayer1;
@property (weak, nonatomic) IBOutlet UILabel *usernamePlayer2;
@property (weak, nonatomic) IBOutlet UITextField *pointsPlayer1;
@property (weak, nonatomic) IBOutlet UITextField *pointsPlayer2;
@property (nonatomic, strong) UserPreferences *preferences;
@property (weak, nonatomic) IBOutlet UIView *subBackground2;
@property (weak, nonatomic) IBOutlet UIView *subBackground1;
@property (weak, nonatomic) IBOutlet UILabel *matchNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *sportLabel;
@property (weak, nonatomic) IBOutlet UIImageView *sportIcon;
@property (weak, nonatomic) IBOutlet UILabel *vsLabel;
//@property (weak, nonatomic) IBOutlet UIBarButtonItem *doneButton;
@property (weak, nonatomic) IBOutlet UIButton *addRoundButton;
@property (weak, nonatomic) IBOutlet UILabel *roundLabel;
@property (weak, nonatomic) IBOutlet UILabel *pointLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *pageScrollView;
@property (weak, nonatomic) IBOutlet UICollectionView *scoreCollectionView;

@property (weak, nonatomic) IBOutlet UILabel *username1Label_v2;
@property (weak, nonatomic) IBOutlet UILabel *username2Label_v2;
@property (nonatomic, strong) IndividualMatch *individualMatch;
@property (weak, nonatomic) IBOutlet UIView *instructionSectionView;
@property (weak, nonatomic) IBOutlet UIImageView *bulbIcon;
@property (weak, nonatomic) IBOutlet UIView *blueBackground;
@property (weak, nonatomic) IBOutlet UILabel *instructionText;
@property (weak, nonatomic) IBOutlet UILabel *challengeVSLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeAndLocationLabel;
@property (weak, nonatomic) IBOutlet UILabel *monthAbbrevLabel;
@property (weak, nonatomic) IBOutlet UILabel *dayLabel;

@property (nonatomic, strong) UIView *tempView;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIView *scoreEntryBackground;
@property (nonatomic, strong) UIBarButtonItem *doneBarButton;

@property (weak, nonatomic) IBOutlet UIView *roundCornerTableView;
@property (weak, nonatomic) IBOutlet UILabel *hintLabel;
@end

@implementation RecordIndMatchScoreVC

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self showTempSpinner];
    [self.roundCornerTableView makeRoundedBorderWithRadius:3];
    self.scoreCollectionView.delegate = self;
    self.scoreCollectionView.dataSource = self;
	[self.individualMatch.scoreIndMatch.roundScoresIndMatch removeAllObjects]; // reset scores
    
    //[self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back_button"] style:UIBarButtonItemStyleBordered target:self.navigationController action:@selector(popViewControllerAnimated:)]];

    [self loadMatchData];
    //[self setDoneButton];
    
    //self.blueBackground.backgroundColor = [UIColor newBlueLight];
}

/*- (void)setDoneButton
{
    UIButton *btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 50, 21);
    //btn.layer.borderColor = [UIColor whiteColor].CGColor;
    //btn.layer.borderWidth = 1.0;
    
    [btn addTarget:self action:@selector(submitScore:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundColor:[UIColor clearColor]];
    [btn setTitle:@"DONE" forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-MediumItalic" size:12.0]];
    
    // Initialize UIBarbuttonitem...
    self.doneBarButton = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = self.doneBarButton;
}*/

- (void)showTempSpinner
{
    self.tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    self.tempView.backgroundColor = [UIColor lightGrayColor];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] init];
    [self.activityIndicator setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width/2.0, [UIScreen mainScreen].bounds.size.height/2.0 - 50)];
    [self.activityIndicator startAnimating];
    [self.tempView addSubview:self.activityIndicator];
    
    [self.view addSubview:self.tempView];
    
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    self.pageScrollView.contentSize = CGSizeMake(320.0, 570.0);
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 2;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    
    return [self.individualMatch.scoreIndMatch.roundScoresIndMatch count];
}

- (void)loadMatchData
{
    [IOSRequest fetchIndividualMatchDetails:self.matchID
                                sessionUser:[self.preferences getUserID]
                               onCompletion:^(NSDictionary *object) {
                                   
                                   NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
                                   [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                                   NSDate *dateTime = [[formatter dateFromString:object[@"date_time"]] toLocalTime];
                                   
                                   Venue *venue = [[Venue alloc] initVenueWithName:object[@"venue_name"]
                                                                        andVenueID:[object[@"venue_id"] integerValue]
                                                                        andAddress:object[@"venue_street"]
                                                                           andCity:object[@"venue_city"]
                                                                      andStateName:object[@"venue_state"]
                                                                        andStateID:[object[@"venue_state_id"] integerValue]
                                                                       andLatitude:[object[@"latitude"] floatValue]
                                                                      andLongitude:[object[@"longitude"] floatValue]
                                                                    andDescription:object[@"description"]
                                                                        andCreator:[object[@"venue_creator_id"] integerValue]];
                                   
                                   
                                   Sport *sport = [[Sport alloc] initWithSportDetails:[object[@"sport_id"] integerValue]
                                                                              andName:object[@"sport_name"]
                                                                              andType:object[@"sport_type"]
                                                                      largerScoreWins:[object[@"score_order"] boolValue]
                                                                          setUnitName:object[@"set_unit_name"]
                                                                         gameUnitName:object[@"game_unit_name"]];
                                   [sport setMaxRound:[object[@"sport_max_round"] integerValue]];
                                   
                                   MatchPlayer *player2 = [MatchPlayer makeMatchCompetitorObject:object[@"competitors"]
                                                                                       forPlayer:2
                                                                                    forSportName:object[@"sport_name"]
                                                                                         andType:object[@"sport_type"]];
                                   
                                   MatchPlayer *player1 = [MatchPlayer makeMatchCompetitorObject:object[@"competitors"]
                                                                                       forPlayer:1
                                                                                    forSportName:object[@"sport_name"]
                                                                                         andType:object[@"sport_type"]];
                                   
                                   MatchPlayer *sessionPlayer = nil;
                                   MatchPlayer *otherPlayer = nil;
                                   
                                   if (player1.userID == [self.preferences getUserID]) {
                                       sessionPlayer = player1;
                                       otherPlayer = player2;
                                   } else {
                                       sessionPlayer = player2;
                                       otherPlayer = player1;
                                   }
                                   
                                   NSDictionary *prob = object[@"probabilities"];
                                   
                                   float prob_session_player;
                                   float prob_other_player;
                                   
                                   if ([prob[@"player1_id"] integerValue] == sessionPlayer.userID) {
                                       prob_session_player = [prob[@"prob_player1"] floatValue];
                                       prob_other_player = [prob[@"prob_player2"] floatValue];
                                   } else {
                                       prob_session_player = [prob[@"prob_player2"] floatValue];
                                       prob_other_player = [prob[@"prob_player1"] floatValue];
                                   }
                                   
                                
                                   self.individualMatch = [[IndividualMatch alloc]
                                                           initMatchWithDetails:[object[@"match_id"] integerValue]
                                                           andMatchName:object[@"match_name"]
                                                           isPrivate:[object[@"is_private"] boolValue]
                                                           withSport:sport
                                                           withMessage:object[@"message"]
                                                           onDateTime:dateTime
                                                           atVenue:venue
                                                           createdByUser:[object[@"creator_id"] integerValue]
                                                           withMatchPlayer1:sessionPlayer
                                                           withMatchPlayer2:otherPlayer
                                                           andActive:[object[@"active"] boolValue]
                                                           andProbabilityPlayer1Wins:prob_session_player
                                                           andProbabilityPlayer2Wins:prob_other_player];
                                   
                                   dispatch_async(dispatch_get_main_queue(), ^ {
                                       [self.activityIndicator stopAnimating];
                                       self.tempView.hidden = YES;
                                       [self setPageValues];
                                   });
                                   
                                   
                               }];
    
}


- (IBAction)submitScore:(id)sender
{
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.labelText = @"Recording Score";
    NSLog(@"%@,%hhd",self.individualMatch.sport.sportName,self.individualMatch.sport.largerScoreWins);
    if ([self.pointsPlayer1.text integerValue] > 200 || [self.pointsPlayer2.text integerValue] > 200) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Invalid Points";
        alert.message = [NSString stringWithFormat:@"Sorry, but points for a player should be less than 200"];
        [alert addButtonWithTitle:@"OK"];
        [alert show];
    }else if ([self.pointsPlayer1.text length] > 0 && [self.pointsPlayer2.text length] > 0) {
        [self addRoundToArray];
        self.doneBarButton.enabled = NO;
        
        [IOSRequest setScoreForIndividualMatchWithMultipleRounds:self.individualMatch
                                                      withScores:[self makeDataObject]
                                                     confirmedBy:[self.preferences getUserID]
                                                    onCompletion:^(NSDictionary *results) {
                                                        dispatch_async(dispatch_get_main_queue(), ^ {
                                                            [MBProgressHUD hideHUDForView:self.view animated:YES];
                                                            [self.preferences setNewsfeedRefreshState:YES];
                                                            [self.preferences setMatchNotificationRefreshState:YES];
                                                            [self.preferences setAlertsPageRefreshState:YES];
                                                            
                                                            if ([results[@"creation_status"] isEqualToString:@"OK"]) {
                                                                [self performSegueWithIdentifier:@"Record Score Success" sender:self];
                                                            } else if ([results[@"creation_status"] isEqualToString:@"SCORE_ALREADY_RECORDED"]) {
                                                                [self showScoreAlreadyRecordedError];
                                                                [self.navigationController popToRootViewControllerAnimated:YES];
                                                            } else if ([results[@"creation_status"] isEqualToString:@"VOIDED_MATCH"]) {
                                                                [self showMatchVoidedError];
                                                                [self.navigationController popToRootViewControllerAnimated:YES];
                                                            }
                                                            
                                                            
                                                            self.doneBarButton.enabled = YES;
                                                        });
                                                        
                                                    }];
    } else if (([self.pointsPlayer1.text length] == 0 || [self.pointsPlayer2.text length] == 0)
               && [self.individualMatch.scoreIndMatch.roundScoresIndMatch count] == 0) {
        // show alert because if going to submit, then need both text fields with something in it.
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Submission Error";
        alert.message = @"Please enter the scores for both players.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
    } else {
        self.doneBarButton.enabled = NO;
        // can submit when both text fields are empty...just don't add the values to the round array
        [IOSRequest setScoreForIndividualMatchWithMultipleRounds:self.individualMatch
                                                      withScores:[self makeDataObject]
                                                     confirmedBy:[self.preferences getUserID]
                                                    onCompletion:^(NSDictionary *results) {
                                                        
                                            
                                                        dispatch_async(dispatch_get_main_queue(), ^ {
                                                            [MBProgressHUD hideHUDForView:self.view animated:YES];
                                                            [self.preferences setNewsfeedRefreshState:YES];
                                                            [self.preferences setMatchNotificationRefreshState:YES];
                                                            [self.preferences setAlertsPageRefreshState:YES];
                                                            
                                                            if ([results[@"creation_status"] isEqualToString:@"OK"]) {
                                                                [self performSegueWithIdentifier:@"Record Score Success" sender:self];
                                                            } else if ([results[@"creation_status"] isEqualToString:@"SCORE_ALREADY_RECORDED"]) {
                                                                [self showScoreAlreadyRecordedError];
                                                                [self.navigationController popToRootViewControllerAnimated:YES];
                                                            } else if ([results[@"creation_status"] isEqualToString:@"VOIDED_MATCH"]) {
                                                                [self showMatchVoidedError];
                                                                [self.navigationController popToRootViewControllerAnimated:YES];
                                                            }
                                                            
                                                            
                                                            [self performSegueWithIdentifier:@"Record Score Success" sender:self];
                                                            self.doneBarButton.enabled = YES;
                                                        });
                                                        
                                                    }];
    }
    
    
}

- (void)showScoreAlreadyRecordedError
{
    UIAlertView *alert = [[UIAlertView alloc] init];
    alert.title = @"Submission Error";
    alert.message = [NSString stringWithFormat:@"Your opponent has already recorded a score.  Please review the score they entered"];
    [alert addButtonWithTitle:@"OK"];
    [alert show];
    
}

- (void)showMatchVoidedError
{
    UIAlertView *alert = [[UIAlertView alloc] init];
    alert.title = @"Submission Error";
    alert.message = [NSString stringWithFormat:@"Your opponent said this match never occurred.  Unfortunately, a score cannot be recorded for this match"];
    [alert addButtonWithTitle:@"OK"];
    [alert show];
}
- (NSMutableDictionary *)makeDataObject
{
    NSMutableDictionary *myScores = [[NSMutableDictionary alloc] init];
    int counter = 0;
    NSString *player1String = @"";
    NSString *player2String = @"";
    
    for (IndividualMatchRoundScore *matchScore in self.individualMatch.scoreIndMatch.roundScoresIndMatch) {
        player1String = [NSString stringWithFormat:@"%@Round%d-Score%ld--", player1String, counter + 1, matchScore.scorePlayer1];
        
        
        player2String = [NSString stringWithFormat:@"%@Round%d-Score%ld--", player2String, counter + 1, matchScore.scorePlayer2];
        
        counter ++;
    }
    
    [myScores setObject:player1String forKey:@"player1_score"];
    [myScores setObject:player2String forKey:@"player2_score"];
    
    return myScores;
}


- (IBAction)addAnotherRound:(id)sender
{
    if ([self.individualMatch.scoreIndMatch.roundScoresIndMatch count] >= self.individualMatch.sport.maxRound) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = [NSString stringWithFormat:@"Too many %@", self.individualMatch.sport.setUnitName];
        alert.message = [NSString stringWithFormat:@"Sorry but the maximum number of %@s allowed is %lu", self.individualMatch.sport.setUnitName, self.individualMatch.sport.maxRound];
        [alert addButtonWithTitle:@"OK"];
        [alert show];
        self.pointsPlayer1.text = @"";
        self.pointsPlayer2.text = @"";

    } else if ([self.pointsPlayer1.text length] > 0 && [self.pointsPlayer2.text length] > 0) {
        if ([self.pointsPlayer1.text integerValue] > 200 || [self.pointsPlayer2.text integerValue] > 200) {
            UIAlertView *alert = [[UIAlertView alloc] init];
            alert.title = @"Invalid Points";
            alert.message = [NSString stringWithFormat:@"Sorry, but points for a player should be less than 200"];
            [alert addButtonWithTitle:@"OK"];
            [alert show];
        } else {
        
            [self addRoundToArray];
        
            self.pointsPlayer1.text = @"";
            self.pointsPlayer2.text = @"";
        }
    } else {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Incomplete Form";
        alert.message = [NSString stringWithFormat:@"Please enter the scores for both players before proceeding to the next %@", [self.individualMatch.sport.setUnitName capitalizedString]];
        [alert addButtonWithTitle:@"OK"];
        [alert show];
    }
}

- (void)addRoundToArray
{
    [self.pointsPlayer1 resignFirstResponder];
    [self.pointsPlayer2 resignFirstResponder];
    
    NSInteger round = [self.individualMatch.scoreIndMatch.roundScoresIndMatch count] + 1;
    
    [self.individualMatch.scoreIndMatch addRoundScore:round
                                           forPlayer1:self.individualMatch.player1.userID
                                     withScorePlayer1:[self.pointsPlayer1.text integerValue]
                                           forPlayer2:self.individualMatch.player2.userID
                                     withScorePlayer2:[self.pointsPlayer2.text integerValue]];
    
    if (self.individualMatch.sport.maxRound > 1) {
        self.roundLabel.text = [NSString stringWithFormat:@"%@ %lu" , [self.individualMatch.sport.setUnitName capitalizedString], [self.individualMatch.scoreIndMatch.roundScoresIndMatch count] + 1];
    } else {
        //  self.roundLabel.hidden = YES;
        self.roundLabel.text = @"Game";
    }
    
    [self.scoreCollectionView reloadData];
}


- (void)setPageValues
{
 //   [self.submitScoreButton makeRoundBorderWithColor:[UIColor salmonColor]];
    
    if (self.individualMatch.sport.maxRound > 1) {
        self.addRoundButton.hidden = NO;
        [self.addRoundButton makeRoundedBorderWithRadius:3];
        
    //    [self.addRoundButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //    [self.addRoundButton makeRoundBorderWithColor:[UIColor whiteColor]];
        
        /*UIView *whiteBar = [[UIView alloc] initWithFrame:CGRectMake(self.username1Label_v2.frame.origin.x, self.username1Label_v2.frame.origin.y + 30, 300, 0.5)];
        whiteBar.backgroundColor = [UIColor whiteColor];
        [self.scoreEntryBackground addSubview:whiteBar];*/
        
    } else {
        self.hintLabel.hidden = YES;
        UILabel *messageForOneRoundMatch = [[UILabel alloc] initWithFrame:CGRectMake(30, self.addRoundButton.frame.origin.y, 260, 60)];
        messageForOneRoundMatch.textColor = [UIColor whiteColor];
        messageForOneRoundMatch.font = [UIFont fontWithName:@"HelveticaNeue-MediumItalic" size:13];
        messageForOneRoundMatch.text = @"The score you enter must be confirmed by your opponent";
        messageForOneRoundMatch.numberOfLines = 0;
        [messageForOneRoundMatch setTextAlignment:NSTextAlignmentCenter];
        
        self.addRoundButton.hidden = YES;
        
        self.scoreCollectionView.hidden = YES;
        
        [self.scoreEntryBackground addSubview:messageForOneRoundMatch];
        
        /*
         self.bulbIcon.image = [[UIImage imageNamed:@"Bulb Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
         [self.bulbIcon setTintColor:[UIColor salmonColor]];
         */
        UIImageView *player1IV = [[UIImageView alloc] initWithFrame:CGRectMake(self.username1Label_v2.frame.origin.x + 35, self.username1Label_v2.frame.origin.y + 13, 36, 36)];
        
        
        NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                baseString:self.individualMatch.player1.profileBaseString];
        
        [player1IV setImageWithURL:[NSURL URLWithString:url]
                  placeholderImage:[UIImage imageNamed:@"avatar round"]];
        
        [UIImage makeRoundedImage:player1IV withRadius:18];
        /*
        if ([self.individualMatch.player1.profileBaseString length] == 0) {
            [player1IV setTintColor:[UIColor darkGrayColor]];
        } else {
            [player1IV setTintColor:[UIColor clearColor]];
        }*/
        
        [self.scoreEntryBackground addSubview:player1IV];
        
        
        UIImageView *player2IV = [[UIImageView alloc] initWithFrame:CGRectMake(self.username2Label_v2.frame.origin.x + 35, self.username2Label_v2.frame.origin.y + 15, 36, 36)];
        
        
        NSString *url2 = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                 baseString:self.individualMatch.player2.profileBaseString];
        
        [player2IV setImageWithURL:[NSURL URLWithString:url2]
                  placeholderImage:[UIImage imageNamed:@"avatar round"]];
        
        [UIImage makeRoundedImage:player2IV withRadius:18];
        
        /*
        if ([self.individualMatch.player2.profileBaseString length] == 0) {
            [player2IV setTintColor:[UIColor darkGrayColor]];
        } else {
            [player2IV setTintColor:[UIColor clearColor]];
        }*/
        
        [self.scoreEntryBackground addSubview:player2IV];
        
        
        UILabel *newUsername1 = [[UILabel alloc] initWithFrame:CGRectMake(self.username1Label_v2.frame.origin.x + 100, self.username1Label_v2.frame.origin.y + 20, self.username1Label_v2.frame.size.width + 50, self.username1Label_v2.frame.size.height)];
        newUsername1.text = self.individualMatch.player1.userName;
        newUsername1.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:13];
        newUsername1.textColor = [UIColor colorWithRed:119/255 green:119/255 blue:119/255 alpha:1.0];
        
        UILabel *newUsername2 = [[UILabel alloc] initWithFrame:CGRectMake(self.username2Label_v2.frame.origin.x + 100, self.username2Label_v2.frame.origin.y + 22, self.username2Label_v2.frame.size.width + 50, self.username2Label_v2.frame.size.height)];
        newUsername2.text = self.individualMatch.player2.userName;
        newUsername2.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:13];
        newUsername2.textColor = [UIColor colorWithRed:119/255 green:119/255 blue:119/255 alpha:1.0];
        
        [self.scoreEntryBackground addSubview:newUsername1];
        [self.scoreEntryBackground addSubview:newUsername2];
        
        self.username1Label_v2.hidden = YES;
        self.username2Label_v2.hidden = YES;

        /*UIView *whiteBar = [[UIView alloc] initWithFrame:CGRectMake(self.username1Label_v2.frame.origin.x + 50, self.username1Label_v2.frame.origin.y + 30, 250, 0.5)];
        whiteBar.backgroundColor = [UIColor whiteColor];
        [self.scoreEntryBackground addSubview:whiteBar];*/
        
 /*
        self.submitScoreButton.hidden = YES;
        UIButton *wideSubmitButton = [[UIButton alloc] initWithFrame:CGRectMake(20, 320, 280, 51)];
        [wideSubmitButton setBackgroundColor:[UIColor salmonColor]];
        [wideSubmitButton makeRoundBorderWithColor:[UIColor salmonColor]];
        [wideSubmitButton setTitle:@"Record Score" forState:UIControlStateNormal];
        [wideSubmitButton addTarget:self action:@selector(submitScore:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:wideSubmitButton];
  */
    }
    
    NSString *monthText = @"";
    NSString *dayText = @"";
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMM"];
    monthText = [dateFormatter stringFromDate:self.individualMatch.dateTime];
    
    NSDateFormatter *dateFormatterDay = [[NSDateFormatter alloc] init];
    [dateFormatterDay setDateFormat:@"d"];
    dayText = [dateFormatterDay stringFromDate:self.individualMatch.dateTime];
    
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    [timeFormatter setDateFormat:@"h:mm a"];
    self.dateTimeLabel.text = [NSString stringWithFormat:@"%@ at %@", monthText, [timeFormatter stringFromDate: self.individualMatch.dateTime]];
    
   // self.locationLabel.text = self.individualMatch.venue.venueName;
    self.monthAbbrevLabel.text = monthText;
    self.dayLabel.text = dayText;
    
    self.timeAndLocationLabel.text = [NSString stringWithFormat:@"%@ at %@", [timeFormatter stringFromDate:self.individualMatch.dateTime], self.individualMatch.venue.venueName];
    
    self.usernamePlayer1.text = self.individualMatch.player1.userName;
    self.usernamePlayer2.text = self.individualMatch.player2.userName;
    
    self.username1Label_v2.text = self.individualMatch.player1.userName;
    self.username2Label_v2.text = self.individualMatch.player2.userName;
    
    self.vsLabel.text = [NSString stringWithFormat:@"%@ vs %@", self.individualMatch.player1.userName, self.individualMatch.player2.userName];
    self.sportIcon.image = [self.individualMatch.sport getSportIcon];
    
    
    
    
    
    
    
    
    
    
   // self.pointsPlayer1.backgroundColor = [UIColor colorWithRed:176.0/255 green:23.0/255.0 blue:31.0/255.0 alpha:0.1];
   // self.pointsPlayer2.backgroundColor = [UIColor colorWithRed:176.0/255 green:23.0/255.0 blue:31.0/255.0 alpha:0.1];
    
    self.challengeVSLabel.text = [NSString stringWithFormat:@"%@ vs %@", self.individualMatch.player1.userName, self.individualMatch.player2.userName];
    
  //  [self.subBackground1 makeCircleWithColor:[UIColor midGray2] andRadius:5];
    
 /*   self.bulbIcon.image = [[UIImage imageNamed:@"Bulb Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.bulbIcon setTintColor:[UIColor salmonColor]];
    self.instructionText.textColor = [UIColor salmonColor];
    [self.instructionSectionView.layer setBorderColor:[UIColor lightLightGray].CGColor];
*/
    self.matchNameLabel.text = self.individualMatch.matchName;
    self.sportLabel.text = [self.individualMatch.sport.sportName capitalizedString];
    
    
    if (self.individualMatch.sport.maxRound > 1) {
        self.roundLabel.text = [NSString stringWithFormat:@"%@ %lu" , [self.individualMatch.sport.setUnitName capitalizedString], [self.individualMatch.scoreIndMatch.roundScoresIndMatch count] + 1];
    } else {
      //  self.roundLabel.hidden = YES;
        self.roundLabel.text = [NSString stringWithFormat:@"%@ Game", [self.individualMatch.sport.sportName capitalizedString]];
    }
    self.pointLabel.text = [NSString stringWithFormat:@"%@s", [self.individualMatch.sport.gameUnitName capitalizedString]];
    
    [self.scoreCollectionView reloadData];
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"My Cell";
    
    RecordIndMatchScoreCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];

    IndividualMatchRoundScore *matchScore = [self.individualMatch.scoreIndMatch.roundScoresIndMatch objectAtIndex:indexPath.section];
    
 //   if (indexPath.row == 0) { // base section
 //       cell.scoreLabel.text = [NSString stringWithFormat:@"%d", indexPath.section + 1];
 //       cell.scoreLabel.textColor = [UIColor lightGrayColor];
     if (indexPath.row == 0) { // first section
    //    cell.scoreLabel.textColor = [UIColor darkGrayColor];
        cell.scoreLabel.text = [NSString stringWithFormat:@"%ld", matchScore.scorePlayer1];
        self.username1Label_v2.text = self.individualMatch.player1.userName; // I know this is not efficient...doing this for time's sake for now.
    } else if (indexPath.row == 1) { // second section
   //     cell.scoreLabel.textColor = [UIColor darkGrayColor];
        cell.scoreLabel.text = [NSString stringWithFormat:@"%ld", matchScore.scorePlayer2];
        self.username2Label_v2.text = self.individualMatch.player2.userName; // I know this is not efficient...doing this for time's sake for now.
    }
    
    return cell;
}
- (IBAction)touchFirstCell:(id)sender
{
 //   [self.pageScrollView setContentOffset:CGPointMake(0, 135) animated:YES];
    [self.pointsPlayer1 becomeFirstResponder];
}
- (IBAction)touchSecondCell:(id)sender
{
//    [self.pageScrollView setContentOffset:CGPointMake(0, 135) animated:YES];
    [self.pointsPlayer2 becomeFirstResponder];
}
- (IBAction)tapOffFields:(id)sender
{
    [self.pointsPlayer1 resignFirstResponder];
    [self.pointsPlayer2 resignFirstResponder];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Record Score Success"]) {
        RecordIndMatchScoreSuccess *controller = segue.destinationViewController;
        controller.individualMatch = self.individualMatch;
    }
}

@end
