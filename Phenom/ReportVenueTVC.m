//
//  ReportVenueTVC.m
//  Phenom
//
//  Created by James Chung on 7/5/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "ReportVenueTVC.h"
#import "IOSRequest.h"
#import "UserPreferences.h"

@interface ReportVenueTVC ()
@property (nonatomic, strong) NSIndexPath *selectedRatingIndexPath;
@property (nonatomic, strong) UserPreferences *preferences;
@property (weak, nonatomic) IBOutlet UITextView *commentTextView;
@property (nonatomic, strong) UIBarButtonItem *doneButton;
@end

@implementation ReportVenueTVC

#define  RATING_SECTION 0

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //[self setDoneButton];
}

/*- (void)setDoneButton
{
    UIButton *btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 50, 21);
    btn.layer.borderColor = [UIColor whiteColor].CGColor;
    btn.layer.borderWidth = 1.0;
    
    [btn addTarget:self action:@selector(doneAction:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundColor:[UIColor clearColor]];
    [btn setTitle:@"DONE" forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-MediumItalic" size:12.0]];
    
    // Initialize UIBarbuttonitem...
    self.doneButton = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = self.doneButton;
}*/

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == RATING_SECTION) {
        UITableViewCell *oldCell = [self.tableView cellForRowAtIndexPath:self.selectedRatingIndexPath];
        oldCell.accessoryType = UITableViewCellAccessoryNone;
        
        UITableViewCell *newCell = [self.tableView cellForRowAtIndexPath:indexPath];
        newCell.accessoryType = UITableViewCellAccessoryCheckmark;
        
        self.selectedRatingIndexPath = [NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section];
        [self.commentTextView resignFirstResponder];
        
    }
}


- (IBAction)doneAction:(id)sender
{
    if ([self.commentTextView.text length] > 250) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Comment Posting Error";
        alert.message = @"Sorry but your comment cannot exceed 200 characters in length.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
        
    } else {
        
    
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:self.selectedRatingIndexPath];
 //   if (cell.accessoryType == UITableViewCellAccessoryCheckmark) {
    NSString *flagString = cell.textLabel.text;
    
    if (self.selectedRatingIndexPath.section == 0) {
        switch (self.selectedRatingIndexPath.row) {
            case 0:
                flagString = @"Wrong Name";
                break;
            case 1:
                flagString = @"Inaccurate Address";
                break;
            case 2:
                flagString = @"No Facilities At This Address";
                break;
            case 3:
                flagString = @"Inaccurate Description";
                break;
            case 4:
                flagString = @"Abusive Content";
                break;
            default:
                flagString = @"";
                
        }
    }
            
    
 //   }
        
        HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        HUD.labelText = @"Saving";
        self.doneButton.enabled = NO;

    
    [IOSRequest submitVenueFlag:flagString
                    withComment:self.commentTextView.text
                         byUser:[self.preferences getUserID]
                       forVenue:self.venueID
                   onCompletion:^(NSDictionary *results) {
                   
                        dispatch_async(dispatch_get_main_queue(), ^ {
                            
                            [MBProgressHUD hideHUDForView:self.view animated:YES];
                            self.doneButton.enabled = YES;
                            
                            if (![results[@"outcome"] isEqualToString:@"failure"]) {
                                [self.navigationController popViewControllerAnimated:YES];
                            } else {
                                UIAlertView *alert = [[UIAlertView alloc] init];
                                alert.title = @"Submission Error";
                                alert.message = @"There was a problem completing your request.  Please retry.";
                                [alert addButtonWithTitle:@"OK"];
                                [alert show];
                            }
                            
                            
                        });
                   }];
        double delayInSeconds = 10.0;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            //code to be executed on the main queue after delay
            self.doneButton.enabled = YES;
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        });
                         
                         
    }
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
