//
//  InviteMoreTeamMembersTVC.m
//  Vaiden
//
//  Created by James Chung on 4/9/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "InviteMoreTeamMembersTVC.h"
#import "UserPreferences.h"
#import "IOSRequest.h"
#import "S3Tools.h"
#import "AFNetworking.h"
#import "InviteMoreTeamMembersCell.h"
#import "TeamPlayer.h"
#import "UIImage+ProportionalFill.h"
#import "UIColor+VaidenColors.h"
#import "UIView+Manipulate.h"

@interface InviteMoreTeamMembersTVC ()
@property (nonatomic, strong) UserPreferences *preferences;
@property (nonatomic, strong) NSMutableArray *myContacts;
@property (nonatomic, strong) UIView *tempView;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
@property (nonatomic, strong) UIBarButtonItem *doneButton;
@property (nonatomic, strong) NSMutableArray *invitedList;
@property (nonatomic, strong) UIView *overlay;

@end

@implementation InviteMoreTeamMembersTVC

- (NSMutableArray *)invitedList
{
    if (!_invitedList) _invitedList = [[NSMutableArray alloc] init];
    return _invitedList;
}

- (NSMutableArray *)myContacts
{
    if (!_myContacts) _myContacts = [[NSMutableArray alloc] init];
    return _myContacts;
}

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];

    [self setDoneButton];

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self loadContacts];

}

- (void)showTempSpinner
{
    self.tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    self.tempView.backgroundColor = [UIColor lightGrayColor];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] init];
    [self.activityIndicator setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width/2.0, [UIScreen mainScreen].bounds.size.height/2.0 - 50)];
    [self.activityIndicator startAnimating];
    [self.tempView addSubview:self.activityIndicator];
    
    [self.view addSubview:self.tempView];
    
}

- (void)setDoneButton
{
    UIButton *btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 50, 21);
    btn.layer.borderColor = [UIColor whiteColor].CGColor;
    btn.layer.borderWidth = 1.0;
    
    [btn addTarget:self action:@selector(done:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundColor:[UIColor clearColor]];
    [btn setTitle:@"DONE" forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-MediumItalic" size:12.0]];
    
    // Initialize UIBarbuttonitem...
    self.doneButton = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = self.doneButton;
}

- (void)loadContacts
{
    [self.myContacts removeAllObjects];
    [self showTempSpinner];
    self.tableView.scrollEnabled = NO;
    [IOSRequest getUninvitedContacts:[self.preferences getUserID]
                             forTeam:self.teamID
                        onCompletion:^(NSMutableArray *results) {
                            
                            for (id object in results) {
                                NSString *memberStatus = @"";
                                
                                if ([object[@"status"] length] == 0)
                                    memberStatus = @"UNINVITED"; // otherwise it will be the value from the server
                                else
                                    memberStatus = object[@"status"];
                                    
                                TeamPlayer *tp = [[TeamPlayer alloc] initWithTeamPlayerDetails:[object[@"user_id"] integerValue]
                                                                                   andUserName:object[@"username"]
                                                                           andProfilePicString:object[@"profile_pic_string"]
                                                                               andMemberStatus:memberStatus];
                                [self.myContacts addObject:tp];
                            }
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                [self.activityIndicator stopAnimating];
                                self.tempView.hidden = YES;
                                
                                if ([self.myContacts count] == 0) {
                                    [self displayErrorOverlay];
                                } else {
                                    self.overlay.hidden = YES;
                                    self.tableView.scrollEnabled = YES;
                                    [self.tableView reloadData];
                                }
                                
                            });
                        }];
    
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   return [self.myContacts count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60.0;
}

- (IBAction)inviteAction:(UIButton *)sender
{
    if ([sender.titleLabel.text isEqualToString:@"Invite"]) {
        [sender setTitle:@"Invited" forState:UIControlStateNormal];
        TeamPlayer *p = [self.myContacts objectAtIndex:sender.tag];
        p.memberStatus = @"NEWLYINVITED";
        [self.invitedList addObject:p];
        [sender setBackgroundColor:[UIColor newBlueDark]];
        
    } else if ([sender.titleLabel.text isEqualToString:@"Invited"]) {
        [sender setTitle:@"Invite" forState:UIControlStateNormal];
        TeamPlayer *p = [self.myContacts objectAtIndex:sender.tag];
        p.memberStatus = @"UNINVITED";
        [sender setBackgroundColor:[UIColor newBlueLight]];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userID != %d", p.userID];
        self.invitedList = [NSMutableArray arrayWithArray:[self.invitedList filteredArrayUsingPredicate:predicate]];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString static *CellIdentifier = @"Player Cell";
    
    InviteMoreTeamMembersCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    TeamPlayer *tp = [self.myContacts objectAtIndex:indexPath.row];
    
    NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                            baseString:tp.profileBaseString];
    
    [cell.profilePic setImageWithURL:[NSURL URLWithString:url]
                    placeholderImage:[UIImage imageNamed:@"avatar square"]];
    
    [UIImage makeRoundedImage:cell.profilePic withRadius:20];
    
    cell.usernameLabel.text = tp.userName;
    
    if ([tp.memberStatus isEqualToString:@"CONFIRMED"]) {
        [cell.inviteButton setTitle:@"UnInvite" forState:UIControlStateNormal];
    } else if ([tp.memberStatus isEqualToString:@"UNCONFIRMED"]) {
        [cell.inviteButton setTitle:@"UnInvite" forState:UIControlStateNormal];
    } else if ([tp.memberStatus isEqualToString:@"UNINVITED"]) {
        [cell.inviteButton setTitle:@"Invite" forState:UIControlStateNormal];
    }
    cell.inviteButton.tag = indexPath.row;
    [cell.inviteButton makeRoundedBorderWithRadius:3];
    
    return cell;
}

- (void)done:(UIButton *)sender
{
    // only send newly invited participants
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"memberStatus == 'NEWLYINVITED'"];
    NSMutableArray *tempArray = [NSMutableArray arrayWithArray:[self.invitedList filteredArrayUsingPredicate:predicate]];
    
    
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.labelText = @"Updating";
    self.doneButton.enabled = NO;
    
    [IOSRequest setTeamMembersForTeam:self.teamID
                          withMembers:tempArray
                           andCaptain:[self.preferences getUserID]
                         onCompletion:^(NSDictionary *results) {
                             
                             dispatch_async(dispatch_get_main_queue(), ^{
                                 self.doneButton.enabled = YES;
                                [MBProgressHUD hideHUDForView:self.view animated:YES];
                                 [self.navigationController popViewControllerAnimated:YES];
                                 
                             });
                         }];
    
    double delayInSeconds = 10.0;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
           //code to be executed on the main queue after delay
           self.doneButton.enabled = YES;
          [MBProgressHUD hideHUDForView:self.view animated:YES];// just in case
      });
}

- (void)displayErrorOverlay
{
    if (self.overlay == nil) {
        self.overlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, [UIScreen mainScreen].bounds.size.height)];
        self.overlay.backgroundColor = [UIColor lightLightGray];
    
        UILabel *firstLine = [[UILabel alloc] initWithFrame:CGRectMake(30, ([UIScreen mainScreen].bounds.size.height / 2) - 115, 260, 30)];
        firstLine.textColor = [UIColor darkGrayColor];
        firstLine.font = [UIFont systemFontOfSize:23];
        firstLine.text = @"No Additional Contacts";
    
        UILabel *secondLine = [[UILabel alloc] initWithFrame:CGRectMake(30, ([UIScreen mainScreen].bounds.size.height / 2) - 90, 260, 100)];
        secondLine.textColor = [UIColor lightGrayColor];
        secondLine.font = [UIFont systemFontOfSize:17];
        secondLine.text = @"You do not have any other contacts to invite to this team.  Follow more users to add additional contacts.";
        secondLine.numberOfLines = 0;
    
        UIButton *contactsButton = [[UIButton alloc] initWithFrame:CGRectMake(27, ([UIScreen mainScreen].bounds.size.height / 2) + 20  , 210, 40)];
        [contactsButton setTitle:@"Add Contacts" forState:UIControlStateNormal];
        [contactsButton setTitleColor:[UIColor peacock] forState:UIControlStateNormal];
        contactsButton.titleLabel.font = [UIFont systemFontOfSize:16.0];
        [contactsButton addTarget:self action:@selector(newContacts) forControlEvents:UIControlEventTouchUpInside];
    
    
        [contactsButton makeRoundedBorderWithRadius:3];
        [contactsButton.layer setBorderWidth:1.0];
        [contactsButton.layer setBorderColor:[[UIColor peacock] CGColor]];



    
        [self.overlay addSubview:firstLine];
        [self.overlay addSubview:secondLine];
        [self.overlay addSubview:contactsButton];
    
        [self.tableView addSubview:self.overlay];
    } else {
        self.overlay.hidden = NO;
    }
}

- (void)newContacts
{
    [self performSegueWithIdentifier:@"Add Contacts Segue" sender:self];
}
@end
