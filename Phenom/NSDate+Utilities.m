//
//  NSDate+Utilities.m
//  Vaiden
//
//  Created by James Chung on 10/11/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "NSDate+Utilities.h"

@implementation NSDate (Utilities)

// calculates time diff from current date and refDate

+ (NSString *)timeDiffCalc:(NSDate *)refDate
{
    NSDate *currentDate = [NSDate date];
    NSTimeInterval timeDifference = [currentDate timeIntervalSinceDate:refDate];
    int minutes = timeDifference / 60;
    int hours = minutes / 60;
    int days = minutes / 1440;
    NSString *timeDiffString = nil;
    
    if (minutes > 1440) {
        if (days == 1)
            timeDiffString = [NSString stringWithFormat:@"%d day ago", days];
        else
            timeDiffString = [NSString stringWithFormat:@"%d days ago", days];
    } else if (minutes > 60) {
        if (hours == 1)
            timeDiffString = [NSString stringWithFormat:@"%d hour ago", hours];
        else
            timeDiffString = [NSString stringWithFormat:@"%d hours ago", hours];
    } else {
        if (minutes == 1)
            timeDiffString = [NSString stringWithFormat:@"%d minute ago", minutes];
        else if (minutes < 0)
            timeDiffString = @"0 minutes ago";
        else
            timeDiffString = [NSString stringWithFormat:@"%d minutes ago", minutes];
    }
    return timeDiffString;

}

- (BOOL)isDateOverWeekAgo
{
    NSDate *currentDate = [NSDate date];
    NSTimeInterval timeDifference = [currentDate timeIntervalSinceDate:self];
    int minutes = timeDifference / 60;
    int days = minutes / 1440;
    
    if (minutes > 1440) {
        if (days > 7)
            return YES;
    }
    
    return NO;
}

-(NSDate *) toLocalTime
{
    NSTimeZone *tz = [NSTimeZone defaultTimeZone];
    NSInteger seconds = [tz secondsFromGMTForDate: self];
    return [NSDate dateWithTimeInterval: seconds sinceDate: self];
}


-(NSDate *) toGlobalTime
{
    NSTimeZone *tz = [NSTimeZone defaultTimeZone];
    NSInteger seconds = -[tz secondsFromGMTForDate: self];
    return [NSDate dateWithTimeInterval: seconds sinceDate: self];
}

// Returns a string if date is "Today" or "Tomorrow"...returns an empty string if date is neither of these options

- (NSString *)getTodayTomorrowDateStatus
{
    // compare todays date

    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *todayComponents = [cal components:(NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit) fromDate:[NSDate date]];
    NSDate *today = [cal dateFromComponents:todayComponents];
    
    NSDateComponents *components = [cal components:(NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit) fromDate:self];
    NSDate *matchDate = [cal dateFromComponents:components];
    
    if([today isEqualToDate:matchDate]) {
        return @"Today";
    }
    
    // compare tomorrows date
    NSDate *now = [NSDate date];
    int daysToAdd = 1;
    NSDate *tomorrowsDate = [now dateByAddingTimeInterval:60*60*24*daysToAdd];
    
    NSDateComponents *tomorrowComponents = [cal components:(NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit) fromDate:tomorrowsDate];
    NSDate *tomorrow = [cal dateFromComponents:tomorrowComponents];
    
    if ([tomorrow isEqualToDate:matchDate]) {
        return @"Tomorrow";
    }
    
    return @"";
}

+ (NSInteger)minDifference:(NSDate *)dateA andSecondDate:(NSDate *)dateB
{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit
                                               fromDate:dateA
                                                 toDate:dateB
                                                options:0];
    NSLog (@"%ld", components.minute);
    return components.minute;
}

// returns in format of month, day, year and time
- (NSString *)getDateFullDateString
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"M/dd/yyyy hh:mm a"];
    return [dateFormat stringFromDate:self];
    
}

@end
