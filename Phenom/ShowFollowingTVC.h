//
//  ShowFollowingTVC.h
//  Phenom
//
//  Created by James Chung on 7/9/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomBaseTVC.h"

@interface ShowFollowingTVC : CustomBaseTVC

@property (nonatomic) NSUInteger baseUserID;

@end
