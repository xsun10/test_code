//
//  Sport.m
//  Phenom
//
//  Created by James Chung on 5/1/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "Sport.h"

@implementation Sport




// without sport ID
- (Sport *) initWithSportDetails:(NSString *)sportName
                         andType:(NSString *)sportType
                     {
    self = [super init];
    
    if (self) {
        _sportName = sportName;
        _sportType = sportType;
    }
    
    return self;
}

- (Sport *) initWithSportDetails:(NSInteger)sportID
                         andName:(NSString *)sportName
                         andType:(NSString *)sportType
{
    self = [super init];
    
    if (self) {
        _sportID    = sportID;
        _sportName = sportName;
        _sportType = sportType;
    }
    
    return self;
}


- (Sport *) initWithSportDetails:(NSInteger)sportID
                         andName:(NSString *)sportName
                         andType:(NSString *)sportType
                 largerScoreWins:(BOOL)largerScoreWins
{
    self = [super init];
    
    if (self) {
        _sportID    = sportID;
        _sportName = sportName;
        _sportType = sportType;
        _largerScoreWins = largerScoreWins;
    }
    
    return self;
}

- (Sport *)initWithSportDetails:(NSInteger)sportID
                        andName:(NSString *)sportName
                        andType:(NSString *)sportType
                largerScoreWins:(BOOL)largerScoreWins
                    setUnitName:(NSString *)setUnitName
                   gameUnitName:(NSString *)gameUnitName
{
    self = [self initWithSportDetails:sportID
                              andName:sportName
                              andType:sportType
                      largerScoreWins:largerScoreWins];
    
    if (self) {
        _setUnitName = setUnitName;
        _gameUnitName = gameUnitName;
    }
    
    return self;
}

- (UIImage *)getSportIcon
{
    UIImage *iconImage = nil;
    
    if ([self.sportName isEqualToString:@"basketball"]) {
        iconImage = [UIImage imageNamed:@"BBall Man Dark"];
    } else if ([self.sportName isEqualToString:@"tennis"]) {
        iconImage = [UIImage imageNamed:@"Tennis Icon"];
    } else if ([self.sportName isEqualToString:@"golf"]) {
        iconImage = [UIImage imageNamed:@"Golf Icon"];
    } else if ([self.sportName isEqualToString:@"ping-pong"]) {
        iconImage = [UIImage imageNamed:@"Ping Pong Icon"];
    } else if ([self.sportName isEqualToString:@"baseball"]) {
        iconImage = [UIImage imageNamed:@"Baseball Icon"];
    } else if ([self.sportName isEqualToString:@"football"]) {
        iconImage = [UIImage imageNamed:@"Football Icon"];
    } else if ([self.sportName isEqualToString:@"volleyball"]) {
        iconImage = [UIImage imageNamed:@"Volleyball Icon"];
    } else if ([self.sportName isEqualToString:@"soccer"]) {
        iconImage = [UIImage imageNamed:@"Soccer Icon"];
    } else if ([self.sportName isEqualToString:@"swimming"]) {
        iconImage = [UIImage imageNamed:@"Swimming Icon"];
    } else if ([self.sportName isEqualToString:@"badminton"]) {
        iconImage = [UIImage imageNamed:@"Badminton Icon"];
    } else if ([self.sportName isEqualToString:@"racquetball"]) {
        iconImage = [UIImage imageNamed:@"Tennis Icon"];
    } else if ([self.sportName isEqualToString:@"hockey"]) {
        iconImage = [UIImage imageNamed:@"Hockey Icon"];
    } else if ([self.sportName isEqualToString:@"running"]) {
        iconImage = [UIImage imageNamed:@"Running Icon"];
    } else if ([self.sportName isEqualToString:@"fitness"]) {
        iconImage = [UIImage imageNamed:@"Fitness Icon"];
    }
    
    return iconImage;
}

- (UIImage *)getSportIcon2
{
    UIImage *iconImage = nil;
    
    if ([self.sportName isEqualToString:@"basketball"]) {
        iconImage = [UIImage imageNamed:@"Basketball Icon 2"];
    } else if ([self.sportName isEqualToString:@"tennis"]) {
        iconImage = [UIImage imageNamed:@"Tennis Icon 2"];
    } else if ([self.sportName isEqualToString:@"golf"]) {
        iconImage = [UIImage imageNamed:@"Golf Icon 2"];
    } else if ([self.sportName isEqualToString:@"ping-pong"]) {
        iconImage = [UIImage imageNamed:@"Ping Pong Icon 2"];
    } else if ([self.sportName isEqualToString:@"baseball"]) {
        iconImage = [UIImage imageNamed:@"Baseball Icon 2"];
    } else if ([self.sportName isEqualToString:@"football"]) {
        iconImage = [UIImage imageNamed:@"Football Icon 2"];
    } else if ([self.sportName isEqualToString:@"volleyball"]) {
        iconImage = [UIImage imageNamed:@"Volleyball Icon 2"];
    } else if ([self.sportName isEqualToString:@"soccer"]) {
        iconImage = [UIImage imageNamed:@"Soccer Icon 2"];
    } else if ([self.sportName isEqualToString:@"swimming"]) {
        iconImage = [UIImage imageNamed:@"Swimming Icon 2"];
    } else if ([self.sportName isEqualToString:@"badminton"]) {
        iconImage = [UIImage imageNamed:@"Badminton Icon 2"];
    } else if ([self.sportName isEqualToString:@"racquetball"]) {
        iconImage = [UIImage imageNamed:@"Racquetball Icon 2"];
    } else if ([self.sportName isEqualToString:@"hockey"]) {
        iconImage = [UIImage imageNamed:@"Hockey Icon 2"];
    } else if ([self.sportName isEqualToString:@"running"]) {
        iconImage = [UIImage imageNamed:@"Running Icon 2"];
    } else if ([self.sportName isEqualToString:@"fitness"]) {
        iconImage = [UIImage imageNamed:@"Fitness Icon 2"];
    }
    
    return iconImage;
}


- (UIImage *)backgroundImage
{
    // return an individual sport image vs team image
  //  if ([self.sportType isEqualToString:@"individual"]) {
        if ([self.sportName isEqualToString:@"tennis"]) {
            return [UIImage imageNamed:@"Match BBall Background"];
        } else {
            return  [UIImage imageNamed:@"Match BBall Background"];
        }
  //  } else if ([self.sportType isEqualToString:@"pickup"]) {
   //     if ([self.sportName isEqualToString:@"tennis"]) {
   //         return [UIImage imageNamed:@"Tennis Pickup Background"];
   //     } else {
    //        return  [UIImage imageNamed:@"Blue Back"];
    //    }
   // }
    
    return nil;
}


@end
