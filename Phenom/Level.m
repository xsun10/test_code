//
//  Level.m
//  Phenom
//
//  Created by James Chung on 5/20/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "Level.h"

@implementation Level

- (Level *) initWithLevelDetails:(NSInteger)levelID
                        andLevel:(NSString *)level
                        andValue:(float)value
{
    self = [super init];
    
    if (self) {
        _levelID = levelID;
        _level = level;
        _value = value;
    }
    
    return self;
}

@end
