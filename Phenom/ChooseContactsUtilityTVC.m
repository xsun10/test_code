//
//  CheckInContactsTVC.m
//  Vaiden
//
//  Created by James Chung on 4/22/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "ChooseContactsUtilityTVC.h"
#import "IOSRequest.h"
#import "UserPreferences.h"
#import "CheckInContactsTVCell.h"
#import "S3Tools.h"
#import "AFNetworking.h"
#import "UIImage+ProportionalFill.h"
#import "UIView+Manipulate.h"
#import "UIColor+VaidenColors.h"

@interface ChooseContactsUtilityTVC ()

@property (nonatomic, strong) UserPreferences *preferences;
@property (nonatomic, strong) NSMutableArray *contactsArray;
@property (nonatomic, strong) NSMutableArray *contactsToNotifyArray;
@property (nonatomic, strong) UIBarButtonItem *doneBarButton;
@property (nonatomic, strong) UIView *tempView;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
@property (nonatomic, strong) UIView *overlay;
@property (nonatomic) NSInteger numSelected;
@property (weak, nonatomic) IBOutlet UILabel *headlineMessage1;
@property (weak, nonatomic) IBOutlet UILabel *headlineMessage2;

@end

@implementation ChooseContactsUtilityTVC

- (NSMutableArray *)contactsToNotifyArray
{
    if (!_contactsToNotifyArray) _contactsToNotifyArray = [[NSMutableArray alloc] init];
    return _contactsToNotifyArray;
}

- (NSMutableArray *)contactsArray
{
    if (!_contactsArray) _contactsArray = [[NSMutableArray alloc] init];
    return _contactsArray;
}

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    if (self.pageIsForLockerPeopleTag) {
        self.headlineMessage1.text = @"Choose contacts to tag in the photo";
        self.headlineMessage2.text = @"They will be notified and the photo will be added to their profile";
    } else {
        self.headlineMessage1.text = @"Choose up to 10 people to notify";
        self.headlineMessage2.text = @"If you do not see one of your contacts here, please tell them to turn on notifications";
    }
    [self.navigationItem setBackBarButtonItem: backButton];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];

//    self.numSelected = 0;
    
    [self showTempSpinner];
    //[self setDoneButton];
    [self loadAllContacts];

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if ([self.preferences getChallengePlayersListRefreshState])
        [self loadAllContacts];
}

- (void)showTempSpinner
{
    self.tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    self.tempView.backgroundColor = [UIColor lightGrayColor];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] init];
    [self.activityIndicator setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width/2.0, [UIScreen mainScreen].bounds.size.height/2.0 - 50)];
    [self.activityIndicator startAnimating];
    [self.tempView addSubview:self.activityIndicator];
    
    [self.view addSubview:self.tempView];
    
}


/*- (void)setDoneButton
{
    UIButton *btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 50, 21);
    btn.layer.borderColor = [UIColor whiteColor].CGColor;
    btn.layer.borderWidth = 1.0;
    
    [btn addTarget:self action:@selector(doneAction:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundColor:[UIColor clearColor]];
    [btn setTitle:@"DONE" forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-MediumItalic" size:12.0]];
    
    // Initialize UIBarbuttonitem...
    self.doneBarButton = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = self.doneBarButton;
}*/

- (void)loadAllContacts
{
    [IOSRequest fetchAllContacts:[self.preferences getUserID]
                    onCompletion:^(NSMutableArray *results) {
                        [self.contactsArray removeAllObjects];
                        for (id object in results) {
                            
                            Player *p = [[Player alloc] initWithPlayerDetails:[object[@"user_id"] integerValue]
                                                                  andUserName:object[@"username"]
                                                          andProfilePicString:object[@"profile_pic_string"]];
                            [p setLocation:object[@"location"]];
                            
                            [self.contactsArray addObject:p];
                            
                        }
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            if ([self.contactsArray count] == 0) {
                                [self displayErrorOverlay];
                            } else {
                                self.overlay.hidden = YES;
                            }
                            
                            [self.activityIndicator stopAnimating];
                            self.tempView.hidden = YES;
                            [self.tableView reloadData];
                            
                        });

                    }];
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.contactsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CheckInContactsTVCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Player Cell" forIndexPath:indexPath];
    
    Player *p = [self.contactsArray objectAtIndex:indexPath.row];
    
    cell.usernameLabel.text = p.userName;
    cell.locationLabel.text = p.location;
    
    if (p.isSelected) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                            baseString:p.profileBaseString];
    
    [cell.profilePic setImageWithURL:[NSURL URLWithString:url]
                    placeholderImage:[UIImage imageNamed:@"avatar square"]];
    
    [UIImage makeRoundedImage:cell.profilePic withRadius:20];
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    Player *p = [self.contactsArray objectAtIndex:indexPath.row];
    
    if (p.isSelected) {
        p.isSelected = NO;
        cell.accessoryType = UITableViewCellAccessoryNone;

    } else {
        p.isSelected = YES;
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (IBAction)doneAction:(id)sender
{
    [self.contactsToNotifyArray removeAllObjects];
    
    for (Player *p in self.contactsArray) {
        if (p.isSelected) {
            [self.contactsToNotifyArray addObject:p];
        }
        
    }
    
    if ([self.contactsToNotifyArray count] > self.maxContactsSelected) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Too many contacts chosen";
        alert.message = [NSString stringWithFormat:@"Please choose up to %ld contacts", self.maxContactsSelected];
        [alert addButtonWithTitle:@"OK"];
        [alert show];
    } else {
    
        [self.delegate setWithAllContactsController:self withContactsArray:self.contactsToNotifyArray];
        [self.navigationController popViewControllerAnimated:YES];
    }
}


- (void)displayErrorOverlay
{
    if (self.overlay == nil) {
        self.overlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, [UIScreen mainScreen].bounds.size.height)];
        self.overlay.backgroundColor = [UIColor lightLightGray];
        
        UIImage *img = [UIImage imageNamed:@"no_contact"];
        UIImageView *icon = [[UIImageView alloc] initWithImage:img];
        [icon setFrame:CGRectMake(140, 70, 50, 50)];
        [icon setContentMode:UIViewContentModeCenter];
        
        UILabel *firstLine = [[UILabel alloc] initWithFrame:CGRectMake(30, 150, 260, 21)];
        firstLine.textColor = [UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:1.0];;
        firstLine.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:15];
        firstLine.text = @"No Contacts Yet";
        [firstLine setTextAlignment:NSTextAlignmentCenter];
        
        UILabel *secondLine = [[UILabel alloc] initWithFrame:CGRectMake(25, 170, 280, 63)];
        secondLine.textColor = [UIColor colorWithRed:170/255.0 green:170/255.0 blue:170/255.0 alpha:1.0];
        secondLine.font = [UIFont fontWithName:@"HelveticaNeue" size:13];
        secondLine.text = self.errorOverlayLongMessage;
        secondLine.numberOfLines = 0;
        [secondLine setTextAlignment:NSTextAlignmentCenter];
        
        UIButton *createButton = [[UIButton alloc] initWithFrame:CGRectMake(55, 235, 210, 40)];
        [createButton setTitle:@"Add Contacts" forState:UIControlStateNormal];
        [createButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        createButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:15];
        [createButton addTarget:self action:@selector(addContacts:) forControlEvents:UIControlEventTouchUpInside];
        [createButton setBackgroundColor:[UIColor colorWithRed:170/255.0 green:170/255.0 blue:170/255.0 alpha:1.0]];
        [createButton makeRoundedBorderWithRadius:3];
        
        [self.overlay addSubview:icon];
        [self.overlay addSubview:firstLine];
        [self.overlay addSubview:secondLine];
        [self.overlay addSubview:createButton];
        
        [self.tableView addSubview:self.overlay];
    } else {
        self.overlay.hidden = NO;
    }
}

- (void)addContacts:(UIButton *)sender
{
    [self performSegueWithIdentifier:@"Add Contacts Segue" sender:self];
}
@end
