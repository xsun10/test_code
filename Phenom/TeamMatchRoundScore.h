//
//  TeamMachRoundScore.h
//  Vaiden
//
//  Created by James Chung on 4/16/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "RoundScore.h"

@interface TeamMatchRoundScore : RoundScore

@property (nonatomic) NSInteger scoreTeam1;
@property (nonatomic) NSInteger scoreTeam2;
@property (nonatomic) NSInteger team1ID;
@property (nonatomic) NSInteger team2ID;

- (TeamMatchRoundScore *) initWithRoundScore:(NSInteger)round
                                  scoreTeam1:(NSInteger)scoreTeam1
                                 withTeam1ID:(NSInteger)team1ID
                                  scoreTeam2:(NSInteger)scoreTeam2
                                 withTeam2ID:(NSInteger)team2ID;

@end
