//
//  PhenomViewController.m
//  Phenom
//
//  Created by James Chung on 3/14/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "PhenomViewController.h"
#import "UserPreferences.h"

@interface PhenomViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *launchImage;
@property (strong, nonatomic) UserPreferences *preferences;

@end

@implementation PhenomViewController

// lazy instantiation
-(UserPreferences *) preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

// me
- (void) setLaunchImage:(UIImageView *)launchImage
{
 //   [self.preferences setZeroID]; // need to put in convenience init
//    NSLog(@"Launch Page User ID is: %d", [self.preferences getUserID]);
//    _launchImage = launchImage;
//    self.launchImage.image = [UIImage imageNamed:@"Intro1.jpg"];
}
// end me

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
