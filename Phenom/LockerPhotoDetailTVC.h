//
//  LockerPhotoDetailTVC.h
//  Vaiden
//
//  Created by James Chung on 5/13/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "CustomBaseTVC.h"
#import "Sport.h"
#import "CommentsTVC.h"
#import "MBProgressHUD.h"
#import "LPDTaggedUsersCell.h"

@interface LockerPhotoDetailTVC : CustomBaseTVC <CommentsTVC_Delegate, MBProgressHUDDelegate, LPDTaggedUsersCell_Delegate>
{
    MBProgressHUD *HUD;
}

@property (nonatomic) NSInteger lockerID;
@property (nonatomic, strong) NSString *imageName;
@property (nonatomic) NSInteger picCreatorID;
@property (nonatomic, strong) NSString *picSeason;
@property (nonatomic) NSInteger picYear;
@property (nonatomic, strong) Sport *picSport;
@property (assign) BOOL fromExplore;

@end
