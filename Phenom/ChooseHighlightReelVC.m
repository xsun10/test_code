//
//  ChooseHighlightReelVC.m
//  Phenom
//
//  Created by James Chung on 5/28/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "ChooseHighlightReelVC.h"
#import "AFNetworking.h"
#import "UIImage+VidTools.h"
#import "UIImage+ProportionalFill.h"


@interface ChooseHighlightReelVC () <UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (weak, nonatomic) UIActionSheet *profilePicActionSheet;
@property (strong, nonatomic)UIPopoverController *imagePickerPopover;

@end

@implementation ChooseHighlightReelVC


- (void)viewDidLoad
{
    [super viewDidLoad];

    
}



#define TITLE_OF_ACTIONSHEET @"Choose A Highlight Reel"
#define CANCEL_BUTTON_ACTIONSHEET @"Cancel "
#define FROM_CAMERA @"Take a 30 sec video"
#define FROM_LIBRARY @"Choose From Photo Library"

- (void)chooseVideo
{
    if (!self.profilePicActionSheet) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                      initWithTitle:TITLE_OF_ACTIONSHEET delegate:self
                                      cancelButtonTitle:CANCEL_BUTTON_ACTIONSHEET destructiveButtonTitle:nil
                                      otherButtonTitles:FROM_CAMERA, FROM_LIBRARY, nil];
        [actionSheet showInView:self.view];
        self.profilePicActionSheet = actionSheet;
    }
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == actionSheet.destructiveButtonIndex) {
        [self.view.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    } else {
        NSString *choice = [actionSheet buttonTitleAtIndex:buttonIndex];
        
        NSLog(@"Choice is: %@", choice);
        if ([choice isEqualToString:FROM_CAMERA]) {
            [self changePicFromCamera];
        } else if ([choice isEqualToString:FROM_LIBRARY]){
            [self changePicFromPhotoLibrary];
        }
    }
}

- (void)changePicFromCamera
{
    [self presentImagePicker:UIImagePickerControllerSourceTypeCamera];
}


- (void) changePicFromPhotoLibrary
{
    [self presentImagePicker:UIImagePickerControllerSourceTypeSavedPhotosAlbum];
}

- (void) presentImagePicker:(UIImagePickerControllerSourceType)sourceType
{
    if ([UIImagePickerController isSourceTypeAvailable:sourceType]) {
        NSArray *availableMediaTypes = [UIImagePickerController availableMediaTypesForSourceType:sourceType];
        if ([availableMediaTypes containsObject:(NSString *)kUTTypeMovie]) {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.sourceType = sourceType;
            picker.mediaTypes = @[(NSString *)kUTTypeMovie];
            picker.allowsEditing = YES;
            picker.videoQuality = UIImagePickerControllerQualityTypeLow;
            picker.videoMaximumDuration = 30.0f;
            picker.delegate = self;
            
            // present the picker
            
            //    if ((sourceType != UIImagePickerControllerSourceTypeCamera) && (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)) {
            //       self.imagePickerPopover = [[UIPopoverController alloc] initWithContentViewController:picker];
            //        [self.imagePickerPopover presentPopoverFromBarButtonItem:<#(UIBarButtonItem *)#> permittedArrowDirections:<#(UIPopoverArrowDirection)#> animated:<#(BOOL)#>]
            //    }
            [self presentViewController:picker animated:YES completion:nil];
        }
    }
}

- (void) imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

#define MAX_IMAGE_WIDTH 300 // might want to change this to height....

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSURL *vidURL = info[UIImagePickerControllerMediaURL];
    
    NSData *vidData = [NSData dataWithContentsOfURL:vidURL];
    
    UIImage *vidThumbnail = [[UIImage imageFromMovie2:vidURL] imageCroppedToFitSize:CGSizeMake(70.0, 70.0)];
        
    /*
    NSString *path = [NSTemporaryDirectory() stringByAppendingPathComponent:@"upload-image.tmp"];
    NSData *imageData = UIImageJPEGRepresentation(image, 1);
    //you can also use UIImageJPEGRepresentation(img,1); for jpegs
    [imageData writeToFile:path atomically:YES];
    
    
    if (!image) image = info[UIImagePickerControllerOriginalImage];
    if (image) {
        
        
        UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
        CGRect frame = imageView.frame;
        if (frame.size.width > MAX_IMAGE_WIDTH) {
            frame.size.height = (frame.size.height / frame.size.width) * MAX_IMAGE_WIDTH;
            frame.size.width = MAX_IMAGE_WIDTH;
        }
        imageView.frame = frame;
 
        
     
    }*/
    
    
    
    if (self.imagePickerPopover) {
        [self.imagePickerPopover dismissPopoverAnimated:YES];
        self.imagePickerPopover = nil;
    } else {
        [self.delegate setWithHighlighVC:self withVideoData:vidData andThumbnail:vidThumbnail andFileExt:[vidURL pathExtension]];
        [self dismissViewControllerAnimated:YES completion:^ {
            [self.navigationController popViewControllerAnimated:YES];
        }];
            
    }
    
}
- (IBAction)chooseFileAction:(id)sender
{
    [self chooseVideo];
}


@end
