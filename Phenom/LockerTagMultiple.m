//
//  LockerTagMultiple.m
//  Vaiden
//
//  Created by James Chung on 5/7/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "LockerTagMultiple.h"

@implementation LockerTagMultiple


- (LockerTagMultiple *) initMatchWithDetails:(NSInteger)lockerTagID
                                andTagType:(NSInteger)lockerTagType
                                andTagName:(NSString *)lockerTagName
                                 andTagArray:(NSMutableArray *)lockerTagArray

{
    
    self = [super initWithTagDetails:lockerTagID
                        andTagType:lockerTagType
                        andTagName:lockerTagName];
    
    if (self) {
        _tagSelectionArray = [[NSMutableArray alloc] initWithArray:lockerTagArray];
    }
    
    return self;
    
}

@end
