//
//  VoteReceiving.h
//  Vaiden
//
//  Created by Turbo on 7/30/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "News.h"

@interface VoteReceiving : News

@property (nonatomic) NSInteger targetID;
@property (nonatomic, strong) NSString *targetName;
@property (nonatomic, strong) NSString *targetProfileBaseString;
@property (nonatomic, strong) NSString *string;
@property (nonatomic, strong) NSString *type;

@property (nonatomic, strong) NSMutableArray * winners;
@property (nonatomic, strong) NSString *region;

- (VoteReceiving *)initWithNewsDetails:(NSInteger)newsMakerUserID
                        andUserName:(NSString *)newsMakerUserName
                      andProfileStr:(NSString *)profileBaseString
                           withDate:(NSDate *)newsDate
                        andPostType:(NSInteger)postType
                          andNewsID:(NSInteger)newsID
                        andTextPost:(NSString *)textPost
                           andTargetID:(NSInteger)targetID
                     andTargetUsername:(NSString *)targetName
                   andtargetProfileStr:(NSString *)targetProfileBaseString
                     andNumComments:(NSUInteger)numComments
                      andNumRemixes:(NSUInteger)numRemixes
                        andLockerID:(NSInteger)lockerID
                               andType:(NSString *)type;

- (VoteReceiving *)initWithNewsDetails:(NSInteger)newsMakerUserID
                           andUserName:(NSString *)newsMakerUserName
                         andProfileStr:(NSString *)profileBaseString
                              withDate:(NSDate *)newsDate
                           andPostType:(NSInteger)postType
                             andNewsID:(NSInteger)newsID
                           andTextPost:(NSString *)textPost
                        andNumComments:(NSUInteger)numComments
                         andNumRemixes:(NSUInteger)numRemixes
                             andwinner:(NSMutableArray *)winners
                               andType:(NSString *)type
                         andUserRegion:(NSString *)region;



@end
