//
//  SportsLevelsCell.h
//  Vaiden
//
//  Created by James Chung on 12/26/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IMDSportsLevelsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *sportsLevelPicBackground;
@property (weak, nonatomic) IBOutlet UIImageView *sportsLevelPic;
@property (weak, nonatomic) IBOutlet UIImageView *sportIcon;
@property (weak, nonatomic) IBOutlet UILabel *sportLabel;
@property (weak, nonatomic) IBOutlet UILabel *player1UsernameLabel;
@property (weak, nonatomic) IBOutlet UILabel *player2UsernameLabel;
@property (weak, nonatomic) IBOutlet UILabel *player1LevelLabel;
@property (weak, nonatomic) IBOutlet UILabel *player2LevelLabel;
@property (weak, nonatomic) IBOutlet UILabel *probabilityLabel1;
@property (weak, nonatomic) IBOutlet UILabel *probabilityLabel2;

@property (weak, nonatomic) IBOutlet UIView *myBackgroundView;
@end
