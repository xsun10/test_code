//
//  PlayerDetailNewsfeedPaginator.h
//  Vaiden
//
//  Created by James Chung on 2/21/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "NMPaginator.h"

@interface PlayerDetailNewsfeedPaginator : NMPaginator

@property (nonatomic) BOOL noPermissions;

//- (id)initWithPDNPageSize:(NSInteger)pageSize delegate:(id<NMPaginatorDelegate>)paginatorDelegate andDataObject:(id)results;
- (id)initWithPDNPageSize:(NSInteger)pageSize delegate:(id<NMPaginatorDelegate>)paginatorDelegate andPlayerID:(NSInteger)playerID;

#define PLAYER_DETAIL_SHOW_NEWS_POSTS 2
#define PLAYER_DETAIL_SHOW_LOCKER_COVERS 1
#define PLAYER_DETAIL_SHOW_MATCHES_ONLY 3

@property (nonatomic) NSInteger tabToShow;
- (void)fetchFirstPageWithTab:(NSInteger)tabToShow;

@end
