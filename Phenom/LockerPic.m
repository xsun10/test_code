//
//  LockerPic.m
//  Vaiden
//
//  Created by James Chung on 5/12/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "LockerPic.h"

@implementation LockerPic

- (LockerPic *)initWithDetails:(NSInteger)lockerID
                   andFileName:(NSString *)imageFileName
                    andCreator:(Player *)creator
                     andSeason:(NSString *)picSeason
                       andYear:(NSInteger)picYear
                       andTags:(NSMutableArray *)tagsArray
               andTaggedPeople:(NSMutableArray *)taggedPeopleArray
            andSharedFollowers:(NSMutableArray *)sharedFollowersArray
    andShareToAllFollowersFlag:(BOOL)shareToAllFollowers
                    andCaption:(NSString *)caption
                   andComments:(NSMutableArray *)comments
                      andLikes:(NSMutableArray *)likes
                   andPostDate:(NSDate *)postDate
{
    self = [super init];
    
    if (self) {
        _lockerID = lockerID;
        _imageFileName = imageFileName;
        _creator = creator;
        _picSeason = picSeason;
        _picYear = picYear;
        _tagsArray = tagsArray;
        _taggedPeopleArray = taggedPeopleArray;
        _sharedFollowersArray = sharedFollowersArray;
        _shareToAllFollowers = shareToAllFollowers;
        _caption = caption;
        _comments = comments;
        _likes = likes;
        _postDate = postDate;
        _statsGrp = [[StatsGroup alloc] init];
        _trainingTagsArray = [[NSMutableArray alloc] init];
    }
    return self;
}

- (LockerPic *)initWithDetails:(NSInteger)lockerID
                   andFileName:(NSString *)imageFileName
                    andCreator:(Player *)creator
                     andSeason:(NSString *)picSeason
                       andYear:(NSInteger)picYear
                andNumComments:(NSInteger)numComments
                    andNumLikes:(NSInteger)numLikes
                   andPostDate:(NSDate *)postDate
{
    self = [super init];
    
    if (self) {
        _lockerID = lockerID;
        _imageFileName = imageFileName;
        _creator = creator;
        _picSeason = picSeason;
        _picYear = picYear;
        _numLikes = numLikes;
        _numComments = numComments;
        _postDate = postDate;
        _taggedPeopleArray = [[NSMutableArray alloc] init];
        _sharedFollowersArray = [[NSMutableArray alloc] init];
        _statsGrp = [[StatsGroup alloc] init];
        _trainingTagsArray = [[NSMutableArray alloc] init];
        
    }
    return self;
}

- (LockerPic *)init
{
    self = [super init];
    
    if (self) {
        _taggedPeopleArray = [[NSMutableArray alloc] init];
        _sharedFollowersArray = [[NSMutableArray alloc] init];
    }
    return self;
}
@end
