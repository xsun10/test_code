//
//  IndividualMatch.m
//  Phenom
//
//  Created by James Chung on 4/17/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "IndividualMatch.h"
#import "IndividualMatchRoundScore.h"



@implementation IndividualMatch

- (IndividualMatch *) init
{
    self = [super init];
    
    if (self) {
        _scoreIndMatch = [[IndividualMatchScore alloc] initWithMatchScore];
    }
    
    return  self;
}
- (IndividualMatch *) initMatchWithDetails:(NSInteger)matchID
                          andMatchName:(NSString *)matchName
                             isPrivate:(BOOL)isPrivate
                             withSport:(Sport *)sport
                           withMessage:(NSString *)message
                            onDateTime:(NSDate *)dateTime
                               atVenue:(Venue *)venue
                         createdByUser:(NSInteger)creatorID
                       withMatchPlayer1:(MatchPlayer *)player1
                          withMatchPlayer2:(MatchPlayer *)player2
                                 andActive:(BOOL)active
                 andProbabilityPlayer1Wins:(float)probabilityUser1
                 andProbabilityPlayer2Wins:(float)probabilityUser2
{

    self = [super initMatchWithDetails:matchID
                          andMatchName:matchName
                             isPrivate:isPrivate
                             withSport:sport
                           withMessage:message
                            onDateTime:dateTime
                               atVenue:venue
                         createdByUser:creatorID
                             andActive:active
                        andMatchStatus:@""];
    
    if (self) {
        _player1 = player1;
        _player2 = player2;
        _probabilityPlayer1Wins = probabilityUser1;
        _probabilityPlayer2Wins = probabilityUser2;
        _scoreIndMatch = [[IndividualMatchScore alloc] initWithMatchScore];
    }
    return self;
    
}

// no probability List

- (IndividualMatch *) initMatchWithDetails:(NSInteger)matchID
                              andMatchName:(NSString *)matchName
                                 isPrivate:(BOOL)isPrivate
                                 withSport:(Sport *)sport
                               withMessage:(NSString *)message
                                onDateTime:(NSDate *)dateTime
                                   atVenue:(Venue *)venue
                             createdByUser:(NSInteger)creatorID
                          withMatchPlayer1:(MatchPlayer *)player1
                          withMatchPlayer2:(MatchPlayer *)player2
                                 andActive:(BOOL)active
   //                     andConfirmedStatus:(BOOL)isConfirmed

{
    self = [super initMatchWithDetails:matchID
                          andMatchName:matchName
                             isPrivate:isPrivate
                             withSport:sport
                           withMessage:message
                            onDateTime:dateTime
                               atVenue:venue
                         createdByUser:creatorID
                             andActive:active
                        andMatchStatus:@""];
    
    if (self) {
        _player1 = player1;
        _player2 = player2;
        _scoreIndMatch = [[IndividualMatchScore alloc] initWithMatchScore];
    }
    return self;
    
}

- (NSString *)winnerUsernameForRound:(NSInteger)roundNumber
{
    IndividualMatchRoundScore *matchScore = [self.scoreIndMatch.roundScoresIndMatch objectAtIndex:roundNumber];
    
    if (self.sport.largerScoreWins) {
        
        if (matchScore.scorePlayer1 > matchScore.scorePlayer2) {
            return self.player1.userName;
        } else if (matchScore.scorePlayer2 > matchScore.scorePlayer1) {
            return self.player2.userName;
        }
        
    } else if (!self.sport.largerScoreWins) { // lowest score wins
        
        if (matchScore.scorePlayer2 > matchScore.scorePlayer1) {
            return self.player1.userName;
        } else if (matchScore.scorePlayer1 > matchScore.scorePlayer2) {
            return self.player2.userName;
        }
    }
    
    return @"";
}

- (BOOL)wasMatchConfirmedBySessionUser:(NSInteger)sessionUserID
{

/*
    for (id object in iMatch.scoreIndMatch.roundScoresIndMatch) {
    //    if ([object[@"confirmer_id"] integerValue] == sessionUserID &&
    //        [object[@"disputed"] integerValue] != 1) {
    //        return YES;
            
    //    }
    }*/
    return NO;
}
/*
+ (NSInteger)numUndisputedConfirmations:(IndividualMatch *)iMatch
{
    NSInteger counter = 0;
    
    for (id object in iMatch.scoreIndMatch.roundScoresIndMatch) {
        if ([object[@"disputed"] integerValue] == 0) {
            counter ++;
        }
    }
    return counter;
}
*/

/*
+ (NSInteger)numDisputedConfirmations:(IndividualMatch *)iMatch
{
    NSInteger counter = 0;
    
    for (id object in iMatch.scoreIndMatch.roundScoresIndMatch) {
        if ([object[@"disputed"] integerValue] == 1) {
            counter ++;
        }
    }
    return counter;
}
*/

// returns "YES" if
- (BOOL)isScoreConfirmed
{
    if (self.scoreIndMatch.scoreConfirmedBy != 0)
        return YES;
    return NO;
}

- (BOOL)isScoreRecorded
{
    if (self.scoreIndMatch.scoreRecordedBy != 0)
        return YES;
    return NO;
}

- (BOOL)isScoreDisputed
{
    if (self.scoreIndMatch.scoreDisputedBy != 0)
        return YES;
    return NO;
}
/*
- (void)setScoreConfirmationFlag:(BOOL)confirmationFlag
{
    self.scoreIndMatch.isScoreConfirmed = confirmationFlag;
}*/


- (BOOL)isMatchRSVPConfirmed
{
    if ([self.player1.status isEqualToString:@"CONFIRMED"] && [self.player2.status isEqualToString:@"CONFIRMED"])
        return YES;
    else
        return NO;
}

// a match can have up to two versions of scores
// note: right now the server only returns the latest version of the score
// Note2: Want to make sure that the session user is player1...
- (void)setScoreInfoWithServerObject:(NSDictionary *)dataObject withSessionUserID:(NSInteger)sessionUserID
{
    
    if (![dataObject[@"version"] isKindOfClass:[NSNull class]]) {
        self.scoreIndMatch.version = [dataObject[@"version"] integerValue];
    
        
        if (![dataObject[@"recorder_user_id"] isKindOfClass:[NSNull class]]) {
            self.scoreIndMatch.scoreRecordedBy = [dataObject[@"recorder_user_id"] integerValue];
        }
        
        if (![dataObject[@"confirmer_user_id"] isKindOfClass:[NSNull class]]) {
            self.scoreIndMatch.scoreConfirmedBy = [dataObject[@"confirmer_user_id"] integerValue];
        }
        
        if (![dataObject[@"disputor_user_id"] isKindOfClass:[NSNull class]]) {
            self.scoreIndMatch.scoreDisputedBy = [dataObject[@"disputor_user_id"] integerValue];
        }
        
        if (![dataObject[@"was_session_user_score_overruled"] isKindOfClass:[NSNull class]]) {
            self.scoreIndMatch.wasFirstVersionIndividualScoreRecordedBySessionUserAndDisputedByOtherUser = [dataObject[@"was_session_user_score_overruled"] boolValue];
        }
        
        NSArray *scoresArray = dataObject[@"match_scores"];
    
        for (id obj in scoresArray) {
            
            if (sessionUserID == [obj[@"player1_id"] integerValue]) {
              [self.scoreIndMatch addRoundScore:[obj[@"round"] integerValue]
                               forPlayer1:[obj[@"player1_id"] integerValue]
                         withScorePlayer1:[obj[@"score_player1"] integerValue]
                               forPlayer2:[obj[@"player2_id"] integerValue]
                         withScorePlayer2:[obj[@"score_player2"] integerValue]];
            } else {
                [self.scoreIndMatch addRoundScore:[obj[@"round"] integerValue]
                                       forPlayer1:[obj[@"player2_id"] integerValue]
                                 withScorePlayer1:[obj[@"score_player2"] integerValue]
                                       forPlayer2:[obj[@"player1_id"] integerValue]
                                 withScorePlayer2:[obj[@"score_player1"] integerValue]];
                
            }
        }
    
    
    }
}

- (NSString *)matchCreatorUsername
{
    if (self.matchCreatorID == self.player1.userID) {
        return self.player1.userName;
    } else if (self.matchCreatorID == self.player2.userID) {
        return self.player2.userName;
    }
    return @"";
}

- (NSString *)matchCreatorProfileBaseString
{
    if (self.matchCreatorID == self.player1.userID) {
        return self.player1.profileBaseString;
    } else if (self.matchCreatorID == self.player2.userID) {
        return self.player2.profileBaseString;
    }
    return @"";
}

// The player that is not the session user
- (MatchPlayer *)getChallengerPlayerObject:(NSInteger)sessionUserID
{
    MatchPlayer *player = nil;
    
    if (self.player1.userID == sessionUserID) {
        player = self.player2;
    } else {
        player = self.player1;
    }
    
    return player;
}

// This is used the individual match status page
- (NSString *)getMatchStatusMessage:(NSUInteger)sessionUserID
{
    
    NSString *returnMessage;
    
    MatchPlayer *player1 = self.player1;
    MatchPlayer *player2 = self.player2;
    
    NSDate *thirtyMinAfter = [self.dateTime dateByAddingTimeInterval:1800];
    
    if (([[NSDate date] compare:thirtyMinAfter] == NSOrderedAscending && [self.matchStatus isEqualToString:@"UNCONFIRMED"])
        || [[NSDate date] compare:self.dateTime] == NSOrderedAscending) {
        
        if ([player1.status isEqualToString:@"DECLINED"]) {
            if (player1.userID == sessionUserID) {
                returnMessage = [NSString stringWithFormat:@"Match cancelled.  You have declined to play"];
            } else if (player2.userID == sessionUserID) {
                returnMessage = [NSString stringWithFormat:@"Match cancelled.  %@ has declined to play.", player1.userName];
            } else {
                returnMessage = [NSString stringWithFormat:@"This match has been cancelled"];
            }
        } else if ([player2.status isEqualToString:@"DECLINED"]) {
            if (player1.userID == sessionUserID) {
                returnMessage = [NSString stringWithFormat:@"Match cancelled.  %@ has declined to play.", player2.userName];
            } else if (player2.userID == sessionUserID) {
                 returnMessage = [NSString stringWithFormat:@"Match cancelled.  You have declined to play"];
            } else {
                returnMessage = [NSString stringWithFormat:@"This match has been cancelled"];
            }
        } else if ([self.matchStatus isEqualToString:@"CANCELLED"]) {
            returnMessage = @"This match has been cancelled";
        } else if ([player1.status isEqualToString:@"UNCONFIRMED"]) {
            if (player1.userID == sessionUserID) {
                returnMessage = [NSString stringWithFormat:@"%@ is awaiting your RSVP", player2.userName];
            } else if (player2.userID == sessionUserID) {
                returnMessage = [NSString stringWithFormat:@"Awaiting confirmation from %@", player1.userName];
            } else {
                returnMessage = [NSString stringWithFormat:@"Awaiting confirmation from %@", player1.userName];
            }
        } else if ([player2.status isEqualToString:@"UNCONFIRMED"]) {
            if (player1.userID == sessionUserID) {
                returnMessage = [NSString stringWithFormat:@"Awaiting confirmation from %@", player2.userName];
            } else if (player2.userID == sessionUserID) {
                returnMessage = [NSString stringWithFormat:@"%@ is awaiting your RSVP", player1.userName];
            } else {
                returnMessage = [NSString stringWithFormat:@"Awaiting confirmation from %@", player2.userName];
            }
        } else if ([self.player1.status isEqualToString:@"ACCEPTED"] && [self.player2.status isEqualToString:@"ACCEPTED"]) {
            returnMessage = @"This match is Ready To Play";
        } else {
            returnMessage = [NSString stringWithFormat:@"%@ challenged %@", [self matchCreatorUsername], [self getOtherPlayer:self.matchCreatorID].userName];
        }

        
    } else {
        
        if ([self.matchStatus isEqualToString:@"CANCELLED"]) {
            returnMessage = @"This match has been cancelled";
        } else if ([self.matchStatus isEqualToString:@"NEVER_OCCURRED"]
                   || [self.matchStatus isEqualToString:@"UNCONFIRMED"]) {
            returnMessage = @"This match never occurred";
        } else if ([self isScoreDisputed]) { //[self canSessionUserJoinMatch:sessionUserID] &&
            returnMessage = @"This match outcome is in dispute";
            
        } else if ([self isScoreConfirmed]) {
            MatchPlayer *winner = [self getWinner];
            if (winner.userID == sessionUserID)
                returnMessage = @"You won this match";
            else if (winner.userName == nil)
                returnMessage = @"This match has a tie score";
            else
                returnMessage = [NSString stringWithFormat:@"%@ won this match", winner.userName];
            
        } else if ([self canSessionUserJoinMatch:sessionUserID] && [self isScoreRecorded] && ![self isScoreConfirmed] && self.scoreIndMatch.scoreRecordedBy != sessionUserID) {
            // only one person confirmed match but not the session user
            returnMessage = [NSString stringWithFormat:@"Confirm the score set by %@", [self getPlayerForID:self.scoreIndMatch.scoreRecordedBy].userName];
        } else if ([self canSessionUserJoinMatch:sessionUserID] && [self isScoreRecorded] == NO) {
            // no one confirmed score.  Therefore, session use can record score.
            returnMessage = @"Record the score from your match";
        } else if ([self canSessionUserJoinMatch:sessionUserID] && self.scoreIndMatch.scoreRecordedBy == sessionUserID) {
            returnMessage = @"Awaiting opponent to confirm score";
        } else if (self.scoreIndMatch.scoreRecordedBy > 0 && self.scoreIndMatch.scoreConfirmedBy == 0) {
            returnMessage = [NSString stringWithFormat:@"Awaiting %@ to confirm score", [self getOtherPlayer:self.scoreIndMatch.scoreRecordedBy].userName];
        } else {
            //return @"";
            return @"This match has expired"; //?
        }
    }
    
    
    return returnMessage;
}

/*
// This is used the individual match status page
- (NSString *)getMatchStatusMessage:(NSUInteger)sessionUserID
{
    // First check if session user can join match
    if (![self canSessionUserJoinMatch:sessionUserID]) {
        return @"";
    }
    
    // If can join the proceed to set the status message
    
    NSString *returnMessage;
    
    MatchPlayer *sessionPlayer = nil;
    MatchPlayer *otherPlayer = nil;
    
    // Get other player
    if (sessionUserID == self.player1.userID) {
        sessionPlayer = self.player1;
        otherPlayer = self.player2;
    } else  {
        otherPlayer = self.player1;
        sessionPlayer = self.player2;
    }
    
    
    NSDate *thirtyMinAfter = [self.dateTime dateByAddingTimeInterval:1800];
    
    if (([[NSDate date] compare:thirtyMinAfter] == NSOrderedAscending && [self.matchStatus isEqualToString:@"UNCONFIRMED"])
        || [[NSDate date] compare:self.dateTime] == NSOrderedAscending) {

        if ([otherPlayer.status isEqualToString:@"DECLINED"]){
            returnMessage = [NSString stringWithFormat:@"Match cancelled.  %@ has declined to play.", otherPlayer.userName];
            
        } else if ([sessionPlayer.status isEqualToString:@"DECLINED"]) {
            returnMessage = [NSString stringWithFormat:@"Match cancelled.  You have declined to play."];
            
        } else if ([self.matchStatus isEqualToString:@"CANCELLED"]) {
            returnMessage = @"This match has been cancelled";
        } else if ([sessionPlayer.status isEqualToString:@"UNCONFIRMED"]) {
            returnMessage = [NSString stringWithFormat:@"%@ is awaiting your RSVP", otherPlayer.userName];
            
            
        } else if ([otherPlayer.status isEqualToString:@"CONFIRMED"]) {
            returnMessage = [NSString stringWithFormat:@"%@ is confirmed to play", otherPlayer.userName];
            
        } else if ([otherPlayer.status isEqualToString:@"DECLINED"]){
            returnMessage =[NSString stringWithFormat:@"%@ has declined to play", otherPlayer.userName];
            
        } else if ([otherPlayer.status isEqualToString:@"UNCONFIRMED"]){
            returnMessage = [NSString stringWithFormat:@"Awaiting confirmation from %@", otherPlayer.userName];
            
        } else if ([otherPlayer.status isEqualToString:@"ACCEPTED"] && [sessionPlayer.status isEqualToString:@"ACCEPTED"]) {
            returnMessage = @"This match is Ready To Play";
        } else {
            returnMessage = [NSString stringWithFormat:@"%@ challenged %@", [self matchCreatorUsername], [self getOtherPlayer:self.matchCreatorID].userName];
            
            
        }
        
    } else {
    
        
        
        if ([self.matchStatus isEqualToString:@"CANCELLED"]) {
            returnMessage = @"This match has been cancelled";
        } else if ([self.matchStatus isEqualToString:@"NEVER_OCCURRED"]
                   || [self.matchStatus isEqualToString:@"UNCONFIRMED"]) {
            returnMessage = @"This match never occurred";
        } else if ([self canSessionUserJoinMatch:sessionUserID] && [self isScoreDisputed]) {
            returnMessage = @"This match outcome is in dispute";
            
        } else if ([self isScoreConfirmed]) {
            MatchPlayer *winner = [self getWinner];
            if (winner.userID == sessionPlayer.userID)
                returnMessage = @"You won this match";
            else
                returnMessage = [NSString stringWithFormat:@"%@ won this match", winner.userName];
            
        } else if ([self canSessionUserJoinMatch:sessionUserID] && [self isScoreRecorded] && ![self isScoreConfirmed] && self.scoreIndMatch.scoreRecordedBy != sessionUserID) {
            // only one person confirmed match but not the session user
            returnMessage = [NSString stringWithFormat:@"Confirm the score set by %@", otherPlayer.userName];
        } else if ([self canSessionUserJoinMatch:sessionUserID] && [self isScoreRecorded] == NO) {
            // no one confirmed score.  Therefore, session use can record score.
            returnMessage = @"Record the score from your match.";
        } else if ([self canSessionUserJoinMatch:sessionUserID] && self.scoreIndMatch.scoreRecordedBy == sessionUserID) {
            returnMessage = @"Awaiting opponent to confirm score";
        } else {
            return @"";
        }
    }
    

    return returnMessage;
}
*/

- (NSString *)getSessionUserStatusMessage:(NSInteger)sessionUserID
{
    
    // first set the match status message
    
    MatchPlayer *sessionPlayer = nil;
    MatchPlayer *otherPlayer = nil;
    NSString *playerStatusText = @"";
    
    // Get other player
    if (sessionUserID == self.player1.userID) {
        sessionPlayer = self.player1;
        otherPlayer = self.player2;
    } else  {
        otherPlayer = self.player1;
        sessionPlayer = self.player2;
    }
    
    NSDate *thirtyMinAfter = [self.dateTime dateByAddingTimeInterval:1800];
    
    if (([[NSDate date] compare:thirtyMinAfter] == NSOrderedAscending && [self.matchStatus isEqualToString:@"UNCONFIRMED"])
        || [[NSDate date] compare:self.dateTime] == NSOrderedAscending) {
       
        if (self.matchCreatorID == sessionUserID) {
            
            playerStatusText = @"You are confirmed to play.";
            
        } else if ([sessionPlayer.status isEqualToString:@"ACCEPTED"]) {
            playerStatusText = @"You are confirmed to play.";
            
        } else if ([sessionPlayer.status isEqualToString:@"UNCONFIRMED"]) {
            playerStatusText = @"You have not RSVP'd for this match.";
            
        } else if ([sessionPlayer.status isEqualToString:@"DECLINED"]) {
            playerStatusText = @"You have declined this match.";
        }
    
    } else {
       
        
        if ([self.matchStatus isEqualToString:@"NEVER_OCCURRED"]
            || [self.matchStatus isEqualToString:@"UNCONFIRMED"]
            || [self.matchStatus isEqualToString:@"CANCELLED"]) {
            playerStatusText = @"You did not play in this match";
        } else if ([self canSessionUserJoinMatch:sessionUserID] && [self isScoreDisputed] && self.scoreIndMatch.scoreDisputedBy == sessionUserID) {
            playerStatusText = @"You disputed the score your opponent entered";
        } else if ([self canSessionUserJoinMatch:sessionUserID] && [self isScoreDisputed] && self.scoreIndMatch.scoreDisputedBy != sessionUserID) {
            playerStatusText = @"You entered a score that was disputed";
        } else if ([self canSessionUserJoinMatch:sessionUserID] && [self isScoreConfirmed] && self.scoreIndMatch.scoreConfirmedBy == sessionUserID) {
            playerStatusText = @"You confirmed the match score";
        } else if ([self canSessionUserJoinMatch:sessionUserID] && [self isScoreConfirmed] && self.scoreIndMatch.scoreConfirmedBy != sessionUserID) {
            playerStatusText = @"You recorded the match score";
        } else if ([self canSessionUserJoinMatch:sessionUserID] &&[self isScoreRecorded] && self.scoreIndMatch.scoreRecordedBy == sessionUserID) {
            playerStatusText = @"You recorded the match score";
        } else if ([self canSessionUserJoinMatch:sessionUserID] &&[self isScoreRecorded] && self.scoreIndMatch.scoreRecordedBy != sessionUserID) {
            playerStatusText = @"You played in this match";
        } else if ([self canSessionUserJoinMatch:sessionUserID] && ![self isScoreRecorded]) {
            playerStatusText = @"You played in this match";
        } else {
            playerStatusText = @"You did not play in this match";
        }

    
    }
    return  playerStatusText;

}
/*
- (NSInteger)getMatchPhase:(NSInteger)sessionUserID
{
    NSInteger phaseState = 0;

    // If can join the proceed to set the status message
    
    NSString *returnMessage;
    
    MatchPlayer *sessionPlayer = nil;
    MatchPlayer *otherPlayer = nil;
    
    // Get other player
    if (sessionUserID == self.player1.userID) {
        sessionPlayer = self.player1;
        otherPlayer = self.player2;
    } else  {
        otherPlayer = self.player1;
        sessionPlayer = self.player2;
    }
    

    
    if ([self.dateTime compare:[NSDate date]] == NSOrderedAscending) { // match is in past
        
        if ([self.matchStatus isEqualToString:@"CANCELLED"]) { // what about MATCH_NULLIFIED?
            
            if (![self canSessionUserJoinMatch:sessionUserID]) {
                return INDIVIDUAL_MATCH_PHASE_SESSION_USER_IS_NOT_MATCH_PARTICIPANT_AND_MATCH_IS_CANCELLED;
            } else if (sessionUserID == self.matchCreatorID) {
                return INDIVIDUAL_MATCH_PHASE_SESSION_USER_IS_MATCH_CREATOR_AND_MATCH_IS_CANCELLED;
            } else if ([self canSessionUserJoinMatch:sessionUserID]) {
                return INDIVIDUAL_MATCH_PHASE_SESSION_USER_IS_MATCH_PARTICIPANT_AND_MATCH_IS_CANCELLED;
            }
          
        } else if ([self.matchStatus isEqualToString:@"NEVER_OCCURRED"]) {
            
            if (![self canSessionUserJoinMatch:sessionUserID]) {
                
            }
            
        } else if ([self.matchStatus isEqualToString:@"UNCONFIRMED"]) {
            
            if (![self canSessionUserJoinMatch:sessionUserID]) {
                return INDIVIDUAL_MATCH_PHASE_SESSION_USER_IS_NOT_MATCH_PARTICIPANT_AND_MATCH_IS_EXPIRED;
            } else if (sessionUserID == self.matchCreatorID) {
                return INDIVIDUAL_MATCH_PHASE_SESSION_USER_IS_MATCH_CREATOR_AND_MATCH_EXPIRED;
            } else if ([self canSessionUserJoinMatch:sessionUserID]) {
                return INDIVIDUAL_MATCH_PHASE_SESSION_USER_IS_MATCH_PARTICIPANT_AND_MATCH_EXPIRED;
            }
            
        } else if ([self isScoreDisputed]) {
            
            if (![self canSessionUserJoinMatch:sessionUserID]) {
                return INDIVIDUAL_MATCH_PHASE_SESSION_USER_IS_NOT_MATCH_PARTICIPANT_AND_MATCH_SCORE_DISPUTED;
            } else if (sessionUserID == self.matchCreatorID && self.scoreIndMatch.scoreDisputedBy == self.matchCreatorID) {
                return INDIVIDUAL_MATCH_PHASE_SESSION_USER_IS_MATCH_CREATOR_AND_MATCH_SCORE_DISPUTED_BY_MATCH_CREATOR;
            } else if (sessionUserID == self.matchCreatorID && self.scoreIndMatch.scoreDisputedBy != self.matchCreatorID) {
                return INDIVIDUAL_MATCH_PHASE_SESSION_USER_IS_MATCH_CREATOR_AND_MATCH_SCORE_DISPUTED_BY_OPPONENT;
            } else if ([self canSessionUserJoinMatch:sessionUserID] && self.scoreIndMatch.scoreDisputedBy == sessionUserID) {
                return INDIVIDUAL_MATCH_PHASE_SESSION_USER_IS_MATCH_PARTICIPANT_AND_MATCH_SCORE_DISPUTED_BY_SESSION_USER;
            } else if ([self canSessionUserJoinMatch:sessionUserID] && self.scoreIndMatch.scoreDisputedBy != sessionUserID) {
                return INDIVIDUAL_MATCH_PHASE_SESSION_USER_IS_MATCH_PARTICIPANT_AND_MATCH_SCORE_DISPUTED_BY_OPPONENT;
            }
            
        } else if ([self isScoreConfirmed]) {
            
            if (![self canSessionUserJoinMatch:sessionUserID]) {
                return INDIVIDUAL_MATCH_PHASE_SESSION_USER_IS_NOT_MATCH_PARTICIPANT_AND_MATCH_SCORE_CONFIRMED;
            } else if (sessionUserID == self.matchCreatorID && self.scoreIndMatch.scoreRecordedBy == sessionUserID) {
                return INDIVIDUAL_MATCH_PHASE_SESSION_USER_IS_MATCH_CREATOR_AND_MATCH_SCORE_RECORDED_BY_SESSION_USER_AND_SCORE_IS_CONFIRMED;
            } else if (sessionUserID == self.matchCreatorID && self.scoreIndMatch.scoreRecordedBy != sessionUserID) {
                return INDIVIDUAL_MATCH_PHASE_SESSION_USER_IS_MATCH_CREATOR_AND_MATCH_SCORE_RECORDED_BY_OPPONENT_AND_SCORE_IS_CONFIRMED;
            }else if ([self canSessionUserJoinMatch:sessionUserID] && self.scoreIndMatch.scoreRecordedBy == sessionUserID) {
                return INDIVIDUAL_MATCH_PHASE_SESSION_USER_IS_MATCH_PARTICIPANT_AND_MATCH_SCORE_RECORDED_BY_SESSION_USER_AND_SCORE_IS_CONFIRMED;
            } else if ([self canSessionUserJoinMatch:sessionUserID] && self.scoreIndMatch.scoreRecordedBy != sessionUserID) {
                return INDIVIDUAL_MATCH_PHASE_SESSION_USER_IS_MATCH_PARTICIPANT_AND_MATCH_SCORE_RECORDED_BY_OPPONENT_AND_SCORE_IS_CONFIRMED;
            }
        } else if ([self isScoreRecorded]) {
            
            
        } else if ([self.matchStatus isEqualToString:@"RSVP_CONFIRMED"]) {
            
        }
            
        } else if ([self canSessionUserJoinMatch:sessionUserID] && [self isScoreRecorded] && ![self isScoreConfirmed] && self.scoreIndMatch.scoreRecordedBy != sessionUserID) {
            // only one person confirmed match but not the session user
            returnMessage = [NSString stringWithFormat:@"Confirm the score set by %@", otherPlayer.userName];
        } else if ([self canSessionUserJoinMatch:sessionUserID] && [self isScoreRecorded] == NO) {
            // no one confirmed score.  Therefore, session use can record score.
            returnMessage = @"Record the score from your match.";
        } else if ([self canSessionUserJoinMatch:sessionUserID] && self.scoreIndMatch.scoreRecordedBy == sessionUserID) {
            returnMessage = @"Awaiting opponent to confirm score";
        } else {
            return @"";
        }
        

        
    } else { // match is now or in future
        
        if ([otherPlayer.status isEqualToString:@"DECLINED"]){
            returnMessage = [NSString stringWithFormat:@"Match cancelled.  %@ has declined to play", otherPlayer.userName];
            
        } else if ([sessionPlayer.status isEqualToString:@"DECLINED"]) {
            returnMessage = [NSString stringWithFormat:@"Match cancelled.  You have declined to play %@", otherPlayer.userName];
            
        } else if ([self.matchStatus isEqualToString:@"CANCELLED"]) {
            returnMessage = @"This match has been cancelled";
        } else if ([sessionPlayer.status isEqualToString:@"UNCONFIRMED"]) {
            returnMessage = [NSString stringWithFormat:@"%@ is awaiting your RSVP", otherPlayer.userName];
            
            
        } else if ([otherPlayer.status isEqualToString:@"CONFIRMED"]) {
            returnMessage = [NSString stringWithFormat:@"%@ is confirmed to play in the match", otherPlayer.userName];
            
        } else if ([otherPlayer.status isEqualToString:@"DECLINED"]){
            returnMessage =[NSString stringWithFormat:@"%@ has declined to play in the match", otherPlayer.userName];
            
        } else if ([otherPlayer.status isEqualToString:@"UNCONFIRMED"]){
            returnMessage = [NSString stringWithFormat:@"Awaiting confirmation from %@", otherPlayer.userName];
            
        }

    }
    
    return phaseState;
}

 */
    
- (BOOL)canSessionUserJoinMatch:(NSUInteger)sessionUserID
{
    // Check if the session user is a match participant
    
    if (self.player1.userID == sessionUserID)
        return YES;
    else if (self.player2.userID == sessionUserID)
        return YES;
    
    return NO;
}

- (void)setPlayerProbabilities:(NSDictionary *)object
{

    if ([object[@"player1_id"] integerValue] == self.player1.userID) {
        self.probabilityPlayer1Wins = [object[@"prob_player1"] floatValue];
        self.probabilityPlayer2Wins = [object[@"prob_player2"] floatValue];
    } else if ([object[@"player1_id"] integerValue] == self.player2.userID){
        self.probabilityPlayer1Wins = [object[@"prob_player2"] floatValue];
        self.probabilityPlayer2Wins = [object[@"prob_player1"] floatValue];
    }
}

- (NSString *)usernameOfUserWhoDidNotRecordOrConfirmScore
{
    NSString *username;
    
    if (self.scoreIndMatch.scoreRecordedBy == self.player1.userID) {
        username = self.player2.userName;
    } else {
        username = self.player1.userName;
    }
    
    return  username;
}

// For a match with multiple rounds, will return the "set" unit name.
// For a match with a single round, will return the "game" unit name
- (NSString *)getScoreUnitValue
{
    NSString *result = nil;
    
    if ([self.scoreIndMatch.roundScoresIndMatch count] > 1) {
        result = self.sport.setUnitName;
    } else {
        result = self.sport.gameUnitName;
    }
    
    
    return  result;
}

// returns the player opposite the userID that is passed in
- (Player *)getOtherPlayer:(NSInteger)userID
{
    if (self.player1.userID == userID) {
        return self.player2;
    } else if (self.player2.userID == userID) {
        return self.player1;
    }
    
    return nil;
}

- (void)loadCompetitorArrayFromObject:(NSArray *)competitorObject
{
    for (id object in competitorObject) {
  
        // I'll store the creator into player 1
        if ([object[@"competitor_id"] integerValue] == self.matchCreatorID) {
            self.player1 = [[MatchPlayer alloc] initWithPlayerDetails:[object[@"competitor_id"] integerValue]
                                                               andProfileStr:object[@"profile_pic_string"]
                                                                 andUserName:object[@"username"]
                                                                 andFullName:object[@"fullname"]
                                                                 andLocation:object[@"location"]
                                                                    andAbout:object[@"about"]
                                                                    andCoins:[object[@"coins"] integerValue]
                                                                   andStatus:object[@"status"]];
            
        } else {
            self.player2 = [[MatchPlayer alloc] initWithPlayerDetails:[object[@"competitor_id"] integerValue]
                                                        andProfileStr:object[@"profile_pic_string"]
                                                          andUserName:object[@"username"]
                                                          andFullName:object[@"fullname"]
                                                          andLocation:object[@"location"]
                                                             andAbout:object[@"about"]
                                                             andCoins:[object[@"coins"] integerValue]
                                                            andStatus:object[@"status"]];

        }
    }
}
// This function should only be called once score is confirmed
// this assumes the highest score wins
- (MatchPlayer *)getWinner
{
    NSMutableArray *roundScores = self.scoreIndMatch.roundScoresIndMatch;
    
    NSInteger player1TotalScore = 0;
    NSInteger player2TotalScore = 0;
    NSInteger player1ID = 0;
    NSInteger player2ID = 0;
    
    
    if ([roundScores count] > 1) {
        
        for (IndividualMatchRoundScore *score in roundScores) {
            
            if (player1ID == 0 && player2ID == 0) {
                player1ID = score.userIDPlayer1;
                player2ID = score.userIDPlayer2;
            }
            
            if (score.scorePlayer1 > score.scorePlayer2) {
                player1TotalScore ++;
            } else if (score.scorePlayer2 > score.scorePlayer1) {
                player2TotalScore ++;
            } else if (score.scorePlayer1 == score.scorePlayer2) {
                player1TotalScore ++;
                player2TotalScore ++;
            }
            
        }
    } else {
        // only 1 round / set
        IndividualMatchRoundScore *imrs = [self.scoreIndMatch.roundScoresIndMatch objectAtIndex:0];
        player1TotalScore = imrs.scorePlayer1;
        player2TotalScore = imrs.scorePlayer2;
        
        player1ID = imrs.userIDPlayer1;
        player2ID = imrs.userIDPlayer2;
    }
    
    
    
    if (player1TotalScore > player2TotalScore) {
        return [self getPlayerForID:player1ID];
    } else if (player2TotalScore > player1TotalScore) {
        return [self getPlayerForID:player2ID];
    } else if (player1TotalScore == player2TotalScore) {
        return nil; // tie
    }
    return nil;
}

// This function should only be called once score is confirmed
// this assumes highest score wins
- (MatchPlayer *)getLoser
{
    NSMutableArray *roundScores = self.scoreIndMatch.roundScoresIndMatch;
    
    NSInteger player1TotalScore = 0;
    NSInteger player2TotalScore = 0;
    NSInteger player1ID = 0;
    NSInteger player2ID = 0;
    
    
    if ([roundScores count] > 1) {
        
        for (IndividualMatchRoundScore *score in roundScores) {
            
            if (player1ID == 0 && player2ID == 0) {
                player1ID = score.userIDPlayer1;
                player2ID = score.userIDPlayer2;
            }
            
            if (score.scorePlayer1 > score.scorePlayer2) {
                player1TotalScore ++;
            } else if (score.scorePlayer2 > score.scorePlayer1) {
                player2TotalScore ++;
            } else if (score.scorePlayer1 == score.scorePlayer2) {
                player1TotalScore ++;
                player2TotalScore ++;
            }
            
        }
    } else {
        // only 1 round / set
        IndividualMatchRoundScore *imrs = [self.scoreIndMatch.roundScoresIndMatch objectAtIndex:0];
        player1TotalScore = imrs.scorePlayer1;
        player2TotalScore = imrs.scorePlayer2;
        
        player1ID = imrs.userIDPlayer1;
        player2ID = imrs.userIDPlayer2;
    }
    
    
    
    if (player1TotalScore < player2TotalScore) {
        return [self getPlayerForID:player1ID];
    } else if (player2TotalScore < player1TotalScore) {
        return [self getPlayerForID:player2ID];
    } else if (player1TotalScore == player2TotalScore) {
        return nil; // tie
    }
    return nil;
}

- (BOOL)isTie
{
    NSMutableArray *roundScores = self.scoreIndMatch.roundScoresIndMatch;
    
    NSInteger player1TotalScore = 0;
    NSInteger player2TotalScore = 0;
    NSInteger player1ID = 0;
    NSInteger player2ID = 0;
    
    
    if ([roundScores count] > 1) {
        
        for (IndividualMatchRoundScore *score in roundScores) {
            
            if (player1ID == 0 && player2ID == 0) {
                player1ID = score.userIDPlayer1;
                player2ID = score.userIDPlayer2;
            }
            
            if (score.scorePlayer1 > score.scorePlayer2) {
                player1TotalScore ++;
            } else if (score.scorePlayer2 > score.scorePlayer1) {
                player2TotalScore ++;
            } else if (score.scorePlayer1 == score.scorePlayer2) {
                player1TotalScore ++;
                player2TotalScore ++;
            }
            
        }
    } else {
        // only 1 round / set
        IndividualMatchRoundScore *imrs = [self.scoreIndMatch.roundScoresIndMatch objectAtIndex:0];
        player1TotalScore = imrs.scorePlayer1;
        player2TotalScore = imrs.scorePlayer2;
        
        player1ID = imrs.userIDPlayer1;
        player2ID = imrs.userIDPlayer2;
    }
    
    
    
    if (player1TotalScore < player2TotalScore) {
        return NO;
    } else if (player2TotalScore < player1TotalScore) {
        return NO;
    } else if (player1TotalScore == player2TotalScore) {
        return YES; // tie
    }
    return NO;
}


- (MatchPlayer *)getPlayerForID:(NSInteger)playerID
{
    if (playerID == self.player1.userID)
        return self.player1;
    else if (playerID == self.player2.userID)
        return self.player2;
    
    return nil;
        
}

// this is a hack to make sure that the right username is being associated with the right userID in the matchScore object
- (NSString *)getUsernameForScoreUserID:(NSInteger)userID
{
    if (self.player1.userID == userID)
        return self.player1.userName;
    else if (self.player2.userID == userID)
        return self.player2.userName;
    
    return @"";
}

- (BOOL)isUserMatchParticipant:(NSUInteger)userID
{
    if (self.player1.userID == userID)
        return YES;
    
    if (self.player2.userID == userID)
        return YES;
    
    return NO;
}


@end
