//
//  IndividualMatchDetailTVC.h
//  Phenom
//
//  Created by James Chung on 5/7/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IndividualMatch.h"
#import "MapViewTVC.h"
#import "MBProgressHUD.h"
#import "IMDHeaderInfoCell.h"


@interface IndividualMatchDetailTVC : MapViewTVC <MBProgressHUDDelegate, IMDHeaderInfoCell_Delegate>
{
    MBProgressHUD *HUD;
}

//@property (strong, nonatomic) IndividualMatch *individualMatch;
@property (nonatomic) NSUInteger matchID;

@end

