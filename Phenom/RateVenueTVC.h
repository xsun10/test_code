//
//  RateVenueTVC.h
//  Phenom
//
//  Created by James Chung on 7/3/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "CustomBaseTVC.h"

@interface RateVenueTVC : CustomBaseTVC <MBProgressHUDDelegate>
{
    MBProgressHUD *HUD;
}

@property (nonatomic) NSInteger venueID;

@end
