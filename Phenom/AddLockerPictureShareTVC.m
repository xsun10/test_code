//
//  AddLockerPictureShareTVC.m
//  Vaiden
//
//  Created by James Chung on 5/8/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "AddLockerPictureShareTVC.h"
#import "AddLockerPictureShareCell.h"
#import "UserPreferences.h"
#import "IOSRequest.h"
#import "Contact.h"
#import "S3Tools.h"
#import "AFNetworking.h"
#import "NewsFeedTVC.h"

@interface AddLockerPictureShareTVC () <UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *allFollowersPlusMinus;

@property (weak, nonatomic) IBOutlet UIImageView *imagePreview;
@property (weak, nonatomic) IBOutlet UILabel *addCaptionLabel;
@property (weak, nonatomic) IBOutlet UITextView *captionTextView;
@property (nonatomic, strong) UserPreferences *preferences;
@property (nonatomic, strong) NSMutableArray *contactsArray;
@property (nonatomic) BOOL allFollowersFlag;
@property (weak, nonatomic) IBOutlet UILabel *charactersRemainingLabel;
@property (nonatomic, strong) UIBarButtonItem *doneButton;
@property (nonatomic, strong) NSMutableArray *contactsToShareArray;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *doneBarButton;
@property (nonatomic) NSInteger charRemaining;

@end

@implementation AddLockerPictureShareTVC

#define MAX_CAPTION_LENGTH 200

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (NSMutableArray *)contactsArray
{
    if (!_contactsArray) _contactsArray = [[NSMutableArray alloc] init];
    return _contactsArray;
}

- (NSMutableArray *)contactsToShareArray
{
    if (!_contactsToShareArray) _contactsToShareArray = [[NSMutableArray alloc] init];
    return _contactsToShareArray;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
    
    self.captionTextView.delegate = self;
    self.imagePreview.image = self.viObject.imageHandle;
    self.allFollowersPlusMinus.image = [UIImage imageNamed:@"Check Circle Icon"];
    self.allFollowersFlag = YES;
    self.charRemaining = 0;
  //  [self setDoneButton];
    [self loadContacts];
}

/*- (void)setDoneButton
{
    UIButton *btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 50, 21);
    btn.layer.borderColor = [UIColor whiteColor].CGColor;
    btn.layer.borderWidth = 1.0;
    
    [btn addTarget:self action:@selector(done:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundColor:[UIColor clearColor]];
    [btn setTitle:@"DONE" forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-MediumItalic" size:12.0]];
    
    // Initialize UIBarbuttonitem...
    self.doneButton = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = self.doneButton;
}*/

- (IBAction)doneAction:(UIButton *)sender
{
    if (self.charRemaining >= 0) {
        self.doneBarButton.enabled = NO;
        
        if (self.allFollowersFlag) {
            [self.contactsToShareArray removeAllObjects];
            //    self.contactsToShareArray = self.contactsArray;
        } else {
            for (Contact *c in self.contactsArray) {
                if (c.isSelected)
                    [self.contactsToShareArray addObject:c];
            }
        }
        
        NSInteger venueID;
        
        if (self.venueObject == nil)
            venueID = -1;
        else
            venueID = self.venueObject.venueID;
        
        if (self.eventObject == nil) {
            self.eventObject = [[EventTag alloc] init];
            [self.eventObject setEventID:-1];
        }
        
        NSString *fileName = [self.imageFileName stringByAppendingString:@".jpg"];
        
        [IOSRequest saveLockerPostWithImage:fileName
                                  createdBy:[self.preferences getUserID]
                                 withSeason:self.seasonObject.season
                                    andYear:self.yearObject.year
                                   andSport:self.sportObject.sportID
                                   andVenue:venueID
                               andVenueName:self.venueObject.venueName
                             andTagContacts:self.contactsToTagArray
                           andShareContacts:self.contactsToShareArray
                                 shareToAll:self.allFollowersFlag
                               andStatsTags:self.statsTagsArray
                            andTrainingTags:self.trainingTagsArray
                             andUserCaption:self.captionTextView.text
                                andEventTag:self.eventObject
                               onCompletion:^(NSDictionary *results) {
                                   
                                   dispatch_async(dispatch_get_main_queue(), ^{
                                       self.doneBarButton.enabled = YES;
                                       if ([results[@"outcome"] isEqualToString:@"success"]) {
                                           [self.preferences setProfilePageRefreshState:YES];
                                           [self.preferences setNewsfeedRefreshState:YES];
                                           [self.navigationController popToRootViewControllerAnimated:YES];
                                       } else if ([results[@"outcome"] isEqualToString:@"failure"]) {
                                           UIAlertView *alert = [[UIAlertView alloc] init];
                                           alert.title = @"Image Upload Error";
                                           alert.message = @"There was a problem completing your request.  Please try again.";
                                           [alert addButtonWithTitle:@"OK"];
                                           [alert show];
                                       }
                                   });
                               }
         ];
    } else if(self.charRemaining < 0) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Caption Overlength";
        alert.message = [NSString stringWithFormat:@"The maximum limit for caption is %d.", MAX_CAPTION_LENGTH];
        [alert addButtonWithTitle:@"OK"];
        [alert show];
    }
}

- (void)done:(UIButton *)sender
{
   
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    self.addCaptionLabel.hidden = YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if ([textView.text length] == 0)
        self.addCaptionLabel.hidden = NO;
}

- (void)textViewDidChange:(UITextView *)textView
{
    self.charRemaining = MAX_CAPTION_LENGTH - [textView.text length];
    
    self.charactersRemainingLabel.text = [NSString stringWithFormat:@"%d Remaining", self.charRemaining];
    
    if (self.charRemaining < 0)
        self.charactersRemainingLabel.textColor = [UIColor redColor];
    else
        self.charactersRemainingLabel.textColor = [UIColor lightGrayColor];
}

- (void)loadContacts
{
    [IOSRequest fetchUserFollowersList:[self.preferences getUserID]
                         sessionUserID:[self.preferences getUserID]
                          onCompletion:^(NSMutableArray *results) {
                              
                              for (id object in results) {
                                  Contact *c = [[Contact alloc] initWithContactDetails:[object[@"user_id"] integerValue]
                                                                         andProfileStr:object[@"profile_pic_string"]
                                                                           andUserName:object[@"username"]
                                                                           andFullName:object[@"fullname"]
                                                                           andLocation:object[@"location"]
                                                                              andAbout:object[@"about"]
                                                                              andCoins:[object[@"coins"] floatValue]
                                                                           contactType:CONTACT_TYPE_FOLLOWING
                                                                       followingStatus:[object[@"follow_status"] integerValue]];
                                  [self.contactsArray addObject:c];
                              }
                              
                              dispatch_async(dispatch_get_main_queue(), ^{
                                  
                                  [self.tableView reloadData];
                              });
                          }];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60.0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.contactsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AddLockerPictureShareCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Share Cell" forIndexPath:indexPath];
    
    Contact *c = [self.contactsArray objectAtIndex:indexPath.row];
    
    cell.usernameLabel.text = [NSString stringWithFormat:@"@%@", c.userName];
    
    if ([c.fullName length] > 0)
        cell.fullnameLabel.text = c.fullName;
    else
        cell.fullnameLabel.text = c.userName;
    
    NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                            baseString:c.profileBaseString];
    
    [cell.profilePic setImageWithURL:[NSURL URLWithString:url]
                    placeholderImage:[UIImage imageNamed:@"avatar square"]];
    
    if (self.allFollowersFlag || c.isSelected) {
        cell.checkmarkIcon.image = [UIImage imageNamed:@"Check Circle Icon"];
    } else {
        cell.checkmarkIcon.image = [UIImage imageNamed:@"Minus Circle Icon"];
    }
    
    
    return cell;
}

- (IBAction)allFollowersAction:(id)sender
{
    [self.captionTextView resignFirstResponder];
    
    if (self.allFollowersFlag) {
        self.allFollowersFlag = NO;
        self.allFollowersPlusMinus.image = [UIImage imageNamed:@"Minus Circle Icon"];
    } else {
        self.allFollowersFlag = YES;
        self.allFollowersPlusMinus.image = [UIImage imageNamed:@"Check Circle Icon"];
    }
    [self.tableView reloadData];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.captionTextView resignFirstResponder];
    
    Contact *c = [self.contactsArray objectAtIndex:indexPath.row];
    
    if (c.isSelected)
        c.isSelected = NO;
    else
        c.isSelected = YES;
    
    [self.contactsArray replaceObjectAtIndex:indexPath.row withObject:c];
    [self.tableView reloadData];
    
}
@end
