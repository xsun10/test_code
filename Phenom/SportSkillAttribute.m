//
//  SportSkillType.m
//  Vaiden
//
//  Created by James Chung on 10/7/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "SportSkillAttribute.h"

@implementation SportSkillAttribute

-(SportSkillAttribute *)initWillSportSkillType:(NSUInteger)sportSkillID
                 andSportSkillDescription:(NSString *)sportSkillDescription


{
    self = [super init];
    
    if (self) {
        _skillTypeDescription = sportSkillDescription;
        _skillTypeID = sportSkillID;
    }
    
    return self;
}


@end
