//
//  TeamMachRoundScore.m
//  Vaiden
//
//  Created by James Chung on 4/16/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "TeamMatchRoundScore.h"

@implementation TeamMatchRoundScore


- (TeamMatchRoundScore *) initWithRoundScore:(NSInteger)round
                                  scoreTeam1:(NSInteger)scoreTeam1
                                 withTeam1ID:(NSInteger)team1ID
                                  scoreTeam2:(NSInteger)scoreTeam2
                                 withTeam2ID:(NSInteger)team2ID

{
    self = [super initWithScore:round];
    
    
    if (self) {
        _scoreTeam1 = scoreTeam1;
        _scoreTeam2 = scoreTeam2;
        _team1ID = team1ID;
        _team2ID = team2ID;
    }
    
    return self;
}

@end
