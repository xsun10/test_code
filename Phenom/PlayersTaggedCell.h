//
//  PlayersTaggedCell.h
//  Vaiden
//
//  Created by James Chung on 5/12/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlayersTaggedCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *playerUsername;
@property (weak, nonatomic) IBOutlet UIImageView *profilePic;
@end
