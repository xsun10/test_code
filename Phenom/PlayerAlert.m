//
//  PlayerAlert.m
//  Vaiden
//
//  Created by James Chung on 5/6/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "PlayerAlert.h"

@implementation PlayerAlert


- (PlayerAlert *) initWithPlayerAlertDetails:(NSInteger)userID
                       andUserName:(NSString *)userName
                         andProfilePicString:(NSString *)profilePicString
                                  andMessage:(NSString *)message
                                andAlertType:(NSInteger)alertType
                                andAlertDate:(NSDate *)alertDate
                                andWasViewed:(BOOL)wasViewed
{
    self = [super init];
    
    if (self) {
        _refPlayer = [[Contact alloc] initWithPlayerDetails:userID
                                               andUserName:userName
                                       andProfilePicString:profilePicString];
        _message = message;
        _alertType = alertType;
        _wasViewed = wasViewed;
        _alertDateTime = alertDate;
    }
    return self;
}
@end
