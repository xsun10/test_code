//
//  LPDTaggedUsersCell.h
//  Vaiden
//
//  Created by James Chung on 5/14/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>


@class LPDTaggedUsersCell;

@protocol LPDTaggedUsersCell_Delegate <NSObject>

- (void)setWithCell:(LPDTaggedUsersCell *)cell withUserID:(NSInteger)userID;

@end







@interface LPDTaggedUsersCell : UITableViewCell <UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, weak) id <LPDTaggedUsersCell_Delegate> delegate;

@property (weak, nonatomic) IBOutlet UICollectionView *usersCollectionView;
@property (nonatomic, strong) NSMutableArray *taggedUsers;
- (void)myInit;
@end
