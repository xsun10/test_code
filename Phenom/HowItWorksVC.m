//
//  HowItWorksVC.m
//  Phenom
//
//  Created by James Chung on 3/14/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "HowItWorksVC.h"
#import "HowItWorksView.h"
#import "HowItWorksModel.h"

@interface HowItWorksVC ()
@property (weak, nonatomic) IBOutlet HowItWorksView *slideView;
@property (strong, nonatomic) HowItWorksModel *howData;
@property (weak, nonatomic) IBOutlet UIPageControl *slideProgress;


@end

@implementation HowItWorksVC


- (HowItWorksModel *) howData
{
    if (!_howData) _howData = [[HowItWorksModel alloc] init];
    return _howData;
}


// swipe right gesture
- (IBAction)swipe:(UISwipeGestureRecognizer *)sender
{
    self.slideView.slideImageFile = [self.howData getNextSlide:@"Right"];
    self.slideProgress.currentPage = [self.howData currentCount];
}

// swipe left gesture
- (IBAction)swipeLeft:(UISwipeGestureRecognizer *)sender
{
    self.slideView.slideImageFile = [self.howData getNextSlide:@"Left"];
    self.slideProgress.currentPage = [self.howData currentCount];
}


- (void) viewWillAppear:(BOOL)animated
{
    // how I am initializing the view with default image 
    self.slideView.slideImageFile = [self.howData getNextSlide:@""];
  
}





@end
