//
//  ConfirmIndMatchScoreSuccessVC.m
//  Vaiden
//
//  Created by James Chung on 8/1/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "ConfirmIndMatchScoreSuccessVC.h"
#import "UIView+Manipulate.h"
#import "UIImage+ImageEffects.h"
#import "UIColor+VaidenColors.h"
#import "UserPreferences.h"
#import "IOSRequest.h"

@interface ConfirmIndMatchScoreSuccessVC ()
@property (weak, nonatomic) IBOutlet UILabel *changeLevelLabel;
@property (weak, nonatomic) IBOutlet UILabel *origLevelLabel;
@property (weak, nonatomic) IBOutlet UILabel *revisedLevelLabel;
@property (weak, nonatomic) IBOutlet UILabel *sportNameLevelLabel;

@property (weak, nonatomic) IBOutlet UILabel *changeCoinsLabel;
@property (weak, nonatomic) IBOutlet UILabel *origCoinLabel;
@property (weak, nonatomic) IBOutlet UILabel *revisedCoinLabel;
@property (weak, nonatomic) IBOutlet UIView *subBackground1;
@property (weak, nonatomic) IBOutlet UIView *subBackground2;
@property (weak, nonatomic) IBOutlet UIImageView *successIcon;
@property (weak, nonatomic) IBOutlet UIImageView *blurredImageBackground;
@property (weak, nonatomic) IBOutlet UIScrollView *pageScrollView;
@property (weak, nonatomic) IBOutlet UIButton *doneButtonHandle;
@property (weak, nonatomic) IBOutlet UILabel *updatedStatsHeadlineLabel;

@property (weak, nonatomic) IBOutlet UIView *circleBackground;

@property (nonatomic, strong) UserPreferences *preferences;
@property (nonatomic, strong) UIView *tempView;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;

@property (weak, nonatomic) IBOutlet UIImageView *titleConfirmIcon;
@end

@implementation ConfirmIndMatchScoreSuccessVC

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
 //   [self setPageBlurredBackgroundImage:1];

    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
    
    [self showTempSpinner];
    [self setPageInfo];
    //[self setDoneButton];
    
    [self.pageScrollView setContentInset:UIEdgeInsetsMake(63,0,40,0)]; // bottom was 130
    [self.circleBackground makeCircleWithColor:[UIColor colorWithRed:239/255.0 green:239/255.0 blue:244/255.0 alpha:1.0] andRadius:44];
}

/*- (void)setDoneButton
{
    UIButton *btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 50, 21);
    btn.layer.borderColor = [UIColor whiteColor].CGColor;
    btn.layer.borderWidth = 1.0;
    
    [btn addTarget:self action:@selector(doneAction:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundColor:[UIColor clearColor]];
    [btn setTitle:@"DONE" forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-MediumItalic" size:12.0]];
    
    // Initialize UIBarbuttonitem...
    UIBarButtonItem *proe = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = proe;
}*/


- (IBAction)doneAction:(id)sender
{
 //   [self performSegueWithIdentifier:@"Exit To Alerts Segue" sender:self];
    [self.preferences setAlertsPageRefreshState:YES];
    [self.preferences setNewsfeedRefreshState:YES];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.navigationItem setHidesBackButton:YES animated:YES];
}

- (void)showTempSpinner
{
    self.tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    self.tempView.backgroundColor = [UIColor lightGrayColor];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] init];
    [self.activityIndicator setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width/2.0, [UIScreen mainScreen].bounds.size.height/2.0 )];
    [self.activityIndicator startAnimating];
    [self.tempView addSubview:self.activityIndicator];
    
    [self.view addSubview:self.tempView];
    
}

- (void)setPageInfo
{
    [self.doneButtonHandle makeRoundedBorderWithColor:[UIColor salmonColor]];
    // let's convert the floats to diffs.  For now, round the level and coin numbers down.
    
    /*
    
    NSInteger sessionUserNewLevel = self.sessionUserNewLevel;
    NSInteger sessionUserOldLevel = self.sessionUserOldLevel;
    NSInteger sessionUserDiffLevel = sessionUserNewLevel - sessionUserOldLevel;
    
    NSInteger sessionUserNewCoins = self.sessionUserNewCoins;
    NSInteger sessionUserOldCoins = self.sessionUserOldCoins;
    NSInteger sessionUserDiffCoins = sessionUserNewCoins - sessionUserOldCoins;
    
    self.origLevelLabel.text = [NSString stringWithFormat:@"%d", sessionUserOldLevel];
    self.updatedStatsHeadlineLabel.textColor = [UIColor peacock];
    
    self.sportNameLevelLabel.text = [NSString stringWithFormat:@"%@ Level", [self.sportName capitalizedString]];
    
    if (sessionUserDiffLevel > 0) {
        self.changeLevelLabel.textColor = [UIColor greenColor];
        self.changeLevelLabel.text = [NSString stringWithFormat:@"+%d Levels Up", sessionUserDiffLevel];

    } else if (sessionUserDiffLevel < 0) {
        self.changeLevelLabel.textColor = [UIColor redColor];
        self.changeLevelLabel.text = [NSString stringWithFormat:@"%d Levels Down", sessionUserDiffLevel];
    } else {
        self.changeLevelLabel.textColor = [UIColor greenColor];
        self.changeLevelLabel.text = [NSString stringWithFormat:@"%d Level Change", sessionUserDiffLevel];
    }
    
    self.revisedLevelLabel.text = [NSString stringWithFormat:@"%d", sessionUserNewLevel];
    
    
    self.origCoinLabel.text = [NSString stringWithFormat:@"%d", sessionUserOldCoins];
    
    if (sessionUserDiffCoins > 0) {
        self.changeCoinsLabel.textColor = [UIColor greenColor];
        self.changeCoinsLabel.text = [NSString stringWithFormat:@"+%d", sessionUserDiffCoins];
    } else if (sessionUserDiffCoins < 0) {
        self.changeCoinsLabel.textColor = [UIColor redColor];
        self.changeCoinsLabel.text = [NSString stringWithFormat:@"%d", sessionUserDiffCoins];
    } else {
        self.changeCoinsLabel.textColor = [UIColor greenColor];
        self.changeCoinsLabel.text = [NSString stringWithFormat:@"%d", sessionUserDiffCoins];
    }
    
    self.revisedCoinLabel.text = [NSString stringWithFormat:@"%d", sessionUserNewCoins];
    [self.subBackground1 makeRoundedBorderWithColor:[UIColor darkGrayColor]];
    [self.subBackground2 makeRoundedBorderWithColor:[UIColor darkGrayColor]];
    
    self.successIcon.image = [[UIImage imageNamed:@"Positive Graph Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.successIcon setTintColor:[UIColor whiteColor]];
    */
    
    [IOSRequest getUpdatedLevelsForUser:[self.preferences getUserID]
                               forMatch:self.matchID
                           onCompletion:^(NSDictionary *results) {
                               
                               dispatch_async(dispatch_get_main_queue(), ^{
                                   
                                   NSInteger sessionUserNewLevel = [results[@"new_level"] integerValue];
                                   NSInteger sessionUserOldLevel = [results[@"old_level"] integerValue];
                                   NSInteger sessionUserDiffLevel = sessionUserNewLevel - sessionUserOldLevel;
                                   
                                   self.origLevelLabel.text = [NSString stringWithFormat:@"%ld", sessionUserOldLevel];
                         //          self.updatedStatsHeadlineLabel.textColor = [UIColor peacock];
                                   
                                   self.sportNameLevelLabel.text = [NSString stringWithFormat:@"%@ Level", [results[@"sport_name"] capitalizedString]];
                                   
                                   if (sessionUserDiffLevel > 0) {
                                       self.changeLevelLabel.textColor = [UIColor lightGreenColor];
                                       self.changeLevelLabel.text = [NSString stringWithFormat:@"+%ld Levels Up", sessionUserDiffLevel];
                                       
                                   } else if (sessionUserDiffLevel < 0) {
                                       self.changeLevelLabel.textColor = [UIColor redColor];
                                       self.changeLevelLabel.text = [NSString stringWithFormat:@"%ld Levels Down", sessionUserDiffLevel];
                                   } else {
                                       self.changeLevelLabel.textColor = [UIColor lightGreenColor];
                                       self.changeLevelLabel.text = [NSString stringWithFormat:@"%ld Level Change", sessionUserDiffLevel];
                                   }
                                   
                                   self.revisedLevelLabel.text = [NSString stringWithFormat:@"%ld", sessionUserNewLevel];

                                   [self.activityIndicator stopAnimating];
                                   self.tempView.hidden = YES;

                               });
                               
                           }];
    
}

- (void)setPageBlurredBackgroundImage:(NSUInteger)imageIndex
{
    NSString *effectText = @"";
    UIImage *effectImage = nil;
    UIImage *backImage = [UIImage imageNamed:@"BBall Player Background"];
    
    effectImage = backImage;
    
    switch (imageIndex)
    {
        case 0:
            effectImage = backImage;
            break;
        case 1:
            effectImage = [backImage applyLightEffect];
            effectText = NSLocalizedString(@"Light", @"");
            break;
        case 2:
            effectImage = [backImage applyExtraLightEffect];
            effectText = NSLocalizedString(@"Extra light", @"");
            break;
        case 3:
            effectImage = [backImage applyDarkEffect];
            effectText = NSLocalizedString(@"Dark", @"");
            break;
        case 4:
            effectImage = [backImage applyTintEffectWithColor:[UIColor blueColor]];
            effectText = NSLocalizedString(@"Color tint", @"");
            break;
    }
    
    self.blurredImageBackground.image = effectImage;
    
    
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    self.pageScrollView.contentSize = CGSizeMake(320.0, 550.0);
    
}

@end
