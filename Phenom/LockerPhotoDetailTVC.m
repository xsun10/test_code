//
//  LockerPhotoDetailTVC.m
//  Vaiden
//
//  Created by James Chung on 5/13/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "LockerPhotoDetailTVC.h"
#import "LockerPic.h"
#import "UserPreferences.h"
#import "IOSRequest.h"
#import "UIImage+ProportionalFill.h"
#import "S3Tools2.h"
#import "S3Tools.h"
#import "AFNetworking.h"
#import "LockerPic.h"
#import "PlayersTaggedCell.h"
#import "UIColor+VaidenColors.h"
#import "ShowLikesTVC.h"
#import "UIView+Manipulate.h"
#import "LPDTopCell.h"
#import "UIButton+RoundBorder.h"
#import "LPDTagCell.h"
#import "PlayingSeason.h"
#import "PlayingYear.h"
#import <FacebookSDK/FacebookSDK.h>
#import "PlayerDetailTVC.h"
#import "EventTag.h"
#import "TrainingTag.h"
#import "StatTag.h"
#import "LDStatCell.h"
#import "StatsGroup.h"
#import "StatTagCombination.h"
#import "StatsGroup.h"
#import "LDTrainingCell.h"
#import "FBShareRequestDialog.h"

@interface LockerPhotoDetailTVC () <UIActionSheetDelegate, FBShareRequestDialogDelegate>
@property (nonatomic, strong) UserPreferences *preferences;
@property (weak, nonatomic) IBOutlet UIView *imageOutterView;
@property (weak, nonatomic) IBOutlet UIImageView *detailPic;
@property (nonatomic, strong) Player *creator;
@property (nonatomic, strong) NSMutableArray *tagsArray;
@property (nonatomic, strong) NSMutableArray *taggedUsers;
@property (nonatomic, strong) Venue *taggedVenue;
@property (nonatomic) NSInteger newsfeedID;
@property (nonatomic) BOOL doesSessionUserLikePic;
@property (nonatomic) NSInteger numProps;
@property (nonatomic) NSInteger numComments;
@property (nonatomic) BOOL showKeyboardForCommentsPage;
@property (nonatomic, strong) NSString *caption;
@property (nonatomic, strong) PlayingSeason *season;
@property (nonatomic, strong) PlayingYear *year;
@property (nonatomic, strong) Sport *taggedSport;
@property (nonatomic, strong) EventTag *event;
@property (nonatomic, strong) StatsGroup *statsGp;
@property (nonatomic, strong) NSMutableArray *trainingArray;

@property (nonatomic) BOOL pageDoneLoading;
@property (nonatomic) NSInteger selectedPlayer;

@property (nonatomic) CGFloat captionHeight;

// Image Overlay parameters
@property (weak, nonatomic) IBOutlet UIView *overlayBackView;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;
@property (weak, nonatomic) IBOutlet UILabel *statValue1;
@property (weak, nonatomic) IBOutlet UILabel *statDescription1;
@property (weak, nonatomic) IBOutlet UILabel *statValue2;
@property (weak, nonatomic) IBOutlet UILabel *statDescription2;
@property (weak, nonatomic) IBOutlet UILabel *statValue3;
@property (weak, nonatomic) IBOutlet UILabel *statDescription3;
@property (weak, nonatomic) IBOutlet UIView *seperator2;
@property (weak, nonatomic) IBOutlet UIView *seperator1;
@property (weak, nonatomic) IBOutlet UIView *line2;
@property (weak, nonatomic) IBOutlet UIView *line1;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftpaddingTitle2;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftpaddingTitle1;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftpaddingContent1;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftpaddingSeperator;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftpaddingContent2;
@property (weak, nonatomic) IBOutlet UIImageView *trainingIcon;
@property (weak, nonatomic) IBOutlet UIImageView *vaidenIcon;
@property (weak, nonatomic) IBOutlet UILabel *drillLabel;
@end

@implementation LockerPhotoDetailTVC
#define TITLE_OF_ACTIONSHEET @"Locker Actions"
#define CANCEL_BUTTON_ACTIONSHEET @"Cancel "
#define FLAG_POST @"Report Inappropriate"
#define DELETE_POST @"Delete Image"
#define SHARE_POST @"Share Image"
#define MAKE_COVER @"Make this the Cover Photo"
#define REMOVE_USER_TAG @"Remove Me From Tag List"

#define STAT_CELL 0
#define TRAINING_CELL 1
#define TOP_CELL 2

#define TAGS_CELL 3
#define TAGGED_USERS_CELL 4



- (NSMutableArray *)trainingArray
{
    if (!_trainingArray) _trainingArray = [[NSMutableArray alloc] init];
    return _trainingArray;
}


- (NSMutableArray *)tagsArray
{
    if (!_tagsArray) _tagsArray = [[NSMutableArray alloc] init];
    return _tagsArray;
}

- (NSMutableArray *)taggedUsers
{
    if (!_taggedUsers) _taggedUsers = [[NSMutableArray alloc] init];
    return _taggedUsers;
}

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
    self.modalPresentationStyle = UIModalPresentationCurrentContext;
    self.tabBarController.modalPresentationStyle = UIModalPresentationCurrentContext;
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
    
    if (self.fromExplore) {
        self.navigationController.navigationBar.topItem.title = @"";
    }

    self.pageDoneLoading = NO;
    
    HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    HUD.labelText = @"Loading...";
    [self loadPicDetails];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(controlOverlayDisplay)];
    [self.imageOutterView addGestureRecognizer:tapGesture];
    
    [self.overlayBackView sendSubviewToBack:self.backgroundImage];
    [self.statValue1 setTextColor:[UIColor whiteColor]];
    [self.statDescription1 setTextColor:[UIColor whiteColor]];
    [self.statValue2 setTextColor:[UIColor whiteColor]];
    [self.statDescription2 setTextColor:[UIColor whiteColor]];
    [self.statValue3 setTextColor:[UIColor whiteColor]];
    [self.statDescription3 setTextColor:[UIColor whiteColor]];
    
    self.trainingIcon.image = [[UIImage imageNamed:@"Training Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.trainingIcon setTintColor:[UIColor whiteColor]];
    
    [self dismissAllOverlay];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if ([self.preferences getNewsfeedRefreshState]) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)dismissAllOverlay
{
    [self.overlayBackView setHidden:YES];
    [self.statValue1 setHidden:YES];
    [self.statDescription1 setHidden:YES];
    [self.statValue2 setHidden:YES];
    [self.statDescription2 setHidden:YES];
    [self.statValue3 setHidden:YES];
    [self.statDescription3 setHidden:YES];
    [self.seperator1 setHidden:YES];
    [self.seperator2 setHidden:YES];
    [self.line1 setHidden:YES];
    [self.line2 setHidden:YES];
    [self.drillLabel setHidden:YES];
    [self.trainingIcon setHidden:YES];
}

- (void)controlOverlayDisplay
{
    // Dismiss the overlay with amation
    if ([self.statsGp.statsArray count] > 0 || [self.trainingArray count] > 0) {
        if (self.overlayBackView.alpha == 0) {
            [UIView animateWithDuration:0.5
                                  delay:0
                                options:UIViewAnimationOptionCurveEaseIn
                             animations:^{
                                 self.overlayBackView.alpha = 1;
                             }
                             completion: nil];
        } else {
            [UIView animateWithDuration:0.5
                                  delay:0
                                options:UIViewAnimationOptionCurveEaseInOut
                             animations:^{
                                 self.overlayBackView.alpha = 0;
                             }
                             completion:nil];
        }
    }
}

- (void)loadPicDetails
{
    
    
    
    [IOSRequest fetchLockerPicDetail:self.lockerID
                       sessionUserID:[self.preferences getUserID]
                        onCompletion:^(NSDictionary *results) {
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                self.pageDoneLoading = YES;
                                self.creator = [[Player alloc] initWithPlayerDetails:[results[@"creator_id"] integerValue]
                                                                         andUserName:results[@"creator_username"]
                                                                 andProfilePicString:results[@"creator_profile_pic_string"]];
                                [self.creator setFullName:results[@"fullname"]];
                                
                                self.newsfeedID = [results[@"newsfeed_id"] integerValue];
                                
                                if (self.imageName == nil)
                                    self.imageName = results[@"image_name"];

                                NSLog(@"%@",results[@"users_tagged"]);
                                for (id object in results[@"users_tagged"]) {
                                    Player *p = [[Player alloc] initWithPlayerDetails:[object[@"user_id"] integerValue]
                                                                          andUserName:object[@"username"]
                                                                  andProfilePicString:object[@"profile_pic_string"]];
                                    
                                    [self.taggedUsers addObject:p];
                                }
                                
                                self.taggedVenue = [[Venue alloc] initVenueWithName:results[@"venue_name"]
                                                                         andVenueID:[results[@"venue_id"] integerValue]];
                                self.numProps = [results[@"num_likes"] integerValue];

                                self.numComments = [results[@"num_comments"] integerValue];
                                self.doesSessionUserLikePic = [results[@"does_session_user_like_pic"] boolValue];
                                self.caption = results[@"caption"];
                                self.season = [[PlayingSeason alloc] initWithSeason:results[@"season"]];
                                self.year = [[PlayingYear alloc] initWithYear:results[@"year"]];
                                self.taggedSport = [[Sport alloc] initWithSportDetails:[results[@"sport_id"] integerValue]
                                                                               andName:results[@"sport_name"]
                                                                               andType:nil];
                                
                                NSString *urlPostPic = [S3Tools2 getFileNameStringWithType:@"photo_regsize" baseString:[NSString stringWithFormat:@"%ld/photo/%@", self.creator.userID, self.imageName]];
                                
                                self.detailPic.contentMode = UIViewContentModeScaleAspectFill;
                                self.detailPic.clipsToBounds = YES;
                                
                                [self.detailPic setImageWithURL:[NSURL URLWithString:urlPostPic] placeholderImage:[UIImage imageNamed:@"Landscape Placeholder"]];
                                
                                
                                if (![results[@"training_tags"] isKindOfClass:[NSNull class]] && [results[@"training_tags"] count] > 0 ) {
                                    for (id obj in results[@"training_tags"]) {
                                        
                                   
                                        TrainingTag *tag = [[TrainingTag alloc] initWithTrainingTagInfo:[obj[@"training_id"] integerValue]
                                                                                     andTrainingTagName:obj[@"training_name"]];
                                        [self.trainingArray addObject:tag];
                                    }
                                }
                                
                           /*     if (![results[@"stat_tags"] isKindOfClass:[NSNull class]] && [results[@"stat_tags"] count] > 0 ) {
                                    
                                    for (id obj in results[@"stat_tags"]) {
                                        StatTag *stat = [[StatTag alloc] initWithStatTag:[obj[@"stat_id"] integerValue]
                                                                      andStatTagName:obj[@"stat_name"]
                                                                     andStatTagValue:obj[@"stat_value"]
                                                                         andStatLink:obj[@"stat_link"]
                                                                      andSubCategory:obj[@"stat_subcategory"]
                                                                  andShowSubcategory:[obj[@"stat_showsubcategory"] boolValue]];
                                        [self.statsArray addObject:stat];
                                        
                                    }
                                    
                                   
                                }
                            */
                                self.statsGp = [[StatsGroup alloc] initWithServerObject:results[@"stat_tags"]];
                                
                                
                                if (![results[@"event_tags"] isKindOfClass:[NSNull class]] && [results[@"event_tags"] count] > 0 ) {
                                    id obj = results[@"event_tags"];
                                    
                                
                                    self.event = [[EventTag alloc] initWithEvent:[obj[@"event_id"] integerValue]
                                                                    andEventName:obj[@"event_name"]
                                                                    andEventDate:nil];
                                }
                                
                                
                                [self.tableView reloadData];
                                if ([self.statsGp.statsArray count] == 1) {
                                    [self.overlayBackView setHidden:NO];
                                    [self.statValue1 setHidden:NO];
                                    [self.statDescription1 setHidden:NO];
                                    CGFloat tmp = self.leftpaddingTitle1.constant;
                                    [self.leftpaddingTitle1 setConstant: tmp + 90];
                                    tmp = self.leftpaddingContent1.constant;
                                    [self.leftpaddingContent1 setConstant: tmp + 90];
                                    [self.line1 setHidden:NO];
                                    [self.line2 setHidden:NO];
                                } else if ([self.statsGp.statsArray count] == 2) {
                                    [self.overlayBackView setHidden:NO];
                                    [self.statValue1 setHidden:NO];
                                    [self.statDescription1 setHidden:NO];
                                    [self.statValue2 setHidden:NO];
                                    [self.statDescription2 setHidden:NO];
                                    [self.seperator1 setHidden:NO];
                                    CGFloat tmp = self.leftpaddingTitle1.constant;
                                    [self.leftpaddingTitle1 setConstant: tmp + 25];
                                    tmp = self.leftpaddingContent1.constant;
                                    [self.leftpaddingContent1 setConstant: tmp + 25];
                                    tmp = self.leftpaddingTitle2.constant;
                                    [self.leftpaddingTitle2 setConstant: tmp + 70];
                                    tmp = self.leftpaddingContent2.constant;
                                    [self.leftpaddingContent2 setConstant: tmp + 70];
                                    tmp = self.leftpaddingSeperator.constant;
                                    [self.leftpaddingSeperator setConstant: tmp + 50];
                                } else if ([self.statsGp.statsArray count] == 3) {
                                    [self.overlayBackView setHidden:NO];
                                    [self.statValue1 setHidden:NO];
                                    [self.statDescription1 setHidden:NO];
                                    [self.statValue2 setHidden:NO];
                                    [self.statDescription2 setHidden:NO];
                                    [self.statValue3 setHidden:NO];
                                    [self.statDescription3 setHidden:NO];
                                    [self.seperator1 setHidden:NO];
                                    [self.seperator2 setHidden:NO];
                                } else if (self.trainingArray.count > 0) { // containing drills
                                    [self.overlayBackView setHidden:NO];
                                    [self.trainingIcon setHidden:NO];
                                    [self.drillLabel setHidden:NO];
                                    CGRect iconSize = self.vaidenIcon.frame;
                                    [self.vaidenIcon setFrame:CGRectMake(iconSize.origin.x, iconSize.origin.y + 5, iconSize.size.width-5, iconSize.size.height-5)];
                                } else {
                                    [self.overlayBackView setHidden:YES];
                                }
                                
                                [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
                                
                               /*
                                // set tagged users
                                
                                
                                [self.playersTaggedCollectionView reloadData];
                                
                                
                                [self setTags];
                                
                                [self.tagsCollectionView reloadData];
                                
                                if ([results[@"num_comments"] integerValue] == 1)
                                    self.numCommentsLabel.text = [NSString stringWithFormat:@"%ld Comment", [results[@"num_comments"] integerValue]];
                                else
                                    self.numCommentsLabel.text = [NSString stringWithFormat:@"%ld Comments", [results[@"num_comments"] integerValue]];
                                
                                
                                
                                
                                
                                if (self.numProps == 1)
                                    self.numPropsLabel.text = [NSString stringWithFormat:@"%ld Prop", self.numProps];
                                else
                                    self.numPropsLabel.text = [NSString stringWithFormat:@"%ld Props", self.numProps];
                                
                                */
                            });
                        }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.pageDoneLoading)
        return 5;
    
    return  0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == STAT_CELL) {
        
        /*if ([self.statsGp.statsArray count] > 0 ) {
            if ([self checkIfHeightShouldBeIncreasedBecauseLongStatName:indexPath.row])
                return 130;
            else
                return 105;
        } else */
            return 0;
    } else if (indexPath.row == TRAINING_CELL) {
        /*if ([self.trainingArray count] > 0)
            return [self calculateCellHeightForTrainingTag:indexPath.row];
        else*/
            return 0;
    
    } else if (indexPath.row == TOP_CELL) {
        
        if ([self.caption length] == 0)
            return [self calculateCellHeightForCaptionArea];
        else {
            return [self calculateCellHeightForCaptionArea]; // need to calculate dynamic
        }
    } else if (indexPath.row == TAGS_CELL) {
        if ([self.event.eventName length] > 0)
            return 127;
        else if ([self.taggedVenue.venueName length] > 0)
            return 95;
        else
            return 75;
        
    } else if (indexPath.row == TAGGED_USERS_CELL) {
        if ([self.taggedUsers count] > 0)
            return 102;
    }
    return 0;
}


- (BOOL)checkIfHeightShouldBeIncreasedBecauseLongStatName:(NSInteger)row
{
    for (id obj in self.statsGp.statsArray) {
        
        if ([obj isKindOfClass:[StatTagCombination class]]) {
            StatTagCombination *sta = (StatTagCombination *)obj;
            
            if ([sta.numerator.statLink length] > 13)
                return YES;
        }
    }
    return NO;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == STAT_CELL) {
        LDStatCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Stat Cell" forIndexPath:indexPath];
        
        
        if ([self.statsGp.statsArray count] > 0) {
           int counter = 0;
        
            NSString *otherStatText = @"";
        
            for (id obj in self.statsGp.statsArray) {
        
                if ([obj isKindOfClass:[StatTag class]]) {
                    
                    StatTag *stat = (StatTag *)obj;
                    
                    switch (counter) {
                        case 0:
                            cell.statValue1.text = [NSString stringWithFormat:@"%@", stat.statTagValue];
                            self.statValue1.text = [NSString stringWithFormat:@"%@", stat.statTagValue];
                            if (stat.showSubCategory) {
                                cell.statDescription1.text = [NSString stringWithFormat:@"%@ %@", stat.statLink, stat.statTagName];
                                self.statDescription1.text = [NSString stringWithFormat:@"%@ %@", stat.statLink, stat.statTagName];
                            } else {
                                cell.statDescription1.text = stat.statTagName;
                                self.statDescription1.text = stat.statTagName;
                            }
                            break;
                            
                        case 1:
                            cell.statValue2.text = [NSString stringWithFormat:@"%@", stat.statTagValue];
                            self.statValue2.text = [NSString stringWithFormat:@"%@", stat.statTagValue];
                            if (stat.showSubCategory) {
                                cell.statDescription2.text = [NSString stringWithFormat:@"%@ %@", stat.statLink, stat.statTagName];
                                self.statDescription2.text = [NSString stringWithFormat:@"%@ %@", stat.statLink, stat.statTagName];
                            } else {
                                cell.statDescription2.text = stat.statTagName;
                                self.statDescription2.text = stat.statTagName;
                            }
                            break;
                        case 2:
                            cell.statValue3.text = [NSString stringWithFormat:@"%@", stat.statTagValue];
                            self.statValue3.text = [NSString stringWithFormat:@"%@", stat.statTagValue];
                            if (stat.showSubCategory) {
                                cell.statDescription3.text = [NSString stringWithFormat:@"%@ %@", stat.statLink, stat.statTagName];
                                self.statDescription3.text = [NSString stringWithFormat:@"%@ %@", stat.statLink, stat.statTagName];
                            } else {
                                cell.statDescription3.text = stat.statTagName;
                                self.statDescription3.text = stat.statTagName;
                            }
                            break;
                        default:
                            break;
                    }
                    
                    
                    
                    /*
                    
                    if (counter == 0) {
                        if (stat.showSubCategory) {
                            cell.statNumLabel.text = [NSString stringWithFormat:@"%@", stat.statTagValue];
                        
                            cell.featuredStatLabel.text = [[NSString stringWithFormat:@"%@ %@", stat.statLink, stat.statTagName] capitalizedString];
                        } else {
                            cell.statNumLabel.text = [NSString stringWithFormat:@"%@", stat.statTagValue];
                            cell.featuredStatLabel.text = [[NSString stringWithFormat:@"%@", stat.statTagName] capitalizedString];
                        }
                    } else {
                        if ([otherStatText length] > 0)
                            otherStatText = [otherStatText stringByAppendingString:@", "];
                
                        if (stat.showSubCategory)
                            otherStatText = [otherStatText stringByAppendingString:[[NSString stringWithFormat:@"%@ %@ %@", stat.statTagValue, stat.statLink, stat.statTagName] capitalizedString]];
                        else
                            otherStatText = [otherStatText stringByAppendingString:[[NSString stringWithFormat:@"%@ %@", stat.statTagValue, stat.statTagName] capitalizedString]];
                    }
                     */
                    
                } else if ([obj isKindOfClass:[StatTagCombination class]]) {
                    StatTagCombination *sta = (StatTagCombination *)obj;
                    
                    switch (counter) {
                        case 0:
                            cell.statValue1.text = [NSString stringWithFormat:@"%@ / %@", sta.numerator.statTagValue, sta.denominator.statTagValue];
                            cell.statDescription1.text = [NSString stringWithFormat:@"%@", sta.numerator.statLink];
                            self.statValue1.text = [NSString stringWithFormat:@"%@ / %@", sta.numerator.statTagValue, sta.denominator.statTagValue];
                            self.statDescription1.text = [NSString stringWithFormat:@"%@", sta.numerator.statLink];
                            break;
                        case 1:
                            cell.statValue2.text = [NSString stringWithFormat:@"%@ / %@", sta.numerator.statTagValue, sta.denominator.statTagValue];
                            cell.statDescription2.text = [NSString stringWithFormat:@"%@", sta.numerator.statLink];
                            self.statValue2.text = [NSString stringWithFormat:@"%@ / %@", sta.numerator.statTagValue, sta.denominator.statTagValue];
                            self.statDescription2.text = [NSString stringWithFormat:@"%@", sta.numerator.statLink];
                            break;
                        case 2:
                            cell.statValue3.text = [NSString stringWithFormat:@"%@ / %@", sta.numerator.statTagValue, sta.denominator.statTagValue];
                            cell.statDescription3.text = [NSString stringWithFormat:@"%@", sta.numerator.statLink];
                            self.statValue3.text = [NSString stringWithFormat:@"%@ / %@", sta.numerator.statTagValue, sta.denominator.statTagValue];
                            self.statDescription3.text = [NSString stringWithFormat:@"%@", sta.numerator.statLink];
                            break;
                        default:
                            break;
                    }
                    /*
                    if (counter == 0) {
                    //    if (sta.numerator.showSubCategory) {
                            cell.statNumLabel.text = [NSString stringWithFormat:@"%@ / %@", sta.numerator.statTagValue, sta.denominator.statTagValue];
                            
                            cell.featuredStatLabel.text = [[NSString stringWithFormat:@"%@", sta.numerator.statLink] capitalizedString];
                      //  } else {
                      //      cell.statNumLabel.text = [NSString stringWithFormat:@"%@ / %@", sta.numerator.statTagValue, sta.denominator.statTagValue];
                      //      cell.featuredStatLabel.text = [[NSString stringWithFormat:@"%@ / %@ %@", sta.numerator.statTagValue, sta.denominator.statTagValue, sta.numerator.statTagName] capitalizedString];
                      //  }
                    } else {
                        if ([otherStatText length] > 0)
                            otherStatText = [otherStatText stringByAppendingString:@", "];
                        
                     //   if (sta.numerator.showSubCategory)
                     //       otherStatText = [otherStatText stringByAppendingString:[[NSString stringWithFormat:@"%@ / %@ %@ %@", sta.numerator.statTagValue, sta.denominator.statTagValue, sta.numerator.statLink, sta.numerator.statTagName] capitalizedString]];
                     //   else
                            otherStatText = [otherStatText stringByAppendingString:[[NSString stringWithFormat:@"%@ / %@ %@", sta.numerator.statTagValue, sta.denominator.statTagValue, sta.numerator.statLink] capitalizedString]];
                    
                    }*/
                    
                    
                }
                counter ++;
            }
        
            if ([otherStatText length] > 0)
                cell.otherStatsLabel.text = otherStatText;
            else
                cell.otherStatsLabel.hidden = YES;
            
            
            
        }
    //    cell.sportIcon.image = [[self.taggedSport getSportIcon] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    //    [cell.sportIcon setTintColor:[UIColor blackColor]];
    /*
        if ([self.statsGp.statsArray count] > 0 || [self.trainingArray count] > 0) {
            cell.likeIcon.image = [[UIImage imageNamed:@"Like Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            
            if (self.doesSessionUserLikePic)
                [cell.likeIcon setTintColor:[UIColor goldColor]];
            else
                [cell.likeIcon setTintColor:[UIColor darkGrayColor2]];
           
            cell.likeIcon.hidden = NO;
            cell.commentIcon.hidden = NO;
            cell.likeButtonHandle.hidden = NO;
            cell.commentButtonHandle.hidden = NO;
            [cell.likeButtonHandle addTarget:self action:@selector(likeAction:) forControlEvents:UIControlEventTouchUpInside];
            [cell.commentButtonHandle addTarget:self action:@selector(commentAction:) forControlEvents:UIControlEventTouchUpInside];
        } else {
            cell.likeButtonHandle.hidden = YES;
            cell.commentButtonHandle.hidden = YES;
            cell.likeIcon.hidden = YES;
            cell.commentIcon.hidden = YES;
        }
        */
        return cell;
    } else if (indexPath.row == TRAINING_CELL) {
        LDTrainingCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Training Cell" forIndexPath:indexPath];

         if ([self.trainingArray count] > 0) {
            
            NSString *otherTrainingText = @"";
             
            for (TrainingTag *tag in self.trainingArray) {
                if ([otherTrainingText length] > 0)
                    otherTrainingText = [otherTrainingText stringByAppendingString:@", "];
                
                otherTrainingText = [otherTrainingText stringByAppendingString:tag.trainingTagName];
            }
            
            if ([otherTrainingText length] > 0) {
                cell.trainingLabel.text = otherTrainingText;
                self.drillLabel.text = otherTrainingText;
            } else {
                cell.trainingLabel.hidden = YES;
            }
        }

        return cell;
    } else if (indexPath.row == TOP_CELL) {
        LPDTopCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Top Cell" forIndexPath:indexPath];
        
      /*  if ([self.statsGp.statsArray count] > 0 || [self.trainingArray count] > 0) {
            cell.likeButtonHandle.hidden = YES;
            cell.commentButtonHandle.hidden = YES;
            cell.likeIcon.hidden = YES;
            cell.commentIcon.hidden = YES;
        } else {
            cell.likeIcon.image = [[UIImage imageNamed:@"Like Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            
            if (self.doesSessionUserLikePic)
                [cell.likeIcon setTintColor:[UIColor goldColor]];
            else
                [cell.likeIcon setTintColor:[UIColor darkGrayColor2]];
            
            cell.likeButtonHandle.hidden = NO;
            cell.commentButtonHandle.hidden = NO;
            cell.likeIcon.hidden = NO;
            cell.commentIcon.hidden = NO;
            [cell.likeButtonHandle addTarget:self action:@selector(likeAction:) forControlEvents:UIControlEventTouchUpInside];
            [cell.commentButtonHandle addTarget:self action:@selector(commentAction:) forControlEvents:UIControlEventTouchUpInside];
        }*/
        
        [cell.likeButtonHandle addTarget:self action:@selector(likeAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell.commentButtonHandle addTarget:self action:@selector(commentAction:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.likeIcon.image = [[UIImage imageNamed:@"Like Icon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];

        if (self.doesSessionUserLikePic)
            [cell.likeIcon setTintColor:[UIColor goldColor]];
        else
            [cell.likeIcon setTintColor:[UIColor darkGrayColor2]];

        cell.creatorUsernameLabel.text = [NSString stringWithFormat:@"@%@", self.creator.userName];
        NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                                baseString:self.creator.profileBaseString];
        
        [cell.creatorPic setImageWithURL:[NSURL URLWithString:url]
                        placeholderImage:[UIImage imageNamed:@"avatar square"]];
        
        UIButton *picButton1 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
        [picButton1 addTarget:self action:@selector(goToProfile:) forControlEvents:UIControlEventTouchUpInside];
        picButton1.tag = self.creator.userID;
        [cell.creatorPic addSubview:picButton1];
        cell.creatorPic.userInteractionEnabled = YES;
        
        [cell.smallDot makeRoundedBorderWithRadius:2.5];
        
        if ([self.creator.fullName length] > 0)
            cell.creatorFullnameLabel.text = self.creator.fullName;
        else
            cell.creatorFullnameLabel.text = self.creator.userName;
        
        cell.numCommentsLabel.text = [NSString stringWithFormat:@"%ld comments", self.numComments];
        cell.numPropsLabel.text = [NSString stringWithFormat:@"%ld props", self.numProps];
        
        
        cell.captionLabel = [self makeDynamicHeighLabel:self.caption forLabel:cell.captionLabel];
        cell.captionLabel.text = self.caption;
        
        return cell;
    } else if (indexPath.row == TAGS_CELL) {
        LPDTagCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Tag Cell" forIndexPath:indexPath];
        
        cell.seasonIcon.image = [self.season seasonIcon];
        cell.seasonLabel.text = [self.season.season capitalizedString];
        cell.yearLabel.text = self.year.year;
        cell.eventLabel.text = self.event.eventName;
        if ([self.taggedVenue.venueName length] > 0)
            cell.venueLabel.text = self.taggedVenue.venueName;
        else
            cell.venueLabel.text = @"No location specified";
        
        cell.sportLabel.text = [self.taggedSport.sportName capitalizedString];
        cell.sportIcon.image = [[self.taggedSport getSportIcon2] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [cell.sportIcon setTintColor:[UIColor midGray]];
    
        return cell;
    } else if (indexPath.row == TAGGED_USERS_CELL) {
        LPDTaggedUsersCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Users Cell" forIndexPath:indexPath];
      
        cell.delegate = self;
        
        [cell myInit];
        
        cell.taggedUsers = self.taggedUsers;
        [cell.usersCollectionView reloadData];
        
        return cell;
    }

    return nil;
}


- (IBAction)likeAction:(UIButton *)sender
{
    if (self.doesSessionUserLikePic) {
        [IOSRequest UnLikeNewsfeedPost:self.newsfeedID
                         sessionUserID:[self.preferences getUserID]
                          onCompletion:^(NSDictionary *results) {
                              dispatch_async(dispatch_get_main_queue(), ^{
                                  
                                  if ([results[@"outcome"] isEqualToString:@"failure"]) {
                                      UIAlertView *alert = [[UIAlertView alloc] init];
                                      alert.title = @"UnLike Error";
                                      alert.message = @"There was a problem completing your request.  Please try again later.";
                                      [alert addButtonWithTitle:@"OK"];
                                      [alert show];
                                  } else {
                                      self.doesSessionUserLikePic = NO;
                                      self.numProps--;
                                      [self.tableView reloadData];
                                  }
                              });
                              
                          }];
    } else {
        [IOSRequest likeNewsfeedPost:self.newsfeedID
                       sessionUserID:[self.preferences getUserID]
                        onCompletion:^(NSDictionary *results) {
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                if ([results[@"outcome"] isEqualToString:@"failure"]) {
                                    UIAlertView *alert = [[UIAlertView alloc] init];
                                    alert.title = @"Like Error";
                                    alert.message = @"There was a problem completing your request.  Please try again later.";
                                    [alert addButtonWithTitle:@"OK"];
                                    [alert show];
                                } else {
                                    self.doesSessionUserLikePic = YES;
                                    self.numProps++;
                                    [self.tableView reloadData];
                                }
                            });
                        }];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Comments Segue"]) {
        CommentsTVC *controller = segue.destinationViewController;
        controller.newsID = self.newsfeedID;
        controller.delegate = self;
        controller.currentTableSection = 0; // not relevant
        controller.showKeyboardOnLoad = self.showKeyboardForCommentsPage;
    } else if ([segue.identifier isEqualToString:@"Likes Segue"]) {
        ShowLikesTVC *controller = segue.destinationViewController;
        controller.newsfeedID = self.newsfeedID;
    } else if ([segue.identifier isEqualToString:@"Profile Segue"]) {
        PlayerDetailTVC *controller = segue.destinationViewController;
        controller.playerUserID = self.selectedPlayer;
    } else if ([segue.identifier isEqualToString:@"popup_segue"]) {
        FBShareRequestDialog *controller = segue.destinationViewController;
        controller.image = [self addOverlayToPostImage:self.detailPic.image];
        controller.delegate = self;
    }
}
- (IBAction)commentAction:(id)sender
{
    self.showKeyboardForCommentsPage = YES;
    [self performSegueWithIdentifier:@"Comments Segue" sender:self];
}

- (IBAction)likeListAction:(id)sender
{
    [self performSegueWithIdentifier:@"Likes Segue" sender:self];
}

- (IBAction)allCommentsAction:(id)sender
{
    self.showKeyboardForCommentsPage = NO;
    [self performSegueWithIdentifier:@"Comments Segue" sender:self];
}

- (void)setWithNumCommentsFromVC:(CommentsTVC *)controller withNumComments:(NSUInteger)numComments forSection:(NSUInteger)currentTableSection
{
  //  if (numComments == 1)
    //    self.numCommentsLabel.text = [NSString stringWithFormat:@"%ld Comment", numComments];
  //  else
    //    self.numCommentsLabel.text = [NSString stringWithFormat:@"%ld Comments", numComments];
}

- (IBAction)showLockerActionSheet:(id)sender
{
    if ([self.preferences getUserID] == self.creator.userID) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                      initWithTitle:nil delegate:self
                                      cancelButtonTitle:CANCEL_BUTTON_ACTIONSHEET destructiveButtonTitle:nil
                                      otherButtonTitles:MAKE_COVER, SHARE_POST, FLAG_POST, DELETE_POST, nil];
        [actionSheet showFromTabBar:self.tabBarController.tabBar];
        
    } else if ([self isOneOfTaggedUsers]){
        UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                      initWithTitle:nil delegate:self
                                      cancelButtonTitle:CANCEL_BUTTON_ACTIONSHEET destructiveButtonTitle:nil
                                      otherButtonTitles:MAKE_COVER, SHARE_POST, REMOVE_USER_TAG, FLAG_POST, nil];
        [actionSheet showFromTabBar:self.tabBarController.tabBar];
    } else {
        UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                      initWithTitle:nil delegate:self
                                      cancelButtonTitle:CANCEL_BUTTON_ACTIONSHEET destructiveButtonTitle:nil
                                      otherButtonTitles:SHARE_POST, FLAG_POST, nil];
        [actionSheet showFromTabBar:self.tabBarController.tabBar];
        
    }
    
    
}

// check if session user is of tagged users
- (BOOL)isOneOfTaggedUsers
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userID=%ld && userID!=%ld", [self.preferences getUserID], self.creator.userID];
    NSArray *filteredArray = [self.taggedUsers filteredArrayUsingPredicate:predicate];
    
    if ([filteredArray count] > 0)
        return YES;
    
    return NO;
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == actionSheet.destructiveButtonIndex) {
        [self.view.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    } else {
        NSString *choice = [actionSheet buttonTitleAtIndex:buttonIndex];
        
        if ([choice isEqualToString:FLAG_POST]) {
            [self flagPost];
        } else if ([choice isEqualToString:DELETE_POST]) {
            [self deletePost];
        } else if ([choice isEqualToString:MAKE_COVER]) {
            [self assignLockerAlbumCover];
        } else if ([choice isEqualToString:SHARE_POST]) {
            [self startFBShare];
        } else if ([choice isEqualToString:REMOVE_USER_TAG]) {
            [self removeUserTag];
        }
    }
}

- (void)deletePost
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Delete Confirmation"
                                                    message:@"Are you sure you would like to delete this image?"
                                                   delegate:self cancelButtonTitle:@"Cancel"
                                          otherButtonTitles:@"Delete", nil];
    
    [alert show];
    
}

- (void)flagPost
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Report Confirmation"
                                                    message:@"Are you sure you would like to report this image as inappropriate?"
                                                   delegate:self cancelButtonTitle:@"Cancel"
                                          otherButtonTitles:@"Report", nil];
    
    [alert show];
}

// user should only be able to do this if he is one of the tagged users
- (void)assignLockerAlbumCover
{
    HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    HUD.labelText = @"Upating";
    [IOSRequest makeLockerPostAlbumCover:self.lockerID
                                forSport:self.taggedSport.sportID
                               andSeason:self.picSeason
                                 andYear:self.picYear
                        forsessionUserID:[self.preferences getUserID]
                            onCompletion:^(NSDictionary *results) {
                                
                                dispatch_async(dispatch_get_main_queue(), ^{
                                   
                                    if ([results[@"outcome"] isEqualToString:@"failure"]) {
                                        [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
                                        UIAlertView *alert = [[UIAlertView alloc] init];
                                        alert.title = @"Submission Error";
                                        alert.message = @"There was a problem completing your request.  Please try again.";
                                        [alert addButtonWithTitle:@"OK"];
                                        [alert show];
                                    } else {
                                        HUD.labelText = @"Updated!";
                                        double delayInSeconds = 2.0;
                                        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
                                        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                                            //code to be executed on the main queue after delay
                                            [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
                                            [self.preferences setProfilePageRefreshState:YES];
                                            [self.navigationController popViewControllerAnimated:YES];
                                        });

                                    }
                                });
                                
                            }];
    double delayInSeconds = 10.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        //code to be executed on the main queue after delay
        [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
    });
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"Delete"]) {
        HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        HUD.labelText = @"Deleting";
        [IOSRequest deletePost:self.newsfeedID
                        byUser:[self.preferences getUserID]
                  onCompletion:^(NSDictionary *results) {
                      
                      dispatch_async(dispatch_get_main_queue(), ^{
                          if ([results[@"outcome"] isEqualToString:@"success"]) {
                              HUD.labelText = @"Deleted!";
                              double delayInSeconds = 2.0;
                              dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
                              dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                                  //code to be executed on the main queue after delay
                                  [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
                                  [self.preferences setProfilePageRefreshState:YES];
                                  [self.navigationController popViewControllerAnimated:YES];
                              });
                          } else {
                              [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
                              UIAlertView *alert = [[UIAlertView alloc] init];
                              alert.title = @"Deletion Error!";
                              alert.message = @"There was a problem completing your request.  Please try again.";
                              [alert addButtonWithTitle:@"OK"];
                              [alert show];
                          }
                      });
                  }];
        
        double delayInSeconds = 10.0;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            //code to be executed on the main queue after delay
            [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
        });
    } if ([[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"Report"]) {
        HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        HUD.labelText = @"Reporting Inappropriate";
        [IOSRequest flagPost:self.newsfeedID
                      byUser:[self.preferences getUserID]
                withFeedback:@""
            onCompletion:^(NSDictionary *results) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    if ([results[@"outcome"] isEqualToString:@"success"]) {
                        HUD.labelText = @"Image Reported!";
                        double delayInSeconds = 2.0;
                        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
                        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                            //code to be executed on the main queue after delay
                            [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
                            [self.preferences setProfilePageRefreshState:YES];
                            [self.navigationController popViewControllerAnimated:YES];
                        });
                    } else {
                        [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
                        UIAlertView *alert = [[UIAlertView alloc] init];
                        alert.title = @"Submission Error!";
                        alert.message = @"There was a problem completing your request.  Please try again.";
                        [alert addButtonWithTitle:@"OK"];
                        [alert show];
                    }
                });
                
            }];
    }
}


- (void)startFBShare
{
        /*if (FBSession.activeSession.isOpen && [FBSession.activeSession.permissions indexOfObject:@"publish_actions"] == NSNotFound) {
        // Request publish_actions
        [FBSession.activeSession requestNewPublishPermissions:[NSArray arrayWithObject:@"publish_actions"]
                                              defaultAudience:FBSessionDefaultAudienceFriends
                                            completionHandler:^(FBSession *session, NSError *error) {
                                                __block NSString *alertText;
                                                __block NSString *alertTitle;
                                                if (!error) {
                                                    if ([FBSession.activeSession.permissions
                                                         indexOfObject:@"publish_actions"] == NSNotFound){
                                                        // Permission not granted, tell the user we will not publish
                                                        alertTitle = @"Permission not granted";
                                                        alertText = @"Your action will not be published to Facebook.";
                                                        [[[UIAlertView alloc] initWithTitle:@"title"
                                                                                    message:@"text"
                                                                                   delegate:self
                                                                          cancelButtonTitle:@"OK!"
                                                                          otherButtonTitles:nil] show];
                                                    } else {
                                                        // Permission granted, publish the OG story
                                                        [self presentFBDialog];
                                                    }
                                                    
                                                } else {
                                                    // There was an error, handle it
                                                    // See https://developers.facebook.com/docs/ios/errors/
                                                }
                                            }];*/
    
    if (!FBSession.activeSession.isOpen) {
        // if the session is closed, then we open it here, and establish a handler for state changes
        [FBSession openActiveSessionWithReadPermissions:nil
                                           allowLoginUI:YES
                                      completionHandler:^(FBSession *session,
                                                          FBSessionState state,
                                                          NSError *error) {
                                          if (error) {
                                              UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                                                  message:error.localizedDescription
                                                                                                 delegate:nil
                                                                                        cancelButtonTitle:@"OK"
                                                                                        otherButtonTitles:nil];
                                              [alertView show];
                                          } else {
                                              [self startFBShare];
                                          }
                                      }
         ];
        return;
    } else {
        // permission exists
        [self showFBRequestDialog];
        //[self presentFBDialog];
    }
   
}

/*- (void)initFBRequestDialog
{
    UIView * fbPopupDialog = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [fbPopupDialog setBackgroundColor:[UIColor darkGrayColor2]];
    [fbPopupDialog setAlpha:1];
    
    UIView *window = [[UIView alloc] initWithFrame:CGRectMake(20, 0, [[UIScreen mainScreen] bounds].size.width-40, [[UIScreen mainScreen] bounds].size.height-self.navigationController.navigationBar.frame.size.height-self.tabBarController.tabBar.frame.size.height-20)];
    [window setBackgroundColor:[UIColor colorWithRed:239/255.0 green:239/255.0 blue:244/255.0 alpha:1.0]];
    [fbPopupDialog addSubview:window];
    
    [self.tableView setUserInteractionEnabled:NO];
    [self.view addSubview:fbPopupDialog];
}*/

- (void)showFBRequestDialog
{
    [self performSegueWithIdentifier:@"popup_segue" sender:self];
}

- (void)startShareImage
{
    // Loading overlay
    HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    HUD.labelText = @"Sharing...";
    
    NSMutableDictionary* params = [[NSMutableDictionary alloc] init];
    NSString *myMessage = self.caption;
    [params setObject:myMessage forKey:@"message"]; // caption showed on the top of the sharing image
    UIImage * image = [self addOverlayToPostImage:self.detailPic.image];
    [params setObject:UIImagePNGRepresentation(image) forKey:@"picture"];
    
    [FBRequestConnection startWithGraphPath:@"me/photos"
                                 parameters:params
                                 HTTPMethod:@"POST"
                          completionHandler:^(FBRequestConnection *connection,
                                              id result,
                                              NSError *error)
    {
        [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
        if (error){
            UIAlertView *alert = [[UIAlertView alloc] init];
            alert.title = @"Submission Error!";
            alert.message = [NSString stringWithFormat:@"%@", error];
            [alert addButtonWithTitle:@"OK"];
            [alert show];
        } else {
            HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            HUD.labelText = @"Shared!";
            //[HUD setMode:MBProgressHUDModeText];
            double delayInSeconds = 1.0; // delay one sec for the sucess message, and dismiss
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
            });
        }
    }];
    
    /*NSString *myTitle = [NSString stringWithFormat:@"%@ shared an image via Vaiden", [self.preferences getUserName]];
    NSString *myMessage = [NSString stringWithFormat:@"Image Caption: \"%@\"", self.caption];
    NSString *urlPostPic = [S3Tools2 getFileNameStringWithType:@"photo_regsize" baseString:[NSString stringWithFormat:@"%d/photo/%@", self.picCreatorID, self.imageName]];

    // Put together the dialog parameters
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   myTitle, @"name",
                                   myMessage, @"caption",
                                   @"Vaiden is a community for athletes to play their favorite sports using a social platform.", @"description",
                                   @"https://itunes.apple.com/us/app/vaiden/id796761406?ls=1&mt=8", @"link",
                                   urlPostPic, @"picture",
                                   nil];
    
    // Show the feed dialog
    [FBWebDialogs presentFeedDialogModallyWithSession:nil
                                           parameters:params
                                              handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
                                                  if (error) {
                                                      // An error occurred, we need to handle the error
                                                      // See: https://developers.facebook.com/docs/ios/errors
                                                      //                                         NSLog([NSString stringWithFormat:@"Error publishing story: %@", error.description]);
                                                  } else {
                                                      if (result == FBWebDialogResultDialogNotCompleted) {
                                                          // User cancelled.
                                                          NSLog(@"User cancelled.");
                                                      } else {
                                                          // Handle the publish feed callback
                                                          NSDictionary *urlParams = [self parseURLParams:[resultURL query]];
                                                          
                                                          if (![urlParams valueForKey:@"post_id"]) {
                                                              // User cancelled.
                                                              NSLog(@"User cancelled.");
                                                              
                                                          } else {
                                                              // User clicked the Share button
                                                              NSString *result = [NSString stringWithFormat: @"Posted story, id: %@", [urlParams valueForKey:@"post_id"]];
                                                              NSLog(@"result %@", result);
                                                          }
                                                      }
                                                  }
                                              }];*/
}

- (UIImage *)addOverlayToPostImage:(UIImage *)sourcePic
{
    int total = self.statsGp.statsArray.count;
    
    UIImage *rightCornerLogo = [UIImage imageNamed:@"vaiden_overlay_logo"];
    UIImage *bgShadow = [UIImage imageNamed:@"overlay_shadow"];
    
    if (total > 0) {
        UIImage *seperator = [UIImage imageNamed:@"seperator"];
        UIImage *lineImage;
        
        // draw the lines for single stat
        if (total == 1) {
            CGSize imageSize = CGSizeMake(60, 1);
            UIColor *fillColor = [UIColor whiteColor];
            UIGraphicsBeginImageContextWithOptions(imageSize, YES, 0);
            CGContextRef context = UIGraphicsGetCurrentContext();
            [fillColor setFill];
            CGContextFillRect(context, CGRectMake(0, 0, imageSize.width, imageSize.height));
            lineImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
        }
        
        CGSize size = CGSizeMake(sourcePic.size.width, sourcePic.size.height);
        
        UIGraphicsBeginImageContext(size);
        
        [sourcePic drawInRect:CGRectMake(0, 0, size.width, size.height)];
        [bgShadow drawInRect:CGRectMake(0, size.height-bgShadow.size.height, size.width, bgShadow.size.height)];
        [rightCornerLogo drawInRect:CGRectMake(size.width-rightCornerLogo.size.width-20, size.height-rightCornerLogo.size.height-10, rightCornerLogo.size.width, rightCornerLogo.size.height)];
        
        // Write the stats upon the image
        NSString *upperStr = @"";
        NSString *lowerStr = @"";
        
        for (int i=0; i<total; i++) {
            id test = [self.statsGp.statsArray objectAtIndex:i];
            if ([test isKindOfClass:[StatTag class]]) {
                StatTag *stat = (StatTag *)test;
                if (stat.showSubCategory) {
                    upperStr = stat.statTagValue;
                    lowerStr = [NSString stringWithFormat:@"%@ %@",stat.statLink, stat.statTagName];
                } else {
                    upperStr = stat.statTagValue;
                    lowerStr = stat.statTagName;
                }
            } else if ([test isKindOfClass:[StatTagCombination class]]) {
                StatTagCombination *sta = (StatTagCombination *)test;
                upperStr = [NSString stringWithFormat:@"%@/%@", sta.numerator.statTagValue, sta.denominator.statTagValue];
                lowerStr = sta.numerator.statLink;
            }
            
            CGFloat width = size.width - rightCornerLogo.size.width - 30; //10 for space
            if (total == 1) {
                width = width/2;
            } else if (total == 2) {
                width = width/2;
            } else if (total == 3) {
                width = width/3;
            }
            
            UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:15];
            CGSize lineSize = [upperStr sizeWithAttributes:@{NSFontAttributeName:font}];
            CGRect upperRect;
            if (total == 1) {
                upperRect = CGRectMake(width - lineSize.width/2, size.height- (10 + 35), size.width, width-10);
            } else {
                upperRect = CGRectMake(width*i + width/2 - lineSize.width/2, size.height- (10 + 35), size.width, width-10);
            }
            [[UIColor whiteColor] set];
            [upperStr drawInRect:CGRectIntegral(upperRect) withFont:font];
            
            if (i < total - 1) { // Not the last item
                [seperator drawInRect:CGRectMake(width*(i+1), size.height- (5 + 23), seperator.size.width, seperator.size.height)];
            }
            
            UIFont *font2 = [UIFont fontWithName:@"HelveticaNeue" size:9];
            CGSize lineSize2 = [lowerStr sizeWithAttributes:@{NSFontAttributeName:font2}];
            if (lineSize2.width > width - 10) { // check if the string is out of bound, if so wrap the string by space
                NSArray * sepStr = [self wrapSentences:lowerStr withFont:font2 withinBounds:(width - 10)];
                for (int k=0; k<sepStr.count; k++) {
                    lineSize2 = [[sepStr objectAtIndex:k] sizeWithAttributes:@{NSFontAttributeName:font2}];
                    CGRect lowerRect = CGRectMake(width*i + width/2 - lineSize2.width/2, size.height- (17 + k*8), size.width, width-10);
                    [[sepStr objectAtIndex:k] drawInRect:CGRectIntegral(lowerRect) withFont:font2];
                }
            } else {
                CGRect lowerRect;
                if (total == 1) {
                    lowerRect = CGRectMake(width - lineSize2.width/2, size.height- (10 + 12), size.width, width-10);
                } else {
                    lowerRect = CGRectMake(width*i + width/2 - lineSize2.width/2, size.height- (10 + 12), size.width, width-10);
                }
                [lowerStr drawInRect:CGRectIntegral(lowerRect) withFont:font2];
            }
            
            if (total == 1) {
                [lineImage drawInRect:CGRectMake(width - lineSize2.width/2 - 10 - lineImage.size.width, size.height - (10 + 17), lineImage.size.width, lineImage.size.height)];
                [lineImage drawInRect:CGRectMake(width + lineSize2.width/2 + 10, size.height - (10 + 17), lineImage.size.width, lineImage.size.height)];
            }
        }
        
        UIImage *finalImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return finalImage;
    } else if (self.trainingArray.count > 0) {
        UIImage *drillIcon = [UIImage imageNamed:@"white_drill"];
        
        CGSize size = CGSizeMake(sourcePic.size.width, sourcePic.size.height);
        UIGraphicsBeginImageContext(size);
        
        [sourcePic drawInRect:CGRectMake(0, 0, size.width, size.height)];
        [bgShadow drawInRect:CGRectMake(0, size.height-bgShadow.size.height, size.width, bgShadow.size.height)];
        [rightCornerLogo drawInRect:CGRectMake(size.width-rightCornerLogo.size.width-20, size.height-rightCornerLogo.size.height-10, rightCornerLogo.size.width, rightCornerLogo.size.height)];
        [drillIcon drawInRect:CGRectMake(10, size.height-drillIcon.size.height - 28, drillIcon.size.width, drillIcon.size.height)];
        
        // Write the drills upon the image
        NSString *drillText = @"";
        
        for (TrainingTag *tag in self.trainingArray) {
            if ([drillText length] > 0)
                drillText = [drillText stringByAppendingString:@", "];
            drillText = [drillText stringByAppendingString:tag.trainingTagName];
        }
        
        CGFloat width = size.width - rightCornerLogo.size.width - 30; //10 for space
        
        UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:10];
        //CGSize lineSize = [drillText sizeWithAttributes:@{NSFontAttributeName:font}];
        CGRect textFrame = CGRectMake(10 + drillIcon.size.width + 5, size.height- (10 + 30), width-30, width-10);
        [[UIColor whiteColor] set];
        [drillText drawInRect:CGRectIntegral(textFrame) withFont:font];
        
        UIImage *finalImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return finalImage;
    } else {
        return sourcePic;
    }
}

/* Wrap the sentence without UILabelView by space character based on bounds and font */
- (NSMutableArray *) wrapSentences:(NSString *)source withFont:(UIFont *)font withinBounds:(CGFloat)bounds
{
    NSArray *sec = [source componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSMutableArray * sperateString = [[NSMutableArray alloc] init];
    NSString * part = @"";
    NSString * tmp = @"";
    CGSize tmpSize;
    for (int i=0; i<sec.count; i++) {
        if (part.length == 0) {
            part = (NSString *)[sec objectAtIndex:i];
        } else {
            tmp = [part stringByAppendingString:[NSString stringWithFormat:@" %@", (NSString *)[sec objectAtIndex:i]]];
            tmpSize = [tmp sizeWithAttributes:@{NSFontAttributeName:font}];
        }
        if (tmpSize.width > bounds) {
            [sperateString addObject:part];
            part = @""; // clear string
            tmpSize = CGSizeZero;
            i--; //back to last item
        } else if (i==sec.count-1) { //last item of the array
            [sperateString addObject:part];
        } else if (tmp.length > 0){ // otherwise
            part = tmp;
        }
    }
    NSLog(@"%@",sperateString);
    return sperateString;
}

// A function for parsing URL parameters returned by the Feed Dialog.
/*- (NSDictionary*)parseURLParams:(NSString *)query {
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *pair in pairs) {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        NSString *val =
        [kv[1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        params[kv[0]] = val;
    }
    return params;
}*/


- (void)goToProfile:(UIButton *)sender
{
    self.selectedPlayer = sender.tag;
    [self performSegueWithIdentifier:@"Profile Segue" sender:self];
}

- (void)setWithCell:(LPDTaggedUsersCell *)cell withUserID:(NSInteger)userID
{
    self.selectedPlayer = userID;
    [self performSegueWithIdentifier:@"Profile Segue" sender:self];
}

- (void)removeUserTag
{
    HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    HUD.labelText = @"Removing Tag";
        [IOSRequest removeLockerUserTagForUser:[self.preferences getUserID]
                                     andlocker:self.lockerID
                                  onCompletion:^(NSDictionary *results) {
                                      
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          if ([results[@"outcome"] isEqualToString:@"success"]) {
                                              HUD.labelText = @"Done!";
                                              double delayInSeconds = 2.0;
                                              dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
                                              dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                                                  //code to be executed on the main queue after delay
                                                  [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
                                                  [self.preferences setProfilePageRefreshState:YES];
                                                  [self.preferences setAlertsPageRefreshState:YES];
                                                  [self.navigationController popViewControllerAnimated:YES];
                                              });

                                          } else {
                                              [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
                                              UIAlertView *alert = [[UIAlertView alloc] init];
                                              alert.title = @"Submission Error!";
                                              alert.message = @"There was a problem completing your request.  Please try again.";
                                              [alert addButtonWithTitle:@"OK"];
                                              [alert show];
                                          }
                                      });
                                  }];
    double delayInSeconds = 10.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        //code to be executed on the main queue after delay
        [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
    });
}

#define CELL_CONTENT_WIDTH 200
#define CELL_CONTENT_MARGIN 56
#define FONT_SIZE 10
#define HEIGHT_OF_REST_OF_CELL 60

- (CGFloat)calculateCellHeightForCaptionArea
{
    NSString *aggregateString = self.caption;
    
    CGSize constraint = CGSizeMake(275, 1000.0f);
    
    /********** because sizeWithFont deprecated *************/
    
    NSMutableDictionary *attributes = [NSMutableDictionary new];
    [attributes setObject:[UIFont systemFontOfSize:13] forKey:NSFontAttributeName];
    
    CGRect sizeRect = [aggregateString boundingRectWithSize:constraint options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
    
    CGSize size = sizeRect.size;
    //      CGSize size = [text sizeWithFont:[UIFont systemFontOfSize:FONT_SIZE] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    
    /********* end because sizeWithFont deprecated *********/
    
    return size.height + HEIGHT_OF_REST_OF_CELL  + 55;

}

- (CGFloat)calculateCellHeightForTrainingTag:(NSUInteger)row
{

    NSString *aggregateString = @"";
    
    if ([self.statsGp.statsArray count] > 0) {
        int counter = 0;
        
        for (id obj in self.statsGp.statsArray) {
            
            
            if (counter == 0) {

            } else if ([obj isKindOfClass:[StatTag class]]) {
                
                StatTag *stat = (StatTag *)obj;
                
                if ([aggregateString length] > 0)
                    aggregateString = [aggregateString stringByAppendingString:@", "];
                
                if (stat.showSubCategory)
                    aggregateString = [aggregateString stringByAppendingString:[NSString stringWithFormat:@"%@ %@ %@", stat.statTagValue, stat.statLink, stat.statTagName]];
                else
                    aggregateString = [aggregateString stringByAppendingString:[NSString stringWithFormat:@"%@ %@", stat.statTagValue, stat.statTagName]];
                
            } else if ([obj isKindOfClass:[StatTagCombination class]]) {
                StatTagCombination *sta = (StatTagCombination *)obj;
                if ([aggregateString length] > 0)
                    aggregateString = [aggregateString stringByAppendingString:@", "];
                
                    aggregateString = [aggregateString stringByAppendingString:[NSString stringWithFormat:@"%@/%@ %@", sta.numerator.statTagValue, sta.denominator.statTagValue, sta.numerator.statLink]];
             }
            counter ++;
        }
        
    } else if ([self.trainingArray count] > 0) {
        
   
        
        for (TrainingTag *tag in self.trainingArray) {
            if ([aggregateString length] > 0)
                aggregateString = [aggregateString stringByAppendingString:@", "];
            
            aggregateString = [aggregateString stringByAppendingString:tag.trainingTagName];
        }
        
    }

    NSInteger additionalHeight = 0;
    
    if ([aggregateString length] > 0)
        additionalHeight = 10;
    
    CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH, 1000.0f);
    
    /********** because sizeWithFont deprecated *************/
    
    NSMutableDictionary *attributes = [NSMutableDictionary new];
    [attributes setObject:[UIFont systemFontOfSize:FONT_SIZE] forKey:NSFontAttributeName];
    
    CGRect sizeRect = [aggregateString boundingRectWithSize:constraint options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
    
    CGSize size = sizeRect.size;
    //      CGSize size = [text sizeWithFont:[UIFont systemFontOfSize:FONT_SIZE] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    
    /********* end because sizeWithFont deprecated *********/
    
    return size.height + HEIGHT_OF_REST_OF_CELL + additionalHeight;
}

- (UILabel *)makeDynamicHeighLabel:(NSString *)myText forLabel:(UILabel *)myLabel
{
    CGSize maxLabelSize = CGSizeMake(300,500);
    
    
    /********** because sizeWithFont deprecated *************/
    
    NSMutableDictionary *attributes = [NSMutableDictionary new];
    [attributes setObject:[UIFont systemFontOfSize:myLabel.font.pointSize] forKey:NSFontAttributeName];
    
    CGRect sizeRect = [myText boundingRectWithSize:maxLabelSize options:NSLineBreakByWordWrapping | NSStringDrawingUsesLineFragmentOrigin  attributes:attributes context:nil];
    
    CGSize expectedLabelSize = sizeRect.size;
    //      CGSize expectedLabelSize = [myText sizeWithFont:myLabel.font
    //constrainedToSize:maxLabelSize
    //lineBreakMode:myLabel.lineBreakMode];
    
    /********* end because sizeWithFont deprecated *********/
    
    
    
    //adjust the label the new height.
    CGRect newFrame = myLabel.frame;
    newFrame.size.height = expectedLabelSize.height;
    myLabel.frame = newFrame;
    
    return myLabel;
}


@end
