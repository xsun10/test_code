//
//  ChoosePickupSportCell.h
//  Vaiden
//
//  Created by James Chung on 8/5/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChoosePickupSportCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *sportName;
@property (weak, nonatomic) IBOutlet UIImageView *sportIcon;

@end
