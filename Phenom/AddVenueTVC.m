//
//  AddVenueTVC.m
//  Phenom
//
//  Created by James Chung on 4/3/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "AddVenueTVC.h"
#import "IOSRequest.h"
#import "UserPreferences.h"
#import "MBProgressHUD.h"
#import "UITextField+FormValidations.h"
#import "UITextView+FormValidations.h"
#import "SetVenuePicsVC.h"

@interface AddVenueTVC ()
@property (weak, nonatomic) IBOutlet UITextField *venueNameField;
@property (weak, nonatomic) IBOutlet UITextField *streetAddressField;
@property (weak, nonatomic) IBOutlet UITextField *cityField;
@property (weak, nonatomic) IBOutlet UILabel *stateLabel;
@property (weak, nonatomic) IBOutlet UILabel *countryLabel;
@property (weak, nonatomic) IBOutlet UILabel *hiddenLabel;
@property (weak, nonatomic) IBOutlet UITextView *descriptionField;
@property BOOL descriptionFieldEmpty;
@property (strong, nonatomic) UserPreferences *preferences;
@property (nonatomic, strong) NSString *sportsCodedString;
@property (nonatomic) NSInteger venueID;
@property (weak, nonatomic) IBOutlet UISwitch *isPrivateSwitch;
@property (strong, nonatomic)  UIBarButtonItem *doneButton;

//@property (strong, nonatomic) Venue *venue;

@end

@implementation AddVenueTVC

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

// lazy instantiation
- (UserPreferences *) preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    
    return _preferences;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.descriptionField.text = @"Venue Description";
    self.descriptionField.textColor = [UIColor lightGrayColor];
    self.descriptionFieldEmpty = YES;
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
    
    //[self setDoneButton];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}


/*- (void)setDoneButton
{
    UIButton *btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 50, 21);
    btn.layer.borderColor = [UIColor whiteColor].CGColor;
    btn.layer.borderWidth = 1.0;
    
    [btn addTarget:self action:@selector(done:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundColor:[UIColor clearColor]];
    [btn setTitle:@"DONE" forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-MediumItalic" size:12.0]];
    
    // Initialize UIBarbuttonitem...
    self.doneButton = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = self.doneButton;
}*/

- (BOOL)textViewShouldBeginEditing:(UITextView*)textView {
    if (self.descriptionFieldEmpty) {
        self.descriptionField.text = @"";
        self.descriptionField.textColor = [UIColor blackColor];
        self.descriptionFieldEmpty = NO;
        [self.descriptionField becomeFirstResponder];
    }
    return YES;
}

// did not use this method...
- (void) textViewDidEndEditing:(UITextView*)textView {
    if(self.descriptionField.text.length == 0){
        self.descriptionField.textColor = [UIColor lightGrayColor];
        self.descriptionField.text = @"Description";
        self.descriptionFieldEmpty = YES;
    }
}

- (IBAction)descriptionFieldTap:(id)sender
{
    [self textViewShouldBeginEditing:self.descriptionField];
}

- (IBAction)clearVenueName:(id)sender
{
    self.venueNameField.text = @"";
    [self.venueNameField resignFirstResponder];
}
- (IBAction)clearStreetAddress:(id)sender
{
    self.streetAddressField.text = @"";
    [self.streetAddressField resignFirstResponder];
}

- (IBAction)clearCity:(id)sender
{
    self.cityField.text = @"";
    [self.cityField resignFirstResponder];
}

- (IBAction)done:(id)sender
{
    if ([self isFormOK]) {
        CLLocationCoordinate2D point = [self geoCodeUsingAddress:[NSString stringWithFormat:@"%@, %@, %@", self.streetAddressField.text, self.cityField.text, self.stateLabel.text]];
        HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        HUD.labelText = @"Saving";
        
        NSString *venueDescription;
        if ([self.descriptionField.text isEqualToString:@"Venue Description"]) {
            venueDescription = @"";
        } else {
            venueDescription = self.descriptionField.text;
        }
        self.doneButton.enabled = NO;
        [IOSRequest saveVenueForUser:[self.preferences getUserID]
                           venueName:self.venueNameField.text
                              street:self.streetAddressField.text
                                city:self.cityField.text
                             stateID:self.hiddenLabel.text
                         description:venueDescription
                           longitude:point.longitude
                            latitude:point.latitude
                          withSports:self.sportsCodedString
                           isPrivate:self.isPrivateSwitch.on
                        onCompletion:^(NSDictionary *complete){
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                [MBProgressHUD hideHUDForView:self.view animated:YES];
                                self.doneButton.enabled = YES;

                                if ([complete[@"reachedMaxError"] isEqualToString:@"TRUE"]) {
                                    UIAlertView *alert = [[UIAlertView alloc] init];
                                    alert.title = @"Venue Creation Error";
                                    alert.message = @"You have reached your max venues.  Please delete a venue before creating another.";
                                    [alert addButtonWithTitle:@"OK"];
                                    [alert show];

                                } else if ([complete[@"outcome"] isEqualToString:@"failure"]) {
                                    
                                    UIAlertView *alert = [[UIAlertView alloc] init];
                                    alert.title = @"Venue Creation Error";
                                    alert.message = @"User not located.";
                                    [alert addButtonWithTitle:@"OK"];
                                    [alert show];
                                } else {
                                    self.venueID = [complete[@"venue_id"] integerValue];
                                    
                                    if (self.isCreatingMatch) {
                                        
                                        [self.delegate setMyVenueViewController:self andVenueID:self.venueID andVenueName:self.venueNameField.text];

                                        NSInteger currentIndex = [self.navigationController.viewControllers indexOfObject:self];
                                        [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:currentIndex-3] animated:YES];
                                        
                                        

                                    } else {// only can add pics if we are not creating a match
                                        [self.preferences setVenuesListPageRefreshState:YES];
                                        [self performSegueWithIdentifier:@"Set Venue Pics Segue" sender:self];
                                    }
                                    
                                }
                       
                            });
                        
                        }];
        double delayInSeconds = 10.0;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            //code to be executed on the main queue after delay
            self.doneButton.enabled = YES;
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        });
    
    
    
    }
    
}

- (BOOL)isFormOK
{
    if ([self.venueNameField.text length] == 0) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Venue Name Needed";
        alert.message = @"Please enter the official name of the venue.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
        return NO;
    }
    
    if (![self.venueNameField checkLocationName]) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Venue Name Not Valid";
        alert.message = @"The venue name you entered does not fit our formats.  Please retry.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
        return NO;
    }
    
    if ([self.streetAddressField.text length] == 0) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Street Address Needed";
        alert.message = @"Please enter the street address of the venue.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
        return NO;
    }
    
    if (![self.streetAddressField checkStreetAddress]) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Street Address Not Valid";
        alert.message = @"The street address you entered does not fit our formats.  Please retry.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
        return NO;
    }
    
    if ([self.cityField.text length] == 0) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"City Name Needed";
        alert.message = @"Please enter the city where the venue is located.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
        return NO;
    }
    
    if (![self.cityField checkCityName]) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"City Name Not Valid";
        alert.message = @"The city name you entered does not fit our formats.  Please retry.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
        return NO;
    }
    
    UITableViewCell *stateCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]];
    if ([stateCell.detailTextLabel.text isEqualToString:@"Detail"]) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"State Needed";
        alert.message = @"Please enter the state where the venue is located.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
        return NO;
    }
    
    UITableViewCell *sportsCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
    if ([sportsCell.detailTextLabel.text isEqualToString:@"Detail"]) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Sports Needed";
        alert.message = @"Please enter the sports that can be played at this venue.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
        return NO;
    }
    
    if (![self.descriptionField checkVenueDescription]) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Description Not Valid";
        alert.message = @"The venue description you entered does not fit our formats.  Please retry.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
        return NO;
    }
    return YES
    ;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

// delegate method for choose states

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Venue To States"]) {
        ChooseStatesCDTVC *controller = (ChooseStatesCDTVC *)segue.destinationViewController;
        controller.delegate = self;
    } else if ([segue.identifier isEqualToString:@"Choose Sports Segue"]) {
        AddVenueSportsTVC *controller = (AddVenueSportsTVC *)segue.destinationViewController;
        controller.delegate = self;
    } else if ([segue.identifier isEqualToString:@"Set Venue Pics Segue"]) {
        SetVenuePicsVC *controller = segue.destinationViewController;
        controller.numVCsToPopBack = 3;
        controller.venueID = self.venueID;
    }
}

- (void)setMyViewController:(ChooseStatesCDTVC *)controller withStateName:(NSString *)stateName withStateID:(NSInteger)stateID
{
//    self.stateLabel.text = stateName;
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]];
    cell.detailTextLabel.text = stateName;
//    self.stateLabel.textColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1.0];
    self.hiddenLabel.text = [NSString stringWithFormat:@"%ld", stateID];
    
}

- (void)setViewController:(AddVenueSportsTVC *)controller withSports:(NSMutableArray *)sports
{
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
    
    if ([sports count] > 1)
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%ld sports", [sports count]];
    else if ([sports count] == 1)
        cell.detailTextLabel.text = [[sports objectAtIndex:0] sportName];
    else
        cell.detailTextLabel.text = @"";
    
    
    self.sportsCodedString = @"";
    
    for (int i = 0; i < [sports count]; i++) {
        if (i == 0)
            self.sportsCodedString = [[sports objectAtIndex:i] sportName];
        else
            self.sportsCodedString = [NSString stringWithFormat:@"%@_%@", self.sportsCodedString, [[sports objectAtIndex:i] sportName]];
    }
}

/*

- (void)assignMapImage
{
    NSArray *annotations = self.mapView.annotations;
    CLLocationCoordinate2D coordinate = [(id <MKAnnotation>)[annotations objectAtIndex:0] coordinate];
    NSLog (@"venue longitude:%f", coordinate.longitude);
    NSLog (@"venue latitude:%f", coordinate.latitude);
        
    
    
}
*/

- (CLLocationCoordinate2D) geoCodeUsingAddress:(NSString *)address
{
    double latitude = 0, longitude = 0;

    NSString *esc_addr =  [address stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *req = [NSString stringWithFormat:@"http://maps.google.com/maps/api/geocode/json?sensor=false&address=%@", esc_addr];
    NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:req] encoding:NSUTF8StringEncoding error:NULL];
    if (result) {
        NSScanner *scanner = [NSScanner scannerWithString:result];
        if ([scanner scanUpToString:@"\"lat\" :" intoString:nil] && [scanner scanString:@"\"lat\" :" intoString:nil]) {
            [scanner scanDouble:&latitude];
            if ([scanner scanUpToString:@"\"lng\" :" intoString:nil] && [scanner scanString:@"\"lng\" :" intoString:nil]) {
                [scanner scanDouble:&longitude];
            }
        }
    }
    CLLocationCoordinate2D center;
    center.latitude = latitude;
    center.longitude = longitude;
    return center;
}
@end
