//
//  NewMessagesTVC.m
//  Vaiden
//
//  Created by James Chung on 12/24/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "NewMessagesTVC.h"
#import "NewMessagesCell.h"
#import "IOSRequest.h"
#import "UIView+Manipulate.h"
#import "S3Tools.h"
#import "AFNetworking.h"
#import "UserPreferences.h"
#import "MatchPlayerNews.h"
#import "NSDate+Utilities.h"
#import "UIImage+ProportionalFill.h"
#import "MatchHistoryTVC.h"


@interface NewMessagesTVC ()

@property (nonatomic, strong) NSMutableArray *newMessages;
@property (nonatomic, strong) UserPreferences *preferences;
@property (nonatomic) BOOL noResultsToDisplay;
@property (nonatomic, strong) UIView *tempView;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;

@property (nonatomic) NSInteger selectedRow;

@end

@implementation NewMessagesTVC

- (UserPreferences *)preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (NSMutableArray *)newMessages
{
    if (!_newMessages) _newMessages = [[NSMutableArray alloc] init];
    return _newMessages;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];

    
    [self showTempSpinner];
    
    self.selectedRow = 0;
    
    [self loadMessages];
   
}

- (void)showTempSpinner
{
    self.tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    self.tempView.backgroundColor = [UIColor lightGrayColor];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] init];
    [self.activityIndicator setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width/2.0, [UIScreen mainScreen].bounds.size.height/2.0 - 50)];
    [self.activityIndicator startAnimating];
    [self.tempView addSubview:self.activityIndicator];
    
    [self.view addSubview:self.tempView];
    
}


- (void)loadMessages
{
    [IOSRequest getNewMessagesForUser:[self.preferences getUserID]
                         onCompletion:^(NSMutableArray *resultArray) {
        
                             for (id results in resultArray) {
                                 
                                 NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
                                 [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                                 NSDate *dateTime = [[formatter dateFromString:results[@"ts"]] toLocalTime];
                                 
                                 Player *postingPlayer = [[Player alloc] initWithPlayerDetails:[results[@"player_id"] integerValue]
                                                                                   andUserName:results[@"username"]
                                                                           andProfilePicString:results[@"profile_pic_string"]];
                                 
                                 if ([results[@"match_category"] isEqualToString:@"individual"]) {
                                 
                                     // Only gonna set two values in Individual Match for now
                                     IndividualMatch *iMatch = [[IndividualMatch alloc] init];
                                     [iMatch setMatchID:[results[@"match_id"] integerValue]];
                                     [iMatch setMatchName:results[@"match_name"]];
                                 
                                     PlayerSport *pSport = [[PlayerSport alloc] init];
                                     [pSport setSportID:[results[@"sport_id"] integerValue]];
                                     [pSport setSportName:results[@"sport_name"]];
                                 
                                     [iMatch setSport:pSport];
                                 
                                     MatchPlayerNews *mpn = [[MatchPlayerNews alloc] initWithDetails:iMatch
                                                                                            postDate:dateTime
                                                                                     headLineMessage:[self.preferences getUserName]
                                                                                      generalMessage:nil
                                                                                           forPlayer:postingPlayer
                                                                                         withMessage:results[@"player_message"]];
                                
                                     [self.newMessages addObject:mpn];
                                 } else {
                                     // Only gonna set two values in Individual Match for now
                                     PickupMatch *pMatch = [[PickupMatch alloc] init];
                                     [pMatch setMatchID:[results[@"match_id"] integerValue]];
                                     [pMatch setMatchName:results[@"match_name"]];
                                     
                                     PlayerSport *pSport = [[PlayerSport alloc] init];
                                     [pSport setSportID:[results[@"sport_id"] integerValue]];
                                     [pSport setSportName:results[@"sport_name"]];
                                     
                                     [pMatch setSport:pSport];
                                     
                                     MatchPlayerNews *mpn = [[MatchPlayerNews alloc] initWithDetails:pMatch
                                                                                            postDate:dateTime
                                                                                     headLineMessage:[self.preferences getUserName]
                                                                                      generalMessage:nil
                                                                                           forPlayer:postingPlayer
                                                                                         withMessage:results[@"player_message"]];
                                     
                                     [self.newMessages addObject:mpn];

                                 }
                                 
                             }
                             dispatch_async(dispatch_get_main_queue(), ^{
                                 [self.activityIndicator stopAnimating];
                                 self.tempView.hidden = YES;
                                 if ([resultArray count] == 0) {
                                     self.tableView.separatorColor = [UIColor clearColor];
                                     self.noResultsToDisplay = YES;
                                 } else {
                                     self.noResultsToDisplay = NO;
                                 }
                                 
                                 [self.tableView reloadData];
                             });
        
    }];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.noResultsToDisplay)
        return 1;
    return [self.newMessages count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"New Message Cell";
    NewMessagesCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if (self.noResultsToDisplay) {
        cell.usernameLabel.hidden = YES;
        cell.matchNameLabel.hidden = YES;
        cell.profilePic.hidden = YES;
        cell.dateTimeLabel.hidden = YES;
        cell.commentLabel.hidden = YES;
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.textLabel.text = @"No New Messages";
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.textLabel.textColor = [UIColor lightGrayColor];
        cell.textLabel.hidden = NO;
        return cell;
    }
    MatchPlayerNews *mpn = [self.newMessages objectAtIndex:indexPath.row];
    
    cell.usernameLabel.text = mpn.player.userName;
    
    NSString *url = [S3Tools getFileNameStringWithType:@"thumbnail"
                                            baseString:mpn.player.profileBaseString];
    
    [cell.profilePic setImageWithURL:[NSURL URLWithString:url]
                 placeholderImage:[UIImage imageNamed:@"avatar round"]];
    [UIImage makeRoundedImage:cell.profilePic withRadius:20];
    
    if ([mpn.match isKindOfClass:[IndividualMatch class]])
        cell.matchNameLabel.text = [NSString stringWithFormat:@"Challenge Match: %@", mpn.match.matchName];
    else
        cell.matchNameLabel.text = [NSString stringWithFormat:@"Pickup Match: %@", mpn.match.matchName];

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMMM d, YYYY h:mm a"];

    cell.dateTimeLabel.text = [dateFormatter stringFromDate:mpn.newsDate];
    
    if ([mpn.playerMessage length] > 33) {
        cell.commentLabel.text = [NSString stringWithFormat:@"\"%@...\"", [mpn.playerMessage substringToIndex:33]];
    } else {
        cell.commentLabel.text = [NSString stringWithFormat:@"\"%@\"", mpn.playerMessage];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!self.noResultsToDisplay) {
        self.selectedRow = indexPath.row;
        [self performSegueWithIdentifier:@"Match History Segue" sender:self];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Match History Segue"]) {
        MatchHistoryTVC *controller = segue.destinationViewController;
        
        MatchPlayerNews *mpn = [self.newMessages objectAtIndex:self.selectedRow];
        controller.playerMatch = mpn.match;
        
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.noResultsToDisplay) {
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        return screenRect.size.height - 150;
    }
    return 96;
}
- (IBAction)composeMessage:(id)sender
{
    [self performSegueWithIdentifier:@"Compose Message Segue" sender:self];
}

@end
