//
//  TagDetailTVC.h
//  Vaiden
//
//  Created by Turbo on 7/8/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TagDetailDelegate <NSObject>

- (void)GetGroupedStats:(NSMutableArray *)tagsArray fromView:(UIViewController *)controller;

@end

@interface TagDetailTVC : UITableViewController
@property (nonatomic, weak) id <TagDetailDelegate> delegate;

@property (nonatomic, strong) NSMutableArray *tagsArray;
@property (nonatomic) NSString * statLink;

@end
