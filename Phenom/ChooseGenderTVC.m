//
//  ChooseGenderTVC.m
//  Vaiden
//
//  Created by James Chung on 11/7/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "ChooseGenderTVC.h"

@interface ChooseGenderTVC ()

@end

@implementation ChooseGenderTVC


- (void)viewDidLoad
{
    [super viewDidLoad];

}

- (IBAction)done:(id)sender
{
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *genderString = @"";
    
    switch (indexPath.row) {
        case 0:
             genderString = @"male";
             break;
        case 1:
            genderString = @"female";
            break;
        case 2:
            genderString = @"coed";
            break;
        default:
            break;
    }
    
    [self.delegate setGenderWithController:self withGenderString:genderString];
    [self.navigationController popViewControllerAnimated:YES];
}

@end
