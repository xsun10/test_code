//
//  LoginTableVC.m
//  Phenom
//
//  Created by James Chung on 3/18/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "LoginTableVC.h"
//#import "SkillsTableVC.h"
#import "IOSRequest.h"
#import "UserPreferences.h"
#import "PhenomAppDelegate.h"
#import "UIView+Manipulate.h"


@interface LoginTableVC () <UITextFieldDelegate>

@property (nonatomic, strong) NSDictionary *user;

@property (weak, nonatomic) IBOutlet UITextField *userNameField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loading;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIView *topBk;
@property (weak, nonatomic) IBOutlet UIView *lowBk;

@property (weak, nonatomic) IBOutlet UIButton *clearBtn1;
@property (weak, nonatomic) IBOutlet UIButton *clearBtn2;
@property (strong, nonatomic)  UIBarButtonItem *doneButton;

@property (strong, nonatomic) UserPreferences *preferences;

@property (weak, nonatomic) IBOutlet UIImageView *usernameIcON;
@property (weak, nonatomic) IBOutlet UIImageView *pwdIcon;
@property (weak, nonatomic) IBOutlet UIButton *loginBtn;
@end

@implementation LoginTableVC


// lazy instantiation
-(UserPreferences *) preferences
{
    if (!_preferences) _preferences = [[UserPreferences alloc] init];
    return _preferences;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @""
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    [self.userNameField setAutocorrectionType:UITextAutocorrectionTypeNo];
    [self.passwordField setAutocorrectionType:UITextAutocorrectionTypeNo];
    
    self.userNameField.delegate = self;
    self.passwordField.delegate = self;
    //[self setDoneButton];
    
  //   self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_grain"]];
 //   self.topBk.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_grain"]];
 //   self.lowBk.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_grain"]];
    /*UIColor *grayColor = [UIColor colorWithRed:119/255.0 green:119/255.0 blue:119/255.0 alpha:1.0];
    self.usernameIcON.image = [[UIImage imageNamed:@"username"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.usernameIcON setTintColor:grayColor];
    self.pwdIcon.image = [[UIImage imageNamed:@"lock_grey"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.pwdIcon setTintColor:grayColor];*/
    [self.loginBtn makeRoundedBorderWithRadius:3];
}

/*- (void)setDoneButton
{
    UIButton *btn = [UIButton  buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 50, 21);
    btn.layer.borderColor = [UIColor whiteColor].CGColor;
    btn.layer.borderWidth = 1.0;
    
    [btn addTarget:self action:@selector(login:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundColor:[UIColor clearColor]];
    [btn setTitle:@"Go!" forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-MediumItalic" size:12.0]];
    
    // Initialize UIBarbuttonitem...
    self.doneButton = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = self.doneButton;
}*/

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self.loading stopAnimating];
}

-(void)loginWithUserName:(NSString *)userName andPassword:(NSString *)password
{
    __weak LoginTableVC *weakSelf = self;
    
    NSLog(@"%@ : %@",userName,password);
    
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.labelText = @"Logging In";
    self.doneButton.enabled = NO;
    [IOSRequest loginWithUserName:userName andPassword:password
                   andDeviceToken:[self.preferences getDeviceToken]
                     onCompletion:^(NSDictionary *user){
        dispatch_async(dispatch_get_main_queue(), ^{
            self.doneButton.enabled = YES;
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            if (![user[@"outcome"] isEqualToString:@"failure"]) {
                [weakSelf setUser:user];
                [weakSelf endLogin];
            
                NSInteger userID = [user[@"user_id"] intValue];
                NSLog(@"LoginTableVC User ID is %ld", (long)userID);
                [self.preferences logInSession:userID
                                  andPicString:user[@"profile_pic_string"]
                                   andUserName:userName
                              andVerifiedState:[user[@"verified"] boolValue]];
                [self.preferences setUserPhoneNumber:user[@"phone_number"]];
                [weakSelf performSegueWithIdentifier:@"loggedInTransition" sender:weakSelf];
            } else {
                UIAlertView *alert = [[UIAlertView alloc] init];
                alert.title = @"Login Error";
                alert.message = @"Username or password is not valid.  Please try again.";
                [alert addButtonWithTitle:@"OK"];
                [alert show];
            }
        });
    }];
    
    double delayInSeconds = 10.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        //code to be executed on the main queue after delay
        self.doneButton.enabled = YES;
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    });

}

- (IBAction)login:(id)sender
{
    if (self.userNameField.text.length == 0 || self.passwordField.text.length == 0) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.title = @"Login Error";
        alert.message = @"Please check your username and password, then try again.";
        [alert addButtonWithTitle:@"OK"];
        [alert show];
    } else {
        [self beginLogin];
        [self loginWithUserName:self.userNameField.text andPassword:self.passwordField.text];
    }
}


-(void)setUser:(NSDictionary *)user
{
    if (_user != user) {
        _user = user;
    }
    [self updateUIWithUser:user];
}

-(void)updateUIWithUser:(NSDictionary *)user
{
    if (user) {
 //       self.firstNameLabel.text = user[@"first_name"];
 //       self.lastNameLabel.text = user[@"last_name"];
 //       self.hashLabel.text = user[@"hash"];
 //       self.numberOfSkillsLabel.text = [NSString stringWithFormat:@"%i",[user[@"skills"] count]];
    } else {
        [self resetUI];
    }
}

//-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
//{
//    if ([segue.identifier isEqualToString:@"Skills Segue"]) {
//        SkillsTableViewController *skillsTVC = segue.destinationViewController;
 //       [skillsTVC setSkills:self.user[@"skills"]];
 //   }
//}

-(void)beginLogin
{
    NSLog(@"Attempting Login...");
    [self.loading startAnimating];
    self.loginButton.enabled = NO;
    [self.userNameField resignFirstResponder];
    [self.passwordField resignFirstResponder];
}
- (IBAction)fbLogin:(id)sender
{
 //   PhenomAppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
//    [appDelegate openSession];
}

- (void)loginFailed
{
    // User switched back to the app without authorizing. Stay here, but
    // stop the spinner.
//    [self.spinner stopAnimating];
}

-(void)endLogin
{
    NSLog(self.user ? @"Login Successfull!" : @"Failed to Login!");
    [self.loading stopAnimating];
    self.loginButton.enabled = YES;
}

- (IBAction)clearUserNameField:(id)sender
{
    self.userNameField.text = @"";
}
- (IBAction)clearPasswordField:(id)sender
{
    self.passwordField.text = @"";
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if ([textField.restorationIdentifier isEqualToString:@"1"]) {
        self.clearBtn1.hidden = NO;
        self.clearBtn2.hidden = YES;
    } else {
        self.clearBtn1.hidden = YES;
        self.clearBtn2.hidden = NO;
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    self.clearBtn1.hidden = NO;
    self.clearBtn2.hidden = NO;
}

-(void)resetUI
{
    // Reset Login UI
    self.userNameField.text = @"";
    self.passwordField.text = @"";
    [self endLogin];
    
    
    // Reset User UI
//    self.firstNameLabel.text = @"...";
//    self.lastNameLabel.text = @"...";
//    self.hashLabel.text = @"...";
//    self.numberOfSkillsLabel.text = @"0";
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.userNameField resignFirstResponder];
    [self.passwordField resignFirstResponder];
}
@end
