//
//  IMDSessionUserStatusCell.h
//  Vaiden
//
//  Created by James Chung on 12/26/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IMDSessionUserStatusCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *statusIconBackground;
@property (weak, nonatomic) IBOutlet UIImageView *statusIconImage;
@property (weak, nonatomic) IBOutlet UIImageView *profilePic;
@property (weak, nonatomic) IBOutlet UILabel *playStatusLabel;
@property (weak, nonatomic) IBOutlet UIView *myBackgroundView;

@end
