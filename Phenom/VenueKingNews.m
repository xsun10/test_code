//
//  VenueKingNews.m
//  Vaiden
//
//  Created by James Chung on 3/7/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "VenueKingNews.h"

@implementation VenueKingNews

- (VenueKingNews *)initWithNewsID:(NSInteger)newsMakerID
                        andUsername:(NSString *)newsMakerUsername
                      andProfileStr:(NSString *)newsMakerProfileString
                           withDate:(NSDate *)newsDate
                        andPostType:(NSInteger)postType
                     andNumComments:(NSInteger)numComments
                      andNumRemixes:(NSInteger)numRemixes
                          andNewsID:(NSInteger)newsID
                         andVenue:(Venue *)venueObj
                         andSport:(Sport *)sportObj

{
    self = [super initWithNewsDetails:newsMakerID
                          andUserName:newsMakerUsername
                        andProfileStr:newsMakerProfileString
                             withDate:newsDate
                          andPostType:postType
                       andNumComments:numComments
                        andNumRemixes:numRemixes
                            andNewsID:newsID];
    
    if (self) {
        _sport = sportObj;
        _venue = venueObj;
        
    }
    return self;
}

@end
