//
//  TeamPlayer.m
//  Vaiden
//
//  Created by James Chung on 4/7/14.
//  Copyright (c) 2014 James Chung. All rights reserved.
//

#import "TeamPlayer.h"

@implementation TeamPlayer

- (TeamPlayer *) initWithTeamPlayerDetails:(NSInteger)userID
                               andUserName:(NSString *)userName
                       andProfilePicString:(NSString *)profileBaseString
                           andMemberStatus:(NSString *)memberStatus
{
    self = [super initWithPlayerDetails:userID
                            andUserName:userName
                    andProfilePicString:profileBaseString];
    
    if (self) {
        _memberStatus = memberStatus;
    }
    return self;
    
}

@end
