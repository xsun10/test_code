//
//  PickupMatchCreationNews.h
//  Phenom
//
//  Created by James Chung on 4/23/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "News.h"
#import "PickupMatch.h"

@interface PickupMatchCreationNews : News

@property (nonatomic, strong) PickupMatch *pickupMatch;

- (PickupMatchCreationNews *) initWithPickupMatchCreationNewsDetails:(NSInteger)matchID
                                                        andMatchName:(NSString *)matchName
                                                      matchCreatorID:(NSInteger)matchCreatorID
                                                matchCreatorUserName:(NSString *)matchCreatorUserName
                                                     andProfileStr:(NSString *)profileBaseString
                                                           matchDate:(NSDate *)matchDate
                                                        newsPostDate:(NSDate *)newsPostDate
                                                         andPostType:(NSInteger)postType
                                                           atVenueID:(NSInteger)venueID
                                                         atVenueName:(NSString *)venueName
                                                         withSportID:(NSInteger)sportID
                                                        andSportName:(NSString *)sportName
                                                        andSportType:(NSString *)sportType
                                                           isPrivate:(BOOL)isPrivate
                                                        andMatchStatus:(NSString *)matchStatus
                                                      minCompetitors:(NSUInteger)minCompetitors
                                                  requiredExperience:(NSMutableArray *)requiredExperience
                                                      andMatchTypeID:(NSUInteger)matchTypeID
                                                    andMatchTypeDesc:(NSString *)matchTypeDesc
                                                           andNewsID:(NSInteger)newsID
                                                      andNumComments:(NSUInteger)numComments
                                                       andNumRemixes:(NSUInteger)numRemixes;

@end
