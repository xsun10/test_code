//
//  IndividualMatch.h
//  Phenom
//
//  Created by James Chung on 4/17/13.
//  Copyright (c) 2013 James Chung. All rights reserved.
//

#import "Match.h"
#import "MatchPlayer.h"
#import "IndividualMatchScore.h"

@interface IndividualMatch : Match

/* Phases */
/*
#define INDIVIDUAL_MATCH_PHASE_SESSION_USER_IS_MATCH_CREATOR_AND_OPPONENT_RSVP_UNCONFIRMED 0
#define INDIVIDUAL_MATCH_PHASE_SESSION_USER_IS_MATCH_CREATOR_AND_OPPONENT_RSVP_JOINED 1
#define INDIVIDUAL_MATCH_PHASE_SESSION_USER_IS_MATCH_CREATOR_AND_OPPONENT_RSVP_DECLINED 2
#define INDIVIDUAL_MATCH_PHASE_SESSION_USER_IS_MATCH_CREATOR_AND_MATCH_IS_CANCELLED 3
#define INDIVIDUAL_MATCH_PHASE_SESSION_USER_IS_MATCH_CREATOR_AND_MATCH_EXPIRED 4 // OPPONENT NEVER RSVP'D
#define INDIVIDUAL_MATCH_PHASE_SESSION_USER_IS_MATCH_CREATOR_AND_MATCH_IS_READY_TO_PLAY 5
#define INDIVIDUAL_MATCH_PHASE_SESSION_USER_IS_MATCH_CREATOR_AND_MATCH_NEEDS_SCORE 6
#define INDIVIDUAL_MATCH_PHASE_SESSION_USER_IS_MATCH_CREATOR_AND_MATCH_SCORE_RECORDED_BY_SESSION_USER_BUT_SCORE_IS_NOT_CONFIRMED 7
#define INDIVIDUAL_MATCH_PHASE_SESSION_USER_IS_MATCH_CREATOR_AND_MATCH_SCORE_RECORDED_BY_OPPONENT_BUT_SCORE_IS_NOT_CONFIRMED 8
#define INDIVIDUAL_MATCH_PHASE_SESSION_USER_IS_MATCH_CREATOR_AND_MATCH_SCORE_RECORDED_BY_SESSION_USER_AND_SCORE_IS_CONFIRMED 9
#define INDIVIDUAL_MATCH_PHASE_SESSION_USER_IS_MATCH_CREATOR_AND_MATCH_SCORE_RECORDED_BY_OPPONENT_AND_SCORE_IS_CONFIRMED 10
#define INDIVIDUAL_MATCH_PHASE_SESSION_USER_IS_MATCH_CREATOR_AND_MATCH_SCORE_DISPUTED_BY_MATCH_CREATOR 11
#define INDIVIDUAL_MATCH_PHASE_SESSION_USER_IS_MATCH_CREATOR_AND_MATCH_SCORE_DISPUTED_BY_OPPONENT 12

#define INDIVIDUAL_MATCH_PHASE_SESSION_USER_IS_MATCH_PARTICIPANT_AND_RSVP_UNCONFIRMED 13
#define INDIVIDUAL_MATCH_PHASE_SESSION_USER_IS_MATCH_PARTICIPANT_AND_RSVP_JOINED 14
#define INDIVIDUAL_MATCH_PHASE_SESSION_USER_IS_MATCH_PARTICIPANT_AND_RSVP_DECLINED 15
#define INDIVIDUAL_MATCH_PHASE_SESSION_USER_IS_MATCH_PARTICIPANT_AND_MATCH_IS_CANCELLED 16
#define INDIVIDUAL_MATCH_PHASE_SESSION_USER_IS_MATCH_PARTICIPANT_AND_MATCH_EXPIRED 17
#define INDIVIDUAL_MATCH_PHASE_SESSION_USER_IS_MATCH_PARTICIPANT_AND_MATCH_IS_READY_TO_PLAY 18
#define INDIVIDUAL_MATCH_PHASE_SESSION_USER_IS_MATCH_PARTICIPANT_AND_MATCH_NEEDS_SCORE 19
#define INDIVIDUAL_MATCH_PHASE_SESSION_USER_IS_MATCH_PARTICIPANT_AND_MATCH_SCORE_RECORDED_BY_SESSION_USER_BUT_SCORE_IS_NOT_CONFIRMED 20
#define INDIVIDUAL_MATCH_PHASE_SESSION_USER_IS_MATCH_PARTICIPANT_AND_MATCH_SCORE_RECORDED_BY_OPPONENT_BUT_SCORE_IS_NOT_CONFIRMED 21
#define INDIVIDUAL_MATCH_PHASE_SESSION_USER_IS_MATCH_PARTICIPANT_AND_MATCH_SCORE_RECORDED_BY_SESSION_USER_AND_SCORE_IS_CONFIRMED 22
#define INDIVIDUAL_MATCH_PHASE_SESSION_USER_IS_MATCH_PARTICIPANT_AND_MATCH_SCORE_RECORDED_BY_OPPONENT_AND_SCORE_IS_CONFIRMED 23
#define INDIVIDUAL_MATCH_PHASE_SESSION_USER_IS_MATCH_PARTICIPANT_AND_MATCH_SCORE_DISPUTED_BY_SESSION_USER 24
#define INDIVIDUAL_MATCH_PHASE_SESSION_USER_IS_MATCH_PARTICIPANT_AND_MATCH_SCORE_DISPUTED_BY_OPPONENT 25

#define INDIVIDUAL_MATCH_PHASE_SESSION_USER_IS_NOT_MATCH_PARTICIPANT_AND_MATCH_PENDING 26  // Session user is not allowed to join match
#define INDIVIDUAL_MATCH_PHASE_SESSION_USER_IS_NOT_MATCH_PARTICIPANT_AND_MATCH_READY_TO_PLAY 27
#define INDIVIDUAL_MATCH_PHASE_SESSION_USER_IS_NOT_MATCH_PARTICIPANT_AND_MATCH_IS_CANCELLED 28
#define INDIVIDUAL_MATCH_PHASE_SESSION_USER_IS_NOT_MATCH_PARTICIPANT_AND_MATCH_IS_EXPIRED 29
#define INDIVIDUAL_MATCH_PHASE_SESSION_USER_IS_NOT_MATCH_PARTICIPANT_AND_MATCH_SCORE_RECORDED_BUT_NOT_CONFIRMED 30
#define INDIVIDUAL_MATCH_PHASE_SESSION_USER_IS_NOT_MATCH_PARTICIPANT_AND_MATCH_SCORE_CONFIRMED 31
#define INDIVIDUAL_MATCH_PHASE_SESSION_USER_IS_NOT_MATCH_PARTICIPANT_AND_MATCH_SCORE_DISPUTED 32
 */
/* End Phases */


@property (nonatomic, strong) MatchPlayer *player1;
@property (nonatomic, strong) MatchPlayer *player2;

@property (nonatomic, strong) IndividualMatchScore *scoreIndMatch;

@property (nonatomic) float probabilityPlayer1Wins;
@property (nonatomic) float probabilityPlayer2Wins;

- (IndividualMatch *) init;

- (IndividualMatch *) initMatchWithDetails:(NSInteger)matchID
                              andMatchName:(NSString *)matchName
                                 isPrivate:(BOOL)isPrivate
                                 withSport:(Sport *)sport
                               withMessage:(NSString *)message
                                onDateTime:(NSDate *)dateTime
                                   atVenue:(Venue *)venue
                             createdByUser:(NSInteger)creatorID
                          withMatchPlayer1:(MatchPlayer *)player1
                          withMatchPlayer2:(MatchPlayer *)player2
                                 andActive:(BOOL)active
                 andProbabilityPlayer1Wins:(float)probabilityUser1
                 andProbabilityPlayer2Wins:(float)probabilityUser2;

// no probability list
- (IndividualMatch *) initMatchWithDetails:(NSInteger)matchID
                              andMatchName:(NSString *)matchName
                                 isPrivate:(BOOL)isPrivate
                                 withSport:(Sport *)sport
                               withMessage:(NSString *)message
                                onDateTime:(NSDate *)dateTime
                                   atVenue:(Venue *)venue
                             createdByUser:(NSInteger)creatorID
                          withMatchPlayer1:(MatchPlayer *)player1
                          withMatchPlayer2:(MatchPlayer *)player2
                                 andActive:(BOOL)active;
        //                andConfirmedStatus:(BOOL)isConfirmed;

- (BOOL)wasMatchConfirmedBySessionUser:(NSInteger)sessionUserID;

- (NSString *)winnerUsernameForRound:(NSInteger)roundNumber;

- (BOOL)isMatchRSVPConfirmed;

- (void)setScoreInfoWithServerObject:(NSDictionary *)dataObject withSessionUserID:(NSInteger)sessionUserID;

- (BOOL)isScoreConfirmed;
- (BOOL)isScoreRecorded;
- (BOOL)isScoreDisputed;

// The player that is not the session user
- (MatchPlayer *)getChallengerPlayerObject:(NSInteger)sessionUserID;

- (NSString *)getMatchStatusMessage:(NSUInteger)sessionUserID;
- (BOOL)canSessionUserJoinMatch:(NSUInteger)sessionUserID;
- (void)setPlayerProbabilities:(NSDictionary *)object;
- (NSString *)usernameOfUserWhoDidNotRecordOrConfirmScore;
- (NSString *)getScoreUnitValue;
- (Player *)getOtherPlayer:(NSInteger)userID;
- (NSString *)getSessionUserStatusMessage:(NSInteger)sessionUserID;

- (void)loadCompetitorArrayFromObject:(NSArray *)competitorObject;

- (MatchPlayer *)getWinner;
- (MatchPlayer *)getLoser;
- (BOOL)isTie;
- (MatchPlayer *)getPlayerForID:(NSInteger)playerID;

- (NSString *)getUsernameForScoreUserID:(NSInteger)userID;

- (BOOL)isUserMatchParticipant:(NSUInteger)userID;

@end
